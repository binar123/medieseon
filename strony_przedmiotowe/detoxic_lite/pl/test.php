<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once ('googleads/vendor/autoload.php');


use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\Reporting\v201609\ReportDownloader;
use Google\AdsApi\AdWords\Reporting\v201609\DownloadFormat;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\Common\OAuth2TokenBuilder;


/**
 * Downloads CRITERIA_PERFORMANCE_REPORT for the specified client customer ID.
 */
class DownloadCriteriaReportWithAwql {

    public static function runExample(AdWordsSession $session, $reportFormat) {

        // Create report query to get the data for last 7 days.
        $reportQuery = 'SELECT CampaignId, AdGroupId, Id, Criteria, CriteriaType, '
            . 'Impressions, Clicks, Cost FROM CRITERIA_PERFORMANCE_REPORT '
            . 'WHERE Status IN [ENABLED, PAUSED] DURING LAST_7_DAYS';
        // Download report as a string.
        $reportDownloader = new ReportDownloader($session);
        $reportDownloadResult = $reportDownloader->downloadReportWithAwql(
            $reportQuery, $reportFormat);
        print "Report was downloaded and printed below:\n";
        print $reportDownloadResult->getAsString();
    }
    public static function main() {


        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->fromFile()
            ->build();
        // See: ReportSettingsBuilder for more options (e.g., suppress headers)
        // or set them in your adsapi_php.ini file.
        $reportSettings = (new ReportSettingsBuilder())
            ->fromFile()
            ->includeZeroImpressions(false)
            ->build();
        // See: AdWordsSessionBuilder for setting a client customer ID that is
        // different from that specified in your adsapi_php.ini file.
        // Construct an API session configured from a properties file and the OAuth2
        // credentials above.
        $session = (new AdWordsSessionBuilder())
            ->fromFile()
            ->withOAuth2Credential($oAuth2Credential)
            ->withReportSettings($reportSettings)
            ->build();
        self::runExample($session, DownloadFormat::CSV);
    }
}




DownloadCriteriaReportWithAwql::main();



/*
print_r($_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']);

$h = curl_init();
//http://acdns.pro/forms/?target=-4AAIJIAJzFgAAAAAAAAAAAARz40blAA
curl_setopt($h, CURLOPT_URL, "https://api.adcombo.com/order/create/");
curl_setopt($h, CURLOPT_POST, true);

$web['api_key']='08d09fe5c071b341febea90047ccd58f';
$web['name']='john malkovicc';
$web['phone']='7873432344';
$web['offer_id']=6900;
$web['country_code']='PL';
$web['base_url']='detoxic24.com';
$web['price']='200';
$web['referrer']='google.com';
$web['ip']='86.111.105.65';


curl_setopt($h, CURLOPT_POSTFIELDS,$web);
curl_setopt($h, CURLOPT_HEADER, false);
curl_setopt($h, CURLOPT_RETURNTRANSFER, 1);

//$result = curl_exec($h);

//print_r($result);*/