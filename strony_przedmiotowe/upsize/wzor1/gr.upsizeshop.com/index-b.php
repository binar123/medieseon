<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 9005 -->
        <script>var locale = "cy";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIkFQQST-GJAAEAAQACjxQBAAItIwIGAQEABFo9NOgA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Μπουσης Δημητρης';
            var phone_hint = '+306972426877';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cy");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/favicon.ico" rel="shortcut icon"/>
        <script src="./assets/jquery.countdown.js"></script>
        <script>

            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
                $('body').click(function(){
                    $(".js_errorMessage2").remove();
                });
            });


            /* Таймер */
            $(document).ready(function () {
                if ($('#timer').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer').html()), 1);
                    $('#timer').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' + '<span class="margin minutes">{m10}{m1}</span>' + '<span class="seconds">{s10}{s1}</span>'
                    }).removeClass('hidden');
                }
                ;
                if ($('#timer2').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer2').html()), 1);
                    $('#timer2').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' + '<span class="margin minutes">{m10}{m1}</span>' + '<span class="seconds">{s10}{s1}</span>'
                    }).removeClass('hidden');
                }

            });
        </script>
    </head>
    <body>
    <div id="wrapper" class="s__main">
        <header id="header">
            <div class="wrap">
                <div class="logo">UpSize</div>
                <div class="holder">
                    <ul id="nav">
                        <li><a class="scrollto lt2 for-event" data-href="#home"> Κεντρική σελίδα </a></li>
                        <li><a class="scrollto lt3 for-event" data-href="#breast-cream"> Upsize </a></li>
                        <li><a class="scrollto lt4 for-event" data-href="#expert"> Άποψη ειδικών </a></li>
                        <li><a class="scrollto lt5 for-event" data-href="#comment-review"> Αξιολογήσεις </a></li>
                    </ul>
                    <button class="link-order lt6 pre_toform"> Παράγγειλε τώρα</button>
                </div>
            </div>
        </header>
        <div class="w1" id="home">
            <div class="intro"><strong class="logo"><span class="log">  UpSize </span>
                    <span>Ανόρθωση - μια για πάντα!</span></strong>
                <div class="photo"><img alt="image description" src="./assets/mobile/img-01.png"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/mobile/pack.png"/></div>
                <div class="block">
                    <div class="info-box"><img alt="image description" height="39" src="./assets/mobile/icon-01.png" width="48"/>
                        <p class="lt8"> Φυσική μεγέθυνση στήθους </p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-02.png" width="48"/>
                        <p class="lt9"> Διορθωτική στήθους και ανόρθωση</p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-03.png" width="48"/>
                        <p class="lt10"> Μόνο φυσικά συστατικά</p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-04.png" width="48"/>
                        <p class="lt11"> 95% των γυναικών επιβεβαιώνουν τα αποτελέσματα </p>
                    </div>
                </div>
                <div class="panel">
                    <span class="text lt12">ΜΟΝΟ  <span> ΣΗΜΕΡΑ</span></span>
                    <span class="price new-price price_main">
<span class="price_main_value">
<span class="js-pp-new">98</span>
</span>
<span class="price_main_currency"><span class="js_curs"> € </span></span>
</span> <span class="discount"> -50% </span>
                    <strike class="old-price price_old"><span class="price_main_value"><span class="js-pp-old">49</span></span><span class="price_main_currency"><span class="js_curs"> € </span></span></strike>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-container">
                <h2 class="lt18">Upsize - αποτελεσμα με ογκο! </h2>
                <div class="holder">
                    <ul class="list">
                        <li class="lt19"> 100% φυσικός όγκος</li>
                        <li class="lt20"> Κατάλληλο για γυναίκες κάθε ηλικίας</li>
                        <li class="lt22">Η διαδικασία γήρανσης του δέρματος γίνεται 3 φορές πιο αργά</li>
                    </ul>
                    <ul class="list alt">
                        <li class="lt23"> Αυξάνει το μέγεθος του στήθους σας κατά δύο μεγέθη</li>
                        <li class="lt24"> Θα ξεχάσετε τις ραγάδες</li>
                        <li class="lt25"> Κάνει το στήθος σας στρογγυλό και σφιχτό, προβάλει το σχήμα</li>
                    </ul>
                </div>
            </div>
            <div class="step-block" id="breast-cream">
                <div class="heading">
                    <h2 class="lt26"> Αποτελεσμα του αορατου σουτιεν </h2>
                    <p class="lt27"> το στήθος σας δεν θα χρειάζεται πλέον υποστήριξη </p>
                </div>
                <ul class="step-list">
                    <li><strong class="lt28">Βήμα 1</strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-03.png" width="144"/>
                        <p><span class="lt29"> Εφαρμόστε την κρέμα σε καθαρό δέρμα. Αρχικά κάντε μασάζ στο δεξί στήθος για 5-10 λεπτά,
						μέχρι να απορροφηθεί πλήρως η κρέμα. Επαναλάβετε πολλές φορές. </span>
                            <br/></p>
                    </li>
                    <li><strong class="lt31">Βήμα 2</strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-04.png" width="144"/>
                        <p class="lt32"> Στη συνέχεια κάντε μασάζ στο δεύτερο στήθος. Μετά από αυτό περιμένετε μέχρι η κρέμα
                            να
                            στεγνώσει. </p>
                    </li>
                    <li><strong class="lt33">Βήμα 3</strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-05.png" width="144"/>
                        <p class="lt34">Στο τέλος κάντε μασάζ και στα δύο στήθη με κυκλικές κινήσεις στην περιφέρεια,
                            από το κάτω μέρος του στήθους προς τα πάνω και ολοκληρώστε εκεί απ' όπου
                            ξεκινήσατε. </p>
                    </li>
                    <li><strong class="lt35">Βήμα 4</strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-06.png" width="144"/>
                        <p class="lt36" id="pro"> Χρησιμοποιήστε το προϊόν δύο φορές την ημέρα: το πρωί και το
                            απόγευμα. </p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="expert">
                <h2 class="lt37"> Η γνωμη του ειδικου για το Upsize </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/mobile/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p class="lt38">Για πολλές γυναίκες σήμερα η αισθητική ανάπλαση στήθους είναι απαραίτητη.
                                Ουσιαστικά κάθε γυναίκα επιθυμεί ένα πλούσιο και στητό στήθος. Δεν προκαλεί
                                έκπληξη το γεγονός ότι το ντεκολτέ αποτελούσε πάντα για περιοχή αυξημένου
                                ενδιαφέροντος για τους άνδρες.</p>
                        </div>
                        <p class="lt39">Εάν χρησιμοποιείτε την κρέμα καθημερινά, ο όγκος του στήθους σας θα αυξηθεί μέσα σε
                            3-4 εβδομάδες, το σχήμα θα δείχνει όλο και πιο στρογγυλό και σφιχτό, ενώ το δέρμα
                            θα γίνει πιο ελαστικό και απαλό.</p>
                        <div class="text-box">
                            <p class="lt40">Η κρέμα έχει περάσει από κλινικές δοκιμές, οι οποίες πραγματοποιήθηκαν από
                                ειδικούς του Παγκόσμιου Οργανισμού Υγείας σε 14 κράτη στον κόσμο. Χιλιάδες
                                γυναίκες επιβεβαίωσαν ότι η κρέμα Upsize είναι αποτελεσματική.</p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/mobile/img-09.jpg"/></div>
                        <div class="box">
                            <h3 class="lt41"> Δεοξυμιροεστρόλη </h3>
                            <p class="lt42"> έχει ισχυρό αναγεννητικό αποτέλεσμα, βοηθά στην ανάπτυξη του στήθους, </p>
                        </div>
                        <div class="box">
                            <h3 class="lt43"> Εκχύλισμα Ρίζας <br/> Pueraria Mirifica</h3>
                            <p class="lt44"> θρέφει και προστατεύει αποτελεσματικά το δέρμα, </p>
                        </div>
                        <div class="box">
                            <h3 class="lt45"> Αιθέριο Έλαιο Ρόδων </h3>
                            <p class="lt46">αυξάνει την αντοχή και την ελαστικότητα του δέρματος, διώχνει τις ραγάδες και
                                αποτρέπει την
                                επανεμφάνισή τους.</p>
                        </div>
                    </div>
                </div>
                <div class="description" id="opinion">
                    <div class="text"><strong class="lt47"> Τόνια Λουκάκη </strong>
                        <span class="lt48">Διδάκτωρ Καθηγήτρια Βιολογικών Επιστημών</span>
                    </div>
                    <img alt="image description" height="79" src="./assets/mobile/img-11.png" width="133"/></div>
            </div>
            <div class="reviews-block" id="comment-review">
                <h2 class="lt49"> Αξιολογησεις για το Upsize </h2>
                <div class="box">
                    <div class="img-holder"><img alt="image description" height="130" src="./assets/mobile/img-07.png" width="130"/>
                        <strong class="name lt50"> Σοφία </strong> <span class="lt51"> 37 ετών </span></div>
                    <div class="holder">
                        <p class="lt52"> Τελευταία άρχισα να παρατηρώ ότι το στήθος μου αλλάζει. Άρχισε να χαλαρώνει και
                            να χάνει την ελαστικότητά του με τα χρόνια. Τα φορέματα με ντεκολτέ δεν μου
                            έκαναν πλέον και έτσι σταμάτησα να τα αγοράζω. Ωστόσο βρήκα τη λύτρωση με αυτή
                            την εκπληκτική κρέμα! Τα αποτελέσματα με άφησαν έκπληκτη! Σε 2 εβδομάδες οι
                            ρυτίδες και οι πτυχώσεις στο στήθος έγιναν ανεπαίσθητες και το στήθος μου πιο
                            σφιχτό. Έναν μήνα αργότερα οι γνωστοί άρχισαν να με βομβαρδίζουν με ερωτήσεις
                            σχετικά με το που είχα κάνει αυτή την τόσο εξαιρετική χειρουργική επέμβαση - και
                            κανείς δεν μπορούσε να πιστέψει ότι με βοήθησε μια απλή κρέμα!</p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder"><img alt="image description" height="130" src="./assets/mobile/img-08.png" width="130"/>
                        <strong class="name lt53"> Αιμιλία</strong> <span class="lt54">24 ετών</span></div>
                    <div class="holder">
                        <p class="lt55"> Το πρόβλημα με το σχήμα και την ελαστικότητα του στήθους μου άρχισε να με
                            ταλαιπωρεί αμέσως μόλις έχασα 21 κιλά. Ήταν τρομερό σοκ για μένα! Το στήθος μου
                            χαλάρωσε και έχασε την ομορφιά του! Άρχισα να αγοράζω διορθωτικά εσώρουχα για να
                            κρύψω το πρόβλημά μου. Η διάθεσή μου ήταν πολύ άσχημη, είχα πέσει σε κατάθλιψη.
                            Σε ηλικία 24 ετών το στήθος μου έδειχνε σαν να ήταν 30! Πήρα την κρέμα από
                            τη μητέρα μου ως δώρο γενεθλίων. Στην αρχή μπορώ να πω ότι ένιωσα να με προσβάλει
                            ένα δώρο αυτού του είδους, αλλά τώρα πλέον νιώθω μεγάλη ευγνωμοσύνη, που δεν
                            μπορώ καν να περιγράψω! Μέσα σε έναν μήνα είχα ένα νέο στήθος και έμοιαζε σαν
                            το στήθος μοντέλου σε διαφήμιση εσωρούχων! Ποτέ δεν θα μπορούσα να φανταστώ ότι
                            αυτό το τέλειο σχήμα θα μπορούσε να είναι φυσικό!</p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order"><span class="decoration-03"></span>
                <div class="price-block">
                    <h3 class="lt56"> ΜΟΝΟ ΣΗΜΕΡΑ <span> ΕΙΔΙΚΗ ΤΙΜΗ  </span></h3>
                    <span class="price new-price price_main"><span class="price_main_value">
<span class="js-pp-new">49</span></span><span class="price_main_currency">
<span class="js_curs"> € </span></span>
</span>
                    <span class="old-price"><span class="lt57"> Παλια τιμη </span>
<strike class="price_old"><span class="price_main_value">
<span class="js-pp-old">98</span></span>
<span class="price_main_currency"><span class="js_curs"> € </span></span></strike></span>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block2">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer2">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order_form order-form" id="order_form" method="POST">
                    <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price" value="49.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAIkFQQST-GJAAEAAQACjxQBAAItIwIGAQEABFo9NOgA">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="Upsize_CY_GR">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="al" value="9005">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="CY">
                    <input type="hidden" name="shipment_vat" value="0.24">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.24">
                    <!--   -->
                    <div class="s__of">
                        <div class="box">
                            <div class="frame">
                                <div class="row">

                                    <select class="select-purple country_select" id="country_code_selector" name="country_code">
                                        <option value="GR"> Ελλάδα</option>
                                        <option value="CY"> Κύπρος</option>
                                       </select>
                                </div>
                                <div class="row">

                                    <div class="text">
                                        <input name="name" placeholder="Το ονομα σου" type="text">
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="text">
                                        <input class="only_number" name="phone" placeholder="Τηλέφωνο" type="text">
                                    </div>
                                </div>
                            </div>
                            <button class="btn-search ">ΠΑΡΑΓΓΕΛΙΑ</button>
                            <div class="toform"></div>
                        </div>
                    </div>

                    <!--   -->

                </form>

            </div>
        </div>

    </div>
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }


        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="./assets/beauty_1.css" rel="stylesheet"/>
    <div class="hidden-window v1_1_1_1">
        <div class="h-w-header">
            <div class="h-w-contant">
                <div class="h-w-header-text">
                    <span><b>UPSIZE</b> - ΠΑΡΑΓΓΕΛΙΑ ΤΩΡΑ!</span>
                </div>
            </div>
        </div>
        <div class="h-w-main">
            <div class="h-w-contant">
                <div class="h-w-left">
                    <div class="h-w-payment h-w-decor-block h-w-vis-mob">
                        <p>Πληρωμή <span>ΜΕ ΑΝΤΙΚΑΤΑΒΟΛΉ</span></p>
                        <ul>
                            <li><span>Η ΠΑΡΑΓΓΕΛΊΑ</span></li>
                            <li><span>ΜΕΤΑΦΟΡΆ</span></li>
                            <li><span>ΠΑΡΆΔΟΣΗ ΚΑΙ ΠΛΗΡΩΜΉ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob h-w-ico h-w-select">
                        <select class="change-package-selector" name="count_select">
                            <option data-slide-index="0" value="1">1 πακέτο</option>
                            <option data-slide-index="1" selected="selected" value="3">2+1 πακέτα</option>
                            <option data-slide-index="2" value="5">3+2 πακέτα</option>
                        </select>
                    </div>
                    <ul class="bx-bx">
                        <li data-value="1">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class=" h-w-item-prod"></div>
                                    </div>
                                    <div class="h-w-item-description">Σφριγηλό και ανορθωμένο στήθος από την πρώτη κιόλας εφαρμογή!</div>
                                </div>
                                <div class="h-w-price">
                                    <p>πακέτο</p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Κανονική τιμή:</span><span>98 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Νέα τιμή:</span><span>49 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Προσοχή:</b> ΥΠΆΡΧΕΙ <span>50% ΈΚΠΤΩΣΗ</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="active" data-value="3">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"></div>
                                        <span><font>+ ένα δώρο</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        Τρίμηνο πρόγραμμα! Συν 1 μέγεθος! Εγγυημένος πλούσιος όγκος!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>πακέτα <span>ένα δώρο</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Κανονική τιμή:</span><span>196 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Νέα τιμή:</span><span>98 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Προσοχή:</b> ΥΠΆΡΧΕΙ <span>50% ΈΚΠΤΩΣΗ</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-value="5">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"><span></span></div>
                                        <span><font>+ ένα δώρο</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        Εξάμηνο πρόγραμμα! Αποτέλεσμα παρόμοιο του χειρουργείου! Αύξηση έως 2 μεγέθη! Χωρίς ουλές και πόνο!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>πακέτα <span>ένα δώρο</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Κανονική τιμή:</span><span>294 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Νέα τιμή:</span><span>147 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Προσοχή:</b> ΥΠΆΡΧΕΙ <span>50% ΈΚΠΤΩΣΗ</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="h-w-right">
                    <div class="h-w-payment h-w-decor-block h-w-hide-mob">
                        <p>Πληρωμή <span>ΜΕ ΑΝΤΙΚΑΤΑΒΟΛΉ</span></p>
                        <ul>
                            <li><span>Η ΠΑΡΑΓΓΕΛΊΑ</span></li>
                            <li><span>ΜΕΤΑΦΟΡΆ</span></li>
                            <li><span>ΠΑΡΆΔΟΣΗ ΚΑΙ ΠΛΗΡΩΜΉ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-honesty h-w-ico h-w-hide-mob">ΚΑΛΉ ΕΥΚΑΙΡΊΑ</div>
                    <div class="h-w-form h-w-decor-block js_scrollForm">
                        <form action="" method="post">
                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="49.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAIkFQQST-GJAAEAAQACjxQBAAItIwIGAQEABFo9NOgA">
                            <input type="hidden" name="goods_id" value="20">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Upsize_CY_GR">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="al" value="9005">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="CY">
                            <input type="hidden" name="shipment_vat" value="0.24">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.24">

                            <div class="h-w-select h-w-ico" style="display:none;">
                                <select id="country_code_selector" name="country_code">
                                    <option value="CY">Κύπρος</option>
                                    <option value="GR">Ελλάδα</option>
                                </select>
                            </div>
                            <div class="h-w-select h-w-ico">
                                <select class="change-package-selector" name="count_select">
                                    <option data-slide-index="0" value="1">1 πακέτο</option>
                                    <option data-slide-index="1" selected="selected" value="3">2+1 πακέτα</option>
                                    <option data-slide-index="2" value="5">3+2 πακέτα</option>
                                </select>
                            </div>
                            <div style="display:none;">
                                <input name="name" type="text"/>
                                <input class="only_number" name="phone" type="text"/>
                            </div>
                            <input name="address" placeholder="Διεύθυνση" type="text"/>
                            <!--  -->
                            <div class="h-w-text-total">Συνολική τιμή: <span><font class="js_full_price">98</font> €</span></div>

                        </form>
                    </div>
                    <div class="h-w-guarantee h-w-decor-block">
                        <p>ΕΓΓΥΟΎΜΑΣΕ:</p>
                        <ul>
                            <li><b>100%</b> ποιότητα</li>
                            <li><b>Έλεγχο του προϊόντος <br> κατά την παράδοση</br></b></li>
                            <li>Ασφάλεια των προσωπικών σας δεδομένων</li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob"><div class="h-w-honesty h-w-ico">ΚΑΛΉ ΕΥΚΑΙΡΊΑ</div></div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 9005 -->
        <script>var locale = "cy";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIkFQQST-GJAAEAAQACjxQBAAItIwIGAQEABFo9NOgA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Μπουσης Δημητρης';
            var phone_hint = '+306972426877';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cy");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="./assets/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="./assets/favicon.ico" rel="shortcut icon"/>
        <script src="./assets/jquery.countdown.js"></script>
        <script>

            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
                $('body').click(function(){
                    $(".js_errorMessage2").remove();
                });
            });


            /* Таймер */
            $(document).ready(function () {
                if ($('#timer').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer').html()), 1);
                    $('#timer').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' + '<span class="margin minutes">{m10}{m1}</span>' + '<span class="seconds">{s10}{s1}</span>'
                    }).removeClass('hidden');
                }
                ;
                if ($('#timer2').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer2').html()), 1);
                    $('#timer2').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' + '<span class="margin minutes">{m10}{m1}</span>' + '<span class="seconds">{s10}{s1}</span>'
                    }).removeClass('hidden');
                }

            });
        </script>
    </head>
    <body>
    <div class="s__main" id="wrapper">
        <header id="header">
            <div class="wrap">
                <div class="logo"> UpSize</div>
                <div class="holder">
                    <ul id="nav">
                        <li><a class="scrollto lt2 for-event" data-href="#home"> Κεντρική σελίδα </a></li>
                        <li><a class="scrollto lt3 for-event" data-href="#breast-cream"> Upsize </a></li>
                        <li><a class="scrollto lt4 for-event" data-href="#expert"> Άποψη ειδικών </a></li>
                        <li><a class="scrollto lt5 for-event" data-href="#comment-review"> Αξιολογήσεις </a></li>
                    </ul>
                    <button class="link-order lt6 pre_toform">Παράγγειλε τώρα</button>
                </div>
            </div>
        </header>
        <div class="w1 s__main" id="home">
            <div class="intro"><strong class="logo"><span class="log">UpSize</span>
                    <span>Ανόρθωση - μια για πάντα!</span></strong>
                <div class="photo"><img alt="image description" src="./assets/img-01.png"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/pack.png"/></div>
                <div class="block">
                    <div class="info-box"><img alt="image description" height="39" src="./assets/icon-01.png" width="48"/>
                        <p class="lt8"> Φυσική μεγέθυνση στήθους </p>
                        <img alt="image description" height="39" src="./assets/icon-02.png" width="48"/>
                        <p class="lt9"> Διορθωτική στήθους και ανόρθωση</p>
                        <img alt="image description" height="39" src="./assets/icon-03.png" width="48"/>
                        <p class="lt10"> Μόνο φυσικά συστατικά</p>
                        <img alt="image description" height="39" src="./assets/icon-04.png" width="48"/>
                        <p class="lt11"> 95% των γυναικών επιβεβαιώνουν τα αποτελέσματα </p>
                    </div>
                </div>
                <div class="panel">
                    <span class="text lt12">ΜΟΝΟ  <span> ΣΗΜΕΡΑ</span></span>
                    <span class="price new-price price_main">
<span class="price_main_value">
<span class="js-pp-new">49</span>
</span>
<span class="price_main_currency"><span class="js_curs"> € </span></span>
</span> <span class="discount"> -50% </span>
                    <strike class="old-price price_old"><span class="price_main_value"><span class="js-pp-old">98</span></span><span class="price_main_currency"><span class="js_curs"> € </span></span></strike>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-container">
                <h2 class="lt18">Upsize - αποτελεσμα με ογκο! </h2>
                <div class="holder">
                    <ul class="list">
                        <li class="lt19"> 100% φυσικός όγκος</li>
                        <li class="lt20"> Κατάλληλο για γυναίκες κάθε ηλικίας</li>
                        <li class="lt22">Η διαδικασία γήρανσης του δέρματος γίνεται 3 φορές πιο αργά</li>
                    </ul>
                    <ul class="list alt">
                        <li class="lt23"> Αυξάνει το μέγεθος του στήθους σας κατά δύο μεγέθη</li>
                        <li class="lt24"> Θα ξεχάσετε τις ραγάδες</li>
                        <li class="lt25"> Κάνει το στήθος σας στρογγυλό και σφιχτό, προβάλει το σχήμα</li>
                    </ul>
                </div>
            </div>
            <div class="step-block" id="breast-cream">
                <div class="heading">
                    <h2 class="lt26"> Αποτελεσμα του αορατου σουτιεν </h2>
                    <p class="lt27"> το στήθος σας δεν θα χρειάζεται πλέον υποστήριξη </p>
                </div>
                <ul class="step-list">
                    <li><strong class="lt28">Βήμα 1</strong>
                        <img alt="image description" height="144" src="./assets/img-03.png" width="144"/>
                        <p><span class="lt29"> Εφαρμόστε την κρέμα σε καθαρό δέρμα. Αρχικά κάντε μασάζ στο δεξί στήθος για 5-10 λεπτά,
						μέχρι να απορροφηθεί πλήρως η κρέμα. Επαναλάβετε πολλές φορές. </span>
                            <br/></p>
                    </li>
                    <li><strong class="lt31">Βήμα 2</strong>
                        <img alt="image description" height="144" src="./assets/img-04.png" width="144"/>
                        <p class="lt32"> Στη συνέχεια κάντε μασάζ στο δεύτερο στήθος. Μετά από αυτό περιμένετε μέχρι η κρέμα
                            να
                            στεγνώσει. </p>
                    </li>
                    <li><strong class="lt33">Βήμα 3</strong>
                        <img alt="image description" height="144" src="./assets/img-05.png" width="144"/>
                        <p class="lt34">Στο τέλος κάντε μασάζ και στα δύο στήθη με κυκλικές κινήσεις στην περιφέρεια,
                            από το κάτω μέρος του στήθους προς τα πάνω και ολοκληρώστε εκεί απ' όπου
                            ξεκινήσατε. </p>
                    </li>
                    <li><strong class="lt35">Βήμα 4</strong>
                        <img alt="image description" height="144" src="./assets/img-06.png" width="144"/>
                        <p class="lt36" id="pro"> Χρησιμοποιήστε το προϊόν δύο φορές την ημέρα: το πρωί και το
                            απόγευμα. </p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="expert">
                <h2 class="lt37"> Η γνωμη του ειδικου για το Upsize </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p class="lt38">Για πολλές γυναίκες σήμερα η αισθητική ανάπλαση στήθους είναι απαραίτητη.
                                Ουσιαστικά κάθε γυναίκα επιθυμεί ένα πλούσιο και στητό στήθος. Δεν προκαλεί
                                έκπληξη το γεγονός ότι το ντεκολτέ αποτελούσε πάντα για περιοχή αυξημένου
                                ενδιαφέροντος για τους άνδρες.</p>
                        </div>
                        <p class="lt39">Εάν χρησιμοποιείτε την κρέμα καθημερινά, ο όγκος του στήθους σας θα αυξηθεί μέσα σε
                            3-4 εβδομάδες, το σχήμα θα δείχνει όλο και πιο στρογγυλό και σφιχτό, ενώ το δέρμα
                            θα γίνει πιο ελαστικό και απαλό.</p>
                        <div class="text-box">
                            <p class="lt40">Η κρέμα έχει περάσει από κλινικές δοκιμές, οι οποίες πραγματοποιήθηκαν από
                                ειδικούς του Παγκόσμιου Οργανισμού Υγείας σε 14 κράτη στον κόσμο. Χιλιάδες
                                γυναίκες επιβεβαίωσαν ότι η κρέμα Upsize είναι αποτελεσματική.</p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/img-09.jpg"/></div>
                        <div class="box">
                            <h3 class="lt41"> Δεοξυμιροεστρόλη </h3>
                            <p class="lt42"> έχει ισχυρό αναγεννητικό αποτέλεσμα, βοηθά στην ανάπτυξη του στήθους, </p>
                        </div>
                        <div class="box">
                            <h3 class="lt43"> Εκχύλισμα Ρίζας <br/> Pueraria Mirifica</h3>
                            <p class="lt44"> θρέφει και προστατεύει αποτελεσματικά το δέρμα, </p>
                        </div>
                        <div class="box">
                            <h3 class="lt45"> Αιθέριο Έλαιο Ρόδων </h3>
                            <p class="lt46">αυξάνει την αντοχή και την ελαστικότητα του δέρματος, διώχνει τις ραγάδες και
                                αποτρέπει την
                                επανεμφάνισή τους.</p>
                        </div>
                    </div>
                </div>
                <div class="description" id="opinion">
                    <div class="text"><strong class="lt47"> Τόνια Λουκάκη </strong>
                        <span class="lt48">Διδάκτωρ Καθηγήτρια Βιολογικών Επιστημών</span>
                    </div>
                    <img alt="image description" height="79" src="./assets/img-11.png" width="133"/></div>
            </div>
            <div class="reviews-block" id="comment-review">
                <h2 class="lt49"> Αξιολογησεις για το Upsize </h2>
                <div class="box">
                    <div class="img-holder"><img alt="image description" height="130" src="./assets/img-07.png" width="130"/>
                        <strong class="name lt50"> Σοφία </strong> <span class="lt51"> 37 ετών </span></div>
                    <div class="holder">
                        <p class="lt52"> Τελευταία άρχισα να παρατηρώ ότι το στήθος μου αλλάζει. Άρχισε να χαλαρώνει και
                            να χάνει την ελαστικότητά του με τα χρόνια. Τα φορέματα με ντεκολτέ δεν μου
                            έκαναν πλέον και έτσι σταμάτησα να τα αγοράζω. Ωστόσο βρήκα τη λύτρωση με αυτή
                            την εκπληκτική κρέμα! Τα αποτελέσματα με άφησαν έκπληκτη! Σε 2 εβδομάδες οι
                            ρυτίδες και οι πτυχώσεις στο στήθος έγιναν ανεπαίσθητες και το στήθος μου πιο
                            σφιχτό. Έναν μήνα αργότερα οι γνωστοί άρχισαν να με βομβαρδίζουν με ερωτήσεις
                            σχετικά με το που είχα κάνει αυτή την τόσο εξαιρετική χειρουργική επέμβαση - και
                            κανείς δεν μπορούσε να πιστέψει ότι με βοήθησε μια απλή κρέμα!</p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder"><img alt="image description" height="130" src="./assets/img-08.png" width="130"/>
                        <strong class="name lt53"> Αιμιλία</strong> <span class="lt54">24 ετών</span></div>
                    <div class="holder">
                        <p class="lt55"> Το πρόβλημα με το σχήμα και την ελαστικότητα του στήθους μου άρχισε να με
                            ταλαιπωρεί αμέσως μόλις έχασα 21 κιλά. Ήταν τρομερό σοκ για μένα! Το στήθος μου
                            χαλάρωσε και έχασε την ομορφιά του! Άρχισα να αγοράζω διορθωτικά εσώρουχα για να
                            κρύψω το πρόβλημά μου. Η διάθεσή μου ήταν πολύ άσχημη, είχα πέσει σε κατάθλιψη.
                            Σε ηλικία 24 ετών το στήθος μου έδειχνε σαν να ήταν 30! Πήρα την κρέμα από
                            τη μητέρα μου ως δώρο γενεθλίων. Στην αρχή μπορώ να πω ότι ένιωσα να με προσβάλει
                            ένα δώρο αυτού του είδους, αλλά τώρα πλέον νιώθω μεγάλη ευγνωμοσύνη, που δεν
                            μπορώ καν να περιγράψω! Μέσα σε έναν μήνα είχα ένα νέο στήθος και έμοιαζε σαν
                            το στήθος μοντέλου σε διαφήμιση εσωρούχων! Ποτέ δεν θα μπορούσα να φανταστώ ότι
                            αυτό το τέλειο σχήμα θα μπορούσε να είναι φυσικό!</p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order"><span class="decoration-03"></span>
                <div class="price-block">
                    <h3 class="lt56"> ΜΟΝΟ ΣΗΜΕΡΑ <span> ΕΙΔΙΚΗ ΤΙΜΗ  </span></h3>
                    <span class="price new-price price_main"><span class="price_main_value">
<span class="js-pp-new">49</span></span><span class="price_main_currency">
<span class="js_curs"> € </span></span>
</span>
                    <span class="old-price"><span class="lt57"> Παλια τιμη </span>
<strike class="price_old"><span class="price_main_value">
<span class="js-pp-old">98</span></span>
<span class="price_main_currency"><span class="js_curs"> € </span></span></strike></span>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block2">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer2">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order_form order-form" id="order_form" method="POST">
<!--
                    <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price" value="49.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAIkFQQST-GJAAEAAQACjxQBAAItIwIGAQEABFo9NOgA">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="Upsize_CY_GR">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="al" value="9005">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="CY">
                    <input type="hidden" name="shipment_vat" value="0.24">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.24">

                    <!--  -->
                    <div class="s__of">
                        <h3 class="lt59"></h3>
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                    <div class="holder">
                                    </div>
                                    <select class="select-purple country_select" id="country_code_selector" name="country_code">
                                        <option value="GR"> Ελλάδα</option>
                                        <option value="CY"> Κύπρος</option>
                                      </select>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <span class="info name_helper"></span>
                                    </div>
                                    <div class="text">
                                        <input name="name" placeholder="Το ονομα σου" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <span class="info info_phone phone_helper only_number"></span>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" name="phone" placeholder="Τηλέφωνο" type="text">
                                    </div>
                                </div>
                            </div>
                            <button class="btn-search ">ΠΑΡΑΓΓΕΛΙΑ</button>
                            <div class="toform"></div>
                        </div>
                    </div>
                    <!--  -->
                </form>
            </div>
        </div>
    </div>
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }


        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="./assets/beauty_1.css" rel="stylesheet"/>
    <div class="hidden-window v1_1_1_1">
        <div class="h-w-header">
            <div class="h-w-contant">
                <div class="h-w-header-text">
                    <span><b>UPSIZE</b> - ΠΑΡΑΓΓΕΛΙΑ ΤΩΡΑ!</span>
                </div>
            </div>
        </div>
        <div class="h-w-main">
            <div class="h-w-contant">
                <div class="h-w-left">
                    <div class="h-w-payment h-w-decor-block h-w-vis-mob">
                        <p>Πληρωμή <span>ΜΕ ΑΝΤΙΚΑΤΑΒΟΛΉ</span></p>
                        <ul>
                            <li><span>Η ΠΑΡΑΓΓΕΛΊΑ</span></li>
                            <li><span>ΜΕΤΑΦΟΡΆ</span></li>
                            <li><span>ΠΑΡΆΔΟΣΗ ΚΑΙ ΠΛΗΡΩΜΉ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob h-w-ico h-w-select">
                        <select class="change-package-selector" name="count_select">
                            <option data-slide-index="0" value="1">1 πακέτο</option>
                            <option data-slide-index="1" selected="selected" value="3">2+1 πακέτα</option>
                            <option data-slide-index="2" value="5">3+2 πακέτα</option>
                        </select>
                    </div>
                    <ul class="bx-bx">
                        <li data-value="1">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class=" h-w-item-prod"></div>
                                    </div>
                                    <div class="h-w-item-description">Σφριγηλό και ανορθωμένο στήθος από την πρώτη κιόλας εφαρμογή!</div>
                                </div>
                                <div class="h-w-price">
                                    <p>πακέτο</p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Κανονική τιμή:</span><span>98 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Νέα τιμή:</span><span>49 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Προσοχή:</b> ΥΠΆΡΧΕΙ <span>50% ΈΚΠΤΩΣΗ</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="active" data-value="3">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"></div>
                                        <span><font>+ ένα δώρο</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        Τρίμηνο πρόγραμμα! Συν 1 μέγεθος! Εγγυημένος πλούσιος όγκος!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>πακέτα <span>ένα δώρο</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Κανονική τιμή:</span><span>196 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Νέα τιμή:</span><span>98 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Προσοχή:</b> ΥΠΆΡΧΕΙ <span>50% ΈΚΠΤΩΣΗ</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-value="5">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"><span></span></div>
                                        <span><font>+ ένα δώρο</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        Εξάμηνο πρόγραμμα! Αποτέλεσμα παρόμοιο του χειρουργείου! Αύξηση έως 2 μεγέθη! Χωρίς ουλές και πόνο!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>πακέτα <span>ένα δώρο</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Κανονική τιμή:</span><span>294 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Νέα τιμή:</span><span>147 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Προσοχή:</b> ΥΠΆΡΧΕΙ <span>50% ΈΚΠΤΩΣΗ</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="h-w-right">
                    <div class="h-w-payment h-w-decor-block h-w-hide-mob">
                        <p>Πληρωμή <span>ΜΕ ΑΝΤΙΚΑΤΑΒΟΛΉ</span></p>
                        <ul>
                            <li><span>Η ΠΑΡΑΓΓΕΛΊΑ</span></li>
                            <li><span>ΜΕΤΑΦΟΡΆ</span></li>
                            <li><span>ΠΑΡΆΔΟΣΗ ΚΑΙ ΠΛΗΡΩΜΉ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-honesty h-w-ico h-w-hide-mob">ΚΑΛΉ ΕΥΚΑΙΡΊΑ</div>
                    <div class="h-w-form h-w-decor-block js_scrollForm">
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIAIkFQAAAAAAAAAAAASVCIYnAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="49.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAIkFQQST-GJAAEAAQACjxQBAAItIwIGAQEABFo9NOgA">
                            <input type="hidden" name="goods_id" value="20">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Upsize_CY_GR">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="al" value="9005">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="CY">
                            <input type="hidden" name="shipment_vat" value="0.24">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.24">

                            <div class="h-w-select h-w-ico" style="display:none;">
                                <select id="country_code_selector" name="country_code">
                                    <option value="CY">Κύπρος</option>
                                    <option value="GR">Ελλάδα</option>
                                </select>
                            </div>
                            <div class="h-w-select h-w-ico">
                                <select class="change-package-selector" name="count_select">
                                    <option data-slide-index="0" value="1">1 πακέτο</option>
                                    <option data-slide-index="1" selected="selected" value="3">2+1 πακέτα</option>
                                    <option data-slide-index="2" value="5">3+2 πακέτα</option>
                                </select>
                            </div>
                            <div style="display:none;">
                                <input name="name" type="text"/>
                                <input class="only_number" name="phone" type="text"/>
                            </div>
                            <input name="address" placeholder="Διεύθυνση" type="text"/>
                            <!--  -->
                            <div class="h-w-text-total">Συνολική τιμή: <span><font class="js_full_price">98</font> €</span></div>

                        </form>
                    </div>
                    <div class="h-w-guarantee h-w-decor-block">
                        <p>ΕΓΓΥΟΎΜΑΣΕ:</p>
                        <ul>
                            <li><b>100%</b> ποιότητα</li>
                            <li><b>Έλεγχο του προϊόντος <br> κατά την παράδοση</br></b></li>
                            <li>Ασφάλεια των προσωπικών σας δεδομένων</li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob"><div class="h-w-honesty h-w-ico">ΚΑΛΉ ΕΥΚΑΙΡΊΑ</div></div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    </body>
    </html>
<?php } ?>