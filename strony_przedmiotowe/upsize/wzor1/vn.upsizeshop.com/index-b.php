<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 3461 -->
        <script>var locale = "vn";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIlDASBV-aJAAEAAQACxwsBAAKFDQGXAjoCBIim8ZUA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 67500;
            var name_hint = 'Phạm Xuân';
            var phone_hint = '+84917989737';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("vn");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <link href="//st.acstnst.com/content/UpSize_VN/mobile/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
        <title>UpSize</title>
        <link href="./assets/mobile/index.css" media="all" rel="stylesheet" type="text/css"/>
        <!--Second Page-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,greek-ext,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic&amp;subset=latin,greek-ext,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/style2.css" media="all" rel="stylesheet"/>
        <!--Second Page-->
        <style>
            .order .line span.errorMedium {
                color: #ff0000 !important;
            }
        </style>
        <script src="./assets/jquery.bxslider.min.js"></script>
    </head>
    <body>
    <div class="wrapper hidejs">
        <div class="w0">
            <div class="w1">
                <div class="header">
                    <h1><a>UpSize</a></h1>
                    <div class="description">
                        <p>Nâng ngực tự nhiên</p>
                        <p>Có tác dụng chỉnh hình và nâng ngực</p>
                        <p>Chỉ gồm các thành phần tự nhiên</p>
                        <p>95% nữ giới xác nhận có kết quả</p>
                    </div>
                    <div class="btn-wrap toform">
                        <a class="btn-order jsOpenWindow" href="#order_form">Đặt hàng</a>
                    </div>
                    <div class="b-text">
                        <div class="title">Nâng ngực!</div>
                        <span>một lần và mãi mãi!</span>
                    </div>
                </div>
                <!-- header -->
                <div class="page-info">
                    <div class="dscription">100% hàm lượng tự nhiên</div>
                    <div class="dscription">Phù hợp cho nữ giới ở mọi độ tuổi</div>
                    <div class="dscription">Làm chậm quá trình lão hóa da đến 3 lần</div>
                    <div class="dscription">Vòng ngực nở nang đến 2 cỡ áo</div>
                    <div class="dscription">Những vết rạn da chỉ còn là quá khứ</div>
                    <div class="dscription">Giúp vòng 1 tròn đầy và săn chắc, khuôn ngực nở nang</div>
                </div>
                <!-- page info -->
                <div class="how-to-use">
                    <div class="content">
                        <div class="box">
                            <h3>Bước 1</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="./assets/mobile/show-1.png"/>
                                </div>
                                <div class="description">Thoa kem lên vùng da sạch. Đầu tiên, mát xa ngực trong vòng 5-10 phút bên phải cho đến khi kem thấm hoàn toàn. Lặp lại vài lần.</div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Bước 2</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="./assets/mobile/show-2.png"/>
                                </div>
                                <div class="description">Tiếp đến mátxa ngực bên trái. Sau đó đợi cho đến khi kem khô hoàn toàn.</div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Bước 3</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="./assets/mobile/show-311.png"/>
                                </div>
                                <div class="description">Cuối cùng là mátxa cả 2 bên ngực với động tác theo hình xoắn ốc từ dưới lên trên và kết thúc ở nơi bắt đầu.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- how-to-use -->
                    <div class="reviews">
                        <h2>Nhận xét về UpSize</h2>
                        <div class="content">
                            <div class="box-reviews">
                                <div class="wrap-title">
                                    <div class="title"><span class="name">Thùy An,</span>37 tuổi</div>
                                </div>
                                <div class="text">
                                    <div class="wrapimg"><img alt="" src="./assets/mobile/ava2.png"/>
                                    </div>
                                    Gần đây tôi bắt đầu cảm nhận được những thay đổi của bộ ngực, làn da trở lên nhăn nheo và mất đi tính đàn hồi vốn có theo thời gian. Những bộ cánh xẻ ngực giờ không còn phù hợp để trưng diện, và tôi cũng chẳng thiết tha mua sắm quần áo nữa. Nhưng tôi đã tìm thấy sự cứu của cuộc đời và dó là loại kem tuyệt vời này! Kết quả làm tôi hết sức ngạc nhiên! Trong vòng hai tuần làn da nhăn nheo và chảy xệ nơi vùng ngực đã hoàn toàn biến mất đồng thời kết cấu ngực trở lên săn chắc hơn. Một tháng sau đó, tôi bị dội bom bởi vô số những câu hỏi về nơi mình đã đi làm phẫu thuật để có được kết quả mỹ mãn như vậy - và chẳng ai tin rằng tôi chỉ dùng loại kem hết sức thông thường, mà đã cứu cuộc đời tôi.
                                </div>
                            </div>
                            <div class="box-reviews">
                                <div class="wrap-title">
                                    <div class="title"><span class="name">Hương,</span> 24 tuổi</div>
                                </div>
                                <div class="text">
                                    <div class="wrapimg"><img alt="" src="./assets/mobile/ava1.png"/>
                                    </div>
                                    Những vấn đề về hình dáng và độ săn chắc của vòng một bắt đầu xảy đến với mình ngay sau khi mình sút đi 21 kg. Đó thực sự là một cú sốc lớn! Vòng 1 trở lên chảy xệ và mất đi tính thẩm mỹ vốn có! Mình bắt đầu mua áo nâng chỉnh ngực để khắc phục vấn đề này. Cảm xúc thật kinh khủng, tinh thần xuống cấp nghiêm trọng. Vòng 1 như kiểu gái quá lứa dù mới bước sang tuổi 24! Rồi mẹ mình tặng một hộp kem bôi ngực nhân ngày sinh nhật. Ban đầu mình cảm thấy tự ái khi nhận một món quà như vậy, nhưng giờ đây mình thấy biết ơn mẹ rất nhiều! Trong vòng một tháng, vòng 1 như được tái sinh hoàn toàn, cảm giác như đang sở hữu bộ ngực của một nàng người mẫu trên tạp chí nào đó vậy! Cảm giác có bộ ngực hoàn hảo mà sử dụng một cách hết sức tự nhiên thật là sung sướn!

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- reviews -->
                    <div class="sellblock">
                        <h2>- CHỈ CÓ HÔM NAY -</h2>
                        <p>GIÁ ĐẶC BIỆT:
                            <br/><span class="old_p">1 580 000 ₫</span>
                            <br/><span class="new_p"> 790 000 ₫</span>
                        </p>
                    </div>
                    <!-- sellblock -->
                    <form action="./process.php" id="order_form" method="post" name="order_all">
                        <input type="hidden" name="template_name" value="UpSize_VN">
                        <input type="hidden" name="currency" value="₫">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="country_code" value="PL">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="preland_name" value="UpSize_VN">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="goods_id" value="20">
                        <input type="hidden" name="title" value="UpSize - VN">
                        <input type="hidden" name="accept_languages" value="pl-pl">
                        <input type="hidden" name="test" value="1">
                        <input type="hidden" name="price" value="790000">
                        <input type="hidden" name="price_vat" value="0.0">
                        <input type="hidden" name="esub" value="-4AAEvAiUMAAAAAALHCwEAAoUNAZcCOgIEUYYkegA">
                        <input type="hidden" name="shipment_price" value="67500">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="offer_id" value="3109">
                        <input type="hidden" name="old_price" value="1580000">
                        <input type="hidden" name="total_price" value="857500.0">
                        <input type="hidden" name="total_price_wo_shipping" value="790000.0">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <select class="country" id="country_code_selector" name="country">
                            <option value="VN">
                                Việt Nam
                            </option>
                         </select>
                        <input id="name" name="name" placeholder="Họ và tên" type="text" value="">
                        <input class="only_number" data-check="1" id="phone" name="phone" placeholder="Điện thoại" type="text" value="">
                        <!--
                      <span class="js_new_price_curs" id="itog">Giao hàng: 67 500 ₫ </span>
                      <span class="js_full_price_curs" id="itogo">Tổng cộng: 857 500 ₫ </span>
                        -->
                        <input class="jsOpenWindow btn-order js_submit" type="button" value="Đặt hàng">
                        <input type="hidden" name="time_zone" value="1"></form>
                    <!-- order -->
                    <!-- footer -->
                </div>
                <!-- w1 -->
            </div>
            <!-- w0 -->
        </div>
        <!-- wrapper -->
        <script>
            $('.toform').click(function () {
                $("html, body").animate({
                    scrollTop: $("form").offset().top - 300
                }, 1000);
                return false;
            });
        </script>
    </div></body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 3461 -->
        <script>var locale = "vn";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIlDASBV-aJAAEAAQACxwsBAAKFDQGXAjoCBIim8ZUA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>

        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 67500;
            var name_hint = 'Phạm Xuân';
            var phone_hint = '+84917989737';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("vn");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));




            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
        <link href="./assets/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css"/>
        <script src="./assets/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="./assets/main.js" type="text/javascript"></script>
        <link href="//st.acstnst.com/content/UpSize_VN/css/style.css" media="all" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <div id="wrapper">
        <div id="header">
            <div class="wrap">
                <strong class="logo"> UpSize </strong>
                <div class="holder">
                    <ul id="nav">
                        <li><a block="wrapper" href="#"> Trang chủ </a></li>
                        <li><a block="bust" href="#"> UpSize? </a></li>
                        <li><a block="doctor" href="#"> Chuyên gia kiểm chứng </a></li>
                        <li><a block="comments" href="#"> Nhận xét </a></li>
                    </ul>
                    <a block="order_form" class="link-order" href="#"> Đặt hàng ngay </a>
                </div>
            </div>
        </div>
        <div class="w1">
            <div class="intro">
                <strong class="logo"><span class="log"> UpSize</span> <span> Nâng ngực - một lần và mãi mãi! </span></strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/img-01.png" width="448"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/pack.png" style="width: 300px;"/></div>
                <div class="block">
                    <div class="info-box">
                        <img alt="image description" height="39" src="./assets/icon-01.png" width="48"/>
                        <p> Nâng ngực tự nhiên </p>
                        <img alt="image description" height="39" src="./assets/icon-02.png" width="48"/>
                        <p> Có tác dụng chỉnh hình và nâng ngực </p>
                        <img alt="image description" height="39" src="./assets/icon-03.png" width="48"/>
                        <p> Chỉ gồm các thành phần tự nhiên  </p>
                        <img alt="image description" height="39" src="./assets/icon-04.png" width="48"/>
                        <p> 95% nữ giới xác nhận có kết quả</p>
                    </div>
                </div>
                <div class="panel" id="bust">
                    <span class="text"> CHỈ CÓ  <span> HÔM NAY </span></span>
                    <span class="price js_new_price_curs"> 1 580 000 ₫ </span>
                    <span class="discount">-50%</span>
                    <strike class="product_old_price js_old_price_curs"> 790 000 ₫ </strike>
                    <a block="order_form" class="link-order btn-search" href="#"> Đặt hàng </a>
                </div>
            </div>
            <div class="info-container">
                <h2> UpSize - kết quả đáng ngạc nhiên! </h2>
                <div class="holder">
                    <ul class="list">
                        <li>100% thành phần tự nhiên</li>
                        <li> Phù hợp cho nữ giới ở mọi độ tuổi </li>
                        <li> Làm chậm quá trình lão hóa da đến 3 lần
                        </li>
                    </ul>
                    <ul class="list alt">
                        <li> Vòng ngực nở nang đến 2 cỡ áo </li>
                        <li> Những vết rạn da chỉ còn là quá khứ </li>
                        <li> Giúp vòng 1 tròn đầy và săn chắc, khuôn ngực nở nang </li>
                    </ul>
                </div>
            </div>
            <div class="step-block">
                <div class="heading">
                    <h2> Tác dụng như chiếc áo ngực vô hình </h2>
                    <p> Vòng 1 không cần thêm bất kỳ hỗ trợ nào nữa! </p>
                </div>
                <ul class="step-list">
                    <li>
                        <strong> Bước 1 </strong>
                        <img alt="image description" height="144" src="./assets/img-03.png" width="144"/>
                        <p>
                            Thoa kem lên vùng da sạch. Đầu tiên, mát xa ngực trong vòng 5-10 phút bên phải cho đến khi kem thấm hoàn toàn. Lặp lại vài lần.
                        </p>
                    </li>
                    <li>
                        <strong> Bước 2 </strong>
                        <img alt="image description" height="144" src="./assets/img-04.png" width="144"/>
                        <p> Tiếp đến mátxa ngực bên trái. Sau đó đợi cho đến khi kem khô hoàn toàn.  </p>
                    </li>
                    <li>
                        <strong> Bước 3 </strong>
                        <img alt="image description" height="144" src="./assets/img-051.png" width="144"/>
                        <p> Cuối cùng là mátxa cả 2 bên ngực với động tác theo hình xoắn ốc từ dưới lên trên và kết thúc ở nơi bắt đầu.
                        </p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="doctor">
                <h2> Ý kiến chuyên gia về sản phẩm Kem nâng ngực này: </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/doctor.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p>
                                Rất nhiều chị em phụ nữ ngày nay đang ao ước có được vòng 1 hoàn hảo, ai cũng muốn sở hữu bộ ngực căng tròn gì làm lạ khi đây là bộ phận luôn thu hút ánh nhìn của cánh đàn ông.

                            </p>
                        </div>
                        <p>
                            Nếu bạn dùng kem hàng ngày, kích cỡ vòng 1 sẽ nở nang hơn trong vòng  3-4 tuần, khuôn ngực trở lên căng và  chắc, làn da đàn hồi và mềm mịn hơn.

                        </p>
                        <div class="text-box">
                            <p>
                                Sản phẩm trải qua những thử nghiệm lâm sàng được tiến hành bởi các chuyên gia của Tổ chức Y tế Thế giới tại 14 quốc gia trên toàn thế giới. Hàng nghìn phụ nữ đã thừa nhận tính hiệu quả của kem nâng ngực này.

                            </p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/img-09.jpg"/></div>
                        <div class="box">
                            <h3> Deoxymiroestrol </h3>
                            <p> có tác dụng trẻ hóa, đồng thời thúc đẩy sự nở nang của vòng 1; </p>
                        </div>
                        <div class="box">
                            <h3> Chiết xuất từ rễ  <br/>  cây Pueraria Mirifica </h3>
                            <p> giúp dưỡng ẩm và bảo vệ da hiệu quả; </p>
                        </div>
                        <div class="box">
                            <h3> Dầu tinh chất hoa hồng </h3>
                            <p> làm tăng khả năng đàn hồi, loại bỏ vết rạn da đồng thời ngăn ngừa sự xuất hiện. </p>
                        </div>
                    </div>
                </div>
                <div class="description">
                    <div class="text">
                        <strong> Hoàng Lan </strong>
                        <span> Bác sĩ, giáo sư sinh học </span>
                    </div>
                    <img alt="image description" height="79" src="./assets/img-11.png" width="133"/>
                </div>
            </div>
            <div class="reviews-block" id="comments">
                <h2> Nhận xét về UpSize </h2>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" class="rud" height="130" src="./assets/img-07.png" width="130"/>
                        <strong class="name"> Thùy An </strong>
                        <span> 37 tuổi </span>
                    </div>
                    <div class="holder">
                        <p>
                            Gần đây tôi bắt đầu cảm nhận được những thay đổi của bộ ngực, làn da trở lên nhăn nheo và mất đi tính đàn hồi vốn có theo thời gian. Những bộ cánh xẻ ngực giờ không còn phù hợp để trưng diện, và tôi cũng chẳng thiết tha mua sắm quần áo nữa. Nhưng tôi đã tìm thấy sự cứu của cuộc đời  và dó là  loại kem tuyệt vời này! Kết quả làm tôi hết sức ngạc nhiên! Trong vòng hai tuần làn da nhăn nheo và chảy xệ nơi vùng ngực đã hoàn toàn biến mất đồng thời kết cấu ngực trở lên săn chắc hơn. Một tháng sau đó, tôi bị dội bom bởi vô số những câu hỏi về nơi mình đã đi làm phẫu thuật để có được kết quả mỹ mãn như vậy - và chẳng ai tin rằng tôi chỉ dùng loại kem hết sức thông thường, mà đã cứu cuộc đời tôi.

                        </p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" class="rud" height="130" src="./assets/img-08.png" width="130"/>
                        <strong class="name"> Hương </strong>
                        <span> 24 tuổi </span>
                    </div>
                    <div class="holder">
                        <p>
                            Những vấn đề về hình dáng và độ săn chắc của vòng một bắt đầu xảy đến với mình ngay sau khi mình sút đi 21 kg. Đó thực sự là một cú sốc lớn! Vòng 1 trở lên chảy xệ và mất đi tính thẩm mỹ vốn có! Mình bắt đầu mua áo nâng chỉnh ngực để khắc phục vấn đề này. Cảm xúc thật kinh khủng, tinh thần xuống cấp nghiêm trọng. Vòng 1 như kiểu gái quá lứa dù mới bước sang tuổi 24! Rồi mẹ mình tặng một hộp kem bôi ngực nhân ngày sinh nhật. Ban đầu mình cảm thấy tự ái khi nhận một món quà như vậy, nhưng giờ đây mình thấy biết ơn mẹ rất nhiều Trong vòng một tháng, vòng 1 như được tái sinh hoàn toàn, cảm giác như đang sở hữu bộ ngực của một nàng người mẫu trên tạp chí nào đó vậy! Cảm giác có bộ ngực hoàn hảo mà sử dụng một cách hết sức tự nhiên thật là sung sướn!

                        </p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order">
                <img alt="" class="prod" src="./assets/pack.png"/>
                <div class="price-block">
                    <h3> CHỈ CÓ HÔM NAY  <span> GIÁ ĐẶC BIỆT </span></h3>
                    <span class="price js_new_price_curs"> 790 000 ₫ </span>
                    <span class="old-price"> Giá cũ
              <strike>
<span class="product_old_price js_old_price_curs" id="old-price"> 1 580 000 ₫ </span>
</strike>
</span>
                    <div class="timer-wrap">
                        <p> Thời gian còn lại trước khi kết thúc khuyến mại: </p>
                        <div class="timer">
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order-form" id="order_form" method="POST">
                    <h3> Đặt mua </h3>
                    <div class="box">
                        <div class="frame">
                            <div class="row">
                                <div class="holder">
                                    <label for="country_code_selector"> Quốc gia </label>
                                </div>
                                <div class="text">
                                    <select class="country" id="country_code_selector" name="country">
                                        <option value="VN">
                                            Việt Nam
                                        </option>
                                       </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="holder">
                                    <label for="name"> Họ và tên  </label>
                                    <span class="info"> ví dụ: Nguyễn Tố Như </span>
                                </div>
                                <div class="text">
                                    <input id="name" name="name" type="text" value="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="holder">
                                    <label for="phone"> Điện thoại </label>
                                    <span class="info"> ví dụ: 0914245932 </span>
                                </div>
                                <div class="text">
                                    <input class="only_number" data-check="1" id="phone" name="phone" type="text" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block">
                        <!--
                      <dl>
                      <dt> Giao hàng: </dt>
                      <dd><span class="js_new_price_curs" id="itog"> 67 500 ₫ </span>
                      </dd>
                      <dt> Tổng cộng: </dt>
                      <dd><span class="js_full_price_curs" id="itogo"> 857 500 ₫ </span>
                      </dd>
                      </dl>
                        -->

                        <button class="btn-search js_submit submit" type="submit">
                          Đặt hàng
                        </button>

                    </div>
                   <!-- <input type="hidden" name="total_price" value="857500.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAIlDASBV-aJAAEAAQACxwsBAAKFDQGXAjoCBIim8ZUA">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="UpSize - VN">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="UpSize_VN">
                    <input type="hidden" name="price" value="790000">
                    <input type="hidden" name="old_price" value="1580000">
                    <input type="hidden" name="total_price_wo_shipping" value="790000.0">
                    <input type="hidden" name="package_prices" value="{}">
                    <input type="hidden" name="currency" value="₫">
                    <input type="hidden" name="package_id" value="0">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="VN">
                    <input type="hidden" name="shipment_vat" value="0.0">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="shipment_price" value="67500">
                    <input type="hidden" name="price_vat" value="0.0">
                    <fieldset>
                        <h3> Đặt mua </h3>
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                      <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIALXDQAAAAAAAAAAAASLtDODAA"></iframe>

                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <!--
                          <dl>
                          <dt> Giao hàng: </dt>
                          <dd><span class="js_new_price_curs" id="itog"> 67 500 ₫ </span>
                          </dd>
                          <dt> Tổng cộng: </dt>
                          <dd><span class="js_full_price_curs" id="itogo"> 857 500 ₫ </span>
                          </dd>
                          </dl>
                            -->

                </form>
            </div>
            <div id="footer">
            </div>
        </div>
    </div>
    </body>
    </html>
<?php } ?>