<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 6985 -->
        <script>var locale = "sk";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIAEwQC6OWJAAEAAQACZxIBAAJJGwIGAQEABFkyU7EA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Martin Mičiak';
            var phone_hint = '0908267142';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("sk");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
        <title>UPSIZE</title>
        <link href="./assets/mobile/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/jquery.countdown.css" media="all" rel="stylesheet"/>
        <link href="./assets/mobile/favicon.ico" rel="shortcut icon"/>
        <style>.order .line span.errorMedium { color: #ff0000 !important; } </style>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <script src="./assets/mobile/js/jquery.countdown.js"></script>
        <script src="./assets/mobile/js/main.js" type="text/javascript"></script>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $(".js_errorMessage2").remove();
                        $('.toform').click();
                    }
                });
            });
        </script>
    </head>
    <body>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#phone').keypress(function (e) {
                if (!(e.which == 8 || (e.which > 47) & (e.which <= 57))) {
                    return false;
                }
            });

        });
    </script>
    <div class="wrapper s__main">
        <div class="w0">
            <div class="w1">
                <div class="header">
                    <h1><a>UPSIZE</a></h1>
                    <div class="description">Prirodzené zväčšenie pŕs
                        <br/> Korekcia a lifting pŕs
                        <br/> Iba prírodné zložky
                        <br/> 95% žien potvrdili výsledky
                    </div>
                    <div class="b-text">
                        <div class="panel">
                            <div class="block1">IBA DNES</div>
                            <div class="block2">ŠPECIÁLNA CENA <br/> <span class="yelpay-mobsale-basediscountprice" style="color:black;"><span class="js-pp-new">49</span> <span class="js_curs">€</span></span> <br/><span>STARÁ CENA<br/>
<s class="yelpay-mobsale-baseprice"><span class="js-pp-old">98</span> <span class="js_curs">€</span></s></span>
                            </div>
                            <div class="block3">50%</div>

                            <button class="button rushOrder pre_toform"><span>Objednať</span></button>

                        </div>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="blk_block">
                        <div class="title">UpSize</div>
                        <span>OBJEMNÝ VÝSLEDOK!</span>
                    </div>
                </div>
            </div>
            <!-- header -->
            <div class="page-info">
                <div class="dscription">
                    <h3>100% prírodný objem</h3>
                </div>
                <div class="dscription">
                    <h3>Vhodné pre ženy bez ohľadu na ich vek</h3>
                </div>
                <div class="dscription">
                    <h3>Proces vädnutie kože sa stáva 3x pomalší</h3>
                </div>
            </div>
            <!-- page info -->
            <div class="how-to-use">
                <h2><span>EFEKT NEVIDITEĽNEJ PODPRSENKY</span>Vaše prsia už nebudú potrebovať podporu!</h2>
                <div class="content">
                    <div class="box">
                        <h3>1. Krok</h3>
                        <div class="c-box">
                            <div class="sm-cover"><img alt="" src="./assets/mobile/show-1.png"/></div>
                            <div class="description">
                                Krém sa aplikuje na čistú pokožku. Najprv masírujte pravý prsník po dobu 5-10 minút, kým sa krém úplne neabsorbuje. Opakujte niekoľkokrát.
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <h3>2. Krok</h3>
                        <div class="c-box">
                            <div class="sm-cover"><img alt="" src="./assets/mobile/show-2.png"/></div>
                            <div class="description">
                                Potom masírujte druhý prsník. Potom počkajte, kým krém vyschne.
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <h3>3. Krok</h3>
                        <div class="c-box">
                            <div class="sm-cover"><img alt="" src="./assets/mobile/show-3.png"/></div>
                            <div class="description">
                                V závere masírujte oba prsníky kruhovým pohybov pozdĺž obrysu zo spodnej časti prsníka nahor, a skončite tam, kde ste začali.
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <h3>4. Krok</h3>
                        <div class="c-box">
                            <div class="sm-cover"><img alt="" src="./assets/mobile/show-4.png"/></div>
                            <div class="description">
                                Používajte dvakrát denne: ráno a večer.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- how-to-use -->
            <div class="reviews">
                <h2>HODNOTENIA UPSIZE</h2>
                <div class="content">
                    <div class="box-reviews">
                        <div class="wrap-title">
                            <div class="title"><span class="name">Sofia, 37 rokov  </span></div>
                        </div>
                        <div class="text">
                            <div class="wrapimg"><img alt="" src="./assets/mobile/img-07.png"/></div>
                            V poslednej dobe som začala pozorovať, že sa moje prsia mení. Ide o to, že koža ochabuje a stráca svoju bývalú elasticitu, ako idú roky. Šaty s hlbokým výstrihom už mi nesedí, tak som ich prestala kupovať. Ale našla som spásu v tomto super prípravku! Výsledky ma naozaj prekvapili! Za 2 týždne sú vrásky a záhyby na mojich prsiach sotva viditeľné, a moje prsia sú pevnejšie. O mesiac neskôr som ma priateľky bombardovali otázkami, kde mi urobili taký skvelý zákrok - a nikto mi stále neverí, že to bol len obyčajný krém, ktorý mi pomohol!
                        </div>
                    </div>
                    <div class="box-reviews">
                        <div class="wrap-title">
                            <div class="title"><span class="name">Emilie, 24 rokov</span></div>
                        </div>
                        <div class="text">
                            <div class="wrapimg"><img alt="" src="./assets/mobile/img-08.png"/></div>
                            Problémy s tvarom a pružnosťou mojej hrudi začali okamžite, keď som schudla 21 kíl. Bol to pre mňa taký šok! Moje prsia ochabli a stratili svoju krásu! Začala som si kupovať push-up bielizeň. Moja nálada Bolo to hrozné, mala som depresie. Vo veku 24, moje prsia vyzerala, ako keby mi bolo 30! Dostala som krém od svojej matky ako darček k narodeninám. Spočiatku ma to trochu urazilo, ale teraz som tak vďačná, že nemám slov! Počas mesiaca som mám nové prsia a tie vyzerajú, ako by som bola modelka z reklamy na spodnú bielizeň! Nikdy by som neverila, že taký dokonalý tvar môže byť prirodzený!
                        </div>
                    </div>
                </div>
            </div>
            <div class="sellblock">
                <h2>- IBA DNES -</h2>
                <p>ŠPECIÁLNA CENA</p>
                <div class="price yelpay-mobsale-basediscountprice"><span class="js-pp-new">49</span> <span class="js_curs">€</span></div>
                <p>Alter Preis <s class="yelpay-mobsale-baseprice"><span class="js-pp-old">98</span> <span class="js_curs">€</span></s></p>
            </div>



            <div class="order">

                <form action="./process.php" id="order_form" method="post" name="order_all"><input type="hidden" name="template_name" value="UpSize_SK3">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="country_code" value="PL">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="preland_name" value="UpSize_SK3">
                    <input type="hidden" name="shipment_vat" value="0.2">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="accept_languages" value="pl-pl">
                    <input type="hidden" name="test" value="1">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="price_vat" value="0.2">
                    <input type="hidden" name="esub" value="-4AAEvAgATAAAAAAJnEgEAAkkbAgYBAQAECyurwAA">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="offer_id" value="4864">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="total_price" value="49.0">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <!--   -->
                    <div class="s__of">
                        <div class="wrap-lines">
                            <div class="line">
                                <select class="country_select" id="country_code_selector" name="country_code">
                                    <option value="SK">Slovensko</option>
                                  </select>
                            </div>
                            <div class="line">
                                <div class="title">Celé meno</div>
                                <div class="wrapfield">
                                    <input class="f-field" name="name" placeholder="meno" type="text">
                                </div>
                            </div>
                            <div class="line">
                                <div class="title">Telefón</div>
                                <div class="wrapfield">
                                    <input class="only_number f-field" name="phone" placeholder="Telefón" type="text">
                                </div>
                            </div>
                            <!--<div class="line">
                            <div class="title">Zadajte adresu</div>
                            <div class="wrapfield">
                            <input class="f-field f-error" name="address" placeholder="Zadajte adresu" type="text" value=""/>
                            </div>
                            </div>-->
                        </div>
                        <button class="btn-order ">Objednať</button>
                        <div class="toform"></div>
                    </div>
                    <!--    -->
                    <input type="hidden" name="time_zone" value="1"></form>

            </div>
        </div>
    </div>

    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }


        }
    </style>

    <!---->
    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 6985 -->
        <script>var locale = "sk";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIAEwQC6OWJAAEAAQACZxIBAAJJGwIGAQEABFkyU7EA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Martin Mičiak';
            var phone_hint = '0908267142';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("sk");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> Upsize </title>
        <link href="./assets/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css"/>
        <script src="./assets/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="./assets/main.js" type="text/javascript"></script>
        <link href="./assets/index.css" media="all" rel="stylesheet" type="text/css"/></head><body><p>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                          $(".js_errorMessage2").remove();
                        $('.toform').click();
                    }
                });
            });
        </script>

    </p>
    <div class="s__main" id="wrapper">
        <div id="header">
            <div class="wrap">
                <strong class="logo"><a block="wrapper" href="#"> Upsize </a></strong>
                <div class="holder">
                    <ul id="nav">
                        <li><a block="wrapper" href="#"> Hlavná strana </a></li>
                        <li><a block="bust" href="#"> Upsize? </a></li>
                        <li><a block="doctor" href="#"> Názor odborníka </a></li>
                        <li><a block="comments" href="#"> Hodnotenia </a></li>
                    </ul>
                    <button class="link-order pre_toform">Objednať teraz</button>
                </div>
            </div>
        </div>
        <div class="w1">
            <div class="intro">
                <strong class="logo"><a block="order_form" class="other-link-order" href="#"> Upsize  <span> Push-up - raz a navždy! </span></a></strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/img-01.png" width="448"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/decoration-02.png"/></div>
                <div class="block">
                    <div class="info-box">
                        <img alt="image description" height="39" src="./assets/icon-01.png" width="48"/>
                        <p> Prirodzené zväčšenie pŕs </p>
                        <img alt="image description" height="39" src="./assets/icon-02.png" width="48"/>
                        <p> Korekcia a lifting pŕs </p>
                        <img alt="image description" height="39" src="./assets/icon-03.png" width="48"/>
                        <p> Iba prírodné zložky  </p>
                        <img alt="image description" height="39" src="./assets/icon-04.png" width="48"/>
                        <p> 95% žien potvrdili výsledky </p>
                    </div>
                </div>
                <div class="panel" id="bust">
                    <span class="text"> IBA  <span> DNES </span></span>
                    <span class="price js_new_price_curs">98 €</span>
                    <span class="discount">-50%</span>
                    <strike class="product_old_price js_old_price_curs">49 €</strike>
                    <button class="link-order pre_toform">Objednať</button>
                </div>
            </div>
            <div class="info-container">
                <h2> Upsize - objemný výsledok! </h2>
                <div class="holder">
                    <ul class="list">
                        <li> 100% prírodný objem </li>
                        <li> Vhodné pre ženy bez ohľadu na ich vek </li>
                        <li> Proces vädnutie kože sa stáva 3x pomalší
                        </li>
                    </ul>
                    <ul class="list alt">
                        <li> Zväčšuje prsia o dve veľkosti košíčkov </li>
                        <li> Zabudnete na strie </li>
                        <li> Zpevňuje vaše prsia a vyrýsováva kontúry </li>
                    </ul>
                </div>
            </div>
            <div class="step-block">
                <div class="heading">
                    <h2> Efekt neviditeľnej podprsenky </h2>
                    <p> Vaše prsia už nebudú potrebovať podporu! </p>
                </div>
                <ul class="step-list">
                    <li>
                        <strong> Krok 1 </strong>
                        <img alt="image description" height="144" src="./assets/img-03.png" width="144"/>
                        <p>
                            Krém sa aplikuje na čistú pokožku. Najprv masírujte pravý prsník po dobu 5-10 minút, kým sa krém úplne neabsorbuje. Opakujte niekoľkokrát.
                        </p>
                    </li>
                    <li>
                        <strong> Krok 2 </strong>
                        <img alt="image description" height="144" src="./assets/img-04.png" width="144"/>
                        <p> Potom masírujte druhý prsník. Potom počkajte, kým krém vyschne.  </p>
                    </li>
                    <li>
                        <strong> Krok 3 </strong>
                        <img alt="image description" height="144" src="./assets/img-05.png" width="144"/>
                        <p> V závere masírujte oba prsníky kruhovým pohybov pozdĺž obrysu zo spodnej časti prsníka nahor, a skončite tam, kde ste začali.
                        </p>
                    </li>
                    <li>
                        <strong> Krok 4 </strong>
                        <img alt="image description" height="144" src="./assets/img-06.png" width="144"/>
                        <p> Používajte dvakrát denne: ráno a večer. </p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="doctor">
                <h2> Znalecký posudok na Upsize: </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p>
                                Mnoho žien dnes čelí nutnosti estetickej modelácie. Prakticky každý chce mať plné energické prsia. A to nie je prekvapujúce, pretože dekolt bol vždy zónou pánskej zvýšenej pozornosti.

                            </p>
                        </div>
                        <p>
                            Ak budete používať krém každý deň, objem Vašich pŕs za zväčší počas 3-4 týždňov, budú  vyzerať guľatejší a pevnejší, pokožka bude pružnejšia a zamatová.

                        </p>
                        <div class="text-box">
                            <p>
                                Krém absolvoval klinické štúdie u odborníkov zo Svetovej zdravotníckej organizácie v 14 krajinách sveta. Tisíce žien sa uistili, že krém na poprsie Upsize je účinný.

                            </p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/img-09.jpg"/></div>
                        <div class="box">
                            <h3> Deoxymiroestrol </h3>
                            <p> má výrazný omladzujúci účinok, apodporuje rast pŕs; </p>
                        </div>
                        <div class="box">
                            <h3> Extrakt z koreňa  <br/>  Pueraria Mirifica </h3>
                            <p> účinne vyživuje a chráni pokožku; </p>
                        </div>
                        <div class="box">
                            <h3> Ružový esenciálny olej </h3>
                            <p> zvyšuje odolnosť kože a pružnosť, odstraňuje strie a zabraňuje ich navráteniu. </p>
                        </div>
                    </div>
                </div>
                <div class="description">
                    <div class="text">
                        <strong> prof. PhDr. Adéla Makovcová </strong>
                        <span> doktorka biologických vied </span>
                    </div>
                    <img alt="image description" height="79" src="./assets/img-11.png" width="133"/>
                </div>
            </div>
            <div class="reviews-block" id="comments">
                <h2> Hodnotenia Upsize </h2>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/img-07.png" width="130"/>
                        <strong class="name"> Sofia </strong>
                        <span> 37 rokov </span>
                    </div>
                    <div class="holder">
                        <p>
                            V poslednej dobe som začala pozorovať, že sa moje prsia mení. Ide o to, že koža ochabuje a stráca svoju bývalú elasticitu, ako idú roky. Šaty s hlbokým výstrihom už mi nesedí, tak som ich prestala kupovať. Ale našla som spásu v tomto super prípravku! Výsledky ma naozaj prekvapili! Za 2 týždne sú vrásky a záhyby na mojich prsiach sotva viditeľné, a moje prsia sú pevnejšie. O mesiac neskôr som ma priateľky bombardovali otázkami, kde mi urobili taký skvelý zákrok - a nikto mi stále neverí, že to bol len obyčajný krém, ktorý mi pomohol!

                        </p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/img-08.png" width="130"/>
                        <strong class="name"> Emília </strong>
                        <span> 24 rokov </span>
                    </div>
                    <div class="holder">
                        <p>
                            Problémy s tvarom a pružnosťou mojej hrudi začali okamžite, keď som schudla 21 kíl. Bol to pre mňa taký šok! Moje prsia ochabli a stratili svoju krásu! Začala som si kupovať push-up bielizeň. Moja nálada Bolo to hrozné, mala som depresie. Vo veku 24, moje prsia vyzerala, ako keby mi bolo 30! Dostala som krém od svojej matky ako darček k narodeninám. Spočiatku ma to trochu urazilo, ale teraz som tak vďačná, že nemám slov! Počas mesiaca som mám nové prsia a tie vyzerajú, ako by som bola modelka z reklamy na spodnú bielizeň! Nikdy by som neverila, že taký dokonalý tvar môže byť prirodzený!

                        </p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order">
                <span class="decoration-02"></span>
                <div class="price-block">
                    <h3> IBA DNES  <span> ŠPECIÁLNA CENA </span></h3>
                    <span class="price js_new_price_curs">49 €</span>
                    <span class="old-price"> Stará cena
              <strike>
<span class="product_old_price js_old_price_curs" id="old-price">98 €</span>
</strike>
</span>
                    <div class="timer-wrap">
                        <p> Do konca akcie zostáva: </p>
                        <div class="timer">
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order-form" id="order_form" method="POST">
                    <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price" value="49.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAIAEwQC6OWJAAEAAQACZxIBAAJJGwIGAQEABFkyU7EA">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="UpSize_SK3">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="al" value="6985">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="SK">
                    <input type="hidden" name="shipment_vat" value="0.2">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.2">

                    <!--   -->
                    <fieldset class="s__of">
                        <h3>Objednať</h3>
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                    <div class="holder">
                                        <label>Krajina</label>
                                    </div>
                                    <div class="text">
                                        <select class="country" id="country_code_selector" name="country_code">
                                            <option value="SK">Slovensko</option>
                                           </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label>Celé meno</label>
                                    </div>
                                    <div class="text">
                                        <input id="name" name="name" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label>Telefón</label>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" name="phone" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <dl>
                                <dt> Dodanie: </dt>
                                <dd><span class="js_new_price_curs" id="itog">0 €</span>
                                </dd>
                                <dt> Ceľková cena: </dt>
                                <dd><span class="js_full_price_curs" id="itogo">49 €</span>
                                </dd>
                            </dl>
                            <button class="btn-search ">Objednať</button>
                            <div class="toform"></div>
                        </div>
                    </fieldset>
                    <!--    -->
                </form>
            </div>
            <div id="footer"></div>
        </div>
    </div>

    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }


        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="./assets/beauty_1.css" rel="stylesheet"/>

    <!---->


    </body></html>
<?php } ?>