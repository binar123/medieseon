<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 8944 -->
        <script>var locale = "pt";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIsFQTwCuWJAAEAAQAClRQBAALwIgIGAQEABIGJT2wA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Rosa Correia';
            var phone_hint = '+351912629188';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("pt");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/favicon.ico" rel="shortcut icon"/>
        <script src="./assets/jquery.countdown.js"></script>
        <script src="./assets/main.js"></script>
        <script>

            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
                $('body').click(function(){
                    $(".js_errorMessage2").remove();
                });
            });

            /* Таймер */
            $(document).ready(function () {
                if ($('#timer').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer').html()), 1);
                    $('#timer').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' + '<span class="margin minutes">{m10}{m1}</span>' + '<span class="seconds">{s10}{s1}</span>'
                    }).removeClass('hidden');
                }
                ;
                if ($('#timer2').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer2').html()), 1);
                    $('#timer2').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' + '<span class="margin minutes">{m10}{m1}</span>' + '<span class="seconds">{s10}{s1}</span>'
                    }).removeClass('hidden');
                }

            });
        </script>
    </head>
    <body>
    <div id="wrapper" class="s__main">
        <div id="header">
            <div class="wrap">
                <div class="logo"> UpSize</div>
                <div class="holder">
                    <ul id="nav">
                        <li><a class="scrollto lt2 for-event" data-href="#home">Página principal </a></li>
                        <li><a class="scrollto lt3 for-event" data-href="#breast-cream">Upsize? </a></li>
                        <li><a class="scrollto lt4 for-event" data-href="#expert"> Prova especializada </a></li>
                        <li><a class="scrollto lt5 for-event" data-href="#comment-review"> Críticas</a></li>
                    </ul>
                    <a class="link-order lt6 scrollto for-event" data-href="#order">Encomendar agora </a></div>
            </div>
        </div>
        <div class="w1" id="home">
            <div class="intro"><strong class="logo"><span class="log">  UpSize </span>
                    <span> Efeito “push-up” - de uma vez por todas!</span></strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/mobile/img-01.png" width="448"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/mobile/pack.png"/></div>
                <div class="block">
                    <div class="info-box"><img alt="image description" height="39" src="./assets/mobile/icon-01.png" width="48"/>
                        <p class="lt8"> Aumento natural do peito </p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-02.png" width="48"/>
                        <p class="lt9"> Correção e elevação do peito</p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-03.png" width="48"/>
                        <p class="lt10">  Apenas componentes naturais </p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-04.png" width="48"/>
                        <p class="lt11">95% das mulheres confirmaram os resultados</p>
                    </div>
                </div>
                <div class="panel">
                    <span class="text lt12">APENAS <span> HOJE</span></span>
                    <span class="price new-price price_main"><span class="price_main_value"><span class="js-pp-new">98</span></span>
<span class="price_main_currency"><span class="js_curs">€</span></span>
</span>
                    <span class="discount">-50%</span>
                    <strike class="old-price price_old"><span class="price_main_value"><span class="js-pp-old">49</span></span><span class="price_main_currency"><span class="js_curs">€</span></span></strike>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-container">
                <h2 class="lt18"> Upsize - resultado volumizador! </h2>
                <div class="holder">
                    <ul class="list">
                        <li class="lt19">Volume 100% natural</li>
                        <li class="lt20"> Apropriado para mulheres, independentemente da sua idade</li>
                        <li class="lt22"> O processo de descaimento da pele torna-se 3 vezes mais lento</li>
                    </ul>
                    <ul class="list alt">
                        <li class="lt23"> Aumenta o seu peito em dois tamanhos de copa</li>
                        <li class="lt24"> Irá esquecer-se das estrias</li>
                        <li class="lt25">Torna os seus seios redondos e firmes, dá saliência às formas</li>
                    </ul>
                </div>
            </div>
            <div class="step-block" id="breast-cream">
                <div class="heading">
                    <h2 class="lt26">O efeito do sutiã invisível</h2>
                    <p class="lt27"> O seu peito não precisará mais de apoio!</p>
                </div>
                <ul class="step-list">
                    <li><strong class="lt28">Passo 1</strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-03.png" width="144"/>
                        <p><span class="lt29">O creme é aplicado na pele limpa. Primeiro o seio direito deve ser massageado durante 5-10 minutos até que o creme seja totalmente absorvido. Repita várias vezes.</span>
                            <br/></p>
                    </li>
                    <li><strong class="lt31">Passo 2</strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-04.png" width="144"/>
                        <p class="lt32">Em seguida, massageie o seio esquerdo. Depois disso, espere que o creme seja absorvido e repita.</p>
                    </li>
                    <li><strong class="lt33">Passo 3</strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-05.png" width="144"/>
                        <p class="lt34">No final, massageie ambos os seios com movimentos circulares ao longo do contorno, de baixo para cima, e acabe onde começou.</p>
                    </li>
                    <li><strong class="lt35">Passo 4</strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-06.png" width="144"/>
                        <p class="lt36" id="pro">Use duas vezes por dia: de manhã e à noite.</p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="expert">
                <h2 class="lt37"> Opinião de um especialista sobre o Upsize: </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/mobile/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p class="lt38">Muitas mulheres são hoje em dia confrontadas com a necessidade de modelar de forma estética o peito. Praticamente todas as mulheres querem ter um peito cheio e firme. E não é de estranhar, já que o decote sempre foi a zona que chama mais atenção para os homens.</p>
                        </div>
                        <p class="lt39">Se usar o creme diariamente, o volume do seu peito irá aumentar em 3-4 semanas, a forma irá ficar mais redonda e firme, a pele ficará mais elástica e aveludada.</p>
                        <div class="text-box">
                            <p class="lt40">O creme passou por ensaios clínicos realizados por especialistas da Organização Mundial de Saúde em 14 países do mundo. Milhares de mulheres certificaram-se de que o creme Upsize é eficaz. </p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/mobile/img-09.jpg"/></div>
                        <div class="box">
                            <h3 class="lt41">Deoxi-miroestrol</h3>
                            <p class="lt42"> tem um efeito de rejuvenescimento pronunciado, promove o crescimento dos seios; </p>
                        </div>
                        <div class="box">
                            <h3 class="lt43"> Extrato de Raiz  <br/>de "Pueraria Mirifica"</h3>
                            <p class="lt44"> nutre eficientemente e protege a pele; </p>
                        </div>
                        <div class="box">
                            <h3 class="lt45">Óleo Essencial de Rosa</h3>
                            <p class="lt46">aumenta a resistência e elasticidade da pele, remove as estrias e impede o seu surgimento.</p>
                        </div>
                    </div>
                </div>
                <div class="description" id="opinion">
                    <div class="text"><strong class="lt47"> Adelaide Silva </strong> <span class="lt48"> Professora Doutorada em Ciências Biológicas </span>
                    </div>
                    <img alt="image description" height="79" src="./assets/mobile/img-11.png" width="133"/></div>
            </div>
            <div class="reviews-block" id="comment-review">
                <h2 class="lt49">Comentários sobre o Upsize</h2>
                <div class="box">
                    <div class="img-holder"><img alt="image description" height="130" src="./assets/mobile/img-07.png" width="130"/>
                        <strong class="name lt50"> Sofia Maria, </strong> <span class="lt51">37 anos</span></div>
                    <div class="holder">
                        <p class="lt52"> Ultimamente comecei a notar que o meu peito estava a mudar. A coisa que me preocupava é que a pele se tornasse flácida e começasse a perder a sua elasticidade anterior com o passar dos anos. Os meus vestidos decotados já não me cabiam mais, por isso deixei de os comprar. Mas eu encontrei a salvação neste creme maravilhoso! O resultado surpreendeu-me mesmo! Em 2 semanas as rugas e dobras no meu peito tornaram-se difíceis de ver e o meu peito ficou mais firme. Um mês depois, eu fui bombardeada com perguntas sobre o lugar onde tinha feito uma cirurgia tão fantástica – e ninguém ainda acredita em mim quando digo que foi só um creme comum que me ajudou! </p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder"><img alt="image description" height="130" src="./assets/mobile/img-08.png" width="130"/>
                        <strong class="name lt53"> Emília </strong> <span class="lt54">24 anos</span></div>
                    <div class="holder">
                        <p class="lt55"> Os problemas com a forma e a elasticidade do meu peito começaram a perseguir-me imediatamente depois de ter perdido 21 quilos. Foi um choque enorme para mim! O meu peito ficou flácido e perdeu a sua beleza! Eu comecei a comprar roupa interior corretiva para ocultar o problema. Eu fiquei com um humor horrível e ganhei uma depressão. Com 24 anos o meu peito parecia o de uma mulher de 30! Eu recebi o creme da minha mãe como presente de aniversário. No início eu estava até um pouco ofendida pelo presente, mas agora estou tão agradecida que não consigo encontrar palavras! No prazo de um mês eu fiquei com um peito novo e agora o meu peito parece um peito de uma modelo de publicidade de roupa interior. Nunca pensei que uma forma tão perfeita pudesse ser natural! </p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order"><span class="decoration-02"></span>
                <div class="price-block" endif="" if="" protected="">
                    <h3 class="lt56"> APENAS HOJE <span>PREÇO ESPECIAL</span></h3>
                    <span class="price new-price price_main"><span class="price_main_value"><span class="js-pp-new">49</span></span><span class="price_main_currency"><span class="js_curs">€</span></span>
</span>
                    <span class="old-price"><span class="lt57">PREÇO ANTIGO</span> <strike class="price_old"><span class="price_main_value"><span class="js-pp-old">98</span></span>
<span class="price_main_currency"><span class="js_curs">€</span></span></strike></span>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block2">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer2">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order_form order-form" id="order_form" method="POST">
                    <!--<input type="hidden" name="total_price" value="49.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAIsFQTwCuWJAAEAAQAClRQBAALwIgIGAQEABIGJT2wA">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="UpSize_PT_Pink">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="PT">
                    <input type="hidden" name="shipment_vat" value="0.23">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.23">
                    <!--   -->
                    <div class="s__of">
                        <h3 class="lt59"></h3>
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt60">País</label>
                                    </div>
                                    <select class="select-purple country_select" id="country_code_selector" name="country_code">
                                        <option value="PT">Portugal</option>
                                        </select>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt61">Nome Completo</label>
                                    </div>
                                    <div class="text">
                                        <input name="name" placeholder="Nome completo" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt62">Telefone</label>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" name="phone" placeholder="Telefone" type="text">
                                    </div>
                                </div>
                            </div>
                            <button class="btn-search ">Encomendar</button>
                            <div class="toform"></div>
                        </div>
                    </div>

                    <!--   -->

                </form>


            </div>
        </div>
    </div>
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }

        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="./assets/beauty_1.css" rel="stylesheet"/>

    <!---->
    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 8944 -->
        <script>var locale = "pt";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIsFQTwCuWJAAEAAQAClRQBAALwIgIGAQEABIGJT2wA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Rosa Correia';
            var phone_hint = '+351912629188';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("pt");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="./assets/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="./assets/favicon.ico" rel="shortcut icon"/>
        <script src="./assets/jquery.countdown.js"></script>
        <script>

            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
                $('body').click(function(){
                    $(".js_errorMessage2").remove();
                });
            });

            /* Таймер */
            $(document).ready(function () {
                if ($('#timer').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer').html()), 1);
                    $('#timer').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' + '<span class="margin minutes">{m10}{m1}</span>' + '<span class="seconds">{s10}{s1}</span>'
                    }).removeClass('hidden');
                }
                ;
                if ($('#timer2').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer2').html()), 1);
                    $('#timer2').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' + '<span class="margin minutes">{m10}{m1}</span>' + '<span class="seconds">{s10}{s1}</span>'
                    }).removeClass('hidden');
                }
                $('.for-event').on('touchend, click', function (e) {
                    e.preventDefault();
                    var el = $(this).attr('data-href');
                    $('html, body').animate({
                        scrollTop: $(el).offset().top
                    }, 900);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <div class="s__main" id="wrapper">
        <div id="header">
            <div class="wrap">
                <div class="logo">UpSize</div>
                <div class="holder">
                    <ul id="nav">
                        <li><a class="scrollto lt2 for-event" data-href="#home">Página principal </a></li>
                        <li><a class="scrollto lt3 for-event" data-href="#breast-cream">Upsize? </a></li>
                        <li><a class="scrollto lt4 for-event" data-href="#expert"> Prova especializada </a></li>
                        <li><a class="scrollto lt5 for-event" data-href="#comment-review"> Críticas</a></li>
                    </ul>
                    <a class="link-order lt6 scrollto for-event" data-href="#order"> Encomendar agora</a></div>
            </div>
        </div>
        <div class="w1" id="home">
            <div class="intro"><strong class="logo"><span class="log">  UpSize </span>
                    <span> Efeito “push-up” - de uma vez por todas! </span></strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/img-01.png" width="448"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/pack.png"/></div>
                <div class="block">
                    <div class="info-box"><img alt="image description" height="39" src="./assets/icon-01.png" width="48"/>
                        <p class="lt8"> Aumento natural do peito </p>
                        <img alt="image description" height="39" src="./assets/icon-02.png" width="48"/>
                        <p class="lt9"> Correção e elevação do peito</p>
                        <img alt="image description" height="39" src="./assets/icon-03.png" width="48"/>
                        <p class="lt10">  Apenas componentes naturais </p>
                        <img alt="image description" height="39" src="./assets/icon-04.png" width="48"/>
                        <p class="lt11">95% das mulheres confirmaram os resultados</p>
                    </div>
                </div>
                <div class="panel">
                    <span class="text lt12">APENAS <span> HOJE</span></span>
                    <span class="price new-price price_main"><span class="price_main_value"><span class="js-pp-new">49</span></span>
<span class="price_main_currency"><span class="js_curs">€</span></span>
</span>
                    <span class="discount">-50%</span>
                    <strike class="old-price price_old"><span class="price_main_value"><span class="js-pp-old">98</span></span><span class="price_main_currency"><span class="js_curs">€</span></span></strike>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-container">
                <h2 class="lt18"> Upsize - resultado volumizador! </h2>
                <div class="holder">
                    <ul class="list">
                        <li class="lt19">Volume 100% natural</li>
                        <li class="lt20"> Apropriado para mulheres, independentemente da sua idade</li>
                        <li class="lt22"> O processo de descaimento da pele torna-se 3 vezes mais lento</li>
                    </ul>
                    <ul class="list alt">
                        <li class="lt23"> Aumenta o seu peito em dois tamanhos de copa</li>
                        <li class="lt24"> Irá esquecer-se das estrias</li>
                        <li class="lt25">Torna os seus seios redondos e firmes, dá saliência às formas</li>
                    </ul>
                </div>
            </div>
            <div class="step-block" id="breast-cream">
                <div class="heading">
                    <h2 class="lt26">O efeito do sutiã invisível </h2>
                    <p class="lt27">O seu peito não precisará mais de apoio!  </p>
                </div>
                <ul class="step-list">
                    <li><strong class="lt28">Passo 1</strong>
                        <img alt="image description" height="144" src="./assets/img-03.png" width="144"/>
                        <p><span class="lt29">O creme é aplicado na pele limpa. Primeiro o seio direito deve ser massageado durante 5-10 minutos até que o creme seja totalmente absorvido. Repita várias vezes.</span>
                            <br/></p>
                    </li>
                    <li><strong class="lt31">Passo 2</strong>
                        <img alt="image description" height="144" src="./assets/img-04.png" width="144"/>
                        <p class="lt32">Em seguida, massageie o seio esquerdo. Depois disso, espere que o creme seja absorvido e repita.</p>
                    </li>
                    <li><strong class="lt33">Passo 3</strong>
                        <img alt="image description" height="144" src="./assets/img-05.png" width="144"/>
                        <p class="lt34">No final, massageie ambos os seios com movimentos circulares ao longo do contorno, de baixo para cima, e acabe onde começou.</p>
                    </li>
                    <li><strong class="lt35">Passo 4</strong>
                        <img alt="image description" height="144" src="./assets/img-06.png" width="144"/>
                        <p class="lt36" id="pro">Use duas vezes por dia: de manhã e à noite.</p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="expert">
                <h2 class="lt37"> Opinião de um especialista sobre o Upsize: </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p class="lt38">Muitas mulheres são hoje em dia confrontadas com a necessidade de modelar de forma estética o peito. Praticamente todas as mulheres querem ter um peito cheio e firme. E não é de estranhar, já que o decote sempre foi a zona que chama mais atenção para os homens.</p>
                        </div>
                        <p class="lt39">Se usar o creme diariamente, o volume do seu peito irá aumentar em 3-4 semanas, a forma irá ficar mais redonda e firme, a pele ficará mais elástica e aveludada.</p>
                        <div class="text-box">
                            <p class="lt40">O creme passou por ensaios clínicos realizados por especialistas da Organização Mundial de Saúde em 14 países do mundo. Milhares de mulheres certificaram-se de que o creme Upsize é eficaz. </p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/img-09.jpg"/></div>
                        <div class="box">
                            <h3 class="lt41">Deoxi-miroestrol</h3>
                            <p class="lt42"> tem um efeito de rejuvenescimento pronunciado, promove o crescimento dos seios; </p>
                        </div>
                        <div class="box">
                            <h3 class="lt43"> Extrato de Raiz  <br/>de "Pueraria Mirifica"</h3>
                            <p class="lt44"> nutre eficientemente e protege a pele; </p>
                        </div>
                        <div class="box">
                            <h3 class="lt45">Óleo Essencial de Rosa</h3>
                            <p class="lt46">aumenta a resistência e elasticidade da pele, remove as estrias e impede o seu surgimento.</p>
                        </div>
                    </div>
                </div>
                <div class="description" id="opinion">
                    <div class="text"><strong class="lt47"> Adelaide Silva </strong> <span class="lt48"> Professora Doutorada em Ciências Biológicas </span>
                    </div>
                    <img alt="image description" height="79" src="./assets/img-11.png" width="133"/></div>
            </div>
            <div class="reviews-block" id="comment-review">
                <h2 class="lt49">Comentários sobre o Upsize</h2>
                <div class="box">
                    <div class="img-holder"><img alt="image description" height="130" src="./assets/img-07.png" width="130"/>
                        <strong class="name lt50"> Sofia Maria, </strong> <span class="lt51">37 anos</span></div>
                    <div class="holder">
                        <p class="lt52"> Ultimamente comecei a notar que o meu peito estava a mudar. A coisa que me preocupava é que a pele se tornasse flácida e começasse a perder a sua elasticidade anterior com o passar dos anos. Os meus vestidos decotados já não me cabiam mais, por isso deixei de os comprar. Mas eu encontrei a salvação neste creme maravilhoso! O resultado surpreendeu-me mesmo! Em 2 semanas as rugas e dobras no meu peito tornaram-se difíceis de ver e o meu peito ficou mais firme. Um mês depois, eu fui bombardeada com perguntas sobre o lugar onde tinha feito uma cirurgia tão fantástica – e ninguém ainda acredita em mim quando digo que foi só um creme comum que me ajudou! </p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder"><img alt="image description" height="130" src="./assets/img-08.png" width="130"/>
                        <strong class="name lt53"> Emília </strong> <span class="lt54">24 anos</span></div>
                    <div class="holder">
                        <p class="lt55"> Os problemas com a forma e a elasticidade do meu peito começaram a perseguir-me imediatamente depois de ter perdido 21 quilos. Foi um choque enorme para mim! O meu peito ficou flácido e perdeu a sua beleza! Eu comecei a comprar roupa interior corretiva para ocultar o problema. Eu fiquei com um humor horrível e ganhei uma depressão. Com 24 anos o meu peito parecia o de uma mulher de 30! Eu recebi o creme da minha mãe como presente de aniversário. No início eu estava até um pouco ofendida pelo presente, mas agora estou tão agradecida que não consigo encontrar palavras! No prazo de um mês eu fiquei com um peito novo e agora o meu peito parece um peito de uma modelo de publicidade de roupa interior. Nunca pensei que uma forma tão perfeita pudesse ser natural! </p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order"><span class="decoration-02"></span>
                <div class="price-block" endif="" if="" protected="">
                    <h3 class="lt56"> APENAS HOJE <span>PREÇO ESPECIAL</span></h3>
                    <span class="price new-price price_main"><span class="price_main_value"><span class="js-pp-new">49</span></span><span class="price_main_currency"><span class="js_curs">€</span></span>
</span>
                    <span class="old-price"><span class="lt57">PREÇO ANTIGO</span> <strike class="price_old"><span class="price_main_value"><span class="js-pp-old">98</span></span>
<span class="price_main_currency"><span class="js_curs">€</span></span></strike></span>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block2">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer2">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order_form order-form" id="order_form" method="POST">

                    <div class="s__of">
                        <h3 class="lt59"></h3>
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt60">País</label>
                                    </div>
                                    <select class="select-purple country_select" id="country_code_selector" name="country_code">
                                        <option value="PT">Portugal</option>
                                        <option value="PL" selected="selected">Poland</option></select>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt61">Nome Completo</label>
                                    </div>
                                    <div class="text">
                                        <input name="name" placeholder="Nome completo" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt62">Telefone</label>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" name="phone" placeholder="Telefone" type="text">
                                    </div>
                                </div>
                            </div>
                            <button class="btn-search ">Encomendar</button>
                            <div class="toform"></div>
                        </div>
                    </div>
                    <!--    -->
                </form>
            </div>
        </div>
    </div>
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }

        }
    </style>

    <!---->
    </body>
    </html>
<?php } ?>