<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7055 -->
        <script>var locale = "cz";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIOEwTsCeCJAAEAAQACcxIBAAKPGwIGAQEABEaJJ84A";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":1300,"old_price":2600,"shipment_price":0},"3":{"price":2600,"old_price":5200,"shipment_price":0},"5":{"price":3900,"old_price":7800,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Ivana Michalkova';
            var phone_hint = '+420774115732';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cz");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href=./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
        <link href="./assets/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css"/>
        <script src="./assets/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="./assets/main.js" type="text/javascript"></script>
        <!-- <script src="./assets/secondPage.js" type="text/javascript"></script> -->
        <link href="./assets/mobile/index.css" media="all" rel="stylesheet" type="text/css"/>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
                $('body').click(function(){
                    $(".js_errorMessage2").remove();
                });
            });
        </script>
    </head>
    <body>
    <div class="wrapper hidejs">
        <div class="w0">
            <div class="w1">
                <div class="header">
                    <h1>UpSize</h1>
                    <div class="description">
                        <p>Přírodní zvětšení prsou</p>
                        <p>Úprava a pozvednutí prsou</p>
                        <p>Pouze přírodní látky</p>
                        <p>95% žen potvrdilo výsledky</p>
                    </div>
                    <div class="btn-wrap toform">
                        <a class="btn-order" href="#order_form">Objednat</a>
                    </div>
                    <div class="b-text">
                        <div class="title">Zvětšete si poprsí</div>
                        <span>jednou provždy!</span>
                    </div>
                </div>
                <!-- header -->
                <div class="page-info">
                    <div class="dscription">100% přírodní zvětšení prsou</div>
                    <div class="dscription">Vhodné pro všechny ženy bez ohledu na věk</div>
                    <div class="dscription">Povolování kůže na prsou se trojnásobně zpomalí</div>
                    <div class="dscription">Zvětšení prsou až o dvě čísla</div>
                    <div class="dscription">Zapomenete na strie</div>
                    <div class="dscription">Budete mít kulatá a pevná prsa s krásným tvarem</div>
                </div>
                <!-- page info -->
                <div class="how-to-use">
                    <div class="content">
                        <div class="box">
                            <h3>Krok 1</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="./assets/mobile/show-1.png"/>
                                </div>
                                <div class="description">Krém nanášejte na čistou pokožku. Nejprve s ním 5-10 minut masírujte pravé prso, dokud se krém nevstřebá. Zopakujte několikrát.</div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Krok 2</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="./assets/mobile/show-2.png"/>
                                </div>
                                <div class="description">Potom promasírujte druhé prso a počkejte, dokud krém neuschne.</div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Krok 3</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="./assets/mobile/show-3.png"/>
                                </div>
                                <div class="description">Na konec promasírujte obě prsa krouživými pohyby odspodu směrem nahoru a skončete na místě, kde jste začala.
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Krok 4</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="./assets/mobile/show-4.png"/>
                                </div>
                                <div class="description">Používejte krém dvakrát denně - jednou ráno a jednou večer.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- how-to-use -->
                    <div class="reviews">
                        <h2>Recenze krému Upsize</h2>
                        <div class="content">
                            <div class="box-reviews">
                                <div class="wrap-title">
                                    <div class="title"><span class="name">Sofie,</span>37 let</div>
                                </div>
                                <div class="text">
                                    <div class="wrapimg"><img alt="" src="./assets/mobile/img-07.png"/>
                                    </div>
                                    V poslední době jsem si všimla, že se moje prsa mění. S postupem času kůže povoluje a ztrácí svoji elasticitu. Šaty s velkým výstřihem už mi nepadnou, takže jsem si je přestala kupovat. Naštěstí jsem ale našla záchranu v podobě skvělého krému! Výsledky mě skutečně překvapily. Během dvou týdnů mi vrásky na prsou skoro zmizely a poprsí se mi zpevnilo. O měsíc později se mě všichni začali ptát, kde jsem si nechala udělat tak povedenou operaci - nikdo mi stále nevěří, že mi pomohl jen obyčejný krém!
                                </div>
                            </div>
                            <div class="box-reviews">
                                <div class="wrap-title">
                                    <div class="title"><span class="name">Emma,</span> 24 let</div>
                                </div>
                                <div class="text">
                                    <div class="wrapimg"><img alt="" src="./assets/mobile/img-08.png"/>
                                    </div>
                                    Emma, 24 let: Problém s tvarem a pevností prsou jsem začala mít, když jsem zhubla 21 kilo. Byl to pro mě strašný šok! Moje prsa byla najednou povislá a ztratila svoji krásu. Začala jsem si kupovat speciální spodní prádlo, abych tento problém vyřešila. Měla jsem příšernou náladu, měla jsem deprese. Ve věku 24 let moje prsa vypadala, kdyby mi bylo více než 30! Krém jsem dostala od svojí mámy k narozeninám. Ze začátku jsem byla trochu uražená, ale nyní jsem vděčná a nemohu ani popsat, jak moc mi pomohl. Během jednoho měsíce jsem získala nová prsa a vypadám jako modelka z reklam na spodní prádlo! Nikdy jsem si nemyslela, že by tak krásná prsa mohla být přirozená.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- reviews -->
                    <div class="sellblock">
                        <h2>- POUZE DNES -</h2>
                        <p>SPECIÁLNÍ CENA:
                            <br/><span class="old_p">2600 Kč</span>
                            <br/><span class="new_p">1300 Kč</span>
                        </p>
                    </div>
                    <!-- sellblock -->
                    <div class="order">
                        <form action="./process.php" id="order_form" method="post" name="order_all"><input type="hidden" name="total_price" value="1300.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAIOEwS8a9iZAAEAAQACcxIBAAKPGwIGAQEABF2RPMYA">
                            <input type="hidden" name="goods_id" value="20">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="UpSize_CZ_Mauve">
                            <input type="hidden" name="price" value="1300">
                            <input type="hidden" name="old_price" value="2600">
                            <input type="hidden" name="total_price_wo_shipping" value="1300.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 1300, 'old_price': 2600, 'shipment_price': 0}, u'3': {'price': 2600, 'old_price': 5200, 'shipment_price': 0}, u'5': {'price': 3900, 'old_price': 7800, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Kč">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="PL">
                            <input type="hidden" name="shipment_vat" value="0.21">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-pl">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.21">
                            <select class="country" id="country_code_selector" name="country_code">
                                <option value="CZ">Česká republika</option>
                               </select>
                            <input name="name" placeholder="Jméno a příjmení" type="text">
                            <input class="only_number" name="phone" placeholder="Telefonní číslo" type="text">
                            <!-- <span class="js_new_price_curs" id="itog">Dodací adresa: 0 Kč </span>
                                <span class="js_full_price_curs" id="itogo">Celková cena: 1300 Kč </span> -->
                            <!--   -->
                            <div class="s__of">
                                <button class="btn-order ">Objednat</button>
                                <div class="toform"></div>
                            </div>
                            <!--    -->
                            <input type="hidden" name="time_zone" value="1"></form>
                    </div>
                    <!-- order -->
                    <!-- footer -->
                </div>
                <!-- w1 -->
            </div>
            <!-- w0 -->
        </div>
        <!-- wrapper -->
    </div>


    <link href="./assets/mobile/styleSecond.css" rel="stylesheet" type="text/css"/>
    <script src="./assets/mobile/jquery.bxslider.min.js"></script>
    <div class="hidden-window">
        <div class="new-header_goji"><img alt="logo" src="./assets/mobile/header_logo.png" width="300"/></div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Platba
                                <b>po dodání</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr" src="./assets/mobile/Zarr.png"/><img alt="alt1" src="./assets/mobile/dtc1.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1" src="./assets/mobile/dtc2.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft" src="./assets/mobile/Zarr.png"/><img alt="alt1" src="./assets/mobile/dtc3.png"/></div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><b>Objednávka</b></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><b>Poštovné</b></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Dodání <br/><span>a platba</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">Výhodná nabídka</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 balení</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 balení</option>
                                <option data-slide-index="2" value="5">3+2 balení</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs"><img alt="zt1" src="./assets/mobile/1.png"/> </div>
                                            <div class="zHeader"><b>1</b> balení</div>
                                        </div>
                                        <div class="pack_descr">Pevná a pozvednutá prsa už po první aplikaci!</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">Cena</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new">  1300 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old">  2600 </text>
                                                </div> Kč</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile"></div>
                                                    <img alt="zt1" src="./assets/mobile/2+1.png"/>
                                                </div>
                                                <div class="zHeader"><b>2</b> balení
                                                    <br/> <span class="dib zplus">dárek</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">Tříměsíční kůra! O jednu velikost větší prsa! Garantujeme úžasně objemná prsa!</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">Cena</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new">  2600 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old">  5200 </text>
                                                </div>
                                                Kč
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <div class="giftmobile"></div>
                                                <img alt="zt1" src="./assets/mobile/3+2.png"/>
                                            </div>
                                            <div class="zHeader"><b>3</b> balení
                                                <br/> <span class="dib zplus sec">dárky</span>
                                            </div>
                                        </div>
                                        <div class="pack_descr">Půlroční kůra! Efekt jako po plastické operaci! Zvětšení až o 2 velikosti! Bez jizev a bez bolesti!</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">Cena</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new">  3900 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 7800 </text>
                                                </div>
                                                Kč
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager">
                            <a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a>
                            <a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a>
                            <a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                        </div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIAIOEwAAAAAAAAAAAAQ5hIJBAA"></iframe>
                            <!--<input type="hidden" name="total_price" value="1300.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAIOEwTsCeCJAAEAAQACcxIBAAKPGwIGAQEABEaJJ84A">
                            <input type="hidden" name="goods_id" value="20">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="UpSize_CZ_Mauve">
                            <input type="hidden" name="price" value="1300">
                            <input type="hidden" name="old_price" value="2600">
                            <input type="hidden" name="total_price_wo_shipping" value="1300.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 1300, 'old_price': 2600, 'shipment_price': 0}, u'3': {'price': 2600, 'old_price': 5200, 'shipment_price': 0}, u'5': {'price': 3900, 'old_price': 7800, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Kč">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="CZ">
                            <input type="hidden" name="shipment_vat" value="0.21">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.21">

                            <div class="formHeader">Zadejte údaje a proveďte objednávku</div>
                            <select class="select inp" id="country_code_selector">
                                <option value="CZ">Česká republika</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 balení</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 balení</option>
                                <option data-slide-index="2" value="5">3+2 balení</option>
                            </select>
                            <input class="inp j-inp" data-count-length="2+" name="name" placeholder="Jméno a příjmení" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Telefonní číslo" type="text" value=""/>
                            <!--
                            <input class="inp" name="city" placeholder="Stadt eintragen" style="display: none" type="text"/>
                            <input class="inp" name="house" placeholder="Haus" style="display: none" type="text"/>
                            <input class="inp" name="address" placeholder="Zadajte adresu" type="text"/>
                            <p style="text-align:center;font-size: 18px;color: #e70116;padding: 5px;">+Poštovné: <span class="js_delivery_price">0 Kč</span></p> -->
                            <div class="text-center totalpriceForm">Celkom: <span class="js_total_price js_full_price">1300 </span> Kč</div>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Garantujeme:</div>
                            <ul>
                                <li><b>100%</b> kvalitu</li>
                                <li><b>Kontrolu</b> produktu při převzetí</li>
                                <li><b>Bezpečnost</b> vašich údajů</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <style>
        #bx-pager{text-align:center;}
    </style>

    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7055 -->
        <script>var locale = "cz";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIOEwTsCeCJAAEAAQACcxIBAAKPGwIGAQEABEaJJ84A";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":1300,"old_price":2600,"shipment_price":0},"3":{"price":2600,"old_price":5200,"shipment_price":0},"5":{"price":3900,"old_price":7800,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Ivana Michalkova';
            var phone_hint = '+420774115732';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cz");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href=./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
         <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css"/>
        <script src="./assets/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="./assets/main.js" type="text/javascript"></script>
        <!-- <script src="./assets/secondPage.js" type="text/javascript"></script> -->
        <link href="./assets/style.css" media="all" rel="stylesheet" type="text/css"/>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
                $('body').click(function(){
                    $(".js_errorMessage2").remove();
                });
            });
        </script>
    </head>
    <body>
    <div class="s__main" id="wrapper">
        <div id="header">
            <div class="wrap">
                <a href=""><strong class="logo"> UpSize </strong></a>
                <div class="holder">
                    <ul id="nav">
                        <li><a block="wrapper" href="#"> Hlavní strana </a></li>
                        <li><a block="bust" href="#"> Upsize? </a></li>
                        <li><a block="doctor" href="#"> Důkazy odborníků </a></li>
                        <li><a block="comments" href="#"> Recenze </a></li>
                    </ul>
                    <button class="link-order pre_toform">Objednat nyní</button>
                </div>
            </div>
        </div>
        <div class="w1">
            <div class="intro">
                <strong class="logo"><span class="log"> UpSize</span> <span> Zvětšete si poprsí - jednou provždy! </span></strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/img-01.png" width="448"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/pack.png" style="width: 300px;"/></div>
                <div class="block">
                    <div class="info-box">
                        <img alt="image description" height="39" src="./assets/icon-01.png" width="48"/>
                        <p>Přírodní zvětšení prsou</p>
                        <img alt="image description" height="39" src="./assets/icon-02.png" width="48"/>
                        <p>Úprava a pozvednutí prsou</p>
                        <img alt="image description" height="39" src="./assets/icon-03.png" width="48"/>
                        <p> Pouze přírodní látky </p>
                        <img alt="image description" height="39" src="./assets/icon-04.png" width="48"/>
                        <p> 95% žen potvrdilo výsledky</p>
                    </div>
                </div>
                <div class="panel" id="bust">
                    <span class="text"> POUZE DNES </span>
                    <span class="price js_new_price_curs"> 2600 Kč</span>
                    <span class="discount">-50%</span>
                    <strike class="product_old_price js_old_price_curs">1300 Kč </strike>
                    <button class="link-order btn-search pre_toform">Objednat</button>
                </div>
            </div>
            <div class="info-container">
                <h2> Upsize - ohromující výsledky! </h2>
                <div class="holder">
                    <ul class="list">
                        <li>100% přírodní zvětšení prsou</li>
                        <li> Vhodné pro všechny ženy bez ohledu na věk </li>
                        <li> Povolování kůže na prsou se trojnásobně zpomalí</li>
                    </ul>
                    <ul class="list alt">
                        <li>Zvětšení prsou až o dvě čísla</li>
                        <li> Zapomenete na strie </li>
                        <li> Budete mít kulatá a pevná prsa s krásným tvarem </li>
                    </ul>
                </div>
            </div>
            <div class="step-block">
                <div class="heading">
                    <h2> Efekt neviditelné podprsenky </h2>
                    <p>Vaše poprsí již podporu nebude potřebovat!</p>
                </div>
                <ul class="step-list">
                    <li>
                        <strong> Krok 1 </strong>
                        <img alt="image description" height="144" src="./assets/img-03.png" width="144"/>
                        <p>Krém nanášejte na čistou pokožku. Nejprve s ním 5-10 minut masírujte pravé prso, dokud se krém nevstřebá. Zopakujte několikrát.
                        </p>
                    </li>
                    <li>
                        <strong> Krok 2 </strong>
                        <img alt="image description" height="144" src="./assets/img-04.png" width="144"/>
                        <p> Potom promasírujte druhé prso a počkejte, dokud krém neuschne. </p>
                    </li>
                    <li>
                        <strong> Krok 3 </strong>
                        <img alt="image description" height="144" src="./assets/img-05.png" width="144"/>
                        <p> Na konec promasírujte obě prsa krouživými pohyby odspodu směrem nahoru a skončete na místě, kde jste začala.
                        </p>
                    </li>
                    <li>
                        <strong> Krok 4 </strong>
                        <img alt="image description" height="144" src="./assets/img-06.png" width="144"/>
                        <p> Používejte krém dvakrát denně - jednou ráno a jednou večer. </p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="doctor">
                <h2> Názor odborníka na krém Upsize: </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p>
                                Spousta žen se dnes uchyluje k plastickým operacím prsou. Prakticky každá žena chce mít krásná plná prsa. Není se čemu divit, protože dekolt byl vždy oblastí, které muži věnovali zvýšenou pozornost.
                            </p>
                        </div>
                        <p>
                            Když budete tento krém používat každý den, poprsí se vám zvětší za 3-4 týdny, bude plnější a kulatější a pleť bude elastičtější a sametově hladká.
                        </p>
                        <div class="text-box">
                            <p>
                                Krém prošel klinickými testy v laboratoři Světové zdravotnické organizace ve 14 zemích světa. Tisíce žen se již ujistily, že krém Upsize skutečně funguje.
                            </p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/img-09.jpg"/></div>
                        <div class="box">
                            <h3> Deoxymiroestrol </h3>
                            <p> má výrazný omlazující účinek a podporuje růst prsou </p>
                        </div>
                        <div class="box">
                            <h3>Extrakt z kořene <br/>Pueraria Mirifica </h3>
                            <p> efektivně vyživuje a chrání pleť </p>
                        </div>
                        <div class="box">
                            <h3> Růžový esenciální olej  </h3>
                            <p> zvyšuje odolnost a elasticitu pleti, odstraňuje strie a předchází jejich vzniku </p>
                        </div>
                    </div>
                </div>
                <div class="description">
                    <div class="text">
                        <strong> Adéla Dimitriová </strong>
                        <span> lékařka a profesorka biologie </span>
                    </div>
                    <img alt="image description" height="79" src="./assets/img-11.png" width="133"/>
                </div>
            </div>
            <div class="reviews-block" id="comments">
                <h2> Recenze krému Upsize </h2>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" class="rud" height="130" src="./assets/img-07.png" width="130"/>
                        <strong class="name"> Sofie </strong>
                        <span> 37 let </span>
                    </div>
                    <div class="holder">
                        <p>
                            V poslední době jsem si všimla, že se moje prsa mění. S postupem času kůže povoluje a ztrácí svoji elasticitu. Šaty s velkým výstřihem už mi nepadnou, takže jsem si je přestala kupovat. Naštěstí jsem ale našla záchranu v podobě skvělého krému! Výsledky mě skutečně překvapily. Během dvou týdnů mi vrásky na prsou skoro zmizely a poprsí se mi zpevnilo. O měsíc později se mě všichni začali ptát, kde jsem si nechala udělat tak povedenou operaci - nikdo mi stále nevěří, že mi pomohl jen obyčejný krém!
                        </p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" class="rud" height="130" src="./assets/img-08.png" width="130"/>
                        <strong class="name"> Emma </strong>
                        <span> 24 let  </span>
                    </div>
                    <div class="holder">
                        <p>
                            Emma, 24 let: Problém s tvarem a pevností prsou jsem začala mít, když jsem zhubla 21 kilo. Byl to pro mě strašný šok! Moje prsa byla najednou povislá a ztratila svoji krásu. Začala jsem si kupovat speciální spodní prádlo, abych tento problém vyřešila. Měla jsem příšernou náladu, měla jsem deprese. Ve věku 24 let moje prsa vypadala, kdyby mi bylo více než 30! Krém jsem dostala od svojí mámy k narozeninám. Ze začátku jsem byla trochu uražená, ale nyní jsem vděčná a nemohu ani popsat, jak moc mi pomohl. Během jednoho měsíce jsem získala nová prsa a vypadám jako modelka z reklam na spodní prádlo! Nikdy jsem si nemyslela, že by tak krásná prsa mohla být přirozená.
                        </p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order">
                <div class="price-block">
                    <h3> POUZE DNES  <span> SPECIÁLNÍ CENA </span></h3>
                    <span class="price js_new_price_curs"> 1300 Kč  </span>
                    <span class="old-price"> Stará cena
        <strike>
<span class="product_old_price js_old_price_curs" id="old-price"> 2600 Kč </span>
</strike>
</span>
                    <div class="timer-wrap">
                        <p> Zbývá do konce akce: </p>
                        <div class="timer">
                        </div>
                    </div>
                </div>
                <div class="img_block">
                    <img alt="" src="./assets/pack_2.png"/>
                </div>
                <div class="__right_block">
                    <form action="./process.php" class="order-form" id="order_form" method="POST">
                        <!--<input type="hidden" name="total_price" value="1300.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAIOEwTsCeCJAAEAAQACcxIBAAKPGwIGAQEABEaJJ84A">
                        <input type="hidden" name="goods_id" value="20">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="UpSize_CZ_Mauve">
                        <input type="hidden" name="price" value="1300">
                        <input type="hidden" name="old_price" value="2600">
                        <input type="hidden" name="total_price_wo_shipping" value="1300.0">
                        <input type="hidden" name="package_prices" value="{u'1': {'price': 1300, 'old_price': 2600, 'shipment_price': 0}, u'3': {'price': 2600, 'old_price': 5200, 'shipment_price': 0}, u'5': {'price': 3900, 'old_price': 7800, 'shipment_price': 0}}">
                        <input type="hidden" name="currency" value="Kč">
                        <input type="hidden" name="package_id" value="1">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="CZ">
                        <input type="hidden" name="shipment_vat" value="0.21">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.21">

                        <h3>Objednat</h3>
                        <!--   -->
                        <div class="s__of">
                            <div class="box">
                                <div class="frame">
                                    <div class="holder">
                                        <label>Země</label>
                                    </div>
                                    <div class="text">
                                        <select class="country" id="country_code_selector" name="country_code">
                                            <option value="CZ">Česká republika</option>
                                           </select>
                                    </div>
                                    <div class="holder">
                                        <label>Jméno a příjmení</label>
                                    </div>
                                    <div class="text">
                                        <input name="name" type="text">
                                    </div>
                                    <div class="holder">
                                        <label>Telefonní číslo</label>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" name="phone" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <button class="btn-search js_pre_toform">Objednat</button>
                                <div class="toform"></div>
                            </div>
                        </div>
                        <!--   -->
                    </form>
                </div>
            </div>
            <div id="footer">
            </div>
        </div>
    </div>

    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href=./assets/beauty_1.css" rel="stylesheet"/>

    <!---->
    </body>
    </html>
<?php } ?>