/**
 * Created by Serjok on 21.03.2016.
 */
$(document).ready(function () {


    $('#header a, a.link-order, a.other-link-order').click(function (e) {
        e.preventDefault();
        $('html,body').animate({scrollTop: $('#' + $(this).attr("block")).offset().top}, 400);
    });


    var _currentDate = new Date();
    var count = 15; // 8 hours
    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + count, 1);

    $('.timer').countdown({
        until: _toDate,
        format: 'HMS',
        compact: true,
        layout: /* Hours */
        '<span class="countHours"><span class="position">{h10}</span><span class="position">{h1}</span></span>' +

            /* Monutes */
        '<span class="countMinutes"><span class="position">{m10}</span><span class="position">{m1}</span></span>' +

            /* Seconds */
        '<span class="countSeconds"><span class="position">{s10}</span><span class="position">{s1}</span></span>' +

        '<div class="clear"></div>'
    }).removeClass('hidden');


});
