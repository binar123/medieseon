<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 11786 -->
        <script>var locale = "bg";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAI4FwT3Ud-JAAEAAQAC0xYBAAIKLgIGAQEABIvxaSYA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":80,"old_price":160,"shipment_price":0},"3":{"price":160,"old_price":320,"shipment_price":0},"5":{"price":240,"old_price":480,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Миндов Стефан';
            var phone_hint = '+359897974512';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("bg");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
        <link href="./assets/mobile/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css"/>
        <script src="./assets/main.js" type="text/javascript"></script>
        <link href="./assets/mobile/style.css" media="all" rel="stylesheet" type="text/css"/>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
                $('body').click(function(){
                    $(".js_errorMessage2").remove();
                });
            });

        </script>
    </head>
    <body>
    <div class="hide-it s__main" id="wrapper">
        <div id="header">
            <div class="wrap">
                <strong class="logo">
                    <span>UpSize</span>
                </strong>
                <div class="holder">
                    <ul id="nav">
                        <li><a block="wrapper" href="#"> Начало </a></li>
                        <li><a block="bust" href="#"> UpSize </a></li>
                        <li><a block="doctor" href="#"> Експертно мнение </a></li>
                        <li><a block="comments" href="#"> Мнения </a></li>
                    </ul>
                    <button class="link-order pre_toform">Поръчай сега</button>
                </div>
            </div>
        </div>
        <div class="w1">
            <div class="intro">
                <strong class="logo">
                    <span>UpSize</span> <span> Повдигане – веднъж и завинаги! </span>
                </strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/mobile/img-01.png" width="448"/></div>
                <div class="decoration-01">
                    <img alt="image description" src="./assets/mobile/pack.png"/>
                </div>
                <div class="block">
                    <div class="info-box">
                        <img alt="image description" height="39" src="./assets/mobile/icon-01.png" width="48"/>
                        <p> Естествено увеличаване на бюста </p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-02.png" width="48"/>
                        <p> Корекции на гърдите и повдигане на бюста </p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-03.png" width="48"/>
                        <p> Само натурални съставки  </p>
                        <img alt="image description" height="39" src="./assets/mobile/icon-04.png" width="48"/>
                        <p> 95% от жените потвърждават резултатите </p>
                    </div>
                </div>
                <div class="panel">
                    <span class="text lt12">САМО  <span> ДНЕС</span></span>
                    <span class="price new-price price_main"><span class="price_main_value">
    160
</span>
<span class="price_main_currency">Лева</span></span>
                    <span class="discount">-50%</span>
                    <strike class="old-price price_old"><span class="price_main_value"><span class="">80</span></span><span class="price_main_currency"><span class="js_curs">Лева</span></span></strike>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block">
                            <div class="landing__countdown">
                                <div class="clearfix hasCountdown" id="timer"><span class="margin hours">16</span><span class="margin minutes">36</span><span class="seconds">23</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-container">
                <h2> UpSize - уголемяващ резултат! </h2>
                <div class="holder">
                    <ul class="list">
                        <li> 100% естествен обем </li>
                        <li> Подходящ за жени, независимо от възрастта </li>
                        <li> Процесът на загуба на еластичност се забавя три пъти
                        </li>
                    </ul>
                    <ul class="list alt">
                        <li> Гърдите се увеличават с два размера на чашката </li>
                        <li> Ще забравите за грозните стрии </li>
                        <li> Направете гърдите си кръгли и твърди, подчертайте формата им </li>
                    </ul>
                </div>
            </div>
            <div class="step-block">
                <div class="heading">
                    <h2> Ефектът невидим сутиен </h2>
                    <p> гърдите няма да се нуждаят повече от сутиен! </p>
                </div>
                <ul class="step-list">
                    <li>
                        <strong> Стъпка 1: </strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-03.png" width="144"/>
                        <p>Кремът се нанася на почистена кожа. Първо се масажира дясната гърда в продължение на 5-10 минути, докато кремът попие напълно. Повторете няколко пъти.</p>
                    </li>
                    <li>
                        <strong> Стъпка 2: </strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-04.png" width="144"/>
                        <p> След това се масажира другата гърда. После изчакайте кремът да попие.  </p>
                    </li>
                    <li>
                        <strong> Стъпка 3: </strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-05.png" width="144"/>
                        <p> Накрая масажирайте двете гърди с кръгообразни движения, следващи формата им от долната част на гърдите нагоре и завършете там,  откъдето сте започнали.
                        </p>
                    </li>
                    <li>
                        <strong> Стъпка 4: </strong>
                        <img alt="image description" height="144" src="./assets/mobile/img-06.png" width="144"/>
                        <p> Използвайте два пъти дневно: сутрин и вечер. </p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="doctor">
                <h2> Експертно становище относно UpSize: </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/mobile/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p>Много жени днес са изправени пред необходимостта от естетично моделиране на гърдите. Практически всяка жена иска да има стегнати и оформени гърди. Не е изненадващо, тъй като деколтето винаги е било зона на повишено внимание за мъжете. </p>
                        </div>
                        <p>
                            Ако използвате крема ежедневно, обемът на гърдите ви ще се увеличи след 3-4 седмици, формата им ще е по-кръгла и стегната, а кожата ще бъде по-еластична и мека.

                        </p>
                        <div class="text-box">
                            <p>
                                Кремът е преминал успешно клинични изследвания, проведени от експерти от Световната здравна организация в 14 страни на света. Хиляди жени са се убедили в ефикасността на крема UpSize.

                            </p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/mobile/img-09.jpg"/></div>
                        <div class="box">
                            <h3> Деоксимироестрол </h3>
                            <p> има ясно изразен подмладяващ ефект и насърчава растежа на гърдите; </p>
                        </div>
                        <div class="box">
                            <h3> Екстрактът от корена  <br/>  пуерариа мирифика </h3>
                            <p> ефективно подхранва и защитава кожата; </p>
                        </div>
                        <div class="box">
                            <h3> Етерично розово масло </h3>
                            <p> подобрява гъвкавостта и еластичността на кожата, премахва стриите и предотвратява появата на нови. </p>
                        </div>
                    </div>
                </div>
                <div class="description">
                    <div class="text">
                        <strong> Вили Димитрова </strong>
                        <span> Доктор на биологичните науки </span>
                    </div>
                    <img alt="image description" height="79" src="./assets/mobile/img-11.png" width="133"/>
                </div>
            </div>
            <div class="reviews-block" id="comments">
                <h2> Мнения за UpSize </h2>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/mobile/img-07.png" width="130"/>
                        <strong class="name"> София, </strong>
                        <span> 37 г. </span>
                    </div>
                    <div class="holder">
                        <p>
                            Напоследък започнах да забелязвам, че гърдите ми се променят. Работата е там, че с годините кожата става отпусната и губи своята еластичност. Моите рокли  с дълбоко деколте не ми стават вече, така че просто спрях да ги нося. Но намерих спасение с този прекрасен крем! Резултатите наистина ме изненадаха! За 2 седмици бръчките и гънките по кожата на гърдите ми ставаха едва видими и гърдите ми станаха по-твърди. Месец по-късно вече ме заливаха с въпроси къде съм си направила такава жестока операция – и никой не ми вярва, че само съм се мазала с крем, който ми помогна!

                        </p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/mobile/img-08.png" width="130"/>
                        <strong class="name"> Емилия </strong>
                        <span> 24 г. </span>
                    </div>
                    <div class="holder">
                        <p>
                            Проблемите с форма на гърдите и това, че не са стегнати апочнаха  да ме преследват, веднага щом отслабвах с 21 кг. Беше абсолютен шок за мен! Гърдите ми увиснаха и губиха своята красота! Започнах да купувам коригиращо бельо за да скрия проблема. Настроението ми беше ужасно, направо бях в депресия. На 24, а гърдите ми изглеждаха все едно съм на 30! Майка ми ми поадри крема за рождения ден. В началото бях дори малко обидена от подаръка, но сега съм толкова благодарна, че не мога да намеря думи! В рамките на един месец се сдобих с нови гърди, и изглеждаха като бюста от реклама за бельо! Никога не съм вярвала, че такава красива форма може да се получи по естествен начин!

                        </p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order">
                <span class="decoration-02"></span>
                <div class="price-block">
                    <h3> САМО ДНЕС  <span> СПЕЦИАЛНА ЦЕНА </span></h3>

                    <button class="mm_button_pr jsOpenWindow" type="submit">
                        Поръчай
                    </button>


                    <span class="price ">80 Лева</span>
                    <span class="old-price"> Стара цена:
              <strike>
<span class="product_old_price " id="old-price">160 Лева</span>
</strike>
</span>
                    <div class="timer-wrap">



                        <div class="timer-wrap">
                            <div class="timer top" id="clock_block2">
                                <div class="landing__countdown">
                                    <div class="clearfix hasCountdown" id="timer2"><span class="margin hours">16</span><span class="margin minutes">37</span><span class="seconds">22</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order-form" id="order_form" method="POST"><input type="hidden" name="total_price" value="80.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAI4FwQScNmZAAEAAQAC0xYBAAIKLgIGAQEABIZXI78A">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="UpSize_BG_Pink">
                    <input type="hidden" name="price" value="80">
                    <input type="hidden" name="old_price" value="160">
                    <input type="hidden" name="total_price_wo_shipping" value="80.0">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 240, 'old_price': 480, 'shipment_price': 0}}">
                    <input type="hidden" name="currency" value="Лева">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="PL">
                    <input type="hidden" name="shipment_vat" value="0.2">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="accept_languages" value="pl-pl">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.2">


                    <!--    -->
                    <fieldset class="s__of">
                        <h3>Поръчай UpSize</h3>
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                    <div class="holder">
                                        <label for="country_code_selector">Страна</label>
                                    </div>
                                    <div class="text">
                                        <select class="country" id="country_code_selector" name="country_code">
                                            <option value="BG">България</option>
                                           </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label>Име и фамилия</label>
                                    </div>
                                    <div class="text">
                                        <input name="name" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label>Телефон</label>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" name="phone" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <dl>
                                <dt>Доставка:</dt>
                                <dd><span class="" id="itog">0 Лева</span>
                                </dd>
                                <dt>Общо:</dt>
                                <dd><span class="js_full_price_curs" id="itogo">80 Лева</span>
                                </dd>
                            </dl>
                            <button class="btn-search js_pre_toform">Поръчай</button>
                            <div class="toform"></div>
                        </div>
                    </fieldset>
                    <!--   -->
                    <input type="hidden" name="time_zone" value="1"></form>
            </div>
            <div id="footer">
            </div>
        </div>
    </div>
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }

        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="./assets/beauty_1.css" rel="stylesheet"/>
    <div class="hidden-window v1_1_1_1">
        <div class="h-w-header">
            <div class="h-w-contant">
                <div class="h-w-header-text">
                    <span>ПОРЪЧАЙ <b>UPSIZE</b></span>
                </div>
            </div>
        </div>
        <div class="h-w-main">
            <div class="h-w-contant">
                <div class="h-w-left">
                    <div class="h-w-payment h-w-decor-block h-w-vis-mob">
                        <p>ПЛАЩАНЕ <span>ПРИ Доставка</span></p>
                        <ul>
                            <li><span>ПОРЪЧКАТА</span></li>
                            <li><span>Доставка</span></li>
                            <li><span>Доставка И ПЛАЩАНЕ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob h-w-ico h-w-select">
                        <select class="change-package-selector" name="count_select">
                            <option data-slide-index="0" value="1">1 опаковка</option>
                            <option data-slide-index="1" selected="selected" value="3">2+1 опаковки</option>
                            <option data-slide-index="2" value="5">3+2 опаковки</option>
                        </select>
                    </div>
                    <ul class="bx-bx">
                        <li data-value="1">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class=" h-w-item-prod"></div>
                                    </div>
                                    <div class="h-w-item-description">Стегнат и повдигнат бюст още след първото нанасяне!</div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковка</p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>160 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Цена:</span><span>80 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има <span>50% ОТСТЪПКА</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="active" data-value="3">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"></div>
                                        <span><font>+ подарък</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        Тримесечен курс! Плюс 1 размер! Гарантирано добър резултат!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковки <span>подарък</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>320 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Цена:</span><span>160 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има <span>50% ОТСТЪПКА</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-value="5">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"><span></span></div>
                                        <span><font>+ подаръка</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        Половин годишен курс! Резултати като след операция! Увеличете до 2 размера! Без белези и болка!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковки <span>подаръка</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>480 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Цена:</span><span>240 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има <span>50% ОТСТЪПКА</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="h-w-right">
                    <div class="h-w-payment h-w-decor-block h-w-hide-mob">
                        <p>ПЛАЩАНЕ <span>при Доставка</span></p>
                        <ul>
                            <li><span>ПОРЪЧКАТА</span></li>
                            <li><span>Доставка</span></li>
                            <li><span>Доставка И ПЛАЩАНЕ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-honesty h-w-ico h-w-hide-mob">ДОБРА СДЕЛКА</div>
                    <div class="h-w-form h-w-decor-block js_scrollForm">
                        <p><b>ВЪВЕДЕТЕ ДАННИ </b>ЗА ПОРЪЧКА</p>
                        <form action="" method="post">
                            <!--<input type="hidden" name="total_price" value="80.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAI4FwT3Ud-JAAEAAQAC0xYBAAIKLgIGAQEABIvxaSYA">
                            <input type="hidden" name="goods_id" value="20">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="UpSize_BG_Pink">
                            <input type="hidden" name="price" value="80">
                            <input type="hidden" name="old_price" value="160">
                            <input type="hidden" name="total_price_wo_shipping" value="80.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 240, 'old_price': 480, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Лева">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="BG">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.2">

                            <div class="h-w-select h-w-ico" style="display:none;">
                                <select id="country_code_selector" name="country_code">
                                    <option value="HU">Magyarország</option>
                                </select>
                            </div>
                            <div class="h-w-select h-w-ico">
                                <select class="change-package-selector" name="count_select">
                                    <option data-slide-index="0" value="1">1 опаковка</option>
                                    <option data-slide-index="1" selected="selected" value="3">2+1 опаковки</option>
                                    <option data-slide-index="2" value="5">3+2 опаковки</option>
                                </select>
                            </div>
                            <div style="display:none;">
                                <input name="name" type="text"/>
                                <input class="only_number" name="phone" type="text"/>
                            </div>
                            <input name="address" placeholder="Въведете адрес" type="text"/>
                            <!--  -->
                            <div class="h-w-text-total">Общо: <span><font class="js_full_price">160</font> Лева</span></div>

                        </form>
                    </div>
                    <div class="h-w-guarantee h-w-decor-block">
                        <p>НИЕ ГАРАНТИРАНЕ:</p>
                        <ul>
                            <li><b>100%</b> качество</li>
                            <li><b>Проверка</b> на продукта при получаване</li>
                            <li><b>Безопасност</b> на вашите данни</li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob"><div class="h-w-honesty h-w-ico">ДОБРА СДЕЛКА</div></div>
                </div>
            </div>
        </div>
    </div>
    <!---->

    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 11786 -->
        <script>var locale = "bg";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAI4FwT3Ud-JAAEAAQAC0xYBAAIKLgIGAQEABIvxaSYA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":80,"old_price":160,"shipment_price":0},"3":{"price":160,"old_price":320,"shipment_price":0},"5":{"price":240,"old_price":480,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Миндов Стефан';
            var phone_hint = '+359897974512';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("bg");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> UpSize </title>
        <link href="//st.acstnst.com/content/UpSize_BG_Pink/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css"/>
        <script src="./assets/main.js" type="text/javascript"></script>
        <link href="./assets/style.css" media="all" rel="stylesheet" type="text/css"/>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
                $('body').click(function(){
                    $(".js_errorMessage2").remove();
                });
            });
        </script>
    </head>
    <body>
    <div class="hide-it s__main" id="wrapper">
        <div id="header">
            <div class="wrap">
                <strong class="logo">
                    <span> UpSize </span>
                </strong>
                <div class="holder">
                    <ul id="nav">
                        <li class="top-menu"><a block="wrapper" href="#wrapper"> Начало </a></li>
                        <li class="top-menu"><a block="bust" href="#bust"> UpSize </a></li>
                        <li class="top-menu"><a block="doctor" href="#doctor"> Експертно мнение </a></li>
                        <li class="top-menu"><a block="comments" href="#comments"> Мнения </a></li>
                    </ul>
                    <button class="link-order pre_toform">Поръчай сега</button>
                </div>
            </div>
        </div>
        <div class="w1">
            <div class="intro">
                <strong class="logo">
                    <span>UpSize</span> <span> Повдигане – веднъж и завинаги! </span>
                </strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/img-01.png" width="448"/></div>
                <div class="decoration-01">
                    <img alt="image description" src="./assets/pack.png"/>
                </div>
                <div class="block">
                    <div class="info-box">
                        <img alt="image description" height="39" src="./assets/icon-01.png" width="48"/>
                        <p> Естествено увеличаване на бюста </p>
                        <img alt="image description" height="39" src="./assets/icon-02.png" width="48"/>
                        <p> Корекции на гърдите и повдигане на бюста </p>
                        <img alt="image description" height="39" src="./assets/icon-03.png" width="48"/>
                        <p> Само натурални съставки  </p>
                        <img alt="image description" height="39" src="./assets/icon-04.png" width="48"/>
                        <p> 95% от жените потвърждават резултатите </p>
                    </div>
                </div>
                <div class="panel">
                    <span class="text lt12">САМО  <span> ДНЕС</span></span>
                    <span class="price new-price price_main"><span class="price_main_value"><span class="">80</span></span>
<span class="price_main_currency"><span class="js_curs">Лева</span></span></span>
                    <span class="discount">-50%</span>
                    <strike class="old-price price_old"><span class="price_main_value"><span class="">160</span></span><span class="price_main_currency"><span class="js_curs">Лева</span></span></strike>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block">
                            <div class="landing__countdown">
                                <div class="clearfix hasCountdown" id="timer"><span class="margin hours">16</span><span class="margin minutes">36</span><span class="seconds">23</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-container">
                <h2> UpSize - уголемяващ резултат! </h2>
                <div class="holder">
                    <ul class="list">
                        <li> 100% естествен обем </li>
                        <li> Подходящ за жени, независимо от възрастта </li>
                        <li> Процесът на загуба на еластичност се забавя три пъти
                        </li>
                    </ul>
                    <ul class="list alt">
                        <li> Гърдите се увеличават с два размера на чашката </li>
                        <li> Ще забравите за грозните стрии </li>
                        <li> Направете гърдите си кръгли и твърди, подчертайте формата им </li>
                    </ul>
                </div>
            </div>
            <div class="step-block" id="bust">
                <div class="heading">
                    <h2> Ефектът невидим сутиен </h2>
                    <p> гърдите няма да се нуждаят повече от сутиен! </p>
                </div>
                <ul class="step-list">
                    <li>
                        <strong> Стъпка 1: </strong>
                        <img alt="image description" height="144" src="./assets/img-03.png" width="144"/>
                        <p>
                            Кремът се нанася на почистена кожа. Първо се масажира дясната гърда в продължение на 5-10 минути, докато кремът попие напълно. Повторете няколко пъти.
                        </p>
                    </li>
                    <li>
                        <strong> Стъпка 2: </strong>
                        <img alt="image description" height="144" src="./assets/img-04.png" width="144"/>
                        <p> След това се масажира другата гърда. После изчакайте кремът да попие.  </p>
                    </li>
                    <li>
                        <strong> Стъпка 3: </strong>
                        <img alt="image description" height="144" src="./assets/img-05.png" width="144"/>
                        <p> Накрая масажирайте двете гърди с кръгообразни движения, следващи формата им от долната част на гърдите нагоре и завършете там,  откъдето сте започнали.
                        </p>
                    </li>
                    <li>
                        <strong> Стъпка 4: </strong>
                        <img alt="image description" height="144" src="./assets/img-06.png" width="144"/>
                        <p> Използвайте два пъти дневно: сутрин и вечер. </p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="doctor">
                <h2> Експертно становище относно UpSize: </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p>
                                Много жени днес са изправени пред необходимостта от естетично моделиране на гърдите. Практически всяка жена иска да има стегнати и оформени гърди. Не е изненадващо, тъй като деколтето винаги е било зона на повишено внимание за мъжете.

                            </p>
                        </div>
                        <p>
                            Ако използвате крема ежедневно, обемът на гърдите ви ще се увеличи след 3-4 седмици, формата им ще е по-кръгла и стегната, а кожата ще бъде по-еластична и мека.

                        </p>
                        <div class="text-box">
                            <p>
                                Кремът е преминал успешно клинични изследвания, проведени от експерти от Световната здравна организация в 14 страни на света. Хиляди жени са се убедили в ефикасността на крема UpSize.

                            </p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/img-09.jpg"/></div>
                        <div class="box">
                            <h3> Деоксимироестрол </h3>
                            <p> има ясно изразен подмладяващ ефект и насърчава растежа на гърдите; </p>
                        </div>
                        <div class="box">
                            <h3> Екстрактът от корена  <br/>  пуерариа мирифика </h3>
                            <p> ефективно подхранва и защитава кожата; </p>
                        </div>
                        <div class="box">
                            <h3> Етерично розово масло </h3>
                            <p> подобрява гъвкавостта и еластичността на кожата, премахва стриите и предотвратява появата на нови. </p>
                        </div>
                    </div>
                </div>
                <div class="description">
                    <div class="text">
                        <strong> Вили Димитрова </strong>
                        <span> Доктор на биологичните науки </span>
                    </div>
                    <img alt="image description" height="79" src="./assets/img-11.png" width="133"/>
                </div>
            </div>
            <div class="reviews-block" id="comments">
                <h2> Мнения за UpSize </h2>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/img-07.png" width="130"/>
                        <strong class="name"> София, </strong>
                        <span> 37 г. </span>
                    </div>
                    <div class="holder">
                        <p>
                            Напоследък започнах да забелязвам, че гърдите ми се променят. Работата е там, че с годините кожата става отпусната и губи своята еластичност. Моите рокли  с дълбоко деколте не ми стават вече, така че просто спрях да ги нося. Но намерих спасение с този прекрасен крем! Резултатите наистина ме изненадаха! За 2 седмици бръчките и гънките по кожата на гърдите ми ставаха едва видими и гърдите ми станаха по-твърди. Месец по-късно вече ме заливаха с въпроси къде съм си направила такава жестока операция – и никой не ми вярва, че само съм се мазала с крем, който ми помогна!

                        </p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/img-08.png" width="130"/>
                        <strong class="name"> Емилия </strong>
                        <span> 24 г. </span>
                    </div>
                    <div class="holder">
                        <p>
                            Проблемите с форма на гърдите и това, че не са стегнати апочнаха  да ме преследват, веднага щом отслабвах с 21 кг. Беше абсолютен шок за мен! Гърдите ми увиснаха и губиха своята красота! Започнах да купувам коригиращо бельо за да скрия проблема. Настроението ми беше ужасно, направо бях в депресия. На 24, а гърдите ми изглеждаха все едно съм на 30! Майка ми ми поадри крема за рождения ден. В началото бях дори малко обидена от подаръка, но сега съм толкова благодарна, че не мога да намеря думи! В рамките на един месец се сдобих с нови гърди, и изглеждаха като бюста от реклама за бельо! Никога не съм вярвала, че такава красива форма може да се получи по естествен начин!

                        </p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order">
                <span class="decoration-02"></span>
                <div class="price-block">
                    <h3> САМО ДНЕС  <span> СПЕЦИАЛНА ЦЕНА </span></h3>
                    <span class="price ">80 Лева</span>
                    <span class="old-price"> Стара цена:
              <strike>
<span class="product_old_price " id="old-price">160 Лева</span>
</strike>
</span>
                    <div class="timer-wrap">



                        <div class="timer-wrap">
                            <div class="timer top" id="clock_block2">
                                <div class="landing__countdown">
                                    <div class="clearfix hasCountdown" id="timer2"><span class="margin hours">16</span><span class="margin minutes">37</span><span class="seconds">22</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order-form" id="order_form" method="POST">
                    <input type="hidden" name="total_price" value="80.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAI4FwQScNmZAAEAAQAC0xYBAAIKLgIGAQEABIZXI78A">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="UpSize_BG_Pink">
                    <input type="hidden" name="price" value="80">
                    <input type="hidden" name="old_price" value="160">
                    <input type="hidden" name="total_price_wo_shipping" value="80.0">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 240, 'old_price': 480, 'shipment_price': 0}}">
                    <input type="hidden" name="currency" value="Лева">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="PL">
                    <input type="hidden" name="shipment_vat" value="0.2">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="accept_languages" value="pl-pl">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.2">
                    <!--   -->
                    <fieldset class="s__of">
                        <h3>Поръчай UpSize</h3>
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                    <div class="holder">
                                        <label for="country_code_selector"> Страна </label>
                                    </div>
                                    <div class="text">
                                        <select class="country" id="country_code_selector" name="country_code">
                                            <option value="BG">България</option>
                                           </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label> Име и фамилия  </label>
                                    </div>
                                    <div class="text">
                                        <input name="name" type="text" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label> Телефон </label>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" data-check="1" name="phone" type="text" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <div class="price_text">
                                Доставка: <span class="" id="itog">0 Лева</span>
                            </div>
                            <div class="price_text">Общо: <span class="js_full_price_curs" id="itogo">80 Лева</span>
                            </div>
                            <button class="btn-search  form_btn">Поръчай</button>
                            <div class="toform"></div>
                        </div>
                    </fieldset>
                    <!--    -->
                    <input type="hidden" name="time_zone" value="1"></form>
            </div>
            <div id="footer">
            </div>
        </div>
    </div>

    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }

        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="./assets/beauty_1.css" rel="stylesheet"/>

    <!---->

    </body>
    </html>
<?php } ?>