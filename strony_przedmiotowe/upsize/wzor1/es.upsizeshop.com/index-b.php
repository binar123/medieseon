<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>





    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 4256 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAI_DQRrb6CKAAEAAQAC3QwBAAKgEAIGAQLGBgRYnKaZAA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":39,"old_price":78,"shipment_price":0},"3":{"price":78,"old_price":156,"shipment_price":0},"5":{"price":117,"old_price":234,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Judit Flores';
            var phone_hint = '+34681435813';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <link href="//st.acstnst.com/content/UpSize_ES_Long2/mobile/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
        <title>UpSize</title>
        <link href="./assets/mobile/index.css" media="all" rel="stylesheet" type="text/css"/>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    $(".js_errorMessage2").remove();
                    $(".js_errorMessage").remove();
                    var errors = 0,
                        form = $(this).closest('form'),
                        name = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        countryp = form.find('[id="country_code_selector"]').val(),
                        namep = name.val(),
                        phonep = phone.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;
                    if(name.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(name, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(name, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        var mas={};
                        form.find('input,textatea,select').each(function(){
                            mas[$(this).attr('name')]=$(this).val();
                        });
                        $.post('/order/create_temporary/', mas);
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
    <div class="wrapper s__main" id="wrapper">
        <div class="w0">
            <div class="w1">
                <div class="header">
                    <h1><a>UpSize</a></h1>
                    <div class="description">
                        <p> Aumento natural del pecho </p>
                        <p>Corrección y elevación del pecho</p>
                        <p>Solo componentes naturales</p>
                        <p>95% de las mujeres han
                            <br/> confirmado los resultados</p>
                    </div>
                    <div class="btn-wrap">
                        <button class="btn-order pre_toform">Pide</button>
                    </div>
                    <div class="b-text">
                        <div class="title">Push-up!</div>
                        <span>¡DE UNA VEZ POR TODAS!</span>
                    </div>
                </div>
                <!-- header -->
                <div class="page-info">
                    <div class="dscription">Volumen 100% natural</div>
                    <div class="dscription">Adecuado para todas las mujeres sin importar su edad</div>
                    <div class="dscription">El proceso de envejecimiento de la piel se vuelve 3 veces más lento</div>
                    <div class="dscription">Aumenta tu pecho dos tallas</div>
                    <div class="dscription">Te olvidarás de las marcas de las estrías</div>
                    <div class="dscription">Modela tu pecho de manera redonda y firme, subrayando su forma</div>
                </div>
                <!-- page info -->
                <div class="how-to-use">
                    <div class="content">
                        <div class="box">
                            <h3>Paso 1</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="/./assets/mobile/show-1.png"/>
                                </div>
                                <div class="description">La crema se aplica en la piel limpia. Primero, el pecho derecho debe masajearse durante 5-10 minutos hasta que la crema se haya absorbido. Repetir varias veces.</div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Paso 2</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="/./assets/mobile/show-2.png"/>
                                </div>
                                <div class="description">Después masajear el segundo pecho. Después de eso, dejar que la crema se seque.</div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Paso 3</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="/./assets/mobile/show-3.png"/>
                                </div>
                                <div class="description">Al final, masajear ambos pechos con movimientos redondeados a lo largo del contorno desde la parte inferior hacia arriba y terminar dónde empezó.
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Paso 4</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="/./assets/mobile/show-4.png"/>
                                </div>
                                <div class="description">Usar dos veces al día: por la mañana y por la noche.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- how-to-use -->
                <div class="reviews">
                    <h2>RECENSIONI DI UPSIZE</h2>
                    <div class="content">
                        <div class="box-reviews">
                            <div class="wrap-title">
                                <div class="title"><span class="name">Sofia,</span>37 años</div>
                            </div>
                            <div class="text">
                                <div class="wrapimg"><img alt="" src="/./assets/mobile/ava1.png"/>
                                </div>
                                Últimamente empece a notar que mi pecho estaba cambiando. La cuestión es que la piel se volvió flácida y perdió su elasticidad según han ido pasando las años. Mis vestidos con escotes bajos ya no me quedaban bien, así que dejé de comprarlos. ¡Pero encontré
                                la salvación en esta maravillosa crema! El resultado realmente me sorprendió. En 2 semanas, las arrugas y los pliegues de mi pecho se volvieron prácticamente invisibles, y mi pecho se volvió más firme. Un mes más tarde,
                                mis amigas me bombardeaban con preguntas sobre el sitio donde me habían hecho esa fantástica cirugía - ¡y nadie todavía me cree que fue una crema normal la que me ayudó!

                            </div>
                        </div>
                        <div class="box-reviews">
                            <div class="wrap-title">
                                <div class="title"><span class="name">Emilia,</span> 24 años</div>
                            </div>
                            <div class="text">
                                <div class="wrapimg"><img alt="" src="/./assets/mobile/ava2.png"/>
                                </div>
                                Los problemas con la forma de mi pecho y la flexibilidad comenzaron a perseguirme inmediatamente después de perder 21 kilos. ¡Fue terrible para mí! Mi pecho se volvió flácido y perdió toda su belleza. Empecé a comprarme sujetadores de corrección para
                                ocultar el problema. Mi humor era terrible, incluso cogí una depresión. ¡Con 24 años, mi pecho parecía el de una de 30! Mi madre me regaló la crema como regalo de cumpleaños. Al principio, incluso estaba un poco ofendida
                                por tal regalo, pero ahora estoy agradecida y no tengo palabras para describirlo. En un mes, conseguí un pecho nuevo, y parece como el de las modelos que anuncian lencería. ¡Nunca creí que semejante forma perfecta pudiera
                                obtenerse de manera natural!

                            </div>
                        </div>
                    </div>
                </div>
                <!-- reviews -->
                <div class="sellblock">
                    <h2>- HAGA UN PEDIDO -</h2>
                    <p>PRECIO ESPECIAL:
                        <br/><span class="old_p" style="font-weight: 700;"><s>78 €</s></span>
                        <br/><span class="new_p"> 39 €</span>
                    </p>
                </div>
                <!-- sellblock -->
                <div class="order">

                    <form action="./process.php" id="order_form" method="post" name="order_all">

                        <!-- <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                         <input type="hidden" name="validate_address" value="1">
                         <input type="hidden" name="esub" value="-4A25sMQIJIAI_DQRrb6CKAAEAAQAC3QwBAAKgEAIGAQLGBgRYnKaZAA">
                         <input type="hidden" name="goods_id" value="20">
                         <input type="hidden" name="title" value="UpSize - ES">
                         <input type="hidden" name="ip_city" value="Toruń">
                         <input type="hidden" name="template_name" value="UpSize_ES_Long2">
                         <input type="hidden" name="price" value="39">
                         <input type="hidden" name="old_price" value="78">
                         <input type="hidden" name="total_price_wo_shipping" value="39.0">
                         <input type="hidden" name="package_prices" value="{u'1': {'price': 39, 'old_price': 78, 'shipment_price': 0}, u'3': {'price': 78, 'old_price': 156, 'shipment_price': 0}, u'5': {'price': 117, 'old_price': 234, 'shipment_price': 0}}">
                         <input type="hidden" name="currency" value="€">
                         <input type="hidden" name="package_id" value="1">
                         <input type="hidden" name="total_price" value="39.0">
                         <input type="hidden" name="ip_country_name" value="Poland">
                         <input type="hidden" name="country_code" value="ES">
                         <input type="hidden" name="shipment_vat" value="0.21">
                         <input type="hidden" name="ip_country" value="PL">
                         <input type="hidden" name="protected" value="None">
                         <input type="hidden" name="shipment_price" value="0">
                         <input type="hidden" name="price_vat" value="0.21">
                         <!--   -->

                            <div class="s__of">
                                <select class="country" id="country_code_selector" name="country_code">
                                    <option value="ES">España</option>
                                  </select>
                                <input id="name" name="name" placeholder="Nombre completo" type="text">
                                <input class="only_number" name="phone" placeholder="Teléfono" type="text">
                                <span class="js_new_price_curs">Entrega: 0 € </span>
                                <span class="js_full_price_curs">Precio total: 39 € </span>
                                <button class="btn-order ">Pide</button>
                                <div class="toform"></div>


                        <!--    -->
                    </form>


                </div>
                <!-- order -->
                <!-- footer -->
            </div>
            <!-- w1 -->
        </div>
        <!-- w0 -->
    </div>
    <!-- wrapper -->
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="./assets/beauty_1.css" rel="stylesheet"/>
    <div class="hidden-window v1_1_1_1">
        <div class="h-w-header">
            <div class="h-w-contant">
                <div class="h-w-header-text">
                    <span><b>UPSIZE</b></span>
                </div>
            </div>
        </div>
        <div class="h-w-main">
            <div class="h-w-contant">
                <div class="h-w-left">
                    <div class="h-w-payment h-w-decor-block h-w-vis-mob">
                        <p>PAGO AL <span>SER ENTREGADO</span></p>
                        <ul>
                            <li><span>EL PEDIDO</span></li>
                            <li><span>ENVÍO</span></li>
                            <li><span>ENTREGA Y PAGO</span></li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob h-w-ico h-w-select">
                        <select class="change-package-selector" name="count_select">
                            <option data-slide-index="0" value="1">1 paquete</option>
                            <option data-slide-index="1" selected="selected" value="3">2+1 paquetes</option>
                            <option data-slide-index="2" value="5">3+2 paquetes</option>
                        </select>
                    </div>
                    <ul class="bx-bx">
                        <li data-value="1">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class=" h-w-item-prod"></div>
                                    </div>
                                    <div class="h-w-item-description">¡Pecho firme y elevado desde la primera aplicación!</div>
                                </div>
                                <div class="h-w-price">
                                    <p>paquete</p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Precio anterior:</span><span>78 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Precio:</span><span>39 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Atención :</b> HAY UN <span>50% DE DESCUENTO</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="active" data-value="3">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"></div>
                                        <span><font>+ un regalo</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        ¡Tratamiento de tres meses! ¡Una talla más! ¡Te garantizamos un volumen increíble!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>Packungen <span>un regalo</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Precio anterior:</span><span>156 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Precio:</span><span>78 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Atención :</b> HAY UN <span>50% DE DESCUENTO</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-value="5">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"><span></span></div>
                                        <span><font>+ dos regalos</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        ¡Tratamiento de medio año! ¡Efecto similar a la cirugía! ¡Aumenta hasta 2 tallas! ¡Sin dolor ni cicatrices!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>Packungen <span>dos regalos</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Precio anterior:</span><span>234 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Precio:</span><span>117 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Atención :</b> HAY UN <span>50% DE DESCUENTO</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="h-w-right">
                    <div class="h-w-payment h-w-decor-block h-w-hide-mob">
                        <p>PAGO AL <span>SER ENTREGADO</span></p>
                        <ul>
                            <li><span>EL PEDIDO</span></li>
                            <li><span>ENVÍO</span></li>
                            <li><span>ENTREGA Y PAGO</span></li>
                        </ul>
                    </div>
                    <div class="h-w-honesty h-w-ico h-w-hide-mob">TRANSACCIÓN SEGURA</div>
                    <div class="h-w-form h-w-decor-block js_scrollForm">
                        <p><b>INTRODUCE TUS DATOS </b> PARA HACER UN PEDIDO</p>
                        <form action="/order/create/" method="post"><input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="validate_address" value="1">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAI_DQRrb6CKAAEAAQAC3QwBAAKgEAIGAQLGBgRYnKaZAA">
                            <input type="hidden" name="goods_id" value="20">
                            <input type="hidden" name="title" value="UpSize - ES">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="UpSize_ES_Long2">
                            <input type="hidden" name="price" value="39">
                            <input type="hidden" name="old_price" value="78">
                            <input type="hidden" name="total_price_wo_shipping" value="39.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 39, 'old_price': 78, 'shipment_price': 0}, u'3': {'price': 78, 'old_price': 156, 'shipment_price': 0}, u'5': {'price': 117, 'old_price': 234, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="total_price" value="39.0">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="ES">
                            <input type="hidden" name="shipment_vat" value="0.21">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.21">

                            <div class="h-w-select h-w-ico" style="display:none;">
                                <select id="country_code_selector" name="country_code">
                                    <option value="ES">España</option>
                                </select>
                            </div>
                            <div class="h-w-select h-w-ico">
                                <select class="change-package-selector" name="count_select">
                                    <option data-slide-index="0" value="1">1 paquete</option>
                                    <option data-slide-index="1" selected="selected" value="3">2+1 paquetes</option>
                                    <option data-slide-index="2" value="5">3+2 paquetes</option>
                                </select>
                            </div>
                            <div style="display:none;">
                                <input name="name" type="text"/>
                                <input class="only_number" name="phone" type="text"/>
                            </div>
                            <input name="address" placeholder="introducir la dirección" type="text"/>
                            <!--  -->
                            <div class="h-w-text-total">Precio: <span><font class="js_full_price">78</font> €</span></div>
                            <button class="js_submit">Pide</button>
                        </form>
                    </div>
                    <div class="h-w-guarantee h-w-decor-block">
                        <p>TE GARANTIZAMOS:</p>
                        <ul>
                            <li><b>100%</b> calidad</li>
                            <li><b>Comprobar</b> el producto tras recibirlo</li>
                            <li><b>Seguridad</b> de tus datos</li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob"><div class="h-w-honesty h-w-ico">TRANSACCIÓN SEGURA</div></div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    </body>
    </html>
<?php } else { ?>





    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 4256 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAI_DQRrb6CKAAEAAQAC3QwBAAKgEAIGAQLGBgRYnKaZAA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":39,"old_price":78,"shipment_price":0},"3":{"price":78,"old_price":156,"shipment_price":0},"5":{"price":117,"old_price":234,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Judit Flores';
            var phone_hint = '+34681435813';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> Upsize </title>
        <link href="./assets/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css"/></head><body><p>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    $(".js_errorMessage2").remove();
                    $(".js_errorMessage").remove();
                    var errors = 0,
                        form = $(this).closest('form'),
                        name = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        countryp = form.find('[id="country_code_selector"]').val(),
                        namep = name.val(),
                        phonep = phone.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;
                    if(name.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(name, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(name, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        var mas={};
                        form.find('input,textatea,select').each(function(){
                            mas[$(this).attr('name')]=$(this).val();
                        });
                        $.post('/order/create_temporary/', mas);
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                        return false;
                    }
                });
            });
        </script>
        <script src="./assets/scroll.js"></script>
        <script src="./assets/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="./assets/main.js" type="text/javascript"></script>
        <link href="./assets/index_ru.css" media="all" rel="stylesheet" type="text/css"/>
    </p>
    <div class="s__main" id="wrapper">
        <div id="header">
            <div class="wrap">
                <div class="logo"> UpSize </div>
                <div class="holder">
                    <ul id="nav">
                        <li><a block="wrapper" href="#"> Página pricipal </a></li>
                        <li><a block="bust" href="#"> Upsize? </a></li>
                        <li><a block="doctor" href="#"> Pruebas expertas </a></li>
                        <li><a block="comments" href="#"> Opiniones </a></li>
                    </ul>
                    <button class="link-order pre_toform"> Pedir ahora </button>
                </div>
            </div>
        </div>
        <div class="w1">
            <div class="intro">
                <strong class="logo"><span class="log"> UpSize</span> <span> Push-up - ¡de una vez por todas! </span></strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/img-01.png" width="448"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/img-02.png" width="300"/></div>
                <div class="block">
                    <div class="info-box">
                        <img alt="image description" height="39" src="./assets/icon-01.png" width="48"/>
                        <p> Aumento natural del pecho </p>
                        <img alt="image description" height="39" src="./assets/icon-02.png" width="48"/>
                        <p> Corrección y elevación del pecho </p>
                        <img alt="image description" height="39" src="./assets/icon-03.png" width="48"/>
                        <p> Solo componentes naturales  </p>
                        <img alt="image description" height="39" src="./assets/icon-04.png" width="48"/>
                        <p> 95% de las mujeres han confirmado los resultados </p>
                    </div>
                </div>
                <div class="panel" id="bust">
                    <span class="text"> SOLO  <span> HOY </span></span>
                    <span class="price js_old_price_curs">78 €</span>
                    <span class="discount">-50%</span>
                    <strike class="product_old_price js_new_price_curs">39 €</strike>
                    <a block="order_form" class="link-order" href="#"> Pide </a>
                </div>
            </div>
            <div class="info-container">
                <h2> Upsize - ¡con un resultado voluminoso! </h2>
                <div class="holder">
                    <ul class="list">
                        <li> Volumen 100% natural </li>
                        <li> Adecuado para todas las mujeres sin importar su edad </li>
                        <li> El proceso de envejecimiento de la piel se vuelve 3 veces más lento
                        </li>
                    </ul>
                    <ul class="list alt">
                        <li> Aumenta tu pecho dos tallas </li>
                        <li> Te olvidarás de las marcas de las estrías </li>
                        <li> Modela tu pecho de manera redonda y firme, subrayando su forma </li>
                    </ul>
                </div>
            </div>
            <div class="step-block">
                <div class="heading">
                    <h2> El efecto del sujetador invisible </h2>
                    <p> ¡tu pecho ya no necesita más soporte! </p>
                </div>
                <ul class="step-list">
                    <li>
                        <strong> Paso 1 </strong>
                        <img alt="image description" height="144" src="./assets/img-03.png" width="144"/>
                        <p>
                            La crema se aplica en la piel limpia. Primero, el pecho derecho debe masajearse durante 5-10 minutos hasta que la crema se haya absorbido. Repetir varias veces.
                        </p>
                    </li>
                    <li>
                        <strong> Paso 2 </strong>
                        <img alt="image description" height="144" src="./assets/img-04.png" width="144"/>
                        <p> Después masajear el segundo pecho. Después de eso, dejar que la crema se seque.  </p>
                    </li>
                    <li>
                        <strong> Paso 3 </strong>
                        <img alt="image description" height="144" src="./assets/img-05.png" width="144"/>
                        <p> Al final, masajear ambos pechos con movimientos redondeados a lo largo del contorno desde la parte inferior hacia arriba y terminar dónde empezó.
                        </p>
                    </li>
                    <li>
                        <strong> Paso 4 </strong>
                        <img alt="image description" height="144" src="./assets/img-06.png" width="144"/>
                        <p> Usar dos veces al día: por la mañana y por la noche. </p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="doctor">
                <h2> Opinión experta sobre Upsize: </h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p>
                                Muchas mejores a día de hoy se han enfrentado a la necesidad de modelar su pecho estéticamente. Todas las mujeres desean tener un pecho bonito y erecto. Y no es una sorpresa ya que el escote ha sido siempre la zona de máximo atención para los hombres.

                            </p>
                        </div>
                        <p>
                            Si utilizas la crema a diario, el volumen de tu pecho aumentará en 3-4 semanas, la forma se volverá redondeada y firme, y la piel más elástica y aterciopelada.

                        </p>
                        <div class="text-box">
                            <p>
                                La crema ha pasado por varias pruebas clínicas dirigidas por expertos de la Organización Mundial de la Salud en 24 países del mundo. Miles de mujeres han garantizado que la crema Upsize es eficaz.

                            </p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/img-09.jpg"/></div>
                        <div class="box">
                            <h3> El Deoxymiroestrol </h3>
                            <p> posee un efecto rejuvenecedor pronunciado que promueve el crecimiento del pecho; </p>
                        </div>
                        <div class="box">
                            <h3> El extracto de raíz de  <br/>  Pueraria Mirifica </h3>
                            <p> nutre y protege la piel eficazmente; </p>
                        </div>
                        <div class="box">
                            <h3> El aceite esencial de rosas </h3>
                            <p> aumenta la resistencia y elasticidad de la piel, elimina las estrías y previene su aparición. </p>
                        </div>
                    </div>
                </div>
                <div class="description">
                    <div class="text">
                        <strong> Adela Salas </strong>
                        <span> Doctora y profesora en ciencias biológicas </span>
                    </div>
                    <img alt="image description" height="79" src="./assets/img-11.png" width="133"/>
                </div>
            </div>
            <div class="reviews-block" id="comments">
                <h2> Opiniones sobre Upsize </h2>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/img-07.png" width="130"/>
                        <strong class="name"> Sofía </strong>
                        <span> 37 años </span>
                    </div>
                    <div class="holder">
                        <p>
                            Últimamente empece a notar que mi pecho estaba cambiando. La cuestión es que la piel se volvió flácida y perdió su elasticidad según han ido pasando las años. Mis vestidos con escotes bajos ya no me quedaban bien, así que dejé de comprarlos. ¡Pero encontré la salvación en esta maravillosa crema! El resultado realmente me sorprendió. En 2 semanas, las arrugas y los pliegues de mi pecho se volvieron prácticamente invisibles, y mi pecho se volvió más firme. Un mes más tarde, mis amigas me bombardeaban con preguntas sobre el sitio donde me habían hecho esa fantástica cirugía - ¡y nadie todavía me cree que fue una crema normal la que me ayudó!

                        </p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/img-08.png" width="130"/>
                        <strong class="name"> Emilia </strong>
                        <span> 24 años </span>
                    </div>
                    <div class="holder">
                        <p>
                            Emilia, 24años: Los problemas con la forma de mi pecho y la flexibilidad comenzaron a perseguirme inmediatamente después de perder 21 kilos. ¡Fue terrible para mí! Mi pecho se volvió flácido y perdió toda su belleza. Empecé a comprarme sujetadores de corrección para ocultar el problema. Mi humor era terrible, incluso cogí una depresión. ¡Con 24 años, mi pecho parecía el de una de 30! Mi madre me regaló la crema como regalo de cumpleaños. Al principio, incluso estaba un poco ofendida por tal regalo, pero ahora estoy agradecida y no tengo palabras para describirlo. En un mes, conseguí un pecho nuevo, y parece como el de las modelos que anuncian lencería. ¡Nunca creí que semejante forma perfecta pudiera obtenerse de manera natural!

                        </p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order">
                <span class="decoration-02"><img src="./assets/img-02.png" width="250"/></span>
                <div class="price-block">
                    <h3> SOLO HOY  <span> PRECIO ESPECIAL </span></h3>
                    <span class="price js_new_price_curs">39 €</span>
                    <span class="old-price"> Precio anterior
              <strike>
<span class="product_old_price js_old_price_curs" id="old-price">78 €</span>
</strike>
</span>
                    <div class="timer-wrap">
                        <p> Mantenido hasta el final de la promoción: </p>
                        <div class="timer">
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order-form" id="order_form" method="POST">
                    <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="validate_address" value="1">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAI_DQRrb6CKAAEAAQAC3QwBAAKgEAIGAQLGBgRYnKaZAA">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="UpSize - ES">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="UpSize_ES_Long2">
                    <input type="hidden" name="price" value="39">
                    <input type="hidden" name="old_price" value="78">
                    <input type="hidden" name="total_price_wo_shipping" value="39.0">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 39, 'old_price': 78, 'shipment_price': 0}, u'3': {'price': 78, 'old_price': 156, 'shipment_price': 0}, u'5': {'price': 117, 'old_price': 234, 'shipment_price': 0}}">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="total_price" value="39.0">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="ES">
                    <input type="hidden" name="shipment_vat" value="0.21">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.21">
                    <!--   -->
                    <fieldset class="s__of">
                        <h3> Haga un pedido </h3>
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                    <div class="holder">
                                        <label>País</label>
                                    </div>
                                    <div class="text">
                                        <select class="country" id="country_code_selector" name="country_code">
                                            <option value="ES">España</option>
                                           </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label> Nombre completo</label>
                                    </div>
                                    <div class="text">
                                        <input id="name" name="name" placeholder="Nombre completo" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label> Teléfono </label>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" name="phone" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <dl>
                                <dt> Entrega: </dt>
                                <dd><span class="js_new_price_curs" id="itog">0 €</span>
                                </dd>
                                <dt> Precio total: </dt>
                                <dd><span class="js_full_price_curs" id="itogo">39 €</span>
                                </dd>
                            </dl>
                            <button class="btn-search ">Pedido</button>
                            <div class="toform"></div>
                        </div>
                    </fieldset>
                    <!--    -->
                </form>
            </div>
            <div id="footer">
            </div>
        </div>
    </div>
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="./assets/beauty_1.css" rel="stylesheet"/>
    <div class="hidden-window v1_1_1_1">
        <div class="h-w-header">
            <div class="h-w-contant">
                <div class="h-w-header-text">
                    <span><b>UPSIZE</b></span>
                </div>
            </div>
        </div>
        <div class="h-w-main">
            <div class="h-w-contant">
                <div class="h-w-left">
                    <div class="h-w-payment h-w-decor-block h-w-vis-mob">
                        <p>PAGO AL <span>SER ENTREGADO</span></p>
                        <ul>
                            <li><span>EL PEDIDO</span></li>
                            <li><span>ENVÍO</span></li>
                            <li><span>ENTREGA Y PAGO</span></li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob h-w-ico h-w-select">
                        <select class="change-package-selector" name="count_select">
                            <option data-slide-index="0" value="1">1 paquete</option>
                            <option data-slide-index="1" selected="selected" value="3">2+1 paquetes</option>
                            <option data-slide-index="2" value="5">3+2 paquetes</option>
                        </select>
                    </div>
                    <ul class="bx-bx">
                        <li data-value="1">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class=" h-w-item-prod"></div>
                                    </div>
                                    <div class="h-w-item-description">¡Pecho firme y elevado desde la primera aplicación!</div>
                                </div>
                                <div class="h-w-price">
                                    <p>paquete</p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Precio anterior:</span><span>78 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Precio:</span><span>39 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Atención :</b> HAY UN <span>50% DE DESCUENTO</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="active" data-value="3">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"></div>
                                        <span><font>+ un regalo</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        ¡Tratamiento de tres meses! ¡Una talla más! ¡Te garantizamos un volumen increíble!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>Packungen <span>un regalo</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Precio anterior:</span><span>156 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Precio:</span><span>78 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Atención :</b> HAY UN <span>50% DE DESCUENTO</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-value="5">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"><span></span></div>
                                        <span><font>+ dos regalos</font></span>
                                    </div>
                                    <div class="h-w-item-description">
                                        ¡Tratamiento de medio año! ¡Efecto similar a la cirugía! ¡Aumenta hasta 2 tallas! ¡Sin dolor ni cicatrices!
                                    </div>
                                </div>
                                <div class="h-w-price">
                                    <p>Packungen <span>dos regalos</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Precio anterior:</span><span>234 <font>€</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Precio:</span><span>117 <font>€</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Atención :</b> HAY UN <span>50% DE DESCUENTO</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="h-w-right">
                    <div class="h-w-payment h-w-decor-block h-w-hide-mob">
                        <p>PAGO AL <span>SER ENTREGADO</span></p>
                        <ul>
                            <li><span>EL PEDIDO</span></li>
                            <li><span>ENVÍO</span></li>
                            <li><span>ENTREGA Y PAGO</span></li>
                        </ul>
                    </div>
                    <div class="h-w-honesty h-w-ico h-w-hide-mob">TRANSACCIÓN SEGURA</div>
                    <div class="h-w-form h-w-decor-block js_scrollForm">
                        <p><b>INTRODUCE TUS DATOS </b> PARA HACER UN PEDIDO</p>
                        <form action="" method="post">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="validate_address" value="1">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAI_DQRrb6CKAAEAAQAC3QwBAAKgEAIGAQLGBgRYnKaZAA">
                            <input type="hidden" name="goods_id" value="20">
                            <input type="hidden" name="title" value="UpSize - ES">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="UpSize_ES_Long2">
                            <input type="hidden" name="price" value="39">
                            <input type="hidden" name="old_price" value="78">
                            <input type="hidden" name="total_price_wo_shipping" value="39.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 39, 'old_price': 78, 'shipment_price': 0}, u'3': {'price': 78, 'old_price': 156, 'shipment_price': 0}, u'5': {'price': 117, 'old_price': 234, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="total_price" value="39.0">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="ES">
                            <input type="hidden" name="shipment_vat" value="0.21">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.21">

                            <div class="h-w-select h-w-ico" style="display:none;">
                                <select id="country_code_selector" name="country_code">
                                    <option value="ES">España</option>
                                </select>
                            </div>
                            <div class="h-w-select h-w-ico">
                                <select class="change-package-selector" name="count_select">
                                    <option data-slide-index="0" value="1">1 paquete</option>
                                    <option data-slide-index="1" selected="selected" value="3">2+1 paquetes</option>
                                    <option data-slide-index="2" value="5">3+2 paquetes</option>
                                </select>
                            </div>
                            <div style="display:none;">
                                <input name="name" type="text"/>
                                <input class="only_number" name="phone" type="text"/>
                            </div>
                            <input name="address" placeholder="introducir la dirección" type="text"/>
                            <!--  -->
                            <div class="h-w-text-total">Precio: <span><font class="js_full_price">78</font> €</span></div>
                            <button class="js_submit">Pide</button>
                        </form>
                    </div>
                    <div class="h-w-guarantee h-w-decor-block">
                        <p>TE GARANTIZAMOS:</p>
                        <ul>
                            <li><b>100%</b> calidad</li>
                            <li><b>Comprobar</b> el producto tras recibirlo</li>
                            <li><b>Seguridad</b> de tus datos</li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob"><div class="h-w-honesty h-w-ico">TRANSACCIÓN SEGURA</div></div>
                </div>
            </div>
        </div>
    </div>
    <!---->

    </body></html>
<?php } ?>