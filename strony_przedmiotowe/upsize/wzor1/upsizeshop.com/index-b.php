<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- [pre]land_id = 5071 -->
        <script>var locale = "de";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALXDQThm9uJAAEAAQACcA0BAALPEwIGAQLGBgR9oZThAA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'August lenz';
            var phone_hint = '+492833247';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("de");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
        <title>UPSIZE</title>
        <link href="./assets/mobile/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/jquery.countdown.css" media="all" rel="stylesheet"/>
        <link href="./assets/mobile/favicon.ico" rel="shortcut icon"/>
        <style>.order .line span.errorMedium { color: #ff0000 !important; } </style>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <script src="./assets/jquery.countdown.js"></script>
        <script src="./assets/main.js" type="text/javascript"></script></head><body><p>

        <link href="./assets/mobile/styleSecond.css" rel="stylesheet" type="text/css"/>
        <script src="./assets/jquery.bxslider.min.js"></script>

    </p>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#phone').keypress(function (e) {
                if (!(e.which == 8 || (e.which > 47) & (e.which <= 57))) {
                    return false;
                }
            });
            $('#country').change(function (e, a) {
                def_click(e.currentTarget.value);
                $('#current_country').val($("#country option[value='" + e.currentTarget.value + "']").text());
            });
        });
    </script>
    <div class="wrapper">
        <div class="w0">
            <div class="w1">
                <div class="header">
                    <h1><a>UPSIZE</a></h1>
                    <div class="description">Natürliche Brustvergrößerung
                        <br/> Brustkorrektion und -anhebung
                        <br/> Nur natürliche Bestandteile
                        <br/> 95% der Frauaen haben ergebnisse bestätigt
                    </div>
                    <div class="b-text">
                        <div class="panel">
                            <div class="block1">NUR HEUTE</div>
                            <div class="block2">SPEZIALPREIS <br/> <span class="yelpay-mobsale-basediscountprice" style="color:black;"><span class="js-pp-new">49</span> <span class="js_curs">€</span></span> <br/><span>Alter Preis<br/>
<s class="yelpay-mobsale-baseprice"><span class="js-pp-old">98</span> <span class="js_curs">€</span></s></span>
                            </div>
                            <div class="block3">50%</div>

                            <a class="button rushOrder"><span>Bestellen</span></a>

                        </div>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="blk_block">
                        <div class="title">UpSize</div>
                        <span>VOLUMINÖSES ERGEBNIS!</span>
                    </div>
                </div>
            </div>
            <!-- header -->
            <div class="page-info">
                <div class="dscription">
                    <h3>100% natürliche Fülle</h3>
                </div>
                <div class="dscription">
                    <h3>Vergrößert Ihre Brust um 2 Cups</h3>
                </div>
                <div class="dscription">
                    <h3>Der Prozess des Hänges wird 3 Mal langsamer</h3>
                </div>
            </div>
            <!-- page info -->
            <div class="how-to-use">
                <h2><span>DIE UNSICHTBARE BH-EFFEKT</span>Ihre Brust wird keine Stütze mehr brauchen!</h2>
                <div class="content">
                    <div class="box">
                        <h3>1. Schritt</h3>
                        <div class="c-box">
                            <div class="sm-cover"><img alt="" src="./assets/mobile/show-1.png"/></div>
                            <div class="description">
                                Die Creme wird auf sauberer Haut verwendet. Zu erst sollte die rechte Brust für 5-10 Minuten massiert werden, bis die Creme komplett eingezogen ist. Wiederholen Sie dies mehrmals.
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <h3>2. Schritt</h3>
                        <div class="c-box">
                            <div class="sm-cover"><img alt="" src="./assets/mobile/show-2.png"/></div>
                            <div class="description">
                                Massieren Sie dann die zweite Brust. Warten Sie danach, bis die Creme einzieht.
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <h3>3. Schritt</h3>
                        <div class="c-box">
                            <div class="sm-cover"><img alt="" src="./assets/mobile/show-3.png"/></div>
                            <div class="description">
                                Zum Schluss sollten sie beide Brüste mit runden Bewegungen an der Kontur von unten nach oben massieren. Hören Sie dort auf, wo Sie auch angefangen haben.
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <h3>4. Schritt</h3>
                        <div class="c-box">
                            <div class="sm-cover"><img alt="" src="./assets/mobile/show-4.png"/></div>
                            <div class="description">
                                Zwei Mal täglich verwenden: morgens und abends.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- how-to-use -->
            <div class="reviews">
                <h2>BEWERTUNGEN ÜBER UPSIZE</h2>
                <div class="content">
                    <div class="box-reviews">
                        <div class="wrap-title">
                            <div class="title"><span class="name">Sofia, 37 Jahre alt  </span></div>
                        </div>
                        <div class="text">
                            <div class="wrapimg"><img alt="" src="./assets/mobile/img-07.png"/></div>
                            In letzter Zeit habe ich bemerkt, dass sich meine Brust verändert hat. Die Sache ist, dass die Haut labberig wird und ihre Spannkraft verliert. Meine kurzen Kleider passen mir nicht mehr, weswegen ich sie nicht mehr kaufe. Ich habe mit dieser Creme aber die Rettung gefunden! Die Ergebnisse haben mich wirklich sehr überrascht! In 2 Wochen wurden meine Falten auf den Brüsten kaum noch sichtbar und sie wurden straffer. Schon einen Monat später wurde ich mit Fragen darüber bombardiert, wo ich so eine krasse OP hatte machen lassen und bis heute glaubt mir einfach niemand, dass es eine ganz normale Creme war, die mir geholfen hat!
                        </div>
                    </div>
                    <div class="box-reviews">
                        <div class="wrap-title">
                            <div class="title"><span class="name">Emilie, 24 Jahre alt</span></div>
                        </div>
                        <div class="text">
                            <div class="wrapimg"><img alt="" src="./assets/mobile/img-08.png"/></div>
                            Emilie, 24 Jahre alt: Das Problem ist die Form meiner Brüste. Die Schlaffheit hat mich sofort erreicht, nachdem ich 21kg abgenommen hatte. Das war wirkliche in Schock für mich! Meine Brüste sanken und verloren ihre Schönheit! Ich begann also korrigierende Unterwäsche zu kaufen, um mein Problem zu verstecken. Meine Laune war wirklich schrecklich und ich hatte Depressionen. Im Alter von 24 Jahren sahen meine Brüste aus wie mit 30! Ich bekam die Creme von meiner Mutter als Geburtstagsgeschenk. Anfangs dachte ich, dass es doch ein bisschen zu persönlich sei, so etwas zu schenken. Inzwischen bin ich ihr aber sehr dankbar, und zwar so sehr, dass mir die Worte fehlen, um dies zu beschreiben! In nur einem Monat erhielt ich eine neue Brust und sie sieht gut aus, ganz so wie die eines Models, das Unterwäschewerbung macht! Ich hätte nie gedacht, dass so eine perfekte Form natürlich sein könnte!
                        </div>
                    </div>
                </div>
            </div>
            <div class="sellblock">
                <h2>- NUR HEUTE -</h2>
                <p>SPEZIALPREIS</p>
                <div class="price yelpay-mobsale-basediscountprice"><span class="js-pp-new">49</span> <span class="js_curs">€</span></div>
                <p>Alter Preis <s class="yelpay-mobsale-baseprice"><span class="js-pp-old">98</span> <span class="js_curs">€</span></s></p>
            </div>
            <div class="order">

                <form action="./process.php" id="order_form" method="post" name="order_all"><input type="hidden" name="accept_languages" value="pl-pl">
                    <input type="hidden" name="total_price" value="49.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIALXDQSP-8mZAAEAAQACcA0BAALPEwIGAQLGBgSY-R5TAA">
                    <input type="hidden" name="goods_id" value="20">
                    <input type="hidden" name="title" value="UpSize - DE">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="UpSize_DE_Pink">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="al" value="5071">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="PL">
                    <input type="hidden" name="shipment_vat" value="0.19">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.19">

                    <!--   -->
                    <div class="s__of">
                        <div class="wrap-lines">
                            <div class="line">
                                <select class="country_select" id="country_code_selector" name="country_code">
                                    <option value="GR">Deutschland</option>
                                    <option value="AT">Austria</option>
                                    </select>

                            </div>
                            <div class="line">
                                <div class="title">Vollständiger Name</div>
                                <div class="wrapfield">
                                    <input class="f-field f-error" name="name" placeholder="Name Nachname" type="text">
                                </div>
                            </div>
                            <div class="line">
                                <div class="title">Telefon</div>
                                <div class="wrapfield">
                                    <input class="only_number f-field f-error" name="phone" placeholder="Telefon" type="text">
                                </div>
                            </div>
                        </div>
                        <button class="btn-order js_pre_toform">Bestellen</button>
                        <div class="toform"></div>
                    </div>
                    <!--    -->


                    <input type="hidden" name="time_zone" value="1"></form>


            </div>
        </div>
    </div>

    <style>
        .zplus {color: #f3af42;}
    </style>
    <div class="hidden-window">
        <div class="new-header_goji"><img alt="logo" src="./assets/mobile/header_logo.png" width="300"/></div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Bezahlung<br/>
                                <b>PER NACHNAME</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr" src="./assets/mobile/Zarr.png"/><img alt="alt1" src="./assets/mobile/dtc1.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1" src="./assets/mobile/dtc2.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft" src="./assets/mobile/Zarr.png"/><img alt="alt1" src="./assets/mobile/dtc3.png"/></div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><b>DIE BESTELLUNG</b></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><b>VERSAND</b></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">LIEFERUNG UND <br/><span>BEZAHLUNG</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="new-logo-descr" style="text-align: center;background: #fff; padding: 10px 0; margin-top:10px; margin-bottom:20px;   color: #000; font-size: 24px;">
                            In Zusammenarbeit mit<br/>
                            <img alt="logo2" src="./assets/mobile/nmnm.png"/>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">FAIRER DEAL</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 Packungen</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 Packungen</option>
                                <option data-slide-index="2" value="5">3+2 Packungen</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs"><img alt="zt1" src="./assets/mobile/1.png"/> </div>
                                            <div class="zHeader"><b>1</b>Packungen</div>
                                        </div>
                                        <div class="pack_descr">Feste und gestraffte Brust nach der ersten Anwendung!</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">Preis</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 49 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 98 </text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile"></div>
                                                    <img alt="zt1" src="./assets/mobile/2+1.png"/>
                                                </div>
                                                <div class="zHeader"><b>2</b> Packungen
                                                    <br/> <span class="dib zplus">geschenkt</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">Behandlung über 1 Monat! Plus 1 Größe! Wunderschönes Volumen ist garantiert!</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">Preis</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 98 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 196 </text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <div class="giftmobile"></div>
                                                <img alt="zt1" src="./assets/mobile/3+2.png"/>
                                            </div>
                                            <div class="zHeader"><b>3</b> Packungen
                                                <br/> <span class="dib zplus sec">geschenkt</span>
                                            </div>
                                        </div>
                                        <div class="pack_descr">Effekt, wie nach einer OP! Bis zu 2 Cups größer! Ohne Narben und Schmerzen.</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">Preis</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 147 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 294 </text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager">
                            <a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a>
                            <a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a>
                            <a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                        </div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIALXDQAAAAAAAAAAAASLtDODAA"></iframe>

                            <!-- <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                             <input type="hidden" name="total_price" value="49.0">
                             <input type="hidden" name="esub" value="-4A25sMQIJIALXDQThm9uJAAEAAQACcA0BAALPEwIGAQLGBgR9oZThAA">
                             <input type="hidden" name="goods_id" value="20">
                             <input type="hidden" name="title" value="UpSize - DE">
                             <input type="hidden" name="ip_city" value="Toruń">
                             <input type="hidden" name="template_name" value="UpSize_DE_Pink">
                             <input type="hidden" name="price" value="49">
                             <input type="hidden" name="old_price" value="98">
                             <input type="hidden" name="al" value="5071">
                             <input type="hidden" name="total_price_wo_shipping" value="49.0">
                             <input type="hidden" name="currency" value="€">
                             <input type="hidden" name="package_id" value="1">
                             <input type="hidden" name="protected" value="None">
                             <input type="hidden" name="ip_country_name" value="Poland">
                             <input type="hidden" name="country_code" value="DE">
                             <input type="hidden" name="shipment_vat" value="0.19">
                             <input type="hidden" name="ip_country" value="PL">
                             <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                             <input type="hidden" name="shipment_price" value="0">
                             <input type="hidden" name="price_vat" value="0.19">
                             <div class="formHeader">NUR HEUTE EIN EINZIGARTIGES ANGEBOT!
                             </div>
                             <select class="select inp" id="country_code_selector">
                                 <option value="DE">Deutschland</option>
                                 <option value="AT">Austria</option>
                             </select>
                             <select class="select inp select_count change-package-selector" name="count_select">
                                 <option data-slide-index="0" value="1">1 Packungen</option>
                                 <option data-slide-index="1" selected="selected" value="3">2+1 Packungen</option>
                                 <option data-slide-index="2" value="5">3+2 Packungen</option>
                             </select>
                             <input class="inp j-inp" data-count-length="2+" name="name" placeholder="Namen eintragen" type="text" value=""/>
                             <input class="only_number inp j-inp" name="phone" placeholder="Telefonnummer eintragen" type="text" value=""/>
                             <!--
                             <input class="inp" name="city" placeholder="Stadt eintragen" style="display: none" type="text"/>
                             <input class="inp" name="house" placeholder="Haus" style="display: none" type="text"/>  -->
                            <!--<input class="inp" name="address" placeholder="Adresse eintragen" type="text"/>

                            <!--p style="text-align:center;font-size: 18px;color: #e70116;padding: 5px;">+Versand: <span class="js_delivery_price">0 €</span></p-->
                            <div class="text-center totalpriceForm">Gesamtsumme: <span class="js_total_price js_full_price">49 </span> €</div>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">WIR GARANTIEREN:</div>
                            <ul>
                                <li><b>100%</b>  Qualität</li>
                                <li><b>Produktprüfung </b> nach Erhalt</li>
                                <li><b>Sicherheit</b> Ihrer Daten</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script>
        var slider, options = {
            pagerCustom: '#bx-pager',
            adaptiveHeight: true,
            onSlideBefore: function ($slideElement, oldIndex, newIndex) {
                var ValOptionChg = $slideElement.attr('data-value');
                if (typeof set_package_prices == 'function') {set_package_prices(ValOptionChg);}
                $("select[name='count_select'] option[value='" + ValOptionChg + "']").prop("selected", "selected");
            }
        };

        $('.corbselect, .change-package-selector').on('change', function(){

            slider.goToSlide($(this).find('option:selected').attr('data-slide-index'))
        });

        if (typeof set_package_prices == 'function') {
            set_package_prices(3);
        }
        $('.corbselect').on('change', function () {
            slider.goToSlide($(this).find('option:selected').attr('data-slide-index'))
        });


    </script>
    <style>
        #bx-pager{text-align:center;}
    </style>

    </body></html>
<?php } else { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 5071 -->
        <script>var locale = "de";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALXDQThm9uJAAEAAQACcA0BAALPEwIGAQLGBgR9oZThAA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'August lenz';
            var phone_hint = '+492833247';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("de");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="./assets/secondPage.js"></script>
        <link type="text/css" href="./assets/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>UpSize</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="./assets/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="./assets/favicon.ico" rel="shortcut icon"/>
        <script src="./assets/jquery.countdown.js"></script>
        <script>
            /* Таймер */
            $( document ).ready(function(){
                if ($('#timer').length) {
                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer').html()), 1);
                    $('#timer').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' +
                        '<span class="margin minutes">{m10}{m1}</span>' +
                        '<span class="seconds">{s10}{s1}</span>'

                    }).removeClass('hidden');
                };
                if ($('#timer2').length) {

                    var _currentDate = new Date();
                    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + parseInt($('#timer2').html()), 1);

                    $('#timer2').countdown({
                        until: _toDate,
                        format: 'HMS',
                        compact: true,
                        layout: '<span class="margin hours">{h10}{h1}</span>' +
                        '<span class="margin minutes">{m10}{m1}</span>' +
                        '<span class="seconds">{s10}{s1}</span>'

                    }).removeClass('hidden');
                }

            })


        </script>
    </head><body>
    <div id="wrapper">
        <div id="header">
            <div class="wrap">
                <div class="logo"> UpSize </div>
                <div class="holder">
                    <ul id="nav">
                        <li><a class="scrollto lt2 for-event" data-href="#home">Hauptseite</a></li>
                        <li><a class="scrollto lt3 for-event" data-href="#breast-cream">UpSize</a></li>
                        <li><a class="scrollto lt4 for-event" data-href="#expert">Expertenbeweis</a></li>
                        <li><a class="scrollto lt5 for-event" data-href="#comment-review">Bewertungen</a></li>
                    </ul>
                    <a class="link-order lt6 scrollto jsOpenWindow ">Jetzt bestellen</a>
                </div>
            </div>
        </div>
        <div class="w1" id="home">
            <div class="intro">
                <strong class="logo"><span class="log"> UpSize</span> <span> Push up - una volta per tutte!</span></strong>
                <div class="photo"><img alt="image description" height="614" src="./assets/img-01.png" width="448"/></div>
                <div class="decoration-01"><img alt="image description" src="./assets/pack.png"/></div>
                <div class="block">
                    <div class="info-box">
                        <img alt="image description" height="39" src="./assets/icon-01.png" width="48"/>
                        <p class="lt8">Natürliche Brustvergrößerung</p>
                        <img alt="image description" height="39" src="./assets/icon-02.png" width="48"/>
                        <p class="lt9">Brustkorrektion und -anhebung</p>
                        <img alt="image description" height="39" src="./assets/icon-03.png" width="48"/>
                        <p class="lt10">Nur natürliche Bestandteile</p>
                        <img alt="image description" height="39" src="./assets/icon-04.png" width="48"/>
                        <p class="lt11">95% der Frauaen haben Ergebnisse bestätigt</p>
                    </div>
                </div>
                <div class="panel">
                    <span class="text lt12">NUR<span>HEUTE</span></span>
                    <span class="price new-price price_main"><span class="price_main_value"><span class="js-pp-new">49</span></span>
<span class="price_main_currency"><span class="js_curs">€</span></span></span>
                    <span class="discount">-50%</span>
                    <strike class="old-price price_old"><span class="price_main_value"><span class="js-pp-old">98</span></span><span class="price_main_currency"><span class="js_curs">€</span></span></strike>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-container">
                <h2 class="lt18">UPSIZE - voluminöses Ergebnis!</h2>
                <div class="holder">
                    <ul class="list">
                        <li class="lt19">100% natürliche Fülle</li>
                        <li class="lt20">Passend für alle Frauen, egal welchen Alters</li>
                        <li class="lt22">Vergrößert Ihre Brust um 2 Cups</li>
                    </ul>
                    <ul class="list alt">
                        <li class="lt23">Der Prozess des Hänges wird 3 Mal langsamer</li>
                        <li class="lt24">Sie werden Dehnungsstreifen vergessen</li>
                        <li class="lt25">Macht Ihre Brust rund und fest, unterstreicht die Form</li>
                    </ul>
                </div>
            </div>
            <div class="step-block" id="breast-cream">
                <div class="heading">
                    <h2 class="lt26">Die unsichtbare BH-Effekt</h2>
                    <p class="lt27">Ihre Brust wird keine Stütze mehr brauchen!</p>
                </div>
                <ul class="step-list">
                    <li>
                        <strong class="lt28">1. Schritt</strong>
                        <img alt="image description" height="144" src="./assets/img-03.png" width="144"/>
                        <p>
                            <span class="lt29">Die Creme wird auf sauberer Haut verwendet. Zu erst sollte die rechte Brust für 5-10 Minuten massiert werden, bis die Creme komplett eingezogen ist. Wiederholen Sie dies mehrmals.</span>
                            <br/>
                        </p>
                    </li>
                    <li>
                        <strong class="lt31">2. Schritt</strong>
                        <img alt="image description" height="144" src="./assets/img-04.png" width="144"/>
                        <p class="lt32">Massieren Sie dann die zweite Brust. Warten Sie danach, bis die Creme einzieht.</p>
                    </li>
                    <li>
                        <strong class="lt33">3. Schritt</strong>
                        <img alt="image description" height="144" src="./assets/img-05.png" width="144"/>
                        <p class="lt34">Zum Schluss sollten sie beide Brüste mit runden Bewegungen an der Kontur von unten nach oben massieren. Hören Sie dort auf, wo Sie auch angefangen haben.</p>
                    </li>
                    <li>
                        <strong class="lt35">4. Schritt</strong>
                        <img alt="image description" height="144" src="./assets/img-06.png" width="144"/>
                        <p class="lt36" id="pro">Zwei Mal täglich verwenden: morgens und abends.</p>
                    </li>
                </ul>
            </div>
            <div class="about-box" id="expert">
                <h2 class="lt37">Expertenmeinung über UPSIZE:</h2>
                <div class="wrap">
                    <div class="photo"><img alt="image description" height="526" src="./assets/img-10.png" width="325"/></div>
                    <div class="holder">
                        <div class="text-holder">
                            <p class="lt38">Viele Frauen sehen sich heute mit der Notwendigkeit ihre Brüste nachzuformen konfrontiert. Praktisch jeder möchte pralle und straffe Brüste haben. Das ist nicht erstaunlich, da das De­kolle­tee immer der Bereich war, welcher die Aufmerksamkeit der Männer bekommt.</p>
                        </div>
                        <p class="lt39">Benutzen Sie die Creme täglich, wird sich Ihr Brustvolumen in 3-4 Wochen erhöhen. Die Form wird runder und straffer wirken und die Haut elastischer und weicher sein.</p>
                        <div class="text-box">
                            <p class="lt40">Diese Creme wurde klinischen Tests durch Experten von der Weltgesundheitsorganisation in 14 Ländern unterzogen. Tausende Frauen stellten sicher, dass die Wirkung von UpSize wirklich effektiv ist.</p>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-holder"><img alt="image description" src="./assets/img-09.jpg"/></div>
                        <div class="box">
                            <h3 class="lt41">Deoxymiroestrol</h3>
                            <p class="lt42">hat einen erwähnenswerten Verjüngungseffekt, welcher zur Brustvergrößerung führt:</p>
                        </div>
                        <div class="box">
                            <h3 class="lt43">Pueraria Mirifica<br/>Wurzelextrakt</h3>
                            <p class="lt44">versorgt die Haut mit Feuchtigkeit und schützt sie;</p>
                        </div>
                        <div class="box">
                            <h3 class="lt45">Rosenöl</h3>
                            <p class="lt46">verbessert die Spannkraft und Elastizität, entfernt Dehnungsstreifen und beugt ihrer Erscheinung vor.</p>
                        </div>
                    </div>
                </div>
                <div class="description" id="opinion">
                    <div class="text">
                        <strong class="lt47">Andrea Draugner</strong>
                        <span class="lt48">Doktorin und Professorin der Biologie</span>
                    </div>
                    <img alt="image description" height="79" src="./assets/img-11.png" width="133"/>
                </div>
            </div>
            <div class="reviews-block" id="comment-review">
                <h2 class="lt49">Bewertungen über UPSIZE</h2>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/img-07.png" width="130"/>
                        <strong class="name lt50">Sofia</strong>
                        <span class="lt51">37 Jahre alt</span>
                    </div>
                    <div class="holder">
                        <p class="lt52">In letzter Zeit habe ich bemerkt, dass sich meine Brust verändert hat. Die Sache ist, dass die Haut labberig wird und ihre Spannkraft verliert. Meine kurzen Kleider passen mir nicht mehr, weswegen ich sie nicht mehr kaufe. Ich habe mit dieser Creme aber die Rettung gefunden! Die Ergebnisse haben mich wirklich sehr überrascht! In 2 Wochen wurden meine Falten auf den Brüsten kaum noch sichtbar und sie wurden straffer. Schon einen Monat später wurde ich mit Fragen darüber bombardiert, wo ich so eine krasse OP hatte machen lassen und bis heute glaubt mir einfach niemand, dass es eine ganz normale Creme war, die mir geholfen hat!</p>
                    </div>
                </div>
                <div class="box">
                    <div class="img-holder">
                        <img alt="image description" height="130" src="./assets/img-08.png" width="130"/>
                        <strong class="name lt53">Emilie</strong>
                        <span class="lt54">24 Jahre alt</span>
                    </div>
                    <div class="holder">
                        <p class="lt55">Emilie, 24 Jahre alt: Das Problem ist die Form meiner Brüste. Die Schlaffheit hat mich sofort erreicht, nachdem ich 21kg abgenommen hatte. Das war wirkliche in Schock für mich! Meine Brüste sanken und verloren ihre Schönheit! Ich begann also korrigierende Unterwäsche zu kaufen, um mein Problem zu verstecken. Meine Laune war wirklich schrecklich und ich hatte Depressionen. Im Alter von 24 Jahren sahen meine Brüste aus wie mit 30! Ich bekam die Creme von meiner Mutter als Geburtstagsgeschenk. Anfangs dachte ich, dass es doch ein bisschen zu persönlich sei, so etwas zu schenken. Inzwischen bin ich ihr aber sehr dankbar, und zwar so sehr, dass mir die Worte fehlen, um dies zu beschreiben! In nur einem Monat erhielt ich eine neue Brust und sie sieht gut aus, ganz so wie die eines Models, das Unterwäschewerbung macht! Ich hätte nie gedacht, dass so eine perfekte Form natürlich sein könnte!</p>
                    </div>
                </div>
            </div>
            <div class="order-block" id="order">
                <span class="decoration-03"></span>
                <div class="price-block" endif="" if="" protected="">
                    <h3 class="lt56">NUR HEUTE <span>SPEZIALPREIS</span></h3>

                    <span class="price new-price price_main"><span class="price_main_value"><span class="js-pp-new">49</span></span><span class="price_main_currency"><span class="js_curs">€</span></span></span>
                    <span class="old-price"><span class="lt57">Alter Preis</span> <strike class="price_old"><span class="price_main_value"><span class="js-pp-old">98</span></span>
<span class="price_main_currency"><span class="js_curs">€</span></span></strike></span>
                    <div class="timer-wrap">
                        <div class="timer top" id="clock_block2">
                            <div class="landing__countdown">
                                <div class="clearfix" id="timer2">1000</div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="./process.php" class="order_form order-form" id="order_form" method="POST">
                    <div class="s__of">
                        <div class="box">
                            <div class="frame">
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt60">Land</label>
                                    </div>
                                    <select class="select-purple country_select" id="country_code_selector" name="country_code">
                                        <option value="DE">Deutschland</option>
                                        <option value="AT">Austria</option>
                                        </select>

                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt61">Vollständiger Name</label>
                                    </div>
                                    <div class="text">
                                        <input name="name" placeholder="Name Nachname" type="text" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="holder">
                                        <label class="lt62">Telefon</label>
                                    </div>
                                    <div class="text">
                                        <input class="only_number" name="phone" placeholder="Telefon" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                            </div>
                            <div class="toform"></div>
                        </div>
                    </div>
                    <!--    -->
                    <input type="hidden" name="time_zone" value="1">
<a class="link-order lt6 scrollto jsOpenWindow ">
                    <button class=" btn btn-danger" style="float:right">BESTELLEN</button></a>
                </form>

            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('.for-event').on('touchend, click',function(e){
                    e.preventDefault();
                    var el = $(this).attr('data-href');
                    $('html, body').animate({
                        scrollTop: $(el).offset().top}, 900);
                    return false;
                });
            });

        </script>
    </div>

    <script src="./assets/secondPage.js" type="text/javascript"></script>
    <link href="./assets/styleSecond.css" media="all" rel="stylesheet" type="text/css"/>
    <div class="hidden-window">
        <div class="second-clips-header">
            <div class="containerz">
                <div class="seclogo">Up Size - jetzt bestellen!
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Bezahlung  <br/><b>
                                    per Nachname</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img alt="alt1" src="./assets/dtc1.png"/></div>
                                    <div class="dtable-cell"><b>Die Bestellung</b></div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img alt="alt1" src="./assets/dtc2.png"/></div>
                                    <div class="dtable-cell"><b>Versand</b></div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img alt="alt1" src="./assets/dtc3.png"/></div>
                                    <div class="dtable-cell">Lieferung und<br/>
                                        <span>Bezahlung</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="new-logo-descr" style="text-align: center;background: #fff; padding: 10px 0; margin-top:10px;    color: #000; font-size: 24px;">
                            In Zusammenarbeit mit<br/>
                            <img alt="logo2" src="./assets/nmnm.png"/>
                        </div>
                        <div class="printbg">Fairer Deal</div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIALXDQAAAAAAAAAAAASLtDODAA"></iframe>
                            <!--
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="49.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIALXDQThm9uJAAEAAQACcA0BAALPEwIGAQLGBgR9oZThAA">
                            <input type="hidden" name="goods_id" value="20">
                            <input type="hidden" name="title" value="UpSize - DE">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="UpSize_DE_Pink">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="al" value="5071">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="DE">
                            <input type="hidden" name="shipment_vat" value="0.19">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.19">

                            <div class="formHeader">Nur heute ein einzigartiges Angebot!</div>
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="DE">Deutschland
                                </option>
                                <option value="AT">Austria</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1 Packungen
                                </option>
                                <option selected="selected" value="3">2+1 Packungen
                                </option>
                                <option value="5">3+2 Packungen
                                </option>
                            </select>
                            <input class="inp j-inp" data-count-length="2+" name="name" placeholder="Namen eintragen" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Telefonnummer eintragen" type="text" value=""/>
                            <!--
                            <input class="inp" name="city" placeholder="Stadt eintragen" style="display: none" type="text"/>
                            <input class="inp" name="house" placeholder="Haus" style="display: none" type="text"/>-->
                            <!--<input class="inp" name="address" placeholder="Adresse eintragen" type="text"/>-->

                            <div class="text-center totalpriceForm">Gesamtpreis:
                                <span class="js_total_price js_full_price">49
                        </span> €
                            </div>

                            <div class="zGarant">
                                <div class="zHeader">WIR GARANTIEREN:</div>
                                <ul>
                                    <li><b>100%</b>  Qualität</li>
                                    <li><b>Produktprüfung </b> nach Erhalt</li>
                                    <li><b>Sicherheit</b> Ihrer Daten</li>
                                </ul>
                            </div>
                        </form>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Achtung</b>: Es gibt <br/>
                                einen <span>Rabatt von 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader"><b>1</b> Packungen </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Preis:</div>
                                            <div class="dtable-cell"> <b class="js-pp-new"> 49 </b> <b>€</b> </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old"> 98 </text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Alter<br/>Preis</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Versand:</div>
                                            <div class="dtable-cell"> <b class="js-pp-ship"> 0 </b> <b>€</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Gesamtsumme:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 49 </text>
                                                </b>
                                                €
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="1"></div>
                                <div class="img-wrapp"><img alt="z1" src="./assets/zit1_n.png"/></div>
                                <div class="zHeader"></div>
                                <text>Feste und gestraffte Brust nach der ersten Anwendung!</text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 active">
                            <div class="zstick"></div>
                            <div class="zDiscount">
                                <b>Achtung</b>: Es gibt <br/>
                                einen <span>Rabatt von 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> Packungen  <span class="dib zplus">geschenkt</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Preis:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-new"> 98 </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"> <span class="dib old-pricedecoration"><i></i><span class="js-pp-old"> 196 </span></span></div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Alter<br/>Preis</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Versand:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship"> 0 </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Gesamtsumme:</div>
                                            <div class="dtable-cell prtotal"><b><span class="js-pp-full"> 98 </span> </b> €</div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3"></div>
                                <div class="img-wrapp">
                                    <div class="gift"></div>
                                    <img alt="z1" src="./assets/zit2_n.png"/>
                                </div>
                                <text>Behandlung über 1 Monat! Plus 1 Größe! Wunderschönes Volumen ist garantiert!</text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Achtung</b>: Es gibt <br/>
                                einen <span>Rabatt von 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> Packungen  <span class="dib zplus sec">geschenkt</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Preis:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-new"> 147 </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old"> 294 </text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Alter<br/>Preis</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Versand:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship"> 0 </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Gesamtsumme:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 147 </text>
                                                </b>
                                                €
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5"></div>
                                <div class="img-wrapp">
                                    <div class="gift"></div>
                                    <img alt="z1" src="./assets/zit3_n.png" style="padding-left:10px;"/>
                                </div>
                                <text>Effekt, wie nach einer OP! Bis zu 2 Cups größer! Ohne Narben und Schmerzen.</text>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="shadow" id="popWindow"></div>


    </body></html>
<?php } ?>