<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>





    <!DOCTYPE html>
    <html lang="en">

    <head>

        <!-- [pre]land_id = 3342 -->
        <script>var locale = "fr";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAK6CwQiGR-KAAEAAQACWgsBAAIODQIGAQLGBgRORvRsAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
            var shipment_price = 6;
            var name_hint = 'Rémi morel';
            var phone_hint = '+336314254';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("fr");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="image/vnd.microsoft.icon" rel="shortcut icon" href="http://st.acstnst.com/content/Upsize_FR/mobile/favicon.ico">
        <title>UpSize</title>
        <link rel="stylesheet" type="text/css" href="//st.acstnst.com/content/Upsize_FR/mobile/css/all.css" media="all">
        <!--Second Page-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,greek-ext,latin-ext,cyrillic" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic&subset=latin,greek-ext,latin-ext,cyrillic" rel="stylesheet" type="text/css">
        <link media="all" rel="stylesheet" href="//st.acstnst.com/content/Upsize_FR/mobile/css/style2.css">
        <!--Second Page-->
        <style>
            .order .line span.errorMedium {
                color: #ff0000 !important;
            }
        </style>

        <!--[if lt IE 10]>
        <link rel="stylesheet" media="all" type="text/css" href="//st.acstnst.com/content/Upsize_FR/mobile/css/ie.css" />
        <![endif]-->
    </head>

    <body>


    <div class="wrapper hidejs">
        <div class="w0">
            <div class="w1">
                <div class="header">
                    <h1><a>UpSize</a></h1>
                    <div class="description">
                        <p>Augmentation naturelle des seins</p>
                        <p>Correction et lifting de la ligne</p>
                        <p>Ingrédients exclusivement naturels</p>
                        <p>Résultats confirmés par 95% des femmes</p>
                    </div>
                    <div class="btn-wrap toform">
                        <a class="btn-order jsOpenWindow" href="#order_form">Commander</a>
                    </div>
                    <div class="b-text">
                        <div class="title">Push-up!</div>
                        <span>un résultat qui se mesure!</span>
                    </div>
                </div>
                <!-- header -->
                <div class="page-info">
                    <div class="dscription">Le vieillissement de la peau ralenti de 3 fois</div>
                    <div class="dscription">Convient à toutes les femmes indépendamment de leur âge</div>
                    <div class="dscription">Oubliez les vergetures pour toujours</div>
                    <div class="dscription">Vous pourrez vous permettre un décolleté profond</div>
                    <div class="dscription">Gagnez 2 tailles de bonnet</div>
                    <div class="dscription">Galbe et met en valeur la rondeur de la ligne</div>
                </div>
                <!-- page info -->
                <div class="how-to-use">
                    <div class="content">
                        <div class="box">
                            <h3>Étape 1</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="http://st.acstnst.com/content/Upsize_FR/mobile/images/show-1.png">
                                </div>
                                <div class="description">Appliquez la crème sur une peau nettoyée. Massez un sein pendant 5 à 10 minutes, jusqu'à ce que la crème soit complètement absorbée. Répétez la procédure plusieurs fois</div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Étape 2</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="http://st.acstnst.com/content/Upsize_FR/mobile/images/show-2.png">
                                </div>
                                <div class="description">Massez l'autre sein. Laissez la crème sécher</div>
                            </div>
                        </div>
                        <div class="box">
                            <h3>Étape 3</h3>
                            <div class="c-box">
                                <div class="sm-cover"><img alt="" src="http://st.acstnst.com/content/Upsize_FR/mobile/images/show-3.png">
                                </div>
                                <div class="description">Pour finir, massez les deux seins à la fois avec un mouvement circulaire le long du contour en terminant par la partie inférieure où vous avez commencé</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- how-to-use -->
                <div class="reviews">
                    <h2>Avis sur UpSize</h2>
                    <div class="content">
                        <div class="box-reviews">
                            <div class="wrap-title">
                                <div class="title"><span class="name">Véronique,</span>42 ans</div>
                            </div>
                            <div class="text">
                                <div class="wrapimg"><img alt="" src="http://st.acstnst.com/content/Upsize_FR/mobile/images/ava2.png">
                                </div>
                                Récemment, j’ai remarqué que l'apparence de ma poitrine a commencé à changer. En effet, avec l'âge, la peau commence à flétrir et perd l'élasticité et la douceur d’autrefois. Comme un décolleté plongeant n’était plus pour moi, je n’achetais plus ce style de vêtements. J’ai trouvé la solution dans cette merveilleuse crème! Les résultats m'ont vraiment surprise! Après 2 semaines, les lignes et les plis sont devenus à peine perceptibles, et mes seins sont devenus plus fermes. Un mois plus tard, j’étais bombardée de questions où j’étais faire une chirurgie plastique si réussie et jusqu’à maintenant personne ne croit que cela est le résultat d'une simple crème!
                            </div>
                        </div>
                        <div class="box-reviews">
                            <div class="wrap-title">
                                <div class="title"><span class="name">Isabelle,</span> 24 ans</div>
                            </div>
                            <div class="text">
                                <div class="wrapimg"><img alt="" src="http://st.acstnst.com/content/Upsize_FR/mobile/images/ava1.png">
                                </div>
                                Les problèmes avec la forme et l'élasticité des mes seins ont commencé juste après avoir perdu 21 kilos. J'étais terrifiée! Mes seins ont flétri et ont perdu leur beauté! Je commençais à acheter des sous-vêtements de correction pour cacher le problème ne serait-ce qu’un petit peu. Je me suis senti abattue et déprimée. J’avais 24 ans et mes seins étaient comme ceux d’une femme de 30 ans! J’ai reçu cette crème comme cadeau d'anniversaire de ma mère. Au début, je ne l’ai pas bien pris même si c’était un cadeau. Mais maintenant ma gratitude n’a pas de limite! Un mois plus tard, mes seins ressemblaient à ceux d’une pub de sous-vêtements! Je ne croyais pas que ces formes idéales pourraient être vraiment naturelles!

                            </div>
                        </div>

                    </div>
                </div>
                <!-- reviews -->
                <div class="sellblock">
                    <h2>- Aujourd'hui seulement! -</h2>
                    <p>Prix spécial:
                        <br><strong>49 €</strong>
                        <br><strike>98 €</strike>
                    </p>
                </div>
                <!-- sellblock -->
                <div class="order">
                    <a name="order"></a>
                    <form id="order_form" method="post" name="order_all" action="">
                        <input type="hidden" name="template_name" value="Upsize_FR">
                        <input type="hidden" name="al" value="3342">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="1">
                        <input type="hidden" name="country_code" value="FR">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="shipment_price" value="6">
                        <input type="hidden" name="goods_id" value="20">
                        <input type="hidden" name="title" value="UpSize - FR">
                        <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="price_vat" value="0.2">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAK6CwQiGR-KAAEAAQACWgsBAAIODQIGAQLGBgRORvRsAA">
                        <input type="hidden" name="shipment_vat" value="0.2">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="price" value="49">
                        <input type="hidden" name="old_price" value="98">
                        <input type="hidden" name="total_price" value="55.0">
                        <input type="hidden" name="validate_address" value="1">
                        <input type="hidden" name="total_price_wo_shipping" value="49.0">
                        <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="button" value="Commander" class="jsOpenWindow btn-order">
                    </form>
                </div>
                <!-- order -->

                <!-- footer -->
            </div>
            <!-- w1 -->
        </div>
        <!-- w0 -->
    </div>

    <div class="hidden-window">
        <div class="new-header_goji"><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/header_logo.png" alt="logo" width="300"></div>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic">
        <div class="containerz">
            <section class='h-w__inner'>
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">PAIEMENT À LA LIVRAISON</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/Zarr.png" alt="arrow" class="zarrr"><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/dtc1.png" alt="alt1"></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/dtc2.png" alt="alt1"></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/Zarr.png" alt="arrow" class="zarrrLeft"><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/dtc3.png" alt="alt1"></div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">LA COMMANDE</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">LIVRAISON</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">LIVRAISON ET <span>PAIEMENT</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">OFFRE AVANTAGEUSE</div>
                            <select name="count_select" class="corbselect select inp select_count change-package-selector">
                                <option value="1" data-slide-index="0">1 paquet</option>
                                <option value="3" selected="selected" data-slide-index="1">2+1 paquets!</option>
                                <option value="5" data-slide-index="2">3+2 paquets! </option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value='1'>
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class='ssimgabs'><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/1.png" alt='zt1'> </div>
                                            <div class="zHeader"><b>1</b> paquet </div>
                                            <br>
                                        </div>
                                        <div class="pack_descr">Soulagement immédiat de la lourdeur des jambes! Effet anti-varices après la première application!</div>
                                        <div class="dtable" style='width: 85%;margin: 0 auto;'>
                                            <div class="dtable-cell red text-right">Prix:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">49</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>98</div>€</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value='3'>
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class='ssimgabs'>
                                                    <div class="giftmobile">+ en cadeau</div><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/2+1.png" alt='zt1'> </div>
                                                <div class="zHeader"><b>2</b> paquets
                                                    <br> <span class="dib zplus">en cadeau</span></div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">La cure! Dissout les coagulums, enlève les varicosité! De belles et saines jambes aujourd’hui!</div>
                                        <div class="dtable" style='width: 85%;margin: 0 auto;'>
                                            <div class="dtable-cell red text-right">Prix:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">98</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>196</div>€</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value='5'>
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right">
                                            <div class='ssimgabs'>
                                                <div class="giftmobile">+ en cadeau</div><img src="http://st.acstnst.com/content/Upsize_FR/mobile/imageSecond/3+2.png" alt='zt1' width=""> </div>
                                            <div class="zHeader"><b>3</b> paquets
                                                <br> <span class="dib zplus sec">en cadeau</span> </div>
                                        </div>
                                        <div class="pack_descr">Une cure de 6 mois! Même un hôpital ne peut pas garantir un tel résultat! Élimination complète des varices! Effet garanti.</div>
                                        <div class="dtable" style='width: 85%;margin: 0 auto;'>
                                            <div class="dtable-cell red text-right">Prix:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">147</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>294</div>€</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager"><a data-slide-index="0" class='change-package-button' data-package-id='1' href="">1</a><a data-slide-index="1" class='change-package-button' data-package-id='3' href="">3</a><a data-slide-index="2" class='change-package-button' data-package-id='5' href="">5</a></div>
                        <form action="" method="post" class="js_scrollForm">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAK6CwAAAAAAAAAAAARpxoRrAA"></iframe>

                            <!--   <input type="hidden" name="template_name" value="Upsize_FR">
                               <input type="hidden" name="al" value="3342">
                               <input type="hidden" name="currency" value="€">
                               <input type="hidden" name="package_id" value="1">
                               <input type="hidden" name="country_code" value="FR">
                               <input type="hidden" name="ip_country" value="PL">
                               <input type="hidden" name="shipment_price" value="6">
                               <input type="hidden" name="goods_id" value="20">
                               <input type="hidden" name="title" value="UpSize - FR">
                               <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                               <input type="hidden" name="price_vat" value="0.2">
                               <input type="hidden" name="esub" value="-4A25sMQIJIAK6CwQiGR-KAAEAAQACWgsBAAIODQIGAQLGBgRORvRsAA">
                               <input type="hidden" name="shipment_vat" value="0.2">
                               <input type="hidden" name="ip_city" value="Toruń">
                               <input type="hidden" name="price" value="49">
                               <input type="hidden" name="old_price" value="98">
                               <input type="hidden" name="total_price" value="55.0">
                               <input type="hidden" name="validate_address" value="1">
                               <input type="hidden" name="total_price_wo_shipping" value="49.0">
                               <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                               <input type="hidden" name="protected" value="None">
                               <input type="hidden" name="ip_country_name" value="Poland">
                               <div class="formHeader"></div>
                               <select name="country_code" class='select inp' id="country_code_selector">
                                   <option value="FR">France</option>
                               </select>
                               <select name="count_select" class="select inp select_count change-package-selector">
                                   <option value="1" data-slide-index='0'>1 paquet</option>
                                   <option value="3" selected="selected" data-slide-index='1'>2+1 paquets </option>
                                   <option value="5" data-slide-index='2'>3+2 paquets </option>
                               </select>
                               <input type="text" name="name" class="inp j-inp" value="" data-count-length="2+" placeholder="Prénom Nom de famille">
                               <input type="text" name="phone" class="only_number inp j-inp" value="" placeholder="Entrez votre numéro de téléphone">
                               <!--
                                 <input type="text" name="city" placeholder="Entrez votre ville" class="inp" style="display: none">
                                 <input type="text" name="address" placeholder="Entrer votre adresse" class="inp">
                               -->
                            <div class="js-ship-pr">+Livraison <span class="js_delivery_price">6</span> €</div>
                            <div class="text-center totalpriceForm">total: <span class="js_total_price js_full_price">55 </span> € </div>
                            <div class="zbtn  transitionmyl" onclick='validateAdrr()'>¡HAGA UN PEDIDO!</div>
                            <div class="js_submit" style='visibility: hidden;'></div>
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">NOUS GARANTISSONS</div>
                            <ul>
                                <li><b>100%</b> qualité</li>
                                <li><b>Vérification</b> du produit à la réception</li>
                                <li><b>Sécurité</b> de vos données</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- wrapper -->
    <script src="//st.acstnst.com/content/Upsize_FR/mobile/js/jquery.bxslider.min.js"></script>
    <script>
        var slider, options = {
            pagerCustom: '#bx-pager',
            adaptiveHeight: true,
            auto: true,
            onSlideBefore: function ($slideElement, oldIndex, newIndex) {
                var ValOptionChg = $slideElement.attr('data-value');
                if (typeof set_package_prices == 'function') {
                    set_package_prices(ValOptionChg);
                }
                $("select[name='count_select'] option[value='" + ValOptionChg + "']").prop("selected", true);
            }
        };

        $('.corbselect, .change-package-selector').on('change', function () {
            slider.goToSlide($(this).find('option:selected').attr('data-slide-index'))
        });
        if (typeof set_package_prices == 'function') {
            set_package_prices(3);
        }
        $('.corbselect').on('change', function () {
            slider.goToSlide($(this).find('option:selected').attr('data-slide-index'))
        });
        $(document).ready(function () {
            $('.jsOpenWindow').on('touchend, click', function (e) {
                e.preventDefault();
                $('.hidejs').hide();
                $('.hidden-window').fadeIn();
                $('html, body').animate({
                    scrollTop: 0
                }, 300);
                slider = $('.bx-bx').bxSlider(options);
                setTimeout(function () {
                    $('#bx-pager a').eq(1).click();
                }, 300);
                return false;
            });
        });
    </script>
    </body>

    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html lang="en-US">
    <head>

        <!-- [pre]land_id = 3342 -->
        <script>var locale = "fr";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAK6CwQiGR-KAAEAAQACWgsBAAIODQIGAQLGBgRORvRsAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
            var shipment_price = 6;
            var name_hint = 'Rémi morel';
            var phone_hint = '+336314254';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("fr");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <meta charset="UTF-8">
        <title> Site Officiel de UpSize - Crème pour augmentation mammaire - Garantie des résultats! Avis et prix </title>
        <link rel="shortcut icon" type="image/x-icon" href="http://st.acstnst.com/content/Upsize_FR/favicon.ico"/>
        <link rel="stylesheet" href="//st.acstnst.com/content/Upsize_FR/css/style.css"/>
        <link rel="stylesheet" href="//st.acstnst.com/content/Upsize_FR/css/btn-trans.css"/>

        <!--Second Page-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin-ext' rel='stylesheet'
              type='text/css'>
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic">
        <link rel="stylesheet" href="//st.acstnst.com/content/Upsize_FR/css/styleSecond.css">
        <!--/Second Page-->

        <!--Second Page-->
        <script src="//st.acstnst.com/content/Upsize_FR/js/secondPage.js"></script>
        <!--/Second Page-->

        <script type="text/javascript" src="//st.acstnst.com/content/Upsize_FR/js/js.js"></script>
        <!--[if lt IE 9]>
        <script src="//st.acstnst.com/content/Upsize_FR/js/html5.js"></script>
        <link rel="stylesheet" href="//st.acstnst.com/content/Upsize_FR/css/ie.css"/>
        <![endif]-->
    </head>
    <body>

    <div class="body">
        <header>
            <div class="header-content">
                <nav id="top-menu">
                    <ul>
                        <li><a href="#"><span>  Haut  </span></a></li>
                        <li><a href="#"><span>  UpSize?  </span></a></li>
                        <li><a href="#"><span>  Avis d'expert  </span></a></li>
                        <li><a href="#"><span>  Avis  </span></a></li>
                        <li style="display:none;"><a href="#"><span>  Expédition  </span></a></li>
                        <li><a href="#"><span>  Commander maintenant  </span></a></li>
                    </ul>
                </nav>
            </div>
            <div class="shadow"></div>
        </header>

        <section id="wrapper">
            <section id="info">
                <div class="info-content">
                    <div class="info-content-body">
                        <div class="flag">
                            <p>  Méthode 100%<br>naturelle<br>garantie     </p>
                        </div>
                        <div class="info-block">
                            <h3>  Une poitrine de rêve permanente!  </h3>
                            <p>  Des résultats visibles en une semaine  </p>
                            <ul class="info-lists">
                                <li><img src="http://st.acstnst.com/content/Upsize_FR/_i/list-1.png" alt=""/> <span>  Augmentation naturelle des seins  </span></li>
                                <li><img src="http://st.acstnst.com/content/Upsize_FR/_i/list-2.png" alt=""/> <span>  Remodelage et raffermissement des seins  </span></li>
                                <li><img src="http://st.acstnst.com/content/Upsize_FR/_i/list-3.png" alt=""/> <span>  Ingrédients exclusivement naturels  </span></li>
                                <li><img src="http://st.acstnst.com/content/Upsize_FR/_i/list-4.png" alt=""/> <span>  Résultats confirmés par 95% des femmes  </span></li>
                            </ul>
                        </div>
                        <div class="middle-menu">
                            <a href="#form-action" class="btn-order btn-trans jsOpenWindow">  Commander  </a>
                            <div class="only-today">  Aujourd'hui seulement!  </div>
                            <div class="price">
                                <div class="spec-price">  Prix spécial -   <span class="j_new_price js_new_price_curs"><span style="font-family: arial;"> 49 </span>€ </span></div>
                                <div class="old-price">  Au lieu de   <span class="line j_old_price js_old_price_curs"> <span style="font-family: arial;"> 98 </span>€ </span></div>
                                <div class="diskount">
                                    <div>50%</div>
                                    <div>de remise  </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="result">
                <div class="red-logo"></div>
                <div class="result-content">
                    <h1>  UpSize – un résultat qui se voit!  </h1>
                    <div class="result-lists">
                        <div class="result-block"><span>  Volume 100% naturel  </span></div>
                        <div class="result-block"><span>  Limite les effets du vieillissement  </span></div>
                        <div class="result-block"><span>  Convient à toutes les femmes  </span></div>
                        <div class="result-block"><span>  Efface les vergetures!  </span></div>
                        <div class="result-block"><span>  Augmente le volume de la poitrine  </span></div>
                        <div class="result-block"><span>  A vous les décolletés profonds!  </span></div>
                        <div class="result-block"><span>  Galbe votre poitrine et met en valeur la rondeur des seins  </span></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </section>

            <section id="effetcs">
                <div class="table">
                    <div class="table-content">
                        <h1>  Effet « PUSH UP » GARANTI  </h1>
                        <p>  - Votre poitrine n’aura plus besoin de support!  </p>
                        <div class="table-shows">
                            <div class="show-block">
                                <img src="http://st.acstnst.com/content/Upsize_FR/_i/show-1.png" alt=""/>
                                <h3>  Étape 1  </h3>
                                <p>  Appliquez la crème sur la peau nettoyée. Massez le premier sein sein pendant 5 à 10 minutes, jusqu'à ce que la crème soit complètement absorbée. Répétez la procédure plusieurs fois.  </p>
                            </div>
                            <div class="show-block">
                                <img src="http://st.acstnst.com/content/Upsize_FR/_i/show-2.png" alt=""/>
                                <h3>  Étape 2  </h3>
                                <p>  Massez l'autre sein. Laissez la crème sécher.  </p>
                            </div>
                            <div class="show-block">
                                <img src="http://st.acstnst.com/content/Upsize_FR/_i/show-3.png" alt=""/>
                                <h3>  Étape 3  </h3>
                                <p>  Pour finir, massez les deux seins à la fois avec un mouvement circulaire le long du contour en terminant par la partie opposée  où vous avez commencé.  </p>
                            </div>
                            <div class="show-block">
                                <img src="http://st.acstnst.com/content/Upsize_FR/_i/show-4.png" alt=""/>
                                <h3>  Étape 4  </h3>
                                <p>  Utilisez deux fois par jour, matin et soir.  </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="spec-tell">
                <div class="spec-left">
                    <img src="http://st.acstnst.com/content/Upsize_FR/_i/woman.png" alt=""/>
                    <div class="spec-name">
                        <div>
                            <span>  Aline M. Bonnet  </span><br/>
                            Docteur en sciences biologiques, Professeur
                        </div>
                    </div>
                </div>
                <div class="spec-center">
                    <h1>  Les spécialistes à propos de UpSize  </h1>
                    <p><strong>  Les femmes d’aujourd’hui veulent avoir une poitrine parfaite et beaucoup envisagent  un recours à la chirurgie esthétique. Cela n’est pas étonnant car une belle poitrine  attire toujours le regard des hommes.   </strong></p>
                    <p>  L'application quotidienne de UpSize pendant 3 à 4 semaines favorise une augmentation du volume des seins. Et leur forme s’arrondie. L’épiderme redevient plus souple, et ferme à la fois avec un velouté de grain de peau régénéré.   </p>
                    <p>  UpSize a été testée cliniquement par l'Organisation Mondiale de la Santé. Et des milliers de femmes sont convaincues par l’utilisation de UpSize.</p>
                </div>
                <div class="spec-right">
                    <img src="http://st.acstnst.com/content/Upsize_FR/_i/formula.png" alt=""/>
                    <div class="formula-block">
                        Le <span>  Deoxymiroestrol  </span>qui est un élément actif contenu dans la plante thaïlandaise Pueraria. Mirifica, possède des vertus anti-vieillissement et favorise l'augmentation naturelle de la poitrine.
                    </div>
                    <div class="formula-block">
                        <span>Sa formule 100% naturelle</span>  nourrit et protège la peau efficacement.
                    </div>
                    <div class="formula-block">
                        <span>L’huile essentielle de rose</span> améliore les qualités d’élasticité de la peau, son drainage et  favorise ainsi la prévention anti-vergetures.
                    </div>
                </div>
                <div class="clear"></div>
                <div class="sertificates">
                    <div class="sertificates-block">
                        <img src="http://st.acstnst.com/content/Upsize_FR/_i/sert-1_1.png" alt=""/>
                        <img src="http://st.acstnst.com/content/Upsize_FR/_i/sert-2_1.png" alt=""/>
                        <img src="http://st.acstnst.com/content/Upsize_FR/_i/sert-3.png" alt=""/>
                        <img src="http://st.acstnst.com/content/Upsize_FR/_i/sert-4.png" alt=""/>
                    </div>
                </div>
            </section>

            <section id="comments">
                <h1>  Avis sur UpSize  </h1>
                <div class="comment">
                    <div class="comment-image">
                        <img src="http://st.acstnst.com/content/Upsize_FR/_i/comment-2.png" alt=""/>
                        <span>  Véronique, 42 ans  </span>
                    </div>
                    <div class="comment-content">
                        Récemment, ma poitrine a commencé à changer. En effet, avec l'âge, la ma peau  a commencée à flétrir, perdre son  l'élasticité et la sa douceur d’autrefois. Je  n’osais plus porter de décolletés. Mais avec cette merveilleuse crème, j’ai enfin trouvé la solution! Les résultats m'ont vraiment surpris ! Après seulement 2 semaines, les lignes et les plis sont devenus à peine perceptibles, et mes seins sont devenus plus fermes. Un mois plus tard, j’étais bombardée de questions sur l’endroit où je m’étais fait  faire une chirurgie plastique si réussie ! Encore maintenant personne ne croit que cela est le résultat d'une simple crème!
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="comment">
                    <div class="comment-image">
                        <img src="http://st.acstnst.com/content/Upsize_FR/_i/comment-1.png" alt=""/>
                        <span>  Isabelle, 24 ans  </span>
                    </div>
                    <div class="comment-content">
                        Les problèmes avec la forme et l'élasticité de mes seins ont commencé juste après avoir perdu 21 kilos. J'étais terrifiée! Ma poitrine a flétri et a perdu sa beauté! Je commençais alors  à porter des soutein-gorges rembourrés pour cacher le problème ne serait-ce qu’un petit peu.  J’étais abattue et déprimée. Moi qui avais 24 ans, j’avais les seins comme ceux d’une femme de 50 ans! J’ai reçu cette crème comme cadeau d'anniversaire de ma mère. Au début, je ne l’ai pas vraiment utilisée même si c’était un cadeau. Maintenant ma gratitude n’a pas de limite! En un mois, mes seins ressemblent à ceux d’un mannequin de sous-vêtements! Je ne n’imaginais même pas obtenir une telle poitrine de rêve! avec une méthode 100% naturelle!
                    </div>
                    <div class="clear"></div>
                </div>
            </section>

            <section id="form">
                <div class="form-content style2" id="form-content">
                    <div class="form-container">
                        <img src="http://st.acstnst.com/content/Upsize_FR/_i/pack1.png" class="pack">
                        <div class="discount">50%<br>remise</div>
                        <div class="form-block" style="display:none">

                            <form action="
" method="post" id="order_form" >
                                <input type="hidden" name="template_name" value="Upsize_FR">
                                <input type="hidden" name="al" value="3342">
                                <input type="hidden" name="currency" value="€">
                                <input type="hidden" name="package_id" value="1">
                                <input type="hidden" name="country_code" value="FR">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="shipment_price" value="6">
                                <input type="hidden" name="goods_id" value="20">
                                <input type="hidden" name="title" value="UpSize - FR">
                                <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="price_vat" value="0.2">
                                <input type="hidden" name="esub" value="-4A25sMQIJIAK6CwQiGR-KAAEAAQACWgsBAAIODQIGAQLGBgRORvRsAA">
                                <input type="hidden" name="shipment_vat" value="0.2">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="price" value="49">
                                <input type="hidden" name="old_price" value="98">
                                <input type="hidden" name="total_price" value="55.0">
                                <input type="hidden" name="validate_address" value="1">
                                <input type="hidden" name="total_price_wo_shipping" value="49.0">
                                <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">

                                <h1>  Mon panier  </h1>
                                <div><label>Pays:</label></div>
                                <div class="select input-select">
                                    <div class="select-value">France</div>
                                    <div class="select-drop input-select">
                                        <div rel="FR">France</div>
                                    </div>
                                </div>
                                <!--   							<div><label for="fio">  Nom  </label> <span>  Exemple: Valérie Martin  </span></div> -->
                                <input id="fio" name="name" class="input_form" data-count-length="2+" placeholder="Prénom Nom de famille" type="text" value=""/>

                                <!--   							<div><label for="phone">  Téléphone:  </label> <span>  exemple: +33660022222  </span></div> -->
                                <input id="phone" name="phone" data-count-length="2+" class="phone_input input_form only_number" type="text" placeholder="Téléphone" value=""/>

                                <!-- <div><label for="phone">  Adresse postale:  35 Rue Tandou, 75019, Paris, France </label></div> -->
                                <!--<input id="lebel2" class="input_form" name="address" placeholder="Adresse postale" type="text">-->

                                <!-- <div><label for="phone">  Ville:  </label> </div> -->
                                <!-- <input id="lebel2" class="input_form" name="city" placeholder="Ville"  type="text">-->

                                <!-- <div><label for="phone">  Appartement:  </label></div> -->
                                <!--<input id="lebel2" class="input_form" name="house" placeholder="Appartement"  type="text">-->

                                <div class="product_count_block">
                                    Menge
                                    <input class="inp js_count" value="1" readonly="" type="text">
                                    <div class="controls">
                                        <span class="more" id="more"></span>
                                        <span class="less" id="less"></span>
                                    </div>
                                </div>

                                <!--   							<p>  Prix    <span class="j_new_price js_new_price_curs"> 49 € </span></p> -->
                                <p>  Livraison
                                    <span class="js_delivery_curs" >
  	                                    6 €
  	                                </span>
                                </p>
                                <p class="total-prx">  Prix total
                                    <span class="js_full_price total-prx" >
  	                                    55
  	                                </span>
                                    <span>€</span>
                                </p>

                                <input type="submit" value="Commander" class="js_submit btn-trans"/>
                            </form>
                        </div>
                        <div class="form-info">
                            <h2>  - Aujourd'hui seulement!  </h2>
                            <h1>  Prix spécial  </h1>
                            <div class="icon-beg j_new_price js_new_price_curs"><span style="font-family: arial;"> 49</span> €</div>
                            <div class="form-older-price">  Au lieu de   <span class="j_old_price js_old_price_curs"><span style="font-family: arial;">  98</span> €.</span></div>
                            <div class="finish-discount">  Cette offre   <br/>   se termine dans  </div>
                            <div class="timer">
                                <div class="minutes">
                                    <div class="minute_1"><span>0</span></div>
                                    <div class="clear"></div>
                                    <div class="timer-info">  Minutes  </div>
                                </div>
                                <div class="zap">,</div>
                                <div class="seconds">
                                    <div class="seconde_1"><span>5</span></div>
                                    <div class="clear"></div>
                                    <div class="timer-info">  Secondes  </div>
                                </div>
                                <img src="http://st.acstnst.com/content/Upsize_FR/_i/clock.png" class="form-image" alt=""/>
                                <div class="clear"></div>

                                <div class="form-block">
                                    <input value="Commander" class="jsOpenWindow btn-trans" type="submit">
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </section>

            <footer>
                <div class="footer-left">
                    <p></p>
                    <p></p>
                </div>
                <div class="footer-right">
                    <p></p>
                </div>
                <div class="clear"></div>
            </footer>

        </section>
    </div>


    <div class="hidden-window">
        <div class="h-headerj">
            <div class="containerz clearfix"><img class="left-side-header" src="http://st.acstnst.com/content/Upsize_FR/imageSecond/header_logo.png"></div>
        </div>
        <div class="containerz">
            <section class='h-w__inner'>
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Paiement
                                <br> <b>à la livraison</b></div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img src="http://st.acstnst.com/content/Upsize_FR/imageSecond/dtc1.png" alt="alt1"></div>
                                    <div class="dtable-cell">La commande</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img src="http://st.acstnst.com/content/Upsize_FR/imageSecond/dtc2.png" alt="alt1"></div>
                                    <div class="dtable-cell">Livraison</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img src="http://st.acstnst.com/content/Upsize_FR/imageSecond/dtc3.png" alt="alt1"></div>
                                    <div class="dtable-cell">Livraison et <span>paiement</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">OFFRE AVANTAGEUSE</div>
                        <form action="" method="post" class="js_scrollForm">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAK6CwAAAAAAAAAAAARpxoRrAA"></iframe>

                            <!-- <input type="hidden" name="template_name" value="Upsize_FR">
                             <input type="hidden" name="al" value="3342">
                             <input type="hidden" name="currency" value="€">
                             <input type="hidden" name="package_id" value="1">
                             <input type="hidden" name="country_code" value="FR">
                             <input type="hidden" name="ip_country" value="PL">
                             <input type="hidden" name="shipment_price" value="6">
                             <input type="hidden" name="goods_id" value="20">
                             <input type="hidden" name="title" value="UpSize - FR">
                             <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                             <input type="hidden" name="price_vat" value="0.2">
                             <input type="hidden" name="esub" value="-4A25sMQIJIAK6CwQiGR-KAAEAAQACWgsBAAIODQIGAQLGBgRORvRsAA">
                             <input type="hidden" name="shipment_vat" value="0.2">
                             <input type="hidden" name="ip_city" value="Toruń">
                             <input type="hidden" name="price" value="49">
                             <input type="hidden" name="old_price" value="98">
                             <input type="hidden" name="total_price" value="55.0">
                             <input type="hidden" name="validate_address" value="1">
                             <input type="hidden" name="total_price_wo_shipping" value="49.0">
                             <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                             <input type="hidden" name="protected" value="None">
                             <input type="hidden" name="ip_country_name" value="Poland">
                             <select name="country_code" class="select inp" id="country_code_selector">
                                 <option value="FR">France</option>
                             </select>
                             <select name="count_select" class="select inp select_count change-package-selector">
                                 <option value="1">1 paquet</option>
                                 <option value="3" selected="selected">2+1 paquets </option>
                                 <option value="5">3+2 paquets </option>
                             </select>
                             <input type="text" name="name" class="inp j-inp" value="" data-count-length="2+" placeholder="Prénom Nom de famille">
                             <input type="text" name="phone" class="only_number inp j-inp" value="" placeholder="Entrez votre numéro de téléphone">
                             <!--
                               <input type="text" name="city" placeholder="Entrez votre ville" class="inp" style="display: none">
                               <input type="text" name="address" placeholder="Entrer votre adresse" class="inp">
   -->
                            <div class="text-center totalpriceForm">total: <span class="js_total_price js_full_price">83 </span> eur</div>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Nous garantissons</div>
                            <ul>
                                <li><b>100%</b> qualité</li>
                                <li><b>Vérification</b> du produit à la réception</li>
                                <li><b>Sécurité</b> de vos données</li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1">
                            <div class="zDiscount"><b>Attention</b>:
                                <br><span>Rabais de 50%</span></div>
                            <div class="item__right">
                                <div class="zHeader"><b>1</b> paquet </div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prix:</div>
                                            <div class="dtable-cell"><b class='js-pp-new'>

                                                    49

                                                </b> <b>€</b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
                                                    <span class="dib  old-pricedecoration">
                                                    <i></i>
                                                    <text class="js-pp-old">

                                                            98

                                                    </text>
                                                    </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Ancien prix</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:</div>
                                            <div class="dtable-cell">
                                                <b class='js-pp-ship'>

                                                    6

                                                </b>
                                                <b>€</b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full">

                                                        55

                                                    </text></b>€</div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button " data-package-id='1'></div>
                                <div class="img-wrapp"><img src="http://st.acstnst.com/content/Upsize_FR/imageSecond/1.png" alt="z1"></div>
                                <div class="zHeader"><span>&nbsp;</span></div>
                                <text>Soulagement immédiat de la lourdeur des jambes! Effet anti-varices après la première application!</text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 active">
                            <div class="zstick"></div>
                            <div class="zDiscount"><b>Attention</b>:
                                <br><span>Rabais de 50%</span></div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> paquets <span class="dib zplus">en cadeau</span></div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prix:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-new">

                                                        98

                                                    </text>€
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
                                                        <span class="dib  old-pricedecoration">
                                                            <i></i>
                                                            <span class="js-pp-old">

                                                                196

                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Ancien prix</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship">

                                                        6

                                                    </text>€
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <span class="js-pp-full">

                                                    104
                                                    </span>
                                                </b> €
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id='3'></div>
                                <div class="img-wrapp">
                                    <div class="gift">en cadeau</div><img src="http://st.acstnst.com/content/Upsize_FR/imageSecond/2+1.png" alt="z1"></div>
                                <div class="zHeader"><span>&nbsp;</span></div>
                                <text>La cure! Dissout les coagulums, enlève les varicosité! De belles et saines jambes aujourd’hui!</text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount"><b>Attention</b>:
                                <br><span>Rabais de 50%</span></div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> paquets <span class="dib zplus sec">en cadeau</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst"> Prix:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-new">

                                                        147

                                                    </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
                                                        <span class="dib  old-pricedecoration">
                                                            <i></i>
                                                            <text class="js-pp-old">

                                                                294

                                                            </text>
                                                        </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Ancien prix</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship">

                                                        6

                                                    </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full">

                                                        153

                                                    </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id='5'></div>
                                <div class="img-wrapp">
                                    <div class="gift th">en cadeau</div><img src="http://st.acstnst.com/content/Upsize_FR/imageSecond/3+2.png" alt="z1" width="200"></div>
                                <div class="zHeader sm"><span>&nbsp;</span></div>
                                <text>Une cure de 6 mois! Même un hôpital ne peut pas garantir un tel résultat! Élimination complète des varices! Effet garanti</text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>
    </div>

    </body>
    </html>
<?php } ?>