

var OutOffer={
	
	PreventExitSplash:false,
	msg1:'',
	msg2:'',
	targetUrl:'',
	debug:false,
	redirnow:false,
	closed:false,	
	iscookie:false,
	
	onshow: function(){},
	
	
	run:function()
	{
		if(OutOffer.getCookie('_zakup')!=undefined) return;
		
		setTimeout(OutOffer.tryredir, 100);
		OutOffer.debuger();
		OutOffer.disablelinksfunc();
		OutOffer.disableformsfunc();
		if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){ 
			 var ffversion=new Number(RegExp.$1) ;
			 if (ffversion>=26){
				 
				 OutOffer.addEvent(window, 'beforeunload',OutOffer.DisplayExitSplash2);
				 
				//window.onbeforeunload = OutOffer.DisplayExitSplash2;
				 //window.onmouseout=function(){document.location = OutOffer.targetUrl; console.log('test');};
				  //console.log('test');
				 // window.mouseout = 
			 }
		}
		else if(navigator.userAgent.toLowerCase().indexOf('opera') > -1)
		{
			//window.onbeforeunload = OutOffer.DisplayExitSplash2();
		}
		else
		window.onbeforeunload = OutOffer.DisplayExitSplash;
	},
	tryredir:function()
	{    
	    if (OutOffer.redirnow)
	    	document.location = OutOffer.targetUrl;
	    else
	    	setTimeout(OutOffer.tryredir, 100);
	},
	debuger:function(){
		
		if(OutOffer.debug==false) return;
		if(document.getElementById('OutOfferDebuger_5432426426')==null)
		{
        	var debugWindow=document.createElement("DIV");
			debugWindow.id='OutOfferDebuger_5432426426';
			debugWindow.style.color='#000000';
			debugWindow.style.backgroundColor='#FFFFFF';
			debugWindow.style.position='fixed';
			debugWindow.style.top='0px';
			debugWindow.style.right='0px';
			debugWindow.style.width='100px';
			debugWindow.innerHTML=(OutOffer.PreventExitSplash)?'TAK': 'NIE';
			document.body.appendChild(debugWindow)
        }
		OutOffer.addEvent(document, 'click', function() {
    		debugWindow.innerHTML=(OutOffer.PreventExitSplash)?'TAK': 'NIE';
        });
	},
	
	DisplayExitSplash:function() 
	{
	    if (OutOffer.PreventExitSplash == false) 
	    {
	        window.scrollTo(0, 0);
	        var ifrx=document.getElementById('ifrx');
	        if(ifrx==null){
	        	//tu powinno być tworzenie iframe
	        	ifrx=document.createElement("IFRAME");
	        	ifrx.style.display='none';
	        	ifrx.style.border='0px';
	        	ifrx.width='100%';
	        	ifrx.height='100%';
	        	ifrx.frameborder='no';
	        	document.body.appendChild(ifrx);
	        	
	        }
	        ifrx.style.position='absolute';
	        ifrx.style.top='0px';
	        ifrx.style.left='0px';
	        ifrx.src=OutOffer.targetUrl;
	        ifrx.style.display='inline';
	        
	        window.alert(OutOffer.msg1);
	        OutOffer.PreventExitSplash = true;
	        document.location.href = OutOffer.targetUrl;
	        OutOffer.redirnow = true;
	        OutOffer.onshow();
	        return OutOffer.msg2;
	    }
	},
	DisplayExitSplash2:function() 
	{
		if (OutOffer.PreventExitSplash == false) 
	    {
	        window.scrollTo(0, 0);
	        OutOffer.PreventExitSplash = true;
			OutOffer.redirnow = true;
			OutOffer.onshow();
			return document.location = OutOffer.targetUrl;
	    }
		
		
		
		
		
	},
	disablelinksfunc:function()
	{
		//console.log('disablelinksfunc');
		var a = document.getElementsByTagName('A');
		for (var i = 0; i < a.length; i++)
		{
			var href=a[i].href;
			if(OutOffer.debug){
				//console.log('HREF: '+href);
				console.log(a[i]);
			}
			if(a[i].href.trim()!='') // && (a[i].href.match(/javascript/g) || a[i].href.match(/^#/g))
			if(!a[i].href.match(/(#)|(javascript)/g))
			{
			    if (a[i].target !== '_blank') {
			    	OutOffer.addEvent(a[i], 'click', function() {
			    		if(OutOffer.debug) console.log('CLICK: '+a[i].href);
			    		OutOffer.PreventExitSplash = true;
			    		//if(OutOffer.debug) return false;
			        });
			    }
			    else {
			    	OutOffer.addEvent(a[i], 'click', function() {
			    		if(OutOffer.debug) alert('CLICK: '+a[i].href);
			    		OutOffer.PreventExitSplash = false;
			    		//if(OutOffer.debug) return false;
			        });
			    }
			}
		}
	},
	disableformsfunc:function()
	{
		//console.log('disableformsfunc');
		a = document.getElementsByTagName('FORM');
		for (var i = 0; i < a.length; i++) {
				OutOffer.addEvent(a[i], 'submit', function() {
					/*if(a[i].onsubmit != undefined)
					{
						if(a[i].onsubmit()==true) OutOffer.PreventExitSplash = true;
					}else 
						*/
					OutOffer.PreventExitSplash = true;
					//if(OutOffer.debug) return false;
		        });
		    }
		
	},
	addEvent2: function(obj, evt, fn) {
		if (obj.addEventListener) {
			obj.addEventListener(evt, fn, false);
		}
		else if (obj.attachEvent) {
			obj.attachEvent("on" + evt, fn);
		}
	},

	addEvent: function(ident,event,action)
	{
		if(ident.addEventListener)
		{
			ident.addEventListener(event,  function(evt){ var tmp=action(); }, false);
		} 
		else if(ident.attachEvent)
		{
			ident.attachEvent("on"+event, function(){ var tmp=action(); });
		}
		else
		{
			ident.addEventListener(event,  function(evt){ var tmp=action(); }, false);
		}
	},
	getCookie: function(name)
	{
		  var str = '; '+ document.cookie +';';
		  var index = str.indexOf('; '+ escape(name) +'=');
		  if (index != -1) {
		    index += name.length+3;
		    var value = str.slice(index, str.indexOf(';', index));
		    return unescape(value);
		  }
	}

}