<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com -->
<!--  Last Published: Sat Dec 17 2016 10:54:33 GMT+0000 (UTC)  -->
<html data-wf-page="5810971962cf032f7b79945a" data-wf-site="580634c7011fc44651f15e61">
<head>
    <meta charset="utf-8">
    <title>Vital-offer </title>
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <link href="/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="/css/webflow.css" rel="stylesheet" type="text/css">
    <link href="/css/leadbit.webflow.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
    <script type="text/javascript">
        WebFont.load({
            google: {
                families: ["Montserrat:400,700", "Roboto:300,regular,500"]
            }
        });
    </script>
    <script src="/js/modernizr.js" type="text/javascript"></script>

</head>
<body>
<div class="navigation-bar w-nav " style="padding-top: 0px" data-animation="default" data-collapse="medium"
     data-duration="400">

    <div style="width:50%;float:left;padding-top: 0px; padding-left:50px"><a class="" href="index.php"><img
                src="/images/logo.png" style="height:110px"></a>


    </div>
    <div style="width:50%;float:left;padding-top: 40px;padding-right: 50px">
        <div class="w-container">


            <nav class="navigation-menu w-nav-menu" role="navigation">
                <a class="navigation-link w-nav-link" href="/skin.html">Skin</a>
                <a class="navigation-link w-nav-link" href="/body.html">Body</a>
                <a class="navigation-link w-nav-link" href="/man.html">Man</a>
                <a class="navigation-link w-nav-link" href="/detox.html">Detox</a>

                <a class="navigation-link w-nav-link" href="/contact.html">Contact</a>

            </nav>
            <div class="hamburger-button w-nav-button">
                <div class="w-icon-nav-menu"></div>
            </div>
        </div>
    </div>
</div>


<div class="product-section ">
    <div class="w-container ">
        <div class="w-col w-col-9">
            <div class="product-row w-col w-col-12" style="padding-bottom: 20px;padding-top:20px">

                <div class=" w-col w-col-1"></div>
                <div class=" w-col w-col-10">
                    <div class="w-col w-col-4">
                        <img class="product" style=' float:right;padding-right:20px' height="200"
                             src="/images/psorifix.jpg">
                    </div>
                    <div class="w-col w-col-8">
                        <h2>PsoriFix

                        </h2>
                        <p class="pp-paragraph">Psorifix is a proriasis cream, which does not contain steroids. This is
                            an effective and skin-friendly cream for mild psoriasis.
                        </p>
                        <p class="pp-paragraph">How to use Psorifix?

                        </p>
                        <p class="pp-paragraph">Apply Psorifix to cleansed skin 2 times a day. Massage the product
                            gently into the affected areas.


                        </p>
                        <p class="pp-paragraph">- For Very Dry Skin<br>
                            - Natural Formula<br>
                            - Plant Based<br>
                            - 100 ml


                        </p>
                        <p class="pp-paragraph" style="text-align: left "> Available: <b>In Stock</b><br>
                            <b>Free Shipping!</b><br>
                            New price – Attractive discount</p>


                        <div class="w-col w-col-6">
                            <div class="product-review">Discount 50%</div>
                        </div>
                        <div class="w-col w-col-6"><a class="product-button w-button" style="margin-top: 0px"
                                                      href="/psori-fix.php" rel="nofollow">More info</a></div>

                    </div>


                </div>

                <!-- <div class="w-col w-col-3" style="text-align: right">

                 </div>-->
                <div class=" w-col w-col-1"></div>

            </div>
            <hr>
            <div class="product-row w-col w-col-12" style="padding-bottom: 20px;padding-top:20px">
                <div class=" w-col w-col-1"></div>
                <div class=" w-col w-col-10">
                    <div class="w-col w-col-4">
                        <img class="product" style=' float:right;padding-right:20px' height="200"
                             src="/images/slimmerspray.png">
                    </div>
                    <div class="w-col w-col-8">
                        <h2>Slimmer Spray


                        </h2>
                        <p class="pp-paragraph">Slimmer Spray is an aerosol which is full of natural ingredients and
                            vitamins. This product can care about your health conditiom and promote weight loss.
                        </p>
                        <p class="pp-paragraph"><b>What does Slimmer Spray contain?</b>
                            L-carnitine, Vitamin B1, Vitamin B6, Vitis Vinifera Extract


                        </p>
                        <p class="pp-paragraph"><b>How to use Slimmer Spray</b><br>
                            Slimmer Spray should be used approx. 10 application, four times a day.


                        </p>
                        <p class="pp-paragraph">- May facilitate weight loss<br>
                            - Contains B1 and B6 Vitamins<br>
                            - Only natural ingredients<br>
                            - 40 ml


                        </p>
                        <p class="pp-paragraph" style="text-align: left "> Available: <b>In Stock</b><br>
                            <b>Free Shipping!</b><br>
                            New price – Attractive discount</p>


                        <div class="w-col w-col-6">
                            <div class="product-review">Discount 50%</div>
                        </div>
                        <div class="w-col w-col-6"><a class="product-button w-button" style="margin-top: 0px"
                                                      href="/slimmer-spray.php" rel="nofollow">More info</a></div>

                    </div>


                </div>

                <div class=" w-col w-col-1"></div>

            </div>
            <hr>
            <div class="product-row w-col w-col-12" style="padding-bottom: 20px;padding-top:20px">
                <div class=" w-col w-col-1"></div>
                <div class=" w-col w-col-10">
                    <div class="w-col w-col-4">
                        <img class="product" style=' float:right;padding-right:20px' height="200"
                             src="/images/slimpectin.png">
                    </div>
                    <div class="w-col w-col-8">
                        <h2>Slim Pectin


                        </h2>
                        <p class="pp-paragraph">Slim Pectin is a combination of natural apple pectins and honey.
                            Hippocrates the Father of Western medicine used natural healing very successfully; his
                            favorite form of nutrition and natural medicines were Apple Cider Vinegar and Honey. Apple
                            pectin is a type of jelly, which has many benefits. This is a natural mix of carbohydrates.
                            Pectin can reduce a level of bad cholesterol and it is a natural appetite suppressant.
                        </p>
                        <p class="pp-paragraph"><b>Key ingredients:
                            </b>
                            Pectin, Honey


                        </p>
                        <p class="pp-paragraph"><b>How to use Slim Pectin?

                            </b><br>
                            Add Slim Pectin to 50 ml (water, milk or juice) – 1 package of pectins and 1 package of
                            honey. Then mix all the ingredients together.


                        </p>

                        <p class="pp-paragraph">- Apple Pectin + Honey<br>
                            - 15 servings<br>
                            - A healthy way to a new lifestyle<br>
                            - 300 g


                        </p>
                        <p class="pp-paragraph" style="text-align: left "> Available: <b>In Stock</b><br>
                            <b>Free Shipping!</b><br>
                            New price – Attractive discount</p>


                        <div class="w-col w-col-6">
                            <div class="product-review">Discount 50%</div>
                        </div>
                        <div class="w-col w-col-6"><a class="product-button w-button" style="margin-top: 0px"
                                                      href="/slim-pectin.php" rel="nofollow">More info</a></div>

                    </div>


                </div>
                <div class=" w-col w-col-1"></div>
            </div>
            <hr>
            <div class="product-row w-col w-col-12" style="padding-bottom: 20px;padding-top:20px">
                <div class=" w-col w-col-1"></div>
                <div class=" w-col w-col-10">
                    <div class="w-col w-col-4">
                        <img class="product" style=' float:right;padding-right:20px' height="200"
                             src="/images/prostaplast.jpg">
                    </div>
                    <div class="w-col w-col-8">
                        <h2>Prostaplast


                        </h2>
                        <p class="pp-paragraph">Prostaplast is a gift from Chinese medicine. These patches containing 7
                            Chinese herbal extracts that can help maintain male health on the highest level. Prostaplas
                            may increase libido and improve blood microcirculation.
                        </p>

                        <p class="pp-paragraph"><b>Key ingredients:
                            </b><br>
                            Borneol, Ligusticum Chuan Xlong, Achyrantes bidentata, Codydalis Amibgua, Cinnamon.


                        </p>
                        <p class="pp-paragraph">- May stimulate blood circulation<br>
                            - Helps to maintain good condition<br>
                            - Contains 8 herbal extracts<br>
                            - 5 patches


                        </p>
                        <p class="pp-paragraph" style="text-align: left "> Available: <b>In Stock</b><br>
                            <b>Free Shipping!</b><br>
                            New price – Attractive discount</p>


                        <div class="w-col w-col-6">
                            <div class="product-review">Discount 50%</div>
                        </div>
                        <div class="w-col w-col-6"><a class="product-button w-button" style="margin-top: 0px"
                                                      href="/prosta-plast.php" rel="nofollow">More info</a></div>

                    </div>


                </div>
                <div class=" w-col w-col-1"></div>

            </div>


            <hr>

            <div class="product-row w-col w-col-12" style="padding-bottom: 20px;padding-top:20px">
                <div class=" w-col w-col-1"></div>
                <div class=" w-col w-col-10">
                    <div class="w-col w-col-4">
                        <img class="product" style=' float:right;padding-right:20px' height="200"
                             src="/images/fixline.png">
                    </div>
                    <div class="w-col w-col-8">
                        <h2>Fixline Detox


                        </h2>
                        <p class="pp-paragraph">
                            Our body naturally eradicates toxins and chemicals through our kidneys, lungs, lymph, liver,
                            skin, and colon. However, because of the level of toxins we are exposed to on a daily basis
                            from our food, water, pollution and many other sources, our bodies find it very hard to keep
                            up. Regular cleansing is required in order for your body to remove all toxins and
                            impurities.

                        </p>


                        <p class="pp-paragraph">- Bestseller 2016<br>
                            - Only 30 ml a day<br>
                            - Natural ingredients

                        </p>
                        <p class="pp-paragraph" style="text-align: left "> Available: <b>In Stock</b><br>
                            <b>Free Shipping!</b><br>
                            New price – Attractive discount</p>


                        <div class="w-col w-col-6">
                            <div class="product-review">Discount 50%</div>
                        </div>
                        <div class="w-col w-col-6"><a class="product-button w-button" style="margin-top: 0px"
                                                      href="/fixline-detox.php" rel="nofollow">More info</a></div>

                    </div>


                </div>

                <div class=" w-col w-col-1"></div>

            </div>
            <hr>


        </div>
        <div class="w-col w-col-3">
            <b> Read our blog :</b><br>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 15px">
                <div class="w-col w-col-4" style="text-align: center">
                    <img src="/images/psoriasis.jpg">
                </div>
                <div class="w-col w-col-8">
                    <a href="/blog/psoriasis.html"> <span style="color: rgba(0, 0, 0, .74);"> New Psoriasis Treatment </span><br>read more </a><br>
                </div>

            </div>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 15px">
                <div class="w-col w-col-4" style="text-align: center">
                    <img src="/images/psio_nat.jpg" height="60">
                </div>
                <div class="w-col w-col-8">
                    <a href="/blog/psoriasis_nat.html"> <span style="color: rgba(0, 0, 0, .74);"> Treating Psoriasis Naturally </span><br>read more </a><br>

                </div>

            </div>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 15px">
                <div class="w-col w-col-4" style="text-align: center">
                    <img src="/images/honey.jpg" height="60">

                </div>
                <div class="w-col w-col-8" >
                    <a href="/blog/honey.html"> <span style="color: rgba(0, 0, 0, .74);">Honey Health Benefits</span><br> read more
                    </a><br>

                </div>

            </div>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 15px">
                <div class="w-col w-col-4" style="text-align: center">
                    <img src="/images/cellulite.jpg" height="60">

                </div>
                <div class="w-col w-col-8">
                    <a href="/blog/cellulite.html">  <span style="color: rgba(0, 0, 0, .74);">Cellulite Reduction </span><br> read more
                    </a><br>

                </div>

            </div>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 15px">
                <div class="w-col w-col-4" style="text-align: center">
                    <img src="/images/laser..jpg" height="60">

                </div>
                <div class="w-col w-col-8">
                    <a href="/blog/laser.html"> <span style="color: rgba(0, 0, 0, .74);"> Laser Treatment for Cellulite </span><br> read more

                    </a><br>

                </div>

            </div>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 15px">
                <div class="w-col w-col-4" style="text-align: center">
                    <img src="/images/detox.png" height="60">

                </div>
                <div class="w-col w-col-8">
                    <a href="/blog/l-carnityne.html"> <span style="color: rgba(0, 0, 0, .74);"> L-Carnitine Benefits </span><br> read more


                    </a>

                </div>

            </div>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 15px">
                <div class="w-col w-col-4" style="text-align: center">
                    <img src="/images/detox_2.jpg" height="60">

                </div>
                <div class="w-col w-col-8">
                    <a href="/blog/detox.html">  <span style="color: rgba(0, 0, 0, .74);">How To Detox Your Body Naturally</span> <br> read more
                    </a><br>

                </div>

            </div>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 15px">
                <div class="w-col w-col-4" style="text-align: center">
                    <img src="/images/natur_detox.jpg" height="60">

                </div>
                <div class="w-col w-col-8">
                    <a href="/blog/detox_nat.html"> <span style="color: rgba(0, 0, 0, .74);"> Natural Detox Diet</span> <br> read more

                    </a>

                </div>

            </div>
            <hr>





        </div>
    </div>


    <div class="footer">

        <div class="footer-text"><a href="/policy.html"> Privacy policy</a> | <a href="/terms.html">Terms of Use </a>| <a
                href="https://www.facebook.com/Vital-Offercom-1846073569002006/" target="_blank">Find us on
                Facebook </a> <br>© 2016 vital-offer.com
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
    <script src="/js/webflow.js" type="text/javascript"></script>
    <!-- [if lte IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>