var url_ajax="./getdata.php";
$(document).ready(function () {
    var country_list = $('#country_list').val();

    $.ajax({
        url: url_ajax,
        data: {
            'country_list': country_list
        },
        success: function (data) {
            result = JSON.parse(data);
            $.each(result, function (index, val) {
                if(index == 'spoken_lang'){
                    return;
                }
                $('select[name="country_code"]').append($("<option></option>")
                    .attr("value", val[1])
                    .text(val[2]));
                    if(index > 0){
                        $('select[name="country_code"]').show();
                    }
                if (index == 0) {
                    $('input[name="name"]').attr('placeholder', result['spoken_lang'][4]);
                    $('input[name="phone"]').attr('placeholder', result['spoken_lang'][6]);
                    $('.order_now_btn').text(result['spoken_lang'][3])
                    name_hint = val[5];
                    phone_hint = val[7];
                    no_name=result['spoken_lang'][8];
                    no_phone=result['spoken_lang'][9];
                }


            })

        }
    });

    $('.order_now_btn').on('click',function(event){
        event.preventDefault();
        event.stopPropagation();

        if($('input[name=name]').val()=='') {
            show_form_hint($('input[name=name]'), no_name);
            return false;
        }

        if($('input[name=phone]').val()==''){
            show_form_hint($('input[name=phone]'), no_phone);

            return false;
        }

        $('form').submit();

    })


    $('input[name=name]').on('touched click ', function (e) {

        if (name_hint != '') {
            show_form_hint(this, name_hint);

            return false;
        }
    });

    $('input[name=phone]').on('touched click ', function (e) {

        if (phone_hint != '') {
            show_form_hint(this, phone_hint);

            return false;
        }
    });
    $('.only_number').on('keydown', function (event) {

        if (event.shiftKey && (event.which != 61 && event.which != 187 && event.which != 56 && event.which != 51)) {
            event.preventDefault()
            event.stopPropagation();
        } else if ((event.shiftKey && event.which == 187)) {
            return;
        } else if ((event.shiftKey && event.which == 61)) {

            return;

        } else {


            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 32 ||
                (event.keyCode == 65 && event.ctrlKey === true) || event.keyCode == 51 || event.keyCode == 56 ||


                (event.keyCode >= 35 && event.keyCode <= 39)


            ) {
                return;
            }
            else {
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) || event.keyCode == 187 || event.keyCode == 61 ) {

                    event.preventDefault();
                    event.stopPropagation();
                }
            }
        }
    });


    function show_form_hint(elem, msg) {
        $(".js_errorMessage").remove();
        jQuery('<div class="js_errorMessage">' + msg + '</div>').appendTo('body').css({
            'left': jQuery(elem).offset().left,
            'top': jQuery(elem).offset().top - 40,
            'background-color': '#e74c3c',
            'border': '1px dashed black',
            'border-radius': '5px',
            'color': '#fff',
            'font-family': 'Arial',
            'font-size': '14px',
            'margin': '3px 0 0 0px',
            'padding': '6px 5px 5px',
            'position': 'absolute',
            'z-index': '9999'
        });
        jQuery(elem).focus();
    };

    $('select[name="country_code"]').on('change', function (e) {

        console.log($(this).val());

        country_code = $(this).val();
        $.ajax({
            url: url_ajax,
            data: {
                'country_list': country_code
            },
            success: function (data) {

                result = JSON.parse(data);
                $.each(result, function (index, val) {
                    if(index == 'spoken_lang'){
                        return;
                    }
                    name_hint = val[5];
                    phone_hint = val[7];


                });


            }
        });

    })

});