<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7749 -->
        <script>var locale = "lv";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALXEwRNvl6IAAEAAQACPxMBAAJFHgK-AQEABApSQnoA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Gokhan Karakoc';
            var phone_hint = '+105469655610';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("lv");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
        <title>Chokolate Slim</title>
        <link href="//st.acstnst.com/content/Chocolate_slim_LV/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Noto+Serif:700italic&amp;subset=greek-ext,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Chocolate_slim_LV/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="hide-it">
        <!-- <header> -->
        <div id="header">
            <div class="width">
                <div class="block">
                    <h1>Chocolate Slim</h1>
                    <h2>Dabisks notievēšanas komplekss</h2>
                    <div class="list">
                        Ātri samazina lieko svaru<br/>
                        Cīnās ar celulītu<br/>
                        Atbrīvo ādu no izsitumiem un pinnēm
                    </div>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">Karstais piedāvājums! 50% atlaide!</div>
                    </div>
                    <div class="price">
                        <del> 48  €</del>
                        <div> 24 €</div>
                    </div>
                    <a class="button" href="#footer">Pasūtījums</a>
                </div>
            </div>
        </div>
        <!-- </header> -->
        <!-- <steps> -->
        <div id="steps">
            <div class="width">
                <div class="title">Kā zaudēt 10 kg?</div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico1.jpg"/>
                    </div>
                    <p>2-4 nedēļu kurss</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico2.jpg"/>
                    </div>
                    <p>Dzeriet to no rīta</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico3.jpg"/>
                    </div>
                    <p>Ļaujiet ievilkties krūzē</p>
                </div>
                <div class="clear"></div>
                <div class="text">
                    <div class="title">Chocolate Slim</div>
                    <p>Notievēšanas kompleksā esošie ingridienti līdzsvaro viens otra iedarbību. <br/>Rezultāti pārspēj visas cerības!</p>
                </div>
            </div>
        </div>
        <!-- </steps> -->
        <!-- <sostav> -->
        <div id="sostav">
            <div class="width">
                <div class="title">Kāpēc Chocolate Slim <br/>ir tik ārkārtīgi efektīvs?</div>
                <h2>6 dabiskas sastāvdaļas</h2>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img1.png"/>
                    <div class="message">
                        <div>Zaļās kafijas pupiņas</div>
                        <p>Nomāc ēstgribu <br/>Uzmundrina un uzlādē ar enerģīju.</p>
                    </div>
                </div>
                <div class="item first right">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img2.png"/>
                    <div class="message">
                        <div>Godži ogas</div>
                        <p>Dabisks tauku dedzinātājs. <br/>Aptur tauku uzkrāšānos.</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item second">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img3.png"/>
                    <div class="message">
                        <div>Akai ogas</div>
                        <p>Satur cianidīnu kurš aptur <br/>tauku šūnu veidošanos. <br/>Dabisks antioksidantu avots.</p>
                    </div>
                </div>
                <div class="item second right">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img4.png"/>
                    <div class="message">
                        <div>Čī sēklas</div>
                        <p>Uzlādē ar enerģiju <br/>Novērš tauku uzkrāšanos <br/>Problemātiskajās ķermeņa zonās.</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item third">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img5.png"/>
                    <div class="message">
                        <div>Dabisks kakao</div>
                        <p>Paātrina tauku oksidēšanos. <br/>Nostiprina imūno sistēmu tievēšanas laikā. <br/>Slāpē kāri pēc saldumiem. <br/>Ražo dofamīnu (prieka hormonu).</p>
                    </div>
                </div>
                <div class="item third right">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img6.png"/>
                    <div class="message">
                        <div>Lingzhi sēņu ekstrakts</div>
                        <p>Normalizē metabolismu <br/>Uzlabo kopējo organisma funkcionēšanu <br/>Samazina holesterina līmeni asinīs.</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="plaha">Organisks</div>
            </div>
        </div>
        <!-- </sostav> -->
        <!-- <result> -->
        <div id="result">
            <div class="width">
                <div class="title">Resultāts: ar Chocolate Slim notievēs ikviens!</div>
                <h2>Vairāk nekā 7000 laimīgas sievietes,kurām ir izdevies atbrīvoties no liekā svara!</h2>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img7.jpg"/>
                    <p>Liekie kilogrami pazūdburtiski jūsu acu priekšā</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img8.jpg"/>
                    <p>Jums negribās ēst</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img9.jpg"/>
                    <p>Jūs esat enerģiski</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img10.jpg"/>
                    <p>Jūs vairs neizjūtat kāri pēc saldumiem</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img11.jpg"/>
                    <p>Labs garastāvoklis</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </result> -->
        <!-- <info-block> -->
        <div id="info-block">
            <div class="width">
                <div class="title">Notievēt vel nekad nav bijis <br/>tik vienkārši!</div>
                <p>
                    Ar  Chocolate Slim kompleksu jūs varat zaudēt līdz pat 24 kg mēneša laikā. Tievējot jūs būsiet labā garastāvoklī un enerģijas pilna! Izbaudiet to, kā jūs tievējat! Chocolate ir unikāls produkts, kuru jūs gribēsiet vēl un vēl.
                    <br/><br/>
                    Produkts nav uztura bagātinātājs, tā sastāvā ir tikai dabīgi ingredienti. Pamatsastāvdaļas ir: dabisks kakao, godžī ogas, zaļā kafija un linghzi sēņu ekstrakts. Produktam nav kontrindikācijas un tas ir piemērots lietošanai katru dienu. Tas novērš liekā svara veidošanās cēloni un normalizē vielmaiņu procesu organismā.
                    <br/><br/>
                    Tas ir patiešām revolucionārs produktsfiguraskorekcijas, veselības un organisma atjaunošanās sfērā.
                </p>
            </div>
        </div>
        <!-- </info-block> -->
        <!-- <compare> -->
        <div id="compare">
            <div class="width">
                <div class="title">Chocolate Slim salīdzinājums ar parastām <br/>tievēšanas metodēm</div>
                <h2>Mēs noitervējām 15 sievietes, kuras vēlējās notievēt un rezultāti runā paši par sevi:</h2>
                <div class="wr">
                    <div class="text1">Chokolate Slim<span>(Nemainot savu dzīvesveidu)</span></div>
                    <ul class="text2 clearfix">
                        <li>kg</li>
                        <li>1 nedēļa</li>
                        <li>2 нnedēļas</li>
                        <li>3 nedēļas</li>
                        <li>4 nedēļas</li>
                    </ul>
                    <div class="text3">
                        <span>Fiziskā slodze 3 reizes nedēļā</span>
                        <span>Notievēšanas medikamenti</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- </compare> -->
        <!-- <reviews> -->
        <div id="reviews">
            <div class="width">
                <div class="title">Pajautājiet tiem, kuri jau ir zaudējuši lieko svaru ar Chocolate Slim</div>
                <h2>Reāli cilvēki – reāls rezultāts!</h2>
                <div class="os">
                    <div class="review">
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ava1.jpg"/>
                        <div class="date">
                            <div><script>dtime_nums(-60, true)</script></div>
                            <div><script>dtime_nums(-10, true)</script></div>
                        </div>
                        <div class="name">Santa Muceniete, 25 gadi</div>
                        <div class="email">mucen*****@gmail.com</div>
                        <p>Tas patiešām ir kaut kas vienreizējs! Esmu zaudējusi gandrīz 18 kg primajās trijās nedēļās! Un turpmāko divu nedēļu laikā vēl 7 kg! Galvenais noteikums - strikti ievērot lietošanas instrukciju: 1 dzēriena krūze vieglu brokastu laikā vai to vietā, regulāri katru dienu. Protams, jums nebūtu jāēd konditorejas izstrādājumus pirms gulētiešanas. Ja es to varēju, jūs arī varēsiet!</p>
                    </div>
                    <div></div>
                    <div class="review center">
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ava2.jpg"/>
                        <div class="date">
                            <div><script>dtime_nums(-70, true)</script></div>
                            <div><script>dtime_nums(-4, true)</script></div>
                        </div>
                        <div class="name">Janis Podskochiy, 44 gadi</div>
                        <div class="email">janis*****@gmail.com</div>
                        <p>Mana sieva nopirka man Chocolate Slim pēc tam, kad biju izmēģinājis daudz dažādu līdzekļu notievēšanai– dažāda veida teijas un tabletes. Tikai 1,5 mēnešos esmu zaudējis 12 kg un nēesmu noskaņots apstāties.Paldies!</p>
                    </div>
                    <div></div>
                    <div class="review">
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ava3.jpg"/>
                        <div class="date">
                            <div><script>dtime_nums(-30, true)</script></div>
                            <div><script>dtime_nums(-2, true)</script></div>
                        </div>
                        <div class="name">Katrina Velis, 32 gadi</div>
                        <div class="email">velis*****@gmail.com</div>
                        <p>Tas ir brīnišķīgi, ka Chocolate Slim beidzot var iegādāties arī Latvijā! Biju daudz lasījusi par šo produktu dažādos forumos, un beidzot esmu izmēģinājusi to arī pati. Tikai iedomājaties, es dzeru Chocolate tikai 2 nedēļas un jau varu uzvilkts vecās džinsa bikses, ko es nevarēju izdarīt 4 gadus:)</p>
                    </div>
                </div>
                <div class="buy">
                    <div>Nav ko domāt, sāciet tievēt uzreiz!</div>
                    <a class="button" href="#footer">Pasūtiet</a>
                </div>
            </div>
        </div>
        <!-- </reviews> -->
        <!-- <info-block2> -->
        <div id="info-block2">
            <div class="width">
                <div class="left">
                    <div class="title">
                        <div>Vai tas nav kaitīgs?</div>
                    </div>
                    <p>Chocolate Slim sastāvā ir tikai dabiskas komponentes, bez parabēniem, krāsvielām, sintētiskajiem aromatizētājiem vai ģenētiskie modificētajiem produktiem.   <br/>
                        Izlasiet norādījumus.</p>
                </div>
                <div class="right">
                    <div class="title">
                        <div>Kāds produkta daudzums ir nepieciešams <br/>Lai zaudētu 10 kg?</div>
                    </div>
                    <p>Klīniskie izmēģinājumi ir pierādījuši, ka parasti pietiek ar viena mēneša kursu, lai sasniegtu vēlamo rezultātu. Pirmos rezultātus jūs varēsiet sākt novērot sākot jau ar trešo produkta lietošanas dienu!</p>
                </div>
                <div class="left">
                    <div class="title">
                        <div>Kur iegādāties?</div>
                    </div>
                    <p>In Latvijā mūsu kompānija ir vienīgais oficiālais Chocolate Slim izplatītājs! Visi pārējie ir pakaļdarinājumi! Jūs varat pasūtīt Chocolate Slim tikai un vienīgi  mūsu mājaslapā.</p>
                </div>
                <div class="right">
                    <div class="title">
                        <div>Uzmanaties <br/>no pakaļdarinājumiem!</div>
                    </div>
                    <p>Klīniskie izmēģinājumi ir pierādījuši, ka parasti pietiek ar viena mēneša kursu, lai sasniegtu vēlamo rezultātu. Pirmos rezultātus jūs varēsiet sākt novērot sākot jau ar trešo produkta lietošanas dienu!</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </info-block2> -->
        <!-- <how-buy> -->
        <div id="how-buy">
            <div class="width">
                <div class="title">Kā pasūtīt Chocolate Slim?</div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico4.jpg"/>
                    <p>Aizpildiet formu mūsu mājaslapā</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico5.jpg"/>
                    <p>Sagaidiet, kad ar jums sazināsies mūsu operātors</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico6.jpg"/>
                    <p>Saņemiet sūtījumu tuvākajā pasta nodaļā</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <how-buy> -->
        <div>
            <div class="width">
                <div id="howuse">
                    <h2>Lietot <b>Chokolate Slim</b> Ir tik viegli!</h2>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Chocolate_slim_LV/img/hu_step1.jpg"/>
                        <p class="hu_title">PAGATAVOŠĀNA</p>
                        <p><b>Pagatavojiet kokteili katru rītu izmantojot 250 ml piena</b> Deva sievetēm: 1-2 pulvera karotītes, deva vīriešiem: 2-3 karotītes.</p>
                    </div>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Chocolate_slim_LV/img/hu_step2.jpg"/>
                        <p class="hu_title">LIETOŠANA</p>
                        <p><b>Lietojiet kokteili brokastu vietā</b>, un jūs saņemsiet: 217 kcal, 10g ogļhidrātus, 17g proteīnus, 23 mikroelimentus un vitamīnus, šķiedras.</p>
                    </div>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Chocolate_slim_LV/img/hu_step3.jpg"/>
                        <p class="hu_title">ATKĀRTOJIET</p>
                        <p>dienas gaitā <b>aizvietojiet citu ēdienreizi ar kokteili</b>, rezultāta nostiprināšanai.</p>
                    </div>
                    <p style="clear: both; float: none; width: 0; height: 0;"></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <footer> -->
        <div id="footer">
            <div class="width">
                <div class="left">
                    <h1>Chocolate Slim</h1>
                    <h2>Dabīgs tievēšanas komplekss</h2>
                    <img alt="" class="prod" src="//st.acstnst.com/content/Chocolate_slim_LV/img/prod.png"/>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">Karstais piedāvājums! 50% atlaide!</div>
                    </div>
                    <div class="price">
                        <del> 48  €</del>
                        <div> 24  €</div>
                    </div>
                </div>
                <div class="right">
                    <div class="time">
                        <div class="name-t">Piedāvājums ir spēkā:</div>
                        <div id="timer"></div>
                    </div>
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIALXEwAAAAAAAAAAAASWUD7KAA"></iframe>


                      <!--  <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="24.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIALXEwRNvl6IAAEAAQACPxMBAAJFHgK-AQEABApSQnoA">
                        <input type="hidden" name="goods_id" value="109">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Chocolate_slim_LV">
                        <input type="hidden" name="price" value="24">
                        <input type="hidden" name="old_price" value="48">
                        <input type="hidden" name="al" value="7749">
                        <input type="hidden" name="total_price_wo_shipping" value="24.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="LV">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <select class="country_select" id="country_code_selector" name="country">
                            <option value="LV">Latvija</option>
                        </select>
                        <input name="name" placeholder="Vārds, Uzvārds" type="text" value=""/>
                        <input class="only_number" name="phone" placeholder="Telefons" type="text" value=""/>
                        <!--div class="prices none">
                        <div>Piegāde: 0 €

                                                </div>
                        <div>Kopējā cena: 24 €
                                                </div>
                        </div-->
                      <!--  <input class="button js_submit" type="submit" value="Pasūtīt"/>-->
                    </form>
                </div>
            </div>
        </div>
        <!-- </footer> -->
    </div>
    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7749 -->
        <script>var locale = "lv";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALXEwRNvl6IAAEAAQACPxMBAAJFHgK-AQEABApSQnoA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Gokhan Karakoc';
            var phone_hint = '+105469655610';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("lv");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
        <title>Chokolate Slim</title>
        <link href="//st.acstnst.com/content/Chocolate_slim_LV/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Noto+Serif:700italic&amp;subset=greek-ext,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Chocolate_slim_LV/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="hide-it">
        <!-- <header> -->
        <div id="header">
            <div class="width">
                <div class="block">
                    <h1>Chocolate Slim</h1>
                    <h2>Dabisks notievēšanas komplekss</h2>
                    <div class="list">
                        Ātri samazina lieko svaru<br/>
                        Cīnās ar celulītu<br/>
                        Atbrīvo ādu no izsitumiem un pinnēm
                    </div>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">Karstais piedāvājums! 50% atlaide!</div>
                    </div>
                    <div class="price">
                        <del> 48  €</del>
                        <div> 24 €</div>
                    </div>
                    <a class="button" href="#footer">Pasūtījums</a>
                </div>
            </div>
        </div>
        <!-- </header> -->
        <!-- <steps> -->
        <div id="steps">
            <div class="width">
                <div class="title">Kā zaudēt 10 kg?</div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico1.jpg"/>
                    </div>
                    <p>2-4 nedēļu kurss</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico2.jpg"/>
                    </div>
                    <p>Dzeriet to no rīta</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico3.jpg"/>
                    </div>
                    <p>Ļaujiet ievilkties krūzē</p>
                </div>
                <div class="clear"></div>
                <div class="text">
                    <div class="title">Chocolate Slim</div>
                    <p>Notievēšanas kompleksā esošie ingridienti līdzsvaro viens otra iedarbību. <br/>Rezultāti pārspēj visas cerības!</p>
                </div>
            </div>
        </div>
        <!-- </steps> -->
        <!-- <sostav> -->
        <div id="sostav">
            <div class="width">
                <div class="title">Kāpēc Chocolate Slim <br/>ir tik ārkārtīgi efektīvs?</div>
                <h2>6 dabiskas sastāvdaļas</h2>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img1.png"/>
                    <div class="message">
                        <div>Zaļās kafijas pupiņas</div>
                        <p>Nomāc ēstgribu <br/>Uzmundrina un uzlādē ar enerģīju.</p>
                    </div>
                </div>
                <div class="item first right">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img2.png"/>
                    <div class="message">
                        <div>Godži ogas</div>
                        <p>Dabisks tauku dedzinātājs. <br/>Aptur tauku uzkrāšānos.</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item second">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img3.png"/>
                    <div class="message">
                        <div>Akai ogas</div>
                        <p>Satur cianidīnu kurš aptur <br/>tauku šūnu veidošanos. <br/>Dabisks antioksidantu avots.</p>
                    </div>
                </div>
                <div class="item second right">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img4.png"/>
                    <div class="message">
                        <div>Čī sēklas</div>
                        <p>Uzlādē ar enerģiju <br/>Novērš tauku uzkrāšanos <br/>Problemātiskajās ķermeņa zonās.</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item third">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img5.png"/>
                    <div class="message">
                        <div>Dabisks kakao</div>
                        <p>Paātrina tauku oksidēšanos. <br/>Nostiprina imūno sistēmu tievēšanas laikā. <br/>Slāpē kāri pēc saldumiem. <br/>Ražo dofamīnu (prieka hormonu).</p>
                    </div>
                </div>
                <div class="item third right">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img6.png"/>
                    <div class="message">
                        <div>Lingzhi sēņu ekstrakts</div>
                        <p>Normalizē metabolismu <br/>Uzlabo kopējo organisma funkcionēšanu <br/>Samazina holesterina līmeni asinīs.</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="plaha">Organisks</div>
            </div>
        </div>
        <!-- </sostav> -->
        <!-- <result> -->
        <div id="result">
            <div class="width">
                <div class="title">Resultāts: ar Chocolate Slim notievēs ikviens!</div>
                <h2>Vairāk nekā 7000 laimīgas sievietes,kurām ir izdevies atbrīvoties no liekā svara!</h2>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img7.jpg"/>
                    <p>Liekie kilogrami pazūdburtiski jūsu acu priekšā</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img8.jpg"/>
                    <p>Jums negribās ēst</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img9.jpg"/>
                    <p>Jūs esat enerģiski</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img10.jpg"/>
                    <p>Jūs vairs neizjūtat kāri pēc saldumiem</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/img11.jpg"/>
                    <p>Labs garastāvoklis</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </result> -->
        <!-- <info-block> -->
        <div id="info-block">
            <div class="width">
                <div class="title">Notievēt vel nekad nav bijis <br/>tik vienkārši!</div>
                <p>
                    Ar  Chocolate Slim kompleksu jūs varat zaudēt līdz pat 24 kg mēneša laikā. Tievējot jūs būsiet labā garastāvoklī un enerģijas pilna! Izbaudiet to, kā jūs tievējat! Chocolate ir unikāls produkts, kuru jūs gribēsiet vēl un vēl.
                    <br/><br/>
                    Produkts nav uztura bagātinātājs, tā sastāvā ir tikai dabīgi ingredienti. Pamatsastāvdaļas ir: dabisks kakao, godžī ogas, zaļā kafija un linghzi sēņu ekstrakts. Produktam nav kontrindikācijas un tas ir piemērots lietošanai katru dienu. Tas novērš liekā svara veidošanās cēloni un normalizē vielmaiņu procesu organismā.
                    <br/><br/>
                    Tas ir patiešām revolucionārs produktsfiguraskorekcijas, veselības un organisma atjaunošanās sfērā.
                </p>
            </div>
        </div>
        <!-- </info-block> -->
        <!-- <compare> -->
        <div id="compare">
            <div class="width">
                <div class="title">Chocolate Slim salīdzinājums ar parastām <br/>tievēšanas metodēm</div>
                <h2>Mēs noitervējām 15 sievietes, kuras vēlējās notievēt un rezultāti runā paši par sevi:</h2>
                <div class="wr">
                    <div class="text1">Chokolate Slim<span>(Nemainot savu dzīvesveidu)</span></div>
                    <ul class="text2 clearfix">
                        <li>kg</li>
                        <li>1 nedēļa</li>
                        <li>2 нnedēļas</li>
                        <li>3 nedēļas</li>
                        <li>4 nedēļas</li>
                    </ul>
                    <div class="text3">
                        <span>Fiziskā slodze 3 reizes nedēļā</span>
                        <span>Notievēšanas medikamenti</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- </compare> -->
        <!-- <reviews> -->
        <div id="reviews">
            <div class="width">
                <div class="title">Pajautājiet tiem, kuri jau ir zaudējuši lieko svaru ar Chocolate Slim</div>
                <h2>Reāli cilvēki – reāls rezultāts!</h2>
                <div class="os">
                    <div class="review">
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ava1.jpg"/>
                        <div class="date">
                            <div><script>dtime_nums(-60, true)</script></div>
                            <div><script>dtime_nums(-10, true)</script></div>
                        </div>
                        <div class="name">Santa Muceniete, 25 gadi</div>
                        <div class="email">mucen*****@gmail.com</div>
                        <p>Tas patiešām ir kaut kas vienreizējs! Esmu zaudējusi gandrīz 18 kg primajās trijās nedēļās! Un turpmāko divu nedēļu laikā vēl 7 kg! Galvenais noteikums - strikti ievērot lietošanas instrukciju: 1 dzēriena krūze vieglu brokastu laikā vai to vietā, regulāri katru dienu. Protams, jums nebūtu jāēd konditorejas izstrādājumus pirms gulētiešanas. Ja es to varēju, jūs arī varēsiet!</p>
                    </div>
                    <div></div>
                    <div class="review center">
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ava2.jpg"/>
                        <div class="date">
                            <div><script>dtime_nums(-70, true)</script></div>
                            <div><script>dtime_nums(-4, true)</script></div>
                        </div>
                        <div class="name">Janis Podskochiy, 44 gadi</div>
                        <div class="email">janis*****@gmail.com</div>
                        <p>Mana sieva nopirka man Chocolate Slim pēc tam, kad biju izmēģinājis daudz dažādu līdzekļu notievēšanai– dažāda veida teijas un tabletes. Tikai 1,5 mēnešos esmu zaudējis 12 kg un nēesmu noskaņots apstāties.Paldies!</p>
                    </div>
                    <div></div>
                    <div class="review">
                        <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ava3.jpg"/>
                        <div class="date">
                            <div><script>dtime_nums(-30, true)</script></div>
                            <div><script>dtime_nums(-2, true)</script></div>
                        </div>
                        <div class="name">Katrina Velis, 32 gadi</div>
                        <div class="email">velis*****@gmail.com</div>
                        <p>Tas ir brīnišķīgi, ka Chocolate Slim beidzot var iegādāties arī Latvijā! Biju daudz lasījusi par šo produktu dažādos forumos, un beidzot esmu izmēģinājusi to arī pati. Tikai iedomājaties, es dzeru Chocolate tikai 2 nedēļas un jau varu uzvilkts vecās džinsa bikses, ko es nevarēju izdarīt 4 gadus:)</p>
                    </div>
                </div>
                <div class="buy">
                    <div>Nav ko domāt, sāciet tievēt uzreiz!</div>
                    <a class="button" href="#footer">Pasūtiet</a>
                </div>
            </div>
        </div>
        <!-- </reviews> -->
        <!-- <info-block2> -->
        <div id="info-block2">
            <div class="width">
                <div class="left">
                    <div class="title">
                        <div>Vai tas nav kaitīgs?</div>
                    </div>
                    <p>Chocolate Slim sastāvā ir tikai dabiskas komponentes, bez parabēniem, krāsvielām, sintētiskajiem aromatizētājiem vai ģenētiskie modificētajiem produktiem.   <br/>
                        Izlasiet norādījumus.</p>
                </div>
                <div class="right">
                    <div class="title">
                        <div>Kāds produkta daudzums ir nepieciešams <br/>Lai zaudētu 10 kg?</div>
                    </div>
                    <p>Klīniskie izmēģinājumi ir pierādījuši, ka parasti pietiek ar viena mēneša kursu, lai sasniegtu vēlamo rezultātu. Pirmos rezultātus jūs varēsiet sākt novērot sākot jau ar trešo produkta lietošanas dienu!</p>
                </div>
                <div class="left">
                    <div class="title">
                        <div>Kur iegādāties?</div>
                    </div>
                    <p>In Latvijā mūsu kompānija ir vienīgais oficiālais Chocolate Slim izplatītājs! Visi pārējie ir pakaļdarinājumi! Jūs varat pasūtīt Chocolate Slim tikai un vienīgi  mūsu mājaslapā.</p>
                </div>
                <div class="right">
                    <div class="title">
                        <div>Uzmanaties <br/>no pakaļdarinājumiem!</div>
                    </div>
                    <p>Klīniskie izmēģinājumi ir pierādījuši, ka parasti pietiek ar viena mēneša kursu, lai sasniegtu vēlamo rezultātu. Pirmos rezultātus jūs varēsiet sākt novērot sākot jau ar trešo produkta lietošanas dienu!</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </info-block2> -->
        <!-- <how-buy> -->
        <div id="how-buy">
            <div class="width">
                <div class="title">Kā pasūtīt Chocolate Slim?</div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico4.jpg"/>
                    <p>Aizpildiet formu mūsu mājaslapā</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico5.jpg"/>
                    <p>Sagaidiet, kad ar jums sazināsies mūsu operātors</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolate_slim_LV/img/ico6.jpg"/>
                    <p>Saņemiet sūtījumu tuvākajā pasta nodaļā</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <how-buy> -->
        <div>
            <div class="width">
                <div id="howuse">
                    <h2>Lietot <b>Chokolate Slim</b> Ir tik viegli!</h2>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Chocolate_slim_LV/img/hu_step1.jpg"/>
                        <p class="hu_title">PAGATAVOŠĀNA</p>
                        <p><b>Pagatavojiet kokteili katru rītu izmantojot 250 ml piena</b> Deva sievetēm: 1-2 pulvera karotītes, deva vīriešiem: 2-3 karotītes.</p>
                    </div>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Chocolate_slim_LV/img/hu_step2.jpg"/>
                        <p class="hu_title">LIETOŠANA</p>
                        <p><b>Lietojiet kokteili brokastu vietā</b>, un jūs saņemsiet: 217 kcal, 10g ogļhidrātus, 17g proteīnus, 23 mikroelimentus un vitamīnus, šķiedras.</p>
                    </div>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Chocolate_slim_LV/img/hu_step3.jpg"/>
                        <p class="hu_title">ATKĀRTOJIET</p>
                        <p>dienas gaitā <b>aizvietojiet citu ēdienreizi ar kokteili</b>, rezultāta nostiprināšanai.</p>
                    </div>
                    <p style="clear: both; float: none; width: 0; height: 0;"></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <footer> -->
        <div id="footer">
            <div class="width">
                <div class="left">
                    <h1>Chocolate Slim</h1>
                    <h2>Dabīgs tievēšanas komplekss</h2>
                    <img alt="" class="prod" src="//st.acstnst.com/content/Chocolate_slim_LV/img/prod.png"/>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">Karstais piedāvājums! 50% atlaide!</div>
                    </div>
                    <div class="price">
                        <del> 48  €</del>
                        <div> 24  €</div>
                    </div>
                </div>
                <div class="right">
                    <div class="time">
                        <div class="name-t">Piedāvājums ir spēkā:</div>
                        <div id="timer"></div>
                    </div>
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIALXEwAAAAAAAAAAAASWUD7KAA"></iframe>

                      <!--  <input type="hidden" name="accept_languages" value="pl-pl">
                        <input type="hidden" name="total_price" value="24.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIALXEwRNvl6IAAEAAQACPxMBAAJFHgK-AQEABApSQnoA">
                        <input type="hidden" name="goods_id" value="109">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Chocolate_slim_LV">
                        <input type="hidden" name="price" value="24">
                        <input type="hidden" name="old_price" value="48">
                        <input type="hidden" name="al" value="7749">
                        <input type="hidden" name="total_price_wo_shipping" value="24.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="LV">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <select class="country_select" id="country_code_selector" name="country">
                            <option value="LV">Latvija</option>
                        </select>
                        <input name="name" placeholder="Vārds, Uzvārds" type="text" value=""/>
                        <input class="only_number" name="phone" placeholder="Telefons" type="text" value=""/>
                        <!--div class="prices none">
                        <div>Piegāde: 0 €

                                                </div>
                        <div>Kopējā cena: 24 €
                                                </div>
                        </div-->
                        <!--<input class="button js_submit" type="submit" value="Pasūtīt"/>-->
                    </form>
                </div>
            </div>
        </div>
        <!-- </footer> -->
    </div>
    </body>
    </html>
<?php } ?>