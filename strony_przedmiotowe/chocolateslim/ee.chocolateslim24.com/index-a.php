
<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 8092 -->
    <script>var locale = "ee";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIALdEwS0AGWIAAEAAQACRhMBAAKcHwK-AQEABJ0qkNIA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 0;
        var name_hint = 'Gokhan Karakoc';
        var phone_hint = '+105469652091';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a><br><a class="download" href="#">Download our Tips!</a></div>');

            moment.locale("ee");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));
            $('.download').click(function(e) {
                e.preventDefault();  //stop the browser from following
                window.location.href = 'out_tips.pdf';
            });

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->



    <meta charset="utf-8"/>
    <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
    <title> Chocolate Slim </title>
    <link href="//st.acstnst.com/content/ChocolateSlimEE/css/css.css" rel="stylesheet" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Lobster:400&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
    <script src="//st.acstnst.com/content/ChocolateSlimEE/js/jqueryplugin.js" type="text/javascript"></script>
    <script src="//st.acstnst.com/content/ChocolateSlimEE/js/jquery.countdown.min.js" type="text/javascript"></script>
    <script src="//st.acstnst.com/content/ChocolateSlimEE/js/main.js" type="text/javascript"></script>
    <link href="//st.acstnst.com/content/ChocolateSlimEE/css/custom-styles.min.css" rel="stylesheet" type="text/css"/>

    <style>
        #header{ background: saddlebrown;}
        #info-block{ background: none;}
        #reviews{
            background: lightblue;}
    </style>
</head>
<body>
<div class="hide-it">
    <!-- <header> -->
    <div id="header">
        <div class="width">
            <div class="block">
                <h1>8 suurepärast toiduainet sinu dieedi parandamiseks</h1>
                <h2>Kui sa langetad kaalu, siis lisaks söögi kogusele on oluline ka see, mida sa sööd.
                </h2>
                <div class="list">
                    Seepärast tasub sul teada häid toiduaineid, mis teevad kaalu langetamise imelihtsaks. Olen sulle kirja pannud kaheksa väga head toiduainet, mis aitavad sul muutuda tervislikumaks, ennetada haigusi, näha hea välja ja tunda ennast palju paremini. Pane tähele, et kõiki neid kasulikke toitaineid lihtsalt ei ole võimalik vahetada tablettide voi toidulisandite vastu. Ükski lisand ei suuda asendada päristoitu.

                </div>

            </div>
        </div>
    </div>
    <!-- </header> -->
    <!-- <steps> -->
    <div id="steps">
        <div class="width">
            <div class="title"></div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico1.jpg"/>
                </div>
                <p></p>
            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico2.jpg"/>
                </div>
                <p></p>
            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico3.jpg"/>
                </div>
                <p></p>
            </div>
            <div class="clear"></div>
            <div class="text">
                <div class="title">1. Lõhe</div>
                <p>Lõhe ja ka kalad üldisemalt on superhead toiduained just oma omega-3 rasvhapete sisalduse tõttu. Uuringud näitavad, et omega-3 rasvhapped aitavad kaitsta ka sinu südant. Lisaks kasulikule rasvale on kalades ka väga palju valku, rauda ning nad sisaldavad vähe küllastunud rasvhappeid.  Valguallikana võid lisaks kaladele tarvitada ka näiteks linnuliha voi loomaliha.</p>
                <p>2. Pruun riis
                    Eestlastele on pruun riis üldjuhul võõras ja harjumatu. Tegelikult on pruun riis väga hea süsivesikuallikas ning valgest riisist kasulikum. Pruuni riisi on palju vähem töödeldud ning alles on jäetud suurem osa kiudaineid, mineraale ja vitamiine. Dieedi ajal sobivad süsivesikuallikatena edukalt ka näiteks kartulid, tatar, täisteratooted jms. </p>
            </div>
        </div>
    </div>
    <!-- </steps> -->
    <!-- <sostav> -->
    <div id="sostav">
        <div class="width">
            <div class="title">3. Munad</div>
            <h2></h2>
            <div class="item first">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img1.png"/>
                <div class="message">

                    <p>Vastupidiselt üldisele arvamusele ei ole munad tegelikult sinu tervisele kahjulikud. Uuringutele toetudes aitab hommikuti munade söömine kaotada kaks korda rohkem kaalu kui süües tapselt sama kaloraaziga, kuid ilma munadeta hommikusööki. Munad täidavad sinu kõhu pikaks ajaks, seepärast on ülimalt tõenaoline, et sööd päeva jooksul vähem. Lisaks on munades väga palju valku, vitamiine ja mineraale. </p>
                </div>
            </div>
            <div class="item first right">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img2.png"/>
                <div class="message">
                    <div>4. Pähklid</div>
                    <p>Pähklitel on juures teatud halb kuulsus just nende suure rasvasisalduse tõttu. Tegelikult on pähklites palju head ja kasulikku rasva, lisaks veel valku, mineraalaineid, vitamiine ja kiudaineid. Loomulikult tuleb pähkleid süües arvestada portsjonitega, kuid mõõdukalt tarbides aitavad need vähendada kolesteroolitaset ning kiirendada kaalu langetamist. Lisa pähkleid salatitele, putrudele või söö neid lihtsalt vahepalaks.</p>
                </div>
            </div>
            <div class="clear"></div>
            <div class="item second">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img3.png"/>
                <div class="message">
                    <div>5. Juurviljad
                    </div>
                    <p>Juurviljade kasulikkusest on palju räägitud, kõik teavad, et need on väga vitamiini- ja mineraalirikkad. Lisaks sellele on juurviljad väga odavad ning ideaalsed kõhutäitjad. See tähendab seda, et väga madala kalorsuse juures on nad äärmiselt kõhtutäitvad.</p>
                </div>
            </div>
            <div class="item second right">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img4.png"/>
                <div class="message">
                    <div>6. Täisteratooted</div>
                    <p>Sinu keha kasutab täisteratoodetest saadavat energiat aeglaselt ning veresuhkrutase püsib ühtlasem ning kõht püsib pikemat aega täis. Seepärast vali tavalise pasta asemel täisterapasta, tavalise leiva asemel täisteraleib jne.</p>
                </div>
            </div>
            <div class="clear"></div>
            <div class="item third">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img5.png"/>
                <div class="message">
                    <div>7. Piim
                    </div>
                    <p>Piima joomine on suurepärane võimalus võidelda luuhõrenemise vastu. Raske on leida toiduainet, mis oleks piimast kaltsiumirikkam. Hiljutised uuringud on isegi näidanud, et piimatooteid tarbivad inimesed kaotavad lihtsamalt kaalu. Lisaks kaltsiumile sisaldab piim ka ohtralt valku.</p>
                </div>
            </div>
            <div class="item third right">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img6.png"/>
                <div class="message">
                    <div>8. Puuviljad </div>
                    <p>Peale selle, et puuviljad on väga vitamiinirikkad ja tervislikud, on nad ka väga kättesaadavad. Selle all pean ma silmas seda, et kui sul on kiire ja napib aega söögi valmistamiseks, siis selle asemel, et toidukord vahele jätta, saad kasutada neid edukalt tervisliku vahepalana. Lisa peotäis pähkleid ning kohupiima ning täisvaartuslik toidukord ongi valmis!

                    </p>   </div>
            </div>
            <div class="clear"></div>
            <div class="plaha"></div>
        </div>
    </div>
    <!-- </sostav> -->
    <!-- <result> -->

    <!-- </result> -->
    <!-- <info-block> -->
    <div id="info-block">
        <div class="width">
            <div class="title">Kuidas hoiduda puhkuseaegsest kaalutõusust?</div>
            <p>
                Suvi on kätte jõudnud ja kõigil on kindlasti plaanis ka puhkused. Paraku kipub puhkus endaga kaasa tooma ka märgatavat kaalutõusu. Siin sulle mõned vihjed, kuidas seda vältida:  1. Ära unusta süüa!
                Suured pidustused teevad raskeks kindlast toiduplaanist kinnipidamise. Lihtne on jätta vahele toidukordi või haarata kiireks snäkiks mõne burger või hot dog. Ole selleks valmis ja varu kotti käepäraseid tervislikke vahepalasid nagu näiteks õunad, pirnid, pähklid jne.

            </p>
        </div>
    </div>
    <!-- </info-block> -->

    <!-- <reviews> -->
    <div id="reviews">
        <div class="width">
            <div class="title"></div>
            <h2></h2>
            <div class="review">
                <div class="date">

                </div>
                <div class="name">     2. Ära unusta trenni.</div>
                <div class="email"></div>
                <p>


                    Ma tean, et sul on kiire… Treening aitab sul võidelda puhkuse- ja pühadeaegse stressiga ning hoida edukalt eemal lisakilosid. Suvine treening ei pea olema tundidepikkune rassimine. Kui vajad, jaga oma treening väiksemateks osadeks ja leia kasvõi 15 minutit päevas kiireks treeninguks. Kui sa ei taha treenida spordiklubis, saad väga edukalt hakkama ka kodus kasutades vaid käepäraseid vahendeid. Saa kodusteks harjutusteks ideid meie harjutustepangast.

                </p>
            </div>
            <div class="review center">
                 <div class="date">

                </div>
                <div class="name">  3. Ole mõõdukas.</div>
                <div class="email"></div>
                <p>

                    Puhkuseaeg on täis ohtralt pidusid, kus pakutakse kõike seda, mida sa ilmselt tavaolukorras üritad vältida- pirukad, koogid, limonaadid, küpsised jne. Lihtne on ennast unustada ja minna söömisega liiale. Parim lahendus selle vältimiseks on planeerimine. Minu soovitus on peole mitte minna näljasena. Võta enne kodus üks tervislik vahepala ja klaas vett. See täidab sinu kõhtu ja pärast sööd pidusööki natukene vähem. Katsu vältida magusaid jooke. Selle asemel vali vesi.

                </p>
            </div>
            <div class="review">
                <div class="date">

                </div>
                <div class="name">4. Vii alkoholi tarbimine miinimumini.</div>
                <div class="email"></div>
                <p>


                    Väldi alkoholi nii palju kui vähegi võimalik. Peale selle, et alkoholis on tubli annus tühje kaloreid, pärsib ta ka sinu otsustusvõimet ning kaob igasugune kontroll selle üle, mis suhu läheb.
                </p>
            </div>
            <div class="clear"></div>
            <div class="buy">

            </div>
        </div>
    </div>
    <!-- </reviews> -->
    <!-- <info-block2> -->
    <div id="info-block2">
        <div class="width">
            <div class="left">
                <div class="title">
                    <div>5. Proovi väljavahetamise taktikat
                    </div>
                </div>
                <p>Kui keelad endale puhkuse ajal pidevalt oma lemmiktoite, tunned sa ennast ahistatuna ja suure tõenäosusega saavad isud sinust võitu. Kui sa tõesti tahad minna sõpradega pitsat sööma, mine, kuid luba endale, et näiteks järgneval paaril õhtul jätad magustoidu vahele. Lõppude lõpuks on puhkus ka nautimiseks ja endale kõige keelamine võib viia murdumiseni.
                </p>
            </div>
            <div class="right">
                <div class="title">
                    <div>6. Hoia ennast aktiivsena!
                    </div>
                </div>
                <p>Puhkus võib sinu vormi ka parandada! Võta igapäevaselt midagi ette, sellisel juhul mõtled toidule märksa vähem. Tee sporti, uju, mängi palli.

                </p>
            </div>
            <div class="left">
                <div class="title">
                    <div>7. Väldi täiuslikkust

                    </div>
                </div>
                <p> Sa pidid ära jätma oma treeningu ja sõid restoranis liiga palju? Ära lase sellel ennast heidutada ja tervet puhkust ära rikkuda. Eksimine on inimlik!
                </p>
            </div>
            <div class="right">
                <div class="title">
                    <div>8. Puhka piisavalt.
                    </div>
                </div>
                <p>
                    Raske on vastu pidada puhkuseaegsetele ahvatlustele, kui oled kogu aeg väsinud. Korralik uni on hädavajalik. Maga vähemalt 8 tundi.  </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <!-- </info-block2> -->
    <!-- <how-buy> -->

    <!-- </how-buy> -->
    <!-- <how-buy> -->

    <!-- </how-buy> -->

    <!-- </footer> -->
</div>
</body>
</html>


