<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 8092 -->
        <script>var locale = "ee";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALdEwT0KmeIAAEAAQACRhMBAAKcHwK-AQEABKxgv5kA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Gokhan Karakoc';
            var phone_hint = '+105469652091';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ee");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="width=device-width, height=device-height, initial-scale=1" name="viewport"/>
        <title> Chocolate Slim </title>
        <link href="//st.acstnst.com/content/ChocolateSlimEE/mobile/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/ChocolateSlimEE/mobile/css/custom-styles.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Lobster&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/ChocolateSlimEE/mobile/js/main.js" type="text/javascript"></script>
    </head><body>
    <!-- <header> -->
    <section id="header">
        <div class="width">
            <!--<div class="img_wrapper">-->
            <!--<img src="img/top.png" alt="">-->
            <!--</div>-->
            <div class="block header_bg">
                <h1> Chocolate Slim </h1>
                <h2> Naturaalne salenemis preparaat </h2>
                <div class="list">
                    Vähendab kiiresti lisakilosid<br/>
                    Võitleb tselluliidiga<br/>
                    Eemaldab vistrikud ja akne <br/>
                </div>
                <div class="hidden_upsale" style="visibility: visible">
                    <div class="sale">Super pakkumine! 50% allahindlust

                    </div>
                </div>
                <div class="price">
                    <del><span class="price_land_s4">48</span> <span class="price_land_curr">€</span></del>
                    <div><span class="price_land_s1">24 €</span>
                    </div>
                </div>
                <a class="button all_buttons scrollto" href="#form"> Telli </a>
            </div>
        </div>
    </section>
    <!-- </header> -->
    <!-- <steps> -->
    <section id="steps">
        <div class="width">
            <div class="title">Kuidas kaotada 10 kg?

            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ico1.jpg"/>
                </div>
                <p>
                    2-4 nädalane kuur

                </p>
            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ico2.jpg"/>
                </div>
                <p>
                    Joo hommikul
                </p>
            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ico3.jpg"/>
                </div>
                <p>
                    PLase tassis tõmmata
                </p>
            </div>
            <div class="clear"></div>
            <div class="text">
                <div class="title">Šokolaad Chocolate Slim

                </div>
                <p>
                    on kogum looduslikest salenadavatest koostisosadest, mis tugevdavad üksteise mõjuainet.
                    Tulemused ületavad kõiki ootusi!
                </p>
            </div>
        </div>
    </section>
    <!-- </steps> -->
    <!-- <sostav> -->
    <section id="sostav">
        <div class="width">
            <div class="title"> Miks on Chocolate Slim
                nii ebaharilikult efektiivne?</div>
            <h2> 6 looduslikku koostisosa

            </h2>
            <div class="block_wrap">
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img11.png"/>
                    <div class="message">
                        <div> Kaerakiud	</div>
                        <p>
                            Pärsivad söögiisu.  Tugevdab imuunsussüsteemi ja annab lisa energiat.

                        </p>
                    </div>
                </div>
                <div class="item first ">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img21.png"/>
                    <div class="message">
                        <div> Sooja letsitiin
                        </div>
                        <p>
                            Looduslik rasva põletaja.  Blokeerib rasvarakkude kogunemist
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img31.png"/>
                    <div class="message">
                        <div>Vadakuvalk
                        </div>
                        <p>
                            See sisaldab cyanidiini,   mis hoiab ära rasvarakkude tekke.  Looduslik antioksüdantide allikas.
                        </p>
                    </div>
                </div>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img41.png"/>
                    <div class="message">
                        <div>Glükomannan

                        </div>
                        <p>
                            Annab energiat.   Hoiab ära rasvade kogunemise  probleemsetes piirkondades.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img51.png"/>
                    <div class="message">
                        <div>Looduslik kakao

                        </div>
                        <p>
                            Kiirendab rasva põletust.  Selle jooksul tugevdab ka imuunsüsteemi. Pärsib suhkru vajadust ja aitab toota dopamiini (õnnelikkuse hormooni).
                        </p>
                    </div>
                </div>
                <div class="item first ">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img61.png"/>
                    <div class="message">
                        <div>Vitamiinid ja mineraalid

                        </div>
                        <p>
                            Taastab ainevahetust.  Kasulik protsess tervele kehale. Alandab kolesterooli taset veres.
                        </p>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="plaha">
                Orgaaniline.

            </div>
        </div>
    </section>
    <!-- </sostav> -->
    <!-- <result> -->
    <section id="result">
        <div class="width">
            <div class="title">Tulemus: Chocolate Slim-iga salenevad kõik!

            </div>
            <h2> Üle 7000 õnneliku kaalu langetanud naise!

            </h2>
            <div class="item_wrapper">
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img7.jpg"/>
                    <p>
                        Rasv kaob otse sinu silme all
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img8.jpg"/>
                    <p>
                        Söögiisu väheneb
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img9.jpg"/>
                    <p>
                        Pakatad elujõust
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img10.jpg"/>
                    <p>
                        Aitab vabaneda suhkru himust
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/img11.jpg"/>
                    <p>
                        Hea tuju
                    </p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    <!-- </result> -->
    <!-- <info-block> -->
    <section id="info-block">
        <div class="info-block_wrapper">
            <div class="width">
                <div class="title">Salenemine pole kunagi olnud
                    nii lihtne! </div>
                <p>
                    Chocolate Slim kuuriga suudad sa vabaneda kuni 24 kg-st 4 nädala jooksul. Salenemise jooksul oled rõõmsatujuline ning pakatad energiast! Naudi salenemist! Šokolaad on ainulaadne toode, mida sa tahaksid veel ja veel.

                    <br/><br/>
                    Toode ei ole toidulisand, see sisaldab ainult looduslikke komponente. Peamised komponendid on looduslik kakao, goji marjad, rohelised kohvioad ja ganoderma lucidumi ekstrakt. Toode sobib igapäevaseks kasutamiseks ja vastunäidustused puuduvad. See eemaldab ülekaalulisuse algpõhjuse, normaliseerides ainevahetusprotsesse organismis.

                    <br/><br/>
                    See on toode, mis on tekitanud tõelise revolutsiooni figuuri korrigeerimise, tervise ning noorena püsimise vallas.
                </p>
            </div>
        </div>
    </section>
    <!-- </info-block> -->
    <!-- <reviews> -->
    <section id="reviews">
        <div class="width">
            <div class="title">Küsige neilt, kes on juba kaalu langetanud
                Chocolate Slim-iga
            </div>
            <h2> Ehtsad inimesed -ehtsad tulemused!

            </h2>
            <div class="review clearfix">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ava1.jpg"/>
                <div class="date">
                    <div>
                        <script>dtime_nums(-60, true)</script>
                    </div>
                    <div>
                        <script>dtime_nums(-10, true)</script>
                    </div>
                </div>
                <div class="name"> Alina Pușcaș, 25 de ani

                </div>
                <div class="email">alina*******@gmail.com

                </div>
                <p>
                    Este ceva uimitor! Am slăbit aproape 18 kilograme în primele 3 săptămâni! Și apoi încă 7 kg, în următoarele două săptămâni! Lucrul cel mai important este să urmați instrucțiunile: 1 cană de ciocolată înainte de un mic dejun mai ușor sau în loc de aceasta, în fiecare zi, fără întrerupere. Sigur, nu ar trebui să mănânci produse de patiserie înainte de culcare. Dacă eu am reușit, și tu poți!
                </p>
            </div>
            <div class="review clearfix">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ava2.jpg"/>
                <div class="date">
                    <div>
                        <script>dtime_nums(-70, true)</script>
                    </div>
                    <div>
                        <script>dtime_nums(-4, true)</script>
                    </div>
                </div>
                <div class="name"> Peeter Valli, 44-aastane

                </div>
                <div class="email"> kutuz*****@mail.ee

                </div>
                <p>
                    Minu naine ostis mulle Chocolate Slim-i pärast seda, kui olin mitmeid teisi asju juba proovinud – erinevat sorti teesid ja salendavaid pille. Kõigest 1,5 kuu jooksul suutsin vabaneda 12 kg-st ning hetkel pole mul plaaniski peatuda. Aitäh!

                </p>
            </div>
            <div class="review clearfix">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ava3.jpg"/>
                <div class="date">
                    <div>
                        <script>dtime_nums(-30, true)</script>
                    </div>
                    <div>
                        <script>dtime_nums(-2, true)</script>
                    </div>
                </div>
                <div class="name">Emma Veski, 32-aastane

                </div>
                <div class="email">ladyemma*****@mail.ee

                </div>
                <p>
                    Nii lahe, et Chocolate Slim on lõpuks saadaval ka Eestis! Ma lugesin selle kohta foorumist ning lõpuks proovisin ka ise. Kujutage ette, olen šokolaadi joonud kõigest 2 nädalat ning juba praegu mahun teksastesse, mida viimati kandsin 4 aastat tagasi :)

                </p>
            </div>
            <div class="clear"></div>
            <div class="buy">
                <div>Ärge kahelge, alustage salenemisega koheselt!

                </div>
                <a class="button all_buttons scrollto" href="#form"> Telli  </a>
            </div>
        </div>
    </section>
    <!-- </reviews> -->
    <!-- <how-buy> -->
    <section id="how-buy">
        <div class="width">
            <div class="title">Kuidas tellida Chocolate Slim-i?

            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ico4.jpg"/>
                <p>
                    Täitke ankeet meie kodulehel
                </p>
            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ico5.jpg"/>
                <p>
                    Jääge ootama operaatori kõnet
                </p>
            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/ico6.jpg"/>
                <p>
                    Paki kättesaamine toimub teile lähimast postkontorist
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    <!-- </how-buy> -->
    <!-- <how-buy> -->
    <section>
        <div class="width">
            <div id="howuse">
                <h2>Chocolate Slim-i kasutamine on käki tegu!



                </h2>
                <div class="hu_step">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/hu_step1.jpg"/>
                    <p class="hu_title"> VALMISTAMINE
                    </p>
                    <p><b> Valmistage kokteil igal hommikul kasutades selleks 250 ml piima, </b> 1-2 lusikat segu naise ning 2-3 lusikat segu mehe kohta. </p>
                </div>
                <div class="hu_step">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/hu_step2.jpg"/>
                    <p class="hu_title"> JOOGE </p>
                    <p><b> Jooge valmistatud kokteili hommikusöögi asemel</b> ning teie keha saab: 217 kcal, 10g süsivesikuid, 17g proteiini, 23 mikroelementi, vitamiine ja kiudaineid.  </p>
                </div>
                <div class="hu_step">
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/hu_step3.jpg"/>
                    <p class="hu_title">KORRAKE </p>
                    <p> Asendage päeva jooksul <b> veel üks söögikord kokteiliga </b>  tulemuste tugevdamiseks.  </p>
                </div>
                <p style="clear: both; float: none; width: 0; height: 0;"></p>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    <!-- </how-buy> -->
    <!-- <footer> -->
    <section id="footer">
        <div class="width">
            <div class="left">
                <h1> Chocolate Slim </h1>
                <h2> Looduslik salenemise preparaat

                </h2>
                <img alt="" class="prod" src="//st.acstnst.com/content/ChocolateSlimEE/mobile/img/prod.png"/>
                <div class="hidden_upsale" style="visibility: visible">
                    <div class="sale">Super pakkumine! 50 % allahindlust

                    </div>
                </div>
                <div class="price">
                    <del><span class="price_land_s4">48 €</span></del>
                    <div><span class="price_land_s1">24 €</span></div>
                </div>
            </div>
            <div class="right">
                <div class="timer_title">
                    <h3>Pakkumine lõppeb:
                    </h3>
                </div>
                <div class="timer">
                    <div class="landing_countdown"> </div>
                </div>
                <form action="" id="form" method="post">
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abcdg.pro/forms/?target=-4AAIJIALdEwAAAAAAAAAAAAQND_eNAA"></iframe>

                    <!--
                    <input type="hidden" name="total_price" value="24.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIALdEwT0KmeIAAEAAQACRhMBAAKcHwK-AQEABKxgv5kA">
                    <input type="hidden" name="goods_id" value="109">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="ChocolateSlimEE">
                    <input type="hidden" name="price" value="24">
                    <input type="hidden" name="old_price" value="48">
                    <input type="hidden" name="total_price_wo_shipping" value="24.0">
                    <input type="hidden" name="package_prices" value="{}">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="0">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="EE">
                    <input type="hidden" name="shipment_vat" value="0.0">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.0">

                    <select class="country_select" id="country_code_selector" name="country">
                        <option value="EE">Eesti</option>
                    </select>
                    <input name="name" placeholder="Sinu nimi" type="text"/>
                    <input class="only_number" name="phone" placeholder="Sinu telefoni number" type="text"/>
                    <input class="js_submit button" type="submit" value="Telli"/>-->
                </form>
            </div>
        </div>
    </section>
    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 8092 -->
        <script>var locale = "ee";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALdEwS0AGWIAAEAAQACRhMBAAKcHwK-AQEABJ0qkNIA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Gokhan Karakoc';
            var phone_hint = '+105469652091';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ee");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
        <title> Chocolate Slim </title>
        <link href="//st.acstnst.com/content/ChocolateSlimEE/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Lobster:400&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/ChocolateSlimEE/js/jqueryplugin.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/ChocolateSlimEE/js/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/ChocolateSlimEE/js/main.js" type="text/javascript"></script>
        <link href="//st.acstnst.com/content/ChocolateSlimEE/css/custom-styles.min.css" rel="stylesheet" type="text/css"/>
        <!--[if gte IE 8]>
        <style>
            select {
                background: url(img/input.png);
            }
        </style>
        <![endif]-->
    </head>
    <body>
    <!-- <header> -->
    <section id="header">
        <div class="width">
            <div class="block">
                <h1>  Chocolate Slim  </h1>
                <h2>  Naturaalne salenemis preparaat  </h2>
                <div class="list">
                    Vähendab kiiresti lisakilosid<br/>
                    Võitleb tselluliidiga<br/>
                    Eemaldab vistrikud ja akne
                </div>
                <div class="hidden_upsale" style="visibility: visible">
                    <div class="sale"> Super pakkumine! 50% allahindlust </div>
                </div>
                <div class="price">
                    <del><span class="price_land_s4">48</span> <span class="price_land_curr">€</span></del>
                    <div><span class="price_land_s1">24</span> <span class="price_land_curr">€</span></div>
                </div>
                <a class="button" href="#footer"> Telli </a>
            </div>
        </div>
    </section>
    <!-- </header> -->
    <!-- <steps> -->
    <section id="steps">
        <div class="width">
            <div class="title">  Kuidas kaotada 10 kg? </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ico1.jpg"/>
                </div>
                <p>
                    2-4 nädalane kuur
                </p>
            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ico2.jpg"/>
                </div>
                <p>
                    Joo hommikul
                </p>
            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ico3.jpg"/>
                </div>
                <p>
                    PLase tassis tõmmata
                </p>
            </div>
            <div class="clear"></div>
            <div class="text">
                <div class="title">Šokolaad Chocolate Slim</div>
                <p>
                    on kogum looduslikest salenadavatest koostisosadest, mis tugevdavad üksteise mõjuainet.
                    <br/> Tulemused ületavad kõiki ootusi!
                </p>
            </div>
        </div>
    </section>
    <!-- </steps> -->
    <!-- <sostav> -->
    <section id="sostav">
        <div class="width">
            <div class="title">Miks on Chocolate Slim<br/> nii ebaharilikult efektiivne?</div>
            <h2> 6 looduslikku koostisosa </h2>
            <div class="item first">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img11.png"/>
                <div class="message">
                    <div>Kaerakiud</div>
                    <p>
                        Pärsivad söögiisu. <br/> Tugevdab imuunsussüsteemi ja annab lisa energiat.
                    </p>
                </div>
            </div>
            <div class="item first right">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img21.png"/>
                <div class="message">
                    <div> Sooja letsitiin</div>
                    <p>
                        Looduslik rasva põletaja. <br/> Blokeerib rasvarakkude kogunemist
                    </p>
                </div>
            </div>
            <div class="clear"></div>
            <div class="item second">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img31.png"/>
                <div class="message">
                    <div>Vadakuvalk</div>
                    <p>
                        See sisaldab cyanidiini, <br/>  mis hoiab ära rasvarakkude tekke. <br/> Looduslik antioksüdantide allikas.
                    </p>
                </div>
            </div>
            <div class="item second right">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img41.png"/>
                <div class="message">
                    <div> Glükomannan</div>
                    <p> Annab energiat.  <br/> Hoiab ära rasvade kogunemise  <br/> probleemsetes piirkondades.
                    </p>
                </div>
            </div>
            <div class="clear"></div>
            <div class="item third">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img51.png"/>
                <div class="message">
                    <div>Looduslik kakao</div>
                    <p>
                        Kiirendab rasva põletust. <br/> Selle jooksul tugevdab ka imuunsüsteemi.<br/> Pärsib suhkru vajadust ja aitab toota dopamiini (õnnelikkuse hormooni).
                    </p>
                </div>
            </div>
            <div class="item third right">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img61.png"/>
                <div class="message">
                    <div> Vitamiinid ja mineraalid</div>
                    <p>
                        Taastab ainevahetust. <br/> Kasulik protsess tervele kehale.<br/>Alandab kolesterooli taset veres.
                    </p>
                </div>
            </div>
            <div class="clear"></div>
            <div class="plaha">
                Orgaaniline
            </div>
        </div>
    </section>
    <!-- </sostav> -->
    <!-- <result> -->
    <section id="result">
        <div class="width">
            <div class="title">Tulemus: Chocolate Slim-iga salenevad kõik!  </div>
            <h2> Üle 7000 õnneliku kaalu langetanud naise! </h2>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img7.jpg"/>
                <p>
                    Rasv kaob otse sinu silme all
                </p>
            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img8.jpg"/>
                <p>
                    Söögiisu väheneb
                </p>
            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img9.jpg"/>
                <p>
                    Pakatad elujõust
                </p>
            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img10.jpg"/>
                <p>
                    Aitab vabaneda suhkru himust
                </p>
            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/img11.jpg"/>
                <p>
                    Hea tuju
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    <!-- </result> -->
    <!-- <info-block> -->
    <section id="info-block">
        <div class="width">
            <div class="title"> Salenemine pole kunagi olnud <br/> nii lihtne! </div>
            <p>
                Chocolate Slim kuuriga suudad sa vabaneda kuni 24 kg-st 4 nädala jooksul. Salenemise jooksul oled rõõmsatujuline ning pakatad energiast! Naudi salenemist! Šokolaad on ainulaadne toode, mida sa tahaksid veel ja veel.
                <br/><br/>
                Toode ei ole toidulisand, see sisaldab ainult looduslikke komponente. Peamised komponendid on looduslik kakao, goji marjad, rohelised kohvioad ja ganoderma lucidumi ekstrakt. Toode sobib igapäevaseks kasutamiseks ja vastunäidustused puuduvad. See eemaldab ülekaalulisuse algpõhjuse, normaliseerides ainevahetusprotsesse organismis.
                <br/><br/>
                See on toode, mis on tekitanud tõelise revolutsiooni figuuri korrigeerimise, tervise ning noorena püsimise vallas.
            </p>
        </div>
    </section>
    <!-- </info-block> -->
    <!-- <compare> -->
    <section id="compare">
        <div class="width">
            <div class="title"> Chocolate Slimi võrdlus tavaliste  <br/> salenemis meetoditega.</div>
            <h2> Me intervjueerisime 15 naisest koosnevat gruppi, kelle kõigi sooviks oli salenemine ning tulemused räägivad enda eest: </h2>
            <div class="wr">
                <div class="text1">
                    Chocolate Slim<span> (Ei muutnud nende elustiili) </span>
                </div>
                <ul class="text2 clearfix">
                    <li>
                        kg
                    </li>
                    <li>
                        1 nädal
                    </li>
                    <li>
                        2 nädalat
                    </li>
                    <li>
                        3 nädalat
                    </li>
                    <li>
                        4 nädalat
                    </li>
                </ul>
                <div class="text3">
<span>
            Kehalised harjutused 3 korda nädalas
          </span>
                    <span>
            Salenemisvastased rohud
          </span>
                </div>
            </div>
        </div>
    </section>
    <!-- </compare> -->
    <!-- <reviews> -->
    <section id="reviews">
        <div class="width">
            <div class="title">Küsige neilt, kes on juba kaalu langetanud  <br/>Chocolate Slim-iga </div>
            <h2>  Ehtsad inimesed -ehtsad tulemused! </h2>
            <div class="review">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ava1.jpg"/>
                <div class="date">
                    <div>
                        <script>dtime_nums(-60, true)</script></div>
                    <div>
                        <script>dtime_nums(-10, true)</script></div>
                </div>
                <div class="name">  Alina Pușcaș, 25 de ani  </div>
                <div class="email">  alina*******@gmail.com  </div>
                <p>
                    Este ceva uimitor! Am slăbit aproape 18 kilograme în primele 3 săptămâni! Și apoi încă 7 kg, în următoarele două săptămâni! Lucrul cel mai important este să urmați instrucțiunile: 1 cană de ciocolată înainte de un mic dejun mai ușor sau în loc de aceasta, în fiecare zi, fără întrerupere. Sigur, nu ar trebui să mănânci produse de patiserie înainte de culcare. Dacă eu am reușit, și tu poți!
                </p>
            </div>
            <div class="review center">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ava2.jpg"/>
                <div class="date">
                    <div>
                        <script>dtime_nums(-70, true)</script></div>
                    <div>
                        <script>dtime_nums(-4, true)</script></div>
                </div>
                <div class="name"> Peeter Valli, 44-aastane</div>
                <div class="email"> kutuz*****@mail.ee </div>
                <p>
                    Minu naine ostis mulle Chocolate Slim-i pärast seda, kui olin mitmeid teisi asju juba proovinud – erinevat sorti teesid ja salendavaid pille. Kõigest 1,5 kuu jooksul suutsin vabaneda 12 kg-st ning hetkel pole mul plaaniski peatuda. Aitäh!
                </p>
            </div>
            <div class="review">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ava3.jpg"/>
                <div class="date">
                    <div>
                        <script>dtime_nums(-30, true)</script></div>
                    <div>
                        <script>dtime_nums(-2, true)</script></div>
                </div>
                <div class="name"> Emma Veski, 32-aastane </div>
                <div class="email">ladyemma*****@mail.ee  </div>
                <p>
                    Nii lahe, et Chocolate Slim on lõpuks saadaval ka Eestis! Ma lugesin selle kohta foorumist ning lõpuks proovisin ka ise. Kujutage ette, olen šokolaadi joonud kõigest 2 nädalat ning juba praegu mahun teksastesse, mida viimati kandsin 4 aastat tagasi :)
                </p>
            </div>
            <div class="clear"></div>
            <div class="buy">
                <div> Ärge kahelge, alustage salenemisega koheselt! </div>
                <a class="button" href="#footer"> Telli  </a>
            </div>
        </div>
    </section>
    <!-- </reviews> -->
    <!-- <info-block2> -->
    <section id="info-block2">
        <div class="width">
            <div class="info-block2-inner">
                <div class="left">
                    <div class="title">
                        <span></span>
                        <div> Kas see ei ole kahjulik? </div>
                    </div>
                    <p>
                        Chocolate Slim koosneb täielikult looduslikest komponentidest, on parabeeni, värvainete, sünteetiliste lõhnaainete ja GMO vaba.  <br/>
                        Lugege juhiseid.
                    </p>
                </div>
                <div class="left">
                    <div class="title">
                        <span></span>
                        <div> Kust seda </div>
                    </div>
                    <p>
                        Eestis osta saab? on meie firma ainuke ametlik Chocolate Slim-i edasimüüja! Kõik teised on järele tehtud! Tellimuse saate esitada AINULT meie veebilehel.
                    </p>
                </div>
                <div class="clear"></div>
                <div class="left">
                    <div class="title">
                        <div> Kui palju ma vajan <br/> 10-st kilost vabanemiseks?</div>
                    </div>
                    <p>
                        Kliinilised uuringud on näidanud, et heade tulemuste saavutamiseks piisab kõigest kuu ajasest katse perioodist. Esimesed tulemused ilmnevad juba kolmandal kasutamise päeval.
                    </p>
                </div>
                <div class="left">
                    <div class="title ">
                        Hoiduge <br/> võltsingutest!
                    </div>
                    <p>  Hoiduge võltsitud toodetest! Ostke ainult originaal Chocolate Slim tooteid! Igal pakendil on olemas kaubamärk.  </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>
    <!-- </info-block2> -->
    <!-- <how-buy> -->
    <section id="how-buy">
        <div class="width">
            <div class="title"> Kuidas tellida Chocolate Slim-i? </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ico4.jpg"/>
                <p>
                    Täitke ankeet meie kodulehel
                </p>
            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ico5.jpg"/>
                <p>
                    Jääge ootama operaatori kõnet
                </p>
            </div>
            <div class="item">
                <img alt="" src="//st.acstnst.com/content/ChocolateSlimEE/img/ico6.jpg"/>
                <p>
                    Paki kättesaamine <span class="del_office"> toimub teile lähimast postkontorist  </span>
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    <!-- </how-buy> -->
    <!-- <how-buy> -->
    <section>
        <div class="width">
            <div id="howuse">
                <h2>  Chocolate Slim-i kasutamine on käki tegu! </h2>
                <div class="hu_step">
                    <img alt="шаг 1" src="//st.acstnst.com/content/ChocolateSlimEE/img/hu_step1.jpg"/>
                    <p class="hu_title"> VALMISTAMINE </p>
                    <p><b> Valmistage kokteil igal hommikul kasutades selleks 250 ml piima, </b> 1-2 lusikat segu naise ning 2-3 lusikat segu mehe kohta. </p>
                </div>
                <div class="hu_step">
                    <img alt="шаг 1" src="//st.acstnst.com/content/ChocolateSlimEE/img/hu_step2.jpg"/>
                    <p class="hu_title">  JOOGE  </p>
                    <p><b> Jooge valmistatud kokteili hommikusöögi asemel </b>  ning teie keha saab: 217 kcal, 10g süsivesikuid, 17g proteiini, 23 mikroelementi, vitamiine ja kiudaineid. </p>
                </div>
                <div class="hu_step">
                    <img alt="шаг 1" src="//st.acstnst.com/content/ChocolateSlimEE/img/hu_step3.jpg"/>
                    <p class="hu_title"> KORRAKE </p>
                    <p> Asendage päeva jooksul  <b> veel üks söögikord kokteiliga </b> tulemuste tugevdamiseks. </p>
                </div>
                <p style="clear: both; float: none; width: 0; height: 0;"></p>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    <!-- </how-buy> -->
    <!-- <footer> -->
    <section id="footer">
        <div class="width">
            <div class="left">
                <h1> Chocolate Slim </h1>
                <h2> Looduslik salenemise preparaat </h2>
                <img alt="" class="prod" src="//st.acstnst.com/content/ChocolateSlimEE/img/prod.png"/>
                <div class="hidden_upsale" style="visibility: visible">
                    <div class="sale">Super pakkumine! 50 % allahindlust </div>
                </div>
                <div class="price">
                    <del><span class="price_land_s4">48 €</span></del>
                    <div><span class="price_land_s1">24 €</span></div>
                </div>
            </div>
            <div class="right">
                <div class="time">
                    <div class="name-t"> Pakkumine lõppeb:  </div>
                    <div class="timer" id="timer">
                        <div>
                            {h10}{h1}
                        </div>
                        <p class="dots">:</p>
                        <div>
                            {m10}{m1}
                        </div>
                        <p class="dots">:</p>
                        <div>  {s10}{s1}

                        </div>
                    </div>
                </div>
                <form action="" method="post">
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abcdg.pro/forms/?target=-4AAIJIALdEwAAAAAAAAAAAAQND_eNAA"></iframe>

                    <!--<input type="hidden" name="total_price" value="24.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIALdEwS0AGWIAAEAAQACRhMBAAKcHwK-AQEABJ0qkNIA">
                    <input type="hidden" name="goods_id" value="109">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="ChocolateSlimEE">
                    <input type="hidden" name="price" value="24">
                    <input type="hidden" name="old_price" value="48">
                    <input type="hidden" name="total_price_wo_shipping" value="24.0">
                    <input type="hidden" name="package_prices" value="{}">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="0">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="EE">
                    <input type="hidden" name="shipment_vat" value="0.0">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="accept_languages" value="pl-pl">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.0">

                    <select class="country_select" id="country_code_selector" name="country">
                        <option value="EE">Eesti</option>
                    </select>
                    <input name="name" placeholder="Sinu nimi" type="text"/>
                    <input class="only_number" name="phone" placeholder="Sinu telefoni number" type="text"/>
                    <input class="js_submit button" type="submit" value="Telli"/>-->
                </form>
            </div>
        </div>
    </section>
    </body>
    </html>
<?php } ?>