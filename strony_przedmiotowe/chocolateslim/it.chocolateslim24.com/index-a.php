
<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 8312 -->
    <script>var locale = "it";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAJsEwQS41-IAAEAAQACLxkBAAJ4IAIGAQEABMn7giUA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 0;
        var name_hint = 'Bruno Bellini';
        var phone_hint = '+393476736735';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a><br><a class="download" href="#">Download our Tips!</a></div>');

            moment.locale("it");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));
            $('.download').click(function(e) {
                e.preventDefault();  //stop the browser from following
                window.location.href = 'out_tips.pdf';
            });

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->



    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <title>Chocolate Slim</title>
    <link href="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/css/css.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Lobster&amp;subset=cyrillic,latin-ext,vietnamese" rel="stylesheet"/>
    <script src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/js/main.js" type="text/javascript"></script>
    <style>
        #header{ background: saddlebrown;}
        #info-block{ background: none;}
        #reviews{
            background: lightblue;}
    </style>
</head>
<body>
<div class="hide-it">
    <!-- <header> -->
    <div id="header">
        <div class="width">
            <div class="block">

                <h2>Le diverse diete vegetariane</h2>
                <div class="list">
                    Gli studi degli ultimi decenni si sono concentrati in particolare sulla dieta lacto-ovo-vegetariana e vegetaliana, mentre non esistano ancora valutazioni affidabili e condivise sulle diete crudiste e fruttariane, seguite comunque da una percentuale minima della popolazione mondiale.
                </div>

            </div>
        </div>
    </div>
    <!-- </header> -->
    <!-- <steps> -->
    <div id="steps">
        <div class="width">
            <div class="title"></div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico1.jpg"/>
                </div>
                <p></p>
            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico2.jpg"/>
                </div>
                <p></p>
            </div>
            <div class="item">
                <div>
                    <span></span>
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico3.jpg"/>
                </div>
                <p></p>
            </div>
            <div class="clear"></div>
            <div class="text">
                <div class="title">Caratteristiche</div>
                <p>Chi segue la dieta vegana per motivi salutistici ritiene che gli alimenti di origine animale non siano necessari se non dannosi all'organismo, ritenendo una dieta priva di carne sia tendenzialmente più sana, sia per la natura dei derivati animali in sé, sia per le moderne metodologie di allevamento, che fanno largo uso di prodotti chimici durante varie fasi del ciclo di produzione. Seguendo i risultati di alcuni studi epidemiologici si considerano gli alimenti di derivazione animale all'origine di larga parte delle cosiddette malattie del benessere, come le patologie cardiovascolari, il diabete, l'obesità, alcuni tipi di cancro, etc. Altre motivazioni possono essere la bioaccumulazione negli animali, al vertice della catena alimentare delle sostanze tossiche e inquinanti presenti nell'ambiente, la presenza nei prodotti animali dei farmaci antibiotici e delle sostanze anabolizzanti dei mangimi, lo studio comparativo del sistema digerente umano rispetto a quello onnivoro o frugivoro. Si ricorda nuovamente che nella maggioranza dei casi nel definire i regimi dietici vegetariani non interviene solo l'aspetto salutistico, come nell'ortoressia, o valutazioni di gusto e tradizione, come accade spesso nella dieta onnivora, ma è sufficiente la sicurezza di non fare violenza al proprio corpo sottraendogli nutrienti necessari, per lasciare poi spazio a considerazioni di ordine etico.
                </p>

            </div>
        </div>
    </div>
    <!-- </steps> -->
    <!-- <sostav> -->
    <div id="sostav">
        <div class="width">
            <div class="title"></div>
            <h2></h2>
            <div class="item first">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img1.png"/>
                <div class="message">

                    <p>È dunque importante come fondante possibilità di partenza che sia compreso come l'organismo umano non necessiti di consumare prodotti animali, ed in base a questa premessa diventa poi realizzabile ad esempio una dieta vegetaliana, tale perché cerca di evitare qualsiasi forma di sfruttamento e violenza contro gli animali non-umani, escludendo un consumo anche occasionale, anche se non sono state rilevate possibili controindicazioni mediche dal suo utilizzo.  </p>
                </div>
            </div>
            <div class="item first right">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img2.png"/>
                <div class="message">
                    <div>Critiche agli alimenti di origine animale</div>
                    <p></p>
                </div>
            </div>
            <div class="clear"></div>
            <div class="item second">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img3.png"/>
                <div class="message">
                    <div>Carne
                    </div>
                    <p>Nel particolare è considerata dannosa la carne: un consumo di carne (rossa, bianca, di pesce) sarebbe la causa principale o il fattore scatenante di molte malattie. In particolare viene evidenziato il rapporto tra consumo di carne e tumori, e alcuni studi medici e scientifici sembrano confermare questo collegamento.</p>
                </div>
            </div>
            <div class="item second right">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img4.png"/>
                <div class="message">
                    <div>Pesce</div>
                    <p>La rinuncia al pesce, alimento universalmente considerato più salutare della carne, viene invece giustificata con il crescente inquinamento delle acque, e in particolare con la presenza di mercurio.</p>
                </div>
            </div>
            <div class="clear"></div>
            <div class="item third">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img5.png"/>
                <div class="message">
                    <div>Latte e latticini
                    </div>
                    <p>
                        Le critiche al consumo di latte e di prodotti caseari si concentrano prevalentamente su due temi: "	in primo luogo alcuni nutrizionisti considerano il latte, ed in particolare quello vaccino, un alimento di difficile digeribilità, scarsamente adatto al consumo umano. Si calcola che un numero di neonati tra lo 0,5% ed il 4% del totale manifesti segni di intolleranza alle proteine del latte, che a volte diventano vere e proprie allergie. Si calcola che oggi, in Italia, circa un terzo degli italiani sia intollerante al lattosio. Sono stati inoltre evidenziati legami tra consumo di latte e diabete giovanile, malattie cardio-vascolari (per l'elevata quantità di grassi saturi), e la presenza di tracce di antibiotici, ormoni e pesticidi. "	in secondo luogo le critiche si appuntano sulla filiera di produzione del latte, che è identica a quella della carne: acquistare del latte, soprattutto se non biologico, vuol dire contribuire allo sfruttamento intensivo degli animali

                    </p>
                </div>
            </div>
            <div class="item third right">
                <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img6.png"/>
                <div class="message">
                    <div>Caratteristiche delle diete vegetariane </div>
                    <p>
                        Le diete vegetariane escludono alcuni o tutti gli alimenti di origine animale, ad esempio carne e pesce, uova, latte e latticini, e talvolta anche alcuni prodotti ottenuti in altra maniera dal mondo animale come il miele. Non tutti seguono gli stessi criteri, mentre alcuni possono essere più o meno "flessibili", alcuni intraprendono regimi alimentari più stretti: i granivori mangiano solo cereali, i frugivori o fruttariani soltanto frutta e i crudisti mangiano alimenti totalmente crudi (per conservare le proprietà nutritive degli stessi). Essendo regimi che puntano a un'alimentazione nel rispetto dell'ambiente e della salute, si preferiscono cibi provenienti da coltivazioni biologiche e biodinamiche, non si fa uso di nessun prodotto raffinato, e si fa utilizzo estensivo di prodotti ricavati dalla soia (come tofu e tempeh), e vi è spesso l'introduzione di alimenti particolari e di uso non comune come shoyu, soba, seitan, tè bancha, natto, alghe alimentari, daikon, gomasio, umeboshi, shiitake, arrow-root, kuzu, miso, azuki, mochi, patate taro, molto uso di semi di girasole, semi di sesamo, semi di lino, tahin, germogli consumati preferibilmente crudi, cereali antichi come kamut e farro, uso di dolcificanti naturali come lo sciroppo d'acero, il malto, il succo d'agave, l'amasake, tutti prodotti che si rifanno allo stile dell'alimentazione macrobiotica, cercando se possibile di rispettare anche gli andamenti della natura consumando prevalentemente prodotti di stagione. Non solo la qualità del cibo ma molti curano anche tipo e tempo di cottura, l'uso della fiamma, della pressione e del sale, come del caffè, dell'alcol per massimizzare l'assimibilità dei micronutrienti quando necessario.
                    </p>   </div>
            </div>
            <div class="clear"></div>
            <div class="plaha"></div>
        </div>
    </div>
    <!-- </sostav> -->
    <!-- <result> -->

    <!-- </result> -->
    <!-- <info-block> -->
    <div id="info-block">
        <div class="width">
            <div class="title">L'equilibrio dei nutrienti</div>
            <p>
                L'equilibrio dei nutrienti è il punto più controverso. Secondo alcuni studiosi la dieta vegana è sbilanciata nell'apporto di macro- e micronutrienti e non sembra possibile (a differenza della dieta vegetariana) ottenere in alcun modo un apporto di nutrienti corretto. Secondo altri studiosi, e naturalmente secondo i vegani, questo è vero solo se la dieta è disordinata e poco varia, e quindi un vegano dovrebbe essere al corrente delle carenze a cui potrebbe incorrere e dovrebbe comportarsi di conseguenza. Vi sono diverse opinioni in merito ad una scelta alimentare vegana, ma non vi è tuttora una posizione unanime nel mondo scientifico. Le critiche alla dieta vegana riguardano le insufficienze alimentari che una dieta del genere comporterebbe, vere o presunte, come l'apparente carenza di alcuni importanti principi nutritivi che l'alimentazione carnea fornisce in dosi sufficienti: proteine, ferro, e alcune vitamine. I vegetariani rispondono con l'elevata presenza di questi principi in alimenti come i legumi (soprattutto la soia), i germogli e i cereali. Effettivamente, se la dieta è comunque varia ed equilibrata, il problema sembra legato all'assimilazione più che alla carenza di queste sostanze. In particolare il ferro contenuto nella carne sembra essere maggiormente biodisponibile rispetto a quello contenuto in altri alimenti di origine vegetale.

               </p>
        </div>
    </div>
    <!-- </info-block> -->

    <!-- <reviews> -->
    <div id="reviews">
        <div class="width">
            <div class="title"></div>
            <h2></h2>
            <div class="review">
                <div class="date">

                </div>
                <div class="name">Spettro degli aminoacidi</div>
                <div class="email"></div>
                <p>

                    La ragione principale delle scelte vegane per ragioni salutistiche è la demonizzazione della carne e dei derivati animali. Tuttavia in una dieta non vegana (onnivora) bilanciata, gli alimenti animali apportano il 70% delle proteine necessarie. I vegani cercano di integrare l'alimentazione con cereali e legumi ricchi di proteine, tuttavia i singoli vegetali non dispongono di tutti gli amminoacidi necessari. Secondo alcuni, ma questo è controverso, la soia li possiede invece tutti. È quindi necessario alimentarsi con uno spettro molto ampio di vegetali per assumere tutti gli amminoacidi necessari al corpo umano.

                </p>
            </div>
            <div class="review center">
                 <div class="date">

                </div>
                <div class="name">Vitamina B12</div>
                <div class="email"></div>
                <p>
                    Una possibile conseguenza della adozione per lunghi periodi di tempo della dieta vegana è la deficienza di cianocobalamina, detta anche vitamina B12, che si ritiene presente in quantità e forme assimilabili solo in alimenti di origine animale. Alcune fonti non
                    animali di vitamina B12 inizialmente proposte, come la spirulina o l'alga nori, sono state successivamente trovate inadeguate in studi compiuti dagli stessi vegani. Infatti nonostante che alcuni organismi come le alghe e alcuni prodotti fermentati ne contengano una certa percentuale, essa non presenta le medesime caratteristiche della B12 presente nei prodotti di origine animale e non ne è comprovata l'effettiva assimilabilità
                    da parte dell'organismo umano. Rimane comunque la possibilità di utilizzare integratori o alimenti appositamente arricchiti di vitamina B12,
                </p>
            </div>
            <div class="review">
                 <div class="date">

                </div>
                <div class="name">Proporzione dei nutrienti</div>
                <div class="email"></div>
                <p>
                    sempre che la stessa sia stata ottenuta da fonti non animali, oppure servirsi di alimenti non arricchiti che abbiano però un contenuto certificato di cianocobalamina (ad esempio alcuni estratti di lievito).
                    Il problema nell'assumere costantemente per anni prodtti di sintesi è quello dell'aumentato rischio di introduzione di sostanze di scarto tossiche come alcoli alifatitici o tetracloruro di carbonio usati per la purificazione del composto. Una carenza di vitamina B12 si può manifestare anche dopo moltissimi anni dalla sua non assunzione, per un periodo che va da uno a vent'anni, quindi relative carenze non sono immediatamente evidenti.

                    In alcuni casi limite, inoltre, si finisce per assumere una quantità eccessiva di carboidrati, che in quantità così notevoli vengono trasformati in grassi e portano quindi al sovrappeso.
                </p>
            </div>
            <div class="clear"></div>
            <div class="buy">

            </div>
        </div>
    </div>
    <!-- </reviews> -->
    <!-- <info-block2> -->
    <div id="info-block2">
        <div class="width">
            <div class="left">
                <div class="title">
                    <div>Micronutrienti</div>
                </div>
                <p>Un altro problema è che in una dieta priva di pesce non si assumono alcuni acidi grassi essenziali (EPA e DHA) che non vengono sintetizzati dal corpo umano a partire da altre sostanze. Tuttavia è possibile introdurre buone fonti di acido alfa-linolenico nella dieta, includendo cibi come i semi di lino e l'olio di semi di lino, che - secondo recenti studi in ambito di nutrizione umana - sono in grado di fornire adeguate quantità di Acidi Grassi Omega-3, ad un costo molto inferiore delle fonti di origine animale.</p>
            </div>
            <div class="right">
                <div class="title">
                    <div>Il peperoncino
                    </div>
                </div>
                <p>Fresco o essiccato, crudo o cotto, il peperoncino viene tanto decantato quanto denigrato, spesso per credenze che non hanno prove scientifiche. Uno studio recente sembra smentire l'idea diffusa che mangiarne tanto possa causare la comparsa di emorroidi, o peggiorarne i sintomi, liberando le persone che soffrono di questo disturbo da un regime alimentare che ne limiti l'uso. Lo studio - di tipo cross over randomizzato - non ha utilizzato la cucina messicana o italiana, ma pastiglie contenenti polvere di peperoncino oppure una sostanza inerte (placebo). I 48 pazienti che hanno preso parte alla ricerca, considerati dagli autori un campione di dimensioni adeguate per gli obiettivi dello studio, hanno ricevuto due pastiglie non distinguibili tra loro, una con peperoncino l'altra con placebo, che hanno ingerito durante il pasto a una settimana di distanza una dall'altra. I pazienti, affetti da emorroidi di secondo e terzo grado, hanno valutato sintomi quali dolore, bruciore, gonfiore e sanguinamento anale.
                       </p>
            </div>
            <div class="left">
                <div class="title">
                    <div>
                    </div>
                </div>
                <p>  Per decidere quanta polvere di peperoncino utilizzare per la pastiglia di trattamento, i ricercatori hanno seguito le indicazioni di un membro dell'Associazione insegnanti di cucina italiana, secondo cui per rendere una pietanza piccante la quantità di polvere necessaria e sufficiente è pari alla punta di un coltello, calcolata su una media di misurazioni come pari a 10 mg. La polvere di peperoncino è stata ottenuta macinando insieme frutti e semi della specie Capsicum frutescens. </p>
            </div>
            <div class="right">
                <div class="title">
                    <div>Gli effetti del peperoncino
                    </div>
                </div>
                <p>
                    La gravità dei sintomi è stata valutata da ogni paziente dopo sei ore, dopo un giorno e dopo due giorni dall'assunzione di ciascuna pastiglia, utilizzando una scala graduata da zero a dieci. Le persone coinvolte sono state invitate a non assumere durante lo studio pepe, alcol, caffè e cioccolato, cibi potenzialmente associati a disturbi gastrointestinali o sintomi emorroidali. "I risultati non mostrano differenze significative nella gravità dei sintomi riportata da chi ha assunto peperoncino e da chi ha invece assunto placebo, per nessuno dei sintomi valutati", spiega Donato Altomare, tra gli autori della ricerca. "Non ci sono ragioni insomma perché pazienti affetti da emorroidi non mangino cibo piccante, se lo desiderano". In definitiva, una credenza diffusa da riconsiderare.  </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <!-- </info-block2> -->
    <!-- <how-buy> -->

    <!-- </how-buy> -->
    <!-- <how-buy> -->

    <!-- </how-buy> -->

    <!-- </footer> -->
</div>
</body>
</html>


