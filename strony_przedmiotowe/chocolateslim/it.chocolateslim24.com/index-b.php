<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 8312 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJsEwQS41-IAAEAAQACLxkBAAJ4IAIGAQEABMn7giUA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Bruno Bellini';
            var phone_hint = '+393476736735';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
        <title>Chocolate Slim</title>
        <link href="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/js/main.js" type="text/javascript"></script>
        <link href="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/css/custom-styles.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <div class="hide-it">
        <!-- <header> -->
        <div id="header">
            <div class="width">
                <div class="block">
                    <h1>Chocolate Slim</h1>
                    <h2>Complesso dimagrante naturale</h2>
                    <div class="list">
                        Riduce velocemente il peso in eccesso <br/>
                        Combatte la cellulite <br/>
                        Elimina brufoli e acne
                    </div>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">Offerta imperdibile! 50% di sconto!</div>
                    </div>
                    <div class="price">
                        <del>78 €
                        </del>
                        <div>39 €
                        </div>
                    </div>
                    <a class="button" href="#footer">Ordina</a>
                </div>
            </div>
        </div>
        <!-- </header> -->
        <!-- <steps> -->
        <div id="steps">
            <div class="width">
                <div class="title">Come perdere 10 chili?</div>

                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ico3.jpg"/>
                    </div>
                    <p>Preparalo in una tazza</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ico2.jpg"/>
                    </div>
                    <p>Bevilo la mattina</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ico1.jpg"/>
                    </div>
                    <p>Ciclo di 2-4 settimane</p>
                </div>
                <div class="clear"></div>
                <div class="text">
                    <div class="title">Il cioccolato Chocolate Slim</div>
                    <p>
                        è un complesso dimagrante a base di ingredienti naturali che fanno leva l'uno sull'azione
                        dell'altro. <br/>I risultati hanno superato ogni aspettativa!
                    </p>
                </div>
            </div>
        </div>
        <!-- </steps> -->
        <!-- <sostav> -->
        <div id="sostav">
            <div class="width">
                <div class="title">Perché Chocolate Slim <br/>è così straordinariamente efficace?</div>
                <h2>6 componenti naturali</h2>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img1.png"/>
                    <div class="message">
                        <div>Fibra</div>
                        <p>Riduce la quantità di colesterolo nel sangue</p>
                    </div>
                </div>
                <div class="item first right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img2.png"/>
                    <div class="message">
                        <div>Leticina di soia</div>
                        <p>Bruciagrassi naturale, previene l’accumulo di grasso</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item second">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img3.png"/>
                    <div class="message">
                        <div>Proteina del siero del latte</div>
                        <p>Questo permette di mantenere il tono muscolare, proteggendolo dal danneggiamento.</p>
                    </div>
                </div>
                <div class="item second right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img4.png"/>
                    <div class="message">
                        <div>Glucomannano</div>
                        <p>Energizza, previene l’accumulo di grasso</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item third">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img5.png"/>
                    <div class="message">
                        <div>Cacao</div>
                        <p>Il cacao naturale accelera la lipolisi e rallenta il processo di invecchiamento</p>
                    </div>
                </div>
                <div class="item third right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img6.png"/>
                    <div class="message">
                        <div>Il complesso vitaminico e minerale</div>
                        <p>Accelera il metabolismo dei grassi, migliora il funzionamento di tutti i sistemi del corpo e riduce il glucosio e il colesterolo nel sangue</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="plaha">Biologico</div>
            </div>
        </div>
        <!-- </sostav> -->
        <!-- <result> -->
        <div id="result">
            <div class="width">
                <div class="title">Risultato: con Chocolate Slim dimagriscono tutti!</div>
                <h2>Più di 7.000 donne felici che hanno perso peso!</h2>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img7.jpg"/>
                    <p>Il grasso sparisce proprio davanti ai tuoi occhi</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img8.jpg"/>
                    <p>Non vorrai più mangiare</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img9.jpg"/>
                    <p>Sarai pieno di energia</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img10.jpg"/>
                    <p>Niente più voglie di zucchero</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/img11.jpg"/>
                    <p>Buonumore</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </result> -->
        <!-- <info-block> -->
        <div id="info-block">
            <div class="width">
                <div class="title">Dimagrire non è mai stato <br/>così semplice!</div>
                <p>
                    Con il complesso Chocolate Slim puoi perdere fino a 24 kg in 4 settimane. Durante la fase di
                    dimagrimento sarai di buon umore e pieno di energia! Goditi la tua perdita di peso! Il cioccolato è un
                    alimento esclusivo che vorresti mangiare in continuazione.
                    <br/><br/>
                    Questo prodotto non è un integratore alimentare, contiene solo componenti naturali. Gli elementi
                    essenziali sono cacao naturale, ed estratto di Ganoderma Lucidum. È un
                    prodotto adatto per l'uso quotidiano e non ha controindicazioni. Rimuove la causa all'origine della
                    comparsa del peso in eccesso normalizzando i processi metabolici del corpo.
                    <br/><br/>
                    Questo prodotto ha fatto una vera e propria rivoluzione nel campo della correzione della figura, della
                    salute e del ringiovanimento.
                </p>
            </div>
        </div>
        <!-- </info-block> -->
        <!-- <compare> -->
        <div id="compare">
            <div class="width">
                <div class="title">Chocolate Slim paragonato ad altri <br/>metodi dimagranti ordinari</div>
                <h2>Abbiamo intervistato un gruppo di 15 donne che volevano dimagrire, i risultati parlano da soli:</h2>
                <div class="wr">
                    <div class="text1">Chocolate Slim <span>(senza cambiare il proprio stile di vita)</span></div>
                    <ul class="text2 clearfix">
                        <li>kg</li>
                        <li>1a sett.</li>
                        <li>2a sett.</li>
                        <li>3a sett.</li>
                        <li>4a sett.</li>
                    </ul>
                    <div class="text3">
                        <span>Esercizio fisico 3 volte a settimana</span>
                        <span>Trattamenti dimagranti</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- </compare> -->
        <!-- <reviews> -->
        <div id="reviews">
            <div class="width">
                <div class="title">Chiedi a chi ha già perso peso grazie a Chocolate Slim</div>
                <h2>Persone reali, risultati reali!</h2>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ava1.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-60, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-10, true)</script>
                        </div>
                    </div>
                    <div class="name">Carla Stocco, 25 anni</div>
                    <div class="email">carletta***@gmail.com</div>
                    <p>È una cosa incredibile! Ho perso quasi 18 kg nelle prime tre settimane e altri 7 kg le due settimane
                        successive! La cosa più importante è seguire le istruzioni: una tazza di cioccolata prima o al posto
                        di una colazione leggera, ogni giorno, senza interruzioni. Ovviamente non bisognerebbe mangiare dolci
                        prima di andare a dormire. Se ce l'ho fatta io, puoi farcela anche tu!</p>
                </div>
                <div class="review center">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ava2.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-70, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-4, true)</script>
                        </div>
                    </div>
                    <div class="name">Lorenzo Gualtieri, 44 anni</div>
                    <div class="email">gualtieri****@hotmail.com</div>
                    <p>Mia moglie mi ha comprato Chocolate Slim dopo che avevo provato moltissime altre cose: diversi tipi
                        di tè e pillole dimagranti. In solo un mese e mezzo ho perso 12 kg e non ho intenzione di fermarmi
                        qui. Grazie!</p>
                </div>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ava3.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-30, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-2, true)</script>
                        </div>
                    </div>
                    <div class="name">Stefania Padoan, 32 anni</div>
                    <div class="email">stepad***@libero.it</div>
                    <p>
                        È così bello che Chocolate Slim sia finalmente arrivato in Italia! Ho letto tutto su questo prodotto
                        nel forum e alla fine l'ho provato io stessa. Pensate che ho bevuto la cioccolata per 2 settimane e
                        riesco già a entrare nei jeans, cosa che non potevo fare 4 anni fa :)
                    </p>
                </div>
                <div class="clear"></div>
                <div class="buy">
                    <div>Non esitare, inizia subito a dimagrire!</div>
                    <a class="button" href="#footer">Ordina</a>
                </div>
            </div>
        </div>
        <!-- </reviews> -->
        <!-- <info-block2> -->
        <div id="info-block2">
            <div class="width">
                <div class="info__content clearfix">
                    <div class="info-block_row clearfix">
                        <div class="left info__content_item">
                            <div class="title">
                                <span></span>
                                <div>È dannoso?</div>
                            </div>
                            <p>Chocolate Slim è interamente composto da ingredienti naturali senza parabeni, coloranti,
                                aromatizzanti sintetici e prodotti OGM. <br/>Leggi le istruzioni.</p>
                        </div>
                        <div class="right info__content_item">
                            <div class="title">
                                <span></span>
                                <div>Quanto tempo mi serve <br/>per perdere 10 chili?</div>
                            </div>
                            <p>Una serie di studi clinici hanno dimostrato che, in media, un ciclo di un mese di Chocolate
                                Slim
                                è
                                sufficiente per ottenere buoni risultati. Si possono osservare i primi risultati dal terzo
                                giorno
                                di
                                utilizzo!</p>
                        </div>
                    </div>
                    <div class="info-block_row clearfix">
                        <div class="left info__content_item">
                            <div class="title">
                                <span></span>
                                <div>Dove posso comprarlo?</div>
                            </div>
                            <p>
                                In Italia la nostra azienda è l'unico fornitore ufficiale del cioccolato Chocolate Slim!
                                Tutti gli
                                altri sono falsi! Puoi effettuare un ordine SOLO sul nostro sito.
                            </p>
                        </div>
                        <div class="right info__content_item">
                            <div class="title">
                                <span></span>
                                <div>Attenzione <br/>ai falsi!</div>
                            </div>
                            <p>
<span class="notru">Attenzione ai falsi! Compra solo il prodotto Chocolate Slim originale!
							</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </info-block2> -->
        <!-- <how-buy> -->
        <div id="how-buy">
            <div class="width">
                <div class="title">Come ordinare Chocolate Slim?</div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ico4.jpg"/>
                    <p>Compila il modulo sul nostro sito</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ico5.jpg"/>
                    <p>Attendi la chiamata di un operatore</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/ico6.jpg"/>
                    <p>Consegna e pagamento</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <how-buy> -->
        <div>
            <div class="width">
                <div id="howuse">
                    <h2>Usare <b>Chocolate Slim</b> è facilissimo!</h2>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/hu_step1.jpg"/>
                        <p class="hu_title">PREPARA</p>
                        <p><b>Prepara il cocktail ogni mattina con 250 ml di latte</b>, aggiungendo 1-2 cucchiai della
                            miscela se sei donna e 2-3 cucchiai se
                            sei uomo.</p>
                    </div>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/hu_step2.jpg"/>
                        <p class="hu_title">BEVI</p>
                        <p><b>Bevi il cocktail preparato al posto della colazione</b> e il tuo corpo riceverà: 217 kcal, 10
                            gr di carboidrati, 17 gr di proteine,
                            23 microelementi, vitamine e fibre.
                        </p>
                    </div>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/mobile/img/hu_step3.jpg"/>
                        <p class="hu_title">RIPETI</p>
                        <p>Durante il giorno <b>sostituisci un altro pasto con il cocktail</b> per consolidare il risultato.
                        </p>
                    </div>
                    <p style="clear: both; float: none; width: 0; height: 0;"></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <footer> -->
        <div id="footer">
            <div class="width">
                <div class="left">
                    <h1>Chocolate Slim</h1>
                    <h2>Complesso dimagrante naturale</h2>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">Offerta imperdibile! 50% di sconto!</div>
                    </div>
                    <div class="price">
                        <del> 78 €
                        </del>
                        <div>39 €
                        </div>
                    </div>
                </div>
                <div class="right">
                    <div class="time">
                        <div class="name-t">L'offerta scade tra:</div>
                        <div id="timer"></div>
                    </div>
                    <form action="" method="post">

                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIAJsEwAAAAAAAAAAAATCVhSXAA"></iframe>
<!--
                        <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="39.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAJsEwQS41-IAAEAAQACLxkBAAJ4IAIGAQEABMn7giUA">
                        <input type="hidden" name="goods_id" value="109">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Chocolateslim_IT_Choko_short">
                        <input type="hidden" name="price" value="39">
                        <input type="hidden" name="old_price" value="78">
                        <input type="hidden" name="al" value="8312">
                        <input type="hidden" name="total_price_wo_shipping" value="39.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="IT">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <select class="country_select" id="country_code_selector" name="country">
                            <option value="IT">Italia</option>
                        </select>
                        <input data-count-length="2+" name="name" placeholder="Nome Cognome" type="text" value=""/>
                        <input class="only_number" name="phone" placeholder="Telefono" type="text" value=""/>
                        <div class="prices">
                            <div class="total">
                            </div>
                        </div>
                        <input class="button js_submit" type="submit" value="Ordina"/>-->
                    </form>
                </div>
            </div>
        </div>
        <!-- </footer> -->
    </div>
    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 8312 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJsEwQS41-IAAEAAQACLxkBAAJ4IAIGAQEABMn7giUA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Bruno Bellini';
            var phone_hint = '+393476736735';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <title>Chocolate Slim</title>
        <link href="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Lobster&amp;subset=cyrillic,latin-ext,vietnamese" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="hide-it">
        <!-- <header> -->
        <div id="header">
            <div class="width">
                <div class="block">
                    <h1>Chocolate Slim</h1>
                    <h2>Complesso dimagrante naturale</h2>
                    <div class="list">
                        Riduce velocemente il peso in eccesso <br/>
                        Combatte la cellulite <br/>
                        Elimina brufoli e acne
                    </div>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">Offerta imperdibile! 50% di sconto!</div>
                    </div>
                    <div class="price">
                        <del>78 €
                        </del>
                        <div>39 €
                        </div>
                    </div>
                    <a class="button" href="#footer">Ordina</a>
                </div>
            </div>
        </div>
        <!-- </header> -->
        <!-- <steps> -->
        <div id="steps">
            <div class="width">
                <div class="title">Come perdere 10 chili?</div>

                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ico3.jpg"/>
                    </div>
                    <p>Preparalo in una tazza</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ico2.jpg"/>
                    </div>
                    <p>Bevilo la mattina</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ico1.jpg"/>
                    </div>
                    <p>Ciclo di 2-4 settimane</p>
                </div>

                <div class="clear"></div>
                <div class="text">
                    <div class="title">Il cioccolato Chocolate Slim</div>
                    <p>
                        è un complesso dimagrante a base di ingredienti naturali che fanno leva l'uno sull'azione
                        dell'altro. <br/>I risultati hanno superato ogni aspettativa!
                    </p>
                </div>
            </div>
        </div>
        <!-- </steps> -->
        <!-- <sostav> -->
        <div id="sostav">
            <div class="width">
                <div class="title">Perché Chocolate Slim <br/>è così straordinariamente efficace?</div>
                <h2>6 componenti naturali</h2>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img1.png"/>
                    <div class="message">
                        <div>Fibra</div>
                        <p>Bruciagrassi naturale, previene l’accumulo di grasso</p>
                    </div>
                </div>
                <div class="item first right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img2.png"/>
                    <div class="message">
                        <div>Leticina di soia</div>
                        <p>	Brucia grassi naturale, previene l'accumulo dei grassi</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item second">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img3.png"/>
                    <div class="message">
                        <div>Proteina del siero del latte</div>
                        <p>Questo permette di mantenere il tono muscolare, proteggendolo dal danneggiamento.</p>
                    </div>
                </div>
                <div class="item second right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img4.png"/>
                    <div class="message">
                        <div>Glucomannano</div>
                        <p>Energizza, previene l’accumulo di grasso</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item third">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img5.png"/>
                    <div class="message">
                        <div>Cacao</div>
                        <p>Il cacao naturale accelera la lipolisi e rallenta il processo di invecchiamento</p>
                    </div>
                </div>
                <div class="item third right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img6.png"/>
                    <div class="message">
                        <div>Il complesso vitaminico e minerale</div>
                        <p>Accelera il metabolismo dei grassi, migliora il funzionamento di tutti i sistemi del corpo e riduce il glucosio e il colesterolo nel sangue</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="plaha">Biologico</div>
            </div>
        </div>
        <!-- </sostav> -->
        <!-- <result> -->
        <div id="result">
            <div class="width">
                <div class="title">Risultato: con Chocolate Slim dimagriscono tutti!</div>
                <h2>Più di 7.000 donne felici che hanno perso peso!</h2>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img7.jpg"/>
                    <p>Il grasso sparisce proprio davanti ai tuoi occhi</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img8.jpg"/>
                    <p>Non vorrai più mangiare</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img9.jpg"/>
                    <p>Sarai pieno di energia</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img10.jpg"/>
                    <p>Niente più voglie di zucchero</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/img11.jpg"/>
                    <p>Buonumore</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </result> -->
        <!-- <info-block> -->
        <div id="info-block">
            <div class="width">
                <div class="title">Dimagrire non è mai stato <br/>così semplice!</div>
                <p>
                    Con il complesso Chocolate Slim puoi perdere fino a 24 kg in 4 settimane. Durante la fase di
                    dimagrimento sarai di buon umore e pieno di energia! Goditi la tua perdita di peso! Il cioccolato è un
                    alimento esclusivo che vorresti mangiare in continuazione.
                    <br/><br/>
                    Questo prodotto non è un integratore alimentare, contiene solo componenti naturali. Gli elementi
                    essenziali sono cacao naturale, ed estratto di Ganoderma Lucidum. È un
                    prodotto adatto per l'uso quotidiano e non ha controindicazioni. Rimuove la causa all'origine della
                    comparsa del peso in eccesso normalizzando i processi metabolici del corpo.
                    <br/><br/>
                    Questo prodotto ha fatto una vera e propria rivoluzione nel campo della correzione della figura, della
                    salute e del ringiovanimento.
                </p>
            </div>
        </div>
        <!-- </info-block> -->
        <!-- <compare> -->
        <div id="compare">
            <div class="width">
                <div class="title">Chocolate Slim paragonato ad altri <br/>metodi dimagranti ordinari</div>
                <h2>Abbiamo intervistato un gruppo di 15 donne che volevano dimagrire, i risultati parlano da soli:</h2>
                <div class="wr">
                    <div class="text1">Chocolate Slim <span>(senza cambiare il proprio stile di vita)</span></div>
                    <ul class="text2 clearfix">
                        <li>kg</li>
                        <li>1a sett.</li>
                        <li>2a sett.</li>
                        <li>3a sett.</li>
                        <li>4a sett.</li>
                    </ul>
                    <div class="text3">
                        <span>Esercizio fisico 3 volte a settimana</span>
                        <span>Trattamenti dimagranti</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- </compare> -->
        <!-- <reviews> -->
        <div id="reviews">
            <div class="width">
                <div class="title">Chiedi a chi ha già perso peso grazie a Chocolate Slim</div>
                <h2>Persone reali, risultati reali!</h2>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ava1.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-60, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-10, true)</script>
                        </div>
                    </div>
                    <div class="name">Carla Stocco, 25 anni</div>
                    <div class="email">carletta***@gmail.com</div>
                    <p>È una cosa incredibile! Ho perso quasi 18 kg nelle prime tre settimane e altri 7 kg le due settimane
                        successive! La cosa più importante è seguire le istruzioni: una tazza di cioccolata prima o al posto
                        di una colazione leggera, ogni giorno, senza interruzioni. Ovviamente non bisognerebbe mangiare dolci
                        prima di andare a dormire. Se ce l'ho fatta io, puoi farcela anche tu!</p>
                </div>
                <div class="review center">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ava2.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-70, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-4, true)</script>
                        </div>
                    </div>
                    <div class="name">Lorenzo Gualtieri, 44 anni</div>
                    <div class="email">gualtieri****@hotmail.com</div>
                    <p>Mia moglie mi ha comprato Chocolate Slim dopo che avevo provato moltissime altre cose: diversi tipi
                        di tè e pillole dimagranti. In solo un mese e mezzo ho perso 12 kg e non ho intenzione di fermarmi
                        qui. Grazie!</p>
                </div>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ava3.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-30, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-2, true)</script>
                        </div>
                    </div>
                    <div class="name">Stefania Padoan, 32 anni</div>
                    <div class="email">stepad***@libero.it</div>
                    <p>
                        È così bello che Chocolate Slim sia finalmente arrivato in Italia! Ho letto tutto su questo prodotto
                        nel forum e alla fine l'ho provato io stessa. Pensate che ho bevuto la cioccolata per 2 settimane e
                        riesco già a entrare nei jeans, cosa che non potevo fare 4 anni fa :)
                    </p>
                </div>
                <div class="clear"></div>
                <div class="buy">
                    <div>Non esitare, inizia subito a dimagrire!</div>
                    <a class="button" href="#footer">Ordina</a>
                </div>
            </div>
        </div>
        <!-- </reviews> -->
        <!-- <info-block2> -->
        <div id="info-block2">
            <div class="width">
                <div class="info__content clearfix">
                    <div class="info-block_row clearfix">
                        <div class="left info__content_item">
                            <div class="title">
                                <span></span>
                                <div>È dannoso?</div>
                            </div>
                            <p>Chocolate Slim è interamente composto da ingredienti naturali senza parabeni, coloranti,
                                aromatizzanti sintetici e prodotti OGM. <br/>Leggi le istruzioni.</p>
                        </div>
                        <div class="right info__content_item">
                            <div class="title">
                                <span></span>
                                <div>Quanto tempo mi serve <br/>per perdere 10 chili?</div>
                            </div>
                            <p>Una serie di studi clinici hanno dimostrato che, in media, un ciclo di un mese di Chocolate Slim
                                è
                                sufficiente per ottenere buoni risultati. Si possono osservare i primi risultati dal terzo giorno
                                di
                                utilizzo!</p>
                        </div>
                    </div>
                    <div class="info-block_row clearfix">
                        <div class="left info__content_item">
                            <div class="title">
                                <span></span>
                                <div>Dove posso comprarlo?</div>
                            </div>
                            <p>
                                In Italia la nostra azienda è l'unico fornitore ufficiale del cioccolato Chocolate Slim! Tutti gli
                                altri sono falsi! Puoi effettuare un ordine SOLO sul nostro sito.
                            </p>
                        </div>
                        <div class="right info__content_item">
                            <div class="title">
                                <span></span>
                                <div>Attenzione <br/>ai falsi!</div>
                            </div>
                            <p>
<span class="notru">Attenzione ai falsi! Compra solo il prodotto Chocolate Slim originale!
							</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </info-block2> -->
        <!-- <how-buy> -->
        <div id="how-buy">
            <div class="width">
                <div class="title">Come ordinare Chocolate Slim?</div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ico4.jpg"/>
                    <p>Compila il modulo sul nostro sito</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ico5.jpg"/>
                    <p>Attendi la chiamata di un operatore</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/ico6.jpg"/>
                    <p>Consegna e pagamento</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <how-buy> -->
        <div>
            <div class="width">
                <div id="howuse">
                    <h2>Usare <b>Chocolate Slim</b> è facilissimo!</h2>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/hu_step1.jpg"/>
                        <p class="hu_title">PREPARA</p>
                        <p><b>Prepara il cocktail ogni mattina con 250 ml di latte</b>, aggiungendo 1-2 cucchiai della
                            miscela se sei donna e 2-3 cucchiai se
                            sei uomo.</p>
                    </div>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/hu_step2.jpg"/>
                        <p class="hu_title">BEVI</p>
                        <p><b>Bevi il cocktail preparato al posto della colazione</b> e il tuo corpo riceverà: 217 kcal, 10
                            gr di carboidrati, 17 gr di proteine,
                            23 microelementi, vitamine e fibre.
                        </p>
                    </div>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_IT_Choko_short/img/hu_step3.jpg"/>
                        <p class="hu_title">RIPETI</p>
                        <p>Durante il giorno <b>sostituisci un altro pasto con il cocktail</b> per consolidare il risultato.
                        </p>
                    </div>
                    <p style="clear: both; float: none; width: 0; height: 0;"></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <footer> -->
        <div id="footer">
            <div class="width">
                <div class="left">
                    <h1>Chocolate Slim</h1>
                    <h2>Complesso dimagrante naturale</h2>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">Offerta imperdibile! 50% di sconto!</div>
                    </div>
                    <div class="price">
                        <del>78 €
                        </del>
                        <div>39 €
                        </div>
                    </div>
                </div>
                <div class="right">
                    <div class="time">
                        <div class="name-t">L'offerta scade tra:</div>
                        <div id="timer"></div>
                    </div>
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIAJsEwAAAAAAAAAAAATCVhSXAA"></iframe>

                       <!-- <input type="hidden" name="accept_languages" value="pl-pl">

                        <input type="hidden" name="total_price" value="39.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAJsEwQS41-IAAEAAQACLxkBAAJ4IAIGAQEABMn7giUA">
                        <input type="hidden" name="goods_id" value="109">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Chocolateslim_IT_Choko_short">
                        <input type="hidden" name="price" value="39">
                        <input type="hidden" name="old_price" value="78">
                        <input type="hidden" name="al" value="8312">
                        <input type="hidden" name="total_price_wo_shipping" value="39.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="IT">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <select class="country_select" id="country_code_selector" name="country">
                            <option value="IT">Italia</option>
                        </select>
                        <input data-count-length="2+" name="name" placeholder="Nome Cognome" type="text" value=""/>
                        <input class="only_number" name="phone" placeholder="Telefono" type="text" value=""/>
                        <div class="prices">
                            <div class="total">
                            </div>
                        </div>
                        <input class="button js_submit" type="submit" value="Ordina"/>-->
                    </form>
                </div>
            </div>
        </div>
        <!-- </footer> -->
    </div>
    </body>
    </html>
<?php } ?>