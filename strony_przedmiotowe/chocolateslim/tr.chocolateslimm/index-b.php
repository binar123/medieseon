<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7656 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJlEwRZBFyIAAEAAQAC6BYBAALoHQIGAQEABNlEkz0A";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Gabriela Torres';
            var phone_hint = '+34914123456';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
        <meta charset="utf-8"/>
        <title>Chokolate Slim</title>
        <link href="//st.acstnst.com/content/Brown_Chocolate_slim_ES/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="hide-it">
        <!-- <header> -->
        <div id="header">
            <div class="width">
                <div class="block">
                    <h1>Chocolate Slim</h1>
                    <h2>Complejo natural para adelgazar</h2>
                    <div class="list">
                        Reduce el exceso de peso de forma rápida <br/>
                        Combate la celulitis  <br/>
                        Acaba con las espinillas y el acné
                    </div>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">¡Oferta especial! ¡50 % de descuento!</div>
                    </div>
                    <div class="price">
                        <del>78 €</del>
                        <div>39 €</div>
                    </div>
                    <a class="button" href="#footer">Realizar pedido</a>
                </div>
            </div>
        </div>
        <!-- </header> -->
        <!-- <steps> -->
        <div id="steps">
            <div class="width">
                <div class="title">¿Cómo adelgazar 10 kilos?</div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico1.jpg"/>
                    </div>
                    <p>Tratamiento de 2-4 semanas</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico2.jpg"/>
                    </div>
                    <p>Bebe por las mañanas</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico3.jpg"/>
                    </div>
                    <p>Preparar en una taza</p>
                </div>
                <div class="clear"></div>
                <div class="text">
                    <div class="title">El chocolate Chocolate Slim</div>
                    <p>es un complejo de ingredientes naturales para adelgazar, que aprovecha los beneficios de todos ellos.</p>
                    <p>¡Los resultados exceden cualquier expectativa!</p>
                </div>
            </div>
        </div>
        <!-- </steps> -->
        <!-- <sostav> -->
        <div id="sostav">
            <div class="width">
                <div class="title">¿Por qué es Chocolate Slim <br/>tan extraordinariamente efectivo?</div>
                <h2>6 componentes naturales</h2>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img1.png"/>
                    <div class="message">
                        <div>FIBRA</div>
                        <p>Reduce el apetito, tonifica y aporta energía extra y un efecto positivo en el sistema digestivo</p>
                    </div>
                </div>
                <div class="item first right">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img2.png"/>
                    <div class="message">
                        <div>LECITINA DE SOJA</div>
                        <p>Quemagrasas natural, Previene la acumulación de grasa</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item second">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img3.png"/>
                    <div class="message">
                        <div>PROTEÍNA WHEY</div>
                        <p>Contiene cianidina que bloquea. El desarrollo de las células de grasa</p>
                    </div>
                </div>
                <div class="item second right">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img4.png"/>
                    <div class="message">
                        <div>GLUCOMANÁN</div>
                        <p>Energiza, Previene la acumulación de grasa. En zonas problemáticas</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item third">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img5.png"/>
                    <div class="message">
                        <div>CACAO</div>
                        <p>El cacao natural acelera el proceso de quemar grasas y ralentiza el proceso de envejecimiento.
                            Mejora el sistema inmunológico, elimina los antojos, produce dopamina (hormona de la felicidad)</p>
                    </div>
                </div>
                <div class="item third right">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img6.png"/>
                    <div class="message">
                        <div>EL COMPLEJO DE VITAMINAS Y MINERALES</div>
                        <p>Equilibra el metabolismo de las grasas, aumenta el funcionamiento de todos los sistemas del cuerpo, reduce los niveles de azúcar y de colesterol en la sangre</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="plaha">Orgánico</div>
            </div>
        </div>
        <!-- </sostav> -->
        <!-- <result> -->
        <div id="result">
            <div class="width">
                <div class="title">Resultado: ¡Con Chocolate Slim, todo el mundo adelgaza!</div>
                <h2>¡Más de 7000 mujeres felices ya han adelgazado!</h2>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img7.jpg"/>
                    <p>La grasa desaparece justo delante de tus ojos</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img8.jpg"/>
                    <p>No quieres comer</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img9.jpg"/>
                    <p>Te sientes lleno de energía</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img10.jpg"/>
                    <p>Adiós a los atracones de dulces</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img11.jpg"/>
                    <p>Buen humor</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </result> -->
        <!-- <info-block> -->
        <div id="info-block">
            <div class="width">
                <div class="title">¡Adelgazar nunca había sido <br/>tan fácil!</div>
                <p>
                    Con el complejo Chocolate Slim puedes perder hasta 24 kilos en 4 semanas. Durante el proceso de adelgazamiento, estarás de buen humor y llena de energía. ¡Disfruta adelgazando! El chocolate es uno de esos productos que siempre nos va a apetecer.
                    <br/><br/>
                    Este producto no es un suplemento dietético. Solo contiene componentes naturales. Los componentes principales son cacao natural, bayas goji, café verde y extracto de ganoderma lucidum. El producto es apto para uso diario y no tiene contraindicaciones. Elimina la causa subyacente de la aparición del exceso de peso y normaliza los procesos metabólicos del cuerpo.
                    <br/><br/>
                    Es un producto que ha revolucionado el mundo de la mejora de la figura, la salud y el rejuvenecimiento.
                </p>
            </div>
        </div>
        <!-- </info-block> -->
        <!-- <compare> -->
        <div id="compare">
            <div class="width">
                <div class="title">Comparación de Chocolate Slim con otros <br/>métodos de adelgazamiento</div>
                <h2>Entrevistamos a un grupo de 15 mujeres que querían adelgazar. Los resultados hablan por sí solos:</h2>
                <div class="wr">
                    <div class="text1">Chocolate Slim <span>(No cambiaron su estilo de vida)</span></div>
                    <ul class="text2 clearfix">
                        <li>kilos</li>
                        <li>1 semana</li>
                        <li>2 semanas</li>
                        <li>3 semanas</li>
                        <li>4 semanas</li>
                    </ul>
                    <div class="text3">
                        <span>Ejercicios físicos 3 veces a la semana</span>
                        <span>Fármacos adelgazantes</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- </compare> -->
        <!-- <reviews> -->
        <div id="reviews">
            <div class="width">
                <div class="title">Les preguntamos a personas que ya han perdido peso con Chocolate Slim</div>
                <h2>Gente real, resultados reales.</h2>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ava1.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-60, true)</script></div>
                        <div><script>dtime_nums(-10, true)</script></div>
                    </div>
                    <div class="name">Elisa Camacho, 25 años</div>
                    <div class="email">elisa****@hotmail.com</div>
                    <p>¡Es increíble! Yo perdí casi 18 kilos en las primeras 3 semanas. Y después otros 7 kilos en las siguientes dos semanas. Lo más importante es seguir las instrucciones: 1 taza de chocolate antes de un desayuno ligero o como sustituto de este, todos los días, sin interrupción. No debes comer pastas antes de dormir. Si yo lo hice, ¡tú también puedes!</p>
                </div>
                <div class="review center">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ava2.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-70, true)</script></div>
                        <div><script>dtime_nums(-4, true)</script></div>
                    </div>
                    <div class="name">Lucas del Castillo, 44 años</div>
                    <div class="email">lukis44***@live.com</div>
                    <p>Mi mujer me compró Chocolate Slim después de haber probado cientos de cosas: diversos tipos de tés y pastillas para adelgazar. En solo un mes y medio, he adelgazado 12 kilos y no pienso parar aquí. ¡Gracias!</p>
                </div>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ava3.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-30, true)</script></div>
                        <div><script>dtime_nums(-2, true)</script></div>
                    </div>
                    <div class="name">Emilia Meca, 32 años</div>
                    <div class="email">srtaemily****@gmail.com</div>
                    <p>¡Qué bien que por fin Chocolate Slim haya llegado a España Españ! He leído mucho sobre el producto en foros, y por fin lo he probado yo misma. Imaginaos, llevo 2 semanas tomando chocolate y ya me caben los vaqueros que no podía ponerme desde hacía 4 años :)</p>
                </div>
                <div class="clear"></div>
                <div class="buy">
                    <div>No lo dudéis. ¡Empezad a adelgazar!</div>
                    <a class="button" href="#footer">Realizar pedido</a>
                </div>
            </div>
        </div>
        <!-- </reviews> -->
        <!-- <info-block2> -->
        <div id="info-block2">
            <div class="width">
                <div class="left">
                    <div class="title">
                        <div>¿No es peligroso?</div>
                    </div>
                    <p>Chocolate Slim tiene solo componentes naturales, sin parabenos, colorantes, aromatizantes sintéticos ni productos genéticamente modificados. <br/>Lee las instrucciones.</p>
                </div>
                <div class="right">
                    <div class="title">
                        <div>¿Dónde se puede comprar?</div>
                    </div>
                    <p>
                        En España nuestra empresa es el único proveedor oficial del chocolate Chocolate Slim. ¡Todos los demás son imitaciones! SOLO puedes pedirlo en nuestro sitio web.
                    </p>
                </div>
                <div class="left">
                    <div class="title">
                        <div>¿Cuánto necesito <br/>para perder 10 kilos?</div>
                    </div>
                    <p>Una serie de ensayos clínicos han demostrado que de media un tratamiento de Chocolate Slim es suficiente para conseguir buenos resultados. Los primeros resultados se notan al tercer día de empezar a usarlo.</p>
                </div>
                <div class="right">
                    <div class="title">
                        <div>¡No aceptes <br/>imitaciones!</div>
                    </div>
                    <p>
                        ¡No aceptes imitaciones! ¡Compra solo productos Chocolate Slim originales! Todos los paquetes llevan la marca registrada y un código de registro exclusivo. Si no encuentras el código exclusivo en tu paquete, contacta con nuestros especialistas para recibir ayuda.
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </info-block2> -->
        <!-- <how-buy> -->
        <div id="how-buy">
            <div class="width">
                <div class="title">¿Cómo puedo pedir Chocolate Slim?</div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico4.jpg"/>
                    <p>Rellene el formulario de nuestro sitio web</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico5.jpg"/>
                    <p>Espera la llamada de uno de nuestros operadores</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico6.jpg"/>
                    <p>Recibe tu paquete en tu oficina de correos más cercana</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <how-buy> -->
        <div id="how-buy2">
            <div class="width">
                <div id="howuse">
                    <h2>¡Usar <b>Chocolate Slim</b> es pan comido!</h2>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/hu_step1.jpg"/>
                        <p class="hu_title">PREPARAR</p>
                        <p><b>Prepara el cóctel todas las mañanas utilizando 250 ml de leche</b>, 1-2 cucharas de mezcla para mujeres y 2-3 cucharas para hombres.</p>
                    </div>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/hu_step2.jpg"/>
                        <p class="hu_title">BEBER</p>
                        <p><b>Bébete el cóctel que has preparado en lugar de tomarte el desayuno</b>, ey tu cuerpo recibirá: 217 kcal, 10 gramos de hidratos, 17 gramos de proteínas, 23 microelementos y vitaminas, fibra.</p>
                    </div>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/hu_step3.jpg"/>
                        <p class="hu_title">REPETIR</p>
                        <p>Durante el día, <b>sustituye otra comida por el cóctel</b>, para consolidar los resultados.</p>
                    </div>
                    <p style="clear: both; float: none; width: 0; height: 0;"></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <footer> -->
        <div id="footer">
            <div class="width">
                <div class="left">
                    <h1>Chocolate Slim</h1>
                    <h2>Complejo natural para adelgazar</h2>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">¡Oferta especial! ¡Descuento del 50 %!</div>
                    </div>
                    <div class="price">
                        <del>78 €</del>
                        <div>39 €</div>
                    </div>
                </div>
                <div class="right">
                    <div class="time">
                        <div class="name-t">La oferta termina en:</div>
                        <div id="timer"></div>
                    </div>
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIAJlEwAAAAAAAAAAAAT1evPlAA"></iframe>

                        <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="39.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAJlEwRZBFyIAAEAAQAC6BYBAALoHQIGAQEABNlEkz0A">
                        <input type="hidden" name="goods_id" value="109">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Brown_Chocolate_slim_ES">
                        <input type="hidden" name="price" value="39">
                        <input type="hidden" name="old_price" value="78">
                        <input type="hidden" name="al" value="7656">
                        <input type="hidden" name="total_price_wo_shipping" value="39.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="ES">
                        <input type="hidden" name="shipment_vat" value="0.21">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.1">

                        <select class="country_select" id="country_code_selector" name="country">
                            <option value="ES">España</option>
                        </select>
                        <input name="name" placeholder="Nombre" type="text" value=""/>
                        <input class="only_number" name="phone" placeholder="Teléfono" type="text" value=""/>
                        <div class="prices">
                            <!--div class="ship">Envío: 0 €</div>
                            <div class="total">Precio total: 39 €</div-->
                        <!--  </div>
                          <input class="button js_submit" type="submit" value="Realizar pedido"/>
                          <div class="form_annotation">
                              * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                          </div>-->
                    </form>
                </div>
            </div>
        </div>
        <!-- </footer> -->
    </div>
    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7656 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJlEwQdRFyIAAEAAQAC6BYBAALoHQIGAQEABNAfZHsA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Gabriela Torres';
            var phone_hint = '+34914123456';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
        <meta charset="utf-8"/>
        <title>Chokolate Slim</title>
        <link href="//st.acstnst.com/content/Brown_Chocolate_slim_ES/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="hide-it">
        <!-- <header> -->
        <div id="header">
            <div class="width">
                <div class="block">
                    <h1>Chocolate Slim</h1>
                    <h2>Complejo natural para adelgazar</h2>
                    <div class="list">
                        Reduce el exceso de peso de forma rápida <br/>
                        Combate la celulitis  <br/>
                        Acaba con las espinillas y el acné
                    </div>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">¡Oferta especial! ¡50 % de descuento!</div>
                    </div>
                    <div class="price">
                        <del>78 €</del>
                        <div>39 €</div>
                    </div>
                    <a class="button" href="#footer">Realizar pedido</a>
                </div>
            </div>
        </div>
        <!-- </header> -->
        <!-- <steps> -->
        <div id="steps">
            <div class="width">
                <div class="title">¿Cómo adelgazar 10 kilos?</div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico1.jpg"/>
                    </div>
                    <p>Tratamiento de 2-4 semanas</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico2.jpg"/>
                    </div>
                    <p>Bebe por las mañanas</p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico3.jpg"/>
                    </div>
                    <p>Preparar en una taza</p>
                </div>
                <div class="clear"></div>
                <div class="text">
                    <div class="title">El chocolate Chocolate Slim</div>
                    <p>es un complejo de ingredientes naturales para adelgazar, que aprovecha los beneficios de todos ellos.</p>
                    <p>¡Los resultados exceden cualquier expectativa!</p>
                </div>
            </div>
        </div>
        <!-- </steps> -->
        <!-- <sostav> -->
        <div id="sostav">
            <div class="width">
                <div class="title">¿Por qué es Chocolate Slim <br/>tan extraordinariamente efectivo?</div>
                <h2>6 componentes naturales</h2>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img1.png"/>
                    <div class="message">
                        <div>FIBRA</div>
                        <p>Reduce el apetito, tonifica y aporta energía extra y un efecto positivo en el sistema digestivo</p>
                    </div>
                </div>
                <div class="item first right">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img2.png"/>
                    <div class="message">
                        <div>LECITINA DE SOJA</div>
                        <p>Quemagrasas natural, Previene la acumulación de grasa</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item second">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img3.png"/>
                    <div class="message">
                        <div>PROTEÍNA WHEY</div>
                        <p>Contiene cianidina que bloquea. El desarrollo de las células de grasa</p>
                    </div>
                </div>
                <div class="item second right">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img4.png"/>
                    <div class="message">
                        <div>GLUCOMANÁN</div>
                        <p>Energiza, Previene la acumulación de grasa. En zonas problemáticas</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item third">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img5.png"/>
                    <div class="message">
                        <div>CACAO</div>
                        <p>El cacao natural acelera el proceso de quemar grasas y ralentiza el proceso de envejecimiento.
                            Mejora el sistema inmunológico, elimina los antojos, produce dopamina (hormona de la felicidad)</p>
                    </div>
                </div>
                <div class="item third right">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img6.png"/>
                    <div class="message">
                        <div>EL COMPLEJO DE VITAMINAS Y MINERALES</div>
                        <p>Equilibra el metabolismo de las grasas, aumenta el funcionamiento de todos los sistemas del cuerpo, reduce los niveles de azúcar y de colesterol en la sangre</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="plaha">Orgánico</div>
            </div>
        </div>
        <!-- </sostav> -->
        <!-- <result> -->
        <div id="result">
            <div class="width">
                <div class="title">Resultado: ¡Con Chocolate Slim, todo el mundo adelgaza!</div>
                <h2>¡Más de 7000 mujeres felices ya han adelgazado!</h2>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img7.jpg"/>
                    <p>La grasa desaparece justo delante de tus ojos</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img8.jpg"/>
                    <p>No quieres comer</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img9.jpg"/>
                    <p>Te sientes lleno de energía</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img10.jpg"/>
                    <p>Adiós a los atracones de dulces</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/img11.jpg"/>
                    <p>Buen humor</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </result> -->
        <!-- <info-block> -->
        <div id="info-block">
            <div class="width">
                <div class="title">¡Adelgazar nunca había sido <br/>tan fácil!</div>
                <p>
                    Con el complejo Chocolate Slim puedes perder hasta 24 kilos en 4 semanas. Durante el proceso de adelgazamiento, estarás de buen humor y llena de energía. ¡Disfruta adelgazando! El chocolate es uno de esos productos que siempre nos va a apetecer.
                    <br/><br/>
                    Este producto no es un suplemento dietético. Solo contiene componentes naturales. Los componentes principales son cacao natural, bayas goji, café verde y extracto de ganoderma lucidum. El producto es apto para uso diario y no tiene contraindicaciones. Elimina la causa subyacente de la aparición del exceso de peso y normaliza los procesos metabólicos del cuerpo.
                    <br/><br/>
                    Es un producto que ha revolucionado el mundo de la mejora de la figura, la salud y el rejuvenecimiento.
                </p>
            </div>
        </div>
        <!-- </info-block> -->
        <!-- <compare> -->
        <div id="compare">
            <div class="width">
                <div class="title">Comparación de Chocolate Slim con otros <br/>métodos de adelgazamiento</div>
                <h2>Entrevistamos a un grupo de 15 mujeres que querían adelgazar. Los resultados hablan por sí solos:</h2>
                <div class="wr">
                    <div class="text1">Chocolate Slim <span>(No cambiaron su estilo de vida)</span></div>
                    <ul class="text2 clearfix">
                        <li>kilos</li>
                        <li>1 semana</li>
                        <li>2 semanas</li>
                        <li>3 semanas</li>
                        <li>4 semanas</li>
                    </ul>
                    <div class="text3">
                        <span>Ejercicios físicos 3 veces a la semana</span>
                        <span>Fármacos adelgazantes</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- </compare> -->
        <!-- <reviews> -->
        <div id="reviews">
            <div class="width">
                <div class="title">Les preguntamos a personas que ya han perdido peso con Chocolate Slim</div>
                <h2>Gente real, resultados reales.</h2>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ava1.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-60, true)</script></div>
                        <div><script>dtime_nums(-10, true)</script></div>
                    </div>
                    <div class="name">Elisa Camacho, 25 años</div>
                    <div class="email">elisa****@hotmail.com</div>
                    <p>¡Es increíble! Yo perdí casi 18 kilos en las primeras 3 semanas. Y después otros 7 kilos en las siguientes dos semanas. Lo más importante es seguir las instrucciones: 1 taza de chocolate antes de un desayuno ligero o como sustituto de este, todos los días, sin interrupción. No debes comer pastas antes de dormir. Si yo lo hice, ¡tú también puedes!</p>
                </div>
                <div class="review center">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ava2.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-70, true)</script></div>
                        <div><script>dtime_nums(-4, true)</script></div>
                    </div>
                    <div class="name">Lucas del Castillo, 44 años</div>
                    <div class="email">lukis44***@live.com</div>
                    <p>Mi mujer me compró Chocolate Slim después de haber probado cientos de cosas: diversos tipos de tés y pastillas para adelgazar. En solo un mes y medio, he adelgazado 12 kilos y no pienso parar aquí. ¡Gracias!</p>
                </div>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ava3.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-30, true)</script></div>
                        <div><script>dtime_nums(-2, true)</script></div>
                    </div>
                    <div class="name">Emilia Meca, 32 años</div>
                    <div class="email">srtaemily****@gmail.com</div>
                    <p>¡Qué bien que por fin Chocolate Slim haya llegado a España Españ! He leído mucho sobre el producto en foros, y por fin lo he probado yo misma. Imaginaos, llevo 2 semanas tomando chocolate y ya me caben los vaqueros que no podía ponerme desde hacía 4 años :)</p>
                </div>
                <div class="clear"></div>
                <div class="buy">
                    <div>No lo dudéis. ¡Empezad a adelgazar!</div>
                    <a class="button" href="#footer">Realizar pedido</a>
                </div>
            </div>
        </div>
        <!-- </reviews> -->
        <!-- <info-block2> -->
        <div id="info-block2">
            <div class="width">
                <div class="left">
                    <div class="title">
                        <div>¿No es peligroso?</div>
                    </div>
                    <p>Chocolate Slim tiene solo componentes naturales, sin parabenos, colorantes, aromatizantes sintéticos ni productos genéticamente modificados. <br/>Lee las instrucciones.</p>
                </div>
                <div class="right">
                    <div class="title">
                        <div>¿Dónde se puede comprar?</div>
                    </div>
                    <p>
                        En España nuestra empresa es el único proveedor oficial del chocolate Chocolate Slim. ¡Todos los demás son imitaciones! SOLO puedes pedirlo en nuestro sitio web.
                    </p>
                </div>
                <div class="left">
                    <div class="title">
                        <div>¿Cuánto necesito <br/>para perder 10 kilos?</div>
                    </div>
                    <p>Una serie de ensayos clínicos han demostrado que de media un tratamiento de Chocolate Slim es suficiente para conseguir buenos resultados. Los primeros resultados se notan al tercer día de empezar a usarlo.</p>
                </div>
                <div class="right">
                    <div class="title">
                        <div>¡No aceptes <br/>imitaciones!</div>
                    </div>
                    <p>
                        ¡No aceptes imitaciones! ¡Compra solo productos Chocolate Slim originales! Todos los paquetes llevan la marca registrada y un código de registro exclusivo. Si no encuentras el código exclusivo en tu paquete, contacta con nuestros especialistas para recibir ayuda.
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </info-block2> -->
        <!-- <how-buy> -->
        <div id="how-buy">
            <div class="width">
                <div class="title">¿Cómo puedo pedir Chocolate Slim?</div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico4.jpg"/>
                    <p>Rellene el formulario de nuestro sitio web</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico5.jpg"/>
                    <p>Espera la llamada de uno de nuestros operadores</p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/ico6.jpg"/>
                    <p>Recibe tu paquete en tu oficina de correos más cercana</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <how-buy> -->
        <div id="how-buy2">
            <div class="width">
                <div id="howuse">
                    <h2>¡Usar <b>Chocolate Slim</b> es pan comido!</h2>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/hu_step1.jpg"/>
                        <p class="hu_title">PREPARAR</p>
                        <p><b>Prepara el cóctel todas las mañanas utilizando 250 ml de leche</b>, 1-2 cucharas de mezcla para mujeres y 2-3 cucharas para hombres.</p>
                    </div>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/hu_step2.jpg"/>
                        <p class="hu_title">BEBER</p>
                        <p><b>Bébete el cóctel que has preparado en lugar de tomarte el desayuno</b>, ey tu cuerpo recibirá: 217 kcal, 10 gramos de hidratos, 17 gramos de proteínas, 23 microelementos y vitaminas, fibra.</p>
                    </div>
                    <div class="hu_step">
                        <img src="//st.acstnst.com/content/Brown_Chocolate_slim_ES/img/hu_step3.jpg"/>
                        <p class="hu_title">REPETIR</p>
                        <p>Durante el día, <b>sustituye otra comida por el cóctel</b>, para consolidar los resultados.</p>
                    </div>
                    <p style="clear: both; float: none; width: 0; height: 0;"></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <footer> -->
        <div id="footer">
            <div class="width">
                <div class="left">
                    <h1>Chocolate Slim</h1>
                    <h2>Complejo natural para adelgazar</h2>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">¡Oferta especial! ¡Descuento del 50 %!</div>
                    </div>
                    <div class="price">
                        <del>78 €</del>
                        <div>39 €</div>
                    </div>
                </div>
                <div class="right">
                    <div class="time">
                        <div class="name-t">La oferta termina en:</div>
                        <div id="timer"></div>
                    </div>
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIAJlEwAAAAAAAAAAAAT1evPlAA"></iframe>

                        <!--<input type="hidden" name="accept_languages" value="pl-pl">
                       <!-- <input type="hidden" name="total_price" value="39.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAJlEwQdRFyIAAEAAQAC6BYBAALoHQIGAQEABNAfZHsA">
                        <input type="hidden" name="goods_id" value="109">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Brown_Chocolate_slim_ES">
                        <input type="hidden" name="price" value="39">
                        <input type="hidden" name="old_price" value="78">
                        <input type="hidden" name="al" value="7656">
                        <input type="hidden" name="total_price_wo_shipping" value="39.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="ES">
                        <input type="hidden" name="shipment_vat" value="0.21">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.1">

                        <select class="country_select" id="country_code_selector" name="country">
                            <option value="ES">España</option>
                        </select>
                        <input name="name" placeholder="Nombre" type="text" value=""/>
                        <input class="only_number" name="phone" placeholder="Teléfono" type="text" value=""/>
                        <div class="prices">
                            <!--div class="ship">Envío: 0 €</div>
                            <div class="total">Precio total: 39 €</div-->
                        <!-- </div>
                         <input class="button js_submit" type="submit" value="Realizar pedido"/>
                         <div class="form_annotation">
                             * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                         </div>-->
                    </form>
                </div>
            </div>
        </div>
        <!-- </footer> -->
    </div>
    </body>
    </html>
<?php } ?>