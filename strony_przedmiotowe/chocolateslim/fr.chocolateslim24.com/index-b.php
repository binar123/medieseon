<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 5186 -->
        <script>var locale = "fr";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALPDQSB6GGIAAEAAQACaA0BAAJCFAIGAQLGBgQeSJRNAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
            var shipment_price = 6;
            var name_hint = 'Réni morel';
            var phone_hint = '+336314254';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("fr");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
        <title>Chocolate Slim</title>
        <link href="//st.acstnst.com/content/Chocolateslim_FR_New2/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Chocolateslim_FR_New2/js/main.js" type="text/javascript"></script>
        <link href="//st.acstnst.com/content/Chocolateslim_FR_New2/css/custom-styles.min.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Chocolateslim_FR_New2/js/custom-functions.min.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="hide-it">
        <!-- <header> -->
        <div id="header">
            <div class="width">
                <div class="block">
                    <h1>Chocolate Slim</h1>
                    <h2>Complexe minceur naturel</h2>
                    <div class="list">
                        Réduit rapidement le poids excessif  <br/>
                        Élimine la cellulite  <br/>
                        Supprime les boutons et l'acné
                    </div>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">  À saisir! 50% de réduction!  </div>
                    </div>
                    <div class="price">
                        <del>98€</del>
                        <div>49€</div>
                    </div>
                    <a class="button" href="#footer">Commander </a>
                </div>
            </div>
        </div>
        <!-- </header> -->
        <!-- <steps> -->
        <div id="steps">
            <div class="width">
                <div class="title">  Comment perdre 10 kg?  </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico1.jpg"/>
                    </div>
                    <p>
                        Une cure de 2-4 semaines
                    </p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico2.jpg"/>
                    </div>
                    <p> À boire le matin  </p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico3.jpg"/>
                    </div>
                    <p>À préparer dans une tasse  </p>
                </div>
                <div class="clear"></div>
                <div class="text">
                    <div class="title">  Le chocolat Chocolate Slim  </div>
                    <p>
                        est un complexe d'ingrédients naturels minceur dotés d’action du levier.
                        <br/>  Les résultats qui dépassent toutes les attentes!
                    </p>
                </div>
            </div>
        </div>
        <!-- </steps> -->
        <!-- <sostav> -->
        <div id="sostav">
            <div class="width">
                <div class="title">  Pourquoi Chocolate Slim est   <br/>  si extraordinairement efficace?  </div>
                <h2>  6 composants naturels  </h2>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img1.png"/>
                    <div class="message">
                        <div>  Les grains de café vert  </div>
                        <p>
                            Réduisent l’appétit   <br/>  Effet dynamisant et énergisant.
                        </p>
                    </div>
                </div>
                <div class="item first right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img2.png"/>
                    <div class="message">
                        <div>  Les baies de Goji  </div>
                        <p>
                            Brûleur de graisse naturel   <br/>  Bloquent le dépôt de graisse.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item second">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img3.png"/>
                    <div class="message">
                        <div>  Les baies d'açai  </div>
                        <p>
                            Contiennent de la cyanidine qui bloque   <br/>  le développement des cellules adipeuses.   <br/>  Sourсe naturelle d'antioxydants.
                        </p>
                    </div>
                </div>
                <div class="item second right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img4.png"/>
                    <div class="message">
                        <div>  Les graines de chia  </div>
                        <p>
                            Procurent de l'énergie.   <br/>  Empêchent le dépôt de graisse   <br/>  dans les zones  à problème.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item third">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img5.png"/>
                    <div class="message">
                        <div>  Le cacao naturel  </div>
                        <p>
                            Accélère l'oxydation des graisses.   <br/>  Renforce le système immunitaire tout au long de la cure sveltesse   <br/>  Contrôle l’envie de manger sucré.   <br/>  Produit de la dopamine (l'hormone du bonheur).
                        </p>
                    </div>
                </div>
                <div class="item third right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img6.png"/>
                    <div class="message">
                        <div>  L’extrait de Ganoderma lucidum  </div>
                        <p>
                            Normalise le métabolisme des graisses.   <br/>  Améliore le fonctionnement de l'ensemble des systèmes de l’organisme.   <br/>  Réduit le taux de cholestérol dans le sang.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="plaha"> Biologique  </div>
            </div>
        </div>
        <!-- </sostav> -->
        <!-- <result> -->
        <section id="result">
            <div class="width">
                <div class="title">  Résultat: avec Chocolate Slim tout le monde maigrit!  </div>
                <h2>  Plus de 7000 femmes satisfaites qui sont devenues sveltes!  </h2>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img7.jpg"/>
                    <p>
                        La graisse disparaît devant vos yeux
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img8.jpg"/>
                    <p>
                        Vous n’avez pas fin
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img9.jpg"/>
                    <p>
                        Vous êtes plein d'énergie
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img10.jpg"/>
                    <p>
                        L'envie de manger sucré maîtrisée
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img11.jpg"/>
                    <p>
                        Vous êtes de bonne humeur
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </section>
        <!-- </result> -->
        <!-- <info-block> -->
        <section id="info-block">
            <div class="width">
                <div class="title">  Maigrir n'a jamais été   <br/>  si facile!  </div>
                <p>
                    Avec le complex Chocolate Slim vous pouvez perdre jusqu'à 24 kg en 4 semaines. Au cours de ce processus minceur, vous serez de bonne humeur et rempli d'énergie! C’est de la vraie minceur plaisir! Le chocolat est l’un de ces produits qu’on pourrait manger encore et encore.
                    <br/><br/>
                    Ce produit n’est pas un complément alimentaire, il ne contient que des composants naturels. Les principaux composants sont le cacao naturel, les baies de goji, le café vert et l'extrait de Ganoderma lucidum. Le produit est adapté pour une utilisation quotidienne et n'a pas de contre-indications. Il supprime la cause sous-jacente du poids excessif et  normalise les processus métaboliques dans le corps.
                    <br/><br/>
                    Ce produit a fait une véritable révolution dans le domaine de la sveltesse, la santé et la jeunesse.
                </p>
            </div>
        </section>
        <!-- </info-block> -->
        <!-- <compare> -->
        <div id="compare">
            <div class="width">
                <div class="title">  La comparaison de Chocolate Slim avec les méthodes   <br/>  minceur ordinaires  </div>
                <h2>  Nous avons interviewé un groupe de 15 femmes qui voulaient mincir. Leurs résultats parlent d'eux-mêmes:   </h2>
                <div class="wr">
                    <div class="text1">Chocolate Slim  <span>  (Le mode de vie est resté inchangé)  </span></div>
                    <ul class="text2 clearfix">
                        <li>
                            kg
                        </li>
                        <li>
                            1 semaine
                        </li>
                        <li>
                            2 semaines
                        </li>
                        <li>
                            3 semaines
                        </li>
                        <li>
                            4 semaines
                        </li>
                    </ul>
                    <div class="text3">
                        <span>    Exercices physiques 3 fois par semaine    </span>
                        <span> Médicaments minceur    </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- </compare> -->
        <!-- <reviews> -->
        <div id="reviews">
            <div class="width">
                <div class="title">Demandez à ceux  et celles qui ont déjà perdu du poids avec du chocolat Slim   </div>
                <h2>  De vrais gens, de vrais résultats!  </h2>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ava1.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-60, true)</script></div>
                        <div><script>dtime_nums(-10, true)</script></div>
                    </div>
                    <div class="name">  Alice Wendling, 25 ans  </div>
                    <div class="email">  alice*****@yahoo.fr  </div>
                    <p>
                        Incroyable mais vrai! J’ai perdu près de 18 kilos au cours de 3 premières semaines! Et puis encore 7 kg au cours des deux semaines suivantes! Il faut juste suivre les instructions: 1 tasse de chocolat avant le petit déjeuner léger ou à la place de celui-ci, tous les jours sans exception. Bien sûr, pas de petits gâteaux avant de dormir. Si je l'ai fait, vous le pouvez aussi!
                    </p>
                </div>
                <div class="review center">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ava2.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-70, true)</script></div>
                        <div><script>dtime_nums(-4, true)</script></div>
                    </div>
                    <div class="name">  Benoît Vanadier, 44 ans  </div>
                    <div class="email">  bv*****@free.fr  </div>
                    <p>
                        Ma femme m'a acheté du Chocolate Slim après avoir essayé de nombreux autres produits tels que thés et des cachets pour maigrir. Juste en 1,5 mois, j’ai perdu 12 kg et je ne veux pas m’arrêter là. Je vous remercie!
                    </p>
                </div>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ava3.jpg"/>
                    <div class="date">
                        <div><script>dtime_nums(-30, true)</script></div>
                        <div><script>dtime_nums(-2, true)</script></div>
                    </div>
                    <div class="name">  Emilie Vauclair, 32 ans  </div>
                    <div class="email">  mli*****@laposte.fr  </div>
                    <p>
                        Je suis ravie que Chocolate Slim a finalement paru en   <span class="additional_fields_country_name">  France !  </span>  J’ai beaucoup lu à son sujet  sur les forums et finalement je l'ai essayé moi-même. J'ai bu du chocolat pendant 2 semaines et maintenant je suis capable de rentrer dans mes jeans que je ne pouvais pas mettre depuis 4 ans :)
                    </p>
                </div>
                <div class="clear"></div>
                <div class="buy">
                    <div>   N'hésitez pas, commencez à maigrir immédiatement!  </div>
                    <a class="button" href="#footer">   Commander  </a>
                </div>
            </div>
        </div>
        <!-- </reviews> -->
        <!-- <info-block2> -->
        <div id="info-block2">
            <div class="width">
                <div class="left">
                    <div class="title">
                        <span></span>
                        <div>Est-ce dangereux?</div>
                    </div>
                    <p>  Chocolate Slim est composé entièrement d’ingédients naturels, sans parabène, colorants, aromatisants synthétiques ou des produits OGM.   <br/>
                        Lire les instructions.  </p>
                    <div class="hidden_delivery" style="visibility: visible">
                        <div class="title">
                            <span></span>
                            <div>  Livraison </div>
                        </div>
                        <span class="additional_fields_delivery"> Expédition sur Paris: 2-3 jours à compter de la commande. Pour les autres villes 7-14 jours.    </span>
                    </div>
                </div>
                <div class="right">
                    <div class="title">
                        <span></span>
                        <div>  Où l’acheter?  </div>
                    </div>
                    <p>
                        En   <span class="additional_fields_country_name">  France  </span>   notre société est le seul fournisseur officiel du chocolat Chocolate Slim! Tous les autres produits sont frauduleux! Vous pouvez passer commande SEULEMENT sur notre site.
                        <br/><br/><br/>
                        <span class="notru">  Méfiez-vous des contrefaçons! N’achetez que le produit Chocolate Slim original! Le logo et le code d'enregistrement unique doivent figurer sur chaque paquet. Si vous ne trouvez pas le code unique à l'intérieur du paquet, veuillez contacter nos assistants. de vente.  </span>
                    </p>
                </div>
                <div class="center">
                    <div class="title">
                        <span></span>
                        <div>  De quelle quantité ai-je besoin   <br/>  pour perdre 10 kilos?  </div>
                    </div>
                    <p>
                        Les essais cliniques ont montré qu’en moyenne, une cure d’un mois de Chocolate Slim est suffisante pour obtenir de bons résultats. Les premiers résultats peuvent être remarqués à partir du troisième jour d'utilisation!
                    </p><div class="title second">
                        <span></span>
                        <div>  Attention   <br/>  aux produits contrefaits!  </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </info-block2> -->
        <!-- <how-buy> -->
        <div id="how-buy">
            <div class="width">
                <div class="title">  Comment commander Chocolate Slim?  </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico4.jpg"/>
                    <p>  Remplissez le formulaire sur notre site Web  </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico5.jpg"/>
                    <p>Attendez l'appel de l'opérateur  </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico6.jpg"/>
                    <p> Recevez votre colis   <span class="del_office">  dans le bureau de poste le plus proche </span></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <how-buy> -->
        <div>
            <div class="width">
                <div id="howuse">
                    <h2>  Utiliser   <b>  Chocolate Slim  </b>   est simple comme bonjour!  </h2>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/hu_step1.jpg"/>
                        <p class="hu_title">  PRÉPAREZ   </p>
                        <p><b>  Faites un cocktail chaque matin en utilisant 250 ml de lait,  </b>   comptez 1-2 cuillères de mélange pour les femmes et 2-3 cuillères pour les hommes.  </p>
                    </div>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/hu_step2.jpg"/>
                        <p class="hu_title">   BUVEZ  </p>
                        <p><b>  Buvez le cocktail  au lieu du petit-déjeuner.  </b>   Votre corps recevra: 217 kcal dont 10 g de glucides, 17g de protéines, 23 oligo-éléments et des vitamines, des fibres.  </p>
                    </div>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/hu_step3.jpg"/>
                        <p class="hu_title">  RÉPÉTEZ   </p>
                        <p>  Au cours de la journée   <b>  remplacez un autre repas avec le cocktail,  </b>   pour consolider le résultat.  </p>
                    </div>
                    <p style="clear: both; float: none; width: 0; height: 0;"></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <footer> -->
        <div class=" page2 " id="footer" protected="" style="min-height:900px">
            <div class="width">
                <div class="left">
                    <h1>Chocolate Slim</h1>
                    <h2>  Complexe naturel minceur  </h2>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">À saisir! 50% de réduction! </div>
                    </div>
                    <div class="price">
                        <del>98€</del>
                        <div>49 €</div>
                    </div>
                </div>
                <div class="right" style="">
                    <div class="time">
                        <div class="name-t">L'offre se termine dans:</div>
                        <div id="timer"></div>
                    </div>

                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abdcn.pro/forms/?target=-4AAIJIALPDQAAAAAAAAAAAARAZTGUAA"></iframe>

                </div>
            </div>
        </div>
        <div id="kmacb">
            <a href="#" modal="kmacb-form" title=""></a>
        </div>
        <div class="modal" id="kmacb-form">
            <div class="modal-block">
                <div class="icon-close"></div>
                <div class="title">Avez-vous aimé cette offre?</div>
                <div class="content">
                    <div class="padding">
                        <p>Nous vous disons tout sur ce produit, nous offrons les meilleures conditions et nous sélectionnons des offres spéciales qui vous conviennent! </p>
                        <form action="" method="POST">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIALPDQAAAAAAAAAAAARAZTGUAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="55.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIALPDQSB6GGIAAEAAQACaA0BAAJCFAIGAQLGBgQeSJRNAA">
                            <input type="hidden" name="goods_id" value="109">
                            <input type="hidden" name="title" value="Chocolateslim - FR">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Chocolateslim_FR_New2">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="al" value="5186">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="FR">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                            <input type="hidden" name="shipment_price" value="6">
                            <input type="hidden" name="price_vat" value="0.055">

                            <input name="name" placeholder="Prénom Nom de famille" type="text" value=""/>
                            <input name="phone" placeholder="Numéro de téléphone" type="text" value=""/><br/>
                            <input type="submit" value="Commander"/>-->
                        </form>
                        <p class="bold">Un operateur vous rappellera dans 15-20 minutes </p>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <script src="//st.acstnst.com/content/Chocolateslim_FR_New2/js/jquery.bxslider.min.js"></script>

    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 5186 -->
        <script>var locale = "fr";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALPDQSB6GGIAAEAAQACaA0BAAJCFAIGAQLGBgQeSJRNAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
            var shipment_price = 6;
            var name_hint = 'Réni morel';
            var phone_hint = '+336314254';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("fr");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="width=device-width, height=device-height, initial-scale=1.0" name="viewport"/>
        <title>Chocolate Slim</title>
        <link href="//st.acstnst.com/content/Chocolateslim_FR_New2/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,700,500&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Chocolateslim_FR_New2/js/main.js" type="text/javascript"></script>
        <link href="//st.acstnst.com/content/Chocolateslim_FR_New2/css/custom-styles.min.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Chocolateslim_FR_New2/js/custom-functions.min.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="hide-it">
        <!-- <header> -->
        <div id="header">
            <div class="width">
                <div class="block">
                    <h1>Chocolate Slim</h1>
                    <h2>Complexe minceur naturel</h2>
                    <div class="list">
                        Réduit rapidement le poids excessif  <br/>
                        Élimine la cellulite  <br/>
                        Supprime les boutons et l'acné
                    </div>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">  À saisir! 50% de réduction!  </div>
                    </div>
                    <div class="price">
                        <del>98 €</del>
                        <div>49 €</div>
                    </div>
                    <a class="button" href="#footer">Commander </a>
                </div>
            </div>
        </div>
        <!-- </header> -->
        <!-- <steps> -->
        <div id="steps">
            <div class="width">
                <div class="title">  Comment perdre 10 kg?  </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico1.jpg"/>
                    </div>
                    <p>
                        Une cure de 2-4 semaines
                    </p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico2.jpg"/>
                    </div>
                    <p> À boire le matin  </p>
                </div>
                <div class="item">
                    <div>
                        <span></span>
                        <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico3.jpg"/>
                    </div>
                    <p>À préparer dans une tasse  </p>
                </div>
                <div class="clear"></div>
                <div class="text">
                    <div class="title">  Le chocolat Chocolate Slim  </div>
                    <p>
                        est un complexe d'ingrédients naturels minceur dotés d’action du levier.
                        <br/>  Les résultats qui dépassent toutes les attentes!
                    </p>
                </div>
            </div>
        </div>
        <!-- </steps> -->
        <!-- <sostav> -->
        <div id="sostav">
            <div class="width">
                <div class="title">  Pourquoi Chocolate Slim est   <br/>  si extraordinairement efficace?  </div>
                <h2>  6 composants naturels  </h2>
                <div class="item first">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img1.png"/>
                    <div class="message">
                        <div>  Les grains de café vert  </div>
                        <p>
                            Réduisent l’appétit   <br/>  Effet dynamisant et énergisant.
                        </p>
                    </div>
                </div>
                <div class="item first right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img2.png"/>
                    <div class="message">
                        <div>  Les baies de Goji  </div>
                        <p>
                            Brûleur de graisse naturel   <br/>  Bloquent le dépôt de graisse.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item second">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img3.png"/>
                    <div class="message">
                        <div>  Les baies d'açai  </div>
                        <p>
                            Contiennent de la cyanidine qui bloque   <br/>  le développement des cellules adipeuses.   <br/>  Sourсe naturelle d'antioxydants.
                        </p>
                    </div>
                </div>
                <div class="item second right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img4.png"/>
                    <div class="message">
                        <div>  Les graines de chia  </div>
                        <p>
                            Procurent de l'énergie.   <br/>  Empêchent le dépôt de graisse   <br/>  dans les zones  à problème.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item third">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img5.png"/>
                    <div class="message">
                        <div>  Le cacao naturel  </div>
                        <p>
                            Accélère l'oxydation des graisses.   <br/>  Renforce le système immunitaire tout au long de la cure sveltesse   <br/>  Contrôle l’envie de manger sucré.   <br/>  Produit de la dopamine (l'hormone du bonheur).
                        </p>
                    </div>
                </div>
                <div class="item third right">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img6.png"/>
                    <div class="message">
                        <div>  L’extrait de Ganoderma lucidum  </div>
                        <p>
                            Normalise le métabolisme des graisses.   <br/>  Améliore le fonctionnement de l'ensemble des systèmes de l’organisme.   <br/>  Réduit le taux de cholestérol dans le sang.
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="plaha"> Biologique  </div>
            </div>
        </div>
        <!-- </sostav> -->
        <!-- <result> -->
        <section id="result">
            <div class="width">
                <div class="title">  Résultat: avec Chocolate Slim tout le monde maigrit!  </div>
                <h2>  Plus de 7000 femmes satisfaites qui sont devenues sveltes!  </h2>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img7.jpg"/>
                    <p>
                        La graisse disparaît devant vos yeux
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img8.jpg"/>
                    <p>
                        Vous n’avez pas fin
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img9.jpg"/>
                    <p>
                        Vous êtes plein d'énergie
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img10.jpg"/>
                    <p>
                        L'envie de manger sucré maîtrisée
                    </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/img11.jpg"/>
                    <p>
                        Vous êtes de bonne humeur
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </section>
        <!-- </result> -->
        <!-- <info-block> -->
        <section id="info-block">
            <div class="width">
                <div class="title">  Maigrir n'a jamais été   <br/>  si facile!  </div>
                <p>
                    Avec le complex Chocolate Slim vous pouvez perdre jusqu'à 24 kg en 4 semaines. Au cours de ce processus minceur, vous serez de bonne humeur et rempli d'énergie! C’est de la vraie minceur plaisir! Le chocolat est l’un de ces produits qu’on pourrait manger encore et encore.
                    <br/><br/>
                    Ce produit n’est pas un complément alimentaire, il ne contient que des composants naturels. Les principaux composants sont le cacao naturel, les baies de goji, le café vert et l'extrait de Ganoderma lucidum. Le produit est adapté pour une utilisation quotidienne et n'a pas de contre-indications. Il supprime la cause sous-jacente du poids excessif et  normalise les processus métaboliques dans le corps.
                    <br/><br/>
                    Ce produit a fait une véritable révolution dans le domaine de la sveltesse, la santé et la jeunesse.
                </p>
            </div>
        </section>
        <!-- </info-block> -->
        <!-- <compare> -->
        <div id="compare">
            <div class="width">
                <div class="title">  La comparaison de Chocolate Slim avec les méthodes   <br/>  minceur ordinaires  </div>
                <h2>  Nous avons interviewé un groupe de 15 femmes qui voulaient mincir. Leurs résultats parlent d'eux-mêmes:   </h2>
                <div class="wr">
                    <div class="text1">Chocolate Slim  <span>  (Le mode de vie est resté inchangé)  </span></div>
                    <ul class="text2 clearfix">
                        <li>
                            kg
                        </li>
                        <li>
                            1 semaine
                        </li>
                        <li>
                            2 semaines
                        </li>
                        <li>
                            3 semaines
                        </li>
                        <li>
                            4 semaines
                        </li>
                    </ul>
                    <div class="text3">
                        <span>    Exercices physiques 3 fois par semaine    </span>
                        <span> Médicaments minceur    </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- </compare> -->
        <!-- <reviews> -->
        <div id="reviews">
            <div class="width">
                <div class="title">Demandez à ceux  et celles qui ont déjà perdu du poids avec du chocolat Slim   </div>
                <h2>  De vrais gens, de vrais résultats!  </h2>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ava1.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-60, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-10, true)</script>
                        </div>
                    </div>
                    <div class="name">  Alice Wendling, 25 ans  </div>
                    <div class="email">  alice*****@yahoo.fr  </div>
                    <p>
                        Incroyable mais vrai! J’ai perdu près de 18 kilos au cours de 3 premières semaines! Et puis encore 7 kg au cours des deux semaines suivantes! Il faut juste suivre les instructions: 1 tasse de chocolat avant le petit déjeuner léger ou à la place de celui-ci, tous les jours sans exception. Bien sûr, pas de petits gâteaux avant de dormir. Si je l'ai fait, vous le pouvez aussi!
                    </p>
                </div>
                <div class="review center">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ava2.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-70, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-4, true)</script>
                        </div>
                    </div>
                    <div class="name">  Benoît Vanadier, 44 ans  </div>
                    <div class="email">  bv*****@free.fr  </div>
                    <p>
                        Ma femme m'a acheté du Chocolate Slim après avoir essayé de nombreux autres produits tels que thés et des cachets pour maigrir. Juste en 1,5 mois, j’ai perdu 12 kg et je ne veux pas m’arrêter là. Je vous remercie!
                    </p>
                </div>
                <div class="review">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ava3.jpg"/>
                    <div class="date">
                        <div>
                            <script>dtime_nums(-30, true)</script>
                        </div>
                        <div>
                            <script>dtime_nums(-2, true)</script>
                        </div>
                    </div>
                    <div class="name">  Emilie Vauclair, 32 ans  </div>
                    <div class="email">  mli*****@laposte.fr  </div>
                    <p>
                        Je suis ravie que Chocolate Slim a finalement paru en   <span class="additional_fields_country_name">  France !  </span>  J’ai beaucoup lu à son sujet  sur les forums et finalement je l'ai essayé moi-même. J'ai bu du chocolat pendant 2 semaines et maintenant je suis capable de rentrer dans mes jeans que je ne pouvais pas mettre depuis 4 ans :)
                    </p>
                </div>
                <div class="clear"></div>
                <div class="buy">
                    <div>   N'hésitez pas, commencez à maigrir immédiatement!  </div>
                    <a class="button" href="#footer">   Commander  </a>
                </div>
            </div>
        </div>
        <!-- </reviews> -->
        <!-- <info-block2> -->
        <div id="info-block2">
            <div class="width">
                <div class="left">
                    <div class="title">
                        <span></span>
                        <div>Est-ce dangereux?</div>
                    </div>
                    <p>  Chocolate Slim est composé entièrement d’ingédients naturels, sans parabène, colorants, aromatisants synthétiques ou des produits OGM.   <br/>
                        Lire les instructions.
                    </p>
                    <div class="hidden_delivery" style="visibility: visible">
                        <div class="title">
                            <span></span>
                            <div>  Livraison </div>
                        </div>
                        <span class="additional_fields_delivery"> Expédition sur Paris: 2-3 jours à compter de la commande. Pour les autres villes 7-14 jours.    </span>
                    </div>
                </div>
                <div class="right">
                    <div class="title">
                        <span></span>
                        <div>  Où l’acheter?  </div>
                    </div>
                    <p>
                        En   <span class="additional_fields_country_name">  France  </span>   notre société est le seul fournisseur officiel du chocolat Chocolate Slim! Tous les autres produits sont frauduleux! Vous pouvez passer commande SEULEMENT sur notre site.
                        <br/><br/><br/>
                        <span class="notru">  Méfiez-vous des contrefaçons! N’achetez que le produit Chocolate Slim original! Le logo et le code d'enregistrement unique doivent figurer sur chaque paquet. Si vous ne trouvez pas le code unique à l'intérieur du paquet, veuillez contacter nos assistants. de vente.  </span>
                    </p>
                </div>
                <div class="center">
                    <div class="title">
                        <span></span>
                        <div>  De quelle quantité ai-je besoin   <br/>  pour perdre 10 kilos?  </div>
                    </div>
                    <p>
                        Les essais cliniques ont montré qu’en moyenne, une cure d’un mois de Chocolate Slim est suffisante pour obtenir de bons résultats. Les premiers résultats peuvent être remarqués à partir du troisième jour d'utilisation!
                    </p>
                    <div class="title second">
                        <span></span>
                        <div>  Attention   <br/>  aux produits contrefaits!  </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </info-block2> -->
        <!-- <how-buy> -->
        <div id="how-buy">
            <div class="width">
                <div class="title">  Comment commander Chocolate Slim?  </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico4.jpg"/>
                    <p>  Remplissez le formulaire sur notre site Web  </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico5.jpg"/>
                    <p>Attendez l'appel de l'opérateur  </p>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/ico6.jpg"/>
                    <p> Recevez votre colis   <span class="del_office">  dans le bureau de poste le plus proche </span></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <how-buy> -->
        <div>
            <div class="width">
                <div id="howuse">
                    <h2>  Utiliser   <b>  Chocolate Slim  </b>   est simple comme bonjour!  </h2>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/hu_step1.jpg"/>
                        <p class="hu_title">  PRÉPAREZ   </p>
                        <p><b>  Faites un cocktail chaque matin en utilisant 250 ml de lait,  </b>   comptez 1-2 cuillères de mélange pour les femmes et 2-3 cuillères pour les hommes.  </p>
                    </div>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/hu_step2.jpg"/>
                        <p class="hu_title">   BUVEZ  </p>
                        <p><b>  Buvez le cocktail  au lieu du petit-déjeuner.  </b>   Votre corps recevra: 217 kcal dont 10 g de glucides, 17g de protéines, 23 oligo-éléments et des vitamines, des fibres.  </p>
                    </div>
                    <div class="hu_step">
                        <img alt="шаг 1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/img/hu_step3.jpg"/>
                        <p class="hu_title">  RÉPÉTEZ   </p>
                        <p>  Au cours de la journée   <b>  remplacez un autre repas avec le cocktail,  </b>   pour consolider le résultat.  </p>
                    </div>
                    <p style="clear: both; float: none; width: 0; height: 0;"></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- </how-buy> -->
        <!-- <footer> -->
        <div id="footer">
            <div class="width">
                <div class="left">
                    <h1>Chocolate Slim</h1>
                    <h2>  Complexe naturel minceur  </h2>
                    <div class="hidden_upsale" style="visibility: visible">
                        <div class="sale">À saisir! 50% de réduction! </div>
                    </div>
                    <div class="price">
                        <del>98 €</del>
                        <div>49 €</div>
                    </div>
                </div>
                <div class="right">
                    <div class="time">
                        <div class="name-t">L'offre se termine dans:</div>
                        <div id="timer"></div>
                    </div>

                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abdcn.pro/forms/?target=-4AAIJIALPDQAAAAAAAAAAAARAZTGUAA"></iframe>

                </div>
            </div>
        </div>
        <div id="kmacb">
            <a href="#" modal="kmacb-form" title=""></a>
        </div>
        <div class="modal" id="kmacb-form">
            <div class="modal-block">
                <div class="icon-close"></div>
                <div class="title">Avez-vous aimé cette offre?</div>
                <div class="content">
                    <div class="padding">
                        <p>Nous vous disons tout sur ce produit, nous offrons les meilleures conditions et nous sélectionnons des offres spéciales qui vous conviennent! </p>
                        <form action="" method="POST">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIALPDQAAAAAAAAAAAARAZTGUAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-pl">
                            <input type="hidden" name="total_price" value="55.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIALPDQSB6GGIAAEAAQACaA0BAAJCFAIGAQLGBgQeSJRNAA">
                            <input type="hidden" name="goods_id" value="109">
                            <input type="hidden" name="title" value="Chocolateslim - FR">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Chocolateslim_FR_New2">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="al" value="5186">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="FR">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                            <input type="hidden" name="shipment_price" value="6">
                            <input type="hidden" name="price_vat" value="0.055">

                            <input name="name" placeholder="Prénom Nom de famille" type="text" value=""/>
                            <input name="phone" placeholder="Numéro de téléphone" type="text" value=""/><br/>
                            <input type="submit" value="Commander"/>-->
                        </form>
                        <p class="bold">Un operateur vous rappellera dans 15-20 minutes </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link href="//st.acstnst.com/content/Chocolateslim_FR_New2/css/styleSecond.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet"/>
    <div class="hidden-window">
        <div class="second-clips-header">
            <div class="containerz">
                <div class="h-w__inner">
                    <div class="yop-right__side">
                        <div class="dtable">
                            <div class="dtable-cell">Offre unique valable qu'aujourd'hui
                            </div>
                        </div>
                    </div>
                    <img alt="gg" class="logotop" src="//st.acstnst.com/content/Chocolateslim_FR_New2/imagesSec/logo.png"/>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Paiement à la livraison</div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img alt="alt1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/imagesSec/dtc1.png"/></div>
                                    <div class="dtable-cell">La commande</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img alt="alt1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/imagesSec/dtc2.png"/></div>
                                    <div class="dtable-cell">Livraison</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img alt="alt1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/imagesSec/dtc3.png"/></div>
                                    <div class="dtable-cell">Livraison et <span>paiement</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Offre avantageuse</div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIALPDQAAAAAAAAAAAARAZTGUAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-pl">
                            <input type="hidden" name="total_price" value="55.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIALPDQSB6GGIAAEAAQACaA0BAAJCFAIGAQLGBgQeSJRNAA">
                            <input type="hidden" name="goods_id" value="109">
                            <input type="hidden" name="title" value="Chocolateslim - FR">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Chocolateslim_FR_New2">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="al" value="5186">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="FR">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                            <input type="hidden" name="shipment_price" value="6">
                            <input type="hidden" name="price_vat" value="0.055">

                            <div class="formHeader"><span>Entrez vos données </span><br/> pour passer commande:</div>
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="FR">France</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1 paquet</option>
                                <option selected="selected" value="3">2+1 paquets </option>
                                <option value="5">3+2 paquets </option>
                            </select>
                            <input class="inp j-inp" data-count-length="2+" name="name" placeholder="Prénom Nom de famille" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Numéro de téléphone" type="text" value=""/>
                            <!--
                            <input class="inp" name="house" placeholder="Entrez votre House" style="display:none;" type="text"/>
                            <input class="inp" name="city" placeholder="Entrez votre ville" style="display:none;" type="text"/>  -->
                           <!-- <input class="inp" name="address" placeholder="Entrer votre adresse" type="text"/>
                            <div class="text-center totalpriceForm">total: <span class="js_total_price js_full_price">55 </span> €</div>
                            <div class="zbtn transitionmyl js_submit">La commande</div>-->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Nous garantissons</div>
                            <ul>
                                <li><b>%100</b>  qualité</li>
                                <li><b>Vérification </b>  du produit à la réception</li>
                                <li><b>Sécurité </b> de vos données</li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1 ">
                            <div class="zDiscount">
                                <b>Attention</b>:<br/>
                                <span>Rabais de 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader"><b>1</b> paquet </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prix:</div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new">  49  </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old"> 98  </text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Ancien<br/>prix</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship">  6    </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 55  </text>
                                                </b>
                                                €
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="1"></div>
                                <div class="img-wrapp"><img alt="z1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/imagesSec/zit1.png"/></div>
                                <div class="zHeader"><span> </span></div>
                                <text>Minceur expresse à moins 50%!<br/> Effet immédiat!<br/><br/></text>
                            </div>
                        </div>
                        <div class="item hot transitionmyl1 active ">
                            <div class="zstick"></div>
                            <div class="zDiscount">
                                <b>Attention</b>:<br/>
                                <span>Rabais de 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> paquets   <span class="dib zplus">en cadeau</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prix:</div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new">  98  </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old"> 196  </text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Ancien<br/>prix</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship">  6  </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 104 </text>
                                                </b>
                                                €
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3"></div>
                                <div class="img-wrapp">
                                    <div class="gift">+ en cadeau</div>
                                    <img alt="z1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/imagesSec/zit2.png"/>
                                </div>
                                <div class="zHeader"><span> </span></div>
                                <text>1 paquet de Chocolate Slim<br/> au prix nouveau !<br/></text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Attention</b>:<br/>
                                <span>Rabais de 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> paquets   <span class="dib zplus sec">en cadeau</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prix:</div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new">  147 </text>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old"> 294  </text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Ancien<br/>prix</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:</div>
                                            <div class="dtable-cell">
                                                <b>
<span>
<text class="js-pp-ship">
                                             6

                                          </text>
</span>
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full">
                                                        153

                                                    </text>
                                                </b>
                                                €
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5"></div>
                                <div class="img-wrapp">
                                    <div class="gift">+ en cadeau</div>
                                    <img alt="z1" src="//st.acstnst.com/content/Chocolateslim_FR_New2/imagesSec/zit3_n3.png"/>
                                </div>
                                <div class="zHeader sm"></div>
                                <text>
                                    Le résultat Stable! Une cure de Chocolate Slim vous aidera à éliminer les kilos en trop pour toujours!
                                    3 paquets de Chocolate Slim pour le prix de 2!
                                </text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>

    </div></body>
    </html>
<?php } ?>