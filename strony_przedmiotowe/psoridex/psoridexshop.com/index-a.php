
<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 5486 -->
    <script>var locale = "ro";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAJQEAQk1cGIAAEAAQAC4w8BAAJuFQEKAQ8EfyoNDAA";</script>
    <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
    <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="./assets/js/dr.js"></script>
    <script type="text/javascript" src="./assets/js/dtime.js"></script>
    <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
    <script type="text/javascript" src="./assets/js/validation.js"></script>
    <script type="text/javascript" src="./assets/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="./assets/js/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 0;
        var name_hint = 'Toma Remus';
        var phone_hint = '+40740525322';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("ro");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->



    <script type="text/javascript">
        $(document).ready(function () {
            $('.toform').click(function () {
                $("html, body").animate({scrollTop: $("form").offset().top}, 1000);
                return false;
            });
        });
    </script>
    <meta charset="utf-8"/>
    <title>
        Psoridex
    </title>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <link href="./assets/css/bootstrap.min.css" rel="stylesheet"/>
    <!--  Перерубаем сабсет  -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,700,400italic,300italic,700italic" rel="stylesheet" type="text/css"/>
    <!--  Перерубаем сабсет  -->
    <link href="./assets/css/animate.css" rel="stylesheet"/>
    <link href="./assets/css/animation.css" rel="stylesheet"/>
    <link href="./assets/css/twentytwenty.css" rel="stylesheet"/>
    <link href="./assets/css/style.css" rel="stylesheet"/>
    <script src="./assets/js/jqueryplugin.js" type="text/javascript"></script>
    <script src="./assets/js/jquery.countdown.min.js" type="text/javascript"></script>
    <script src="./assets/js/jquery.onscreen.min.js" type="text/javascript"></script>
    <script src="./assets/js/jquery.event.move.js" type="text/javascript"></script>
    <script src="./assets/js/jquery.twentytwenty.js" type="text/javascript"></script>
    <script src="./assets/js/animations.js" type="text/javascript"></script>
    <script src="./assets/js/main.js" type="text/javascript"></script>
</head>
<body id="fade">
<main>
    <header>
        <div class="main_container">
            <div class="row">
                <a class="col-sm-2" href="#">
                  </a>
                <div class="col-sm-2 warning align-right">
                    <p>ABOUT MANUKA HONEY
                    </p>
                </div>
                <div class="col-sm-4 warning_ph align-left">
                    <p>
                        Manuka Honey comes from the nectar of the beautiful Manuka flowers found around the isolated East Cape of New Zealand. The Manuka tree (leptospermum scoparium) is a native plant indigenous to New Zealand and grows abundantly on the slopes of hills, at the edge of forests and in coastal areas. These areas are as free from any chemicals or sprays as any area could be in beautiful New Zealand.  <script>

                        </script>
                    </p>
                </div>
                <div class="col-sm-4">
                    <div class="timer hidden" data-minutes-left="15">
<span class="hours clock_cols timer_delimiter">{h10}{h1}
                </span>
                        <span class="cl">:
                </span>
                        <span class="minutes clock_cols timer_delimiter">{m10}{m1}
                </span>
                        <span class="cl">:
                </span>
                        <span class="seconds clock_cols ">{s10}{s1}
                </span>
                        <span class="clearDiv">
</span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div id="slider">
            <div class="twentytwenty-before">
            </div>
            <div class="twentytwenty-after">
            </div>
        </div>
        <div class="on_slider">
            <div class="main_container">
                <h1>
                    <strong>    </strong>
                </h1>
                <h2>
<span>
              </span>
                    <span>
              </span>
                    <span>
              </span>
                    <span>
              </span>
                </h2>
                <ul class="header-list">
                    <li>
                    </li>
                    <li>
                    </li>
                    <li>
                    </li>
                    <li>
                    </li>
                </ul>
                  </div>
        </div>
    </section>
    <section class="offected">
        <div class="main_container">
            <a class="button" href="#form">
            </a>
            <h3>

                <br/>

                <span>
            </span>
            </h3>
            <p>
                In ancient times, the New Zealand Maori used a number of natural remedies to maintain their health. One of these was the honey derived from the beautiful flowers of the native Manuka tree. As well as ingesting the honey for good health, it was widely known that by applying it to wounds it reduced the chance of infection and dramatically aided the healing process.

                Whakaari International Limited was established in 2004 specifically to market the honey harvested, extracted and bottled by Whakaari Beekeepers Limited (2001) and East Coast Beekeepers Limited (2003). Whakaari International has developed the "Ora" brand as its flagship. The word "Ora" means health and well-being and is an appropriate word to describe the healthy qualities of Manuka honey. Whakaari International is UMF licensed, which means our Manuka honey is independently tested to establish its antibacterial qualities.</p>
        </div>
    </section>
    <section class="components">
        <div class="main_container">
            <h4>

                <br/>

                <span>
            </span>
            </h4>
            <div class="row">
                <div class="col-sm-4">
                    <article>
                        <div class="img-wrap">
                            <img src="./assets/img/component-1.png"/>
                        </div>
                        <h5>Harvesting Process
                        </h5>
                        <p>Whakaari International has 3 experienced beekeepers that service over 1,200 hives located on lands owned by Maori. Harvesting takes place in January/February of each year. Boxes are taken from the hives and transported to the extraction plant, where a team of locals work to extract the honey from the frames and store them in 44 Gallon drums.
                        </p>
                    </article>
                </div>
                <div class="col-sm-4">
                    <article>
                        <div class="img-wrap">
                            <img src="./assets/img/component-2.png"/>
                        </div>
                        <h5>
                        </h5>
                        <p>Harvesting honey not a job for the faint-hearted and is a labour-intensive activity. Boxes can weigh up to 25 kilograms in a heavy harvest and from 1,000 hives there may be as many as 2,000 boxes shifted from the field to the Extraction Plant.

                        </p>
                    </article>
                </div>
                <div class="col-sm-4">
                    <article>
                        <div class="img-wrap">
                            <img src="./assets/img/component-3.png"/>
                        </div>
                        <h5>
                        </h5>
                        <p>Once the honey has been harvested,the product is transported tothe bottling plant in Whakatane.
                        </p>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <article>
                        <div class="img-wrap">
                            <img src="./assets/img/component-4.png"/>
                        </div>
                        <h5>
                        </h5>
                        <p>It is stored at this location and bottled as required. From here, test samples are sent to the laboratory to measure the level of Unique Manuka Factor (UMF) in the honey. The higher the UMF level, the more valuable and sought after it is.
                        </p>
                    </article>
                </div>
                <div class="col-sm-4">
                    <article>
                        <div class="img-wrap">
                            <img src="./assets/img/component-5.png"/>
                        </div>
                        <h5>Antibacterial Properties
                        </h5>
                        <p>Most honeys have antibacterial activity due to the enzyme, Glucose Oxidase, which gives rise to hydrogen peroxide, a proven antiseptic with antibacterial properties. However, this enzyme becomes inactive when exposed to heat, light and water - the more it is exposed to, the faster it inactivates. </p>
                    </article>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
        </div>
    </section>
    <section class="opinion">
        <div class="main_container">
            <h4>


            </h4>
            <article>
                <p>
                    Although Manuka honey has a varying degree of peroxide activity, it has been found to have further antibacterial properties which weren’t adversely affected by heat or light. This activity is referred to as NPA (Non-peroxide activity).
                </p>
                <p>
                    <b>
                    </b>

                    After further research, the discovery of Methylglyoxal (MGO) was made and found to be responsible for Manuka honey’s stable antibacterial activity. Although MGO is the primary antibacterial ingredient, studies have shown that it is the combined action of MGO and an unidentified synergistic component that gives it its antibacterial properties.Very low levels of MGO are found in most honey, but the high level of MGO in Manuka honey is unique, as it is the presence of the synergist which more than doubles the antibacterial activity. </p>
                <p>
                    <b>In 2008, an MGO rating system was established however to this date only one company uses this rating.  </b>
                </p>
            </article>
        </div>
    </section>
    <section class="customers">
        <div class="main_container">
            <h4>
            </h4>
            <div class="row">
                <div class="col-sm-4">
                      <h5>UMF® (Unique Manuka Factor)

                    </h5>
                    <p class="age">
                    </p>
                    <p class="history">Manuka honey’s unique antibacterial qualities were scientifically proven in 1981 and was labelled as the Unique Manuka Factor (UMF). In the 1990s, the Active Manuka Honey Association (AMHA) was set up primarily to set a standard in the industry, in case of unscrupulous producers falsely marketing Manuka honey with little or no antibacterial properties. UMF® is now trademarked in 46 countries and is the most used and trusted grading system used for Manuka honey.
                    </p>
                </div>
                <div class="col-sm-4">
                     <h5>ΚThe UMF® Rating
                    </h5>
                    <p class="age">
                    </p>
                    <p class="history">
                        The UMF® testing procedure was established to accurately grade the potency of Manuka honey. The name UMF® is followed by a number that indicates the strength of its UMF® antibacterial property in a batch of UMF® Manuka Honey tested in the licensed laboratory after the honey has been packed. The test involves comparing the effectiveness of a Manuka honey sample on Staphylococcus aureus, the most common pathogen found in wounds to Phenol, a common antiseptic. The number following UMF® corresponds to the percentage of Phenol in water that the honey’s antibacterial levels are equal to. For example, UMF 5 is equal to a 5% solution of Phenol and water. (A UMF® 5 rating is the minimum to qualify to use the name UMF®). </p>
                </div>
                <div class="col-sm-4">
                     <h5>
                    </h5>
                    <p class="age">
                    </p>
                    <p class="history">
                        It is generally accepted that Manuka Honey with a UMF® rating of 10 or higher is appropriate for medical use. The higher the UMF rating, the higher the antibacterial activity. The highest rating is UMF 20+.
                        UMF® is the registered name and trademark of the Active Manuka Honey Association (AMHA) and can be used only by licensed users who meet set criteria which include regular monitoring and auditing of the quality of their honey. UMF Manuka Honey from a New Zealand company licensed to use the name UMF®, and labeled in the licensee’s name, is the only honey guaranteed to have the special UMF® healing property and be of the same quality as that referred to in the research and clinical trials.

                    </p>
                </div>
            </div>
        </div>
    </section>

</main>
</body>
</html>