<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html lang="es">
    <head>
        <!-- [pre]land_id = 5486 -->
        <script>var locale = "ro";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJQEAQk1cGIAAEAAQAC4w8BAAJuFQEKAQ8EfyoNDAA";</script>
        <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/js/dr.js"></script>
        <script type="text/javascript" src="./assets/js/dtime.js"></script>
        <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/js/validation.js"></script>
        <script type="text/javascript" src="./assets/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Toma Remus';
            var phone_hint = '+40740525322';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http:/ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ro");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','/connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https:/www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title></title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/mobile/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="https:/fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/css/style.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({scrollTop: $("form").offset().top}, 1000);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <header>
        <div class="header_wrapper">
            <div class="main_container">
                <img class="logo" src="./assets/mobile/images/logo.jpg"/>
                <h1 class="header-head"><strong>PSORIAZISUL ÎŢI ÎNGREUNEAZĂ VIAŢA?</strong></h1>
                <h2>
                    <span>PSORIDEX</span> <span>te ajută să scapi</span>
                    <span>  de problemă  </span> <span></span>
                </h2>
                <img class="product" src="./assets/mobile/images/psoridex-header.png"/>
                <a class="button toform" href="#form">   Comandă acum!   </a>
                <ul class="header-list">
                    <li> eficient în diferite tipuri şi forme ale bolii </li>
                    <li> previne creşterea celulelor pielii deja afectate </li>
                    <li> elimină reapariția bolii </li>
                    <li> controlează inflamaţia </li>
                </ul>
            </div>
        </div>
    </header>
    <section class="offected">
        <div class="main_container">
            <h3>
                zona afectată <br/>
                poate <span>reapărea!</span>
            </h3>
            <p>
                Psoriazisul afectează 4% din populaţia lumii. În majoritatea cazurilor, vorbim despre persoane tinere; Conform statisticilor, 70% dintre cei afectaţi de psoriazis nu au atins încă vârsta de 20 de ani. Oamenii diagnosticaţi cu această boală speră să scape
                de privirile curioase şi de tendinţa celorlalţi de a-i evita. Poţi să îţi îmbunătățești aspectul pielii!
            </p>
        </div>
    </section>
    <section class="components">
        <div class="main_container">
            <h4>
                EFECTELE APLICĂRII<br/>
                PRODUSULUI <span>PSORIDEX</span> PE PIELE!
            </h4>
            <div class="row">
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-1.png"/></div>
                        <h5>HELIANTHUS ANNUUS ULEI DE FLOAREA-SOARELUI</h5>
                        <p>Catifelează, hrănește pielea, are un efect calmant asupra mâncărimii, are efect analgezic asupra pielii şi articulaţiilor.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-2.png"/></div>
                        <h5>PARAFINĂ LICHIDĂ</h5>
                        <p>Are efect antimicrobian, are proprietăţi regeneratoare sporite şi îmbunătăţeşte absorbţia celorlalte componente în piele.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-3.png"/></div>
                        <h5>ULEI DE MIGDALE DULCI</h5>
                        <p>Reduce inflamaţia, controlează glandele sebacee, are efect calmant şi hidratant asupra pielii.</p>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-4.png"/></div>
                        <h5>ULEI DE MĂCEȘE</h5>
                        <p>Menţine pielea hidratată, ajută activ la îndepărtarea pielii îngroșate și exfoliate, are efect antioxidant, oferă pielii vitaminele şi mineralele necesare.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-5.png"/></div>
                        <h5>ULEI DE GERMENI DE GRÂU</h5>
                        <p>Tonifică pielea, încurajează procesul de regenerare a pielii, elimină senzaţia de căldură şi mâncărime, ajută la eliminarea exfolierii pielii.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><h5 id="last">
                                <span style="margin-left: -33px;">BHT,</span> Ascorbyl Palmitate,
                                <span>geraniol,</span>
                                citronellol,
                                <span>eugenol,</span>
                                Isopropyl                             Myristate
                            </h5></div>
                        <p>Aceste ingrediente sunt antiseptice, elimină SIMPTOMELE, au efect purificator, normalizează metabolismul celulelor pielii, inhibă procesul de exfoliere.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section class="opinion">
        <div class="main_container">
            <h4>
                L'OPINIA
                dell'EXPERTULUI
            </h4>
            <img src="./assets/mobile/images/doc.png"/>
            <article>
                <p> Majoritatea persoanelor afectate de psoriazis se confruntă cu dificultăţi în ceea ce priveşte munca şi adaptarea în medii sociale. De exemplu, formele puternice de mâncărime împiedică procesul normal de odihnă, iar plăgile pot limita
                    capacitatea de muncă. Psoriazisul îngreunează semnificativ calitatea vieţii oamenilor care suferă de această boală; ei se confruntă atât cu disconfort fizic, cât şi cu disconfort mental. </p>
                <p><b> Psоridex </b> poate fi utilizat cu succes de toate persoanele afectate de această boală, eficienţa apare încă de la prima utilizare, ajutând în cazurile incipienta, dar şi în cazurile avansate. </p>
                <p><b> Utilizarea produsului PSORIDEX ca metodă principală de tratament elimină afecţiunea în 93% din cazuri! </b></p>
            </article>
        </div>
    </section>
    <section class="customers">
        <div class="main_container">
            <h4>MĂRTURIILE CLIENŢILOR</h4>
            <div class="row">
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-1.png"/>
                    <h5>Alina Vlădescu</h5>
                    <p class="age">30 de ani</p>
                    <p class="history"> Din cauza psoriazisului, nu am participat la o mulţime de activităţi atât la muncă, cât şi în timpul liber. Doctorul m-a diagnosticat cu psoriazis gutos, aveam mici zone afectate, dar pe toată suprafaţa corpului. Psoridex mi-a
                        întrecut aşteptările, acum pot să revin la un stil de viaţă normal.</p>
                </div>
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-2.png"/>
                    <h5>Camelia Prodan</h5>
                    <p class="age">19 ani</p>
                    <p class="history">
                        În cazul meu, psoriazisul este prezent pe spate. Nu puteam să dorm, să mănânc, să învăţ din cauza mâncărimii insistente în zona afectată. Îi sunt recunoscătoare mamei pentru că a găsit acest produs. Psoridex m-a ajutat să lupt cu această problemă. Mâncărimea
                        a dispărut, iar aspectul pielii s-a îmbunătățit deja semnificativ.
                    </p>
                </div>
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-3.png"/>
                    <h5>Ovidiu Niculescu</h5>
                    <p class="age">25 de ani</p>
                    <p class="history">Această cremă are multe avantaje. Eu sufeream de psoriazis în stadiu avansat în zona abdomenului. După ce am început să folosesc Psoridex, schimbările pozitive au apărut rapid. Inflamaţia a dispărut, petele şi erupţia au început
                        să se micşoreze sau să dispară. Crăpăturile s-au vindecat, iar pielea a devenit mai moale şi mai sănătoasă. Dacă încercaţi Psoridex, o să vă convingeţi că psoriazisul se poate vindeca.</p>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="main_container" id="form">
            <div class="row">
                <img alt="" class="prod" src="./assets/mobile/images/prod.png"/>
                <div class="pricer">
<span class="new">doar<br/>
<b>
<i>159</i> Lei
                </b>
</span>
                    <span class="standart">vechiul preţ

            <s>
                318 Lei
            </s>
</span>
                </div>
                <form action="" method="post">


                    <select class="selector" id="country_code_selector">
                        <option value="RO">
                            România
                        </option>
                    </select>
                    <input name="name" placeholder="nume" type="text"/>
                    <input class="only_number" name="phone" placeholder="număr de telefon" type="text"/>
                    <input class="button js_submit" type="submit" value="Comandă acum!"/>
                    <input class="hidden"  name="mobile" value="1" >
                </form>
            </div>
        </div>
    </footer>
    </body>
    </html>
<?php } else { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 5486 -->
        <script>var locale = "ro";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJQEAQk1cGIAAEAAQAC4w8BAAJuFQEKAQ8EfyoNDAA";</script>
        <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/js/dr.js"></script>
        <script type="text/javascript" src="./assets/js/dtime.js"></script>
        <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/js/validation.js"></script>
        <script type="text/javascript" src="./assets/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Toma Remus';
            var phone_hint = '+40740525322';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http:/ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ro");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','/connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https:/www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({scrollTop: $("form").offset().top}, 1000);
                    return false;
                });
            });
        </script>
        <meta charset="utf-8"/>
        <title>
            Psoridex
        </title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet"/>
        <!--  Перерубаем сабсет  -->
        <link href="https:/fonts.googleapis.com/css?family=Roboto:400,300,700,400italic,300italic,700italic" rel="stylesheet" type="text/css"/>
        <!--  Перерубаем сабсет  -->
        <link href="./assets/css/animate.css" rel="stylesheet"/>
        <link href="./assets/css/animation.css" rel="stylesheet"/>
        <link href="./assets/css/twentytwenty.css" rel="stylesheet"/>
        <link href="./assets/css/style.css" rel="stylesheet"/>
        <script src="./assets/js/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.onscreen.min.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.event.move.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.twentytwenty.js" type="text/javascript"></script>
        <script src="./assets/js/animations.js" type="text/javascript"></script>
        <script src="./assets/js/main.js" type="text/javascript"></script>
    </head>
    <body id="fade">
    <main>
        <header>
            <div class="main_container">
                <div class="row">
                    <a class="col-sm-2" href="#"><img class="logo" src="./assets/img/logo.jpg"/></a>
                    <div class="col-sm-2 warning align-right">
                        <p> Atenţie: </p>
                    </div>
                    <div class="col-sm-4 warning_ph align-left">
                        <p>
                            Ca urmare a popularităţii produsului în telesales, există un stoc limitat de Cleanse and Detox începând cu data de
                            <script>
                                dtime_nums(-1, true)
                            </script>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <div class="timer hidden" data-minutes-left="15">
                            <span class="hours clock_cols timer_delimiter"> {h10}{h1} </span>
                            <span class="cl"> : </span>
                            <span class="minutes clock_cols timer_delimiter"> {m10}{m1} </span>
                            <span class="cl"> : </span>
                            <span class="seconds clock_cols"> {s10}{s1} </span>
                            <span class="clearDiv"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section>
            <div id="slider">
                <div class="twentytwenty-before">
                </div>
                <div class="twentytwenty-after">
                    <div class="on_slider">
                        <div class="main_container">
                            <h1><strong> PSORIAZISUL ÎŢI ÎNGREUNEAZĂ VIAŢA? </strong></h1>
                            <h2>
                                <span></span> <span> PSORIDEX  </span>
                                <span> te ajută să scapi </span> <span> de problemă </span>
                            </h2>
                            <ul class="header-list">
                                <li> eficient în diferite tipuri şi forme ale bolii </li>
                                <li> previne creşterea celulelor pielii deja afectate </li>
                                <li> elimină reapariția bolii </li>
                                <li> controlează inflamaţia. </li>
                            </ul>
                            <img class="product" src="./assets/img/psoridex-header.png"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="offected">
            <div class="main_container">
                <a class="button" href="#form"> Comandă acum! </a>
                <h3>
                    zona afectată <br/>
                    poate  <span> reapărea! </span>
                </h3>
                <p>
                    Psoriazisul afectează 4% din populaţia lumii. În majoritatea cazurilor, vorbim despre persoane tinere; Conform statisticilor, 70% dintre cei afectaţi de psoriazis nu au atins încă vârsta de 20 de ani. Oamenii diagnosticaţi cu această boală speră să scape
                    de privirile curioase şi de tendinţa celorlalţi de a-i evita. Poţi să îţi îmbunătățești aspectul pielii!
                </p>
            </div>
        </section>
        <section class="components">
            <div class="main_container">
                <h4>
                    EFECTELE APLICĂRII <br/>
                    PRODUSULUI  <span> PSORIDEX </span>  PE PIELE!
                </h4>
                <div class="row">
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-1.png"/></div>
                            <h5> HELIANTHUS ANNUUS ULEI DE FLOAREA-SOARELUI </h5>
                            <p> Catifelează, hrănește pielea, are un efect calmant asupra mâncărimii, are efect analgezic asupra pielii şi articulaţiilor. </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-2.png"/></div>
                            <h5> PARAFINĂ LICHIDĂ </h5>
                            <p> Are efect antimicrobian, are proprietăţi regeneratoare sporite şi îmbunătăţeşte absorbţia celorlalte componente în piele. </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-3.png"/></div>
                            <h5> ULEI DE MIGDALE DULCI </h5>
                            <p> Reduce inflamaţia, controlează glandele sebacee, are efect calmant şi hidratant asupra pielii. </p>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-4.png"/></div>
                            <h5> ULEI DE MĂCEȘE </h5>
                            <p> Menţine pielea hidratată, ajută activ la îndepărtarea pielii îngroșate și exfoliate, are efect antioxidant, oferă pielii vitaminele şi mineralele necesare. </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-5.png"/></div>
                            <h5> ULEI DE GERMENI DE GRÂU </h5>
                            <p> Tonifică pielea, încurajează procesul de regenerare a pielii, elimină senzaţia de căldură şi mâncărime, ajută la eliminarea exfolierii pielii. </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <h5 id="last">
                                <span> BHT </span> , Ascorbyl Palmitate,
                                <span>  geraniol </span> ,                             citronellol,
                                <span> eugenol </span> ,                             Isopropyl                             Myristate
                            </h5>
                            <p> Aceste ingrediente sunt antiseptice, elimină SIMPTOMELE, au efect purificator, normalizează metabolismul celulelor pielii, inhibă procesul de exfoliere. </p>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <section class="opinion">
            <div class="main_container">
                <h4>
                    OPINIA
                    <img src="./assets/img/doc.png"/>
                    EXPERTULUI
                </h4>
                <article>
                    <p> Majoritatea persoanelor afectate de psoriazis se confruntă cu dificultăţi în ceea ce priveşte munca şi adaptarea în medii sociale. De exemplu, formele puternice de mâncărime împiedică procesul normal de odihnă, iar plăgile pot limita
                        capacitatea de muncă. Psoriazisul îngreunează semnificativ calitatea vieţii oamenilor care suferă de această boală; ei se confruntă atât cu disconfort fizic, cât şi cu disconfort mental. </p>
                    <p><b> Psоridex </b> poate fi utilizat cu succes de toate persoanele afectate de această boală, eficienţa apare încă de la prima utilizare, ajutând în cazurile incipienta, dar şi în cazurile avansate. </p>
                    <p><b> Utilizarea produsului PSORIDEX ca metodă principală de tratament elimină afecţiunea în 93% din cazuri! </b></p>
                </article>
            </div>
        </section>
        <section class="customers">
            <div class="main_container">
                <h4> MĂRTURIILE CLIENŢILOR </h4>
                <div class="row">
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-1.png"/>
                        <h5> Alina Vlădescu </h5>
                        <p class="age"> 30 de ani </p>
                        <p class="history"> Din cauza psoriazisului, nu am participat la o mulţime de activităţi atât la muncă, cât şi în timpul liber. Doctorul m-a diagnosticat cu psoriazis gutos, aveam mici zone afectate, dar pe toată suprafaţa corpului. Psoridex mi-a
                            întrecut aşteptările, acum pot să revin la un stil de viaţă normal. </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-2.png"/>
                        <h5> Camelia Prodan, </h5>
                        <p class="age"> 19 ani </p>
                        <p class="history">
                            În cazul meu, psoriazisul este prezent pe spate. Nu puteam să dorm, să mănânc, să învăţ din cauza mâncărimii insistente în zona afectată. Îi sunt recunoscătoare mamei pentru că a găsit acest produs. Psoridex m-a ajutat să lupt cu această problemă. Mâncărimea
                            a dispărut, iar aspectul pielii s-a îmbunătățit deja semnificativ.
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-3.png"/>
                        <h5> Ovidiu Niculescu </h5>
                        <p class="age"> 25 de ani </p>
                        <p class="history"> Această cremă are multe avantaje. Eu sufeream de psoriazis în stadiu avansat în zona abdomenului. După ce am început să folosesc Psoridex, schimbările pozitive au apărut rapid. Inflamaţia a dispărut, petele şi erupţia au început
                            să se micşoreze sau să dispară. Crăpăturile s-au vindecat, iar pielea a devenit mai moale şi mai sănătoasă. Dacă încercaţi Psoridex, o să vă convingeţi că psoriazisul se poate vindeca. </p>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="main_container" id="form">
                <div class="row">
                    <form action="process.php" method="post">
                        <p> Spune-ne unde să îţi trimitem <br/>
                            <span> coletul </span></p>

<


                        <select id="country_code_selector" name="">
                            <option value="RO">
                                România
                            </option>
                        </select>
                        <input name="name" placeholder="nume" type="text"/>
                        <input class="only_number" name="phone" placeholder="număr de telefon" type="text"/>
                        <input class="button js_submit" type="submit" value="Comandă acum!"/>
                    </form>
                    <span class="new"> doar

                <b>
<i>159</i>  Lei
                </b>
</span>
                    <span class="standart"> vechiul <br/>  preţ

                <s>
                     318 Lei
                </s>
</span>
                </div>
            </div>
        </footer>
    </main>
    </body>
    </html>
<?php } ?>