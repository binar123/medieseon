<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html lang="es">
    <head>
        <!-- [pre]land_id = 9321 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJSEARHM8GIAAEAAQAC5Q8BAAJpJAEKAQ8EiJPidgA";</script>
        <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/js/dr.js"></script>
        <script type="text/javascript" src="./assets/js/dtime.js"></script>
        <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/js/validation.js"></script>
        <script type="text/javascript" src="./assets/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Bruno Bellini';
            var phone_hint = '+393476736735';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title></title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/mobile/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/css/style.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({scrollTop: $("form").offset().top}, 1000);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <header>
        <div class="header_wrapper">
            <div class="main_container">
                <img class="logo" src="./assets/mobile/images/logo.jpg"/>
                <h1 class="header-head"><strong>LA PSORIASI RENDE LA VOSTRA VITA UN INFERNO?</strong></h1>
                <h2>
                    <span>Con</span> <span> psoridex</span>
                    <span>  , vi sentirete  </span> <span> meglio!</span>
                </h2>
                <img class="product" src="./assets/mobile/images/psoridex-header.png"/>
                <a class="button toform" href="#form">  Acquista una confezione  </a>
                <ul class="header-list">
                    <li> Efficace per vari tipi e gradi della malattia </li>
                    <li> Previene l'influenza sullo sviluppo di cellule cutanee </li>
                    <li> Elimina i reumatismi palindromici </li>
                    <li> Fornisce alti livelli di controllo sulle macchie </li>
                </ul>
            </div>
        </div>
    </header>
    <section class="offected">
        <div class="main_container">
            <h3>
                CURARE LA
                VOSTRA PELLE <span>ES POSIBLE!</span>
            </h3>
            <p>
                La psoriasi colpisce il 4% della popolazione mondiale e nella maggior parte dei casi si tratta di persone giovani. Secondo le statistiche, il 70% di chi soffre di psoriasi non ha ancora compiuto 20 anni. Le persone che ne sono affette solitamente evitano di frequentare i luoghi pubblici, perché gli sguardi indiscreti della gente le fanno sentire a disagio. Ma grazie a PSORIDEX, la soluzione è a portata di mano e potrete migliorare l'aspetto e la salute della vostra pelle!
            </p>
        </div>
    </section>
    <section class="components">
        <div class="main_container">
            <h4>
                PERCHÉ SCEGLIERE<br/>
                <span>PSORIDEX</span> PER LA CURA DELLA VOSTRA PELLE?
            </h4>
            <div class="row">
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-1.png"/></div>
                        <h5>OLIO DI SEMI DI GIRASOLE</h5>
                        <p>Ammorbidisce la pelle affetta da cheratodermia, ha un effetto antipruriginoso e agisce come analgesico sulla pelle e sulle articolazioni.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-2.png"/></div>
                        <h5>PARAFFINA LIQUIDA</h5>
                        <p>Ha un'azione antimicrobica, contiene una vasta gamma di proprietà curative e viene assorbita dalla pelle in pochi minuti.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-3.png"/></div>
                        <h5>OLIO DI MANDORLE DOLCI</h5>
                        <p>Riduce il processo infiammatorio, controlla le ghiandole sebacee e ha proprietà lenitive e emollienti.</p>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-4.png"/></div>
                        <h5>OLIO DI SEMI DI ROSA CANINA</h5>
                        <p>Nutre la pelle e allo stesso tempo la rende elastica. Elimina gradualmente la secchezza resistente, ha proprietà antiossidanti e fornisce alla pelle vitamine e minerali.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-5.png"/></div>
                        <h5>OLIO DI GERME DI GRANO</h5>
                        <p>Tonifica la superficie della pelle, allevia il prurito e il fastidio, aumenta il processo di rigenerazione cellulare e elimina le scaglie cutanee.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><h5 id="last">
                                <span style="margin-left: -33px;">Butilidrossitoluolo,</span> ascorbile palmitato,
                                <span>geraniolo,</span>
                                citronellolo,
                                <span>eugenolo,</span>
                                miristato di isopropile
                            </h5></div>
                        <p>Tutti questi componenti antisettici per la pelle prevengono qualunque epifenomeno, rinnovano e normalizzano i processi metabolici cellulari e inibiscono la fissione delle cellule dell'epidermide.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section class="opinion">
        <div class="main_container">
            <h4>
                L'opinione
                dell'esperto
            </h4>
            <img src="./assets/mobile/images/doc.png"/>
            <article>
                <p>La maggior parte delle persone che soffrono di psoriasi hanno grande difficoltà ad adattarsi e ad entrare in contatto con la sfera sociale e lavorativa intorno a loro. Ad esempio, il prurito intenso a volte impedisce loro di risposare
                    e le placche che compaiono sulle mani potrebbero limitare il loro rendimento lavorativo e la loro competenza. La psoriasi incide in modo significativo sulla qualità della vita, poiché le persone che ne soffrono si sentono fisicamente
                    e mentalmente a disagio.</p>
                <p><b>Psоridex</b> è un ottimo prodotto. La sua efficacia si nota sin dalla prima applicazione e aiuta a sbarazzarsi velocemente della malattia.</p>
                <p><b>Usare Psoridex come metodo di trattamento primario elimina i reumatismi palindromici nel 93% dei casi.</b></p>
            </article>
        </div>
    </section>
    <section class="customers">
        <div class="main_container">
            <h4>I commenti dei nostri clienti</h4>
            <div class="row">
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-1.png"/>
                    <h5>Lisa,</h5>
                    <p class="age">30 anni</p>
                    <p class="history"> A causa della psoriasi, ho dovuto rinunciare a partecipare a un sacco di attività. I medici mi hanno diagnosticato la psoriasi guttata, e gli sfoghi, che all'inizio erano piuttosto piccoli, si estendevano poi a tutto il corpo.
                        Sono molto contenta di aver trovato PSORIDEX! Grazie a questo prodotto sono tornata a vivere normalmente!</p>
                </div>
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-2.png"/>
                    <h5>Sofia,</h5>
                    <p class="age">19 anni</p>
                    <p class="history">
                        Tutto è cominciato con delle macchie di psoriasi sulla schiena. Questo problema mi ha causato impedimenti quotidiani seri, non riuscivo a dormire, a studiare o a mangiare. Grazie a mia madre, ho trovato la soluzione: Psoridex.
                        Ora il prurito e il fastidio sono passati e ho notato dei miglioramenti significativi e gratificanti. Grazie mille!
                    </p>
                </div>
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-3.png"/>
                    <h5>Roberto,</h5>
                    <p class="age">25 anni</p>
                    <p class="history">Questa crema ha molti vantaggi! Alcuni anni fa, soffrivo di psoriasi addominale e non stavo molto bene. Ma dopo aver provato questa crema, i cambiamenti sono arrivati subito. Il processo infiammatorio si è fermato, le eruzioni
                        e le macchie cutanee si sono ridotte e la mia pelle è diventata più morbida e più pulita. Raccomando il prodotto! Finalmente c'è una cura per la psoriasi!</p>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="main_container" id="form">
            <div class="row">
                <img alt="" class="prod" src="./assets/mobile/images/prod.png"/>
                <div class="pricer">
<span class="new">Prezzo<br/>
<b>
<i>39</i> €
                </b>
</span>
                    <span class="standart">Prezzo totale

            <s>
                78 €
            </s>
</span>
                </div>
                <form action="process.php" method="post">

                    <select class="selector" id="country_code_selector">
                        <option value="IT">
                            Italia
                        </option>
                    </select>
                    <input name="name" placeholder="Nome" type="text"/>
                    <input class="only_number" name="phone" placeholder="Phone" type="text"/>
                    <input class="button js_submit" type="submit" value="Conseguir mi pack"/>
                    <input class="hidden"  name="mobile" value="1" >
                </form>
            </div>
        </div>
    </footer>
    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 9321 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJSEARHM8GIAAEAAQAC5Q8BAAJpJAEKAQ8EiJPidgA";</script>
        <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/js/dr.js"></script>
        <script type="text/javascript" src="./assets/js/dtime.js"></script>
        <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/js/validation.js"></script>
        <script type="text/javascript" src="./assets/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Bruno Bellini';
            var phone_hint = '+393476736735';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> Psoridex </title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet"/>
        <!--  Перерубаем сабсет  -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,700,400italic,300italic,700italic" rel="stylesheet" type="text/css"/>
        <!--  Перерубаем сабсет  -->
        <link href="./assets/css/animation.css" rel="stylesheet"/>
        <link href="./assets/css/twentytwenty.css" rel="stylesheet"/>
        <link href="./assets/css/style.css" rel="stylesheet"/>
        <script src="./assets/js/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.onscreen.min.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.event.move.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.twentytwenty.js" type="text/javascript"></script>
        <script src="./assets/js/animations.js" type="text/javascript"></script>
    </head>
    <body id="fade">
    <main>
        <header>
            <div class="main_container">
                <div class="row">
                    <a class="col-sm-2" href="#"><img class="logo" src="./assets/img/logo.jpg"/></a>
                    <div class="col-sm-2 warning align-right">
                        <p> ATTENZIONE: </p>
                    </div>
                    <div class="col-sm-4 warning_ph align-left">
                        <p> Vista l'elevata richiesta in TV, lo stock di questo prodotto detergente e disintossicante è limitato fino al
                            <script>
                                dtime_nums(-1, true)
                            </script>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <div class="timer hidden" data-minutes-left="15"> <span class="hours clock_cols timer_delimiter">  {h10}{h1}  </span> <span class="cl">:</span> <span class="minutes clock_cols timer_delimiter">  {m10}{m1}  </span> <span class="cl">:</span> <span class="seconds clock_cols">  {s10}{s1}  </span> <span class="clearDiv"></span> </div>
                    </div>
                </div>
            </div>
        </header>
        <section>
            <div id="slider">
                <div class="twentytwenty-before"> </div>
                <div class="twentytwenty-after"> </div>
                <div class="on_slider">
                    <div class="main_container">
                        <h1><strong>  LA PSORIASI RENDE LA VOSTRA VITA UN INFERNO?  </strong></h1>
                        <h2 style="width: 338px"> <span>  Con  </span> <span> Psoridex,</span><span> vi sentirete  </span> <span>  meglio!  </span> </h2>
                        <ul class="header-list">
                            <li> Efficace per vari tipi e gradi della malattia </li>
                            <li> Previene l'influenza sullo sviluppo di cellule cutanee </li>
                            <li> Elimina i reumatismi palindromici </li>
                            <li> Fornisce alti livelli di controllo sulle macchie </li>
                        </ul>
                        <img class="product" src="./assets/img/psoridex-header.png"/>
                    </div>
                </div>
            </div>
        </section>
        <section class="offected">
            <div class="main_container"> <a class="button" href="#form">  Acquista una confezione  </a>
                <h3>                   CURARE LA  <br/>                   VOSTRA PELLE   <span>  E' POSSIBILE!  </span> </h3>
                <p> La psoriasi colpisce il 4% della popolazione mondiale e nella maggior parte dei casi si tratta di persone giovani. Secondo le statistiche, il 70% di chi soffre di psoriasi non ha ancora compiuto 20 anni. Le persone che ne sono affette
                    solitamente evitano di frequentare i luoghi pubblici, perché gli sguardi indiscreti della gente le fanno sentire a disagio. Ma grazie a PSORIDEX, la soluzione è a portata di mano e potrete migliorare l'aspetto e la salute della
                    vostra pelle! </p>
            </div>
        </section>
        <section class="components">
            <div class="main_container">
                <h4>                   PERCHÉ SCEGLIERE  <br/> <span>  PSORIDEX  </span>   PER LA CURA DELLA VOSTRA PELLE?               </h4>
                <div class="row">
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-1.png"/></div>
                            <h5>  OLIO DI SEMI DI GIRASOLE  </h5>
                            <p> Ammorbidisce la pelle affetta da cheratodermia, ha un effetto antipruriginoso e agisce come analgesico sulla pelle e sulle articolazioni. </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-2.png"/></div>
                            <h5>  PARAFFINA LIQUIDA  </h5>
                            <p> Ha un'azione antimicrobica, contiene una vasta gamma di proprietà curative e viene assorbita dalla pelle in pochi minuti. </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-3.png"/></div>
                            <h5>  OLIO DI MANDORLE DOLCI  </h5>
                            <p> Riduce il processo infiammatorio, controlla le ghiandole sebacee e ha proprietà lenitive e emollienti. </p>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-4.png"/></div>
                            <h5>  OLIO DI SEMI DI ROSA CANINA  </h5>
                            <p> Nutre la pelle e allo stesso tempo la rende elastica. Elimina gradualmente la secchezza resistente, ha proprietà antiossidanti e fornisce alla pelle vitamine e minerali. </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-5.png"/></div>
                            <h5>  OLIO DI GERME DI GRANO  </h5>
                            <p> Tonifica la superficie della pelle, allevia il prurito e il fastidio, aumenta il processo di rigenerazione cellulare e elimina le scaglie cutanee. </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <h5 id="last"> <span style="margin-left: -33px;">  Butilidrossitoluolo,  </span>  ascorbile palmitato,                               <span>  geraniolo,</span> citronellolo,                               <span>  eugenolo,</span> miristato di isopropile                           </h5>
                            <p> Tutti questi componenti antisettici per la pelle prevengono qualunque epifenomeno, rinnovano e normalizzano i processi metabolici cellulari e inibiscono la fissione delle cellule dell'epidermide. </p>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <section class="opinion">
            <div class="main_container">
                <h4>                   L'opinione                   <img src="./assets/img/doc.png"/>                   dell'esperto               </h4>
                <article>
                    <p> La maggior parte delle persone che soffrono di psoriasi hanno grande difficoltà ad adattarsi e ad entrare in contatto con la sfera sociale e lavorativa intorno a loro. Ad esempio, il prurito intenso a volte impedisce loro di risposare
                        e le placche che compaiono sulle mani potrebbero limitare il loro rendimento lavorativo e la loro competenza. La psoriasi incide in modo significativo sulla qualità della vita, poiché le persone che ne soffrono si sentono fisicamente
                        e mentalmente a disagio. </p>
                    <p><b>  Psoridex  </b> è un ottimo prodotto. La sua efficacia si nota sin dalla prima applicazione e aiuta a sbarazzarsi velocemente della malattia. </p>
                    <p><b>  Usare Psoridex come metodo di trattamento primario elimina i reumatismi palindromici nel 93% dei casi.  </b></p>
                </article>
            </div>
        </section>
        <section class="customers">
            <div class="main_container">
                <h4>  I commenti dei nostri clienti  </h4>
                <div class="row">
                    <div class="col-sm-4"> <img src="./assets/img/customer-1.png"/>
                        <h5>  Lisa,  </h5>
                        <p class="age"> 30 anni </p>
                        <p class="history"> A causa della psoriasi, ho dovuto rinunciare a partecipare a un sacco di attività. I medici mi hanno diagnosticato la psoriasi guttata, e gli sfoghi, che all'inizio erano piuttosto piccoli, si estendevano poi a tutto il corpo.
                            Sono molto contenta di aver trovato PSORIDEX! Grazie a questo prodotto sono tornata a vivere normalmente! </p>
                    </div>
                    <div class="col-sm-4"> <img src="./assets/img/customer-2.png"/>
                        <h5>  Sofia,  </h5>
                        <p class="age"> 19 anni </p>
                        <p class="history"> Tutto è cominciato con delle macchie di psoriasi sulla schiena. Questo problema mi ha causato impedimenti quotidiani seri, non riuscivo a dormire, a studiare o a mangiare. Grazie a mia madre, ho trovato la soluzione: Psoridex.
                            Ora il prurito e il fastidio sono passati e ho notato dei miglioramenti significativi e gratificanti. Grazie mille! </p>
                    </div>
                    <div class="col-sm-4"> <img src="./assets/img/customer-3.png"/>
                        <h5>  Roberto,  </h5>
                        <p class="age"> 25 anni </p>
                        <p class="history"> Questa crema ha molti vantaggi! Alcuni anni fa, soffrivo di psoriasi addominale e non stavo molto bene. Ma dopo aver provato questa crema, i cambiamenti sono arrivati subito. Il processo infiammatorio si è fermato, le eruzioni
                            e le macchie cutanee si sono ridotte e la mia pelle è diventata più morbida e più pulita. Raccomando il prodotto! Finalmente c'è una cura per la psoriasi! </p>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="main_container" id="form">
                <div class="row">
                    <form action="process.php" method="post">
                        <p> Dove desideri ricevere <br/> <span>  il pacco  </span></p>



                        <select id="country_code_selector" name=""> <option value="IT">                               Italia                           </option> </select> <input name="name" placeholder="Nome" type="text"/> <input class="only_number" name="phone" placeholder="Phone" type="text"/>
                        <input class="button js_submit" type="submit" value="Conseguir mi pack"/>
                    </form> <span class="new"> Prezzo:                   <b> <i>39</i> €                 </b> </span> <span class="standart">  Prezzo totale:                   <s>                   78 €                 </s> </span> </div>
            </div>
        </footer>
    </main>
    </body>
    </html>
<?php } ?>