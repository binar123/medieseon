<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html lang="es">
    <head>
        <!-- [pre]land_id = 5857 -->
        <script>var locale = "hu";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJTEATKLcCIAAEAAQAC5g8BAALhFgEKAQ8E8j8BgwA";</script>
        <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/js/dr.js"></script>
        <script type="text/javascript" src="./assets/js/dtime.js"></script>
        <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/js/validation.js"></script>
        <script type="text/javascript" src="./assets/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Bacsi Szilárd';
            var phone_hint = '+36309489465';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("hu");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Psoridex</title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/mobile/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/css/style.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({scrollTop: $("form").offset().top}, 1000);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <header>
        <div class="header_wrapper">
            <div class="main_container">
                <img class="logo" src="./assets/mobile/images/logo.jpg"/>
                <h1 class="header-head"><strong>POKOLLÁ TESZI ÉLETÉT A PIKKELYSÖMÖR?</strong></h1>
                <h2>
                    <span>A</span> <span> psoridex</span>
                    <span>segítségével</span> <span> megszabadul a problémától</span>
                </h2>
                <img class="product" src="./assets/mobile/images/psoridex-header.png"/>
                <a class="button toform" href="#form">Megrendelem</a>
                <ul class="header-list">
                    <li>A betegség számos típusa és formája ellen hatásos</li>
                    <li>Gátolja az érintett bőrsejtek növekedését</li>
                    <li>Megakadályozza a kiújulást</li>
                    <li>Eltünteti a bőrfoltokat</li>
                </ul>
            </div>
        </div>
    </header>
    <section class="offected">
        <div class="main_container">
            <h3>
                AZ ÉRINTETT BŐR
                HELYREÁLLÍTÁSA <span>LEHETSÉGES!</span>
            </h3>
            <p>
                A pikkelysömör a világ lakosságának 4%-át érinti. Legtöbb esetben fiatalokról van szó, a statisztikák
                szerint a pikkelysömörben szenvedők 70%-a még nem érte el a 20 éves életkort. Ezeknek a betegeknek az az
                álma, hogy anélkül mehessenek nyilvános helyekre, hogy az emberek fürkésző és gyakran megvető pillantásait
                érezzék magukon. A bőr állapota jelentősen javul

            </p>
        </div>
    </section>
    <section class="components">
        <div class="main_container">
            <h4>
                A BŐR KEZELÉSE<br/>
                <span>PSORIDEX</span>-SZEL!
            </h4>
            <div class="row">
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-1.png"/></div>
                        <h5>NAPRAFORGÓMAG OLAJ</h5>
                        <p>Felpuhítja a megkeményedett bőrt, csillapítja a viszketést, fájdalomcsillapító hatással van a
                            bőrre és ízületekre.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-2.png"/></div>
                        <h5>PARAFFINUM LIQUIDIUM</h5>
                        <p>Antimikróbális hatású, Magas fokú gyógyító képességgel rendelkezik, elősegíti má hatóanyagok
                            bőrön keresztül történő gyors felszívódását.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-3.png"/></div>
                        <h5>ÉDESMANDULA OLAJ</h5>
                        <p>Gyulladáscsökkentő hatású, szabályozza a faggyúmirigyek működését, simító és puhító hatással
                            rendelkezik.</p>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-4.png"/></div>
                        <h5>CSIPKEBOGYÓMAG OLAJ</h5>
                        <p>Ezt a könnyedén felszívódó olajat gyakran használják sebes, és sérült bőrfelületeken. Szükséges
                            vitaminokkal és ásványokkal gazdagítja a bőrt.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-5.png"/></div>
                        <h5>BÚZACSÍRA OLAJ</h5>
                        <p>Tápláló olaj, Mely megőrzi a kötőszövet egészségét, simítja és finomítja a bőr szerkezetét, és
                            elősegíti a regenerálódását.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><h5 id="last">
                                <span>BHT-T</span>, ASZKORBIL,
                                <span>PALMITÁT</span>,
                                GERANIOL,
                                <span>CITRONELLOL</span>,
                                IZOPROPIL-, MIRISZTÁT
                            </h5>
                        </div>
                        <p>
                            Ezen összetevők elegye fertőtleníti a bőrt, gátolja a komplikációkat, és tisztító hatású.
                            Normalizálják a bőr sejtanyagcseréjét és akadályozzák a bőr hámrétegének repedezését.
                        </p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section class="opinion">
        <div class="main_container">
            <h4>
                SZAKÉRTŐI VÉLEMÉNY
            </h4>
            <img src="./assets/mobile/images/doc.png"/>
            <article>
                <p>A pikkelysömörben szenvedő emberek nehézségekbe ütköznek a szociális beilleszkedés vagy akár a munkahelyi
                    adaptáció területén. Például: Az erős viszketés néha zavarja a normális pihenést, a kéz és
                    lábhajlatokban lévő plakkok korlátozhatják a beteg munkaképességét. A psoriasis érzékelhetően rontja az
                    életminőséget, úgy érzi a fizikai tünetek szellemileg is erősen megterhelőek.
                </p>
                <p><b>A Psоridex</b> már az első használat segít a betegnek könnyebben tolerálni a betegséget mind a kezdeti
                    stádiumú mind az előrehaladott betegség esetén.</p>
                <p><b>A krém használata 93%-ban megelőzi a betegség kiújulását.
                    </b></p>
            </article>
        </div>
    </section>
    <section class="customers">
        <div class="main_container">
            <h4>ARINA VOLSKAYA,</h4>
            <div class="row">
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-1.png"/>
                    <h5>ARINA VOLSKAYA,</h5>
                    <p class="age">30 éves</p>
                    <p class="history">
                        Mivel a pikkelysömör van nagyon sok szabadidős tevékenységet és szórakozást ki kellett hagynom, az
                        életem nem volt túl izgalmas.Orvos guttált pikkelysömört diagnosztizáltak nekem. Kis pattanásszerű
                        kiütések borították már az egész testemet.A Psoridex teljesítette a várakozásaimat. És most újra
                        visszatér az életem a normális kerékvágásba!
                    </p>
                </div>
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-2.png"/>
                    <h5>KIRA PANTYUH,</h5>
                    <p class="age">19 éves</p>
                    <p class="history">
                        Sokat aggódtam a hátamon lévő plakkos psoriasis miatt. Nem tudtam aludni, tanulni, enni, mert a
                        folyamatosan viszketett. Hálás vagyok, hogy anyám, aki hallott a krémről, ragaszkodott hozzá, hogy
                        próbáljam ki. Most használom Psoridex rendszeresen és már nem viszketek és a bőröm kinézete is sokat
                        javult!

                    </p>
                </div>
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-3.png"/>
                    <h5>OLEG NOVCHENKO,</h5>
                    <p class="age">25 éves</p>
                    <p class="history">
                        Ennek a krémnek egy csomó előnye van. Előrehaladott szakaszában volt a pikkelysömör a hasamon.
                        Miután elkezdtem használni a Psoridex krémet, úgy éreztem, a pozitív változások nagyon gyorsan
                        bekövetkeztek. A gyulladásos folyamat leállt, a bőrömön foltok és kiütések egyre kisebbek lettek, a
                        repedések meggyógyultak, a bőr egyre lágyabb és világosabb lett. Kövesse a használati utasítását a
                        Psoridex krémnek, és látni fogja, hogy a pikkelysömör valóban gyógyítható!
                    </p>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="main_container" id="form">
            <div class="row">
                <img alt="" class="prod" src="./assets/mobile/images/prod.png"/>
                <div class="pricer">
<span class="new">Új ár<br/>
<b>
<i>9700</i> Ft.
                </b>
</span>
                    <span class="standart">Normál ár

            <s>
                19400 Ft.
            </s>
</span>
                </div>
                <form action="process.php" method="post">


                    <select id="country_code_selector" name="">
                        <option value="HU">
                            Magyarország
                        </option>
                    </select>
                    <input name="name" placeholder="Név" type="text"/>
                    <input class="only_number" name="phone" placeholder="Telefonszám" type="text"/>
                    <input class="button js_submit" type="submit" value="Megrendelem"/>
                    <input class="hidden"  name="mobile" value="1" >
                </form>
            </div>
        </div>
    </footer>
    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 5857 -->
        <script>var locale = "hu";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJTEATKLcCIAAEAAQAC5g8BAALhFgEKAQ8E8j8BgwA";</script>
        <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/js/dr.js"></script>
        <script type="text/javascript" src="./assets/js/dtime.js"></script>
        <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/js/validation.js"></script>
        <script type="text/javascript" src="./assets/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Bacsi Szilárd';
            var phone_hint = '+36309489465';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("hu");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>
            Psoridex
        </title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet"/>
        <!--  Перерубаем сабсет  -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,700,400italic,300italic,700italic" rel="stylesheet" type="text/css"/>
        <!--  Перерубаем сабсет  -->
        <link href="./assets/css/animate.css" rel="stylesheet"/>
        <link href="./assets/css/animation.css" rel="stylesheet"/>
        <link href="./assets/css/twentytwenty.css" rel="stylesheet"/>
        <link href="./assets/css/style.css" rel="stylesheet"/>
        <script src="./assets/js/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="./assets/js/main.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.onscreen.min.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.event.move.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.twentytwenty.js" type="text/javascript"></script>
        <script src="./assets/js/animations.js" type="text/javascript"></script>
    </head>
    <body id="fade">
    <main>
        <header>
            <div class="main_container">
                <div class="row">
                    <a class="col-sm-2" href="#"><img class="logo" src="./assets/img/logo.jpg"/></a>
                    <div class="col-sm-2 warning align-right">
                        <p>Figyelem:</p>
                    </div>
                    <div class="col-sm-4 warning_ph align-left">
                        <p>
                            Az extrém magas televíziós kereslet miatt limitált készlet van raktáron Psoridex-ből,
                            <script>dtime_nums(-1, true)</script>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <div class="timer hidden" data-minutes-left="15">
                            <span class="hours clock_cols timer_delimiter">{h10}{h1}</span>
                            <span class="cl">:</span>
                            <span class="minutes clock_cols timer_delimiter">{m10}{m1}</span>
                            <span class="cl">:</span>
                            <span class="seconds clock_cols ">{s10}{s1}</span>
                            <span class="clearDiv"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section>
            <div id="slider">
                <div class="twentytwenty-before">
                </div>
                <div class="twentytwenty-after">
                </div>
            </div>
            <div class="on_slider">
                <div class="main_container">
                    <h1><strong>POKOLLÁ TESZI ÉLETÉT A PIKKELYSÖMÖR?</strong></h1>
                    <h2>
                        <span>A</span> <span>psoridex</span>
                        <span>segítségével</span> <span>megszabadul a problémától</span>
                    </h2>
                    <ul class="header-list">
                        <li>A betegség számos típusa és formája ellen hatásos</li>
                        <li>Gátolja az érintett bőrsejtek növekedését</li>
                        <li>Megakadályozza a kiújulást</li>
                        <li>Eltünteti a bőrfoltokat</li>
                    </ul>
                    <img class="product" src="./assets/img/psoridex-header.png"/>
                </div>
            </div>
        </section>
        <section class="offected">
            <div class="main_container">
                <a class="button" href="#form">Megrendelem</a>
                <h3>
                    AZ ÉRINTETT BŐR
                    HELYREÁLLÍTÁSA <span>LEHETSÉGES!</span>
                </h3>
                <p>
                    HELYREÁLLÍTÁSA LEHETSÉGES!
                    A pikkelysömör a világ lakosságának 4%-át érinti. Legtöbb esetben fiatalokról van szó, a statisztikák szerint a pikkelysömörben szenvedők 70%-a még nem érte el a 20 éves életkort. Ezeknek a betegeknek az az álma, hogy anélkül mehessenek nyilvános helyekre, hogy az emberek fürkésző és gyakran megvető pillantásait érezzék magukon. A bőr állapota jelentősen javul

                </p>
            </div>
        </section>
        <section class="components">
            <div class="main_container">
                <h4>
                    A BŐR KEZELÉSE<br/>
                    <span>PSORIDEX</span>-SZEL!
                </h4>
                <div class="row">
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-1.png"/></div>
                            <h5>NAPRAFORGÓMAG OLAJ</h5>
                            <p>Felpuhítja a megkeményedett bőrt, csillapítja a viszketést, fájdalomcsillapító hatással van a bőrre és ízületekre.</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-2.png"/></div>
                            <h5>PARAFFINUM LIQUIDIUM</h5>
                            <p>Antimikróbális hatású, Magas fokú gyógyító képességgel rendelkezik, elősegíti má hatóanyagok bőrön keresztül történő gyors felszívódását.</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-3.png"/></div>
                            <h5>ÉDESMANDULA OLAJ</h5>
                            <p>Gyulladáscsökkentő hatású, szabályozza a faggyúmirigyek működését, simító és puhító hatással rendelkezik.</p>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-4.png"/></div>
                            <h5>CSIPKEBOGYÓMAG OLAJ</h5>
                            <p>Ezt a könnyedén felszívódó olajat gyakran használják sebes, és sérült bőrfelületeken. Szükséges vitaminokkal és ásványokkal gazdagítja a bőrt.</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-5.png"/></div>
                            <h5>BÚZACSÍRA OLAJ</h5>
                            <p>Tápláló olaj, Mely megőrzi a kötőszövet egészségét, simítja és finomítja a bőr szerkezetét, és elősegíti a regenerálódását.</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <h5 id="last">
                                <span>BHT-T</span>,
                                ASZKORBIL,
                                <span>PALMITÁT</span>,
                                GERANIOL,
                                <span>CITRONELLOL</span>,
                                IZOPROPIL-, MIRISZTÁT
                            </h5>
                            <p>
                                Ezen összetevők elegye fertőtleníti a bőrt, gátolja a komplikációkat, és tisztító hatású. Normalizálják a bőr sejtanyagcseréjét és akadályozzák a bőr hámrétegének repedezését.
                            </p>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <section class="opinion">
            <div class="main_container">
                <h4>
                    SZAKÉRTŐI
                    <img src="./assets/img/doc.png"/>
                    VÉLEMÉNY
                </h4>
                <article>
                    <p>
                        A pikkelysömörben szenvedő emberek nehézségekbe ütköznek a szociális beilleszkedés vagy akár a munkahelyi adaptáció területén. Például: Az erős viszketés néha zavarja a normális pihenést, a kéz és lábhajlatokban lévő plakkok korlátozhatják a beteg munkaképességét. A psoriasis érzékelhetően rontja az életminőséget, úgy érzi a fizikai tünetek szellemileg is erősen megterhelőek.

                    </p>
                    <p><b>A Psоridex</b>  már az első használat segít a betegnek könnyebben tolerálni a betegséget mind a kezdeti stádiumú mind az előrehaladott betegség esetén.
                        primera aplicación y ayuda a deshacerse rápidamente de esta enfermedad.</p>
                    <p><b>
                            A krém használata 93%-ban megelőzi a betegség kiújulását.
                        </b></p>
                </article>
            </div>
        </section>
        <section class="customers">
            <div class="main_container">
                <h4>FOGYASZTÓI VÉLEMÉNYEK</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-1.png"/>
                        <h5>ARINA VOLSKAYA,</h5>
                        <p class="age">30 éves</p>
                        <p class="history">
                            Mivel a pikkelysömör van nagyon sok szabadidős tevékenységet és szórakozást ki kellett hagynom, az életem nem volt túl izgalmas.Orvos guttált pikkelysömört diagnosztizáltak nekem. Kis pattanásszerű kiütések borították már az egész testemet.A Psoridex teljesítette a várakozásaimat. És most újra visszatér az életem a normális kerékvágásba!

                        </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-2.png"/>
                        <h5>KIRA PANTYUH,</h5>
                        <p class="age">19 éves</p>
                        <p class="history">
                            Sokat aggódtam a hátamon lévő plakkos psoriasis miatt. Nem tudtam aludni, tanulni, enni, mert a folyamatosan viszketett. Hálás vagyok, hogy anyám, aki hallott a krémről, ragaszkodott hozzá, hogy próbáljam ki. Most használom Psoridex rendszeresen és már nem viszketek és a bőröm kinézete is sokat javult!

                        </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-3.png"/>
                        <h5>OLEG NOVCHENKO,</h5>
                        <p class="age">25 éves</p>
                        <p class="history">
                            Ennek a krémnek egy csomó előnye van. Előrehaladott szakaszában volt a pikkelysömör a hasamon. Miután elkezdtem használni a Psoridex krémet, úgy éreztem, a pozitív változások nagyon gyorsan bekövetkeztek. A gyulladásos folyamat leállt, a bőrömön foltok és kiütések egyre kisebbek lettek, a repedések meggyógyultak, a bőr egyre lágyabb és világosabb lett. Kövesse a használati utasítását a Psoridex krémnek, és látni fogja, hogy a pikkelysömör valóban gyógyítható!

                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="main_container" id="form">
                <div class="row">
                    <form action="process.php" method="post">
                        <p>Mondja el nekünk, hogy<br/> hova küldjük<br/>
                            <span>a csomagot</span></p>



                        <select id="country_code_selector" name="">
                            <option value="HU">
                                Magyarország
                            </option>
                        </select>
                        <input name="name" placeholder="Név" type="text"/>
                        <input class="only_number" name="phone" placeholder="Telefonszám" type="text"/>
                        <input class="button js_submit" type="submit" value="Megrendelem"/>
                    </form>
                    <span class="new">Új ár

                <b>
<i>9700</i> Ft.
                </b>
</span>
                    <span class="standart">Normál ár

                <s>
                    19400 Ft.
                </s>
</span>
                </div>
            </div>
        </footer>
    </main>
    </body>
    </html>
<?php } ?>