<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html lang="es">
    <head>
        <!-- [pre]land_id = 5478 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJPEAQob7-IAAEAAQAC4g8BAAJmFQEKAQ8EHFFIygA";</script>
        <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/js/dr.js"></script>
        <script type="text/javascript" src="./assets/js/dtime.js"></script>
        <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/js/validation.js"></script>
        <script type="text/javascript" src="./assets/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Judit Flores';
            var phone_hint = '+34681435813';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title></title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/mobile/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/css/style.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({scrollTop: $("form").offset().top}, 1000);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <header>
        <div class="header_wrapper">
            <div class="main_container">
                <img class="logo" src="./assets/mobile/images/logo.jpg"/>
                <h1 class="header-head"><strong>¿TE HACE LA VIDA UN INFIERNO LA PSORIASIS?</strong></h1>
                <h2>
                    <span>¡Con</span> <span> psoridex</span>
                    <span>te sentirás</span> <span> mejor!</span>
                </h2>
                <img class="product" src="./assets/mobile/images/psoridex-header.png"/>
                <a class="button toform" href="#form">Obtener mi pack</a>
                <ul class="header-list">
                    <li>efectiva en varios tipos y grados de la enfermedad</li>
                    <li>impide que afecte al crecimiento de células de la piel</li>
                    <li>elimina la Palindromía</li>
                    <li>proporciona un alto nivel de control de las manchas</li>
                </ul>
            </div>
        </div>
    </header>
    <section class="offected">
        <div class="main_container">
            <h3>
                ¡LA RECUPERACIÓN DE UNA
                PIEL AFECTADA <span>ES POSIBLE!</span>
            </h3>
            <p>
                Psoriasis afecta a un 4% de la población mundial y en la mayoría de los casos son jóvenes. De acuerdo con
                las estadísticas, el 70% de las personas que han padecido psoriasis, no habían cumplido aún los 20 años de
                edad. Las personas que sufren de esta enfermedad, evitan frecuentar lugares públicos, ya que se sienten
                cohibidas de tantas miradas desagradables. ¡Pero gracias a PSORIDEX, la solución ha llegado a tus manos, tú
                puedes mejorar el aspecto y la salud de tu piel!
            </p>
        </div>
    </section>
    <section class="components">
        <div class="main_container">
            <h4>
                ¡POR QUÉ<br/>
                <span>PSORIDEX</span> PARA TU PIEL!
            </h4>
            <div class="row">
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-1.png"/></div>
                        <h5>ACEITE DE SEMILLA DE HELIANTHUS ANNUUS</h5>
                        <p>Suaviza la queratodermia, tiene un efecto anti pruriginoso y actúa como analgésico en la piel y
                            en las articulaciones.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-2.png"/></div>
                        <h5>PARAFINA LIQUIDA</h5>
                        <p>Tiene acción antimicrobiana conteniendo innúmeras propiedades de curación y es absorbida en pocos
                            minutos dentro de la piel.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-3.png"/></div>
                        <h5>ACEITE PRUNUS AMYGDALUS DULCIS</h5>
                        <p>Reduce los procesos inflamatorios, controla las glándulas sebáceas y tiene propiedades calmantes
                            y suavizantes.</p>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-4.png"/></div>
                        <h5>ACEITE DE SEMILLA DE ROSA MOSQUETA</h5>
                        <p>Humedece la piel y da elasticidad al mismo tiempo, elimina de manera gradual la sequedad
                            resistente, también tiene propiedades antioxidantes y enriquece la piel con vitaminas y
                            minerales necesarios.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><img src="./assets/mobile/images/component-5.png"/></div>
                        <h5>ACEITE DE GERMEN DE TRITICUM VULGARE</h5>
                        <p>Tonifica la parte superior de la piel, alivia el picor y la molestia, promueve el proceso de
                            regeneración y elimina las escamas córneas.</p>
                    </article>
                </div>
                <div class="col-sm-12">
                    <article>
                        <div class="img-wrap"><h5 id="last">
                                <span>BHT</span>, ascorbilo palmitato,
                                <span>geraniol</span>,
                                citronelol,
                                <span>eugenol</span>,
                                miristato de isopropilo
                            </h5></div>
                        <p>Todos estos componentes antisépticos para la piel previenen de los epifenómenos, renuevan y
                            normalizan los procesos metabólicos celulares e inhiben la fisión de células epidérmicas.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section class="opinion">
        <div class="main_container">
            <h4>
                Experto
                opinión
            </h4>
            <img src="./assets/mobile/images/doc.png"/>
            <article>
                <p>La mayoría de las personas que sufren de psoriasis, encuentran una gran dificultad para adaptarse y
                    relacionarse en el mundo social y laboral que les rodea. Por ejemplo, un fuerte prurito a veces
                    interfiere en el descanso, y las placas que se crean en las manos pueden limitar la capacidad en el
                    trabajo, impidiéndoles ser competentes. La psoriasis afecta sensiblemente la calidad de vida, ya que las
                    personas que la padecen se sienten incómodas tanto física como mentalmente.</p>
                <p><b>Psоridex</b> es más que eficaz hasta mismo en pacientes ingresados, su efectividad se nota desde la
                    primera aplicación y ayuda a deshacerse rápidamente de esta enfermedad.</p>
                <p><b>Usar Psoridex como el principal método de tratamiento provoca que en el 93% de los casos se elimine la
                        Palindromia.</b></p>
            </article>
        </div>
    </section>
    <section class="customers">
        <div class="main_container">
            <h4>Los clientes comentario</h4>
            <div class="row">
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-1.png"/>
                    <h5>Adriana,</h5>
                    <p class="age">30 años</p>
                    <p class="history">A causa de la psoriasis, perdí la oportunidad de hacer muchos tipos de actividades y
                        pasatiempos. El médico me ha diagnosticado una psoriasis guttate, donde los pequeños brotes que
                        empezaron al principio se han apoderado de mi cuerpo. Ahora estoy muy feliz de haber encontrado
                        PSORIDEX, gracias a este producto pude volver a la normalidad.</p>
                </div>
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-2.png"/>
                    <h5>Sofía,</h5>
                    <p class="age">19 años</p>
                    <p class="history">
                        Todo empezó cuando me salieron manchas de psoriasis en mi espalda. Este problema me causó graves
                        trastornos, no podía dormir, estudiar, ni comer debido a la molestia que me causaba a diario. Doy
                        las gracias a mi madre, que ha buscado una solución y me ha ayudado a encontrar psoriadex. Ahora no
                        tengo nada de picor, ni molestias, he notado una mejoría bastante significativa y gratificante.
                        Muchas gracias!
                    </p>
                </div>
                <div class="col-sm-12">
                    <img src="./assets/mobile/images/customer-3.png"/>
                    <h5>Oscar García,</h5>
                    <p class="age">25 años</p>
                    <p class="history">Esta crema tiene un montón de ventajas, hace unos años tuve un problema de psoriasis
                        en la barriga y lo pasé más que fatal. Pero después de probar esta crema, los cambios fueron
                        notables enseguida. El proceso inflamatorio se detuvo, las manchas de mi piel y las erupciones se
                        redujeron en gran medida, y mi piel se volvió más suave y limpia. Yo lo recomiendo plenamente, la
                        psoriasis tiene cura!</p>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="main_container" id="form">
            <div class="row">
                <img alt="" class="prod" src="./assets/mobile/images/prod.png"/>
                <div class="pricer">
<span class="new">nuevo precio<br/>
<b>
<i>39</i> €
                </b>
</span>
                    <span class="standart">antes

            <s>
                78 €
            </s>
</span>
                </div>
                <form action="process.php" method="post">

                    <select class="selector" id="country_code_selector">
                        <option value="ES">
                            Spain
                        </option>
                    </select>
                    <input name="name" placeholder="Nombre" type="text"/>
                    <input class="only_number" name="phone" placeholder="Teléfono" type="text"/>
                    <div class="prices">
                        <div class="ship" style="display: none;">
                            Envío: 0 €


                        </div>
                        <div class="total">
                            Precio total: 39 €
                        </div>
                    </div>
                    <input class="button js_submit" type="submit" value="Conseguir mi pack"/>
                       <input class="hidden"  name="mobile" value="1" >
                    <div class="form_annotation">
                        * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                    </div>
                </form>
            </div>
        </div>
    </footer>
    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 5478 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJPEAQob7-IAAEAAQAC4g8BAAJmFQEKAQ8EHFFIygA";</script>
        <script type="text/javascript" src="./assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/js/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/js/dr.js"></script>
        <script type="text/javascript" src="./assets/js/dtime.js"></script>
        <script type="text/javascript" src="./assets/js/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/js/validation.js"></script>
        <script type="text/javascript" src="./assets/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Judit Flores';
            var phone_hint = '+34681435813';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>
            Psoridex
        </title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet"/>
        <!--  Перерубаем сабсет  -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,700,400italic,300italic,700italic" rel="stylesheet" type="text/css"/>
        <!--  Перерубаем сабсет  -->
        <link href="./assets/css/animate.css" rel="stylesheet"/>
        <link href="./assets/css/animation.css" rel="stylesheet"/>
        <link href="./assets/css/twentytwenty.css" rel="stylesheet"/>
        <link href="./assets/css/style.css" rel="stylesheet"/>
        <script src="./assets/js/jqueryplugin.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="./assets/js/main.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.onscreen.min.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.event.move.js" type="text/javascript"></script>
        <script src="./assets/js/jquery.twentytwenty.js" type="text/javascript"></script>
        <script src="./assets/js/animations.js" type="text/javascript"></script>
    </head>
    <body id="fade">
    <main>
        <header>
            <div class="main_container">
                <div class="row">
                    <a class="col-sm-2" href="#"><img class="logo" src="./assets/img/logo.jpg"/></a>
                    <div class="col-sm-2 warning align-right">
                        <p>ADVERTENCIA:</p>
                    </div>
                    <div class="col-sm-4 warning_ph align-left">
                        <p>
                            Debido a la demanda extremadamente alta de TV, hay una oferta limitada de Limpieza y
                            Desintoxicación
                            en la acción como del
                            <script>dtime_nums(-1, true)</script>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <div class="timer hidden" data-minutes-left="15">
                            <span class="hours clock_cols timer_delimiter">{h10}{h1}</span>
                            <span class="cl">:</span>
                            <span class="minutes clock_cols timer_delimiter">{m10}{m1}</span>
                            <span class="cl">:</span>
                            <span class="seconds clock_cols ">{s10}{s1}</span>
                            <span class="clearDiv"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section>
            <div id="slider">
                <div class="twentytwenty-before">
                </div>
                <div class="twentytwenty-after">
                </div>
            </div>
            <div class="on_slider">
                <div class="main_container">
                    <h1><strong>¿TE HACE LA VIDA UN INFIERNO LA PSORIASIS?</strong></h1>
                    <h2>
                        <span>¡Con</span> <span>psoridex</span>
                        <span>te sentirás</span> <span>mejor!</span>
                    </h2>
                    <ul class="header-list">
                        <li>efectiva en varios tipos y grados de la enfermedad</li>
                        <li>impide que afecte al crecimiento de células de la piel</li>
                        <li>elimina la Palindromía</li>
                        <li>proporciona un alto nivel de control de las manchas</li>
                    </ul>
                    <img class="product" src="./assets/img/psoridex-header.png"/>
                </div>
            </div>
        </section>
        <section class="offected">
            <div class="main_container">
                <a class="button" href="#form">Obtener mi pack</a>
                <h3>
                    ¡LA RECUPERACIÓN DE UNA<br/>
                    PIEL AFECTADA <span>ES POSIBLE!</span>
                </h3>
                <p>
                    Psoriasis afecta a un 4% de la población mundial y en la mayoría de los casos son jóvenes. De acuerdo
                    con
                    las estadísticas, el 70% de las personas que han padecido psoriasis, no habían cumplido aún los 20 años
                    de
                    edad. Las personas que sufren de esta enfermedad, evitan frecuentar lugares públicos, ya que se sienten
                    cohibidas de tantas miradas desagradables. ¡Pero gracias a PSORIDEX, la solución ha llegado a tus manos,
                    tú
                    puedes mejorar el aspecto y la salud de tu piel!
                </p>
            </div>
        </section>
        <section class="components">
            <div class="main_container">
                <h4>
                    ¡POR QUÉ<br/>
                    <span>PSORIDEX</span> PARA TU PIEL!
                </h4>
                <div class="row">
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-1.png"/></div>
                            <h5>ACEITE DE SEMILLA DE HELIANTHUS ANNUUS</h5>
                            <p>Suaviza la queratodermia, tiene un efecto anti pruriginoso y actúa como analgésico en la piel
                                y
                                en las articulaciones.</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-2.png"/></div>
                            <h5>PARAFINA LIQUIDA</h5>
                            <p>Tiene acción antimicrobiana conteniendo innúmeras propiedades de curación y es absorbida en
                                pocos
                                minutos dentro de la piel.</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-3.png"/></div>
                            <h5>ACEITE PRUNUS AMYGDALUS DULCIS</h5>
                            <p>Reduce los procesos inflamatorios, controla las glándulas sebáceas y tiene propiedades
                                calmantes
                                y suavizantes.</p>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-4.png"/></div>
                            <h5>ACEITE DE SEMILLA DE ROSA MOSQUETA</h5>
                            <p>Humedece la piel y da elasticidad al mismo tiempo, elimina de manera gradual la sequedad
                                resistente, también tiene propiedades antioxidantes y enriquece la piel con vitaminas y
                                minerales necesarios.</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <div class="img-wrap"><img src="./assets/img/component-5.png"/></div>
                            <h5>ACEITE DE GERMEN DE TRITICUM VULGARE</h5>
                            <p>Tonifica la parte superior de la piel, alivia el picor y la molestia, promueve el proceso de
                                regeneración y elimina las escamas córneas.</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article>
                            <h5 id="last">
                                <span>BHT</span>, ascorbilo palmitato,
                                <span>geraniol</span>,
                                citronelol,
                                <span>eugenol</span>,
                                miristato de isopropilo
                            </h5>
                            <p>Todos estos componentes antisépticos para la piel previenen de los epifenómenos, renuevan y
                                normalizan los procesos metabólicos celulares e inhiben la fisión de células
                                epidérmicas.</p>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <section class="opinion">
            <div class="main_container">
                <h4>
                    Experto
                    <img src="./assets/img/doc.png"/>
                    opinión
                </h4>
                <article>
                    <p>La mayoría de las personas que sufren de psoriasis, encuentran una gran dificultad para adaptarse y
                        relacionarse en el mundo social y laboral que les rodea. Por ejemplo, un fuerte prurito a veces
                        interfiere en el descanso, y las placas que se crean en las manos pueden limitar la capacidad en el
                        trabajo, impidiéndoles ser competentes. La psoriasis afecta sensiblemente la calidad de vida, ya que
                        las
                        personas que la padecen se sienten incómodas tanto física como mentalmente.</p>
                    <p><b>Psоridex</b> es más que eficaz hasta mismo en pacientes ingresados, su efectividad se nota desde
                        la
                        primera aplicación y ayuda a deshacerse rápidamente de esta enfermedad.</p>
                    <p><b>Usar Psoridex como el principal método de tratamiento provoca que en el 93% de los casos se
                            elimine la
                            Palindromia.</b></p>
                </article>
            </div>
        </section>
        <section class="customers">
            <div class="main_container">
                <h4>Los clientes comentario</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-1.png"/>
                        <h5>Adriana,</h5>
                        <p class="age">30 años</p>
                        <p class="history">A causa de la psoriasis, perdí la oportunidad de hacer muchos tipos de
                            actividades y
                            pasatiempos. El médico me ha diagnosticado una psoriasis guttate, donde los pequeños brotes que
                            empezaron al principio se han apoderado de mi cuerpo. Ahora estoy muy feliz de haber encontrado
                            PSORIDEX, gracias a este producto pude volver a la normalidad.</p>
                    </div>
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-2.png"/>
                        <h5>Sofía,</h5>
                        <p class="age">19 años</p>
                        <p class="history">
                            Todo empezó cuando me salieron manchas de psoriasis en mi espalda. Este problema me causó graves
                            trastornos, no podía dormir, estudiar, ni comer debido a la molestia que me causaba a diario.
                            Doy
                            las gracias a mi madre, que ha buscado una solución y me ha ayudado a encontrar psoriadex. Ahora
                            no
                            tengo nada de picor, ni molestias, he notado una mejoría bastante significativa y gratificante.
                            Muchas gracias!
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="./assets/img/customer-3.png"/>
                        <h5>Oscar García,</h5>
                        <p class="age">25 años</p>
                        <p class="history">Esta crema tiene un montón de ventajas, hace unos años tuve un problema de
                            psoriasis
                            en la barriga y lo pasé más que fatal. Pero después de probar esta crema, los cambios fueron
                            notables enseguida. El proceso inflamatorio se detuvo, las manchas de mi piel y las erupciones
                            se
                            redujeron en gran medida, y mi piel se volvió más suave y limpia. Yo lo recomiendo plenamente,
                            la
                            psoriasis tiene cura!</p>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="main_container" id="form">
                <div class="row">
                    <form action="process.php" method="post">
                        <p>Dónde quieres recibir<br/>
                            <span>tu pack</span></p>


                        <select id="country_code_selector" name="">
                            <option value="ES">
                                Spain
                            </option>
                        </select>
                        <input name="name" placeholder="Nombre" type="text"/>
                        <input class="only_number" name="phone" placeholder="Teléfono" type="text"/>
                        <div class="prices">
                            <div class="ship" style="display: none;">
                                Envío: 0 €

                            </div>
                            <div class="total">
                                Precio total: 39 €
                            </div>
                        </div>
                        <input class="button js_submit" type="submit" value="Conseguir mi pack"/>
                        <div class="form_annotation">
                            * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                        </div>
                    </form>
                    <span class="new">Nuevo precio

                <b>
<i>39</i> €
                </b>
</span>
                    <span class="standart">Antes

                <s>
                    78 €
                </s>
</span>
                </div>
            </div>
        </footer>
    </main>
    </body>
    </html>
<?php } ?>