<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7335 -->
        <script>var locale = "bg";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAKHEwQEnSCJAAEAAQACrhcBAAKnHAIGAQEABA1D7akA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":80,"old_price":160,"shipment_price":0},"3":{"price":160,"old_price":320,"shipment_price":0},"5":{"price":320,"old_price":640,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Dobre Mioara';
            var phone_hint = '+35924372590';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("bg");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="//st.acstnst.com/content/second/Titan_Gel_BG/js/secondPage.js"></script>
        <link type="text/css" href="//st.acstnst.com/content/second/Titan_Gel_BG/css/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
        <title>TITAN GEL</title>
        <link href="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/css/slick.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/css/style.css" media="all" rel="stylesheet" type="text/css"/>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $(".js_errorMessage2").remove();
                        $('.toform').click();
                    }
                });
            });
        </script>
    </head>
    <body>
    <div class="wrapper-cont s__main">
        <div class="block block1 with-gradient">
            <div class="limit clearfix">
                <a class="logo pre_toform" data-scroll="true" data-scroll-to="#request-form" href="#">TITAN GEL</a>
                <div class="content">
                    <p class="title1 ttu">УВЕЛИЧАВА РАЗМЕРА НА ПЕНИСА </p>
                    <p class="title2">TITAN GEL НАЙ-ДОБРЕ УГОЛЕМЯВА РАЗМЕРА!</p>
                    <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/thors_hummer.png"/>
                    <a class="btn btn-primary pre_toform" data-scroll="true" data-scroll-to="#request-form" href="#">поръчай сега</a>
                    <img alt="" class="body" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block1-1.png"/>
                    <div class="list">
                        <span class="ttu"> ИЗПОЛЗВАЙКИ TITAN GEL, ЛЕСНО И ЕФЕКТИВНО ЩЕ ПОСТИГНЕТЕ СЛЕДНОТО:</span>
                        <ul>
                            <li>Увеличаване дебелината и дължината на вашия пенис с до 6-8 см</li>
                            <li>Подобряване на еректилната функция</li>
                            <li>По-голяма чувствителност по време на полов акт, по-дълги и по-силни оргазми</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block2">
            <div class="limit limit-bl_2">
                <div class="row">
                    <div class="item">
                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block2-1.jpg"/>
                        <span>Увеличаване дължината и размера на вашия пенис</span>
                    </div>
                    <div class="item">
                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block2-2.jpg"/>
                        <span> Неограничено либидо</span>
                    </div>
                </div>
                <div class="row">
                    <div class="item">
                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block2-3.jpg"/>
                        <span>Мощна ерекция </span>
                    </div>
                    <div class="item">
                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block2-4.jpg"/>
                        <span>Увеличаване количеството на спермата </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block3 with-gradient">
            <div class="limit">
                <p class="title ttu">
<span>
                МАЛКИ РАЗМЕРИ
            </span>
                    1960-2015 гг.
                </p>
                <div class="graph">
                    <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block3_chart.png"/>
                </div>
                <div class="info">
                    Според доклад на СЗО от март 2012 г. броят на мъжете с малки размери се е увеличавал през 2011 г. Пациенти от различни възрасти са търсили медицинска помощ (операция). Съобщава се за важни проблеми в леглото по-често през последните години.

                </div>
            </div>
        </div>
        <div class="block block4">
            <div class="limit">
                <p class="title">
                    <span class="ttu">БОЖЕСТВЕН РАЗМЕР</span>
                    <em>
                        Ефектът на Titan Gel е бърз и перфектен
                    </em>
                </p>
            </div>
        </div>
        <div class="block block5">
            <div class="limit">
                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block5-woman.png"/>
                <p class="label">
<span>
           ПЕНИСЪТ СЕ УГОЛЕМЯВА, НЕЗАВИСИМО ОТ ЗДРАВОСЛОВНОТО ВИ СЪСТОЯНИЕ И НАЧИН НА ЖИВОТ
            </span>
                </p>
                <ol>
                    <li>
                        Натурални съставки без странични ефекти
                    </li>
                    <li>
                        Не води до пристрастяване, следователно не изпитвате нужда
                        да увеличавате дозировката
                    </li>
                    <li>
                        Може да го спрете по всяко време - контролирайте дължината си свободно
                    </li>
                </ol>
            </div>
        </div>
        <div class="block block6 with-gradient">
            <div class="limit">
                <p class="title1 ttu">УГОЛЕМЯВАНЕ НА ПЕНИСА</p>
                <p class="title2">Само една тубичка е достатъчна, за да постигнете значителни и устойчиви резултати: до 4-6 см</p>
                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/thors_hummer.png"/>
                <p class="title3 ttu">
                    СВЕТОВНО ПРИЗНАТИ СЕКСОЛОЗИ ЗА TITAN GEL:
                </p>
                <div class="description">
                    <p>
                        Гелът за уголемяване на пениса Titan Gel, който се е доказал като ефективно средство за увеличаване дължината и дебелината на пениса сред хора от различни възрастови групи, е много органичен продукт. Доставяйки всички необходими витамини и микроелементи, той активира естественото производство на мъжкия хормон, наречен тестостерон и допринася за по-добра циркулация на кръвта в гениталиите.
                        Titan Gel работи лесно и бързо и ефектът ще трае вечно.
                    </p>
                </div>
                <a class="btn btn-primary pre_toform" data-scroll="true" data-scroll-to="#request-form" href="#">
                    поръчай сега</a>
            </div>
        </div>
        <div class="block block7">
            <div class="limit">
                <p class="title1 ttu">ГЕЛ
                    <span>Titan Gel</span></p>
                <p class="title2">
                    ТАЙНИ СЪСТАВКИ, КОИТО ЩЕ СТИМУЛИРАТ РАСТЕЖА НА ВАШИЯ ПЕНИС
                </p>
                <div class="row">
                    <div class="item">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block8-1.jpg"/>
                            <div>
                                <span>ОСИГУРЯВА ВСИЧКО НЕОБХОДИМО ЗА УГОЛЕМЯВАНЕ НА ПЕНИСА</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block8-2.jpg"/>
                            <div>
                                <span>НОРМАЛИЗИРА КРЪВНОТО НАЛЯГАНЕ И КРЪВООБРАЩЕНИЕТО</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="item">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block8-3.jpg"/>
                            <div>
                                <span>ПОВИШАВА ЛИБИДОТО</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block8-4.jpg"/>
                            <div>
                                <span>ЗАДЕЙСТВА ПРОИЗВОДСТВОТО НА МЪЖКИ ХОРМОНИ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block8">
            <div class="dark-title">
                <div class="limit">
                    <div class="table">
<span class="ttu label">
                ТАЙНАТА НА БЕЗПРЕЦЕДЕНТНИЯ ЕФЕКТ
                    <text class="blue">
                       НА TITAN GEL

                    </text>
</span>
                        <span class="value">
<i> Уникалните компоненти гарантират удивителните резултати на Titan Gel:</i>
</span>
                    </div>
                </div>
            </div>
            <div class="limit">
                <div class="item">
                    <div class="image"><img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block8-squad1-1.jpg"/></div>
                    <div class="description with-label">
                        <span>ЕКСТРАКТ ОТ ЕПИМЕДИУМ</span>
                        <p>
                            Това растение има способността естествено да стимулира сексуалното желание и да подобрява сексуалната активност.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="image"><img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block8-squad1-2.jpg"/></div>
                    <div class="description with-label">
                        <span>ЕКСТРАКТ ОТ БЛАГОСЛОВЕН ТРЪН</span>
                        <p>
                            Бодливият подарък на природата спомага за производството на мъжкия хормон тестостерон в тялото. Тъй като този хормон влияе на мъжкото тяло по много начини, екстрактът от тръна спомага за по-дълга, по-твърда и по-продължителна ерекция.

                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="image"><img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block8-squad1-3.jpg"/></div>
                    <div class="description with-label">
                        <span> ЕКСТРАКТ ОТ МАКА (LEPIDIUM MEYENII)</span>
                        <p>
                            От много векове това растение е широко използвано за повишаване на мъжката сила. Нашите предци за знаели за него и растението не е загубило влиянието си и до днес.  </p>
                    </div>
                </div>
                <div class="item">
                    <div class="image"><img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block8-squad1-4.jpg"/></div>
                    <div class="description with-label">
<span>
                      ЕЛАСТИН
                    </span>
                        <p>
                            Това е единственият протеин, който в действителност може да лекува тъканите. Още повече, че той има невероятната способност да повишава чувствителността на пениса и да стимулира растежа му. Ефектът се постига с помощта на активните съставки, включително аминокиселини.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block9 with-gradient">
            <div class="limit">
                <p class="title1 ttu">УГОЛЕМЯВАНЕ НА ВАШИЯ ПЕНИС С TITAN GEL!

                </p>
                <p class="title2 ttu">РЕЗУЛТАТЪТ Е НЕВЕРОЯТЕН!</p>
                <hr class="bl9-hr"/>
                <p class="title3">
                    Преди и след: значително уголемяване на пениса, по-мощна ерекция и удължаване продължителността на половия акт
                </p>
                <div class="compare clearfix">
                    <div class="side before">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block9-before.png"/>
                        </div>
                        <!--  <span class="tooltip tooltip-shoulder">
                                                  Плечо - 38 см
                                              </span>-->
                        <span class="tooltip tooltip-waist">
                 Обиколка: 11 см
                </span>
                        <span class="tooltip tooltip-hip">
                Дължина 15 см
                </span>
                    </div>
                    <div class="side after">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block9-after.png"/>
                            <span class="weight">
</span>
                        </div>
                        <span class="tooltip tooltip-shoulder">
                   Обиколка: 13 см
                </span>
                        <!--   <span class="tooltip tooltip-waist">
                                                   Талия - 70 см
                                               </span>-->
                        <span class="tooltip tooltip-hip tl-rs">
               Дължина: 20 см
                </span>
                    </div>
                </div>
                <div class="mmmr">
                    <div>
                        <b>ПРОДЪЛЖИТЕЛНОСТ НА ЕРЕКЦИЯТА:
                        </b>
                        10 до 15 минути
                    </div>
                    <div>
                        <b>ПРОДЪЛЖИТЕЛНОСТ НА ЕРЕКЦИЯТА:</b>
                        45-65 минути
                    </div>
                </div>
            </div>
        </div>
        <div class="block block10">
            <div class="limit">
                <p class="title1 ttu">ЩЕ ТЕ НАПРАВИ АЛФА МЪЖ ЗА НУЛА ВРЕМЕ!</p>
                <p class="title2">TITAN GEL
                </p>
                <div class="row">
                    <div class="before">
                        <div>
                            <span>ПРЕДИ</span>
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block12-before1.jpg"/>
                        </div>
                    </div>
                    <div class="after">
                        <div>
                            <span>СЛЕД</span>
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block12-after1.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block11">
            <div class="limit">
                <p class="title ttu">
                    ИСТИНСКИ ИСТОРИИ
                </p>
                <div class="content slider">
                    <div class="item">
                        <div class="image">
                            <!--   <span class="minus">-15 кг</span>-->
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block13-1.jpg"/>
                        </div>
                        <p class="label">  Перфектен съм в спалнята. Резултатът е чудесен!</p>
                        <p class="name"> ИВАН, 43 ГОДИНИ</p>
                        <p class="story">
                            Винаги съм работил упорито, за да осигуря по-добър живот за семейството си. За жалост просто не съм толкова голям, за да задоволя жена си. Правим секс само през уикенда и е много скучен, тя се оплаква, че не чувства нищо и никога не достига до оргазъм. Веднъж един приятел ме посъветва да опитам Titan Gel. Направо съм шокиран от резултата. Само след пет дни започнах да забелязвам, че става по-дълъг и по-дебел поне +4 см!  <br/>  Сега спя с жена ми всеки ден, понякога по цяла нощ и тя е толкова щастлива! Очите ѝ отново блестят и усещам, че е е по-активна и изглежда по-млада. Животът ми определено стана по-добър!
                        </p>
                    </div>
                    <div class="item">
                        <div class="image">
                            <!-- <span class="minus">-22 кг</span>-->
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block13-2.jpg"/>
                            <!-- <p class="result">
                                                         <span class="before">До: 65 кг</span>
                                                         <span class="after">После: 43 кг</span>
                                                     </p>-->
                        </div>
                        <p class="label"> Методът, който спаси брака ми</p>
                        <!-- <p class="duration">Время приёма: 2 месяца</p>-->
                        <p class="name">АНИ, 35 ГОДИНИ </p>
                        <p class="story">
                            Винаги се опитвам да се поддържам във форма. Забелязах обаче, че мъжът ми губи интерес към мен. Обмислих проблема от различни гледни точки и осъзнах, че това беше поради факта, че имаше проблем с либидото му. По съвет на моя приятелка му купих Titan Gel. Това, което не подозирах, е че той не просто ще реши проблема с либидото, но и ще го направи МНОГО ПО-ГОЛЯМ! Много ми харесва, сексът ни е страхотен. Това е едно свежо начало в нашия брачен живот. Всъщност преди той искаше да се разведе с мен, защото си мислеше, че заслужавам някой по-добър.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block13-3.jpg"/>
                        </div>
                        <p class="label"> Перфектният резултат във всяка ситуация </p>
                        <!--   <p class="duration">Время приёма: 6 месяцев</p>-->
                        <p class="name"> БИСЕР, 28 ГОДИНИ</p>
                        <p class="story">
                            Тъй като съм културист, прекарвах много време във фитнеса и водех здравословен начин на живот. Мислех си, че никога няма да имам проблеми в леглото. Оказа се, че греша, много момичета не искаха да започнат връзка с мен, изчезваха след първата ни нощ заедно. Те никога не го казваха директно, но проблемът беше малкият ми (12 см) пенис. Веднъж срещнах момиче и този път почувствах, че е специална, затова ѝ казах, че съм много сериозен по отношение на връзката ни и тя отвърна със същото. Бях много разтревожен, когато правихме секс за първи път. За щастие тя се оказа достатъчно умна да не ме съди. Тя не ми се смя, а вместо това ми каза за Titan Gel. Сега съм с +5 см и мога да я задоволявам по три-четири пъти всяка нощ!

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block12 with-gradient">
            <div class="gray-stripe">
                <p class="title1">96% ОТ МЪЖЕТЕ СА ПОТВЪРДИЛИ ЕФЕКТИВНОСТТА НА  <strong>TITAN GEL</strong> </p>
            </div>
            <div class="limit">
                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/block14-zeta.png"/>
                <div class="story">
                    Много мъже страдат от неудовлетворителен размер на пениса. Може да има различни причини, включително възраст, чест стрес, нездравословно или недостатъчно хранене, липса на почивка, липса на хормони, злоупотреба с алкохол и никотин и други. Всички те водят до един и същи резултат: спад в качеството на сексуалния живот. През последните 20 години съм виждал мъже на различни възрасти и начини на живот с този проблем. Успяхме да намерим перфектното средство, което да им помогне. А именно Titan Gel! По време на клиничните проучвания той се доказа като ефективен дори и в най-тежките случаи. Определено мога да препоръчам Titan Gel на всичките си пациенти, като най-доброто средство. Онези, които са го опитали, го оценяват!
                    <span>ЕКСПЕРТНО МНЕНИЕ</span>
                </div>
                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/img/thors_hummer.png" width="350px"/>
                <div class="form">
                    <p class="title1 ttu">ПОРЪЧАЙ СЕГА </p>
                    <p class="title2 ttu">TITAN GEL</p>
                    <form action="/order/create/" id="order_form" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIAKHEwAAAAAAAAAAAARuJTqiAA"></iframe>

                        <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="80.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAKHEwQEnSCJAAEAAQACrhcBAAKnHAIGAQEABA1D7akA">
                        <input type="hidden" name="goods_id" value="49">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Titan_gel_BG_Dark">
                        <input type="hidden" name="price" value="80">
                        <input type="hidden" name="old_price" value="160">
                        <input type="hidden" name="al" value="7335">
                        <input type="hidden" name="total_price_wo_shipping" value="80.0">
                        <input type="hidden" name="currency" value="Лева">
                        <input type="hidden" name="package_id" value="1">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="BG">
                        <input type="hidden" name="shipment_vat" value="0.2">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 320, 'old_price': 640, 'shipment_price': 0}}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.2">
                        <!-- -->
                       <!-- <div class="fields">
                            <div class="form-group">
                                <select id="country_code_selector" name="country" placeholder="asda">
                                    <option selected="selected" value="BG">България</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input name="name" placeholder="Име и фамилия" type="text" value=""/>
                            </div>
                            <div class="form-group">
                                <input class="only_number" name="phone" placeholder="Телефон" type="text" value=""/>
                            </div>-->
                            <div class="price">
                                <s><span class="js_old_price">160</span><span class="js_curs">Лева</span></s>
                                <span> <span class="js_new_price n_price">80</span><span class="js_curs">Лева</span></span>
                            </div>
                            <!--<div class="form-group buttons">
                                <button class="ttu btn btn-primary btn-block js_pre_toform">поръчай</button>
                                <div class="toform"></div>
                            </div>
                        </div>
                        <!---->
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/js/slick.min.js" type="text/javascript"></script>
    <script src="//st.acstnst.com/content/Titan_gel_BG_Dark/mobile/js/main.js" type="text/javascript"></script>
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="//st.acstnst.com/content/second/Titan_Gel_BG/css/beauty_1.css" rel="stylesheet"/>
    <div class="hidden-window v1_1_1_1">
        <div class="h-w-header">
            <div class="h-w-contant">
                <div class="h-w-header-text">
                    <span><b>Titan Gel</b> - поръчайте още сега!</span>
                </div>
            </div>
        </div>
        <div class="h-w-main">
            <div class="h-w-contant">
                <div class="h-w-left">
                    <div class="h-w-payment h-w-decor-block h-w-vis-mob">
                        <p>Плащане <span>ПРИ ДОСТАВКА</span></p>
                        <ul>
                            <li><span>ПОРЪЧКАТА</span></li>
                            <li><span>ДОСТАВКА</span></li>
                            <li><span>ДОСТАВКА И ПЛАЩАНЕ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob h-w-ico h-w-select">
                        <select class="change-package-selector" name="count_select">
                            <option data-slide-index="0" value="1">1 опаковка</option>
                            <option data-slide-index="1" selected="selected" value="3">2+1 опаковки </option>
                            <option data-slide-index="2" value="5">3+2 опаковки </option>
                        </select>
                    </div>
                    <ul class="bx-bx">
                        <li data-value="1">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class=" h-w-item-prod"></div>
                                    </div>
                                    <div class="h-w-item-description">получавате 100 % удължаване на половият акт, както и повишена чувствителност на главичката на пениса</div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковка</p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>160 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Общо:</span><span>80 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има 50% ОТСТЪПКА
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="active" data-value="3">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"></div>
                                        <span><font>+ подарък</font></span>
                                    </div>
                                    <div class="h-w-item-description">този курс Ви помага да увеличите размера с 1-1,5 см, както и да удължите половият акт на 10-15 мин</div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковки <span>подарък</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>320 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Общо:</span><span>160 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има 50% ОТСТЪПКА
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-value="5">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"><span></span></div>
                                        <span><font>+ подарък</font></span>
                                    </div>
                                    <div class="h-w-item-description">курс, с помощта на който членът се увеличава стабилно с 4-5 см. При 85 %, от поръчалите го, се увеличава броят на партньорите, половият акт е ярък, регулярен и продължителен.</div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковки <span>подарък</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>640 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Общо:</span><span>320 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има 50% ОТСТЪПКА
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="h-w-right">
                    <div class="h-w-payment h-w-decor-block h-w-hide-mob">
                        <p>Плащане <span>ПРИ ДОСТАВКА</span></p>
                        <ul>
                            <li><span>ПОРЪЧКАТА</span></li>
                            <li><span>ДОСТАВКА</span></li>
                            <li><span>ДОСТАВКА И ПЛАЩАНЕ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-honesty h-w-ico h-w-hide-mob">ДОБРА СДЕЛКА</div>
                    <div class="h-w-form h-w-decor-block js_scrollForm">
                        <p>ВЪВЕДЕТЕ ДАННИ ЗА ПОРЪЧКА</p>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAKHEwAAAAAAAAAAAARuJTqiAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="80.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAKHEwQEnSCJAAEAAQACrhcBAAKnHAIGAQEABA1D7akA">
                            <input type="hidden" name="goods_id" value="49">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Titan_gel_BG_Dark">
                            <input type="hidden" name="price" value="80">
                            <input type="hidden" name="old_price" value="160">
                            <input type="hidden" name="al" value="7335">
                            <input type="hidden" name="total_price_wo_shipping" value="80.0">
                            <input type="hidden" name="currency" value="Лева">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="BG">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 320, 'old_price': 640, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.2">

                            <div class="h-w-select h-w-ico" style="display:none;">
                                <select id="country_code_selector" name="country_code">
                                    <option value="BG">България</option>
                                </select>
                            </div>
                            <div class="h-w-select h-w-ico">
                                <select class="change-package-selector" name="count_select">
                                    <option data-slide-index="0" value="1">1 опаковка</option>
                                    <option data-slide-index="1" selected="selected" value="3">2+1 опаковки </option>
                                    <option data-slide-index="2" value="5">3+2 опаковки </option>
                                </select>
                            </div>
                            <div style="display:none;">
                                <input name="name" type="text"/>
                                <input class="only_number" name="phone" type="text"/>
                            </div>
                            <input name="address" placeholder="Add meg a címed" type="text"/>
                            <!--  -->
                            <div class="h-w-text-total">Общо: <span><font class="js_full_price">160</font> Лева</span></div>
                           <!-- <button class="js_submit">Поръчай</button>-->
                        </form>
                    </div>
                    <div class="h-w-guarantee h-w-decor-block">
                        <p>НИЕ ГАРАНТИРАНЕ:</p>
                        <ul>
                            <li><b>100%</b> качество</li>
                            <li><b>Проверка</b> на продукта при получаване</li>
                            <li><b>Безопасност</b> на вашите данни</li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob"><div class="h-w-honesty h-w-ico">ДОБРА СДЕЛКА</div></div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7335 -->
        <script>var locale = "bg";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAKHEwQEnSCJAAEAAQACrhcBAAKnHAIGAQEABA1D7akA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":80,"old_price":160,"shipment_price":0},"3":{"price":160,"old_price":320,"shipment_price":0},"5":{"price":320,"old_price":640,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Dobre Mioara';
            var phone_hint = '+35924372590';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("bg");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript" src="//st.acstnst.com/content/second/Titan_Gel_BG/js/secondPage.js"></script>
        <link type="text/css" href="//st.acstnst.com/content/second/Titan_Gel_BG/css/secondPage.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>TITAN GEL
        </title>
        <link href="//st.acstnst.com/content/Titan_gel_BG_Dark/css/style.css" media="all" rel="stylesheet" type="text/css"/>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $(".js_errorMessage2").remove();
                        $('.toform').click();
                    }
                });
            });
        </script>
    </head>
    <body>
    <div class="wrapper s__main">
        <div class="block block1">
            <div class="limit clearfix">
                <a class="logo pre_toform" data-scroll="true" data-scroll-to="#request-form" href="#">TITAN GEL</a>
                <div class="content">
                    <p class="title1 ttu light">  УВЕЛИЧАВА РАЗМЕРА НА ПЕНИСА  </p>
                    <p class="title2 ttu light">  TITAN GEL НАЙ-ДОБРЕ УГОЛЕМЯВА РАЗМЕРА!  </p>
                    <p class="description">  Всичко, което ви трябва, за да накарате жените да полудеят по вас  </p>
                    <div class="list">
                        <span class="ttu">  ИЗПОЛЗВАЙКИ TITAN GEL, ЛЕСНО И ЕФЕКТИВНО ЩЕ ПОСТИГНЕТЕ СЛЕДНОТО:  </span>
                        <ul>
                            <li>  Увеличаване дебелината и дължината на вашия пенис с до 6-8 см  </li>
                            <li>  Подобряване на еректилната функция  </li>
                            <li>  По-голяма чувствителност по време на полов акт, по-дълги и по-силни оргазми  </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block2">
            <div class="limit clearfix">
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block2-1.png"/>
                    <div>
                        <span>  Увеличаване дължината и размера на вашия пенис  </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block2-2.png"/>
                    <div>
                        <span>  Неограничено либидо  </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block2-3.png"/>
                    <div>
                        <span>  Мощна ерекция  </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block2-4.png"/>
                    <div>
                        <span>  Увеличаване количеството на спермата  </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block2-5.png"/>
                    <div>
                        <span>  Продължителен полов акт  </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block3">
            <div class="limit">
                <p class="title1 ttu">  В МОМЕНТА КРЕМОВЕТЕ И БИОЛОГИЧНО АКТИВНИТЕ ДОБАВКИ ВСЕ ОЩЕ СА ОСНОВНИТЕ МЕТОДИ ЗА УГОЛЕМЯВАНЕ НА ПЕНИСА.
                </p>
                <div class="clearfix">
                    <div class="l-graph">
                        <p class="title ttu">
<span>МАЛКИ РАЗМЕРИ
                </span>
                            1960-2015
                        </p>
                        <div class="content">
<span class="line line1">Проблеми в леглото 18-24
                </span>
                            <span class="line line2">Проблеми в леглото 25-34
                </span>
                            <span class="line line3" style="top: 167px;"> Проблеми в леглото  <br/> 35-50
                </span>
                            <span class="line line4" style="top: 180px;">Намалено либидо  <br/> 35-55
                </span>
                            <span class="line line5">Намалено либидо 35-55
                </span>
                        </div>
                        <div class="info">
                            Според доклад на СЗО от март 2012 г. броят на мъжете с малки размери се е увеличавал през 2011 г. Пациенти от различни възрасти са търсили медицинска помощ (операция). Съобщава се за важни проблеми в леглото по-често през последните години.
                        </div>
                    </div>
                    <div class="r-graph">
                        <div class="title clearfix">
                            <p class="ttu">ПРЕДПОЧИТАНИЯТ МЕТОД ЗА УГОЛЕМЯВАНЕ НА ПЕНИСА
                            </p>
                            <ul>
                                <li>Мъже, 18-30 години</li>
                                <li>Мъже, 30-60 години</li>
                            </ul>
                        </div>
                        <div class="content">
                            <div class="row clearfix">
                                <div class="label">Пълнители и биологично активни добавки</div>
                                <div class="scale">
<span class="man" style="width: 100%">
</span>
                                    <span class="woman" style="width: 97%;">
</span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Само пълнители
                                </div>
                                <div class="scale">
<span class="man" style="width: 94%;">
</span>
                                    <span class="woman" style="width: 55%;">
</span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Само биологично активни добавки
                                </div>
                                <div class="scale">
<span class="man" style="width: 83%;">
</span>
                                    <span class="woman" style="width: 36%;">
</span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Операция
                                </div>
                                <div class="scale">
<span class="man" style="width: 40%;">
</span>
                                    <span class="woman" style="width: 36%;">
</span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Народни средства
                                </div>
                                <div class="scale">
<span class="man" style="width: 47%;">
</span>
                                    <span class="woman" style="width: 36%;">
</span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Здравословен начин на живот
                                </div>
                                <div class="scale">
<span class="man" style="width: 32%;">
</span>
                                    <span class="woman" style="width: 18%;">
</span>
                                </div>
                            </div>
                        </div>
                        <div class="info">
                            Уролозите признават, че 70% от мъжете предпочитат да уголемят мъжкия си орган, като използват фармацевтични лекарства. Статистиката обаче показва, че техният ефект изчезва, след като се прекрати приема им.
                        </div>
                    </div>
                </div>
                <div class="block-title">
                    <div class="cell">
<span class="light ttu">БОЖЕСТВЕН РАЗМЕР
              </span>
                        <em> Ефектът на Titan Gel е бърз и перфектен.
                        </em>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block4 japan">
            <div class="limit clearfix">
                <div class="side light-side">
                    <ol>
                        <li>  Натурални съставки без странични ефекти  </li>
                        <li>  Не води до пристрастяване, следователно не изпитвате нужда  <br/>  да увеличавате дозировката  </li>
                        <li>  Може да го спрете по всяко време -   контролирайте дължината си свободно  </li>
                    </ol>
                    <div class="prompt light ttu">
<span class="cell">
                ПЕНИСЪТ СЕ УГОЛЕМЯВА, НЕЗАВИСИМО ОТ ЗДРАВОСЛОВНОТО ВИ СЪСТОЯНИЕ И НАЧИН НА ЖИВОТ
              </span>
                    </div>
                    <div class="info">
                        Уголемяването на пениса с Titan Gel е просто и ефективно!
                    </div>
                </div>
                <div class="side dark-side">
                    <ol>
                        <li>  Операцията е радикален, спорен и много рискован метод, който може да се отрази отрицателно на сексуалните функции на вашето тяло.  </li>
                        <li>  Помпите и удължителите не са се доказали като ефективни, правят пениса ви по-голям, само когато не е еректирал  </li>
                        <li>  После ще трябва да търсите други по-безопасни методи заради здравословни проблеми.  </li>
                    </ol>
                    <div class="clearfix ">
                        <div class="prompt light ttu">
<span class="cell">
                 ОПЕРАЦИИТЕ, ПОМПИТЕ, ЛЕКАРСТВАТА ЗА УГОЛЕМЯВАНЕ ИМАТ СЕРИОЗНИ СТРАНИЧНИ ЕФЕКТИ
                </span>
                        </div>
                    </div>
                    <div class="info">
                        Дори най-ефикасните хапчета нямат постоянен ефект, прекомерната употреба и последващото пристрастяване може да доведат до проблеми с потентността.
                    </div>
                </div>
            </div>
        </div>
        <div class="block block5 japan">
            <div class="limit clearfix">
                <div class="side l-side">
                    <div class="cell">
                        <p class="title1 light ttu">  УГОЛЕМЯВАНЕ НА ПЕНИСА

                        </p>
                        <em>Само една тубичка е достатъчна, за да постигнете значителни и устойчиви резултати: до 4-6 см
                        </em>
                        <div class="image">
<span class="front label">
<span class="cell">
<text class="js_new_price">80
  </text> <span class="js_curs">Лева</span>
</span>
</span>
                            <span class="back label">
<span class="cell">50%
                  </span>
</span>
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/thor2.png"/>
                            <em class="name">Гел за подсилване на мъжествеността Titan Gel
                            </em>
                        </div>
                    </div>
                </div>
                <div class="side r-side">
                    <div class="cell">
                        <p class="title1 ttu">СВЕТОВНО ПРИЗНАТИ СЕКСОЛОЗИ ЗА TITAN GEL:
                        </p>
                        <div class="description">
                            <p>
                                Гелът за уголемяване на пениса Titan Gel, който се е доказал като ефективно средство за увеличаване дължината и дебелината на пениса сред хора от различни възрастови групи, е много органичен продукт. Доставяйки всички необходими витамини и микроелементи, той активира естественото производство на мъжкия хормон, наречен тестостерон и допринася за по-добра циркулация на кръвта в гениталиите.  <br/>  Titan Gel работи лесно и бързо и ефектът ще трае вечно.
                            </p>
                        </div>
                        <a class="btn btn-transparent btn-with-arrow pre_toform" data-scroll="true" data-scroll-to="#request-form" href="#">поръчай сега</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block6">
            <div class="limit">
                <div class="before-after">
                    <div class="side before">
                        <div class="block6-line">
                        </div>
                        <span class="label label-l">
               преди
              </span>
                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block6-1.png"/>
                        <div class="clearfix">
                        </div>
                        <div class="lowest_description_block">
                            <p>размер на пениса
                                <span class="lowest_description">15 - 17 сантиметра
                  </span>
                            </p>
                            <p>либидо
                                <span class="lowest_description">не или слабо
                  </span>
                            </p>
                            <p>оргазъм
                                <span class="lowest_description"> не или слаб
                  </span>
                            </p>
                        </div>
                    </div>
                    <div class="side after">
                        <div class="block6-line block6-line-r">
                        </div>
                        <span class="label">
               след

              </span>
                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block6-2.png"/>
                        <div class="lowest_description_block lowest_description_block-r">
                            <p>размер на пениса
                                <span class="lowest_description">19 - 23 сантиметра
                  </span>
                            </p>
                            <p>либидо
                                <span class="lowest_description">два или три пъти по-силно либидо
                  </span>
                            </p>
                            <p>оргазъм
                                <span class="lowest_description">дълготраен мощен оргазъм
                  </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="block block7">
            <div class="limit">
                <p class="title ttu light">ОСНОВНИТЕ ПРИЧИНИ ЗА МАЛКИЯ РАЗМЕР:           </p>
                <div class="content">
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block7-1.png"/>
                        </div>
                        <span class="label ttu">ЛИПСА НА ХОРМОНИ
              </span>
                        <p>  Понякога действието на основния за мъжете хормон тестостерон, е намалено. Освен това функционирането на сърдечно-съдовата система се влошава с възрастта, което се отразява на ерекцията и размера.  </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block7-2.png"/>
                        </div>
                        <span class="label ttu">СТРЕС
              </span>
                        <p>
                            Животът ни в модерното общество е изпълнен със стрес, който влияе неблагоприятно върху цялото ни тяло.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block7-3.png"/>
                        </div>
                        <span class="label ttu">НЕЗДРАВОСЛОВНО ХРАНЕНЕ
              </span>
                        <p>
                            Неправилният начин на хранене, който включва много мазнини, консерванти и често канцерогенни вещества, може да доведе до влошено здраве, проблеми със сърцето и кръвоносните съдове и наднормено тегло, както и директно да се отрази на плодовитостта, което на свой ред води до отпуснат, малък пенис.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block7-4.png"/>
                        </div>
                        <span class="label ttu">БОЛЕСТ
              </span>
                        <p>
                            Болести, които са се появили преди или по време на пубертета, имат значително влияние над дължината на пениса. Някои от болестите директно засягат репродуктивните органи и нарушават физическите процеси, необходими за удължаването на пениса.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block8">
            <div class="limit">
                <h3 class="ttu clearfix">
                    <span class="label1">  ГЕЛ  <span>  TITAN GEL  </span></span>
                    <span class="label2" style="padding-left: 20px;">  ТАЙНИ СЪСТАВКИ, КОИТО ЩЕ СТИМУЛИРАТ РАСТЕЖА НА ВАШИЯ ПЕНИС
            <em>  Забележими резултати в рамките на 3-5 седмици!  </em>
</span>
                </h3>
                <div class="content line1 clearfix">
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block8-1.png"/>
                        </div>
                        <div class="label">
                            <span>  ОСИГУРЯВА ВСИЧКО НЕОБХОДИМО ЗА УГОЛЕМЯВАНЕ НА ПЕНИСА  </span>
                        </div>
                        <p>  Titan е богат на елементи, които действат директно на либидото и производството на сперма и засилват действието на мъжките полови хормони. Може да засили и подобри сърдечно-съдовата система.  </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block8-2.png"/>
                        </div>
                        <div class="label">
                            <span>  НОРМАЛИЗИРА КРЪВНОТО НАЛЯГАНЕ И КРЪВООБРАЩЕНИЕТО  </span>
                        </div>
                        <p>  Активните компоненти проникват в тъканите на пениса, подобрявайки циркулирането на кръвта в гениталиите и предизвиквайки естествен растеж чрез увеличаване количеството кислород в кръвта и разширяване на вените на пениса.  </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block8-3.png"/>
                        </div>
                        <div class="label">
                            <span>  ПОВИШАВА ЛИБИДОТО  </span>
                        </div>
                        <p>  Titan Gel съдържа съставките, които подпомагат процеса на регенериране на клетките. Пенисът става по-чувствителен, става твърд много бързо и става по-голям по време на ерекция.  </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block8-4.png"/>
                        </div>
                        <div class="label">
                            <span>  ЗАДЕЙСТВА ПРОИЗВОДСТВОТО НА МЪЖКИ ХОРМОНИ  </span>
                        </div>
                        <p>  Тестостеронът не само прави ерекцията по-дълга и силна, но и ви прави по-привлекателни в очите на жените, тъй като той определя физическите ви харакетристики.  </p>
                    </div>
                </div>
                <div class="content line2">
                    <h4 class="block-title">
<span class="title" style="font-size: 24px;">  ТАЙНАТА НА БЕЗПРЕЦЕДЕНТНИЯ ЕФЕКТ  <span>  НА TITAN GEL  </span>
</span>
                        <span class="description">  Уникалните компоненти гарантират удивителните резултати на Titan Gel:  </span>
                    </h4>
                    <div class="squad1 clearfix">
                        <div class="clearfix">
                            <div class="item">
                                <div>
                                    <div class="image">
                                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block8-squad1-1.jpg"/>
                                    </div>
                                    <div class="description with-label _first">
                                        <span>  Екстракт от епимедиум  </span>
                                        <p>  Това растение има способността естествено да стимулира сексуалното желание и да подобрява сексуалната активност.  </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <div class="image">
                                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block8-squad1-2.jpg"/>
                                    </div>
                                    <div class="description with-label _second">
                                        <span>  Екстракт от благословен трън  </span>
                                        <p>  Бодливият подарък на природата спомага за производството на мъжкия хормон тестостерон в тялото. Тъй като този хормон влияе на мъжкото тяло по много начини, екстрактът от тръна спомага за по-дълга, по-твърда и по-продължителна ерекция.  </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="item">
                                <div>
                                    <div class="image">
                                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block8-squad1-3.jpg"/>
                                    </div>
                                    <div class="description with-label _third">
                                        <span>  Екстракт от мака (Lepidium meyenii)  </span>
                                        <p>  От много векове това растение е широко използвано за повишаване на мъжката сила. Нашите предци за знаели за него и растението не е загубило влиянието си и до днес.  </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <div class="image">
                                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block8-squad1-4.jpg"/>
                                    </div>
                                    <div class="description with-label _fourth">
                                        <span>  Еластин  </span>
                                        <p>  Това е единственият протеин, който в действителност може да лекува тъканите. Още повече, че той има невероятната способност да повишава чувствителността на пениса и да стимулира растежа му. Ефектът се постига с помощта на активните съставки, включително аминокиселини.  </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block9">
            <div class="limit">
                <p class="title ttu">  УГОЛЕМЯВАНЕ НА ВАШИЯ ПЕНИС С TITAN GEL!
                    <span>  РЕЗУЛТАТЪТ Е НЕВЕРОЯТЕН!  </span>
                </p>
                <p class="description">  Преди и след: значително уголемяване на пениса, по-мощна ерекция и удължаване продължителността на половия акт  </p>
                <div class="content">
                    <div class="compare clearfix">
                        <div class="side before">
                            <div class="image">
                                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block9-before.jpg"/>
                            </div>
                            <span class="tooltip tooltip-volume">  Обиколка: 11 сантиметра  <i></i></span>
                            <span class="tooltip tooltip-height">  Дължина 15 сантиметра  <i></i></span>
                            <div class="duration">
                                <span>  ПРОДЪЛЖИТЕЛНОСТ НА ЕРЕКЦИЯТА:  </span>
                                10 до 15 минути
                            </div>
                        </div>
                        <div class="side after">
                            <div class="image">
                                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block9-after.jpg"/>
                            </div>
                            <span class="tooltip tooltip-volume">  Дължина: 20 сантиметра  <i></i></span>
                            <span class="tooltip tooltip-height">  Обиколка: 13 сантиметра  <i></i></span>
                            <div class="duration dur-rs">
                                <span>  ПРОДЪЛЖИТЕЛНОСТ НА ЕРЕКЦИЯТА:  </span>
                                45-65 минути
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block10 japan">
            <div class="limit clearfix">
                <div class="side l-side">
                    <h3 class="ttu">  ДРУГИ ПРОДУКТИ ЗА УДЪЛЖАВАНЕ  </h3>
                    <p class="description">  Защо другите методи на удължаване не могат да дадат такива резултати?  </p>
                    <ul>
                        <li>  Временни резултати, които ще изчезнат, след като приключите курса  </li>
                        <li>  Прекалено рисковани, може да причинят сексуални разстройства  </li>
                        <li>  Влияят неблагоприятно върху сърцето и кръвоносните съдове  </li>
                        <li>  Възможен риск от предозиране  </li>
                    </ul>
                </div>
                <div class="side r-side">
                    <h3 class="ttu ttu-r">  БЕЗПРЕЦЕДЕНТНИЯТ ПРОДУКТ TITAN GEL  </h3>
                    <p class="description description-r">  Предимства на Titan Gel:  </p>
                    <ul>
                        <li>  Резултатите са завинаги  </li>
                        <li>  Няма симптоми на отравяне, без противопоказания, без странични ефекти  </li>
                        <li>  Прави пениса ви с до 6-8 см по-дълъг, подобрява състоянието на кръвоносните съдове  </li>
                        <li>  Няма вероятност за предозиране  </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="block block11">
            <div class="limit clearfix">
                <div class="content">
                    <p class="title1 ttu">  ГЕЛЪТ  <br/>  Titan Gel  </p>
                    <p class="title2 ttu">  ГО КАРА ДА РАСТЕ НАИСТИНА БЪРЗО!  </p>
                    <p class="title3">  Прост и ефективен метод за удължаване на пениса  </p>
                    <p class="title4">  Гигантският ти хуй ще задоволи всяка жена!  </p>
                    <div class="brands">
                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block11-brands.png"/>
                    </div>
                    <a class="btn btn-primary pre_toform" data-scroll="true" data-scroll-to="#request-form" href="#">  ПОРЪЧАЙ СЕГА  </a>
                </div>
            </div>
        </div>
        <div class="block block12 japan">
            <div class="limit clearfix">
                <div class="title">
                    <div>
                        <div class="cell label ttu">
                            <span>  TITAN GEL  </span>
                            ЩЕ ТЕ НАПРАВИ АЛФА МЪЖ ЗА НУЛА ВРЕМЕ!
                        </div>
                        <div class="cell description">  Прост е ефективен начин за удължаване на пениса  </div>
                        <div class="cell before step1">
                            <div class="before">
                                <span>  преди  </span>
                                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block12-before1.jpg"/>
                            </div>
                            <div class="after">
                                <span>  след  </span>
                                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block12-after1.jpg"/>
                            </div>
                        </div>
                        <div class="cell before step2">
                            <div class="before">
                                <span>  преди  </span>
                                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block12-before2.jpg"/>
                            </div>
                            <div class="after">
                                <span>  след  </span>
                                <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block12-after2.jpg"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content clearfix">
                    <div class="side l-side">
                        <div class="row clearfix">
                            <span class="label">  ИМЕ:  </span>
                            <span class="value">  Titan Gel  </span>
                        </div>
                        <div class="row clearfix">
                            <span class="label">  ОПИСАНИЕ:  </span>
                            <span class="value">  опаковка с гел  </span>
                        </div>
                        <div class="row clearfix">
                            <span class="label">  КОМПОНЕНТ:  </span>
                            <span class="value">  натурални растителни екстракти  </span>
                        </div>
                        <div class="row clearfix">
                            <span class="label">  ИНСТРУКЦИИ:  </span>
                            <span class="value">  Нанасяйте гела върху пениса си два пъти на ден в продължение на 1 месец. Масажирайте пениса с гела, за да си осигурите по-бързи резултати.  </span>
                        </div>
                    </div>
                    <div class="side r-side">
                        <div class="row clearfix">
                            <span class="label">  ВНИМАНИЕ:  </span>
                            <span class="value">  Тъй като съставът на продукта е базиран на мощни натурални съставки, той позволява на мъже от всякакви възрасти да уголемят сексуалния си орган. Titan Gel се препоръчва от най-добрите световни уролози. Той също така ефективно подобрява еректилната функция, спомага за повишаването на нивата на тестостерон в тялото, удължава продължителността на половия акт и оргазма. Продуктът има благоприятен ефект върху сърдечно-съдовата система и никога не води до пристрастяване.  </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block13 japan">
            <div class="limit clearfix">
                <div class="title ttu">  ИСТИНСКИ ИСТОРИИ  </div>
                <div class="content">
                    <div class="item">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block13-1-a.jpg"/>
                        </div>
                        <p class="label">  Перфектен съм в спалнята. Резултатът е чудесен!  </p>
                        <p class="name">  ИВАН, 43 ГОДИНИ  </p>
                        <p class="story">  Винаги съм работил упорито, за да осигуря по-добър живот за семейството си. За жалост просто не съм толкова голям, за да задоволя жена си. Правим секс само през уикенда и е много скучен, тя се оплаква, че не чувства нищо и никога не достига до оргазъм. Веднъж един приятел ме посъветва да опитам Titan Gel. Направо съм шокиран от резултата. Само след пет дни започнах да забелязвам, че става по-дълъг и по-дебел поне +4 см!  <br/>  Сега спя с жена ми всеки ден, понякога по цяла нощ и тя е толкова щастлива! Очите ѝ отново блестят и усещам, че е е по-активна и изглежда по-млада. Животът ми определено стана по-добър!  </p>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block13-2-a.jpg"/>
                        </div>
                        <p class="label">  Методът, който спаси брака ми  </p>
                        <p class="name">  АНИ, 35 ГОДИНИ  </p>
                        <p class="story">  Винаги се опитвам да се поддържам във форма. Забелязах обаче, че мъжът ми губи интерес към мен. Обмислих проблема от различни гледни точки и осъзнах, че това беше поради факта, че имаше проблем с либидото му. По съвет на моя приятелка му купих Titan Gel. Това, което не подозирах, е че той не просто ще реши проблема с либидото, но и ще го направи МНОГО ПО-ГОЛЯМ! Много ми харесва, сексът ни е страхотен. Това е едно свежо начало в нашия брачен живот. Всъщност преди той искаше да се разведе с мен, защото си мислеше, че заслужавам някой по-добър.  </p>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/block13-3-a.jpg"/>
                        </div>
                        <p class="label">  Перфектният резултат във всяка ситуация  </p>
                        <p class="name">  БИСЕР, 28 ГОДИНИ  </p>
                        <p class="story">  Тъй като съм културист, прекарвах много време във фитнеса и водех здравословен начин на живот. Мислех си, че никога няма да имам проблеми в леглото. Оказа се, че греша, много момичета не искаха да започнат връзка с мен, изчезваха след първата ни нощ заедно. Те никога не го казваха директно, но проблемът беше малкият ми (12 см) пенис. Веднъж срещнах момиче и този път почувствах, че е специална, затова ѝ казах, че съм много сериозен по отношение на връзката ни и тя отвърна със същото. Бях много разтревожен, когато правихме секс за първи път. За щастие тя се оказа достатъчно умна да не ме съди. Тя не ми се смя, а вместо това ми каза за Titan Gel. Сега съм с +5 см и мога да я задоволявам по три-четири пъти всяка нощ!  </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block14">
            <div class="limit">
                <div class="row clearfix">
                    <div class="content1">
                        <p class="title1 ttu light">  ПРОДУКТ ЗА УДЪЛЖАВАНЕ НА ПЕНИСА  </p>
                        <p class="title2 ttu light">  гелът   <strong>  TITAN GEL  </strong>   Е НАЙ-ДОБРИЯТ!  </p>
                        <p class="description">  Всичко, което ви трябва, за да накарате жените да полудеят по вас  </p>
                    </div>
                    <div class="image">
<span class="front label">
<span class="cell">
<text class="js_new_price">80 </text> <span class="js_curs">Лева</span>
</span>
</span>
                        <span class="back label">
<span class="cell"> 50% </span>
</span>
                        <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/thor4.png"/>
                    </div>
                </div>
                <div class="content2">
                    <p class="title1">  96% ОТ МЪЖЕТЕ СА ПОТВЪРДИЛИ ЕФЕКТИВНОСТТА НА TITAN GEL  </p>
                    <img alt="" src="//st.acstnst.com/content/Titan_gel_BG_Dark/img/man.png"/>
                    <div class="story">
                        <div class="cell">
                            <p class="title2">  ЕКСПЕРТНО МНЕНИЕ  </p>
                            Много мъже страдат от неудовлетворителен размер на пениса. Може да има различни причини, включително възраст, чест стрес, нездравословно или недостатъчно хранене, липса на почивка, липса на хормони, злоупотреба с алкохол и никотин и други. Всички те водят до един и същи резултат: спад в качеството на сексуалния живот. През последните 20 години съм виждал мъже на различни възрасти и начини на живот с този проблем. Успяхме да намерим перфектното средство, което да им помогне. А именно Titan Gel! По време на клиничните проучвания той се доказа като ефективен дори и в най-тежките случаи. Определено мога да препоръчам Titan Gel на всичките си пациенти, като най-доброто средство. Онези, които са го опитали, го оценяват!
                            <span class="gray"><i></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block15 japan">
            <div class="limit clearfix">
                <div class="content">
                    <div class="title1 ttu">
                        ПОРЪЧАЙ СЕГА
                    </div>
                    <div class="title2 ttu">
                        TITAN GEL
                    </div>
                    <form action="" class="form" id="order_form" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIAKHEwAAAAAAAAAAAARuJTqiAA"></iframe>

                        <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="80.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAKHEwQEnSCJAAEAAQACrhcBAAKnHAIGAQEABA1D7akA">
                        <input type="hidden" name="goods_id" value="49">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Titan_gel_BG_Dark">
                        <input type="hidden" name="price" value="80">
                        <input type="hidden" name="old_price" value="160">
                        <input type="hidden" name="al" value="7335">
                        <input type="hidden" name="total_price_wo_shipping" value="80.0">
                        <input type="hidden" name="currency" value="Лева">
                        <input type="hidden" name="package_id" value="1">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="BG">
                        <input type="hidden" name="shipment_vat" value="0.2">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 320, 'old_price': 640, 'shipment_price': 0}}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.2">
                        <!-- -->
                       <!-- <div class="form-group">
                            <select class="country" id="country_code_selector" name="country">
                                <option selected="selected" value="BG">България</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input name="name" placeholder="Име и фамилия" type="text" value=""/>
                        </div>
                        <div class="form-group">
                            <input class="only_number" name="phone" placeholder="Телефон" type="text" value=""/>
                        </div>

                        <div class="form-group">
                            <button class="ttu btn btn-primary btn-block js_pre_toform" type="submit">поръчай</button>
                            <div class="toform"></div>
                        </div>
                        <!---->
                        <div class="price">
                            <s><span class="js_old_price">160</span> <span class="js_curs">Лева</span></s>
                            <span>Цена <span class="js_new_price">80</span> <span class="js_curs">Лева</span></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <a class="btn btn-fixed pre_toform" data-scroll="true" data-scroll-to="#request-form" href="#"><span>поръчай сега!</span></a>
    </div>
    <!-- -->
    <style>
        .h-w-shipment{
            display:none;
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link data-id="css" href="//st.acstnst.com/content/second/Titan_Gel_BG/css/beauty_1.css" rel="stylesheet"/>
    <div class="hidden-window v1_1_1_1">
        <div class="h-w-header">
            <div class="h-w-contant">
                <div class="h-w-header-text">
                    <span><b>Titan Gel</b> - поръчайте още сега!</span>
                </div>
            </div>
        </div>
        <div class="h-w-main">
            <div class="h-w-contant">
                <div class="h-w-left">
                    <div class="h-w-payment h-w-decor-block h-w-vis-mob">
                        <p>Плащане <span>ПРИ ДОСТАВКА</span></p>
                        <ul>
                            <li><span>ПОРЪЧКАТА</span></li>
                            <li><span>ДОСТАВКА</span></li>
                            <li><span>ДОСТАВКА И ПЛАЩАНЕ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob h-w-ico h-w-select">
                        <select class="change-package-selector" name="count_select">
                            <option data-slide-index="0" value="1">1 опаковка</option>
                            <option data-slide-index="1" selected="selected" value="3">2+1 опаковки </option>
                            <option data-slide-index="2" value="5">3+2 опаковки </option>
                        </select>
                    </div>
                    <ul class="bx-bx">
                        <li data-value="1">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class=" h-w-item-prod"></div>
                                    </div>
                                    <div class="h-w-item-description">получавате 100 % удължаване на половият акт, както и повишена чувствителност на главичката на пениса</div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковка</p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>160 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Общо:</span><span>80 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има 50% ОТСТЪПКА
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="active" data-value="3">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"></div>
                                        <span><font>+ подарък</font></span>
                                    </div>
                                    <div class="h-w-item-description">този курс Ви помага да увеличите размера с 1-1,5 см, както и да удължите половият акт на 10-15 мин</div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковки <span>подарък</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>320 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Общо:</span><span>160 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има 50% ОТСТЪПКА
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-value="5">
                            <div class="h-w-block">
                                <div class="h-w-check js_changer h-w-ico"></div>
                                <div class="h-w-item">
                                    <div class="h-w-item-img">
                                        <div class="h-w-item-prod"><span></span></div>
                                        <span><font>+ подарък</font></span>
                                    </div>
                                    <div class="h-w-item-description">курс, с помощта на който членът се увеличава стабилно с 4-5 см. При 85 %, от поръчалите го, се увеличава броят на партньорите, половият акт е ярък, регулярен и продължителен.</div>
                                </div>
                                <div class="h-w-price">
                                    <p>опаковки <span>подарък</span></p>
                                    <ul>
                                        <li class="h-w-old-price"><span>Стара цена:</span><span>640 <font>Лева</font></span></li>
                                        <!--  -->
                                        <li class="h-w-total"><span>Общо:</span><span>320 <font>Лева</font></span></li>
                                        <!--  -->
                                    </ul>
                                    <div class="h-w-discount">
                                        <b>Внимание:</b> Има 50% ОТСТЪПКА
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="h-w-right">
                    <div class="h-w-payment h-w-decor-block h-w-hide-mob">
                        <p>Плащане <span>ПРИ ДОСТАВКА</span></p>
                        <ul>
                            <li><span>ПОРЪЧКАТА</span></li>
                            <li><span>ДОСТАВКА</span></li>
                            <li><span>ДОСТАВКА И ПЛАЩАНЕ</span></li>
                        </ul>
                    </div>
                    <div class="h-w-honesty h-w-ico h-w-hide-mob">ДОБРА СДЕЛКА</div>
                    <div class="h-w-form h-w-decor-block js_scrollForm">
                        <p>ВЪВЕДЕТЕ ДАННИ ЗА ПОРЪЧКА</p>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAKHEwAAAAAAAAAAAARuJTqiAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="80.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAKHEwQEnSCJAAEAAQACrhcBAAKnHAIGAQEABA1D7akA">
                            <input type="hidden" name="goods_id" value="49">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Titan_gel_BG_Dark">
                            <input type="hidden" name="price" value="80">
                            <input type="hidden" name="old_price" value="160">
                            <input type="hidden" name="al" value="7335">
                            <input type="hidden" name="total_price_wo_shipping" value="80.0">
                            <input type="hidden" name="currency" value="Лева">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="BG">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 320, 'old_price': 640, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.2">

                            <div class="h-w-select h-w-ico" style="display:none;">
                                <select id="country_code_selector" name="country_code">
                                    <option value="BG">България</option>
                                </select>
                            </div>
                            <div class="h-w-select h-w-ico">
                                <select class="change-package-selector" name="count_select">
                                    <option data-slide-index="0" value="1">1 опаковка</option>
                                    <option data-slide-index="1" selected="selected" value="3">2+1 опаковки </option>
                                    <option data-slide-index="2" value="5">3+2 опаковки </option>
                                </select>
                            </div>
                            <div style="display:none;">
                                <input name="name" type="text"/>
                                <input class="only_number" name="phone" type="text"/>
                            </div>
                            <input name="address" placeholder="Add meg a címed" type="text"/>
                            <!--  -->
                            <div class="h-w-text-total">Общо: <span><font class="js_full_price">160</font> Лева</span></div>
                            <!--<button class="js_submit">Поръчай</button>-->
                        </form>
                    </div>
                    <div class="h-w-guarantee h-w-decor-block">
                        <p>НИЕ ГАРАНТИРАНЕ:</p>
                        <ul>
                            <li><b>100%</b> качество</li>
                            <li><b>Проверка</b> на продукта при получаване</li>
                            <li><b>Безопасност</b> на вашите данни</li>
                        </ul>
                    </div>
                    <div class="h-w-vis-mob"><div class="h-w-honesty h-w-ico">ДОБРА СДЕЛКА</div></div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    </body>
    </html>
<?php } ?>