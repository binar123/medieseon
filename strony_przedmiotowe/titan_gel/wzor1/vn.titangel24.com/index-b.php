<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html lang="en">
    <head>

        <!-- [pre]land_id = 977 -->
        <script>var locale = "vn";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIcAwTUPBeJAAEAAQACvgIBAALRAwGXAjoCBPzf_0oA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 67500;
            var name_hint = 'Phạm Xuân';
            var phone_hint = '+84917989737';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("vn");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">



        <!-- START Exoclick Goal Tag | VN-adult-L -->
        <img width="1" height="1" src="http://main.exoclick.com/tag.php?goal=67abf0d373fc4749eaf77e4db1184ac8" style="display:none">
        <!-- END Exoclick Goal Tag | VN-adult-L -->

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="http://st.acstnst.com/content/Titan_Gel_VN_2/mobile/favicon.ico">
        <title>TITAN GEL</title>

        <!-- Bootstrap -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
        <link href="//st.acstnst.com/content/Titan_Gel_VN_2/mobile/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//st.acstnst.com/content/Titan_Gel_VN_2/mobile/css/owl.carousel.css" type="text/css">
        <link href="//st.acstnst.com/content/Titan_Gel_VN_2/mobile/css/landing.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <div class="wrapper">
        <div class="header">
            <div class="slogan">
                <h1>KÉO DÀI KÍCH CỠ DƯƠNG VẬT</h1>
                <span>KHÔNG RIÊNG MÌNH BẠN CẢM NHẬN RÕ HIỆU QUẢ!</span>
            </div>
            <div class="ok">
                <span>CHỈ TRONG VÒNG MỘT THÁNG</span>
                <span>TĂNG KÍCH CỠ THÊM 5 cm</span>
                <span>ÁP DỤNG 1 LẦN/NGÀY</span>
                <span>TĂNG KÍCH CỠ MỘT CÁCH TỰ NHIÊN!</span>
                <span>100% THÀNH PHẦN THIÊN NHIÊN</span>
            </div>
            <div class="header-image"></div>
            <div class="header-discount"><s>1500000 ₫</s><br><span style="font-size: 20px;">OFERTA</span></div>
            <a href="#" class="order">ĐẶT HÀNG NGAY</a>
        </div>
    </div>
    <div class="block1-wrapper">
        <div class="block1">
            <h1>GEL TITAN - KẾT QUẢ THẤY RÕ CHỈ TRONG VÒNG</h1>
            <h2>4 TUẦN! NẾU BẠN MUỐN:</h2>
            <span class="point"><i>1</i>Đạt KHOÁI CẢM TỘT ĐỘ khi quan hệ!</span>
            <span class="point"><i>2</i>Tăng kích cỡ dương vật nhanh chóng và an toàn!</span>
            <span class="point"><i>3</i>Duy trì thời gian quan hệ TRONG NHIỀU LẦN!</span>
            <span class="point"><i>4</i>Sẵn sàng xung trận BẤT KỲ LÚC NÀO!</span>
        </div>
    </div>
    <div class="block2-wrapper">
        <div class="block2">
            <h1>
                Tăng KÍCH CỠ DƯƠNG
                <br>
                VẬT MÃI MÃI!
            </h1>
            <h2>GEL TITAN - KẾT QUẢ THẤY RÕ CHỈ TRONG VÒNG 4 TUẦN!</h2>
            <span class="point">
			<i>1</i> <span class="red">Tuần thứ 1 và thứ 2:</span>
			<br>
Dương vật trở lên dài hơn và cương cứng hơn, cảm nhận hưng phấn tăng lên gấp đôi, sự biến chuyển có thể thấy rõ khi chiều dài dương vật tăng lên thêm 1.5 cm
			</span>
            <span class="point">
			<i>2</i>
			<span class="red">Tuần thứ 2 và thứ 3:</span>
			<br>
Đáng chú ý là đường kính dương vật KHỦNG HƠN, hình dáng dương vật hình thành một cách hoàn chỉnh. Thời gian quan hệ tăng đến 70%!
			</span>
            <span class="point">
			<i>3</i>
			<span class="red">Tuần thứ 4 và sau đó:</span>
			<br>
Kích cỡ dương vật dài hơn đến 5 cm! Chất lượng các cuộc giao ban tăng lên, lên đến nhiều lần. Cảm giác cực khoái đến nhanh hơn và kéo dài tận 5-7 phút!
			</span>
        </div>
    </div>
    <div class="footer-wrapper">
        <div class="footer">
            <h1>CHỈ CÒN 50 SẢN PHẨM!</h1>
            <form action="" method="POST">
                 <span>
					ƯU ĐÃI CÓ
					<br />
					 HẠN!
					</span>
                <iframe scrolling="no"
                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                        src="http://abcdg.pro/forms/?target=-4AAIJIAIcAwAAAAAAAAAAAAQbnnMpAA"></iframe>

                <!--
                <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                <input type="hidden" name="total_price" value="817500.0">
                <input type="hidden" name="esub" value="-4A25sMQIJIAIcAwTUPBeJAAEAAQACvgIBAALRAwGXAjoCBPzf_0oA">
                <input type="hidden" name="goods_id" value="2">
                <input type="hidden" name="title" value="Titan gel - VN">
                <input type="hidden" name="ip_city" value="Toruń">
                <input type="hidden" name="template_name" value="Titan_Gel_VN_2">
                <input type="hidden" name="price" value="750000">
                <input type="hidden" name="old_price" value="1500000">
                <input type="hidden" name="al" value="977">
                <input type="hidden" name="total_price_wo_shipping" value="750000.0">
                <input type="hidden" name="currency" value="₫">
                <input type="hidden" name="package_id" value="0">
                <input type="hidden" name="protected" value="None">
                <input type="hidden" name="ip_country_name" value="Poland">
                <input type="hidden" name="country_code" value="VN">
                <input type="hidden" name="shipment_vat" value="0.0">
                <input type="hidden" name="ip_country" value="PL">
                <input type="hidden" name="package_prices" value="{}">
                <input type="hidden" name="shipment_price" value="67500">
                <input type="hidden" name="price_vat" value="0.0">
                <input name="name" value='' placeholder="Tên bạn"/>
                <input name="phone" class="only_number" value='' placeholder="Số điện thoại"/>
                <span class="shipment-price">Giá cũ: <s>1500000 ₫</s></span>
                <span class="total-price">GIÁ MỚI!: 817500 ₫</span>
                <a class="order js_submit">ĐẶT HÀNG NGAY</a>
                <div class="oferta">
                    <br />
                    <!--<b>ƯU ĐÃI CÓ HẠN!</b>
                    <span>ANTES DEL FINAL DE LA IZQUIERDA 1000</span>-->

               <!-- </div>
                <div class="product">
                    <img src="http://st.acstnst.com/content/Titan_Gel_VN_2/mobile/img/product.png" width="250" alt="" />
                </div>-->
            </form>
        </div>
    </div>

    <script src="//st.acstnst.com/content/Titan_Gel_VN_2/mobile/js/functions.js"></script>
    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <!--[if lt IE 8]><html class="ltie8"><![endif]-->
    <head>

        <!-- [pre]land_id = 977 -->
        <script>var locale = "vn";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIcAwTUPBeJAAEAAQACvgIBAALRAwGXAjoCBPzf_0oA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 67500;
            var name_hint = 'Phạm Xuân';
            var phone_hint = '+84917989737';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("vn");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">



        <!-- START Exoclick Goal Tag | VN-adult-L -->
        <img width="1" height="1" src="http://main.exoclick.com/tag.php?goal=67abf0d373fc4749eaf77e4db1184ac8" style="display:none">
        <!-- END Exoclick Goal Tag | VN-adult-L -->

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Gel Titan - Sản phẩm hàng đầu cho việc gia tăng kích cỡ dương vật</title>
        <link rel="shortcut icon" type="image/x-icon" href="http://st.acstnst.com/content/Titan_Gel_VN_2/favicon.ico"/>
        <link href="//st.acstnst.com/content/Titan_Gel_VN_2/css/global.css" rel="stylesheet" type="text/css" />
        <link href="//st.acstnst.com/content/Titan_Gel_VN_2/css/style.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]><link href="//st.acstnst.com/content/Titan_Gel_VN_2/css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

        <script type="text/javascript" src="//st.acstnst.com/content/Titan_Gel_VN_2/js/jquery.countdown.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/Titan_Gel_VN_2/js/jquery.defaultValue.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/Titan_Gel_VN_2/js/jquery.overflowed.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/Titan_Gel_VN_2/js/main.js"></script>
    </head>

    <body>

    <div id="body">

        <div class="central_part">
            <div class="top_block">

                <h1>KÉO DÀI KÍCH CỠ DƯƠNG VẬT</h1>

                <div class="top-block-title">
                    KHÔNG RIÊNG MÌNH BẠN CẢM NHẬN RÕ HIỆU QUẢ!
                </div>

                <div class="columns_2 clearfix">
                    <div>
                        <ul>
                            <li>CHỈ TRONG VÒNG MỘT THÁNG</li>
                            <li>TĂNG KÍCH CỠ THÊM 5 cm</li>
                            <li>ÁP DỤNG 1 LẦN/NGÀY</li>
                            <li>TĂNG KÍCH CỠ MỘT CÁCH TỰ NHIÊN!</li>
                            <li>100% THÀNH PHẦN THIÊN NHIÊN</li>
                        </ul>

                        <div id="cost">
                            <div class="crossed js_old_price">1500000</div>
                            <div>
                                <span class="js_new_price">750000 </span><span><i class="js_curs">₫</i></span>
                                <!--<h5>Preț nou</h5>-->
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>

                    <div class="form_wrap">
                        <form action="" method="POST" id="order_form">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAIcAwAAAAAAAAAAAAQbnnMpAA"></iframe>

                           <!-- <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="817500.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAIcAwTUPBeJAAEAAQACvgIBAALRAwGXAjoCBPzf_0oA">
                            <input type="hidden" name="goods_id" value="2">
                            <input type="hidden" name="title" value="Titan gel - VN">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Titan_Gel_VN_2">
                            <input type="hidden" name="price" value="750000">
                            <input type="hidden" name="old_price" value="1500000">
                            <input type="hidden" name="al" value="977">
                            <input type="hidden" name="total_price_wo_shipping" value="750000.0">
                            <input type="hidden" name="currency" value="₫">
                            <input type="hidden" name="package_id" value="0">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="VN">
                            <input type="hidden" name="shipment_vat" value="0.0">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{}">
                            <input type="hidden" name="shipment_price" value="67500">
                            <input type="hidden" name="price_vat" value="0.0">
                            <span class="title_black">CHỈ CÒN 50 SẢN PHẨM</span>
                            <span class="title_white">ƯU ĐÃI CÓ HẠN!</span>
                            <div><input type="text" name="name" value="" class="defvalue" placeholder="Tên bạn"></div>
                            <div><input type="text" name="phone" value="" class="defvalue only_number" placeholder="Số điện thoại"></div>

                            <p>Giá vận chuyển: <span class="inp-delivery js_delivery">67500</span><span class="inp-currency js_curs">₫.</span></p>
                            <p>Tổng giá: <span class="inp-itogo js_full_price">817500</span><span class="inp-currency js_curs">₫.</span></p>
                            <div class="btn_wrap"><input type="button" name="btn_submit" value="ĐẶT HÀNG NGAY" class="submit js_submit" /></div>-->
                        </form>
                    </div>
                </div>

            </div>

            <h2>GEL TITAN <span>- KẾT QUẢ THẤY RÕ CHỈ TRONG VÒNG 4 TUẦN!</span></h2>
            <div class="left">
                <h3>NẾU BẠN MUỐN:</h3>
                <ol>
                    <li>Đạt KHOÁI CẢM TỘT ĐỘ khi quan hệ!</li>

                    <li>Tăng kích cỡ dương vật nhanh chóng và an toàn!</li>

                    <li>Duy trì thời gian quan hệ TRONG NHIỀU LẦN!</li>

                    <li>Sẵn sàng xung trận BẤT KỲ LÚC NÀO!</li>
                </ol>
                <div class="yellow">
                    <h3>KHI ĐẶT MUA SẢN PHẨM GEL TITAN, BẠN CHẮC CHẮN SẼ CÓ:</h3>
                    <ul class="yellow-menu">
                        <li>Kích cỡ dương vật tăng LÊN ĐẾN 5 cm!</li>
                        <li>Đường kính dương vật tăng đến 60%</li>
                        <li>Duy trì phong độ ổn định và bản lĩnh đàn ông bất kỳ thời điểm nào dù ngày hay đêm</li>
                        <li>Tăng cảm giác khoái cảm và duy trì cảm giác cực khoái lâu hơn.</li>
                        <li>Bản lĩnh đàn ông tăng gấp 3 lần (Thời gian quan hệ lên đến 4-5 giờ!)</li>
                    </ul>
                </div>
            </div>

            <h3 class="small-raz">Tăng KÍCH CỠ DƯƠNG VẬT MÃI MÃI!</h3>

            <div class="disk-block">
                <h2 style="text-align: center;">GEL TITAN <span>- KẾT QUẢ THẤY RÕ TRONG VÒNG 4 TUẦN!</span></h2>
                <img src="http://st.acstnst.com/content/Titan_Gel_VN_2/images/cream_.png" alt=""/>
                <div class="disk-content">
                    <ol>
                        <li>
                            Tuần thứ 1 và thứ 2: <br/>
                            Dương vật trở lên dài hơn và cương cứng hơn, cảm nhận hưng phấn tăng lên gấp đôi, sự biến chuyển có thể thấy rõ khi chiều dài dương vật tăng lên thêm 1,5 cm
                        </li>

                        <li>
                            Tuần thứ 2 và thứ 3 <br/>
                            Đáng chú ý là đường kính dương vật KHỦNG HƠN, hình dáng dương vật hình thành một cách hoàn chỉnh. Thời gian quan hệ tăng đến 70%!
                        </li>

                        <li>
                            Tuần thứ 4 và sau đó: <br/>
                            Kích cỡ dương vật dài hơn đến 5 cm! Chất lượng các cuộc giao ban tăng lên, lên đến nhiều lần. Cảm giác cực khoái đến nhanh hơn và kéo dài tận 5-7 phút!
                        </li>
                    </ol>
                </div>
                <div class="clear"></div>
            </div>
            <h3>MỘT THÁNG TĂNG KÍCH CỠ  – TRỌN ĐỜI TẬN HƯỞNG CẢM GIÁC THĂNG HOA!</h3>

            <h4>HIỆU QUẢ ĐƯỢC CHỨNG MINH QUA CÁC THỬ NGHIỆM LÂM SÀNG</h4>

        </div><!-- end div central_part -->

        <div id="bottom_block">
            <div id="bottom_bg"><img src="http://st.acstnst.com/content/Titan_Gel_VN_2/images/bottom_block_bg.png" alt="" /></div>
            <div class="central_part">



                <div>
                    <div class="clearfix">

                        <form action="" method="POST" id="order_form_b">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAIcAwAAAAAAAAAAAAQbnnMpAA"></iframe>

                            <!-- <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                             <input type="hidden" name="total_price" value="817500.0">
                             <input type="hidden" name="esub" value="-4A25sMQIJIAIcAwTUPBeJAAEAAQACvgIBAALRAwGXAjoCBPzf_0oA">
                             <input type="hidden" name="goods_id" value="2">
                             <input type="hidden" name="title" value="Titan gel - VN">
                             <input type="hidden" name="ip_city" value="Toruń">
                             <input type="hidden" name="template_name" value="Titan_Gel_VN_2">
                             <input type="hidden" name="price" value="750000">
                             <input type="hidden" name="old_price" value="1500000">
                             <input type="hidden" name="al" value="977">
                             <input type="hidden" name="total_price_wo_shipping" value="750000.0">
                             <input type="hidden" name="currency" value="₫">
                             <input type="hidden" name="package_id" value="0">
                             <input type="hidden" name="protected" value="None">
                             <input type="hidden" name="ip_country_name" value="Poland">
                             <input type="hidden" name="country_code" value="VN">
                             <input type="hidden" name="shipment_vat" value="0.0">
                             <input type="hidden" name="ip_country" value="PL">
                             <input type="hidden" name="package_prices" value="{}">
                             <input type="hidden" name="shipment_price" value="67500">
                             <input type="hidden" name="price_vat" value="0.0">
                             <div><input type="text" name="name" value="" class="defvalue form-control" placeholder="Tên bạn" /></div>
                             <div style="float: none;"><input type="text" name="phone" value="" class="defvalue only_number" data-check="1" placeholder="Số điện thoại"/></div>
                                <div class="btn_wrap"><input type="button" name="btn_submit" value="ĐẶT HÀNG NGAY" class="submit js_submit" /></div>-->
                        </form>
                    </div>

                    <div id="clock_block">
                        <span class="white">ƯU ĐÃI CÓ HẠN!</span>
                        <span>ƯU ĐÃI SẼ KẾT THÚC TRONG VÒNG</span>
                        <div id="timer" class="clearfix">
                            1000
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--googleoff: all-->
    <!--noindex-->
    <noscript>
        <div class="overflow_all">
            <div id="msg_enable_js">Kích hoạt JavaScript trong trình duyệt của bạn để sử dụng trang này.</div>
        </div>
    </noscript>

    <div id="ltie8_message">
        <div class="overflow_all">
            <div id="msg_update_browser">Hãy nhớ cập nhật phiên bản trình duyệt <a href="http://windows.microsoft.com/ru-RU/internet-explorer/download-ie" target="_blank">web</a> mới nhất.</div>
        </div>
    </div>

    </body>
    </html>
<?php } ?>