<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <!-- [pre]land_id = 2500 -->
        <script>var locale = "fr";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJZCATDlx6JAAEAAQAC5QcBAALECQIGAQLGBgQ268W8AA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
            var shipment_price = 6;
            var name_hint = 'Rémi Morel';
            var phone_hint = '+336314254';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("fr");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>TITAN GEL</title>
        <meta content="telephone=no" name="format-detection"/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <link href="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/css/secondPageStyles.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/css/owl.carousel.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/css/owl.theme.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/js/main.js"></script>
    </head>
    <body>
    <div class="wrhide">
        <div class="logo_top">
            <img class="logo" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/img/logo1.png"/>
        </div>
        <div class="wrapper">
            <header class="base pr_img2">
                <div class="cnt_wrapp">
                    <div class="header_sticker">
                        <div class="sticker_top_text">men's</div>
                        <div class="sticker_line_text">#1</div>
                        <div class="sticker_bottom_text">choice</div>
                    </div>
                    <img class="product_image_top" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/img/titan_cream2.png" width="170"/>
                    <div class="prices_top">
                        <span>98</span>
                        <span>49<span class="currency"> €</span></span>
                    </div>
                    <div class="button top_button toform jsOpenWindow">
                        commanda
                    </div>
                    <ul class="benefits">
                        <li class="li1"> AUGMENTE <span> la taille et l’épaisseur! </span></li>
                        <li class="li2"> 1 MILLION <span> de clients satisfaits! </span></li>
                        <li class="li3"> SANS DANGER <span> et facile à utiliser! </span></li>
                        <li class="li4"> SANS PRODUITS CHIMIQUES <span> sans pompes ou extenseurs. </span></li>
                    </ul>
                </div>
            </header>
            <!-- /header.base -->
            <div class="expert cnt_wrapp">
                <div class="top">
                    <h3 class="headline"> Avis d'experts </h3>
                    <p><i class="pr_name" style="font-style:normal;">TITAN GEL</i> - c’est un agrandissement garanti
                        de la taille du pénis de 5 cm par mois. </p>
                    <p>
                        Ce gel ne provoque pas d'effets secondaires et de réactions allergiques. Titan Gel ne cause pas de
                        dépendance. Le plus important est que le résultat que vous allez atteindre sera permanent.
                    </p>
                    <img class="expert_img" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/img/expert_0_1.jpg"/>
                    <div class="before-after">
                        <span class="before">Avant</span>
                        <span class="after">Après</span>
                    </div>
                </div>
                <div class="bottom">
                    <p>Avec ce gel vous allez atteindre un changement définitif et considérable de la taille du pénis, ce qui
                        aura un effet direct sur votre vie sexuelle, vous recevrez un plaisir surnaturel des rapports avec les
                        femmes et leur donnerez des orgasmes puissants et longs.</p>
                    <p>Avec Titan gel vous serez toujours prêt pour l’action! </p>
                    <img alt="" class="expert_photo" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/img/expert_1.jpg"/>
                    <p class="name">  Médecin en chef du Département d'urologie, CHU de Rennes  </p>
                </div>
            </div>
            <!-- /.expert -->
            <div class="how_work cnt_wrapp">
                <div class="top">
                    <h3 class="headline"> Quel est le principe actif de <i class="pr_name" style="font-style:normal;">TITAN
                            GEL</i></h3>
                    <p>
                        Des enzymes spéciales contenues dans sa formule ont un effet bénéfique sur les tissus du pénis en les
                        étendant d’une manière inoffensive ainsi que les corps caverneux du pénis, ce qui favorise
                        l’agrandissement réel de la longueur et de la circonférence.
                    </p>
                    <div class="image_description">corps caverneux</div>
                </div>
                <div class="pre_bottom">
                    <p> Le gel est appliqué sur le pénis en érection par les mouvements de massage.
                        Vous êtes sûr de gagner 5cm en longueur après le massage avec
                        <i class="pr_name" style="font-style:normal;">TITAN GEL</i>.</p>
                    <p>
                        Pour obtenir le meilleur effet, appliquez le gel une demi-heure avant les rapports sexuels. Cela donnera
                        une stimulation supplémentaire pour l’agrandissement du pénis en longueur et la circonférence.
                    </p>
                </div>
                <ul class="bottom">
                    <li class="li1">
                        <h4> L’EXTASE DE VOTRE PARTENAIRE EST GARANTI </h4>
                        <p> Vous savez ce que les femmes veulent et comment
                            le leur donner. Avec TITAN gel vous les ferez
                            en demander encore et encore !!! </p>
                    </li>
                    <li class="li2">
                        <h4> TOUTE UNE NUIT TOUTES LES NUITS! </h4>
                        <p> Vous deviendrez un champion du marathon sexuel
                            grâce à une capacité de tenir pendant des heures
                            et une érection d’acier </p>
                    </li>
                    <li class="li3">
                        <h4> UNE LIBIDO PUISSANTE </h4>
                        <p> La formule unique augmente la production de testostérone dans le sang, est une hormone essentielle
                            pour les hommes! </p>
                    </li>
                    <li class="li4">
                        <h4> RÉSULTAT RAPIDE AUCUN EFFET SECONDAIRE </h4>
                        <p> En quelques jours seulement, vous sentirez une érection en béton et éprouverez des orgasmes
                            puissants. </p>
                    </li>
                </ul>
            </div>
            <!-- /.how_work -->
            <div class="reviews cnt_wrapp" id="owl">
                <div class="reviews_box rev1">
                    <p class="text">
                        Avant je pensais que 15,5 cm est une taille tout à fait correcte. C’est clair que les rapports n'ont
                        pas été toujours fabuleux. Je suis tombé sur Titan Gel sur le net, et je me suis dit : pourquoi ne
                        pas ajouter quelques centimètres. Après six semaines d'utilisation quotidienne, j’ai pris 3 cm en
                        plus, la verge est sensiblement plus épaisse et plus ferme.

                    </p>
                    <div class="wrapp">
                        <p class="author"> Nicolas, 23 ans </p>
                        <div class="stars">
                            <div></div>
                        </div>
                    </div>
                </div>
                <div class="reviews_box rev2">
                    <p class="text">
                        Mon pénis en érection faisait 12 cm, à l'époque j’ai essayé toutes les pilules et les pompes
                        possibles et imaginables. Je dépensais tout mon salaire avec un effet zéro. Mais après 2 semaines d'utilisation
                        régulière, je constate que mon pénis s‘est allongé de 1,5 cm. J’ai une sensibilité de ouf et des
                        érections sont beaucoup plus fermes.

                    </p>
                    <div class="wrapp">
                        <p class="author"> Martin , 42 ans </p>
                        <div class="stars">
                            <div></div>
                        </div>
                    </div>
                </div>
                <div class="reviews_box rev3">
                    <p class="text">
                        Un soir, ma partenaire m'a dit d’une voix mécontente: "Je vois que ce soir je n’ai pas le
                        droit à un conte" Je me serait fait opérer pour ajouter 1,5 cm. Mais avec ce gel, ma verge a
                        augmenté de 3,9 cm! Maintenant nous avons des rapports de qualité.

                    </p>
                    <div class="wrapp">
                        <p class="author"> Thomas, 32 ans </p>
                        <div class="stars">
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.reviews -->
            <div class="action cnt_wrapp pr_img" id="order_bottom">
                <div class="text_wrapp">
                    <h3 class="headline"> Commande avant qu’il ne soit <br/> trop tard </h3>
                    <p class="h_desc">
                        Après la publication de l'article le du
                        <script type="text/javascript">
                            d = new Date();
                            p = new Date(d.getTime() - 7 * 86400000);
                            monthA = 'Janvier,Fevrier,Mars,Avril,Mai,Juin,Juillet,Aout,Septembre,Octobre,Novembre,Decembre'.split(',');
                            document.write(p.getDate() + ' ' + monthA[p.getMonth()]);
                        </script>
                        dans Playboy, le nombre de produits en stock s’épuise!
                    </p>
                </div>
                <div class="left">
                    <p class="today">
                        <!--<span class="percent">-50%</span> -->
                        aujourd'hui seulement
                    </p>
                    <div class="price_change">
                        <div class="old">
                            <span class="price_land_s4">98</span>
                            <span class="js_curs price_land_curr">€</span>
                        </div>
                        <div class="new">
                            <span class="price_land_s1 js_new_price_curs">49 €</span>
                        </div>
                    </div>
                    <div class="count_wrapp">
                        <p> l’offre se termine dans </p>
                        <div class="countdown"></div>
                    </div>
                </div>
                <form action="" class="right" id="order_form" method="post">

                    <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price" value="55.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAJZCATDlx6JAAEAAQAC5QcBAALECQIGAQLGBgQ268W8AA">
                    <input type="hidden" name="goods_id" value="49">
                    <input type="hidden" name="title" value="Titan gel - FR">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="Titan_Gel_FR_Blue">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="al" value="2500">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="FR">
                    <input type="hidden" name="shipment_vat" value="0.2">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                    <input type="hidden" name="shipment_price" value="6">
                    <input type="hidden" name="price_vat" value="0.2">
-->
                    <p class="restriction"> le nombre est limité! </p>
                    <div class="row clearfix">
                        <button class="order-btn jsOpenWindow" type="button"> Commander</button>
                    </div>
                </form>
            </div>
            <!-- /.action -->
            <footer class="base cnt_wrapp"></footer>
        </div>
    </div>

    <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/js/jquery.plugin.min.js" type="text/javascript"></script>
    <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/js/jquery.countdown.js" type="text/javascript"></script>
    <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/js/owl.carousel.min.js"></script>
    <script type="text/javascript">
        $('#owl').owlCarousel({
            items: 1,
            autoHeight: true,
            singleItem: true
        });
        $('.countdown').countdown({
            until: '+0d +0h 15m ',
            format: 'HMS',
            layout: '<div class="timebox">' +
            '<div class="n">{h10}</div>' +
            '<div class="n">{h1}</div>' +
            '</div>' +
            '<div class="timebox">' +
            '<div class="n">{m10}</div>' +
            '<div class="n">{m1}</div>' +
            '</div>' +
            '<div class="timebox">' +
            '<div class="n">{s10}</div>' +
            '<div class="n">{s1}</div>' +
            '</div>'
        });
        $('.toform').click(function () {
            $("html, body").animate({
                scrollTop: $("form").offset().top - 300
            }, 1000);
            return false;
        });
    </script>
    <!-- hiddenwindow -->
    <div class="hidden-window">
        <div class="containerz">
            <section class="h-w__inner">
                <div class="h-w__header"><span>Titan Gel</span> <br/>commandez maintenant!</div>
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Paiement<br/> <b>à la livraison</b></div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/images/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/images/dtc1.png"/>
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/images/dtc2.png"/>
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/images/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/images/dtc3.png"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">La commande</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Livraisonv</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Livraison et <span>paiement</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext"> Offre avantageuse</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 paquet</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 paquets</option>
                                <option data-slide-index="2" value="5">3+2 paquets</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs"><img alt="zt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/images/zit1.png"/>
                                            </div>
                                            <div class="zHeader"><b>1</b> paquet</div>
                                            <br/>
                                            <div class="pack_descr">Pour ceux qui veulent diversifier leur vie sexuelle
                                            </div>
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Prix</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">49</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>98
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile">en cadeau</div>
                                                    <img alt="zt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/images/zit2.png"/>
                                                </div>
                                                <div class="zHeader"><b>2</b> paquets<br/> <span class="dib zplus">en cadeau</span></div>
                                                <div class="pack_descr">Agrandissement important<br/>
                                                    Il n’y a pas que vous qui remarquerez et apprécierez le résultat!
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Prix</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">98</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>196
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <div class="giftmobile">en cadeau</div>
                                                <img alt="zt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/images/zit3.png"/>
                                            </div>
                                            <div class="zHeader"><b>3</b> paquets<br/> <span class="dib zplus sec">en cadeau</span></div>
                                            <div class="pack_descr"> Agrandissement maximal<br/>
                                                Supériorité au lit garantie, une érection longue et stable
                                            </div>
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Prix</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">147</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>294
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager">
                            <a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a>
                            <a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a>
                            <a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                        </div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAJZCAAAAAAAAAAAAATU6ov6AA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="55.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAJZCATDlx6JAAEAAQAC5QcBAALECQIGAQLGBgQ268W8AA">
                            <input type="hidden" name="goods_id" value="49">
                            <input type="hidden" name="title" value="Titan gel - FR">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Titan_Gel_FR_Blue">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="al" value="2500">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="FR">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                            <input type="hidden" name="shipment_price" value="6">
                            <input type="hidden" name="price_vat" value="0.2">

                            <div class="formHeader"><span>Betaling
</span><br/> na ontvangst
                            </div>
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="FR">France</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 paquet</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 paquets</option>
                                <option data-slide-index="2" value="5">3+2 paquets</option>
                            </select>
                            <input class="inp j-inp" data-count-length="2+" name="name" placeholder="Entrez votre nom et prénom" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Entrez votre numéro de téléphone" type="text" value=""/>
                            <!--
                          <input class="inp" name="city" placeholder="Entrez votre ville" style="display: none" type="text"/>
                          <input class="inp" name="address" placeholder="Entrer votre adresse" type="text"/>
                            -->
                        <!--    <div class="text-center"></div>
                            <div class="text-center totalpriceForm shipmentt">+Livraison: <b class="js_delivery_price">6
                                    €</b></div>
                            <div class="text-center totalpriceForm">total: <span class="js_total_price js_full_price">55 </span>
                                €
                            </div>
                            <div class="zbtn js_pre_submit transitionmyl">Commander</div>
                            <div class="js_submit"></div>-->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Nous garantissons</div>
                            <ul>
                                <li><b>100%</b> qualité</li>
                                <li><b>Vérification</b> du produit à la réception</li>
                                <li><b>Sécurité</b> de vos données</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/mobile/js/jquery.bxslider.min.js"></script>
    <script>
        var slider;


        $(function () {

            if (typeof set_package_prices == 'function') {
                set_package_prices(3);

            }

            $('.corbselect, .change-package-selector').on('change', function () {

                slider.goToSlide($(this).find('option:selected').attr('data-slide-index'))
            });


            $('.jsOpenWindow').on('touchend, click', function (e) {
                e.preventDefault();
                $('.wrhide').hide();
                $('.hidden-window').fadeIn();
                $('html, body').animate({scrollTop: 0}, 300);
                // slider.reloadSlider();
                //         setTimeout(function(){},200);
                slider = $('.bx-bx').bxSlider({
                    pagerCustom: '#bx-pager',
                    adaptiveHeight: true,
                    touchEnabled: true,
                    onSlideBefore: function ($slideElement, oldIndex, newIndex) {
                        var ValOptionChg = $slideElement.attr('data-value');
                        if (typeof set_package_prices == 'function') {
                            set_package_prices(ValOptionChg);

                        }
                        $("select[name='count_select'] option[value='" + ValOptionChg + "']").prop("selected", true);
                    }
                });
                slider.goToSlide(1);


                return false;

            });
        });

    </script>
    <!-- endhiddenwindow   -->
    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <!-- [pre]land_id = 2500 -->
        <script>var locale = "fr";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAJZCATDlx6JAAEAAQAC5QcBAALECQIGAQLGBgQ268W8AA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
            var shipment_price = 6;
            var name_hint = 'Rémi Morel';
            var phone_hint = '+336314254';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("fr");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>TITAN GEL - Le gel unique pour l'agrandissement du pénis
        </title>
        <meta content="telephone=no" name="format-detection"/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <link href="//st.acstnst.com/content/Titan_Gel_FR_Blue/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Titan_Gel_FR_Blue/css/secondPageStyles.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/js/ptimer.js" type="text/javascript">
        </script>
        <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/js/jquery.plugin.min.js" type="text/javascript">
        </script>
        <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/js/jquery.countdown.js" type="text/javascript">
        </script>
        <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/js/script.js" type="text/javascript">
        </script>
        <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/js/secondPage.js" type="text/javascript">
        </script>
        <script src="//st.acstnst.com/content/Titan_Gel_FR_Blue/js/main.js">
        </script>
        <!--[if gte IE 9]>
        <style type="text/css">
            .button,
            .small_banner a {
                filter: none !important;
            }
        </style>
        <![endif]-->
    </head>
    <body>
    <div class="container cf">
        <header class="base">
            <div class="cnt_wrapp">
                <h1> Plus longtemps
                    <span class="s">  plus fort
            </span>
                    <span class="t">  plus grand
            </span>
                </h1>
                <p class="alert"> Le produit est disponible en stock en ce moment.
                    Cependant, après la publication de l'article du
                    <script type="text/javascript">
                        d = new Date();
                        p = new Date(d.getTime() - 7 * 86400000);
                        monthA = 'Janvier,Fevrier,Mars,Avril,Mai,Juin,Juillet,Aout,Septembre,Octobre,Novembre,Decembre'.split(',');
                        document.write(p.getDate() + ' ' + monthA[p.getMonth()]);
                    </script>
                    la disponibilité de ce produit n’est pas garantie
                </p>
                <ul class="benefits">
                    <li class="li1"> AUGMENTE
                        <span>  LA TAILLE ET L’ÉPAISSEUR!
              </span>
                    </li>
                    <li class="li2"> 1 MILLION
                        <span>  DE CLIENTS SATISFAITS!
              </span>
                    </li>
                    <li class="li3"> SANS DANGER
                        <span>  ET FACILE À UTILISER!
              </span>
                    </li>
                    <li class="li4"> SANS PRODUITS CHIMIQUES
                        <span>  SANS POMPES OU EXTENSEURS.
              </span>
                    </li>
                </ul>
                <form action="" class="right" id="order_form" method="post">

                    <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price" value="55.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAJZCATDlx6JAAEAAQAC5QcBAALECQIGAQLGBgQ268W8AA">
                    <input type="hidden" name="goods_id" value="49">
                    <input type="hidden" name="title" value="Titan gel - FR">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="Titan_Gel_FR_Blue">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="al" value="2500">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="FR">
                    <input type="hidden" name="shipment_vat" value="0.2">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                    <input type="hidden" name="shipment_price" value="6">
                    <input type="hidden" name="price_vat" value="0.2">-->
                    <img class="product_image_top" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/_i/titan_cream2.png"/>
                    <p class="restriction"> STOCK LIMITÉ!
                    </p>
                    <div class="row">
                        <button class="button jsOpenWindow" type="button"> Commander
                        </button>
                    </div>
                    <div class="form-shadow">
                    </div>
                </form>
                <!--<div class="pricesn">
      <span>Ancien prix: 78 €</span>
      <span>Prix d'action: 39 €</span>
      </div>-->
                <div class="pricesn">
            <span class="old_price">
              Ancien prix:
              <span> 98 €
              </span>
            </span>
                    <span class="new_price">
              Nouveau prix:
              <span> 49 €
              </span>
            </span>
                </div>
                <div class="doctor_block">
                    <div class="who">
                        TESTE CLINIQUEMENT<br>ET APPROUVE
                        <br/> Ajinomoto, Japon
                    </div>
                </div>
                <div class="days"> seulement
                    <span>4
            </span> semaines
                </div>
            </div>
        </header>
        <!-- /header.base -->
        <div class="expert cnt_wrapp">
            <div class="top">
                <h3 class="headline"> Avis d'experts
                </h3>
                <p> Titan gel garanti un agrandissement de la taille du pénis de 5 cm par mois.
                </p>
                <p> Ce gel ne provoque aucun effets secondaires ni de réactions allergiques. Titan Gel ne cause pas de
                    dépendance. Le plus important est que le résultat que vous allez atteindre sera permanent.
                </p>
                <div class="before-after">
            <span class="before"> Avant
            </span>
                    <span class="after"> Après
            </span>
                </div>
            </div>
            <div class="bottom">
                <p> Avec ce gel vous aurez une augmentation définitive de la taille de votre pénis, ce qui
                    aura un effet direct sur votre vie sexuelle. Vous augmenterez vos capacités érectiles tant en durée qu’en fermeté,vous aurez un contrôle accru et vos rapports sexuels seront plus intenses.
                </p>
                <p> Avec Titan gel vous serez toujours prêt l’action!
                </p>
                <p class="name"> Médecin en chef du Département d'urologie, CHU de Rennes
                </p>
            </div>
        </div>
        <!-- /.expert -->
        <div class="how_work cnt_wrapp">
            <div class="padding_r">
                <h3 class="headline"> Quel est le principe actif de Titan Gel
                </h3>
                <p> Les enzymes contenues dans sa formule unique ont un pouvoir de dilatation sur les tissus et les corps caverneux du pénis en les
                    étendant d’une manière inoffensive et favorisant ainsi,
                    l’augmentation réelle de la longueur et de la circonférence.
                </p>
                <div class="image_description">
                    corps caverneux
                </div>
            </div>
            <div class="padding_l">
                <p> Le gel est appliqué sur le pénis en érection par les mouvements de massage
                </p>
                <p> Gagnez 5cm en longueur après le massage avec TITAN GEL.
                </p>
                <p> Pour obtenir l’effet optimum, appliquez le gel une demi-heure avant les rapports sexuels. Cela apportera
                    une stimulation supplémentaire à l’agrandissement de votre pénis en longueur et en circonférence.
                </p>
            </div>
            <ul class="bottom">
                <li class="li1">
                    <h4> L’EXTASE DE VOTRE PARTENAIRE
                        <br/> EST GARANTIE
                    </h4>
                    <p> Vous savez ce que les femmes veulent et comment le leur donner. Avec TITAN gel elles vous en demanderont encore et encore !!!
                    </p>
                </li>
                <li class="li2">
                    <h4> TOUTE UNE NUIT
                        <br/> TOUTES LES NUITS!
                    </h4>
                    <p> Vous deviendrez le champion du marathon sexuel
                        grâce à une capacité à tenir pendant des heures
                        une érection d’acier
                    </p>
                </li>
                <li class="li3">
                    <h4> UNE LIBIDO
                        <br/> PUISSANTE
                    </h4>
                    <p> Sa formule unique augmente la production de testostérone dans le sang, est une hormone mâle essentielle
                        pour les hommes!
                    </p>
                </li>
                <li class="li4">
                    <h4> RÉSULTAT RAPIDE
                        <br/> AUCUN EFFET SECONDAIRE
                    </h4>
                    <p> En quelques jours seulement, vous sentirez une érection en béton et éprouverez des orgasmes
                        puissants.
                    </p>
                </li>
            </ul>
        </div>
        <!-- /.how_work -->
        <div class="progress cnt_wrapp">
            <h3 class="headline"> Quand verrai-je
                <br/> les résultats?
            </h3>
            <ul class="weeks">
                <li class="li1">
                    Les érections deviennent plus longues et plus dures, la sensibilité de la verge s’intensifie. La longueur et
                    la circonférence augment jusqu’à 1,5 cm.
                </li>
                <li class="li2">
                    La taille de votre pénis augmente sensiblement, la forme devient anatomiquement correcte. La durée des
                    rapports sexuels est prolongée de 70%!
                </li>
                <li class="li3">
                    La verge devient plus longue de 4-5 cm! La qualité des rapports est nettement améliorée. L'orgasme
                    arrive plus vite et dure de 5 à 7 minutes!
                </li>
            </ul>
            <div class="small_banner">
                <div class="left"> AGRANDIS la taille en un mois et
                </div>
                <div class="right"> profite-en toute la vie
                </div>
                <a class="button jsOpenWindow" href="#order"> Commander
                </a>
            </div>
            <ul class="m">
                <li class="li1">
            <span>  +2 cm
            </span> 2me semaine
                </li>
                <li class="li2">
            <span>  + 3 cm
            </span> 3me semaine
                </li>
                <li class="li3">
            <span>  + 5 cm
            </span> 4me semaine
                </li>
            </ul>
        </div>
        <!-- /.progress -->
        <div class="reviews cnt_wrapp">
            <div class="reviews_box">
                <img alt="photo 1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/_i/reviews_1.png"/>
                <div class="wrapp">
                    <p class="text">
                        Avant je pensais que 15,5 cm était une taille tout à fait correcte. C’est clair que mes rapports n'ont
                        pas été toujours fabuleux. J’ai découvert Titan Gel sur le net, et je me suis dit : pourquoi ne
                        pas ajouter quelques centimètres? Après six semaines d'utilisation quotidienne, j’ai pris 3 cm en
                        plus, la verge est sensiblement plus épaisse et plus ferme.
                    </p>
                    <p class="author"> Nicolas, 23 ans
                    </p>
                    <div class="stars">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="reviews_box">
                <img alt="photo 2" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/_i/reviews_2.png"/>
                <div class="wrapp">
                    <p class="text">
                        Mon pénis en érection était de 12 cm, à l'époque et j’ai essayé toutes les pilules toutes les pompes
                        possibles et inimaginables. Je dépensais tout mon salaire un effet nul. Mais après 2 semaines d'utilisation
                        régulière, je constate que mon pénis s‘est allongé de 1,5 cm. J’ai une sensibilité de ouf et des
                        érections beaucoup plus fermes.
                    </p>
                    <p class="author"> Martin , 42 ans
                    </p>
                    <div class="stars">
                        <div style="width:91%;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="reviews_box">
                <img alt="photo 3" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/_i/reviews_3.png"/>
                <div class="wrapp">
                    <p class="text">
                        Un soir, ma partenaire m'a dit d’une voix mécontente: "Je vois que ce soir je n’ai pas droit au bonheur" Après cela, j’étais tellement malheureux que  j’ai pensé me faire opérer pour gagner 1,5 cm.Par hasard, j’ai trouvé sur le net ce gel. Après essai, j’avais gagné en longeur. Depuis ma verge a
                        augmenté de 3,9 cm! Maintenant nous avons des rapports de qualité.
                    </p>
                    <p class="author"> Thomas, 32 ans
                    </p>
                    <div class="stars">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.reviews -->
        <div class="action cnt_wrapp" id="order_bottom">
            <h3 class="headline"> Offre spéciale!
                <br/> Commande avant qu’il ne soit trop tard
            </h3>
            <p class="h_desc"> Après la publication de l'article du
                <script type="text/javascript">
                    d = new Date();
                    p = new Date(d.getTime() - 7 * 86400000);
                    monthA = 'Janvier,Fevrier,Mars,Avril,Mai,Juin,Juillet,Aout,Septembre,Octobre,Novembre,Decembre'.split(',');
                    document.write(p.getDate() + ' ' + monthA[p.getMonth()]);
                </script>
                dans Playboy,
                <br/> le nombre de produits en stock s’épuise!
            </p>
            <div class="b_wrapp">
                <div class="left">
                    <img class="product_image" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/_i/titan_cream2.png" width="140"/>
                    <p class="today">
              <span class="percent">-50%
              </span> aujourd'hui seulement
                    </p>
                    <div class="price_change">
                        <div class="old js_old_price_curs"> 98 €
                        </div>
                        <div class="new js_new_price_curs"> 49 €
                        </div>
                    </div>
                    <div class="count_wrapp">
                        <p> l’offre se termine dans
                        </p>
                        <div class="landing__countdown">
                <span class="hours">00
                </span>
                            <span class="minutes">00
                </span>
                            <span class="seconds">00
                </span>
                        </div>
                    </div>
                </div>
                <form action="" class="right" id="order_form1" method="post">
                    <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price" value="55.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAJZCATDlx6JAAEAAQAC5QcBAALECQIGAQLGBgQ268W8AA">
                    <input type="hidden" name="goods_id" value="49">
                    <input type="hidden" name="title" value="Titan gel - FR">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="Titan_Gel_FR_Blue">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="al" value="2500">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="FR">
                    <input type="hidden" name="shipment_vat" value="0.2">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                    <input type="hidden" name="shipment_price" value="6">
                    <input type="hidden" name="price_vat" value="0.2">-->
                    <div class="form_bottom clearfix" style="padding-left: 218px;">
                        <p class="spy"> strictement
                            <br/> confidentiel
                        </p>
                        <button class="jsOpenWindow" data-form="2" type="button"> Commander
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.action -->
        <footer class="base cnt_wrapp">
        </footer>
        <!-- /footer.base -->
    </div>
    <!-- hiddenwindow -->
    <div class="hidden-window">
        <div class="containerz">
            <section class="h-w__inner">
                <div class="h-w__header">
            <span>Titan Gel – commandez maintenant!
            </span>
                </div>
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Paiement
                                <br/>
                                <b>à la livraison
                                </b>
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>1
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/images/dtc1.png"/>
                                    </div>
                                    <div class="dtable-cell">La commande
                                    </div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>2
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/images/dtc2.png"/>
                                    </div>
                                    <div class="dtable-cell">Livraison
                                    </div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>3
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/images/dtc3.png"/>
                                    </div>
                                    <div class="dtable-cell">Livraison et
                                        <span>paiement
                      </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Offre avantageuse
                        </div>
                        <form action="" class="js_scrollForm" method="post">
                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="55.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAJZCATDlx6JAAEAAQAC5QcBAALECQIGAQLGBgQ268W8AA">
                            <input type="hidden" name="goods_id" value="49">
                            <input type="hidden" name="title" value="Titan gel - FR">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Titan_Gel_FR_Blue">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="al" value="2500">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="FR">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 6}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 6}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 6}}">
                            <input type="hidden" name="shipment_price" value="6">
                            <input type="hidden" name="price_vat" value="0.2">
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="FR">France
                                </option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1 paquet
                                </option>
                                <option selected="selected" value="3">2+1 paquets
                                </option>
                                <option value="5">3+2 paquets
                                </option>
                            </select>
                            <input class="inp j-inp" data-count-length="2+" name="name" placeholder="Entrez votre nom et prénom" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Entrez votre numéro de téléphone" type="text" value=""/>
                            <!--
            <input class="inp" name="city" placeholder="Entrez votre ville" style="display: none" type="text"/>
            <input class="inp" name="address" placeholder="Entrer votre adresse" type="text"/>
            -->
                          <!--  <div class="text-center totalpriceForm">total:
                                <span class="js_total_price js_full_price">
                    55
                  </span>
                                €
                            </div>
                            <div class="zbtn js_pre_submit transitionmyl">Commander
                            </div>
                            <div class="js_submit">
                            </div>-->
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAJZCAAAAAAAAAAAAATU6ov6AA"></iframe>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Nous garantissons
                            </div>
                            <ul>
                                <li>
                                    <b>100%
                                    </b> qualité
                                </li>
                                <li>
                                    <b>Vérification
                                    </b> du produit à la réception
                                </li>
                                <li>
                                    <b>Sécurité
                                    </b> de vos données
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1 ">
                            <div class="zDiscount">
                                <b>Attention
                                </b>:
                                <br/>
                                <span>Remise de 50%
                  </span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader">
                                    <b>1
                                    </b> paquet
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prix:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>49 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
                              <span class="dib old-pricedecoration">
                                <i>
                                </i>
                                98
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
                              <span class="old-pr-descr">
                                <div>
                                  Ancien
                                </div>prix
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>6
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>55
                                                </b> €
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="1">
                                </div>
                                <div class="img-wrapp">
                                    <img alt="z1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/images/zit1.png"/>
                                </div>
                                <div class="zHeader">
                    <span>
                    </span>
                                </div>
                                <text>Pour ceux qui veulent diversifier leur vie sexuelle
                                </text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 active ">
                            <div class="zstick">
                            </div>
                            <div class="zDiscount">
                                <b>Attention
                                </b>:
                                <br/>
                                <span>Rabais de 50%
                  </span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec">
                                    <b>2
                                    </b> paquets
                                    <span class="dib zplus">en cadeau
                    </span>
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prix:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>98 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
                              <span class="dib old-pricedecoration">
                                <i>
                                </i>
                                196
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
                              <span class="old-pr-descr">
                                <div>
                                  Ancien
                                </div>prix
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>6
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>104
                                                </b> €
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3">
                                </div>
                                <div class="img-wrapp">
                                    <div class="gift">en cadeau
                                    </div>
                                    <img alt="z1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/images/zit2.png"/>
                                </div>
                                <div class="zHeader">
                    <span>
                    </span>
                                </div>
                                <text>Agrandissement important
                                    <br/>
                                    Il n’y a pas que vous qui remarquerez et apprécierez le résultat!
                                </text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Attention
                                </b>:
                                <br/>
                                <span>Rabais de 50%
                  </span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec">
                                    <b>3
                                    </b> paquets
                                    <span class="dib zplus sec">en cadeau
                    </span>
                                </div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prix:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>147 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
                              <span class="dib old-pricedecoration">
                                <i>
                                </i>
                                294
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
                              <span class="old-pr-descr">
                                <div>
                                  Ancien
                                </div>prix
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livraison:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>6
                                                    €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>153
                                                </b> €
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5">
                                </div>
                                <div class="img-wrapp">
                                    <div class="gift">en cadeau
                                    </div>
                                    <img alt="z1" src="//st.acstnst.com/content/Titan_Gel_FR_Blue/images/zit3.png"/>
                                </div>
                                <div class="zHeader sm">
                    <span>
                    </span>
                                </div>
                                <text>
                                    Agrandissement maximal
                                    <br/>
                                    Supériorité au lit garantie, une érection longue et stable
                                </text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>
        <!-- endhidden -->
    </div>
    </body>
    </html>
<?php } ?>