<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 4026 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIlDQSX1SSKAAEAAQACwwwBAAK6DwIGAQLGBgQOfrNFAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":39,"old_price":78,"shipment_price":0},"3":{"price":78,"old_price":156,"shipment_price":0},"5":{"price":117,"old_price":234,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Juan Luis';
            var phone_hint = '+34655872135';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>GOJI CREAM
        </title>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <meta content="yes" name="apple-mobile-web-app-capable"/>
        <link href="//st.acstnst.com/content/Goji_cream_IT6/mobile/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Goji_cream_IT6/mobile/css/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Goji_cream_IT6/mobile/js/jquery.countdown.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_IT6/mobile/js/common.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_IT6/mobile/js/scroll.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_IT6/mobile/js/secondPage.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_IT6/mobile/js/jquery.bxslider.min.js"></script>
    </head>
    <body>
    <div class="dbody s__main">
        <header>
            <div class="wrapper">
                <img alt="pack" id="bg" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/header.png"/>
                <img alt="pack" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/6121951.png" style="    position: relative; margin-bottom: 20px; top: 21px;"/>
                <span>Crema per il
            <br/>ringiovanimento
          </span>
                <p>
                    LE BACCHE DI GOJI CONTENGONO:
                </p>
                <ul>
                    <li>20 amminoacidi indispensabili
                    </li>
                    <li>vitamina C (500 volte di più rispetto alle arance))
                    </li>
                    <li>vitamina E
                    </li>
                    <li>ferro (15 volte di più rispetto alle mele)
                    </li>
                </ul>
                <button class="btn for_scroll toform">ordina</button>
            </div>
        </header>
        <aside>
            <div class="wrapper">
                <h3>
                    Ordinatela subito! L innovativa Goji Cream ad un prezzo bassissimo!
                </h3>
                <div class="prices">
                    <p class="old_price ">
<span class="js_old_price_curs">78 €
              </span>
                    </p>
                    <p class="new_price js_new_price_curs">39 €
                    </p>
                </div>
                <div class="sale">
                    <p>50% sconto
                    </p>
                </div>
                <p>ne restano solo 7 a prezzo speciale
                </p>
            </div>
        </aside>
        <section class="extra">
            <div class="wrapper">
                <h3>Effetti di Goji Cream
                </h3>
                <ul>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/img1.png"/>
                        </div>
                        <div>
                            <p>
                                Include diverse componenti che proteggono dagli UV e elementi biologicamente attivi per
                                ripristinare le cellule cutanee.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/img2.png"/>
                        </div>
                        <div>
                            <p>
                                Aiuta a prevenire la ricomparsa delle rughe apportando alla pelle del viso e del collo un
                                ampia gamma di minerali, vitamine e amminoacidi.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/img3.png"/>
                        </div>
                        <div>
                            <p>
                                Attiva la naturale produzione del collagene da parte degli strati più profondi dell
                                epidermide, la quale grazie a Goji Cream recupera l elasticità e la giovinezza di un tempo
                                in tempo record!
                            </p>
                        </div>
                    </li>
                </ul>
                <h3>CANCELLARE 10 ANNI DAL VISO
                    IN 14 GIORNI ORA È POSSIBILE!
                </h3>
                <button class="btn_big for_scroll toform">Voglio sembrare giovane</button>
            </div>
        </section>
        <section class="third_scr">
            <div class="wrapper">
                <h3>L OPINIONE DELL ESPERTO
                </h3>
                <p>
                    Prima di entrare in commercio, i nostri cosmetici vengono sottoposti a controlli dermatologici e a test
                    clinici. Goji Cream si è dimostrata estremamente efficace nella riduzione delle rughe e nel
                    ringiovanimento cutaneo.
                </p>
                <div class="picture">
                    <div class="before"> Prima
                    </div>
                    <div class="after"> Dopo
                    </div>
                </div>
                <img alt="bef_aft" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/img_4.png"/>
                <ul>
                    <li>97 soggetti su 100 hanno esibito risultati visibili già dopo la prima applicazione.
                    </li>
                    <li>Il 93% delle piccole rughe è scomparso dopo aver applicato la crema per 10 giorni
                    </li>
                </ul>
                <button class="red_btn for_scroll toform">comincia il trattamento</button>
            </div>
        </section>
        <section class="third_and_half">
            <div class="wrapper">
                <h3>commenti
                </h3>
                <div class="sliderslider">
                    <div>
                        <div class="bxslider">
                            <div class="slide-list">
                                <div class="img_slide">
                                    <img alt="girl" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/ava1.png"/>
                                </div>
                                <p>
                                    E stata la mia estetista a raccomandarmi Goji Cream,
                                    ha detto che è un prodotto innovativo che però usa la stessa fonte di giovinezza della
                                    medicina cinese - le bacche di goji. Dopo 2 settimane, la mia pelle appariva già più
                                    tonica e le rughe erano appianate. Ora la uso tutti i giorni.
                                </p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide">
                                    <img alt="girl" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/ava2.png"/>
                                </div>
                                <p>
                                    La consistenza della crema è leggera, non appiccica, si assorbe in fretta e appiana le
                                    rughe proprio come un ferro da stiro caldo, e l effetto è visibile dopo un paio di
                                    settimane. E l ideale per la mia pelle.
                                </p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide">
                                    <img alt="girl" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/ava3.png"/>
                                </div>
                                <p>
                                    Me l ha regalata la madre di uno studente, e anche se ricevo regali di ogni tipo, non ho
                                    mai visto un effetto di questo tipo. La pelle è più tonica e le rughe d espressione e le
                                    zampe di gallina scompaiono. La applico da due settimane e i risultati sono evidenti.
                                    Presto comincerò a combattere le rughe profonde.
                                </p>
                            </div>
                        </div>
                    </div>
                    <a class="jcarousel-prev" data-jcarouselcontrol="true" href="#">
                    </a>
                    <a class="jcarousel-next" data-jcarouselcontrol="true" href="#">
                    </a>
                </div>
            </div>
        </section>
        <footer class="form">
            <div class="wrapper clearfix" id="form">
                <img alt="foot" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/img/foot_bg.png"/>
                <h3>
                    OGGI POTETE ORDINARE GOJI CREAM
                    CON UNO SCONTO DEL 50%!
                </h3>
                <div class="prices">
                    <p class="old_price">
<span>Vecchio prezzo:
                <span class="js_old_price_curs">78 €
                </span>
</span>
                    </p>
                    <p class="new_price"> NUOVO PREZZO:
                        <span class="js_new_price_curs">39 €
              </span>
                    </p>
                </div>
                <!-- -->
                <div class="s__of">
                    <button class="btn_sub_of toform" value="">ordina</button>
                </div>
                <!---->
                <div class="counter_wrap">
                    <p>L offerta termina tra:
                    </p>
                    <div class="timer-block countdownHolder countdown_please" id="countdown">
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!--  -->
    <link href="//st.acstnst.com/content/Goji_cream_IT6/mobile/css/stylesecond.css" rel="stylesheet" type="text/css"/>
    <div class="hidden-window">
        <div class="header_new">
            <div class="containerz">
                <div class="h-w__inner">
                    <div class="dtable">
                        <div class="dtable-cell text-center">
                            Goji cream
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">PAGAMENTO IN CONTRASSEGNO<br/>
                            </div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/imagesSec/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/imagesSec/dtc1.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/imagesSec/dtc2.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/imagesSec/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/imagesSec/dtc3.png"/></div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">ORDINE</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">SPEDIZIONE</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">CONSEGNA E <span>PAGAMENTO</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">OTTIMO AFFARE</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 confezione</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 confezione</option>
                                <option data-slide-index="2" value="5">3+2 confezione</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs"><img alt="zt1" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/imagesSec/zit1.png"/> </div>
                                            <div class="zHeader onepack"><b>1</b> confezione</div>
                                            <br/>
                                        </div>
                                        <div class="pack_descr">Metodo di dimagrimento rapido al 50% di sconto! Effetto immediato!
                                            1 confezione di Chocolate Slim ad un nuovo prezzo!
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Prezzo:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 39 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 78 </text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <img alt="zt1" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/imagesSec/zit2.png"/>
                                                </div>
                                                <div class="zHeader"><b>2</b>  confezioni<br/> <span class="dib zplus sec">regalo</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">Risultati stabili! Il trattamento Chocolate Slim vi aiuterà a sbarazzarvi del peso in eccesso per sempre!
                                            3 confezioni di Chocolate Slim al prezzo di 2!
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Prezzo:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 78 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 156 </text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <img alt="zt1" src="//st.acstnst.com/content/Goji_cream_IT6/mobile/imagesSec/zit3.png"/>
                                            </div>
                                            <div class="zHeader"><b>3</b>  confezioni
                                                <br/> <span class="dib zplus sec">regalo</span>
                                            </div>
                                        </div>
                                        <div class="pack_descr">Un approccio professionale! Il trattamento di sei mesi a base di Chocolate Slim vi aiuterà anche in caso di una grande quantità di chili in eccesso!Trasformazioni reali! Risultati garantiti.
                                            5 confezioni di Chocolate Slim al prezzo di 3!
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Prezzo:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 117 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 234 </text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager">
                            <a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a>
                            <a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a>
                            <a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                        </div>
                        <form action="" class="js_scrollForm" method="post"><!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="39.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAIlDQSX1SSKAAEAAQACwwwBAAK6DwIGAQLGBgQOfrNFAA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="Goji cream  - IT">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Goji_cream_IT6">
                            <input type="hidden" name="price" value="39">
                            <input type="hidden" name="old_price" value="78">
                            <input type="hidden" name="al" value="4026">
                            <input type="hidden" name="total_price_wo_shipping" value="39.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="IT">
                            <input type="hidden" name="shipment_vat" value="0.22">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 39, 'old_price': 78, 'shipment_price': 0}, u'3': {'price': 78, 'old_price': 156, 'shipment_price': 0}, u'5': {'price': 117, 'old_price': 234, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.22">

                            <div class="formHeader"></div>
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="IT">Italia</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 paquete</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 paquete </option>
                                <option data-slide-index="2" value="5">3+2 paquete </option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Inserisci il tuo nome" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Inserisci il tuo numero di telefono" type="text" value=""/>
                            <!-- <input class="inp j-inp" name="address" placeholder="Inserisci il tuo indirizzo" type="text"/> -->
                            <!--
                            <input class="inp" name="city" placeholder="Inserisci la tua città" style="display: none" type="text"/>
                            <input class="inp" name="address" placeholder="Inserisci il tuo indirizzo" type="text"/>
                              -->
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIAIlDQAAAAAAAAAAAAT_yB1cAA"></iframe>

                            <div class="text-center"></div>
                            <div class="text-center totalpriceForm shipmentt">+Spedizione: <b class="js_delivery_price">0</b> €</div>
                            <div class="text-center totalpriceForm">Totale: <span class="js_total_price js_full_price">39 </span> €</div>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">GARANTIAMO</div>
                            <ul>
                                <li><b>100%</b> qualità al</li>
                                <li><b>Controllo</b> del prodotto una volta ricevuto</li>
                                <li><b>Sicurezza</b> dei tuoi dati</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!--  -->
    </body>
    </html>
<?php } else { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 4026 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIlDQSX1SSKAAEAAQACwwwBAAK6DwIGAQLGBgQOfrNFAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":39,"old_price":78,"shipment_price":0},"3":{"price":78,"old_price":156,"shipment_price":0},"5":{"price":117,"old_price":234,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Juan Luis';
            var phone_hint = '+34655872135';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <title> Goji Cream </title>
        <meta charset="utf-8"/>
        <meta content="initial-scale=0.95, maximum-scale=0.95, width=device-width" name="viewport"/>
        <link href="//st.acstnst.com/content/Goji_cream_IT6/css/bootstrap.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Goji_cream_IT6/css/styleru.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Goji_cream_IT6/js/bootstrap.min.js"></script>
        <!-- <script src="js/scripts.js"></script> -->
        <script src="//st.acstnst.com/content/Goji_cream_IT6/js/common.js"></script>
    </head>
    <body>
    <div class="s__main">
        <div class="header">
            <div class="container">
                <div class="logo text-center">
                    <button class="order toform"> Goji Cream </button>
                    <p> Crema per il ringiovanimento </p>
                    <!--p> for women aged 50 and over </p-->
                </div>
                <h1 class="text-center title"> Il potere di 1000 bacche in una goccia </h1>
                <div class="cream"></div>
                <div class="right_side">
                    <h3> Le bacche di Goji contengono: </h3>
                    <ul class="list-unstyled">
                        <li><span>  20 amminoacidi indispensabili  </span></li>
                        <li class="padding"><span>  vitamina C (500 volte di più  <br/>  rispetto alle arance)  </span></li>
                        <li><span>  vitamina E  </span></li>
                        <li class="padding"><span>  ferro (15 volte di più  <br/>  rispetto alle mele)  </span></li>
                        <li><span>  vitamine del gruppo B  </span></li>
                        <li class="padding"><span>  betaina  </span></li>
                    </ul>
                    <div class="wrap">
                        <div class="rotate">
                            <p> La formula della crema farà <br/> scomparire le rughe </p>
                            <strong> in 14 giorni! </strong>
                        </div>
                    </div>
                </div>
                <div class="animph">
                    <div class="textimg do active">Prima</div>
                    <div class="wrapimg">
                    </div>
                    <div class="wrapimg1">
                    </div>
                </div>
            </div>
        </div>
        <div class="section_1">
            <div class="container text-center">
                <h3> Ordinatela subito! <br/> L innovativa Goji Cream <br/> ad un prezzo bassissimo! </h3>
                <div class="wrap_1"><span>50%<br/>  sconto  </span></div>
                <div class="wrap_2"><span>  ne restano solo <label>7</label>
<br/>a prezzo speciale </span></div>
                <div class="price">
                    <span class="new js_new_price_curs">  39 €  </span>
                    <span class="old js_old_price_curs">  78 €  </span>
                </div>
                <button class="green_btn j-btn toform"> ordina</button>
            </div>
        </div>
        <div class="section_2">
            <div class="container">
                <h2 class="title"> Effetti di Goji Cream </h2>
                <ul class="list-inline">
                    <li class="letter">
                        <p style="text-align: justify;"> È clinicamente testato che la Goji Cream arresta il processo dell'invecchiamento della pelle e regola il lavoro delle cellule dell'epidermide.</p>
                        <div class="br"></div>
                        <p style="text-align: justify;"> Un ampia gamma di vitamine e minerali, nonché la presenza di
                            biotina nella sua formula, compongono una molecola pesante in grado di penetrare negli strati più
                            profondi della pelle: per questo Goji Cream dà risultati ottimali. </p>
                        <div class="text-center">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_IT6/img/element.jpg"/>
                        </div>
                        <p style="text-align: justify;">
                            Gli amminoacidi che contiene la Goji Cream hanno un potente effetto antiossidante, che permette alla crema di agire sulla pelle per 24 ore in modo continuo. Gli amminoacidi fungono come la spugna, catturando l'acqua transepidermica in modo da prevenire la sua perdita e assicurare l'idratazione profonda contemporaneamente riducendo le rughe sul viso, la zona del collo e del décolleté.
                        </p>
                    </li>
                    <li class="picture">
                        <ul class="list-unstyled">
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_IT6/img/img_1.jpg"/></div>
                                <p style="text-align: justify;"> Include diverse componenti che proteggono dagli UV e elementi
                                    biologicamente attivi per ripristinare le cellule cutanee. </p></li>
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_IT6/img/img_2.jpg"/></div>
                                <p style="text-align: justify;"> Aiuta a prevenire la ricomparsa delle rughe apportando alla
                                    pelle del viso e del collo un ampia gamma di minerali, vitamine e amminoacidi. </p>
                            </li>
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_IT6/img/img_3.jpg"/></div>
                                <p style="text-align: justify;"> Attiva la naturale produzione del collagene da parte degli
                                    strati più profondi dell epidermide, la quale grazie a Goji Cream recupera l elasticità
                                    e la giovinezza di un tempo in tempo record! </p></li>
                        </ul>
                        <h3 class="text-center"> Cancellare 10 anni dal viso <br/> in 14 giorni ora è possibile! </h3>
                        <button class="green_btn j-btn toform"> Voglio sembrare giovane</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_3">
            <div class="container">
                <h2 class="title"> Come applicarla </h2>
                <ul class="list-inline">
                    <li class="step_1">
                        <div class="img_wrap">
                            <h3> Fase 1 </h3>
                        </div>
                        <p> Rimuovere delicatamente il trucco e le impurità con un detergente o un tonico. </p>
                    </li>
                    <li class="step_2">
                        <div class="img_wrap">
                            <h3> Fase 2 </h3>
                        </div>
                        <p> Applicare la crema sulla pelle pulita con movimenti circolari, evitando l area del contorno
                            occhi. </p>
                    </li>
                    <li class="step_3">
                        <div class="img_wrap">
                            <h3> Fase 3 </h3>
                        </div>
                        <p> La vostra pelle apparirà immediatamente più tonica e profondamente idratata. La crema rimane attiva
                            per 24 ore dopo l applicazione. </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wiki_section">
            <div class="container">
                <ul class="list-inline">
                    <li><img alt="" src="//st.acstnst.com/content/Goji_cream_IT6/img/wiki.png"/></li>
                    <li class="text">
                        <h3> Quali sono i pericoli delle rughe? </h3>
                        <p style="text-align: justify;"> La rughe sono pieghe cutanee visibili causate dall eccessiva
                            attività dei muscoli facciali, dalla perdita di elasticità e di tono della pelle e da altri fattori.
                            L elasticità viene fornita dalle fibre di collagene del derma, mentre la tonicità dalle
                            molecole di acido ialuronico che si trovano tra le fibre di collagene. </p>
                        <p style="text-align: justify;"> Le rughe bloccano i vasi sanguigni, impedendo al sangue e alla linfa di
                            circolare nella pelle. Esse non permettono che l ossigeno entri nella pelle, e così mentre le
                            vecchie rughe diventano più evidenti se ne formano di nuove. Se non viene trattato, il processo è
                            irreversibile. </p>
                        <p style="text-align: justify;"> Metodi per la prevenzione delle rughe: lozioni, maschere, iniezioni di
                            Botox, peeling chimici e la chirurgia plastica. Gli ultimi tre sono considerati i più
                            pericolosi. </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_4">
            <div class="container">
                <div class="left_side">
                    <h2 class="title"> I BENEFICI DELLE BACCHE </h2>
                    <p style="text-align: justify;"> Al contrario di quanto affermato dai distributori, le bacche essiccate sono
                        praticamente prive di vitamina C. Per ricevere la quantità di antiossidanti contenuta in una mela rossa,
                        si dovrebbe mangiare un chilo di bacche essiccate, mentre quelle fresche contengono 500 volte la
                        quantità di vitamina C che si trova nelle arance. </p>
                </div>
            </div>
        </div>
        <div class="section_5">
            <div class="container">
                <div class="right_side">
                    <h2 class="title"> L OPINIONE DELL ESPERTO </h2>
                    <p style="text-align: justify;"> Quando creiamo i nostri prodotti per la cura del viso, preferiamo
                        utilizzare ingredienti naturali. La società Hendel usa metodi innovativi per creare la sua famosa linea
                        per la cura del viso. </p>
                    <p style="text-align: justify;"> Prima di entrare in commercio, i nostri cosmetici vengono sottoposti a
                        controlli dermatologici e a test clinici. Goji Cream si è dimostrata estremamente efficace nella
                        riduzione delle rughe e nel ringiovanimento cutaneo. </p>
                    <br/>
                    <div class="picture">
                        <div class="before"> Prima</div>
                        <div class="after"> Dopo</div>
                    </div>
                    <ul class="list-unstyled">
                        <li> 97 soggetti su 100 hanno esibito risultati visibili già dopo la prima applicazione.</li>
                        <li> Il 93% delle piccole rughe è scomparso dopo aver applicato la crema per 10 giorni.</li>
                    </ul>
                    <button class="red_btn j-btn toform"> comincia il trattamento</button>
                </div>
                <div class="doctor">
                    <div class="wrap">
                        <div class="rotate">
                            <h4> Giuseppe Ferrari </h4>
                            <p> Specialista di prodotti<br/>per la cura della pelle, <br/> Hendel </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flower"></div>
        </div>
        <div class="section_6">
            <div class="container">
                <h2 class="title"> commenti </h2>
            </div>
            <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="2" data-target="#carousel-example-generic"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_IT6/img/50/1.png"/>
                                        <span>  prima  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_IT6/img/50/2.png"/>
                                        <span>  dopo  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;"> Amo la medicina tradizionale cinese, perché penso che 5000
                                        anni di storia le abbia permesso di scoprire come meglio preservare la bellezza. Ho 50
                                        anni e recentemente ho notato che la pelle del viso ha perso elasticità. Ho una profonda
                                        ruga tra le sopracciglia e delle zampe di gallina. </p>
                                    <p style="text-align: justify;"> E  stata la mia estetista a raccomandarmi Goji Cream,
                                        ha detto che è un prodotto innovativo che però usa la stessa fonte di giovinezza della
                                        medicina cinese - le bacche di goji. Dopo 2 settimane, la mia pelle appariva già più
                                        tonica e le rughe erano appianate. Ora la uso tutti i giorni. </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_IT6/img/50/3.png"/>
                                        <span>  Prima  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_IT6/img/50/4.png"/>
                                        <span>  Dopo  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;"> Amo tutte le novità, quindi non potevo non provare Goji
                                        Cream, un innovativo trattamento anti-age. Ho quasi 60 anni, e il mio viso si conserva
                                        bene, ma solo perché me ne prendo ottima cura. </p>
                                    <p style="text-align: justify;"> La consistenza della crema è leggera, non appiccica, si
                                        assorbe in fretta e appiana le rughe proprio come un ferro da stiro caldo, e l effetto
                                        è visibile dopo un paio di settimane. E  l ideale per la mia pelle. </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_IT6/img/50/5.png"/>
                                        <span>  Prima  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_IT6/img/50/6.png"/>
                                        <span>  Dopo  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;"> Raccomando caldamente Goji Cream alle donne di tutte le
                                        età, funziona davvero. Le bacche di Goji sono prodotti interessanti già di per se
                                        stesse, hanno molte proprietà magiche, quindi non c è da stupirsi se combattono
                                        anche le rughe. </p>
                                    <p style="text-align: justify;"> Me l ha regalata la madre di uno studente, e anche se
                                        ricevo regali di ogni tipo, non ho mai visto un effetto di questo tipo. La pelle è più
                                        tonica e le rughe d espressione e le zampe di gallina scompaiono. La applico da due
                                        settimane e i risultati sono evidenti. Presto comincerò a combattere le rughe
                                        profonde. </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Controls -->
                <div class="container">
                    <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('prev');" role="button"></a>
                    <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container text-center">
                <h2> Oggi potete ordinare Goji Cream<br/>con uno sconto del 50%! </h2>
                <ul class="list-inline">
                    <li class="timmer_section">
                        <h3 class="old_price"> Vecchio prezzo: <span class="js_old_price_curs"> 78 € </span></h3>
                        <h3 class="new_price"> nuovo prezzo: <br/><span class="js_new_price_curs"> 39 € </span></h3>
                        <ul class="list-unstyled timme_wrap">
                            <li class="left"> L offerta termina tra:</li>
                            <li class="timmer">
                                <ul class="list-inline count">
                                    <li class="hours">01</li>
                                    <li class="minutes">01</li>
                                    <li class="seconds">01</li>
                                </ul>
                            </li>
                            <li class="text">
                                <ul class="list-inline">
                                    <li> ore</li>
                                    <li> minuti</li>
                                    <li> secondi</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="form_wrap">
                        <h3> AFFRETTATEVI IL NUMERO<br/>DI ARTICOLI E  LIMITATO! </h3>
                        <!-- -->
                        <div class="s__of"><button class="green_btn z-btn toform"> ordina</button></div>
                        <!---->
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--  -->
    <link href="//st.acstnst.com/content/Goji_cream_IT6/css/styleSecond.css" media="all" rel="stylesheet" type="text/css"/>
    <div class="hidden-window">
        <div class="second-clips-header">
            <div class="containerz">
                <div class="h-w__inner">Goji cream <br/>ordinalo subito!
                    <div class="yop-right__side">
                        <div class="dtable">
                            <div class="dtable-cell"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader"><b>Pagamento<br/>in contrassegno</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_IT6/imagesSec/dtc1.png"/></div>
                                    <div class="dtable-cell">Ordine</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_IT6/imagesSec/dtc2.png"/></div>
                                    <div class="dtable-cell">Spedizione</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_IT6/imagesSec/dtc3.png"/></div>
                                    <div class="dtable-cell">Consegna e <span>pagamento</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Ottimo affare</div>
                        <form action="" class="js_scrollForm" method="post">
                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="39.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAIlDQSX1SSKAAEAAQACwwwBAAK6DwIGAQLGBgQOfrNFAA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="Goji cream  - IT">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Goji_cream_IT6">
                            <input type="hidden" name="price" value="39">
                            <input type="hidden" name="old_price" value="78">
                            <input type="hidden" name="al" value="4026">
                            <input type="hidden" name="total_price_wo_shipping" value="39.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="IT">
                            <input type="hidden" name="shipment_vat" value="0.22">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 39, 'old_price': 78, 'shipment_price': 0}, u'3': {'price': 78, 'old_price': 156, 'shipment_price': 0}, u'5': {'price': 117, 'old_price': 234, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.22">

                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="IT">Italia</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1 paquete</option>
                                <option selected="selected" value="3">2+1 paquete </option>
                                <option value="5">3+2 paquete </option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Inserisci il tuo nome" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Inserisci il tuo numero di telefono" type="text" value=""/>
                            <!-- <input class="inp" name="address" placeholder="Inserisci il tuo indirizzo" type="text"/> -->
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIAIlDQAAAAAAAAAAAAT_yB1cAA"></iframe>

                            <div class="text-center totalpriceForm">Spedizione: 0 €</div>
                            <div class="text-center totalpriceForm">Totale: <span class="js_total_price js_full_price"> 78 </span> €</div>

                            <div class="zGarant">
                                <div class="zHeader">Garantiamo</div>
                                <ul>
                                    <li><b>100%</b> qualità al</li>
                                    <li><b>Controllo</b> del prodotto una volta ricevuto</li>
                                    <li><b>Sicurezza</b> dei tuoi dati</li>
                                </ul>
                            </div>
                        </form></div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Attenzione</b>:<br/>
                                <span>Sconto del 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader"><b>1</b> confezione </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst"><span class="old-pr-descr">Vecchio prezzo:</span></div>
                                            <div class="dtable-cell nowrap">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old"> 78  €</text>
</span></div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prezzo:</div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new"> 39  €</text></b></div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Spedizione:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship"> 0  €</text></b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Totale:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 39  €</text>
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="1"></div>
                                <div class="img-wrapp"><img alt="z1" src="//st.acstnst.com/content/Goji_cream_IT6/imagesSec/zit1.png"/></div>
                                <div class="zHeader"><span></span></div>
                                <text>Ringiovanimento visibile già dopo un mese di utilizzo</text>
                            </div>
                        </div>
                        <div class="item hot transitionmyl1 active">
                            <div class="zstick"></div>
                            <div class="zDiscount">
                                <b>Attenzione</b>:<br/>
                                <span>Sconto del 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> confezioni <span class="dib zplus">regalo</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst"><span class="old-pr-descr">Vecchio prezzo:</span></div>
                                            <div class="dtable-cell nowrap">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old"> 156  €</text>
</span></div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prezzo:</div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new"> 78  €</text> </b>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Spedizione:</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship"> 0  €</text></b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Totale:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 78  €</text></b></div>
                                            <div class="dtable-cell text-center mw30"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3"></div>
                                <div class="img-wrapp">
                                    <img alt="z1" src="//st.acstnst.com/content/Goji_cream_IT6/imagesSec/zit2.png"/>
                                </div>
                                <div class="zHeader"><span></span></div>
                                <text>Trattamento di giovinezza! Effetto lifting, appiana anche le rughe più profonde in soli tre mesi. Risultati garantiti.</text>
                            </div>
                        </div>
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Attenzione</b>:<br/>
                                <span>Sconto del 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> confezioni <span class="dib zplus sec">regalo</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst"><span class="old-pr-descr">Vecchio prezzo:</span></div>
                                            <div class="dtable-cell nowrap">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old"> 234  €</text>
</span></div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Prezzo:</div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new"> 117  €</text></b>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Spedizione:</div>
                                            <div class="dtable-cell">
                                                <b>
<span>
<text class="js-pp-ship"> 0  €</text></span></b>
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Totale:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 117  €</text></b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5"></div>
                                <div class="img-wrapp">
                                    <img alt="z1" src="//st.acstnst.com/content/Goji_cream_IT6/imagesSec/zit3.png"/>
                                </div>
                                <div class="zHeader sm"></div>
                                <text>Massimo ringiovanimento! Un trattamento completo di 6 mesi non eliminerà solo i cambiamenti legati all'età, ma ne preverrà anche la ricomparsa.</text>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!--  -->
    </body>
    </html>
<?php } ?>