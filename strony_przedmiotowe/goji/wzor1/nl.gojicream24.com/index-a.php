
<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 4080 -->
    <script>var locale = "nl";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAI6DQSUTyOKAAEAAQAC2AwBAALwDwJtAQJyCwQoQxT4AA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
        var shipment_price = 0;
        var name_hint = 'Jessica utrecht';
        var phone_hint = '+31201234568';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a><br><a class="download" href="#">Download our Tips!</a></div>');

            moment.locale("nl");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));
            $('.download').click(function(e) {
                e.preventDefault();  //stop the browser from following
                window.location.href = 'out_tips.pdf';
            });
        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->



    <title> Goji Cream </title>
    <meta charset="utf-8"/>
    <meta content="initial-scale=0.95, maximum-scale=0.95, width=device-width" name="viewport"/>
    <link href="//st.acstnst.com/content/Goji_cream_NL1/css/bootstrap.css" rel="stylesheet"/>
    <link href="//st.acstnst.com/content/Goji_cream_NL1/css/styleru.css" rel="stylesheet"/>
    <script src="//st.acstnst.com/content/Goji_cream_NL1/js/secondPage.js" type="text/javascript"></script>
    <script src="//st.acstnst.com/content/Goji_cream_NL1/js/bootstrap.min.js"></script>
    <script src="//st.acstnst.com/content/Goji_cream_NL1/js/scripts.js"></script>
    <!--[if lte IE 9]><style type="text/css">.footer .form_wrap select {background: none;}</style><![endif]-->
</head>
<body>
<div id="wrapper">
    <div class="header">
        <div class="container">
            <div class="logo text-center">
                <a class="order" href="#"> </a>
                <p>  </p>
                <!--p> for women aged 50 and over </p-->
            </div>
            <h1 class="text-center title"> The Top 5 Benefits of Goji Berries
            </h1>

            <div class="right_side">
                <h3></h3>
                <ul class="list-unstyled">
                    Goji berries continue to gain acclaim as a miracle food packed with health benefits. On a daily basis new information is given to us as studies show more promising positive health results. The nutritional superiority of these amazing berries has been linked to increased healing abilities. Goji berries have been heralded as possibly the most densely nutritious foods available. Chinese medicine has been using these special berries for centuries, and claim to have cured many people of various ailments including eyes, liver and kidney problems.
                </ul>
                <div class="wrap">

                </div>
            </div>

        </div>
    </div>

    <div class="section_2">
        <div class="container">
            <h2 class="title">     </h2>
            <ul class="list-inline">
                <li class="letter">
                    <p style="text-align: justify;">
                        Still not convinced? Here are the top five Goji berry benefits for why you will want to add these berries to your daily diet:
                        Improved Immune System Functions
                        By adding Goji berries to your daily diet, you will benefit in many ways. You will gain improved immune system functions, a necessity for anyone desiring a healthy life. By enhancing the cells used by your immune system, you will be better able to fight bacterial and viral infections and other ailments.
                        Improved Weight Loss

                       </p>
                    <div class="br"></div>
                    <p style="text-align: justify;"> A Goji berry benefit has been touted as excellent in complimenting  a weight loss regime.
                        If you are one of those unfortunate souls who has difficulty in losing weight, try adding Goji berries to your daily diet. The berries have polysaccharides which actually promote the conversion of your food into expendable energy. That makes the berries a guilt free food, because you will not be storing anything in their consumption.
                    </p>
                    <div class="text-center">
                           </div>
                    <p style="text-align: justify;"> Cancer Fighting Properties
                        Goji berries have been linked to retarding cancer growth.
                        These berries contain the proper combination of nutrients which stop the mutation of cancer causing cells. Research has also shown that individuals already suffering from cancer have been benefited by adding Goji berries to their diet.
                        Relief from Sleep Disorders</p>
                    <div class="red_wrap"> Some individuals have reported improved sleep with the use of these berries.
                    </div>
                </li>
                <li class="picture">
                    <ul class="list-unstyled">
                        <li>

                            <p style="text-align: justify;">If you suffer from sleep disorders or simply need to change your sleep pattern, treatment with Goji berries may be a viable option for you. This claim has been verified by medical professionals.
                            </p></li>
                        <li>

                            <p style="text-align: justify;">Improved Longevity
                                Because of the above Goji berry benefits of eating Goji berries, you will extend your lifespan. The dense nutrients found in Goji berries also contains the properties necessary to slow the aging process.
                            </p></li>
                        <li>

                            <p style="text-align: justify;"> What Does Scientific Research Tell Us About the Benefits of the Goji Berry?
                            </p></li>
                    </ul>
                      </li>
            </ul>
        </div>
    </div>
    <div class="section_3">
        <div class="container">

            <ul class="list-inline">
                <li class="step_1">

                    <p> What is the origin of the Goji berry? Tibet, Northern China, and Mongolia are the places in which the Goji berry grows best due to the soil that is abundant with minerals. Goji is a Tibetan word which was coined in 1976. Prior to that, the Goji berry was referred to as the wolfberry in the West.
                    </p>
                </li>
                <li class="step_2">

                    <p> Studies focusing on the medical benefits of Goji berries started around thirty years ago in China.
                    </p>
                </li>
                <li class="step_3">

                    <p> These are some of the Goji berry benefits discovered in these studies.
                        In animal experiments, Goji berries were shown to both preserve DNA and to minimize DNA damage. Anyone who is worried about getting older should be encouraged by these findings. Even though the aging process is multifaceted, one way to address it is to limit the amount of damage done to the DNA.
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="wiki_section">
        <div class="container">
            <ul class="list-inline">
                   <li class="text">

                    <p style="text-align: justify;"> Another closely related study showed that sugars produced from the Goji berry also known as polysaccharides can shield cells found in the testicles from harm caused by free radicals.
                        The goji berry can also protect the brain from Alzheimer’s disease shielding the neurons from a protein called beta amyloid. There is a direct connection between this dangerous protein and Alzheimer’s. Baby boomers should be relieved to hear this as cases of Alzheimer’s are thought to increase in record numbers in the coming years.
                        In 1988, the State Scientific and Technology Commission of China stated that consuming a little less than two ounces or 50 grams of Goji Berries can augment the amount of white blood cells. The antibody immunoglobin A (IgA) was also increased by 75%. The significance of these findings shows that two vital functions of the immune system were enhanced.
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="section_4">
        <div class="container">
            <div class="left_side">
                <h2 class="title"> </h2>
                <p style="text-align: justify;"> Another study done indicates that polysaccharides derived from Goji berries stimulate greater amounts of interleukin-2 to be produced. This particular element shields the body from bacteria and cancer.
                    It is also possible that Goji berries can limit the body’s resistance to insulin. In experiments, those animals who ingested Goji berries for a three week period lost weight and had more acceptable levels of insulin, triglycerides, and cholesterol in their bodies.
                    Sixty seven different medical studies have indicated that Goji berries are useful in maintaining heart health. The book “Discovery of the Ultimate Superfood” references these particular studies. Three additional medical research studies indicate that Goji berries contain antioxidants that restrict the production of lipid peroxidation which can be a cause of heart disease.
                </p>
            </div>
        </div>
    </div>
    <div class="section_5">
        <div class="container">
            <div class="right_side">
                <h2 class="title">  </h2>
                <p style="text-align: justify;">79 cancer patients were treated with a cancer drug mixed with Goji berries and had their cancer reduced. The cancer drugs effects were improved with the introduction of the Goji berries. This more effective combination worked well in the treatment of renal cell carcinoma, malignant melanoma, malignant hydrothrorax, nasopharyngeal carcinoma, and lung cancer.A study done using a test tube shows that Goji berries can retard the growth of leukemia cells found in humans.
                    Goji berries are also useful in protecting the liver. Goji berries are made up of a substance called cerebrosides which is more effective in protecting the liver than milk thistle which is the more well known compound used to treat this organ.
                    So you see there are many Goji berry benefits which encourage us to add them to our diets.
                </p>
                <br/>

                <ul class="list-unstyled">
                    Goji Berry Facial Moisturizer Skin Benefits

                </ul>
                  </div>

        </div>
        <div class="flower"></div>
    </div>
    <div class="section_6">
        <div class="container">
            <h2 class="title"> </h2>
        </div>
        <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                <li data-slide-to="2" data-target="#carousel-example-generic"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">
                                </div>
                                <div class="img_after round">

                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;"> Healthy skin can transform the way you look especially if you use Goji berry facial moisturizer.  Soft, flawless skin can make you look years younger.  It’s important to develop a comprehensive beauty regimen that you follow regularly. Your skin requires care and nourishment. One of the best and quickest ways to get the glowing skin you’ve always dreamed of is to use a facial moisturizer daily.  Products that contain antioxidants such as Goji berries can do wonders for your skin.  Facial moisturizers that contain Goji berries heal and improve your skin from within, so it becomes healthy and beautiful inside and out.
                                    Goji berries have long been considered to be a magical fruit.  They have healing and restorative powers unlike any other ingredients used in skin care.  What makes Goji berries so special is their ability to replenish elements your skin needs to be healthy.  Goji berry facial moisturizers keep your skin nourished and make it look flawless because they work from within.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">

                                </div>
                                <div class="img_after round">

                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;"> The benefits of these super fruits have been recognized for a long time.  In fact, people have been using them for over 6,000 years.  Ancient wisdom dictates that Goji berries improve immune function, increase circulation, and help prevent aging.  They are nature’s answer to problems people encounter as they age.
                                    As it ages, your skin begins to lose its elasticity and glow.  Using a Goji berry facial moisturizer can rejuvenate your skin cells and help them renew faster.  Your skin will become taut and smooth again due to the antioxidants found in these magical fruits.
                                    Goji berries can also add a lot of moisture to your skin and protect it from a number of diseases.  Dry skin can result in eczema, redness, or unattractive flakes.  Facial moisturizers that contain goji berries smooth your skin out and keep it feeling comfortable and healthy.
                                    One of the biggest mistakes people make is using products that contain harmful ingredients.  Many cosmetics on the market today contain toxins or manufactured ingredients.  To avoid putting harmful ingredients on your skin, it’s important to use a 100% natural organic cosmetics skin care.  It’s the only way to know that you are using quality ingredients that are not harmful to your body.  Facial moisturizers that include Goji berries are completely natural.  They don’t contain any chemicals or artificial ingredients.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">

                                </div>
                                <div class="img_after round">

                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;"> In addition to antioxidants, Goji berries contain a number of vitamins and minerals that are good for your health.  They protect your skin from external forces like harmful UV rays and repair damage that has been done to your skin through the years.  Exposure to sun and wind can cause wrinkles.  Goji berries even out your skin’s texture and return it to looking young and beautiful.
                                    In recent years, Goji berries have become one of the hottest trends in the beauty industry.  In addition to their many health benefits when consumed, they can also be used on top of your skin to significantly improve its appearance. With a soft and flawless skin, you’ll feel healthy and confident.  Using a facial moisturizer that contains goji berries can make a dramatic change in the way your skin looks.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <div class="container">
                <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('prev');" role="button"></a>
                <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
            </div>
        </div>
    </div>

</div>

</body>
</html>