<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>





    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 4080 -->
        <script>var locale = "nl";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAI6DQSUTyOKAAEAAQAC2AwBAALwDwJtAQJyCwQoQxT4AA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Jessica utrecht';
            var phone_hint = '+31201234568';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("nl");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>GOJI CREAM</title>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <meta content="yes" name="apple-mobile-web-app-capable"/>
        <link href="//st.acstnst.com/content/Goji_cream_NL1/mobile/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Goji_cream_NL1/mobile/css/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Goji_cream_NL1/mobile/js/jquery.countdown.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_NL1/mobile/js/common.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_NL1/mobile/js/scroll.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_NL1/mobile/js/jquery.bxslider.min.js"></script>
    </head>
    <body>
    <div class="dbody">
        <header>
            <div class="wrapper">
                <img alt="pack" id="bg" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/header.png"/>
                <img alt="pack" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/6121951.png" style="    position: relative; margin-bottom: 20px; top: 21px;"/>
                <span>Verjongingscrème</span>
                <p>
                    Rijpe Goji bevat:
                </p>
                <ul>
                    <li>20 meest waardevolle aminozuren</li>
                    <li>vitamine C (500 keer meer vergeleken met sinaasappels)</li>
                    <li>vitamine E</li>
                    <li>ijzer (15 keer meer vergeleken met appels)</li>
                </ul>
                <div class="btn for_scroll">
                    <a href="#form">Bestel</a>
                </div>
            </div>
        </header>
        <aside>
            <div class="wrapper">
                <h3>
                    Bestel snel! <br/> Innovatief Goji crème product voor een record lage prijs!
                </h3>
                <div class="prices">
                    <p class="old_price "><span class="js_old_price_curs">98 €</span></p>
                    <p class="new_price js_new_price_curs">49 €</p>
                </div>
                <div class="sale">
                    <p>50% korting</p>
                </div>
                <p>Aanbieding op voorraad maar op=op </p>
            </div>
        </aside>
        <section class="extra">
            <div class="wrapper">
                <h3>Effecten Goji cream </h3>
                <ul>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/img1.png"/>
                        </div>
                        <div>
                            <p>
                                Inclusief verschillende UV bescherming, alsook biologisch
                                actieve elementen voor het herstellen van huidcellen.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/img2.png"/>
                        </div>
                        <div>
                            <p> Het helpt voorkomen dat rimpels terugkeren door de gezichtshuid
                                en nek te verrijken met een grote reeks vitaminen, mineralen en aminozuren.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/img3.png"/>
                        </div>
                        <div>
                            <p>
                                Het activeert natuurlijk collageen productie in de diepere
                                lagen van de huid, met Goji cream herwint de huid zijn elasticiteit en jeugd in
                                recordtijd!
                            </p>
                        </div>
                    </li>
                </ul>
                <h3>
                    In 14 dagen een 10 jaar jongere huid!
                </h3>
                <div class="btn_big for_scroll">
                    <a href="#form">Ik wil er jong uit zien</a>
                </div>
            </div>
        </section>
        <section class="third_scr">
            <div class="wrapper">
                <h3>MENING VAN DE EXPERT</h3>
                <p>
                    Voordat ze in de winkel liggen testen onze dermatologen de producten en
                    voeren klinische proeven uit. Goji cream heeft zichzelf bewezen om zeer effectief te zijn tegen
                    rimpelvermindering en huidverjonging.
                </p>
                <div class="picture">
                    <div class="before"> Voor</div>
                    <div class="after"> Na</div>
                </div>
                <img alt="bef_aft" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/img_4.png"/>
                <ul>
                    <li> 97 van de 100 proefpersonen meldde zichtbare resultaten na het eerste gebruik.</li>
                    <li> 93% van de kleine of smalle rimpels verdwenen na het aanbrengen van de crème voor 10 dagen.</li>
                </ul>
                <div class="red_btn for_scroll">
                    <a href="#form">start behandeling </a>
                </div>
            </div>
        </section>
        <section class="third_and_half">
            <div class="wrapper">
                <h3>Ervaringen</h3>
                <div class="sliderslider">
                    <div>
                        <div class="bxslider">
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/ava1.png"/></div>
                                <p>
                                    Mijn schoonheidsspecialiste adviseerde me Goji cream, ze
                                    zei dat het een innovatief product is dat tegelijkertijd gebruik maakt van de
                                    traditionele Chinese bron van de jeugd - Goji bessen. Nadat ik het twee weken had
                                    gebruikt, maakte het mijn huid heel stevig en de rimpels werden gladder, nu gebruik ik
                                    het elke dag.
                                </p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/ava-4.png"/></div>
                                <p>
                                    Wat kan ik zeggen, de structuur van de crème is prima,
                                    licht, niet plakkerig, het wordt snel opgenomen en maakt rimpels kleiner, net als een
                                    strijkijzer, en best snel, het effect was zichtbaar binnen een paar weken. Het past
                                    perfect bij mijn huid.
                                </p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/ava-3.png"/></div>
                                <p>
                                    De moeder van een leerling van mij gaf het cadeau aan me,
                                    en mensen geven me verschillende dingen, maar dit effect had ik nog nooit gezien - de
                                    huid herstelde snel, de spanning en kraaienpoten en kleine imitatie rimpels worden
                                    gladder. Ik breng het nu twee weken aan en de resultaten zijn zichtbaar. Ik denk dat ik
                                    binnenkort ga vechten met serieuze rimpels.
                                </p>
                            </div>
                        </div>
                    </div>
                    <a class="jcarousel-prev" data-jcarouselcontrol="true" href="#"></a>
                    <a class="jcarousel-next" data-jcarouselcontrol="true" href="#"></a>
                </div>
            </div>
        </section>
        <footer class="form">
            <div class="wrapper clearfix" id="form">
                <img alt="foot" src="//st.acstnst.com/content/Goji_cream_NL1/mobile/img/foot_bg.png"/>
                <h3>
                    Alleen vandaag kun je Goji crème bestellen met 50% korting!
                </h3>
                <div class="prices">
                    <p class="old_price"><span>Oude prijs::<span class="js_old_price_curs">98 €</span></span></p>
                    <p class="new_price"> Nieuwe prijs: <span class="js_new_price_curs">49 €</span></p>
                </div>
                <form action="" method="post">
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abcdg.pro/forms/?target=-4AAIJIAI6DQAAAAAAAAAAAASOOHGOAA"></iframe>

                    <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price" value="49.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIAI6DQSUTyOKAAEAAQAC2AwBAALwDwJtAQJyCwQoQxT4AA">
                    <input type="hidden" name="goods_id" value="24">
                    <input type="hidden" name="title" value="Goji cream - NL">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="Goji_cream_NL1">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="al" value="4080">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="NL">
                    <input type="hidden" name="shipment_vat" value="0.0">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.0">

                    <select class="inp" id="country_code_selector" name="country">
                        <option value="NL">Netherlands</option>
                        <option value="BE">België</option>
                    </select>
                    <input class="inp" name="name" placeholder="Naam" type="text"/>
                    <input class="inp only_number" name="phone" placeholder="Vul telefoonnummer in" type="text"/>
                    <!--div class="shipment_price">Verzending: 0 €</div-->
                    <div class="total_price">Totaalprijs: 49 €</div>

                </form>
                <div class="counter_wrap">
                    <p>Aanbieding eindigt in:</p>
                    <div class="timer-block countdownHolder countdown_please">
                        <div class="counter" id="counter">
                            <div class="hours">00</div>
                            <div class="mins">15</div>
                            <div class="secs">00</div>
                        </div>
                    </div>
                </div>
            </div>
            <p style="margin-top: 10px;font-size: 14px;text-transform: none;padding: 0 30px;color: #fff;text-align: center;">Houd u er rekening mee dat lokale BTW tarieven kunnen varieren afhankelijk van de voorschriften van het land waaruit u onze producten besteld. Onze Sales/Klantenservice medewerkers helpen u hier graag verder mee.</p>
        </footer>
    </div>
    </body>
    </html>
<?php } else { ?>





    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 4080 -->
        <script>var locale = "nl";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAI6DQSUTyOKAAEAAQAC2AwBAALwDwJtAQJyCwQoQxT4AA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Jessica utrecht';
            var phone_hint = '+31201234568';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("nl");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <title> Goji Cream </title>
        <meta charset="utf-8"/>
        <meta content="initial-scale=0.95, maximum-scale=0.95, width=device-width" name="viewport"/>
        <link href="//st.acstnst.com/content/Goji_cream_NL1/css/bootstrap.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Goji_cream_NL1/css/styleru.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Goji_cream_NL1/js/secondPage.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Goji_cream_NL1/js/bootstrap.min.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_NL1/js/scripts.js"></script>
        <!--[if lte IE 9]><style type="text/css">.footer .form_wrap select {background: none;}</style><![endif]-->
    </head>
    <body>
    <div class="header">
        <div class="container">
            <div class="logo text-center">
                <a class="order" href="#"> Goji cream </a>
                <p> Verjongingscrème </p>
                <!--p> for women aged 50 and over </p-->
            </div>
            <h1 class="text-center title"> De kracht van 1000 bessen in één druppel crème </h1>
            <div class="cream"></div>
            <div class="right_side">
                <h3> Rijpe Goji bevat: </h3>
                <ul class="list-unstyled">
                    <li><span>  20 meest waardevolle aminozuren  </span></li>
                    <li class="padding"><span>  vitamine C (500 keer meer  <br/>  vergeleken met sinaasappels)  </span></li>
                    <li><span>  vitamine E  </span></li>
                    <li class="padding"><span>  ijzer (15 keer meer  <br/>  vergeleken met appels)  </span></li>
                    <li><span>  vitamine B groep  </span></li>
                    <li class="padding"><span>  betaïne  </span></li>
                </ul>
                <div class="wrap">
                    <div class="rotate">
                        <p> De formule van de crème zorgt voor <br/> het verdwijnen van rimpels </p>
                        <strong> in 14 dagen! </strong>
                    </div>
                </div>
            </div>
            <div class="animph">
                <div class="textimg do active">Voor</div>
                <div class="wrapimg">
                </div>
                <div class="wrapimg1">
                </div>
            </div>
        </div>
    </div>
    <div class="section_1">
        <div class="container text-center">
            <h3> Bestel snel! <br/> INNOVATIEVE GOJI CRÈME VOOR<br/> EEN FANTASTISCH LAGE PRIJS!</h3>
            <div class="wrap_1"><span>50%<br/>  korting  </span></div>
            <div class="wrap_2" style="padding-top: 25px;"><span> Aanbieding op <br/>voorraad maar <br/>op=op </span></div>
            <div class="price">
                <span class="new js_new_price_curs">  49 €  </span>
                <span class="old js_old_price_curs">  98 €  </span>
            </div>
            <div class="green_btn j-btn order"> bestel</div>
        </div>
    </div>
    <div class="section_2">
        <div class="container">
            <h2 class="title">EFFECTEN GOJI CRÈME </h2>
            <ul class="list-inline">
                <li class="letter">
                    <p style="text-align: justify;">Het is klinisch bewezen dat Goji crème het verouderingsproces van cellen stopt en de celvernieuwing ondersteunt.</p>
                    <div class="br"></div>
                    <p style="text-align: justify;"> De grote verscheidenheid aan vitaminen en mineralen, waaronder biotine, vormen de juiste formule die in staat is om in de diepere huidlagen door te dringen. Dit zorgt voor het maximale effect van Goji crème </p>
                    <div class="text-center">
                        <img alt="" src="//st.acstnst.com/content/Goji_cream_NL1/img/element.jpg"/>
                    </div>
                    <p style="text-align: justify;"> De Aminozuren in Goji Bessen hebben een sterke antioxidante werking. Dit zorgt ervoor dat de crème 24 uur lang effectief werkt. Aminozuren werken als een spons voor de maximale vochtopname. Dit garandeert een diepere huid hydratatie en een buitengewone rimpelvermindering. </p>
                    <div class="red_wrap"> Hormoonvrij</div>
                </li>
                <li class="picture">
                    <ul class="list-unstyled">
                        <li>
                            <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_NL1/img/img_1.jpg"/></div>
                            <p style="text-align: justify;">  Met UV filters, alsmede biologisch actieve bestanddelen voor het herstellen van de huidcellen. </p></li>
                        <li>
                            <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_NL1/img/img_2.jpg"/></div>
                            <p style="text-align: justify;"> De vitaminen, mineralen en aminozuren gaan de terugkeer van rimpels tegen. </p></li>
                        <li>
                            <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_NL1/img/img_3.jpg"/></div>
                            <p style="text-align: justify;"> Het activeert de natuurlijke collageen productie in de diepere lagen van de huid. Met Goji cream herwint de huid zijn elasticiteit en jeugdigheid in recordtijd! </p></li>
                    </ul>
                    <h3 class="text-center"> In 14 dagen een 10 jaar<br/> jongere huid! </h3>
                    <button class="green_btn j-btn order"> Ik wil er jong uit zien</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="section_3">
        <div class="container">
            <h2 class="title"> Hoe aan te brengen </h2>
            <ul class="list-inline">
                <li class="step_1">
                    <div class="img_wrap">
                        <h3> Stap 1 </h3>
                    </div>
                    <p> Verwijder voorzichtig de make-up van de huid met een gezichtsreiniger of tonic. </p>
                </li>
                <li class="step_2">
                    <div class="img_wrap">
                        <h3> Stap 2 </h3>
                    </div>
                    <p>Breng de crème op de huid aan met cirkelvormige bewegingen. Vermijd het gebied rondom de ogen.</p>
                </li>
                <li class="step_3">
                    <div class="img_wrap">
                        <h3> Stap 3 </h3>
                    </div>
                    <p> Je huid verstevigt meteen en is diep gehydrateerd. De crème blijft gedurende 24 uur na gebruik actief! </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="wiki_section">
        <div class="container">
            <ul class="list-inline">
                <li><img alt="" src="//st.acstnst.com/content/Goji_cream_NL1/img/wiki.png"/></li>
                <li class="text">
                    <h3> Wat is het gevaar van rimpels? </h3>
                    <p style="text-align: justify;"> Rimpels zijn zichtbare huidplooien door buitensporige activiteit van de
                        gezichtsspieren, het verlies van de elasticiteit van de huid, de stevigheid en andere redenen. De
                        elasticiteit wordt geleverd door collageenvezels van de onderhuid, en de stevigheid - door moleculen
                        van hyaluronzuur die tussen de collageenvezels zit. </p>
                    <p style="text-align: justify;"> Rimpels blokkeren bloedvaten, ze tasten de bloed en lymfe circulatie in
                        de huid aan. Het laat geen zuurstof door in de nieuwe huid, nieuwe rimpels verschijnen en de oude
                        worden dieper. Indien onbehandeld is het proces onomkeerbaar. </p>
                    <p style="text-align: justify;"> Het voorkomen van rimpelmethoden: lotions, maskers, Botox injectie's,
                        chemisch vervellen en plastische chirurgie. De laatste drie methoden worden beschouwd als de meest
                        gevaarlijke. </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="section_4">
        <div class="container">
            <div class="left_side">
                <h2 class="title"> VOORDELEN VAN BESSEN </h2>
                <p style="text-align: justify;"> In tegenstelling tot de beweringen van Marketeers, bevatten gedroogde
                    bessen nagenoeg geen vitamine C. Om zulke hoeveelheden aan anti-oxidanten binnen te krijgen zoals in
                    rode appels, moet men 2 pond gedroogde bessen eten, terwijl de verse bessen 500 keer meer vitamine C
                    bevatten dan sinaasappels. </p>
            </div>
        </div>
    </div>
    <div class="section_5">
        <div class="container">
            <div class="right_side">
                <h2 class="title"> MENING VAN DE EXPERT </h2>
                <p style="text-align: justify;"> Bij het creëren van onze gezichtsproducten geven wij de voorkeur aan
                    natuurlijke ingrediënten. Hendel BV gebruikt innovatieve methoden om populaire
                    gezichtsverzorgingsproducten te ontwerpen. </p>
                <p style="text-align: justify;"> Voordat ze in de winkel liggen testen onze dermatologen de producten en
                    voeren klinische proeven uit. Goji cream heeft zichzelf bewezen om zeer effectief te zijn tegen
                    rimpelvermindering en huidverjonging. </p>
                <br/>
                <div class="picture">
                    <div class="before"> Voor</div>
                    <div class="after"> Na</div>
                </div>
                <ul class="list-unstyled">
                    <li> 97 van de 100 proefpersonen meldden zichtbare resultaten na het eerste gebruik</li>
                    <li> 93% van de kleine rimpels verdwenen na het aanbrengen van de crème voor 10 dagen.</li>
                </ul>
                <button class="red_btn j-btn"> start behandeling</button>
            </div>
            <div class="doctor">
                <div class="wrap">
                    <div class="rotate">
                        <h4> Johan Vaatstra  </h4>
                        <p> Toonaangevende Specialist <br/> van huidverzorgingsproducten, <br/> Hendel BV </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="flower"></div>
    </div>
    <div class="section_6">
        <div class="container">
            <h2 class="title"> ERVARINGEN </h2>
        </div>
        <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                <li data-slide-to="2" data-target="#carousel-example-generic"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_NL1/img/50/1.png"/>
                                    <span>  Voor  </span>
                                </div>
                                <div class="img_after round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_NL1/img/50/2.png"/>
                                    <span>  Na  </span>
                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;"> Ik ben gek op Chinese medicijnen en probeerde Chinese huidverzorgingsmiddelen te kopen. Chinezen weten in 5000 jaar geschiedenis intussen beter hoe je je schoonheid kunt behouden. Ik ben 50 jaar en heb recentelijk ontdekt dat de huid van mijn gezicht aan kwaliteit en elasticiteit verliest. Ik zie soms een vrij diepe fronslijn en diepe kraaienpoten rond mijn ogen. </p>
                                <p style="text-align: justify;">Mijn schoonheidsspecialiste adviseerde me Goji cream, ze zei dat het een innovatief product is dat tegelijkertijd gebruik maakt van de traditionele Chinese bron van de jeugd - Goji bessen. Nadat ik het twee weken had gebruikt, maakte het mijn huid heel stevig en de rimpels werden gladder, nu gebruik ik het elke dag. </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_NL1/images/photo-4530.png"/>
                                    <span>  Voor  </span>
                                </div>
                                <div class="img_after round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_NL1/images/photo-4531.png"/>
                                    <span>  Na  </span>
                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;"> Ik houd van nieuwe dingen, dus kon ik zo'n nieuwtje als Goji cream niet missen, een innovatieve anti-verouderingsbehandeling voor mijn gezicht. Ik ben eind 45, mijn gezicht ziet er best goed uit, maar alleen omdat ik het goed verzorg. </p>
                                <p style="text-align: justify;"> Wat kan ik zeggen, de structuur van de crème is prima, licht, niet plakkerig, het wordt snel opgenomen en maakt rimpels kleiner, net als een strijkijzer. Het effect was al zichtbaar binnen een paar weken. Het past perfect bij mijn huid.</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_NL1/images/photo-4510.png"/>
                                    <span>  Voor  </span>
                                </div>
                                <div class="img_after round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_NL1/images/photo-4511.png"/>
                                    <span>  Na  </span>
                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;"> Ik kan Goji cream met vertrouwen aanbevelen aan alle vrouwen van elke leeftijd, het werkt echt. Goji is op zichzelf een interessant product, deze bessen hebben veel magie in zich, het is geen wonder dat ze effectief zijn tegen rimpels. </p>
                                <p style="text-align: justify;"> De moeder van een leerling gaf het mij cadeau. Dit effect had ik nooit verwacht. Mijn huid herstelde snel, kraaienpootjes en kleine rimpels werden snel gladder. Ik breng het nu twee weken aan en de resultaten zijn zichtbaar.  </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <div class="container">
                <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('prev');" role="button"></a>
                <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container text-center">
            <h2> ALLEEN VANDAAG KUN JE GOJI CREAM<br/> BESTELLEN MET 50% KORTING! </h2>
            <ul class="list-inline">
                <li class="timmer_section">
                    <h3 class="old_price"> Oude prijs: <span class="js_old_price_curs"> 98 € </span></h3>
                    <h3 class="new_price"> nieuwe prijs: <br/><span class="js_new_price_curs"> 49 € </span></h3>
                    <ul class="list-unstyled timme_wrap">
                        <li class="left"> Aanbieding eindigt in:</li>
                        <li class="timmer">
                            <ul class="list-inline counter" id="counter">
                                <li class="hours">00</li>
                                <li class="minutes mins">15</li>
                                <li class="seconds secs">00</li>
                            </ul>
                        </li>
                        <li class="text">
                            <ul class="list-inline">
                                <li> uren</li>
                                <li> minuten</li>
                                <li> seconden</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="form_wrap">
                    <h3 style="padding:0 50px;"> Haast je!  Deze aanbieding is slechts tijdelijk geldig en gelimiteerd in aantal! </h3>
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAI6DQAAAAAAAAAAAASOOHGOAA"></iframe>

                        <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="49.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAI6DQSUTyOKAAEAAQAC2AwBAALwDwJtAQJyCwQoQxT4AA">
                        <input type="hidden" name="goods_id" value="24">
                        <input type="hidden" name="title" value="Goji cream - NL">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Goji_cream_NL1">
                        <input type="hidden" name="price" value="49">
                        <input type="hidden" name="old_price" value="98">
                        <input type="hidden" name="al" value="4080">
                        <input type="hidden" name="total_price_wo_shipping" value="49.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="1">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="NL">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <div class="inp select j-count">
                            <select class="country" id="country_code_selector" name="country">
                                <option value="NL">Netherlands</option>
                                <option value="BE">België</option>
                            </select>
                        </div>
                        <input name="name" placeholder="Naam" type="text"/>
                        <input class="only_number" name="phone" placeholder="Vul telefoonnummer in" type="text"/>
                        <!--div class="shipment_price">Verzending: 0 €</div-->
                        <div class="total_price">Totaalprijs: 49 €</div>

                    </form>
                    <p style="margin-top: 10px;font-size: 14px;text-transform: none;padding: 0 30px;color: #fff;">Houd u er rekening mee dat lokale BTW tarieven kunnen varieren afhankelijk van de voorschriften van het land waaruit u onze producten besteld. Onze Sales/Klantenservice medewerkers helpen u hier graag verder mee.</p>
                </li>
            </ul>
        </div>
    </div>
    </body>
    </html>
<?php } ?>