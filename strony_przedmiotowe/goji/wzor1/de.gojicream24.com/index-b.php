<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>

    <!DOCTYPE html>
    <html>
    <head>

        <!-- [pre]land_id = 3110 -->
        <script>var locale = "de";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALbBARH2yOKAAEAAQACWwoBAAImDAIGAQLGBgTS_44aAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Greta Bessel';
            var phone_hint = '+498458103';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("de");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">



        <script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10014593'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Goji Cream </title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,600,700,300&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width" />
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <link rel="stylesheet" href="//st.acstnst.com/content/Revital_DE/mobile/css/style2.css" type="text/css">
        <link rel="stylesheet" href="//st.acstnst.com/content/Revital_DE/mobile/css/styleSecond.css" type="text/css">

        <script src="//st.acstnst.com/content/Revital_DE/mobile/js/jquery.countdown.js"></script>
        <script src="//st.acstnst.com/content/Revital_DE/mobile/js/jcarousel.js"></script>
        <script src="//st.acstnst.com/content/Revital_DE/mobile/js/slider.js"></script>
        <script src="//st.acstnst.com/content/Revital_DE/mobile/js/common.js"></script>

        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--[if IE 7]> <html class="ie7" lang="en"> <![endif]-->
        <!--[if IE 8]> <html class="ie8" lang="en"> <![endif]-->
        <!--[if gte IE 9]>
        <style type="text/css">
            .gradient {
                filter: none;
            }
        </style>
        <![endif]-->
    </head>
    <body>
    <div class="dbody">
        <header>
            <div class="wrapper">
                <img src="http://st.acstnst.com/content/Revital_DE/mobile/img/header.png" id="bg" alt="pack">
                <img src="http://st.acstnst.com/content/Revital_DE/mobile/img/61219511.png" alt="pack">
                <span>Verjüngerungscreme</span>
                <p>REIFE GOJI ENTHALTEN:</p>
                <ul>
                    <li>die 20 wertvollsten Aminosäuren</li>
                    <li>Vitamin C (500-mal mehr
                        als in Orangen)</li>
                    <li>Vitamin E</li>
                    <li>Eisen (15-mal mehr
                        als in Äpfeln)</li>
                    <li>die Vitamin B-Gruppe</li>
                    <li>Betaine</li>
                </ul>
                <div class="btn">
                    <a href="#form" class='jsOpenWindow'>  Bestellen  </a>
                </div>
            </div>
        </header>
        <aside>
            <div class="wrapper">
                <h3>BEEILEN SIE SICH MIT DEM BESTELLEN!
                    DAS INNOVATIVE PRODUKT Goji Cream
                    FÜR EINEN EXTREM NIEDRIGEN PREIS!</h3>
                <div class="prices">
                    <p class="new_price">49 € </p>
                    <p class="old_price"><span>98 € </span></p>
                </div>
                <div class="sale">
                    <p>50% Rabatt</p>
                </div>
                <p>im Lager 7 übrig im Angebot</p>
            </div>
        </aside>
        <section class="extra">
            <div class="wrapper">
                <h3>EFFEKTE MIT Goji Cream</h3>
                <ul>
                    <li>
                        <div>
                            <img src="http://st.acstnst.com/content/Revital_DE/mobile/img/img1.png" alt="bef">
                        </div>
                        <div>
                            <p>
                                Beinhaltet verschiedene UV-Schutzelemente und bioaktive Komponenten, um die Hautzellen wiederherzustellen.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img src="http://st.acstnst.com/content/Revital_DE/mobile/img/img2.png" alt="bef">
                        </div>
                        <div>
                            <p>
                                Es hilft dabei das Wiederauftreten von Falzen zu verhindern, indem die Creme das Gesicht und den Nacken mit einer Vielzahl an Vitaminen, Mineralien und Aminosäuren versorgt.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img src="http://st.acstnst.com/content/Revital_DE/mobile/img/img3.png" alt="bef">
                        </div>
                        <div>
                            <p>
                                Goji Cream aktiviert die natürliche Kollagenproduktion in den tieferen Hautschichten. Dadurch wird die Haut elastisch und verjüngt sich in Rekordzeit!
                            </p>
                        </div>
                    </li>
                </ul>
                <h3>CREMEN SIE SICH 10 JAHRE AUS DEM GESICHT!<br>
                    JETZT IN 14 TAGEN MÖGLICH!</h3>
                <div class="btn_big">
                    <a href="#form" class='jsOpenWindow'>  Ich will jung aussehen.  </a>
                </div>
            </div>
        </section>
        <section class="third_scr">
            <div class="wrapper">
                <h3>EXPERTENMEINUNG</h3>
                <p>Bei der Zusammensetzung unserer Gesichtspflegeprodukte legen wir sehr großen Wert darauf, nur natürliche Inhaltsstoffe zu benutzen. Die Firma Hernel benutzt innovative Methoden, um eine beliebte Gesichtspflegeserie zu kreieren.</p>
                <img src="http://st.acstnst.com/content/Revital_DE/mobile/img/img_41.png" alt="bef_aft">
                <ul>
                    <li>97 von 100 Testpersonen berichteten von sichtbaren Ergebnissen nach der ersten Verwendung.</li>
                    <li>93% berichten davon, dass kleine Falten verschwunden sind, nachdem die Creme 10 Tage verwendet wurde.</li>
                </ul>
                <div class="red_btn">
                    <a href="#form" class='jsOpenWindow'>  Behandlung  </a>
                </div>
            </div>
        </section>
        <section class="third_and_half">
            <div class="wrapper">
                <h3>KOMMENTARE</h3>
                <div class="sliderslider">
                    <div class="slider jcarousel" data-jcarousel="true">
                        <div>
                            <div class="slide-list">
                                <div class="img_slide"><img src="http://st.acstnst.com/content/Revital_DE/mobile/img/ava1.png" alt="girl"></div>
                                <p>Ich bin ein großer Fan der chinesischen Medizin und versuche die chinesische Gesichtspflege zu kaufen. In 5000 Jahren Geschichte wissen sie sicher besser, wie man Schönheit bewahrt. Ich bin 50 und habe bemerkt, dass meine Gesichtshaut ihre Elastizität verliert. Ich kann eine ziemlich tiefe Faltenlinie unter meinen Augen sehen.</p>

                                <p>Meine Kosmetikerin empfahl mir Goji Cream. Sie sagte, es sei ein innovatives Produkt, welches gleichzeitig die traditionell chinesische Jugendquelle, namens Goji Beeren, verwendet. Nach 2 Wochen bekam meine Haut mehr Spannkraft und die Falten wurden weniger. Jetzt benutze ich sie jeden Tag.</p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide"><img src="http://st.acstnst.com/content/Revital_DE/mobile/img/ava2.png" alt="girl"></div>
                                <p>Goji Cream</p>
                                <p>Über die Textur der Creme kann ich nur sagen, dass es sehr angenehm ist. Sie ist leicht, klebt nicht, zieht schnell ein und glättet Falten, wie ein Bügeleisen. Der Effekt war schon nach wenigen Wochen sichtbar. Es passt perfekt zu meiner Haut.</p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide"><img src="http://st.acstnst.com/content/Revital_DE/mobile/img/ava3.png" alt="girl"></div>
                                <p>Ich kann Goji Cream voller Selbstbewusstsein empfehlen, da es für alle Alter wirklich funktioniert. Goji selbst sind ein interessantes Produkt. In diesen Beeren steckt Magie. So ist es kein Wunder, dass sie gegen Falten wirken.</p>
                                <p>Die Mutter meines Schülers gab mir eine Packung als Geschenk. Ich kriege die ganze Zeit Geschenke, aber so einen Effekt hatte ich zuvor noch nicht gesehen. Meine Haut hat schnell wieder einen normalen Rhythmus eingenommen und Krähenfüße sowie die kleinen Fältchen wurden langsam weniger. Ich habe die Creme für 2 Wochen verwendet und die Ergebnisse sind nicht zu übersehen. Demnächst werde ich gegen die tiefen Falten kämpfen.</p>
                            </div>
                        </div>
                    </div>
                    <a class="jcarousel-prev" href="#" data-jcarouselcontrol="true"></a>
                    <a class="jcarousel-next" href="#" data-jcarouselcontrol="true"></a>
                </div>
            </div>
        </section>
        <footer class="form">
            <div class="wrapper clearfix" id="form">
                <img src="http://st.acstnst.com/content/Revital_DE/mobile/img/foot_bg.png" alt="foot">
                <h3>NUR HEUTE KÖNNEN SIE Goji Cream BESTELLEN UND 50% SPAREN
                    Goji Cream MIT 50% RABATT</h3>
                <div class="prices">
                    <p class="old_price"><span>NEUER PREIS: 98 € </span></p>
                    <p class="new_price"> Alter Preis: 49 €</p>
                </div>
                <form action="" method="post">
                    <input type="hidden" name="template_name" value="Revital_DE">
                    <input type="hidden" name="al" value="3110">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="1">
                    <input type="hidden" name="country_code" value="DE">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="goods_id" value="24">
                    <input type="hidden" name="title" value="Goji cream - DE">
                    <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="price_vat" value="0.19">
                    <input type="hidden" name="esub" value="-4A25sMQIJIALbBARH2yOKAAEAAQACWwoBAAImDAIGAQLGBgTS_44aAA">
                    <input type="hidden" name="shipment_vat" value="0.19">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="price" value="49">
                    <input type="hidden" name="old_price" value="98">
                    <input type="hidden" name="total_price" value="49.0">
                    <input type="hidden" name="validate_address" value="1">
                    <input type="hidden" name="total_price_wo_shipping" value="49.0">
                    <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="submit" class="btn_sub jsOpenWindow" value="Bestellen">
                </form>

                <div class="counter_wrap">
                    <p>Angebot endet in:</p>
                    <div class="timer-block countdownHolder countdown_please" id="countdown"></div>
                </div>
            </div>
        </footer>
    </div>









    <!-- hidden   -->







    <!-- hiddenwindow -->





    <div class="hidden-window">

        <div class="new-header_goji">

        </div>
        <div class="containerz">
            <section class='h-w__inner'>




                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Bezahlung PER NACHNAME</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img src="http://st.acstnst.com/content/Revital_DE/mobile/images/Zarr.png" alt="arrow" class="zarrr"><img src="http://st.acstnst.com/content/Revital_DE/mobile/images/dtc1.png" alt="alt1"></div>

                                        </div>

                                        <div class="dtable-cell">
                                            <div class=""><img src="http://st.acstnst.com/content/Revital_DE/mobile/images/dtc2.png" alt="alt1"></div>

                                        </div>

                                        <div class="dtable-cell">
                                            <div class=""><img src="http://st.acstnst.com/content/Revital_DE/mobile/images/Zarr.png" alt="arrow" class="zarrrLeft"><img src="http://st.acstnst.com/content/Revital_DE/mobile/images/dtc3.png" alt="alt1"></div>

                                        </div>

                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">

                                            <div class="">DIE BESTELLUNG</div>
                                        </div>

                                        <div class="dtable-cell">

                                            <div class="">VERSAND</div>
                                        </div>

                                        <div class="dtable-cell">

                                            <div class="">LIEFERUNG UND <span>BEZAHLUNG</span></div>
                                        </div>
                                    </div>


                                </div>




                            </div>

                        </div>


                        <div class="new-logo-descr" style='text-align: center;background: #fff; padding: 10px 0; margin-top:10px; margin-bottom: 10px;    color: #000; font-size: 24px;'>
                            In Zusammenarbeit mit<br>
                            <img src="http://st.acstnst.com/content/Revital_DE/mobile/images/nmnm.png" alt="logo2">
                        </div>




                        <div class="select-block">
                            <div class="text-center selecttext">FAIRER DEAL</div>

                            <select name="count_select" class="corbselect select inp select_count change-package-selector">
                                <option value="1"  data-slide-index="0">1 Packung</option>
                                <option value="3" selected="selected" data-slide-index="1">2+1 Packungen</option>
                                <option value="5" data-slide-index="2">3+2 Packungen</option>
                            </select>

                        </div>


                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value='1'>

                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class='ssimgabs'><img src="http://st.acstnst.com/content/Revital_DE/mobile/images/zit11.png"  alt='zt1'>  </div>
                                            <div class="zHeader onepack"><b>1</b> Packung </div><br>

                                        </div>

                                        <div class="pack_descr"> Sichtbare Ergebnisse durch die Hydratierung der Epidermiszelle.</div>
                                        <div class="dtable" style='width: 80%;margin: 0 auto;'>
                                            <div class="dtable-cell red text-right">Preis</div>
                                            <div class="dtable-cell"><div class="js_pack_price"><text class='js-pp-new'> 49 </text></div></div>
                                            <div class="dtable-cell currencycell"><div class="pack_old_price"><i></i><text class='js-pp-old'> 98 </text></div>€</div>
                                        </div>

                                    </div>

                                </li>
                                <li data-value='3'>
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class='ssimgabs'>
                                                    <div class="giftmobile">Geschenk</div>
                                                    <img src="http://st.acstnst.com/content/Revital_DE/mobile/images/zit22.png"  alt='zt1'>    </div>
                                                <div class="zHeader"><b>2</b> Packungen  <br> <span class="dib zplus">geschenkt</span></div>

                                            </div>

                                        </div>

                                        <div class="pack_descr">Der Weg zur Jugend! Liftender Effekt und Glättung der tiefsten Falten,
                                            in nur drei Wochen. Garantiertes Ergebnis.</div>



                                        <div class="dtable" style='width: 80%;margin: 0 auto;'>
                                            <div class="dtable-cell red text-right">Preis</div>
                                            <div class="dtable-cell"><div class="js_pack_price"><text class='js-pp-new'> 98 </text> </div></div>
                                            <div class="dtable-cell currencycell"><div class="pack_old_price"><i></i><text class='js-pp-old'> 196 </text></div>€</div>
                                        </div>

                                    </div>
                                </li>

                                <li data-value='5'>

                                    <div class="zItem list transitionmyl ">

                                        <div class="side-right">
                                            <div class='ssimgabs'>
                                                <div class="giftmobile">Geschenk</div>
                                                <img src="http://st.acstnst.com/content/Revital_DE/mobile/images/zit33.png"  alt='zt1'>    </div>
                                            <div class="zHeader"><b>3</b> Packungen  <br> <span class="dib zplus sec">geschenkt</span> </div>


                                        </div>

                                        <div class="pack_descr">Die Behandlung  entfernt nicht nur die Altersveränderungen,
                                            sondern beugt Ihnen auch vor. Lässt die nochmalige Bildung der Falten vermeiden,
                                            mit der Hilfe der reichen Palette der Vitamine, Mikroelemente und Aminosäuren!</div>


                                        <div class="dtable" style='width: 80%;margin: 0 auto;'>
                                            <div class="dtable-cell red text-right">Preis</div>
                                            <div class="dtable-cell"><div class="js_pack_price"><text class='js-pp-new'> 147 </text></div></div>
                                            <div class="dtable-cell currencycell"><div class="pack_old_price"><i></i><text class='js-pp-old'> 294 </text></div>€</div>
                                        </div>

                                    </div>
                                </li>


                            </ul>



                        </div>

                        <div id="bx-pager">
                            <a data-slide-index="0" class='change-package-button' data-package-id='1' href="">1</a>
                            <a data-slide-index="1" class='change-package-button' data-package-id='3' href="">3</a>
                            <a data-slide-index="2" class='change-package-button' data-package-id='5' href="">5</a>
                        </div>









                        <form action="" method="post" class="js_scrollForm">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIALbBAAAAAAAAAAAAARh8AIjAA"></iframe>

                            <!-- <input type="hidden" name="template_name" value="Revital_DE">
                             <input type="hidden" name="al" value="3110">
                             <input type="hidden" name="currency" value="€">
                             <input type="hidden" name="package_id" value="1">
                             <input type="hidden" name="country_code" value="DE">
                             <input type="hidden" name="ip_country" value="PL">
                             <input type="hidden" name="shipment_price" value="0">
                             <input type="hidden" name="goods_id" value="24">
                             <input type="hidden" name="title" value="Goji cream - DE">
                             <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                             <input type="hidden" name="price_vat" value="0.19">
                             <input type="hidden" name="esub" value="-4A25sMQIJIALbBARH2yOKAAEAAQACWwoBAAImDAIGAQLGBgTS_44aAA">
                             <input type="hidden" name="shipment_vat" value="0.19">
                             <input type="hidden" name="ip_city" value="Toruń">
                             <input type="hidden" name="price" value="49">
                             <input type="hidden" name="old_price" value="98">
                             <input type="hidden" name="total_price" value="49.0">
                             <input type="hidden" name="validate_address" value="1">
                             <input type="hidden" name="total_price_wo_shipping" value="49.0">
                             <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                             <input type="hidden" name="protected" value="None">
                             <input type="hidden" name="ip_country_name" value="Poland">




                             <div class="formHeader"><span>Zum Bestellen</span>
                                 <br> Daten eintragen</div>


                             <select name="country_code" class='select inp' id="country_code_selector">

                                 <option value="DE">Deutschland</option>
                                 <option value="AT">Austria</option>
                             </select>
                             <select name="count_select" class="select inp  select_count change-package-selector">
                                 <option value="1"  data-slide-index='0'>1 Packungen</option>
                                 <option value="3" data-slide-index='1' selected="selected">2+1 Packungen</option>
                                 <option value="5" data-slide-index='2'>3+2 Packungen</option>
                             </select>

                             <input type="text" name="name" class="inp j-inp" value="" placeholder="Namen eintragen">
                             <input type="text" name="phone" class="only_number inp j-inp" value="" placeholder="Telefonnummer eintragen">
                             <input type="text" name="address" placeholder="Adresse" class="inp j-inp">




                             <div class="text-center"></div>
                             <!--div class="text-center totalpriceForm shipmentt">+Versand: <b class='js_delivery_price'>0</b> €</div-->
                            <div class="text-center totalpriceForm">Gesamtsumme: <span class="js_total_price js_full_price">49 </span> €</div>



                        </form>

                        <div class="zGarant">
                            <div class="zHeader">WIR GARANTIEREN:</div>
                            <ul>
                                <li><b>100%</b>  Qualität</li>
                                <li><b>Produktprüfung </b> nach Erhalt</li>
                                <li><b>Sicherheit</b> Ihrer Daten</li>
                            </ul>

                        </div>
                    </div>


                </div>
            </section>

        </div>



    </div>














    <script src="//st.acstnst.com/content/Revital_DE/mobile/js/jquery.bxslider.min.js"></script>


    <script>
        var slider;





        $(function() {

            if(typeof set_package_prices == 'function'){
                set_package_prices(3);

            }

            $('.corbselect, .change-package-selector').on('change', function(){

                slider.goToSlide($(this).find('option:selected').attr('data-slide-index'))
            });


            $('.jsOpenWindow').on('touchend, click', function(e){
                e.preventDefault();
                $('.dbody').hide();
                $('.hidden-window').fadeIn();
                $('html, body').animate({scrollTop: 0 }, 300);
                // slider.reloadSlider();
                //         setTimeout(function(){},200);
                slider = $('.bx-bx').bxSlider({
                    pagerCustom: '#bx-pager',
                    adaptiveHeight: true,
                    touchEnabled: true,
                    onSlideBefore : function($slideElement, oldIndex, newIndex){
                        var ValOptionChg = $slideElement.attr('data-value');
                        if(typeof set_package_prices == 'function'){
                            set_package_prices(ValOptionChg);

                        }
                        $("select[name='count_select'] option[value='"+ValOptionChg+"']").prop("selected", "selected");
                    }
                });
                slider.goToSlide(1);


                return false;

            });
        });

    </script>



    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <html>
    <head>

        <!-- [pre]land_id = 3110 -->
        <script>var locale = "de";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALbBARH2yOKAAEAAQACWwoBAAImDAIGAQLGBgTS_44aAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Greta Bessel';
            var phone_hint = '+498458103';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("de");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">



        <script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10014593'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <title> Goji Cream </title>
        <meta charset="UTF-8">
        <link href="//st.acstnst.com/content/Revital_DE/css/bootstrap.css" rel="stylesheet">
        <link href="//st.acstnst.com/content/Revital_DE/css/style.css" rel="stylesheet">
        <link href="//st.acstnst.com/content/Revital_DE/css/styleSecond.css" rel="stylesheet">


        <script src="//st.acstnst.com/content/Revital_DE/js/bootstrap.min.js"></script>
        <script src="//st.acstnst.com/content/Revital_DE/js/secondPage.js"></script>

        <script type="text/javascript">

        </script>


        <script type="text/javascript">
            $(document).ready(function () {

                setInterval(function(){




                    $(".wrapimg1").animate({
                        height: "468px"
                    }, 3500,  function(){

                        $(".wrapimg1").animate({
                            height: "0px"
                        }, 1000);
                    });

                    setTimeout(function(){
                        $('.textimg.do').html("Nacher");
                    }, 3100);

                    setTimeout(function(){
                        $('.textimg.do').hide();
                        $('.textimg.do').html("Vorher");
                        $('.textimg.do').fadeIn();
                    }, 4500 );




                }, 5000);





            });
        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('div.order, button.order, a.order').click(function(){
                    $("html, body").animate({scrollTop: $("form").offset().top-300}, 2000);
                    return false;
                });
            });
        </script>

    </head>
    <body>
    <div class="wrwr1">


        <div class="header">
            <div class="container">
                <div class="logo text-center">
                    <a href="#" class="order">  Goji Cream  </a>
                    <p>  Verjüngerungscreme  </p>
                    <!--p> for women aged 50 and over </p-->

                </div>
                <h1 class="text-center title">  1000 Beeren in einem Tropfen   </h1>
                <div class="cream"></div>
                <div class="right_side">
                    <h3>  Reife Goji enthalten:  </h3>
                    <ul class="list-unstyled">
                        <li><span>  die 20 wertvollsten Aminosäuren  </span></li>
                        <li class="padding"><span>  Vitamin C (500-mal mehr  <br/>  als in Orangen)  </span></li>
                        <li><span>  Vitamin E  </span></li>
                        <li class="padding"><span>  Eisen (15-mal mehr  <br/>  als in Äpfeln)  </span></li>
                        <li><span>  die Vitamin B-Gruppe  </span></li>
                        <li class="padding"><span>  Betaine  </span></li>
                    </ul>
                    <div class="wrap">
                        <div class="rotate">
                            <p>  Die Zusammensetzung sorgt dafür,  <br/>  dass Falten verschwinden   </p>
                            <strong>  und zwar in 14 Tagen!  </strong>
                        </div>
                    </div>
                </div>

                <div class="animph">

                    <div class="textimg do active" style="display: block;">Vorher</div>


                    <div class="wrapimg">

                    </div>

                    <div class="wrapimg1" style="height: 0px;">

                    </div>


                </div>


            </div>
        </div>
        <div class="section_1">
            <div class="container text-center">
                <h3>  Beeilen Sie sich mit dem Bestellen!  <br/>  Das innovative Produkt Goji Cream   <br/>  für einen extrem niedrigen Preis!   </h3>
                <div class="wrap_1"><span>50%<br/>  Rabatt  </span></div>
                <div class="wrap_2"><span>  im Lager  <br/><label>7</label></span><p>  übrig   <br/>  im Angebot  </p></div>
                <div class="price">
                    <span class="new js_new_price_curs">  49 €  </span>
                    <span class="old js_old_price_curs">  98 €  </span>
                </div>
                <div class="green_btn jsOpenWindow order">  Bestellen  </div>
            </div>
        </div>
        <div class="section_2">
            <div class="container">
                <h2 class="title">  EFFEKTE MIT Goji Cream   </h2>
                <ul class="list-inline">
                    <li class="letter">
                        <p style="text-align: justify;">  Es ist klinisch bewiesen, dass Goji Cream den Alterungsprozess der Zellen stoppt und sie reguliert.   </p>
                        <div class="br"></div>
                        <p style="text-align: justify;">  Eine große Anzahl <a href="/next/?esub=-4A25sMQIJIALbBARH2yOKAAEAAQACWwoBAAImDAIGAQLGBgTS_44aAA&al=3110">verschiedener</a> Vitamine und Mineralien sowie das enthaltene Biotin ergeben ein schweres Molekül, welches in die tieferen Hautschichten vordringen kann und somit den maximalen Effekt von Goji Cream hervorbringt.   </p>
                        <div class="text-center">
                            <img src="http://st.acstnst.com/content/Revital_DE/img/element.jpg" alt="">
                        </div>
                        <p style="text-align: justify;">  Die Aminosäuren, welche in Goji Beeren enthalten sind, haben kraftvolle antioxidante Eigenschaften, wodurch die Creme 24 Stunden aktiv wirkt. Aminosäuren fungieren als Schwamm gegen den Verlust von transepidermaler Feuchtigkeit. So wird sichergestellt, dass die Haut mit Feuchtigkeit versorgt ist und vorhandene Falten reduziert werden.   </p>
                        <div class="red_wrap">  Ohne Hormone   </div>
                    </li>
                    <li class="picture">
                        <ul class="list-unstyled">
                            <li><div class="img_wrap"><img src="http://st.acstnst.com/content/Revital_DE/img/img_1.jpg" class="round" alt=""></div><p style="text-align: justify;">  Beinhaltet verschiedene UV-Schutzelemente und bioaktive Komponenten, um die Hautzellen wiederherzustellen.  </p></li>
                            <li><div class="img_wrap"><img src="http://st.acstnst.com/content/Revital_DE/img/img_2.jpg" class="round" alt=""></div><p style="text-align: justify;">  Es hilft, das Wiederauftreten von Falzen zu verhindern, indem die Creme das Gesicht und den Nacken mit einer Vielzahl von Vitaminen, Mineralien und Aminosäuren versorgt.  </p></li>
                            <li><div class="img_wrap"><img src="http://st.acstnst.com/content/Revital_DE/img/img_3.jpg" class="round" alt=""></div><p style="text-align: justify;">  Goji Cream aktiviert die natürliche Kollagenproduktion in den tieferen Hautschichten. Dadurch wird die Haut elastisch und verjüngt sich in Rekordzeit!  </p></li>
                        </ul>
                        <h3 class="text-center">  Cremen Sie sich 10 Jahre aus dem Gesicht!   <br/>  Jetzt in 14 Tagen möglich!   </h3>
                        <button class="green_btn jsOpenWindow order">  Ich will jung aussehen.  </button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_3">
            <div class="container">
                <h2 class="title">  Anwendung  </h2>
                <ul class="list-inline">
                    <li class="step_1">
                        <div class="img_wrap">
                            <h3>  1. Schritt  </h3>
                        </div>
                        <p>  Reinigen Sie das Gesicht vorsichtig mit einem Reinigungsgel oder -wasser von Make-up und Verschmutzungen.   </p>
                    </li>
                    <li class="step_2">
                        <div class="img_wrap">
                            <h3>  2. Schritt  </h3>
                        </div>
                        <p>  Verwenden Sie die Creme auf sauberer Haut und massieren Sie sie mit Kreisbewegungen ein. Vermeiden Sie dabei den Augenbereich.   </p>
                    </li>
                    <li class="step_3">
                        <div class="img_wrap">
                            <h3>  3. Schritt  </h3>
                        </div>
                        <p>  Ihre Haut ist sofort gestrafft und mit Feuchtigkeit versorgt. Die Creme bleibt 24 Stunden nach der Anwendung aktiv.  </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wiki_section">
            <div class="container">
                <ul class="list-inline">
                    <li><img src="http://st.acstnst.com/content/Revital_DE/img/wiki.jpg" alt=""></li>
                    <li class="text">
                        <h3>  Was ist die Gefahr von Falten?  </h3>
                        <p style="text-align: justify;">  Falten sind sichtbare Hautfaltungen, welche durch besondere Belastung oder die Gesichtsmuskulatur entstehen, da die Haut ihre Elastizität und Spannkraft verliert. Sicherlich gibt es auch andere Gründe. Die Elastizität der Haut wird durch die Kollagenfasern in der Dermis möglich gemacht. Die Festigkeit durch Moleküle der Hyaluronsäure, welche sich zwischen den Kollagenfasern befinden.  </p>
                        <p style="text-align: justify;">  Falten blockieren Blutgefäße und beeinträchtigen somit den Blutfluss und die Arbeit der Lymphe. Außerdem kann kein Sauerstoff in die Haut eindringen. Als Folge treten neue Falten auf und die Alten werden noch tiefer. Bei fehlender Behandlung ist dieser Prozess irreversibel.  </p>
                        <p style="text-align: justify;">  Methoden zur Verhinderung der Faltenbildung sind: Lotionen, Masken, Botoxspritzen, chemische Peelings und Schönheitschirurgie. Die letzten drei Methoden werden dabei als am gefährlichsten bezeichnet.  </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_4">
            <div class="container">
                <div class="left_side">
                    <h2 class="title">  VORTEILE DER BEEREN  </h2>
                    <p style="text-align: justify;">  Im Gegensatz zu den Behauptungen vieler Personen des Marketings beinhalten getrocknete Beeren so gut wie kein Vitamin C. Um so viele Antioxidantien aufzunehmen, wie in einem gewöhnlichen roten Apfel zu finden sind, müsste man zwei Kilo getrocknete Beeren essen. Im Gegensatz dazu beinhalten die frischen Beeren 500-mal mehr Vitamin C als Orangen.  </p>
                </div>
            </div>
        </div>
        <div class="section_5">
            <div class="container">
                <div class="right_side">
                    <h2 class="title">  EXPERTENMEINUNG  </h2>
                    <p style="text-align: justify;">  Bei der Zusammensetzung unserer Gesichtspflegeprodukte legen wir sehr großen Wert darauf, nur natürliche Inhaltsstoffe zu benutzen. Die Firma Hernel benutzt innovative Methoden, um eine beliebte Gesichtspflegeserie zu kreieren.  </p>
                    <p style="text-align: justify;">  Bevor sie jedoch ins Verkaufsregal wandern, durchlaufen unsere Kosmetika dermatologische Tests und klinische Versuche. Goji Cream hat sich hierbei als besonders wirksam herausgestellt, um Falten zu reduzieren und die Haut zu verjüngen.  </p>
                    <br/>
                    <div class="picture">
                        <div class="before">  Vorher  </div>
                        <div class="after">  Nacher  </div>
                    </div>
                    <ul class="list-unstyled">
                        <li>  97 von 100 Testpersonen berichteten von sichtbaren Ergebnissen nach der ersten Verwendung.   </li>
                        <li>  93% berichten davon, dass kleine Falten verschwunden sind, nachdem die Creme 10 Tage verwendet wurde.  </li>
                    </ul>
                    <button class="red_btn jsOpenWindow">  Behandlung  </button>
                </div>
                <div class="doctor">
                    <div class="wrap">
                        <div class="rotate">
                            <h4>  Jochen Krauser  </h4>
                            <p>  Führender Spezialist  <br/>  für Hautpflegeprodukte,  <br/>  Hernel  </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flower"></div>
        </div>
        <div class="section_6">
            <div class="container">
                <h2 class="title">  Kommentare  </h2>
            </div>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img src="http://st.acstnst.com/content/Revital_DE/img/50/1.png" alt="">
                                        <span>  Vorher  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img src="http://st.acstnst.com/content/Revital_DE/img/50/2.png" alt="">
                                        <span>  Nachher  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">  Ich bin ein großer Fan der chinesischen Medizin und versuche die chinesische Gesichtspflege zu kaufen. In 5000 Jahren Geschichte wissen sie sicher besser, wie man Schönheit bewahrt. Ich bin 50 und habe bemerkt, dass meine Gesichtshaut ihre Elastizität verliert. Ich kann eine ziemlich tiefe Faltenlinie unter meinen Augen sehen.   </p>
                                    </br>
                                    <p style="text-align: justify;">  Meine Kosmetikerin empfahl mir Goji Cream. Sie sagte, es sei ein innovatives Produkt, welches gleichzeitig die traditionell chinesische Jugendquelle, namens Goji Beeren, verwendet. Nach 2 Wochen bekam meine Haut mehr Spannkraft und die Falten wurden weniger. Jetzt benutze ich sie jeden Tag.  </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img src="http://st.acstnst.com/content/Revital_DE/img/50/3.png" alt="">
                                        <span>  Vorher  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img src="http://st.acstnst.com/content/Revital_DE/img/50/4.png" alt="">
                                        <span>  Nacher  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">  Goji Cream   </p>
                                    </br>
                                    <p style="text-align: justify;">  Über die Textur der Creme kann ich nur sagen, dass es sehr angenehm ist. Sie ist leicht, klebt nicht, zieht schnell ein und glättet Falten, wie ein Bügeleisen. Der Effekt war schon nach wenigen Wochen sichtbar. Es passt perfekt zu meiner Haut.  </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img src="http://st.acstnst.com/content/Revital_DE/img/50/5.png" alt="">
                                        <span>  Vorher  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img src="http://st.acstnst.com/content/Revital_DE/img/50/6.png" alt="">
                                        <span>  Nacher  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">  Ich kann Goji Cream voller Selbstbewusstsein empfehlen, da es für alle Alter wirklich funktioniert. Goji selbst sind ein interessantes Produkt. In diesen Beeren steckt Magie. So ist es kein Wunder, dass sie gegen Falten wirken.  </p>
                                    </br>
                                    <p style="text-align: justify;">  Die Mutter meines Schülers gab mir eine Packung als Geschenk. Ich kriege die ganze Zeit Geschenke, aber so einen Effekt hatte ich zuvor noch nicht gesehen. Meine Haut hat schnell wieder einen normalen Rhythmus eingenommen und Krähenfüße sowie die kleinen Fältchen wurden langsam weniger. Ich habe die Creme für 2 Wochen verwendet und die Ergebnisse sind nicht zu übersehen. Demnächst werde ich gegen die tiefen Falten kämpfen.  </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Controls -->
                <div class="container">
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" onclick="$('#carousel-example-generic').carousel('prev');"></a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"  onclick="$('#carousel-example-generic').carousel('next');"></a>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container text-center">
                <h2>  Nur heute können Sie Goji Cream bestellen und 50% sparen  <br/>  Goji Cream mit 50% Rabatt  </h2>
                <ul class="list-inline">
                    <li class="timmer_section">
                        <h3 class="old_price">  Alter Preis   <span class="js_old_price_curs"> 98 € </span></h3>
                        <h3 class="new_price">  Neuer Preis  <br/><span class="js_new_price_curs"> 49 € </span></h3>
                        <ul class="list-unstyled timme_wrap">
                            <li class="left">  Angebot endet in:  </li>
                            <li class="timmer">
                                <ul class="list-inline count">
                                    <li class="hours">01</li>
                                    <li class="minutes">01</li>
                                    <li class="seconds">01</li>
                                </ul>
                            </li>
                            <li class="text">
                                <ul class="list-inline">
                                    <li>  Stunden  </li>
                                    <li>  Minuten  </li>
                                    <li>  Sekunden  </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="form_wrap">
                        <h3>  BEEILEN SIE SICH  <br/>  DIE ANGEBOTSZAHL  <br/>  IST LIMITIERT  </h3>
                        <form action="" method="post">

                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIALbBAAAAAAAAAAAAARh8AIjAA"></iframe>

                            <div class="toform"></div>
                        </form>
                    </li>
                </ul>

            </div>
        </div>
    </div>










    <!-- hidden -->




    <!-- hiddenwindow -->
    <div class="hidden-window">

        <div class="h-headerj">
            <div class="containerz">
                <div class="left-side-header"></div>
                <div class="h-text">
                    jetzt  <br> <span>bestellen!</span>
                </div>
            </div>
        </div>
        <div class="containerz">

            <section class='h-w__inner'>






                <div class="cf">



                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Bezahlung <br>
                                <b>per Nachname</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img src="http://st.acstnst.com/content/Revital_DE/images/dtc1.png" alt="alt1"></div>
                                    <div class="dtable-cell">Die Bestellung</div>
                                </div>

                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img src="http://st.acstnst.com/content/Revital_DE/images/dtc2.png" alt="alt1"></div>
                                    <div class="dtable-cell">Versand</div>
                                </div>

                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img src="http://st.acstnst.com/content/Revital_DE/images/dtc3.png" alt="alt1"></div>
                                    <div class="dtable-cell">Lieferung und <span>Bezahlung</span></div>
                                </div>
                            </div>

                        </div>

                        <div class="new-logo-descr" style='text-align: center;background: #fff; padding: 10px 0; margin-top:10px;    color: #000; font-size: 24px;'>
                            In Zusammenarbeit mit<br>
                            <img src="http://st.acstnst.com/content/Revital_DE/images/nmnm.png" alt="logo2">
                        </div>


                        <div class="printbg">Fairer  <span>Deal</span></div>

                        <form action="" method="post" class="js_scrollForm">
                          <!--  <input type="hidden" name="template_name" value="Revital_DE">
                            <input type="hidden" name="al" value="3110">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="country_code" value="DE">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="Goji cream - DE">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="price_vat" value="0.19">
                            <input type="hidden" name="esub" value="-4A25sMQIJIALbBARH2yOKAAEAAQACWwoBAAImDAIGAQLGBgTS_44aAA">
                            <input type="hidden" name="shipment_vat" value="0.19">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="total_price" value="49.0">
                            <input type="hidden" name="validate_address" value="1">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <div class="formHeader"><span>Zum Bestellen</span>
                                <br> Daten eintragen</div>


                            <select name="country_code" class='select inp' id="country_code_selector">
                                <option value="DE">Deutschland</option>
                                <option value="AT">Austria</option>

                            </select>
                            <select name="count_select" class="select inp select_count change-package-selector">
                                <option value="1">1 Packung</option>
                                <option value="3" selected="selected">2+1 Packungen</option>
                                <option value="5">3+2 Packungen</option>


                            </select>
                            <input type="text" name="name" class="inp j-inp" value="" placeholder="Namen eintragen">
                            <input type="text" name="phone" class="only_number inp j-inp" value="" placeholder="Telefonnummer eintragen">
                            <input type="text" name="address" placeholder="Adresse" class="inp">
                            <!--<textarea name="comment" placeholder="Kommentar zu Bestellung"></textarea>-->
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIALbBAAAAAAAAAAAAARh8AIjAA"></iframe>

                            <div class="text-center totalpriceForm">Gesamtpreis: <span class="js_total_price js_full_price">49 </span> €</div>

                        </form>

                        <div class="zGarant">
                            <div class="zHeader">Wir garantieren:</div>
                            <ul>
                                <li><b>100%</b>  Qualität</li>
                                <li><b>Produktprüfung </b> nach Erhalt</li>
                                <li><b>Sicherheit</b> Ihrer Daten</li>
                            </ul>

                        </div>
                    </div>





                    <div class="w-h__left" >

                        <div class="item transitionmyl1 active">
                            <div class="zDiscount">
                                <b>Achtung</b>: Es gibt <br>
                                einen <span>Rabatt von 50%</span>
                            </div>

                            <div class="item__right">

                                <div class="zHeader"><b>1</b> Packung </div>

                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">

                                            <div class="dtable-cell fst">Preis:</div>
                                            <div class="dtable-cell"> <b> <span class="js-pp-new"> 49 </span> €</b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"> <span class="dib  old-pricedecoration"><i></i> 98 </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr"><div>Alter </div>Preis</span></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dtr">

                                            <div class="dtable-cell fst padding-top10px">Versand:</div>
                                            <div class="dtable-cell"> <b> 0  €</b></div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>

                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">

                                            <div class="dtable-cell fst fs17">Gesamtsumme:</div>
                                            <div class="dtable-cell prtotal"> <b> 49  </b> €</div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>



                                </div>
                            </div>


                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id='1'></div>
                                <div class="img-wrapp"><img src="http://st.acstnst.com/content/Revital_DE/images/zit11.png" alt="z1"></div>
                                <div class="zHeader"><span>&nbsp;</span></div>
                                <text>Sichtbare Ergebnisse durch die Hydratierung der Epidermiszelle.</text>
                            </div>
                        </div>





                        <!--item2-->

                        <div class="item hot transitionmyl1 ">
                            <div class="zstick"></div>
                            <div class="zDiscount">
                                <b>Achtung</b>: Es gibt <br>
                                einen <span>Rabatt von 50%</span>
                            </div>

                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> Packungen <span class="dib zplus">geschenkt</span></div>

                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">

                                            <div class="dtable-cell fst">Preis:</div>
                                            <div class="dtable-cell"> <b> 98  €</b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"> <span class="dib  old-pricedecoration"><i></i> 196 </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr"><div>Alter </div>Preis</span></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dtr">

                                            <div class="dtable-cell fst padding-top10px">Versand:</div>
                                            <div class="dtable-cell"> <b> 0  €</b></div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>

                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">

                                            <div class="dtable-cell fst fs17">Gesamtsumme:</div>
                                            <div class="dtable-cell prtotal"> <b> 98  </b> €</div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>



                                </div>
                            </div>


                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id='3'></div>
                                <div class="img-wrapp"><div class="gift">Geschenk</div><img src="http://st.acstnst.com/content/Revital_DE/images/zit22.png" alt="z1"></div>
                                <div class="zHeader"><span>&nbsp;</span></div>
                                <text> Der Weg zur Jugend! Liftender Effekt und Glättung der tiefsten Falten,
                                    in nur drei Wochen. Garantiertes Ergebnis.</text>
                            </div>
                        </div>


                        <!--end of item2-->

                        <!--item3-->
                        <div class="item transitionmyl1">

                            <div class="zDiscount">
                                <b>Achtung</b>: Es gibt <br>
                                einen <span>Rabatt von 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> Packungen <span class="dib zplus sec">geschenkt</span></div>

                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">

                                            <div class="dtable-cell fst">Preis:</div>
                                            <div class="dtable-cell"> <b> 147  €</b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"> <span class="dib  old-pricedecoration"><i></i> 294 </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr"><div>Alter </div>Preis</span></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dtr">

                                            <div class="dtable-cell fst padding-top10px">Versand:</div>
                                            <div class="dtable-cell"> <b> 0  €</b></div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>

                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">

                                            <div class="dtable-cell fst fs17">Gesamtsumme:</div>
                                            <div class="dtable-cell prtotal"> <b> 147  </b> €</div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>



                                </div>
                            </div>


                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id='5'></div>
                                <div class="img-wrapp"><div class="gift">Geschenk</div><img src="http://st.acstnst.com/content/Revital_DE/images/zit33.png" alt="z1"></div>
                                <div class="zHeader sm"><span>&nbsp;</span></div>
                                <text>Die Behandlung  entfernt nicht nur die Altersveränderungen,
                                    sondern beugt Ihnen auch vor. Lässt die nochmalige Bildung der Falten vermeiden,
                                    mit der Hilfe der reichen Palette der Vitamine, Mikroelemente und Aminosäuren!</text>
                            </div>
                        </div>

                        <!--end-of-item3-->


                    </div>


            </section>

        </div>

        <!-- endhidden -->





    </body>
    </html>
<?php } ?>