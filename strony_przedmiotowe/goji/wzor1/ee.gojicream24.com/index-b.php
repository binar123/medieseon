<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7433 -->
        <script>var locale = "ee";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALeEwRucCaKAAEAAQACRxMBAAIJHQK-AQEABPQnxKkA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Gokhan Karakoc';
            var phone_hint = '+105469652091';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ee");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>GOJI CREAM</title>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <meta content="yes" name="apple-mobile-web-app-capable"/>
        <link href="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/css/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/js/jquery.countdown.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/js/common.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/js/scroll.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/js/jquery.bxslider.min.js"></script>
    </head>
    <body>
    <div class="dbody">
        <header>
            <div class="wrapper">
                <img alt="pack" id="bg" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/header.png"/>
                <img alt="pack" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/6121951.png" style="    position: relative; margin-bottom: 20px; top: 21px;"/>
                <span>Noorendav kreem</span>
                <p>Küpse Goji <br/>sisaldab:</p>
                <ul>
                    <li>20 kõige tähtsamat aminohapet</li>
                    <li>C-vitamiini (500 korda rohkem  <br/>  kui apelsinides)</li>
                    <li>E-vitamiini</li>
                    <li>Rauda (15 korda rohkem  <br/>  kui õunades)</li>
                </ul>
                <div class="btn for_scroll toform">
                    BETAIINI
                </div>
            </div>
        </header>
        <aside>
            <div class="wrapper">
                <h3>Selle kreemi koostis aitab <br/>  kortsudel kaduda</h3>
                <div class="prices">
                    <p class="old_price "><span class="js_old_price_curs">48 €</span></p>
                    <p class="new_price js_new_price_curs">24 €</p>
                </div>
                <div class="sale">
                    <p>50% Allahindlus</p>
                </div>
                <p>Poes 7 allesjäänud eripakkumist</p>
            </div>
        </aside>
        <section class="extra">
            <div class="wrapper">
                <h3>GOJI CREAM-I EFEKTID</h3>
                <ul>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/img1.png"/>
                        </div>
                        <div>
                            <p>
                                Sisaldab erinevaid UV kaitse komponente, samuti bioloogiliselt aktiivseid elemente naharakkude taastamiseks.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/img2.png"/>
                        </div>
                        <div>
                            <p>
                                See aitab ära hoida kortsude taasteket rikastades näonahka ja kaela erinevate vitamiinidega, mineraalidega ja aminohapetega.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/img3.png"/>
                        </div>
                        <div>
                            <p>
                                See aktiveerib loomuliku kollageeni tootmist sügavamates nahakihtides. Goji cream annab nahale tagasi elastsuse ja nooruse rekordiliselt lühikese aja jooksul!
                            </p>
                        </div>
                    </li>
                </ul>
                <h3>Nüüd on võimalik 14 päeva jooksul   10 aastat noorem välja näha!</h3>
                <div class="btn_big for_scroll">
                    <a href="#form">Ma tahan noor välja näha</a>
                </div>
            </div>
        </section>
        <section class="third_scr">
            <div class="wrapper">
                <h3>EKSPERDI HINNANG</h3>
                <p>Luues meie näohooldustooteid, eelistame me ainult looduslikke koostisosi. Hendel firma kasutab uuenduslikke meetodeid, et luua populaarne näohooldustoodete sari.
                </p>
                <div class="for_img">
                    <img alt="bef_aft" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/img_4new1.png"/>
                    <span class="before">Enne</span>
                    <span class="after">Pärast</span>
                </div>
                <ul>
                    <li>97 katsealust 100-st teatasid silmnähtavatest tulemustest pärast esimest kasutuskorda. </li>
                    <li>93% väikestest ja pealiskautsetest kortsudest kadusid pärast 10 päevast kreemi kasutamist.</li>
                </ul>
                <div class="red_btn for_scroll">
                    <a href="#form">ALUSTA RAVIGA</a>
                </div>
            </div>
        </section>
        <section class="third_and_half">
            <div class="wrapper">
                <h3>Kommentaarid</h3>
                <div class="sliderslider">
                    <div>
                        <div class="bxslider">
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/ava1.png"/></div>
                                <p>Ma olen suur Hiina meditsiini austaja ja proovin alati osta Hiina näohooldusvahendeid, kuna usun, et nende 5000 aastapikkuses ajaloos teavad nad kindlasti, kuidas ilu säilitada. Ma olen 50-aastat vana ja avastasin hiljuti, et minu näonahk on kaotanud oma elastsuse. Mul on väga silmapaistev kulmukorts ja kortsud silmade ümber.</p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/ava2.png"/></div>
                                <p>Ma armastan kõike, mis on uus, seega ei saanud ma ilma jääda sellisest uudsusest nagu Goji Cream, innovatiivne näo vananemisvastane hooldus. Ma olen kuldsetes 50-dates, aga mu nägu näeb tegelikult päris hea välja, kuna hoolitsen selle eest hästi.</p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/ava3.png"/></div>
                                <p>Ma kindlasti soovitan Goji Cream-i kõigile naistele igas eas, see tõesti toimib. Goji ise on väga huvitav mari-nendes marjades on palju maagiat, pole ime, et need võitlevad ka kortsude vastu.</p>
                            </div>
                        </div>
                    </div>
                    <a class="jcarousel-prev" data-jcarouselcontrol="true" href="#"></a>
                    <a class="jcarousel-next" data-jcarouselcontrol="true" href="#"></a>
                </div>
            </div>
        </section>
        <footer class="form">
            <div class="wrapper clearfix" id="form">
                <img alt="foot" src="//st.acstnst.com/content/Goji_cream_EE_Red/mobile/img/foot_bg.png"/>
                <h3>Ainult täna saad sa tellida Goji cream-i 50% soodsamalt!  Goji cream 50% soodsamalt!</h3>
                <div class="counter_wrap">
                    <p>Pakkumine lõppeb:</p>
                    <div class="timer-block countdownHolder countdown_please" id="countdown"></div>
                </div>
                <div class="prices">
                    <p class="old_price"><span>Vana hind: <span class="js_old_price_curs">48 €</span></span></p>
                    <p class="new_price">uus hind: <span class="js_new_price_curs">24 €</span></p>
                </div>
                <form action="" method="post">
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abdcn.pro/forms/?target=-4AAIJIALeEwAAAAAAAAAAAAT5HcUIAA"></iframe>

                    <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price" value="24.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIALeEwRucCaKAAEAAQACRxMBAAIJHQK-AQEABPQnxKkA">
                    <input type="hidden" name="goods_id" value="24">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="Goji_cream_EE_Red">
                    <input type="hidden" name="price" value="24">
                    <input type="hidden" name="old_price" value="48">
                    <input type="hidden" name="al" value="7433">
                    <input type="hidden" name="total_price_wo_shipping" value="24.0">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="0">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="EE">
                    <input type="hidden" name="shipment_vat" value="0.0">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="package_prices" value="{}">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.0">

                    <select class="inp" id="country_code_selector" name="country">
                        <option value="EE">Eesti</option>
                    </select>
                    <input class="inp" name="name" placeholder="Sinu nimi" type="text"/>
                    <input class="inp only_number" name="phone" placeholder="Sinu telefoni number" type="text"/>
                    <input class="btn_sub js_submit" type="submit" value="TELLI"/>-->
                </form>
            </div>
        </footer>
    </div>
    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7433 -->
        <script>var locale = "ee";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALeEwRucCaKAAEAAQACRxMBAAIJHQK-AQEABPQnxKkA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Gokhan Karakoc';
            var phone_hint = '+105469652091';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ee");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <title>GOJI CREAM</title>
        <meta charset="utf-8"/>
        <link href="//st.acstnst.com/content/Goji_cream_EE_Red/css/bootstrap.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Goji_cream_EE_Red/css/style.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Goji_cream_EE_Red/js/bootstrap.min.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_EE_Red/js/jquery.plugin.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_EE_Red/js/jquery.countdown.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_EE_Red/js/scripts.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="header">
        <div class="container">
            <div class="logo text-center">
                <a class="order" href="#">GOJI CREAM</a>
                <p>Noorendav kreem</p>
            </div>
            <h1 class="text-center title"> 1000 marja võim ühes kreemitilgas</h1>
            <div class="cream"></div>
            <div class="right_side">
                <h3>Küpse Goji sisaldab:</h3>
                <ul class="list-unstyled">
                    <li><span> 20 kõige tähtsamat aminohapet</span>
                    </li>
                    <li class="padding"><span> C-vitamiini (500 korda rohkem  <br/>  kui apelsinides)</span>
                    </li>
                    <li><span>E-vitamiini</span>
                    </li>
                    <li class="padding">Rauda (15 korda rohkem  <br/>  kui õunades)
                    </li>
                    <li><span>B grupi vitamiine</span>
                    </li>
                    <li class="padding"><span>Betaiini</span>
                    </li>
                </ul>
                <div class="wrap">
                    <div class="rotate">
                        <p>Selle kreemi koostis aitab  <br/>  kortsudel kaduda
                        </p>
                        <strong>14 päeva jooksul!</strong>
                    </div>
                </div>
            </div>
            <!-- ANIMATION ATTENTION MADE BY COBR3N -->
            <div class="animph">
                <div class="textimg do active">Enne</div>
                <div class="wrapimg">
                </div>
                <div class="wrapimg1">
                </div>
            </div>
            <!-- END COBREN ANIMATION WE CAN RELAX-->
        </div>
    </div>
    <div class="section_1">
        <div class="container text-center">
            <h3>Kiirusta tellimusega!<br/> Innovatiivne toode Goji cream<br/> rekordiliselt madala hinnaga!</h3>
            <div class="wrap_1"><span>50%<br/><span style="font-size: 20px; transform: rotate(-1deg); text-transform: uppercase;">Allahindlus</span></span>
            </div>
            <div class="wrap_2"><span><label>Poes 7<br/>allesjäänud <br/>eripakkumist </label></span>
            </div>
            <div class="price">
                <span class="new js_new_price_curs">24 €</span><br/>
                <span class="old js_old_price_curs">48 €</span>
            </div>
            <div class="green_btn j-btn order">telli</div>
        </div>
    </div>
    <div class="section_2">
        <div class="container">
            <h2 class="title">Goji cream-i efektid</h2>
            <ul class="list-inline">
                <li class="letter">
                    <p style="text-align: justify;">
                        On kliiniliselt tõestatud, et Goji Cream peatab rakkude vananemise ja reguleerib nende tööd.
                    </p>
                    <div class="br"></div>
                    <p style="text-align: justify;">
                        Mitmed erinevad vitamiinid ja mineraalained, samuti biotiin oma valemiga, moodustavad raske molekuli, mis suudab tungida sügavamale nahakihtidesse Goji Cream-i maksimaalse toime abil.
                    </p>
                    <div class="text-center">
                        <img alt="" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/element.jpg"/>
                    </div>
                    <p style="text-align: justify;">Goji marjades sisalduvad aminohapped on võimsad antioksüdandid, mis muudavad kreemi töö aktiivsemaks 24 tunniks. Aminohapped toimivad käsnana püüdes transepidermaalset niiskust vältides selle kadu ja tagades sügava naha niisutuse ja rekord kortsude vähendamise.
                    </p>
                    <div class="red_wrap">Hormooni vaba </div>
                </li>
                <li class="picture">
                    <ul class="list-unstyled">
                        <li>
                            <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/img_1.jpg"/>
                            </div>
                            <p style="text-align: justify;">
                                Sisaldab erinevaid UV kaitse komponente, samuti bioloogiliselt aktiivseid elemente naharakkude taastamiseks.
                            </p>
                        </li>
                        <li>
                            <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/img_2.jpg"/>
                            </div>
                            <p style="text-align: justify;">
                                See aitab ära hoida kortsude taasteket rikastades näonahka ja kaela erinevate vitamiinidega, mineraalidega ja aminohapetega.

                            </p>
                        </li>
                        <li>
                            <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/img_3.jpg"/>
                            </div>
                            <p style="text-align: justify;">
                                See aktiveerib loomuliku kollageeni tootmist sügavamates nahakihtides. Goji cream annab nahale tagasi elastsuse ja nooruse rekordiliselt lühikese aja jooksul!

                            </p>
                        </li>
                    </ul>
                    <h3 class="text-center"> Nüüd on võimalik 14 päeva jooksul 10 aastat noorem välja näha!</h3>
                    <button class="green_btn j-btn order">Ma tahan noor välja näha</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="section_3">
        <div class="container">
            <h2 class="title">Kuidas kreemi peale kanda</h2>
            <ul class="list-inline">
                <li class="step_1">
                    <div class="img_wrap">
                        <h3>1. Samm</h3>
                    </div>
                    <p> Puhasta nahk õrnalt meigist ja mustusest näopuhastaja või toonikuga. </p>
                </li>
                <li class="step_2">
                    <div class="img_wrap">
                        <h3>2. Samm</h3>
                    </div>
                    <p>Kanna kreemi puhtale nahale ümmarguste massaažiliste liigutustega vältides silmaümbrust. </p>
                </li>
                <li class="step_3">
                    <div class="img_wrap">
                        <h3>3. Samm  </h3>
                    </div>
                    <p>Sinu nahk on kohe trimmis ja sügavalt niisutatud. Kreemi mõju toime on aktiivne 24 tundi pärast kasutamist! </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="wiki_section">
        <div class="container">
            <ul class="list-inline">
                <li><img alt="" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/wiki.png"/>
                </li>
                <li class="text">
                    <h3>Miks on kortsud ohtlikud?</h3>
                    <p style="text-align: justify;">
                        Kortsud on nähtavad naha voldid, mis tulenevad liigsest näolihaste aktiivsusest, naha elastsuse ja tugevuse kadust ja muudest põhjustest. Elastsuse tagab pärisnaha kiududes olev kollageen ja selle tugevus - hüaluroonhappe molekulid kollageenkiudude vahel.
                    </p>
                    <p style="text-align: justify;">
                        Kortsud blokeerivad veresooni, kahjustades vere- ja lümfiringet nahas. See ei lase hapnikul nahk siseneda, ilmuvad uued kortsud ja vanad muutuvad sügavamaks. Kui seda ei ravita, on protsess pöördumatu.

                    </p>
                    <p style="text-align: justify;">
                        Kortsude vältimise meetodid: kreemid, maskid, Botox süstid, keemilised koorimised ja plastiline kirurgia. Viimast kolme meetodit peetakse kõige ohtlikumaks.

                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="section_4">
        <div class="container">
            <div class="left_side">
                <h2 class="title">Marjade kasulikkus</h2>
                <p style="text-align: justify;">
                    Vastupidiselt turundajate väidetele, kuivatatud marjad ei sisalda praktiliselt mingit C-vitamiini. Et saada sellisel hulgal antioksüdante, mis on ühes tavalises punases õunas, tuleb süüa peaaegu 1 kg kuivatatud marju, samas kui värsked marjad sisaldavad 500 korda rohkem C-vitamiini kui apelsinid.
                </p></div>
        </div>
    </div>
    <div class="section_5">
        <div class="container">
            <div class="right_side">
                <h2 class="title">Eksperdi hinnang</h2>
                <p style="text-align: justify;">
                    Luues meie näohooldustooteid, eelistame me ainult looduslikke koostisosi. Hendel firma kasutab uuenduslikke meetodeid, et luua populaarne näohooldustoodete sari.
                </p>
                <p style="text-align: justify;">
                    Enne kui need jõuavad riiulitele, läbib meie kosmeetika nahahaiguste kontrolli ja kliinilised uuringud. Goji Cream on osutunud väga tõhusaks kortsude vähendamisel ja naha noorendamisel.
                </p>
                <br/>
                <div class="picture">
                    <div class="before">Enne</div>
                    <div class="after">Pärast</div>
                </div>
                <ul class="list-unstyled">
                    <li><span class="mini">97 katsealust 100-st teatasid silmnähtavatest tulemustest pärast esimest kasutuskorda.</span></li>
                    <li><span class="mini">93% väikestest ja pealiskautsetest kortsudest kadusid pärast 10 päevast kreemi kasutamist.</span></li>
                </ul>
                <button class="red_btn j-btn">alusta raviga</button>
            </div>
            <div class="doctor">
                <div class="wrap">
                    <div class="rotate">
                        <h4>Johannes Kavastu</h4>
                        <p>Peaspetsialist  <br/>  nahatoodetes,  <br/>  Hendel</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="flower"></div>
    </div>
    <div class="section_6">
        <div class="container">
            <h2 class="title">Kommentaarid</h2>
        </div>
        <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                <li data-slide-to="2" data-target="#carousel-example-generic"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/50/1.png"/>
                                    <span>enne</span>
                                </div>
                                <div class="img_after round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/50/2.png"/>
                                    <span>pärast</span>
                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;">
                                    Ma olen suur Hiina meditsiini austaja ja proovin alati osta Hiina näohooldusvahendeid, kuna usun, et nende 5000 aastapikkuses ajaloos teavad nad kindlasti, kuidas ilu säilitada. Ma olen 50-aastat vana ja avastasin hiljuti, et minu näonahk on kaotanud oma elastsuse. Mul on väga silmapaistev kulmukorts ja kortsud silmade ümber.
                                </p>
                                <br/>
                                <p style="text-align: justify;">
                                    Minu kosmeetik soovitas mulle Goji Cream-i ning ütles, et see on innovatiivne toode, mis kasutab samaaegselt traditsioonilisi Hiina noorusallikaid- goji marju. Pärast kahenädalast kasutamist, muutis see mu naha trimmimaks ja kortsud silenesid. Nüüd kasutan ma seda iga päev.
                                </p></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/50/3.png"/>
                                    <span>enne</span>
                                </div>
                                <div class="img_after round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/50/4.png"/>
                                    <span>pärast</span>
                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;">
                                    Ma armastan kõike, mis on uus, seega ei saanud ma ilma jääda sellisest uudsusest nagu Goji Cream, innovatiivne näo vananemisvastane hooldus. Ma olen kuldsetes 50-dates, aga mu nägu näeb tegelikult päris hea välja, kuna hoolitsen selle eest hästi.
                                </p><br/>
                                <p style="text-align: justify;">
                                    Mis ma oskan öelda, selle kreemi koostis on väga hea- õrn, ei kleepu, imendub kiiresti ja pehmendab kortse kiiresti nagu tuline triiklaud. Tulemused olid nähtavada paari nädala jooksul. See sobib mu nahale perfektselt.
                                </p></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <ul class="list-inline">
                            <li class="photo">
                                <div class="img_before round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/50/5.png"/>
                                    <span>enne </span>
                                </div>
                                <div class="img_after round">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_EE_Red/img/50/6.png"/>
                                    <span>pärast</span>
                                </div>
                            </li>
                            <li class="text">
                                <p style="text-align: justify;">
                                    Ma kindlasti soovitan Goji Cream-i kõigile naistele igas eas, see tõesti toimib. Goji ise on väga huvitav mari-nendes marjades on palju maagiat, pole ime, et need võitlevad ka kortsude vastu.
                                </p><br/>
                                <p style="text-align: justify;">
                                    Minu õpilase ema andis selle kreemi mulle kingituseks. Inimesed on kinkinud mulle erinevaid asju, aga ma pole kunagi varem näinud selliseid tulemusi- nahk taastas kiiresti oma toonsuse ja väikesed kortsud hakkasid silenema. Ma olen seda nüüd 2 nädalat kasutanud ja tulemused on silmaga näha. Ma usun, et varsti hakkan võitlema ka tõsiste kortsudega.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <div class="container">
                <a class="left carousel-control" data-slide="ÖNCƏv" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('ÖNCƏv');" role="button"></a>
                <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container text-center">
            <h2>Ainult täna saad sa tellida Goji cream-i 50% soodsamalt!  Goji cream 50% soodsamalt!</h2>
            <ul class="list-inline">
                <li class="timmer_section">
                    <h3 class="old_price">Vana hind: <span class="js_old_price_curs">48 €</span></h3>
                    <h3 class="new_price">uus hind:<br/><span class="js_new_price_curs">24 €</span></h3>
                    <ul class="list-unstyled timme_wrap">
                        <li class="left">Pakkumine lõppeb:</li>
                        <li class="timmer">
                        </li>
                        <li class="text">
                            <ul class="list-inline">
                                <li>tundi</li>
                                <li>minutit</li>
                                <li>sekundit</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="form_wrap">
                    <h3>KIIRUSTA  <br/>  TOOTE KOGUS  <br/>  ON PIIRATUD!</h3>
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abdcn.pro/forms/?target=-4AAIJIALeEwAAAAAAAAAAAAT5HcUIAA"></iframe>

                        <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="24.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIALeEwRucCaKAAEAAQACRxMBAAIJHQK-AQEABPQnxKkA">
                        <input type="hidden" name="goods_id" value="24">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Goji_cream_EE_Red">
                        <input type="hidden" name="price" value="24">
                        <input type="hidden" name="old_price" value="48">
                        <input type="hidden" name="al" value="7433">
                        <input type="hidden" name="total_price_wo_shipping" value="24.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="EE">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <select id="country_code_selector" name="country">
                            <option value="EE">Eesti</option>
                        </select>
                        <input name="name" placeholder="Sinu nimi" type="text"/>
                        <input class="only_number" name="phone" placeholder="Sinu telefoni number" type="text"/>
                        <button class="green_btn js_submit" name="button" type="submit">telli</button>-->
                    </form>
                </li>
            </ul>
        </div>
    </div>
    </body>
    </html>
<?php } ?>