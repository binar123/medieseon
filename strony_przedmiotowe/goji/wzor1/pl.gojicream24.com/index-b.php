<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 9520 -->
        <script>var locale = "pl";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAISFQRUNgGMAAEAAQACfRQBAAIwJQIGAQEABMzmOPwA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":200,"old_price":400,"shipment_price":0},"3":{"price":400,"old_price":800,"shipment_price":0},"5":{"price":600,"old_price":1200,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Renata Czapiewska';
            var phone_hint = '+48519318890';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("pl");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title> Goji Cream </title>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,600,700,300&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <meta content="yes" name="apple-mobile-web-app-capable"/>
        <link href="//st.acstnst.com/content/GojiCream_PL_animated/mobile/css/style2.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/js/jquery.countdown.js"></script>
        <script src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/js/jcarousel.js"></script>
        <script src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/js/slider.js"></script>
        <script src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/js/common.js"></script>
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--[if IE 7]> <html class="ie7" lang="en"> <![endif]-->
        <!--[if IE 8]> <html class="ie8" lang="en"> <![endif]-->
        <!--[if gte IE 9]>
        <style type="text/css">
            .gradient {
                filter: none;
            }
        </style>
        <![endif]--></head><body><p>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    $(".js_errorMessage2").remove();
                    $(".js_errorMessage").remove();
                    var errors = 0,
                        form = $(this).closest('form'),
                        name = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        countryp = form.find('[id="country_code_selector"]').val(),
                        namep = name.val(),
                        phonep = phone.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;
                    if(name.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(name, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(name, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        var mas={};
                        form.find('input,textatea,select').each(function(){
                            mas[$(this).attr('name')]=$(this).val();
                        });
                        $.post('/order/create_temporary/', mas);
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                        return false;
                    }
                });
            });
        </script>

    </p>
    <div class="dbody">
        <header>
            <div class="wrapper">
                <img alt="pack" id="bg" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/header.png"/>
                <img alt="pack" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/61219511.png"/>
                <span>Krem odmładzający</span>
                <p> Jagody Goji zawierają: </p>
                <ul>
                    <li>20 najbardziej wartościowych aminokwasów</li>
                    <li>witaminę C (500 razy więcej niż pomarańcza)</li>
                    <li>witaminę E</li>
                    <li>żelazo (15 razy więcej niż jabłko)</li>
                    <li>witaminy z grupy B</li>
                    <li>betainę</li>
                </ul>

                <div class="btn">
                    <a class="pre_toform" href="">  zamów </a>
                </div>

            </div>
        </header>
        <aside>
            <div class="wrapper">
                <h3>
                    Zamów jak najszybciej! Nowoczesny produkt Goji cream w rekordowo niskiej cenie
                </h3>
                <div class="prices">
                    <p class="new_price">200 zł </p>
                    <p class="old_price"><span>400 zł </span></p>
                </div>
                <div class="sale">
                    <p>50% zniżki</p>
                </div>
                <p>w magazynie 7  pozostało w promocji </p>
            </div>
        </aside>
        <section class="extra">
            <div class="wrapper">
                <h3>Skuteczność Goji cream</h3>
                <ul>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/img1.png"/>
                        </div>
                        <div>
                            <p>
                                Zawiera filtry UV, oraz składniki aktywne, które regenerują komórki skóry.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/img2.png"/>
                        </div>
                        <div>
                            <p>
                                Chroni przed nawrotem zmarszczek, wzbogacając skórę twarzy i szyi o liczne witaminy, minerały i aminokwasy.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/img3.png"/>
                        </div>
                        <div>
                            <p>
                                Aktywuje produkcję naturalnego kolagenu w głębszych warstwach skóry, z Goji cream skóra odzyska elastyczność i młodość w rekordowo krótkim czasie.
                            </p>
                        </div>
                    </li>
                </ul>
                <h3>Odejmij sobie 10 lat<br/>w 14 dni, teraz to możliwe! </h3>

                <div class="btn_big">
                    <a class="pre_toform" href="">  Chcę wyglądać młodo</a>
                </div>

            </div>
        </section>
        <section class="third_scr">
            <div class="wrapper">
                <h3>OPINIA EKSPERTA </h3>
                <p>Podczas tworzenia naszych produktów do pielęgnacji twarzy kładziemy nacisk na naturalne składniki. Firma Hendel użyła innowacyjnych metod, aby stworzyć popularną linię do pielęgnacji twarzy. </p>
                <img alt="bef_aft" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/img_41.png"/>
                <ul>
                    <li>97 na 100 ludzi wyraziło zadowolenie z widocznych efektów już po pierwszym użyciu. </li>
                    <li>93% małych i ledwo widocznych zmarszczek znika po 10 dniach.</li>
                </ul>

                <div class="red_btn">
                    <a class="pre_toform" href=""> stosowania kremu  </a>
                </div>

            </div>
        </section>
        <section class="third_and_half">
            <div class="wrapper">
                <h3>komentarze</h3>
                <div class="sliderslider">
                    <div class="slider jcarousel" data-jcarousel="true">
                        <div>
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/ava1.png"/></div>
                                <p>Polecam Goji Cream z całą stanowczością, wszystkim kobietom, w każdym wieku, to naprawdę działa. Goji to interesujący produkt, te jagody zawierają coś z magii. Nie mam wątpliwości, że dadzą radę zmarszczkom. </p>
                                <p>Mama mojej uczennicy dała mi to w prezencie. Ludzie wręczają mi różne podarunki, ale nigdy nie widziałam jeszcze czegoś podobnego - skra błyskawicznie odzyskała właściwe napięcie i kurze łapki raz zmarszczki mimiczne zaczęły znikać. Używałam tego dwa tygodnie a rezultaty są doskonale widoczne. Myślę, że zwalczyłby nawet głębokie zmarszczki. </p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/ava2.png"/></div>
                                <p>Jestem zwolennikiem Chińskiej medycyny i zazwyczaj próbuję kupować chińskie kosmetyki do pielęgnacji twarzy, przez 5000 lat z pewnością nauczyli się jak dobrze chronić piękno. Mam 50 lat i niedawno zauważyłam, ze skóra mojej twarzy straciła swoją napiętość i elastyczność. Zauważyłam głębokie zmarszczki i kurze łapki wokół oczu. </p>
                                <p>Moja kosmetyczka poleciła mi Goji Cream, powiedziała, że to innowacyjny produkt, który wykorzystuje zdobycze chińskiej medycyny i zawiera jagody goji. Po nakładaniu przez dwa tygodnie, moja skóra wygładziła się, zmarszczki są płytsze, używam codziennie. </p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide"><img alt="girl" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/ava3.png"/></div>
                                <p>Uwielbiam nowości, nie mogłam więc nie wykorzystać takiej nowinki jak Goji Cream, - innowacyjny środek przeciwstarzeniowy. Mam prawie 60 lat i moja twarz wygląda całkiem nieźle, ale tylko dlatego, że nią dbam. </p>
                                <p>Co mogę powiedzieć, konsystencja kremu jest przyjemnie lekka, nie klei się, szybko się wchłania i wygładza zmarszczki, jak gorące żelazko, działa raczej szybko, efekty stają się widoczne po kilku tygodniach. Do mojej skóry pasuje idealnie </p>
                            </div>
                        </div>
                    </div>
                    <a class="jcarousel-prev" data-jcarouselcontrol="true" href="#"></a>
                    <a class="jcarousel-next" data-jcarouselcontrol="true" href="#"></a>
                </div>
            </div>
        </section>
        <footer class="form">
            <div class="wrapper clearfix" id="form">
                <img alt="foot" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/img/foot_bg.png"/>
                <h3>Tylko dziś możesz zamówić Goji cream i dostać 50% rabatu! Goji cream z 50% zniżką! </h3>
                <div class="prices">
                    <p class="old_price"><span>Stara cena: 400 zł </span></p>
                    <p class="new_price"> Nowa cena: 200 zł</p>
                </div>
                <form action="" id="order_form" method="post">
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abcdg.pro/forms/?target=-4AAIJIAISFQAAAAAAAAAAAARQ0GytAA"></iframe>

                    <div class="toform"></div>

                </form>
                <div class="counter_wrap">
                    <p>Oferta kończy się:</p>
                    <div class="timer-block countdownHolder countdown_please" id="countdown"></div>
                </div>
            </div>
        </footer>
    </div>
    <!-- hidden   -->
    <!-- hiddenwindow -->

    <link href="//st.acstnst.com/content/GojiCream_PL_animated/mobile/css/styleSecond.css" rel="stylesheet" type="text/css"/>
    <div class="hidden-window">
        <div class="new-header_goji"></div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">
                                Płatność <b>przy odbiorze </b>
                            </div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">
                                                <img alt="arrow" class="zarrr" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/images/Zarr.png"/>
                                                <img alt="alt1" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/images/dtc1.png"/>
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">
                                                <img alt="alt1" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/images/dtc2.png"/>
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">
                                                <img alt="arrow" class="zarrrLeft" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/images/Zarr.png"/>
                                                <img alt="alt1" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/images/dtc3.png"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">Zamówienie</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Wysyłka</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Dostawa i  <span>płatność</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="new-logo-descr" style='text-align: center;background: #fff; padding: 10px 0; margin-top:10px; margin-bottom: 10px;    color: #000; font-size: 24px;'>
                                                               In Zusammenarbeit mit<br>
                                                               <img src="images/nmnm.png" alt="logo2">
                                                           </div>        -->
                        <div class="select-block">
                            <div class="text-center selecttext">Uczciwy zakup</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 opakowanie</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 opakowania</option>
                                <option data-slide-index="2" value="5">3+2 opakowania</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs"><img alt="zt1" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/images/p1.png"/> </div>
                                            <div class="zHeader onepack"><b>1</b> opakowanie </div><br/>
                                        </div>
                                        <div class="pack_descr"> Widoczne odmłodzenie już po miesiącu stosowania Kurs dla młodzieży!</div>
                                        <div class="dtable" style="width: 80%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Cena:</div>
                                            <div class="dtable-cell"><div class="js_pack_price"><text class="js-pp-new"> 200 </text></div></div>
                                            <div class="dtable-cell currencycell"><div class="pack_old_price"><i></i><text class="js-pp-old">  400  </text></div>zł</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile">prezent</div>
                                                    <img alt="zt1" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/images/p2.png"/>
                                                </div>
                                                <div class="zHeader"><b>2</b> opakowania  <br/> <span class="dib zplus">prezent</span></div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">Efekt liftingu i wygładzając najgłębsze zmarszczki w ciągu zaledwie trzech miesięcy. Gwarantowane rezultaty!
                                        </div>
                                        <div class="dtable" style="width: 80%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Cena:</div>
                                            <div class="dtable-cell"><div class="js_pack_price"><text class="js-pp-new">  400  </text> </div></div>
                                            <div class="dtable-cell currencycell"><div class="pack_old_price"><i></i><text class="js-pp-old">  800  </text></div>zł</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <div class="giftmobile">podarunki</div>
                                                <img alt="zt1" src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/images/p3.png"/>
                                            </div>
                                            <div class="zHeader"><b>3</b> opakowania  <br/> <span class="dib zplus sec">podarunki</span> </div>
                                        </div>
                                        <div class="pack_descr">Maksymalne odmłodzenie! Pełny pół roczny kurs nie tylko pozbędzie się zmian związanych z wiekiem, ale również zapobiegnie ich ponownego wystąpienia.
                                        </div>
                                        <div class="dtable" style="width: 80%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Cena:</div>
                                            <div class="dtable-cell"><div class="js_pack_price"><text class="js-pp-new">  600  </text></div></div>
                                            <div class="dtable-cell currencycell"><div class="pack_old_price"><i></i><text class="js-pp-old">  1200  </text></div>zł</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager">
                            <a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a>
                            <a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a>
                            <a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                        </div>
                        <form action="/order/create/" class="js_scrollForm" method="post"><input type="hidden" name="total_price" value="200.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAISFQRUNgGMAAEAAQACfRQBAAIwJQIGAQEABMzmOPwA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="GojiCream_PL_animated">
                            <input type="hidden" name="price" value="200">
                            <input type="hidden" name="old_price" value="400">
                            <input type="hidden" name="total_price_wo_shipping" value="200.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 200, 'old_price': 400, 'shipment_price': 0}, u'3': {'price': 400, 'old_price': 800, 'shipment_price': 0}, u'5': {'price': 600, 'old_price': 1200, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="zł">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="PL">
                            <input type="hidden" name="shipment_vat" value="0.23">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.23">

                            <div class="formHeader"><span>Wprowadź dane</span>
                                <br/> do złożenia zamówienia
                            </div>
                            <select class="select inp" id="country_code_selector" name="country_code" style="display: none;">
                                <option value="PL"> Polska </option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 opakowanie</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 opakowania</option>
                                <option data-slide-index="2" value="5">3+2 opakowania</option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Namen eintragen" style="display: none;" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Telefon nummer eintragen" style="display: none;" type="text" value=""/>
                            <input class="inp j-inp" name="address" placeholder="Adresse" type="text"/>
                            <div class="text-center"></div>
                            <!--div class="text-center totalpriceForm shipmentt">+Versand: <b class='js_delivery_price'>0</b> zł</div-->
                            <div class="text-center totalpriceForm">Suma: <span class="js_total_price js_full_price">200 </span> zł</div>
                            <div class="zbtn transitionmyl js_submit"> zamów </div>
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Gwarantujemy:</div>
                            <ul>
                                <li>Jakość  <b>100%</b> </li>
                                <li><b>Kontrola</b>  produktu przy otrzymaniu</li>
                                <li><b>Bezpieczeństwo</b> Państwa danych</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/js/jquery.bxslider.min.js"></script>
    <script src="//st.acstnst.com/content/GojiCream_PL_animated/mobile/js/secondPage.js"></script>


    </body></html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 9520 -->
        <script>var locale = "pl";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAISFQREPyWKAAEAAQACfRQBAAIwJQIGAQEABPqtK9EA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":200,"old_price":400,"shipment_price":0},"3":{"price":400,"old_price":800,"shipment_price":0},"5":{"price":600,"old_price":1200,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Renata Czapiewska';
            var phone_hint = '+48519318890';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("pl");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">

        <script type="text/javascript">
            $(document).ready(function() {
                $('div.order, button.order, a.order').click(function(){
                    $("html, body").animate({scrollTop: $("form").offset().top-300}, 2000);
                    return false;
                });
            });
        </script>



        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <title> Goji Cream </title>
        <meta charset="utf-8"/>
        <link href="//st.acstnst.com/content/GojiCream_PL_animated/css/bootstrap.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/GojiCream_PL_animated/css/style2.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/GojiCream_PL_animated/js/bootstrap.min.js"></script>
        <script src="//st.acstnst.com/content/GojiCream_PL_animated/js/scripts.js"></script>

        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/only_number.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jquery.msgerror.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jpcheckpref.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jpgetpref.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                setInterval(function(){

                    $(".wrapimg1").animate({
                        height: "468px"
                    }, 3500,  function(){

                        $(".wrapimg1").animate({
                            height: "0px"
                        }, 1000);
                    });

                    setTimeout(function(){
                        $('.textimg.do').html("Po");
                    }, 3100);

                    setTimeout(function(){
                        $('.textimg.do').hide();
                        $('.textimg.do').html("Przed");
                        $('.textimg.do').fadeIn();
                    }, 4500 );
                }, 5000);
            });
        </script>
    </head>
    <body>
    <div class="wrwr1">
        <div class="header">
            <div class="container">
                <div class="logo text-center">
                    <a class="order" href="#">  Goji Cream  </a>
                    <p>Krem odmładzający </p>
                    <!--p> for women aged 50 and over </p-->
                </div>
                <h1 class="text-center title">  Moc 1000 jagód w jednej kropli kremu</h1>
                <div class="cream"></div>
                <div class="right_side">
                    <h3> Jagody Goji zawierają </h3>
                    <ul class="list-unstyled">
                        <li><span> 20 najbardziej wartościowych aminokwasów </span></li>
                        <li class="padding"><span>witaminę C (500 razy więcej  <br/> niż pomarańcza) </span></li>
                        <li><span>witaminę E </span></li>
                        <li class="padding"><span>żelazo (15 razy więcej<br/> niż jabłko)  </span></li>
                        <li><span> witaminy z grupy B </span></li>
                        <li class="padding"><span> betainę </span></li>
                    </ul>
                    <div class="wrap">
                        <div class="rotate">
                            <p>Specjalna formuła kremu sprawi, <br/> że zmarszczki znikną  </p>
                            <strong>  w 14 dni!  </strong>
                        </div>
                    </div>
                </div>
                <div class="animph">
                    <div class="textimg do active" style="display: block;">Po</div>
                    <div class="wrapimg">
                    </div>
                    <div class="wrapimg1" style="height: 0px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="section_1">
            <div class="container text-center">
                <h3>Zamów jak najszybciej!<br/> Nowoczesny produkt Goji cream <br/> w rekordowo niskiej cenie   </h3>
                <div class="wrap_1"><span>50%<br/> zniżki</span></div>
                <div class="wrap_2"><span> w magazynie <br/><label>7</label></span><p> pozostało <br/> w promocji </p></div>
                <div class="price">
                    <span class="new js_new_price_curs">  200 zł  </span>
                    <span class="old js_old_price_curs">  400 zł  </span>
                </div>

                <div class="green_btn jsOpenWindow order"> zamów </div>

            </div>
        </div>
        <div class="section_2">
            <div class="container">
                <h2 class="title">Skuteczność Goji cream </h2>
                <ul class="list-inline">
                    <li class="letter">
                        <p style="text-align: justify;"> Badania kliniczne dowiodły, że Goji cream zatrzymuje procesy starzenia komórek i reguluję ich działanie.  </p>
                        <div class="br"></div>
                        <p style="text-align: justify;"> Duża dawka witamin i minerałów oraz biotyna tworzą mocne cząsteczki, które wnikają do głębokich warstw skór, dzięki czemu Goji Cream jest maksymalnie skuteczny.  </p>
                        <div class="text-center">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_PL_animated/img/element.jpg"/>
                        </div>
                        <p style="text-align: justify;">  Aminokwasy zawarte w jagodach Goji wykazują bardzo silne działanie przeciwutleniające, dzięki czemu krem działa przez 24 godziny na dobę. Aminokwasy działają jak gąbka, zapobiegając utracie substancji nawilżających przez skórę i zapewniając głębokie nawilżenie i rekordową redukcję zmarszczek.</p>
                        <div class="red_wrap"> Bez hormonów </div>
                    </li>
                    <li class="picture">
                        <ul class="list-unstyled">
                            <li><div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/GojiCream_PL_animated/img/img_1.jpg"/></div><p style="text-align: justify;">Zawiera filtry UV, oraz składniki aktywne, które regenerują komórki skóry. </p></li>
                            <li><div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/GojiCream_PL_animated/img/img_2.jpg"/></div><p style="text-align: justify;"> Chroni przed nawrotem zmarszczek, wzbogacając skórę twarzy i szyi o liczne witaminy, minerały i aminokwasy.  </p></li>
                            <li><div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/GojiCream_PL_animated/img/img_3.jpg"/></div><p style="text-align: justify;"> Aktywuje produkcję naturalnego kolagenu w głębszych warstwach skóry, z Goji cream skóra odzyska elastyczność i młodość w rekordowo krótkim czasie.  </p></li>
                        </ul>
                        <h3 class="text-center"> Odejmij sobie 10 lat  <br/> w 14 dni, teraz to możliwe!  </h3>

                        <button class="green_btn jsOpenWindow order"> Chcę wyglądać młodo</button>

                    </li>
                </ul>
            </div>
        </div>
        <div class="section_3">
            <div class="container">
                <h2 class="title"> Jak stosować </h2>
                <ul class="list-inline">
                    <li class="step_1">
                        <div class="img_wrap">
                            <h3> Krok 1 </h3>
                        </div>
                        <p> Delikatnie oczyść skórę twarzy z makijażu i zanieczyszczeń mleczkiem, lub tonikiem. </p>
                    </li>
                    <li class="step_2">
                        <div class="img_wrap">
                            <h3> Krok 2 </h3>
                        </div>
                        <p> Nałóż krem, wmasowując go okrężnymi ruchami, omijając okolicę oczu. </p>
                    </li>
                    <li class="step_3">
                        <div class="img_wrap">
                            <h3>  Krok 3 </h3>
                        </div>
                        <p> Natychmiastowe wygładzenie i nawilżenie Twojej skóry. Ten krem aktywnie działa przez 24 godziny od aplikacji! </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wiki_section">
            <div class="container">
                <ul class="list-inline">
                    <li><img alt="" src="//st.acstnst.com/content/GojiCream_PL_animated/img/wiki.jpg"/></li>
                    <li class="text">
                        <h3> Jakie są zagrożenia związane ze zmarszczkami? </h3>
                        <p style="text-align: justify;"> Zmarszczki to widoczne zagniecenia skóry wywołane przez ruchy mimiczne mięśni twarzy, utratę elastyczności i jędrności oraz inne przyczyny. Za elastyczność skóry odpowiadają włókna kolagenowe skóry, a za jej jędrność cząsteczki kwasu hialuronowego znajdujące się między włóknami kolagenowymi. </p>
                        <p style="text-align: justify;">  Zmarszczki blokują naczynia krwionośne pogarszając przepływ krwi i limfy w skórze. Z tego powodu tlen nie może wnikać w skórę, co powoduje pojawianie się nowych zmarszczek i pogłębianie starych. Nieleczone, zmarszczki stają się nieodwracalne. </p>
                        <p style="text-align: justify;">  Sposoby zapobiegania zmarszczkom: mleczka, maseczki, zastrzyki z botoksu, peelingi chemiczne, chirurgia plastyczna. Ostatnie trzy z wymienionych metod uważa się za najbardziej niebezpieczne. </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_4">
            <div class="container">
                <div class="left_side">
                    <h2 class="title">KORZYŚCI Z JAGÓD</h2>
                    <p style="text-align: justify;">Wbrew twierdzeniom sprzedawców, suszone jagody praktycznie nie zawierają witaminy C. Aby osiągnąć taką ilość antyoksydantów jaka zawarta jest w jednym czerwonym jabłku musimy zjeść prawie kilogram jagód, podczas gdy świeże jagody zawierają 500 razy więcej witaminy C niż pomarańcze. </p>
                </div>
            </div>
        </div>
        <div class="section_5">
            <div class="container">
                <div class="right_side">
                    <h2 class="title"> OPINIA EKSPERTA </h2>
                    <p style="text-align: justify;">Podczas tworzenia naszych produktów do pielęgnacji twarzy kładziemy nacisk na naturalne składniki. Firma Hendel użyła innowacyjnych metod, aby stworzyć popularną linię do pielęgnacji twarzy. </p>
                    <p style="text-align: justify;"> Nasze kosmetyki przechodzą przez skrupulatne testy dermatologiczne i kliniczne. Badania potwierdziły, że Goji cream jest naprawdę skuteczny w walce ze zmarszczkami i w odmładzaniu skóry. </p>
                    <br/>
                    <div class="picture">
                        <div class="before"> Przed </div>
                        <div class="after"> Po </div>
                    </div>
                    <ul class="list-unstyled">
                        <li> 97 na 100 ludzi wyraziło zadowolenie z widocznych efektów już po pierwszym użyciu.  </li>
                        <li> 93% małych i ledwo widocznych zmarszczek znika po 10 dniach.</li>
                    </ul>

                    <button class="red_btn jsOpenWindow">stosowania kremu </button>


                </div>
                <div class="doctor">
                    <div class="wrap">
                        <div class="rotate">
                            <h4> Jan Malczewski </h4>
                            <p> Główny Specjalista <br/> od produktów do pielęgnacji skóry, <br/> Hendel </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flower"></div>
        </div>
        <div class="section_6">
            <div class="container">
                <h2 class="title"> komentarze</h2>
            </div>
            <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="2" data-target="#carousel-example-generic"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCream_PL_animated/img/50/1.png"/>
                                        <span>  przed </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCream_PL_animated/img/50/2.png"/>
                                        <span> po </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;"> Jestem zwolennikiem Chińskiej medycyny i zazwyczaj próbuję kupować chińskie kosmetyki do pielęgnacji twarzy, przez 5000 lat z pewnością nauczyli się jak dobrze chronić piękno. Mam 50 lat i niedawno zauważyłam, ze skóra mojej twarzy straciła swoją napiętość i elastyczność. Zauważyłam głębokie zmarszczki i kurze łapki wokół oczu. </p>
                                    <p style="text-align: justify;">Moja kosmetyczka poleciła mi Goji Cream, powiedziała, że to innowacyjny produkt, który wykorzystuje zdobycze chińskiej medycyny i zawiera jagody goji. Po nakładaniu przez dwa tygodnie, moja skóra wygładziła się, zmarszczki są płytsze, używam codziennie. </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCream_PL_animated/img/50/3.png"/>
                                        <span> przed  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCream_PL_animated/img/50/4.png"/>
                                        <span> po </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;"> Uwielbiam nowości, nie mogłam więc nie wykorzystać takiej nowinki jak Goji Cream, - innowacyjny środek przeciwstarzeniowy. Mam prawie 60 lat i moja twarz wygląda całkiem nieźle, ale tylko dlatego, że nią dbam.  </p>
                                    <p style="text-align: justify;"> Co mogę powiedzieć, konsystencja kremu jest przyjemnie lekka, nie klei się, szybko się wchłania i wygładza zmarszczki, jak gorące żelazko, działa raczej szybko, efekty stają się widoczne po kilku tygodniach. Do mojej skóry pasuje idealnie </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCream_PL_animated/img/50/5.png"/>
                                        <span>  przed </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCream_PL_animated/img/50/6.png"/>
                                        <span>po </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;"> Polecam Goji Cream z całą stanowczością, wszystkim kobietom, w każdym wieku, to naprawdę działa. Goji to interesujący produkt, te jagody zawierają coś z magii. Nie mam wątpliwości, że dadzą radę zmarszczkom. </p>
                                    <p style="text-align: justify;">Mama mojej uczennicy dała mi to w prezencie. Ludzie wręczają mi różne podarunki, ale nigdy nie widziałam jeszcze czegoś podobnego - skra błyskawicznie odzyskała właściwe napięcie i kurze łapki raz zmarszczki mimiczne zaczęły znikać. Używałam tego dwa tygodnie a rezultaty są doskonale widoczne. Myślę, że zwalczyłby nawet głębokie zmarszczki.  </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Controls -->
                <div class="container">
                    <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('prev');" role="button"></a>
                    <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container text-center">
                <h2>Tylko dziś możesz zamówić Goji cream i dostać 50% rabatu!  <br/> Goji cream z 50% zniżką!  </h2>
                <ul class="list-inline">
                    <li class="timmer_section">
                        <h3 class="old_price"> Stara cena:  <span class="js_old_price_curs"> 400 zł </span></h3>
                        <h3 class="new_price"> Nowa cena:  <br/><span class="js_new_price_curs"> 200 zł </span></h3>
                        <ul class="list-unstyled timme_wrap">
                            <li class="left"> Oferta kończy się:  </li>
                            <li class="timmer">
                                <ul class="list-inline count">
                                    <li class="hours">01</li>
                                    <li class="minutes">01</li>
                                    <li class="seconds">01</li>
                                </ul>
                            </li>
                            <li class="text">
                                <ul class="list-inline">
                                    <li> godz </li>
                                    <li>min</li>
                                    <li> sek</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="form_wrap">
                        <h3> POŚPIESZ SIĘ <br/>  ILOŚĆ OPAKOWAŃ  <br/> JEST OGRANICZONA! </h3>

                        <style>
                            .footer .form_wrap{
                                padding-bottom: 200px;
                            }
                            </style>
                        <form action="" method="post">

                            <div class="select ">
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abcdg.pro/forms/?target=-4AAIJIAISFQAAAAAAAAAAAARQ0GytAA"></iframe>

</div>

                            </div>
                            <div class="toform"></div>

                        </form>

                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- hidden -->
    <!-- hiddenwindow -->

    <link href="//st.acstnst.com/content/GojiCream_PL_animated/css/styleSecond.css" rel="stylesheet"/>
    <script src="//st.acstnst.com/content/GojiCream_PL_animated/js/secondPage.js"></script>
    <div class="hidden-window">
        <div class="h-headerj">
            <div class="containerz">
                <div class="left-side-header"></div>
                <div class="h-text">
                    zamów  <br/> <span>już teraz!</span>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Płatność <br/>
                                <b>przy odbiorze </b>
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img alt="alt1" src="//st.acstnst.com/content/GojiCream_PL_animated/images/dtc1.png"/></div>
                                    <div class="dtable-cell">Zamówienie</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img alt="alt1" src="//st.acstnst.com/content/GojiCream_PL_animated/images/dtc2.png"/></div>
                                    <div class="dtable-cell">Wysyłka</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img alt="alt1" src="//st.acstnst.com/content/GojiCream_PL_animated/images/dtc3.png"/></div>
                                    <div class="dtable-cell">Dostawa  i<span> płatność</span></div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="new-logo-descr" style='text-align: center;background: #fff; padding: 10px 0; margin-top:10px;    color: #000; font-size: 24px;'>
                                                In Zusammenarbeit mit<br>
                                                <img src="images/nmnm.png" alt="logo2">
                                            </div> -->
                        <div class="printbg">Uczciwy zakup</div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAISFQAAAAAAAAAAAARQ0GytAA"></iframe>

                            <!--<input type="hidden" name="total_price" value="200.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAISFQREPyWKAAEAAQACfRQBAAIwJQIGAQEABPqtK9EA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="GojiCream_PL_animated">
                            <input type="hidden" name="price" value="200">
                            <input type="hidden" name="old_price" value="400">
                            <input type="hidden" name="total_price_wo_shipping" value="200.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 200, 'old_price': 400, 'shipment_price': 0}, u'3': {'price': 400, 'old_price': 800, 'shipment_price': 0}, u'5': {'price': 600, 'old_price': 1200, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="zł">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="PL">
                            <input type="hidden" name="shipment_vat" value="0.23">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.23">

                            <div class="formHeader">
                                <span>Wprowadź dane </span> <br/> do złożenia zamówienia
                            </div>
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="PL"> Polska </option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1 opakowanie</option>
                                <option selected="selected" value="3">2+1 opakowania</option>
                                <option value="5">3+2 opakowania</option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Imię i nazwisko" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Telefon" type="text" value=""/>
                            <input class="inp" name="address" placeholder="Adres" type="text"/>
                            <!--<textarea name="comment" placeholder="Kommentar zu Bestellung"></textarea>-->
                            <div class="text-center totalpriceForm">Suma: <span class="js_total_price js_full_price">200 </span> zł</div>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Gwarantujemy:</div>
                            <ul>
                                <li>Jakość <b>100%</b> </li>
                                <li><b>Kontrola</b> produktu przy otrzymaniu</li>
                                <li><b>Bezpieczeństwo</b> Państwa danych</li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1 active">
                            <div class="zDiscount">
                                <b>Uwaga</b>: Jest <br/>
                                <span>50% zniżki</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader"><b>1</b> opakowanie </div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> zł-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Cena:</div>
                                            <div class="dtable-cell"> <b> <span class="js-pp-new">  200  </span> zł</b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"> <span class="dib old-pricedecoration"><i></i>  400  </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Stara <br/>cena</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst ">Wysyłka:</div>
                                            <div class="dtable-cell"> <b>  0   zł</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Suma::</div>
                                            <div class="dtable-cell prtotal"> <b>  200  </b> zł</div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="1"></div>
                                <div class="img-wrapp"><img alt="z1" src="//st.acstnst.com/content/GojiCream_PL_animated/images/zit11.png"/></div>
                                <div class="zHeader"><span> </span></div>
                                <text>Widoczne odmłodzenie już po miesiącu stosowania Kurs dla młodzieży!</text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 ">
                            <div class="zstick"></div>
                            <div class="zDiscount">
                                <b>Uwaga</b>: Jest <br/>
                                <span>50% zniżki</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> opakowania <span class="dib zplus">prezent</span></div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> zł-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Cena:</div>
                                            <div class="dtable-cell"> <b>  400   zł</b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"> <span class="dib old-pricedecoration"><i></i>  800  </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Stara <br/>cena</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst ">Wysyłka:</div>
                                            <div class="dtable-cell"> <b>  0  zł</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Suma::</div>
                                            <div class="dtable-cell prtotal"> <b>  400  </b> zł</div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3"></div>
                                <div class="img-wrapp"><div class="gift">prezent</div><img alt="z1" src="//st.acstnst.com/content/GojiCream_PL_animated/images/zit22.png"/></div>
                                <div class="zHeader"><span> </span></div>
                                <text> Efekt liftingu i wygładzając najgłębsze zmarszczki w ciągu zaledwie trzech miesięcy. Gwarantowane rezultaty!
                                </text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Uwaga</b>: Jest <br/>
                                <span>50% zniżki</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> opakowania <span class="dib zplus sec">podarunki</span></div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> zł-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Cena:</div>
                                            <div class="dtable-cell"> <b>  600  zł</b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"> <span class="dib old-pricedecoration"><i></i>  1200  </span>
                                                    </div>
                                                    <div class="dtable-cell text-left"> <span class="old-pr-descr">Stara <br/>cena</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst ">Wysyłka:</div>
                                            <div class="dtable-cell"> <b>  0   zł</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Suma::</div>
                                            <div class="dtable-cell prtotal"> <b>  600  </b> zł</div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5"></div>
                                <div class="img-wrapp"><div class="gift">podarunki</div><img alt="z1" src="//st.acstnst.com/content/GojiCream_PL_animated/images/zit33.png"/></div>
                                <div class="zHeader sm"><span> </span></div>
                                <text>Maksymalne odmłodzenie! Pełny pół roczny kurs nie tylko pozbędzie się zmian związanych z wiekiem, ale również zapobiegnie ich ponownego wystąpienia.
                                </text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- endhidden -->

    </body>
    </html>
<?php } ?>