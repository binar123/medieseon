<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 10963 -->
        <script>var locale = "bg";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAI_FwTZpQGMAAEAAQAC2hYBAALTKgIGAQEABEbU0tMA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":80,"old_price":160,"shipment_price":0},"3":{"price":160,"old_price":320,"shipment_price":0},"5":{"price":240,"old_price":480,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Миндов Стефан';
            var phone_hint = '+359897974512';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("bg");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Goji Cream - подмладяващ крем</title>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <meta content="yes" name="apple-mobile-web-app-capable"/>
        <link href="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/css/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/js/jquery.countdown.js"></script>
        <script src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/js/common.js"></script>
        <script src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/js/scroll.js"></script>
        <script src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/js/jquery.bxslider.min.js"></script>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    $(".js_errorMessage2").remove();
                    $(".js_errorMessage").remove();
                    var errors = 0,
                        form = $(this).closest('form'),
                        name = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        countryp = form.find('[id="country_code_selector"]').val(),
                        namep = name.val(),
                        phonep = phone.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;
                    if(name.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(name, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(name, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        var mas={};
                        form.find('input,textatea,select').each(function(){
                            mas[$(this).attr('name')]=$(this).val();
                        });
                        $.post('/order/create_temporary/', mas);
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>

    <div class="dbody">
        <header>
            <div class="wrapper">
                <img alt="pack" id="bg" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/header.png"/>
                <img alt="pack" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/6121951.png" style="    position: relative; margin-bottom: 20px; top: 21px;"/>
                <span>подмладяващ крем</span>
                <p>Узрялото годжи бери съдържа:</p>
                <ul>
                    <li>около 20 от най-ценните аминокиселини</li>
                    <li>Витамин С (500 пъти повече отколкото портокалите)</li>
                    <li>витамин Е</li>
                    <li>желязо (15 пъти повече отколкото ябълките)</li>
                </ul>

                <button class="green_btn pre_toform">ПОРЪЧАЙ</button>

            </div>
        </header>
        <aside>
            <div class="wrapper">
                <h3>Побързайте да поръчате иновативния Goji Cream на рекордно ниска цена!</h3>
                <div class="prices">
                    <p class="old_price ">
<span  style="text-decoration: line-through;">160 Лева
              </span>
                    </p>
                    <p>80 Лева
                    </p>
                </div>
                <div class="sale">
                    <p>50% отстъпка
                    </p>
                </div>
                <p>има 7 бройки на промоционална цена</p>
            </div>
        </aside>
        <section class="extra">
            <div class="wrapper">
                <h3>КАК ДЕЙСТВА GOJI CREAM?</h3>
                <ul>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/img1.png"/>
                        </div>
                        <div>
                            <p>Включва различни съставки за защита срещу ултравиолетовите лъчи, както и възстановяващи биологично активни елементи, които преструктурират клетките на кожата.</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/img2.png"/>
                        </div>
                        <div>
                            <p>Помага за предотвратяване на повторната поява на бръчки като насища кожата на шията и лицето с широка гама витамини, микроелементи и аминокиселини.</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="bef" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/img3.png"/>
                        </div>
                        <div>
                            <p>Активизира естественото производство на колаген в дълбоките слоеве на кожата, Goji Cream връща еластичността и младостта максимално бързо!</p>
                        </div>
                    </li>
                </ul>
                <h3>ДА СЕ ПОДМЛАДИШ С 10 ГОДИНИ ЗА 14 ДНИ ВЕЧЕ Е ВЪЗМОЖНО!</h3>

                <button class="green_btn pre_toform" style="font-size: 16px;">Искам да изглеждам млада</button>

            </div>
        </section>
        <section class="third_scr">
            <div class="wrapper">
                <h3>МНЕНИЕТО НА ЕКСПЕРТА</h3>
                <p>Преди да бъдат пуснати за продажба, нашите козметични продукти се подлагат на дерматологичен контрол и клинични изпитвания. Goji Cream показа висока ефективност в борбата с бръчките и подмладяването на кожата.</p>
                <div class="picture">
                    <div class="before">преди</div>
                    <div class="after">след</div>
                </div>
                <img alt="bef_aft" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/img_4.png"/>
                <ul>
                    <li>При 97 от 100 пациенти резултатът се забелязва още след първата употреба.</li>
                    <li>При 93% малките и недълбоки бръчки изчезват след 10 дни употреба.</li>
                </ul>


                <button class="red_btn pre_toform" style="font-size: 16px;">започнете лечение</button>

            </div>
        </section>
        <section class="third_and_half">
            <div class="wrapper">
                <h3>мнения
                </h3>
                <div class="sliderslider">
                    <div>
                        <div class="bxslider">
                            <div class="slide-list">
                                <div class="img_slide">
                                    <img alt="girl" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/ava1.png"/>
                                </div>
                                <p>
                                    Харесвам китайската медицина и се опитвам да намеря китайски кремове за лице, защото за 5000 години история знаят по-добре от нас как да съхраняват красотата. Но вече съм на 50 години и напоследък видът на кожата ми започва да ме притеснява. Кожата под брадичката ми провисва и бръчките са повече.
                                </p>
                                <p>
                                    Моят козметик ми препоръча Goji Cream, защото хем е иновативен продукт, хем е на базата на традиционен китайски продукт за подмладяване - годжи бери. Две седмици след като започнах да го използвам, кожата ми наистина стана по-еластична, бръчките значително се изгладиха и сега го използвам редовно.
                                </p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide">
                                    <img alt="girl" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/ava2.png"/>
                                </div>
                                <p>
                                    Обичам всичко ново, така че не можех да пропусна такава новост като Goji Cream - иновативен подмладяващ крем за лице. Все още нямам 60, но години още не личат на лицето ми, защото полагам много усилия да ги скрия.
                                </p>
                                <p>
                                    И какво да кажа, кремът има приятна структура, лек е, не лепне, бързо попива и изглажда бръчките, като ютия и то доста бързо, ефектът се забеляза само след две седмици. За моята кожа е перфектен.
                                </p>
                            </div>
                            <div class="slide-list">
                                <div class="img_slide">
                                    <img alt="girl" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/ava3.png"/>
                                </div>
                                <p>
                                    Мога да препоръчам Goji Cream на жени на възраст с ясното съзнание, че кремът наистина действа. Изобщо продуктът е много интересен, тези плодове сякаш са магически и не е изненадващо, че се справят с бръчките.
                                </p>
                                <p>
                                    На мен ми го подари майката на мой ученик и съвсем скоро кожата придоби тонус, а пачият крак и другите фини бръчици започнаха да се изглаждат и да изчезват. Вече станаха две седмици откакто започнах да го ползвам и има ефект, след това сигурно ще започне да действа и на по-дълбоките бръчки.
                                </p>
                            </div>
                        </div>
                    </div>
                    <a class="jcarousel-prev" data-jcarouselcontrol="true" href="#">
                    </a>
                    <a class="jcarousel-next" data-jcarouselcontrol="true" href="#">
                    </a>
                </div>
            </div>
        </section>
        <footer class="form">
            <div class="wrapper clearfix" id="form">
                <img alt="foot" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/foot_bg.png"/>
                <h3>САМО ДНЕС МОЖЕТЕ ДА ПОРЪЧАТЕ GOJI CREAM С 50% ОТСТЪПКА!</h3>
                <div class="prices">
                    <p class="old_price">
<span>Стара цена:
                <span style="text-decoration: line-through;">160 Лева
                </span>
</span>
                    </p>
                    <p class="new_price">НОВА ЦЕНА:
                        <span>80 Лева
              </span>
                    </p>
                </div>
                <form action="" id="order_form" method="post">
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abcdg.pro/forms/?target=-4AAIJIAI_FwAAAAAAAAAAAASaFRrAAA"></iframe>

                    <!--<div class="shipment_price">Доставка: 0 Лева
                                    </div>
                    <div class="total_price">Общо: 80 Лева
                                    </div>-->


                    <div class="toform"></div>

                </form>
                <div class="counter_wrap">
                    <p>до края на промоцията остават:</p>
                    <div class="timer-block countdownHolder countdown_please" id="countdown"></div>
                </div>
            </div>
        </footer>
    </div>


    <link href="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/css/stylesecond.css" rel="stylesheet"/>
    <div class="hidden-window">
        <div class="containerz">
            <section class="h-w__inner">
                <div class="h-w__header">
                    <span>GOJI CREAM</span> <br/>поръчайте още сега!
                </div>
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Плащане <b>при доставка</b></div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/imageSecond/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/imageSecond/dtc1.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/imageSecond/dtc2.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/imageSecond/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/imageSecond/dtc3.png"/></div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">Поръчката</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Доставка</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Доставка и
                                                <span>плащане</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">ДОБРА СДЕЛКА</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 опаковка</option>
                                <option data-slide-index="1" selected="selected" value="3">2 опаковки</option>
                                <option data-slide-index="2" value="5">3 опаковки</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs"><img alt="zt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/imageSecond/zit1.png"/> </div>
                                            <div class="zHeader"><b>1</b> опаковка</div>
                                            <div class="pack_descr">Видимо подмладена само след месец употреба</div>
                                        </div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Цена:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">80</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>160</div>Лева</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right side-right--columns">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile">подарък</div><img alt="zt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/imageSecond/zit2.png"/> </div>
                                                <div class="zHeader"><b>2</b> опаковки
                                                    <br/> <span class="dib zplus">подарък</span></div>
                                            </div>
                                            <div class="pack_descr pack_descr--columns">Курс към младостта! Кожата се стяга и даже най-дълбоките бръчки се изглаждат само за три месеца. Гарантиран резултат.</div>
                                        </div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Цена:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">160</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>320</div>Лева</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right side-right--columns">
                                            <div class="ssimgabs">
                                                <div class="giftmobile">подаръка</div><img alt="zt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/mobile/img/imageSecond/zit3.png"/> </div>
                                            <div class="zHeader"><b>3</b> опаковки
                                                <br/> <span class="dib zplus sec">подаръка</span> </div>
                                        </div>
                                        <div class="pack_descr pack_descr--columns">Максимално подмладяване! С пълния шест-месечен курс не само ще се отървете от промените, свързани с възрастта, но също така ще предотвратите повторната им поява.</div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Цена:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">240</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>480</div>Лева</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div id="bx-pager"><a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a><a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a><a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a></div>
                        </div>
                        <form action="/order/create/" class="js_scrollForm" method="post"><input type="hidden" name="total_price" value="80.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAI_FwTZpQGMAAEAAQAC2hYBAALTKgIGAQEABEbU0tMA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="GojiCreamredgreenBG">
                            <input type="hidden" name="price" value="80">
                            <input type="hidden" name="old_price" value="160">
                            <input type="hidden" name="total_price_wo_shipping" value="80.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 240, 'old_price': 480, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Лева">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="BG">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.2">

                            <select style="display: none;" class="select inp" id="country_code_selector" name="country_code">
                                <option value="BG">България</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 опаковка</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 опаковка</option>
                                <option data-slide-index="2" value="5">3+2 опаковки</option>
                            </select>
                            <input style="display: none;" class="inp j-inp" name="name" placeholder="Въведете име" type="text" value=""/>
                            <input style="display: none;" class="only_number inp j-inp" name="phone" placeholder="Въведете телефон" type="text" value=""/>
                            <input autocomplete="off" class="inp ui-autocomplete-input" name="address" placeholder="Въведете адрес" type="text"/>
                            <div class="text-center totalpriceForm">Доставка: 0 Лева</div>
                            <div class="text-center totalpriceForm">Общо: <span class="js_total_price js_full_price">80 </span> Лева</div>
                            <div class="zbtn js_submit transitionmyl" style="cursor:pointer"> поръчай </div>
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Ние гарантиране:</div>
                            <ul>
                                <li><b>100%</b> качество</li>
                                <li><b>Проверка</b> на продукта при получаване</li>
                                <li><b>Безопасност</b> на вашите данни</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 10963 -->
        <script>var locale = "bg";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAI_FwSfpSWKAAEAAQAC2hYBAALTKgIGAQEABPoAc0QA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":80,"old_price":160,"shipment_price":0},"3":{"price":160,"old_price":320,"shipment_price":0},"5":{"price":240,"old_price":480,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Миндов Стефан';
            var phone_hint = '+359897974512';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("bg");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>Goji Cream - подмладяващ крем</title>
        <link href="//st.acstnst.com/content/GojiCreamredgreenBG/css/bootstrap.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/GojiCreamredgreenBG/css/style.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/GojiCreamredgreenBG/js/bootstrap.min.js"></script>
        <script src="//st.acstnst.com/content/GojiCreamredgreenBG/js/scripts.js"></script>
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                    console.log(elem, msg,jQuery(elem).offset().left,jQuery(elem).offset().top - 30);
                }
                $(".pre_toform").on("touchend, click", function (event) {
                    event.preventDefault();
                    $('body,html').animate({scrollTop: $('.toform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    var errors = 0,
                        form = $(this).closest('form'),
                        country = form.find('[name="country_code"]'),
                        fio = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        esub = form.find('[name="esub"]'),
                        namep = fio.val(),
                        phonep = phone.val(),
                        countryp = country.val(),
                        esubp = esub.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;

                    if(fio.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(fio, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        console.log('name: '+namep,'phone:'+phonep,'country_code: '+countryp,'esub: '+esubp);
                        $.post('/order/create_temporary/', {name:namep, phone:phonep, country_code:countryp, esub:esubp});
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {


                setInterval(function () {


                    $(".wrapimg1").animate({
                        height: "468px"
                    }, 3500, function () {

                        $(".wrapimg1").animate({
                            height: "0px"
                        }, 1000);
                    });

                    setTimeout(function () {
                        $('.textimg.do').html("SONRA");
                    }, 3100);

                    setTimeout(function () {
                        $('.textimg.do').hide();
                        $('.textimg.do').html("ÖNCƏ");
                        $('.textimg.do').fadeIn();
                    }, 4500);


                }, 6000);
            });

        </script></head><body><p>

        <style>
            .footer .form_wrap {

                margin-top: 80px;
            }
        </style>


    </p>

    <div class="first-page">
        <div class="header">
            <div class="container">
                <div class="logo text-center">
                    <span class="order">Goji Cream</span>
                    <p>подмладяващ крем</p>
                    <!--p>для женщин старше 50 лет</p-->
                </div>
                <h1 class="text-center title">Силата на 1000 плода в 1 капка крем</h1>
                <div class="cream"></div>
                <div class="right_side">
                    <h3>Узрялото годжи бери съдържа:</h3>
                    <ul class="list-unstyled">
                        <li><span>около 20 от най-ценните аминокиселини</span></li>
                        <li class="padding"><span>Витамин С (500 пъти повече<br/>отколкото портокалите)</span></li>
                        <li><span>витамин Е</span></li>
                        <li class="padding"><span>желязо (15 пъти повече<br/>отколкото ябълките)</span></li>
                        <li><span>витамин В комплекс</span></li>
                        <li class="padding"><span>бетаин</span></li>
                    </ul>
                    <div class="wrap">
                        <div class="rotate">
                            <p>Формулата на крема гарантира<br/>изчезването на бръчките</p>
                            <strong>за 14 дни!</strong>
                        </div>
                    </div>
                </div>
                <div class="animph">
                    <div class="wrapimg"></div>
                    <div class="wrapimg1"></div>
                </div>
            </div>
        </div>
        <div class="section_1">
            <div class="container text-center">
                <h3>Побързайте да поръчате<br/>иновативния Goji Cream<br/>на рекордно ниска цена!</h3>
                <div class="wrap_1"><span>50%<br/>отстъпка</span></div>
                <div class="wrap_2"><span>има<br/><label>7</label></span>
                    <p>бройки на<br/>промоционална цена</p></div>
                <div class="price">
                    <span class="new">80 Лева</span>
                    <span class="old">160 Лева</span>
                </div>

                <div class="green_btn order pre_toform">поръчай</div>

            </div>
        </div>
        <div class="section_2">
            <div class="container">
                <h2 class="title">Как действа Goji Cream?</h2>
                <ul class="list-inline">
                    <li class="letter">
                        <p>
                            Клинично доказано е, че Goji Cream спира процеса на стареене на клетките и нормализира тяхната функция.
                        </p>
                        <div class="br"></div>
                        <p>
                            Широката гама от витамини и минерали, както и биотинът в крема образуват тежка молекула, която може да проникне в по-дълбокия слой на кожата, за да гарантира, че Goji Cream ще е максимално ефективен.
                        </p>
                        <div class="text-center">
                            <img alt="" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/element.jpg"/>
                        </div>
                        <p>
                            Аминокиселините в плодовете годжи бери имат мощни антиоксидантни свойства. Благодарение на това активното действие на крема продължава 24 часа. Аминокиселините действат като гъба, която поема трансепидермалната водна загуба, като така се постига по-дълбоко овлажняване на кожата и рекордно изглаждане на бръчките.
                        </p>
                        <div class="red_wrap">Не съдържа хормони</div>
                    </li>
                    <li class="picture">
                        <ul class="list-unstyled">
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/img_1.jpg"/></div>
                                <p>
                                    Включва различни съставки за защита срещу ултравиолетовите лъчи, както и възстановяващи биологично активни елементи, които преструктурират клетките на кожата.
                                </p>
                            </li>
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/img_2.jpg"/></div>
                                <p>
                                    Помага за предотвратяване на повторната поява на бръчки като насища кожата на шията и лицето с широка гама витамини, микроелементи и аминокиселини.
                                </p></li>
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/img_3.jpg"/></div>
                                <p>
                                    Активизира естественото производство на колаген в дълбоките слоеве на кожата, Goji Cream връща еластичността и младостта максимално бързо!
                                </p></li>
                        </ul>
                        <h3 class="text-center">Да се подмладиш с 10 години <br/>за 14 дни вече е възможно!</h3>

                        <button class="green_btn order pre_toform">Искам да изглеждам млада</button>

                    </li>
                </ul>
            </div>
        </div>
        <div class="section_3">
            <div class="container">
                <h2 class="title">Как се нанася</h2>
                <ul class="list-inline">
                    <li class="step_1">
                        <div class="img_wrap">
                            <h3>Стъпка 1:</h3>
                        </div>
                        <p>Леко почистете кожата от козметиката и натрупаните замърсявания с продукт за измиване или тоник.</p>
                    </li>
                    <li class="step_2">
                        <div class="img_wrap">
                            <h3>Стъпка 2:</h3>
                        </div>
                        <p>Нанесете крема на почистената кожа. Масажирайте с кръгови движения, като избягвайте зоната около очите.</p>
                    </li>
                    <li class="step_3">
                        <div class="img_wrap">
                            <h3>Стъпка 3:</h3>
                        </div>
                        <p>Кожата се стяга мигновено и е дълбоко хидратирана. Кремът продължава да действа активно 24 часа след нанасянето!</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wiki_section">
            <div class="container">
                <ul class="list-inline">
                    <li><img alt="" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/wiki.jpg"/></li>
                    <li class="text">
                        <h3>Защо са опасни бръчките?</h3>
                        <p>
                            Бръчките, това са видими гънки по повърхността на кожата в резултат от прекомерната активност на лицевите мускули, загуба на еластичност на кожата и други причини. Еластичността на кожата се дължи на съдържащи се в дермата колагенови влакна, а плътността - на намиращите се между тях молекули хиалуронова киселина.
                        </p>
                        <p>
                            Бръчките нарушават формата на кръвоносните съдове, като така влошават циркулацията на кръвта и лимфата в кожата. Кожата не получава кислород и се появяват нови бръчки, а старите стават все по-дълбоки. Ако не се лекува, този процес е необратим.
                        </p>
                        <p>
                            Методи за борба срещу бръчките са кремове, маски, инжекции с ботокс, химически пилинг и пластична хирургия. Последните три методи се считат за най-опасни.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_4">
            <div class="container">
                <div class="left_side">
                    <h2 class="title">Ползата от плодовете годжи</h2>
                    <p>
                        Противно на твърденията в рекламите, сушените плодове практически не съдържат витамин С. За да може тялото да получи количеството антиоксиданти, което се съдържа в една прясна червена ябълка, трябва да изядете килограм сушени плодчета. Докато в пресните плодове количеството на витамин С е 500 пъти по-голямо от това в портокалите.
                    </p>
                </div>
            </div>
        </div>
        <div class="section_5">
            <div class="container">
                <div class="right_side">
                    <h2 class="title">Мнението на експерта</h2>
                    <p>
                        За нашата козметика предпочитаме само естествени компоненти. Хендел използва иновативни методи, за да създаде продукти за красота, които имат гарантиран успех.
                    </p>
                    <p>
                        Преди да бъдат пуснати за продажба, нашите козметични продукти се подлагат на дерматологичен контрол и клинични изпитвания. Goji Cream показа висока ефективност в борбата с бръчките и подмладяването на кожата.
                    </p>
                    <br/>
                    <div class="picture">
                        <div class="before">преди</div>
                        <div class="after">след</div>
                    </div>
                    <ul class="list-unstyled">
                        <li>При 97 от 100 пациенти резултатът се забелязва още след първата употреба.</li>
                        <li>При 93% малките и недълбоки бръчки изчезват след 10 дни употреба.</li>
                    </ul>

                    <button class="red_btn pre_toform">започнете лечение</button>

                </div>
                <div class="doctor">
                    <div class="wrap">
                        <div class="rotate">
                            <h4>Андрей Въжаров</h4>
                            <p>Водещ специалист в разработването на козметичната линия на Хендел</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flower"></div>
        </div>
        <div class="section_6">
            <div class="container">
                <h2 class="title">мнения</h2>
            </div>
            <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="2" data-target="#carousel-example-generic"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/1.png"/>
                                        <span>преди</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/2.png"/>
                                        <span>след</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p>
                                        Харесвам китайската медицина и се опитвам да намеря китайски кремове за лице, защото за 5000 години история знаят по-добре от нас как да съхраняват красотата. Но вече съм на 50 години и напоследък видът на кожата ми започва да ме притеснява. Кожата под брадичката ми провисва и бръчките са повече.
                                    </p>
                                    <p>
                                        Моят козметик ми препоръча Goji Cream, защото хем е иновативен продукт, хем е на базата на традиционен китайски продукт за подмладяване - годжи бери. Две седмици след като започнах да го използвам, кожата ми наистина стана по-еластична, бръчките значително се изгладиха и сега го използвам редовно.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/3.png"/>
                                        <span>преди</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/4.png"/>
                                        <span>след</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p>
                                        Обичам всичко ново, така че не можех да пропусна такава новост като Goji Cream - иновативен подмладяващ крем за лице. Все още нямам 60, но години още не личат на лицето ми, защото полагам много усилия да ги скрия.
                                    </p>
                                    <p>
                                        И какво да кажа, кремът има приятна структура, лек е, не лепне, бързо попива и изглажда бръчките, като ютия и то доста бързо, ефектът се забеляза само след две седмици. За моята кожа е перфектен.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/5.png"/>
                                        <span>преди</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/6.png"/>
                                        <span>след</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p>
                                        Мога да препоръчам Goji Cream на жени на възраст с ясното съзнание, че кремът наистина действа. Изобщо продуктът е много интересен, тези плодове сякаш са магически и не е изненадващо, че се справят с бръчките.
                                    </p>
                                    <p>
                                        На мен ми го подари майката на мой ученик и съвсем скоро кожата придоби тонус, а пачият крак и другите фини бръчици започнаха да се изглаждат и да изчезват. Вече станаха две седмици откакто започнах да го ползвам и има ефект, след това сигурно ще започне да действа и на по-дълбоките бръчки.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Controls -->
                <div class="container">
                    <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('prev');" role="button"></a>
                    <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container text-center">
                <h2>Само днес можете да поръчате<br/>Goji Cream с 50% отстъпка!</h2>
                <ul class="list-inline">
                    <li class="timmer_section">
                        <h3 class="old_price">Стара цена: <span>160 Лева</span></h3>
                        <h3 class="new_price">нова цена:<br/><span>80 Лева</span></h3>
                        <ul class="list-unstyled timme_wrap">
                            <li class="left">до края на промоцията остават:</li>
                            <li class="timmer">
                                <ul class="list-inline count">
                                    <li class="hours">01</li>
                                    <li class="minutes">01</li>
                                    <li class="seconds">01</li>
                                </ul>
                            </li>
                            <li class="text">
                                <ul class="list-inline">
                                    <li>часа</li>
                                    <li>минути</li>
                                    <li>секунди</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="form_wrap">
                        <h3>ПОБЪРЗАЙТЕ! КОЛИЧЕСТВАТА СА ОГРАНИЧЕНИ!</h3>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAI_FwAAAAAAAAAAAASaFRrAAA"></iframe>

                            <!--<input type="hidden" name="total_price" value="80.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAI_FwSfpSWKAAEAAQAC2hYBAALTKgIGAQEABPoAc0QA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="GojiCreamredgreenBG">
                            <input type="hidden" name="price" value="80">
                            <input type="hidden" name="old_price" value="160">
                            <input type="hidden" name="total_price_wo_shipping" value="80.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 240, 'old_price': 480, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Лева">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="BG">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.2">


                            <div class="inp select j-count">
                                <select class="country" id="country_code_selector" name="country_code">
                                    <option value="BG">България</option>
                                </select>
                            </div>
                            <input name="name" placeholder="Име" type="text"/>
                            <input class="only_number" name="phone" placeholder="Телефон" type="text"/>
                            <button class="green_btn js_pre_toform">поръчай</button>-->
                            <div class="toform"></div>

                        </form>
                    </li>
                </ul>
                <div class="copyright">
                </div>
            </div>
        </div>
    </div>

    <link href="//st.acstnst.com/content/GojiCreamredgreenBG/css/second-page.css" rel="stylesheet"/>
    <script src="//st.acstnst.com/content/GojiCreamredgreenBG/js/second-page.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet"/>
    <div class="hidden-window">
        <div class="containerz">
            <section class="h-w__inner">
                <div class="h-w__header">
                    <span>GOJI CREAM – поръчайте още сега!</span>
                </div>
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Плащане <b>при доставка</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>1
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/second/dtc1.png"/>
                                    </div>
                                    <div class="dtable-cell">Поръчката
                                    </div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>2
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/second/dtc2.png"/>
                                    </div>
                                    <div class="dtable-cell">Доставка
                                    </div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>3
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/second/dtc3.png"/>
                                    </div>
                                    <div class="dtable-cell">Доставка и
                                        <br/>
                                        <span>плащане</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Добра
                            <span>сделка
                </span>
                        </div>
                        <form action="" class="js_scrollForm" method="post">
                            <!--<input type="hidden" name="total_price" value="80.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAI_FwSfpSWKAAEAAQAC2hYBAALTKgIGAQEABPoAc0QA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="GojiCreamredgreenBG">
                            <input type="hidden" name="price" value="80">
                            <input type="hidden" name="old_price" value="160">
                            <input type="hidden" name="total_price_wo_shipping" value="80.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 80, 'old_price': 160, 'shipment_price': 0}, u'3': {'price': 160, 'old_price': 320, 'shipment_price': 0}, u'5': {'price': 240, 'old_price': 480, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Лева">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="BG">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.2">

                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="BG">България</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1 опаковка</option>
                                <option selected="selected" value="3">2+1 опаковка</option>
                                <option value="5">3+2 опаковки</option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Въведете име" type="text" value=""/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Въведете телефон" type="text" value=""/>
                            <input autocomplete="off" class="inp ui-autocomplete-input" name="address" placeholder="Въведете адрес" type="text"/>-->

                            <div class="text-center totalpriceForm">Доставка: 0 Лева</div>
                            <div class="text-center totalpriceForm">Общо: <span class="js_total_price js_full_price">80</span> Лева</div>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Ние гарантиране:
                            </div>
                            <ul>
                                <li>
                                    <b>100%</b> качество
                                </li>
                                <li>
                                    <b>Проверка</b> на продукта при получаване

                                </li>
                                <li><b>Безопасност</b> на вашите данни
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1 active">
                            <div class="zDiscount">
                                <b>Внимание</b>: Има <span>50% отстъпка</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader">
                                    <b>1
                                    </b> опаковка
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Цена:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>80 Лева
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>160
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Стара цена
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px"> Доставка:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>0 Лева
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Общо:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>80
                                                </b> Лева
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="1">
                                </div>
                                <div class="img-wrapp">
                                    <img alt="z1" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/second/zit1.png" width="100"/>
                                </div>
                                <div class="zHeader">
<span>НОВАК
                    </span>
                                </div>
                                <text>Видимо подмладена само след месец употреба</text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 ">
                            <div class="zstick">
                            </div>
                            <div class="zDiscount">
                                <b>Внимание
                                </b>: Има
                                <span>50% отстъпка
                  </span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec">
                                    <b>2
                                    </b> опаковки
                                    <span class="dib zplus">подарък
                    </span>
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Цена:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>160 Лева
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>320
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Стара цена
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px"> Доставка:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>0 Лева
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Общо:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>160
                                                </b> Лева
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3">
                                </div>
                                <div class="img-wrapp">
                                    <div class="gift">
                                        подарък
                                    </div>
                                    <img alt="z1" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/second/zit2.png" width="220"/>
                                </div>
                                <div class="zHeader">
<span>НАПРЕДНАЛ
                    </span>
                                </div>
                                <text>
                                    Курс към младостта! Кожата се стяга и даже най-дълбоките бръчки се изглаждат само за три месеца. Гарантиран резултат.
                                </text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Внимание
                                </b>: Има
                                <span>50% отстъпка
                  </span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec">
                                    <b>3
                                    </b> опаковки
                                    <span class="dib zplus sec">подаръка
                    </span>
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Цена:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>240 Лева
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>480
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Стара цена
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px"> Доставка:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>0 Лева
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Общо:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>240
                                                </b> Лева
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5">
                                </div>
                                <div class="img-wrapp">
                                    <div class="gift">подаръка
                                    </div>
                                    <img alt="z1" src="//st.acstnst.com/content/GojiCreamredgreenBG/img/second/zit3_n3.png" width="220"/>
                                </div>
                                <div class="zHeader sm">
<span>НАПРЕДНАЛ
                    </span>
                                </div>
                                <text>
                                    Максимално подмладяване! С пълния шест-месечен курс не само ще се отървете от промените, свързани с възрастта, но също така ще предотвратите повторната им поява.
                                </text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>
    </div>


    </body></html>
<?php } ?>