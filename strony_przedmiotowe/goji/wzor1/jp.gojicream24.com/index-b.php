<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- [pre]land_id = 2373 -->
        <script>var locale = "jp";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAK0BQQFOyeKAAEAAQACPgUBAAJFCQIPAQJtBwTIAjrTAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":6260,"old_price":12520,"shipment_price":0},"3":{"price":12520,"old_price":25040,"shipment_price":0},"5":{"price":18780,"old_price":37560,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = '遼 高木';
            var phone_hint = '+8188-921-8600';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("jp");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
        <meta charset="utf-8"/>
        <title>Goji cream</title>
        <link href="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/css/style.css" rel="stylesheet"/>
        <link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow&amp;subset=latin,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/js/jcarousel.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/js/js.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/js/only_number.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/js/jquery.msgerror.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/js/jpcheckpref.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/js/jpgetpref.js"></script>
        <style type="text/css">
            .js_errorMessage {
                z-index: 999999;
            }
        </style>
    </head>
    <body>
    <div id="wrapperDef">
        <section id="block1">
            <div class="wrapper">
                <img alt="" class="goji_top" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/goji_top.png"/>
                <!--			<img src="_i/logo.png" alt="" class="logo">-->
                <h1 class="logo_name" style="text-align: center; font-size: 46px; color: rgb(191, 2, 0);margin-bottom:15px">Goji cream</h1>
                <div class="tx1">果汁入りGoji Cream の成分::</div>
                <ul class="list1">
                    <li class="list1_i1 list1_i">20種類の貴重なアミノ酸</li>
                    <li class="list1_i3 list1_i">  ビタミンE    </li>
                    <li class="list1_i4 list1_i">  ビタミンB群  </li>
                    <li class="list1_i2 list1_i"> ベタイン</li>
                </ul>
                <div class="old_price">  通常価格:   <span class="js_old_price_curs">  ￥ 12520 </span></div>
                <div class="new_price">  新価格:  <span class="js_new_price_curs"> ￥ 6260 </span></div>
            </div>
        </section>
        <section id="block2">
            <div class="wrapper">

                <div class="btn jsOpenWindow">  若く見えたい  </div>

                <div class="head">
                    顔から10年間を取り戻しましょう
                    <br/><span>14日間でできます! </span>
                </div>
                <img alt="" class="kaplya" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/kaplya.png"/>
                <div class="tx2">ビタミンC(オレンジの  500倍)</div>
            </div>
        </section>
        <section id="block4">
            <div class="wrapper">

                <div class="btn jsOpenWindow">若く見えたい</div>

                <div class="head4">塗布方法 </div>
                <ul class="list4">
                    <li class="list4_li list4_li1"><div class="img_b_li"><img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/face1.png" width="146"/></div>
                        <div class="list4_tx">顔の汚れをクレンザーやトニックで優しく落とします。</div></li>
                    <li class="list4_li list4_li2"><div class="img_b_li"><img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/face2.png"/></div>
                        <div class="list4_tx">清潔な肌にクリームを円を描きながら塗ります。目の周りは避けてください。</div></li>
                    <li class="list4_li list4_li3"><div class="img_b_li"><img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/face3.png"/></div>
                        <div class="list4_tx">肌が見る見るうちに張りを取り戻し、保湿されていきます。クリームの効果は使用してから24時間持続します!</div></li>
                </ul>
            </div>
        </section>
        <section id="block5">
            <div class="wrapper">
                <div class="head_5">コメント</div>
                <div class="sliderslider">
                    <div class="slider jcarousel">
                        <div>
                            <div class="slide-list">
                                <div class="img_block"><img alt="" class="before" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/img2.png"/><img alt="" class="after" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/img1.png"/></div>
                                <div class="author_tx">わたしの美容師が革新的な商品で、中国の若さの源、ゴジベリーの入ったGoji Cream を勧めてくれたので、試してみることにしました。使い始めて2週間経った頃には、肌の調子が安定し、しわが減りました。毎日使っています。</div>
                            </div>
                            <div class="slide-list">
                                <div class="img_block"><img alt="" class="before" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/img4.png"/><img alt="" class="after" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/img3.png"/></div>
                                <!--							<div class="author_name">Љубица Ивановска, 47 години </div>-->
                                <div class="author_tx">クリームのテクスチャーが好きです。軽くて、べたべたしていません。すぐに肌に吸収されてアイロンのように素早くしわを伸ばします。効果は2週後に鏡を見ればわかります。私の肌にぴったりです。</div>
                            </div>
                            <div class="slide-list">
                                <div class="img_block"><img alt="" class="before" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/img5.png"/><img alt="" class="after" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/img6.png"/></div>
                                <!--							<div class="author_name">Неда Топаловска, 54 години</div>-->
                                <div class="author_tx">生徒のお母さんがプレゼントにこのクリームをくれました。今まででこんなに効果が出た商品はありません-こわばっていた皮膚が柔らかくなり、小さなしわが消え始めました。2週間塗り続けていますが、効果は明らかです。すぐに深刻なしわも治ると思います。</div>
                            </div>
                        </div>
                    </div>
                    <a class="jcarousel-prev" href="index.html#"></a>
                    <a class="jcarousel-next" href="index.html#"></a>
                </div>
            </div>
        </section>
        <section id="block6">
            <div class="wrapper">
                <div class="sklad">
                    <!--<div class="s_tx1"><span class="s1">APRESÚRATE A COMPRAR</span> <br><span class="s2">CREMA DE GOJI</span> <br>TODAVÍA AL MISMO PRECIO<img src="_i/cream2.png" alt="" class="creamg"></div>-->
                    <div class="s_tx1"><span class="s1">Goji Cream を50%オフで変えるのは今日だけ!
Goji Cream 50%割引!<img alt="" class="creamg" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/cream2.png"/></span></div>
                    <div class="s_tx2">販売中</div>
                    <div class="s_count">07</div>
                    <div class="s_tx3">左
                        スペシャル*</div>
                </div>

                <button class="j-sub jsOpenWindow _bottom">注文</button>

            </div>
        </section>
    </div>
    <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/js/jquery.bxslider.min.js"></script>
    <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/js/main.js"></script>
    <div class="hidden-window">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet"/>
        <div class="new-header_goji">Goji Cream</div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader"><b>代金引換</b></div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/Zarr.png"/>
                                                <img alt="alt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/dtc1.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/dtc2.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/dtc3.png"/></div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">ご注文</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">発送</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">配達とお支払い</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">今だけのキャンペーン</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1本</option>
                                <option data-slide-index="1" selected="selected" value="3">2本＋１本プレゼント</option>
                                <option data-slide-index="2" value="5">３本で＋２本をプレゼント！</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs"><img alt="zt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/zit1.png"/></div>
                                            <div class="zHeader"><b>1</b>本</div>
                                        </div>
                                        <div class="pack_descr">ベーシックセット</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">価格</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new">6260</text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>
                                                    <text class="js-pp-old">12520</text>
                                                </div>￥</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile">1パック無料</div>
                                                    <img alt="zt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/zit2.png"/></div>
                                                <div class="zHeader"><b>2</b> 本
                                                    <br/> <span class="dib zplus">本プレゼント</span></div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">若さへの道Q</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">価格</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new">12520</text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>
                                                    <text class="js-pp-old">25040</text>
                                                </div>￥</div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <div class="giftmobile">2パック無料</div>
                                                <img alt="zt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/mobile/_i/zit3.png"/></div>
                                            <div class="zHeader"><b>3</b> 本で<br/> <span class="dib zplus sec">本プレゼント</span></div>
                                        </div>
                                        <div class="pack_descr">若返りを最大化に！</div>
                                        <div class="dtable" style="width: 80%;text-align: center;">
                                            <div class="dtable-cell red text-right">価格</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new">18780</text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>
                                                    <text class="js-pp-old">37560</text>
                                                </div>￥</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager">
                            <a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a>
                            <a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a>
                            <a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                        </div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAK0BQAAAAAAAAAAAARIvQuoAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="6260.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAK0BQQFOyeKAAEAAQACPgUBAAJFCQIPAQJtBwTIAjrTAA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="Goji cream - JP">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Goji_Cream_JP_Red">
                            <input type="hidden" name="price" value="6260">
                            <input type="hidden" name="old_price" value="12520">
                            <input type="hidden" name="al" value="2373">
                            <input type="hidden" name="total_price_wo_shipping" value="6260.0">
                            <input type="hidden" name="currency" value="￥">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="JP">
                            <input type="hidden" name="shipment_vat" value="0.0">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 6260, 'old_price': 12520, 'shipment_price': 0}, u'3': {'price': 12520, 'old_price': 25040, 'shipment_price': 0}, u'5': {'price': 18780, 'old_price': 37560, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.0">

                            <div class="formHeader">ご注文のためにはデータ入力お願いします</div>
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="JP">日本</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1本</option>
                                <option data-slide-index="1" selected="selected" value="3">2本＋１本プレゼント</option>
                                <option data-slide-index="2" value="5">３本で＋２本をプレゼント！</option>
                            </select>
                            <input class="inp" name="name" placeholder="氏名" type="text"/>
                            <input class="inp" name="postal_code" placeholder="郵便番号" style="position: absolute;visibility:hidden;z-index: -9999;width:10px;" type="text" value=""/>
                            <input class="small-qwe inp" name="qwe1" placeholder="郵便番号" style="width: 100px;" type="text" value=""/>
                            <span class="def">—</span>
                            <input class="small-qwe inp" name="qwe2" placeholder="郵便番号" style="width: 100px;" type="text" value=""/>
                            <input class="inp" name="prefecture" placeholder="都道府県" type="text"/>
                            <input class="inp" name="address" placeholder="市区町村・町名・番地" type="text" value=""/>
                            <input class="inp" name="address_detail" placeholder="ビル・マンション名" type="text" value=""/>
                            <input class="inp only_number" name="phone" placeholder="電話番号" type="text"/>-->
                            <p style="text-align:center;font-size: 18px;color: #e70116;padding: 5px;">代引手数料無料: <span class="js_delivery_price"> ￥ 0 </span></p>
                            <div class="text-center totalpriceForm">総額: ￥ <span class="js_total_price js_full_price">6260 </span></div>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">保証します。</div>
                            <ul>
                                <li><b>100%</b> 高質</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 2373 -->
        <script>var locale = "jp";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAK0BQQFOyeKAAEAAQACPgUBAAJFCQIPAQJtBwTIAjrTAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":6260,"old_price":12520,"shipment_price":0},"3":{"price":12520,"old_price":25040,"shipment_price":0},"5":{"price":18780,"old_price":37560,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = '遼 高木';
            var phone_hint = '+8188-921-8600';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("jp");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <title> Goji Cream  </title>
        <meta charset="utf-8"/>
        <link href="//st.acstnst.com/content/Goji_Cream_JP_Red/css/bootstrap.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Goji_Cream_JP_Red/css/style.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/bootstrap.min.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/scripts.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/only_number.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jquery.msgerror.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jpcheckpref.js"></script>
        <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jpgetpref.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('div.order, button.order, a.order').click(function(){
                    $("html, body").animate({scrollTop: $("form").offset().top-300}, 2000);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <div id="wrapperDef">
        <div class="header">
            <div class="container">
                <div class="logo text-center">
                    <a class="order" href="#">  Goji Cream   </a>
                    <p>  若返りクリーム  </p>
                    <!--p> for women aged 50 and over </p-->
                </div>
                <h1 class="text-center title">  1回でベリー1000個分の力   </h1>
                <div class="cream"></div>
                <div class="right_side">
                    <h3>  果汁入りGoji Cream の成分:  </h3>
                    <ul class="list-unstyled">
                        <li><span>  20種類の貴重なアミノ酸  </span></li>
                        <li class="padding"><span>  ビタミンC(オレンジの  <br/>  500倍)  </span></li>
                        <li><span>  ビタミンE  </span></li>
                        <li class="padding"><span>  鉄(リンゴの  <br/>  15倍)  </span></li>
                        <li><span>  ビタミンB群  </span></li>
                        <li class="padding"><span>  ベタイン  </span></li>
                    </ul>
                    <div class="wrap">
                        <div class="rotate">
                            <p>  クリームの成分で  <br/>  しわが消えます   </p>
                            <strong>  それも14日以内で!  </strong>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function(){

                        $(document).ready(function () {
                            function animate(){
                                $(".wrapimg1").animate({
                                    height: "467px"
                                }, 4000);

                                $(".wrapimg1").animate({
                                    height: "0"
                                }, 2000);
                            }

                            animate();
                            setInterval(function () {
                                animate();
                            }, 5000);
                        });
                    });
                </script>
                <div class="animph">
                    <!--<div class="textimg do active">
                                        ก่อน
                                    </div>-->
                    <div class="wrapimg"></div>
                    <div class="wrapimg1"></div>
                </div>
            </div>
        </div>
        <div class="section_1">
            <div class="container text-center">
                <h3>  今すぐご注文を!  <br/>  革新的な化粧品Goji Cream    <br/>  最低価格でご提供!   </h3>
                <div class="wrap_1"><span>50%<br/>  割引  </span></div>
                <div class="wrap_2"><span>  販売中  <label>7</label> 左 スペシャル </span> </div>
                <div class="price">
                    <span class="new js_new_price_curs">  ￥ 6260   </span>
                    <span class="old js_old_price_curs">  ￥  12520  </span>
                </div>

                <div class="green_btn jsOpenWindow">  注文  </div>

            </div>
        </div>
        <div class="section_2">
            <div class="container">
                <h2 class="title">  Goji Cream の効果   </h2>
                <ul class="list-inline">
                    <li class="letter">
                        <p style="text-align: justify;">  Goji Cream は細胞の老化を食い止めることが臨床的に証明されています。   </p>
                        <div class="br"></div>
                        <p style="text-align: justify;">  様々なビタミンやミネラル及びビオチンが入っており、全て重分子の状態にしてあるので、皮膚の深い層まで浸透します。ですから、Goji Cream の最大の効果を実感いただけます。   </p>
                        <div class="text-center">
                            <img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/element.jpg"/>
                        </div>
                        <p style="text-align: justify;">  ゴジ・ベリーに含まれるアミノ酸には強力な抗酸化作用があるので、クリームの効果が24時間持続します。アミノ酸は表皮を通過し、潤いを逃しません。これでしわが減っていきます。   </p>
                        <div class="red_wrap">  ホルモンフリー   </div>
                    </li>
                    <li class="picture">
                        <ul class="list-unstyled">
                            <li><div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/img_1.jpg"/></div><p style="text-align: justify;">  日焼け止め効果のある成分や、生物学的に有効な成分が細胞の保全を促します。  </p></li>
                            <li><div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/img_2.jpg"/></div><p style="text-align: justify;">  これでしわの再発生を予防し、顔と首の肌を保湿して様々なビタミンとミネラルとアミノ酸を補給できます。  </p></li>
                            <li><div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/img_3.jpg"/></div><p style="text-align: justify;">  Goji Cream は皮膚深層の天然コラーゲンの生成を促し、皮膚の弾力性と若々しさを記録的な速さで取り戻します!  </p></li>
                        </ul>
                        <h3 class="text-center">  顔から10年間を取り戻しましょう   <br/>  14日間でできます!   </h3>

                        <button class="green_btn jsOpenWindow">  若く見えたい  </button>

                    </li>
                </ul>
            </div>
        </div>
        <div class="section_3">
            <div class="container">
                <h2 class="title">  塗布方法  </h2>
                <ul class="list-inline">
                    <li class="step_1">
                        <div class="img_wrap">
                            <h3>  クレンジング1  </h3>
                        </div>
                        <p>  顔の汚れをクレンザーやトニックで優しく落とします。   </p>
                    </li>
                    <li class="step_2">
                        <div class="img_wrap">
                            <h3>  ステップ2  </h3>
                        </div>
                        <p>  清潔な肌にクリームを円を描きながら塗ります。<br/>目の周りは避けてください。   </p>
                    </li>
                    <li class="step_3">
                        <div class="img_wrap">
                            <h3>  ステップ3  </h3>
                        </div>
                        <p>  肌が見る見るうちに張りを取り戻し、保湿されていきます。<br/>クリームの効果は使用してから24時間持続します!  </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wiki_section">
            <div class="container">
                <ul class="list-inline">
                    <li><img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/wiki.png"/></li>
                    <li class="text">
                        <h3>  しわの危険性は?  </h3>
                        <p style="text-align: justify;">  しわは、顔の筋肉を動かし過ぎや、肌の弾力性を失うことなどで発生する皮膚のひだを指します。弾力性はコラーゲン繊維を真皮に、肌の力はコラーゲン繊維中のヒアルロン酸分子で補えます。  </p>
                        <p style="text-align: justify;">  しわは血管を妨げ、皮膚の血液とリンパの循環を損ないます。酸素が皮膚までいきわたらず、また新たなしわが発生し、古いしわは一層深まります。治療しないと、取り返しがつかなくなってしまいます。  </p>
                        <p style="text-align: justify;">  しわの予防法：化粧水、マスク、ボトックス注射、ピーリング、整形。最後の3は危険性があると言われています。  </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_4">
            <div class="container">
                <div class="left_side">
                    <h2 class="title">  ベリーの効能  </h2>
                    <p style="text-align: justify;">  マーケティング担当者の言い分とは反対に、乾燥ベリーにはビタミンCは含まれていません。リンゴ1個分の抗酸化物質を摂取するには乾燥ベリーを2ポンド分食べる必要があります。一方で、新鮮なベリーはオレンジより500倍のビタミンCを含んでいます。  </p>
                </div>
            </div>
        </div>
        <div class="section_5">
            <div class="container">
                <div class="right_side">
                    <h2 class="title">  専門家の意見  </h2>
                    <p style="text-align: justify;">  顔の化粧品を製造するときは、天然成分を使用するのは最優先しています。ヘンデルカンパニーは革新的なメソッドで人気の高い化粧品ラインを製造しています。  </p>
                    <p style="text-align: justify;">  売店の棚に並ぶ前に、化粧品は皮膚科および臨床試験を受けます。Goji Cream の優れた効能でしわが減り、肌が若返ります。  </p>
                    <br/>
                    <div class="picture">
                        <div class="before">  ビフォア  </div>
                        <div class="after">  アフター  </div>
                    </div>
                    <ul class="list-unstyled">
                        <li>  最初に塗った時、100件中97件で目に見える効果が認められました。   </li>
                        <li>  クリームを10日間塗布したら、小さい、あるいは浅いしわの93%が消えました  </li>
                    </ul>

                    <button class="red_btn jsOpenWindow">  治療を始める  </button>

                </div>
                <div class="doctor">
                    <div class="wrap">
                        <div class="rotate">
                            <h4>  此　宇曾子  </h4>
                            <p>  専門家リーダー  <br/>  スキンケア商品  <br/>  ヘンデル  </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flower"></div>
        </div>
        <div class="section_6">
            <div class="container">
                <h2 class="title">  コメント  </h2>
            </div>
            <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="2" data-target="#carousel-example-generic"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/50/1.png"/>
                                        <span>  ビフォア  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/50/2.png"/>
                                        <span>  アフター  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">  私は中国医療が大好きで、5000年の歴史を持つ中国製の化粧品を買い、これで美容は大丈夫だと思っていました。今50歳で顔がどんどん加齢しているのが見て取れます。しわや目の周りのカラスの足跡が目立つようになってきました。   </p>
                                    <p style="text-align: justify;">  わたしの美容師が革新的な商品で、中国の若さの源、ゴジベリーの入ったGoji Cream を勧めてくれたので、試してみることにしました。使い始めて2週間経った頃には、肌の調子が安定し、しわが減りました。毎日使っています。  </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/50/3.png"/>
                                        <span>  ビフォア  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/50/4.png"/>
                                        <span>  アフター  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">  私は新しいものならなんでも好きなので、Goji Cream のような新商品を試そうと思いました。革新的なアンチエイジングクリームだと聞き、使ってみました。現在50代後半ですが、顔の調子は良好です、よくケアしている証ですね。   </p>
                                    <p style="text-align: justify;">  クリームのテクスチャーが好きです。軽くて、べたべたしていません。すぐに肌に吸収されてアイロンのように素早くしわを伸ばします。効果は2週後に鏡を見ればわかります。私の肌にぴったりです。  </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/50/5.png"/>
                                        <span>  ビフォア  </span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_Cream_JP_Red/img/50/6.png"/>
                                        <span>  アフター  </span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">  私は自信をもってGoji Cream をあらゆる世代の全ての女性にお勧めします。ゴジは興味深い商品で、ベリーの力には計り知れないものがあります。しわが消えるのも、全く不思議ではありません。  </p>
                                    <p style="text-align: justify;">  生徒のお母さんがプレゼントにこのクリームをくれました。今まででこんなに効果が出た商品はありません-こわばっていた皮膚が柔らかくなり、小さなしわが消え始めました。2週間塗り続けていますが、効果は明らかです。すぐに深刻なしわも治ると思います。  </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Controls -->
                <div class="container">
                    <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('prev');" role="button"></a>
                    <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container text-center">
                <h2>  Goji Cream を50%オフで変えるのは今日だけ!  <br/>  Goji Cream 50%割引!  </h2>
                <ul class="list-inline">
                    <li class="timmer_section">
                        <h3 class="old_price">  通常価格:   <span class="js_old_price_curs">  ￥ 12520 </span></h3>
                        <h3 class="new_price">  新価格:  <br/><span class="js_new_price_curs">  ￥ 6260</span></h3>
                        <ul class="list-unstyled timme_wrap">
                            <li class="left">  期限は:  </li>
                            <li class="timmer">
                                <ul class="list-inline count">
                                    <li class="hours">01</li>
                                    <li class="minutes">01</li>
                                    <li class="seconds">01</li>
                                </ul>
                            </li>
                            <li class="text">
                                <ul class="list-inline">
                                    <li>  時間  </li>
                                    <li>  分  </li>
                                    <li>  秒  </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="form_wrap">
                        <h3>  急いで  商品数は  <br/>  限られています!  </h3>

                        <button class="jsOpenWindow _bottom">注文</button>

                    </li>
                </ul>
            </div>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/secondPage.js"></script>
    <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jquery.msgerror.js"></script>
    <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jpcheckpref.js"></script>
    <script src="//st.acstnst.com/content/Goji_Cream_JP_Red/js/jpgetpref.js"></script>
    <div class="hidden-window">
        <div class="h-headerj">
            <div class="containerz clearfix">
                <p>Goji Cream</p>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader"><b>代金引換</b></div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img alt="alt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/imagesSec/dtc1.png"/></div>
                                    <div class="dtable-cell">ご注文</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img alt="alt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/imagesSec/dtc2.png"/></div>
                                    <div class="dtable-cell">発送</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img alt="alt1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/imagesSec/dtc3.png"/></div>
                                    <div class="dtable-cell">配達とお支払い</div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">今だけのキャンペーン</div>
                        <form action="" class="js_scrollForm" method="post">

                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAK0BQAAAAAAAAAAAARIvQuoAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="6260.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAK0BQQFOyeKAAEAAQACPgUBAAJFCQIPAQJtBwTIAjrTAA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="Goji cream - JP">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Goji_Cream_JP_Red">
                            <input type="hidden" name="price" value="6260">
                            <input type="hidden" name="old_price" value="12520">
                            <input type="hidden" name="al" value="2373">
                            <input type="hidden" name="total_price_wo_shipping" value="6260.0">
                            <input type="hidden" name="currency" value="￥">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="JP">
                            <input type="hidden" name="shipment_vat" value="0.0">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 6260, 'old_price': 12520, 'shipment_price': 0}, u'3': {'price': 12520, 'old_price': 25040, 'shipment_price': 0}, u'5': {'price': 18780, 'old_price': 37560, 'shipment_price': 0}}">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.0">

                            <!-- -->
                           <!-- <div class="formHeader">ご注文のためにはデータ入力お願いします</div>
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="JP">日本</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1本</option>
                                <option selected="selected" value="3">2本＋１本プレゼント</option>
                                <option value="5">３本で＋２本をプレゼント！</option>
                            </select>
                            <div class="clearfix">
                                <div class="field">
                                    <input class="defvalue inp form-control" name="name" placeholder="氏名" type="text" value=""/>
                                </div>
                                <div class="field">
                                    <input class="defvalue inp posabs" name="postal_code" placeholder="郵便番号" type="text" value=""/>
                                    <input class="small-qwe inp" name="qwe1" placeholder="郵便番号" style="width: 100px;" type="text" value=""/>
                                    <span class="def">—</span>
                                    <input class="small-qwe inp" name="qwe2" placeholder="郵便番号" style="width: 100px;" type="text" value=""/>
                                </div>
                                <div class="field">
                                    <input class="inp" name="prefecture" placeholder="都道府県" type="text"/>
                                </div>
                                <div class="field">
                                    <input class="defvalue inp" name="address" placeholder="市区町村・町名・番地" type="text" value=""/>
                                </div>
                                <div class="field">
                                    <input class="defvalue inp" name="address_detail" placeholder="ビル・マンション名" type="text" value=""/>
                                </div>
                                <div class="field">
                                    <input class="defvalue only_number inp" name="phone" placeholder="電話番号" type="text" value=""/>
                                </div>
                            </div>
                            <div class="text-center totalpriceForm">総額:  <span class="js_total_price js_full_price"> </span>
                            </div>
                            <div class="zbtn js_pre_submit transitionmyl">注文</div>
                            <div class="js_submit" style="display: none;"></div>-->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">保証します。</div>
                            <ul>
                                <li><b>100%</b> 高質</li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1">
                            <div class="zDiscount"><b>注</b>:
                                <br/><span>今だけの５０％割引</span></div>
                            <div class="item__right">
                                <div class="zHeader">1本</div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> -->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">送料無料</div>
                                            <div class="dtable-cell"><b></b> <b class="js-pp-new">6260</b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration"><i></i>
<text class="js-pp-old">12520</text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">元価格</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">代引手数料無料</div>
                                            <div class="dtable-cell"><b></b> <b class="js-pp-ship">0</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">金額:</div>
                                            <div class="dtable-cell prtotal"> <b>
                                                    <text class="js-pp-full">6260</text>
                                                </b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button " data-package-id="1"></div>
                                <div class="img-wrapp"><img alt="z1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/imagesSec/zit1.png"/></div>
                                <div class="zHeader"><span>ベーシックセット</span></div>
                                <text>使用1ヶ月後の目に見える若返り</text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 active">
                            <div class="zstick"></div>
                            <div class="zDiscount"><b>注</b>:
                                <br/><span>今だけの５０％割引</span></div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> 本 <span class="dib zplus">本プレゼント</span></div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> -->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">送料無料</div>
                                            <div class="dtable-cell"><b>
                                                    <text class="js-pp-new">12520</text>
                                                </b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"><span class="dib old-pricedecoration"><i></i><span class="js-pp-old">25040</span></span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">元価格</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">代引手数料無料</div>
                                            <div class="dtable-cell"><b>
                                                    <text class="js-pp-ship">0</text>
                                                </b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">金額:</div>
                                            <div class="dtable-cell prtotal"><b> <span class="js-pp-full">12520</span>
                                                </b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3"></div>
                                <div class="img-wrapp">
                                    <div class="gift">1パック無料</div>
                                    <img alt="z1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/imagesSec/zit2.png"/></div>
                                <div class="zHeader"><span>若さへの道Q</span></div>
                                <text>わずか3ヶ月で深いシワへのリフティング効果</text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount"><b>注</b>:
                                <br/><span>今だけの５０％割引</span></div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> 本で <span class="dib zplus sec">本をプレゼント！</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst"> 送料無料</div>
                                            <div class="dtable-cell"><b>
                                                    <text class="js-pp-new">18780</text>
                                                </b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"><span class="dib old-pricedecoration"><i></i><text class="js-pp-old">37560</text></span></div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">元価格</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">代引手数料無料</div>
                                            <div class="dtable-cell"><b>
                                                    <text class="js-pp-ship">0</text>
                                                </b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">合計金額:</div>
                                            <div class="dtable-cell prtotal"><b>
                                                    <text class="js-pp-full">18780</text>
                                                </b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5"></div>
                                <div class="img-wrapp">
                                    <div class="gift">2パック無料</div>
                                    <img alt="z1" src="//st.acstnst.com/content/Goji_Cream_JP_Red/imagesSec/zit3.png"/></div>
                                <div class="zHeader sm"><span>若返りを最大化に！</span></div>
                                <text>このセットで6ヶ月間お使いいただけます。年齢に関する既存の変化を管理し、その再発を防ぐのに役立ちます。</text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
                <!-- endhidden -->
            </section></div>
    </div>
    </body>
    </html>
<?php } ?>