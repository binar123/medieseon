<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7514 -->
        <script>var locale = "lv";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALYEwS8xSaKAAEAAQACQRMBAAJaHQK-AQEABFKvl3EA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 6;
            var name_hint = 'Gokhan Karakoc';
            var phone_hint = '+105469655610';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("lv");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <title>GOJI CREAM - atjaunošanas krēm</title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
        <link href="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/css/bootstrap.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/css/style.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/js/bootstrap.min.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/js/scripts.js"></script>
    </head>
    <body>
    <div class="wrapper">
        <div class="header">
            <div class="container">
                <div class="logo text-center">
                    <a class="order" href="#"> Goji Cream </a>
                    <p>atjaunošanas krēms</p>
                </div>
                <h1 class="text-center title">1000 ogu spēks 1 krēma pilienā</h1>
                <div class="right_side">
                    <h3>Nobriedušas godži ogas satur:</h3>
                    <ul class="list-unstyled">
                        <li><span>ap 20 visvērtīgāko aminoskābju</span></li>
                        <li class="padding"><span>C vitamīnu (vitamīna koncentrācija 500 reizēs lielāka kā citrusaugos)</span></li>
                        <li><span>vitamīnu E</span></li>
                        <li class="padding"><span>dzelzi (15 reizēs vairāk kā ābolos)</span></li>
                        <li><span>B grupas vitamīnu kompleksu</span></li>
                        <li class="padding"><span>betaīnu</span></li>
                    </ul>
                    <div class="img_wrapper">
                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/zit1.png"/>
                    </div>
                    <div class="wrap">
                        <div class="rotate">
                            <p>Krēma formula nodrošina atbrīvošanos no grumbām</p>
                            <strong>tikai 14 dienu laikā!</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_1">
            <div class="container text-center">
                <h3>Paspējiet pasūtīt inovatīvu Goji Cream kosmētikas līdzekli par vēl nebijušo zemo cenu!</h3>
                <div class="price">
                    <span class="new js_new_price_curs">24€</span>
                    <span class="old js_old_price_curs">48€</span>
                </div>
                <div class="green_btn j-btn order scroll-to">Gribu būt jauna!</div>
            </div>
        </div>
        <div class="section_2">
            <div class="container">
                <h2 class="title">Kā darbojās Goji Cream?</h2>
                <ul class="list-inline">
                    <li class="picture">
                        <ul class="list-unstyled">
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/img_1.jpg"/></div>
                                <p>Satur savā sastāvā dažādus komponentus, kuri aizsargā no ultravioletiem stariem, kā arī bioloģiski aktīvus atjaunojušus elementus, kuri palīdz ādas šūnu reģenerācijai un restrukturizācijai.</p>
                            </li>
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/img_2.jpg"/></div>
                                <p>Palīdz novērst atkārtotu grumbu parādīšanos bagātinot sejas, kakla un plecu zonas ādu ar kompleksu, kurš satur plašu vitamīnu, mikroelementu un aminoskābju kompleksu.</p>
                            </li>
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/img_3.jpg"/></div>
                                <p>Goji Cream, aktivizējot dabīgu kolagēna izstrādi ādas dziļākajos slāņos, atjauno ādas elastību un svaigumu visīsākā laikā!</p>
                            </li>
                        </ul>
                        <h3 class="text-center">Kļūstiet jaunāki par 10 gadiem tikai 14 dienu laikā! tas ir iespējams!</h3>
                        <button class="green_btn j-btn order scroll-to">Gribu būt jauna!</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_3">
            <div class="container">
                <h2 class="title">Uzklāšanas etapi</h2>
                <ul class="list-inline">
                    <li class="step_1">
                        <div class="img_wrap">
                            <h3>1. Solis</h3>
                        </div>
                        <p>Maigi un saudzīgi attīriet ādu no kosmētikas paliekām un piesārņojuma ar sejas tīrīšanas līdzekli vai toniku.</p>
                    </li>
                    <li class="step_2">
                        <div class="img_wrap">
                            <h3>2. Solis</h3>
                        </div>
                        <p>Uzklājiet krēmu uz attīrītu ādu. Ar apļveida masāžas kustībām iemasējiet to ādā, izvairoties pieskarties ādai ap acīm.</p>
                    </li>
                    <li class="step_3">
                        <div class="img_wrap">
                            <h3>3. Solis</h3>
                        </div>
                        <p>Āda ir momentāni savilkusies un dziļi samitrināta. Krēms turpina aktīvi darboties 24 stundu laikā kopš tā uzklāšanas brīža!</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wiki_section">
            <div class="container">
                <ul class="list-inline">
                    <li><img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/wiki.jpg"/></li>
                    <li class="text">
                        <h3>Kāpēc grumbas ir bīstamas?</h3>
                        <p style="text-align: justify;">Grumbas – ādas redzamās krokas, kuras rodas dēļ pārliekas sejas muskuļu darbības, zaudētas ādas elastības un citiem faktoriem. Elastību ādai piešķir dermā esošās kolagēna šķiedras, bet tvirtumu – starp tām esošās hialurona skābes molekulas.</p>
                        <p style="text-align: justify;">Grumbas pārklāj un nospiež asinsvadus, tādējādi pasliktinot asins un limfas cirkulāciju. Ādā nepiekļūst skābeklis un parādās arvien jaunas un jaunas grumbas, bet vecās kļūst dziļākas un pamanāmākas. Gadījumā, ja savlaicīgi netiek uzsākta pretgrumbu ārstēšana process kļūst neatgriezenisks.</p>
                        <p style="text-align: justify;">Grumbu apkarošanas metodes: krēmi, maskas, botoska injekcijas, ķīmiskais pīlings un plastiskās operācijas. Pēdējas trīs metodes tiek uzskatītas par visbīstamākajiem.</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_4">
            <div class="container">
                <div class="left_side">
                    <h2 class="title">Godži ogu labums</h2>
                    <p style="text-align: justify;">Pretēji marketologu apgalvojumiem, žāvētās ogas praktiski nesatur C vitamīnu. Lai organisms saņemtu tādu antioksidantu daudzumu, kādu satur parastais sarkanais ābols, ir nepieciešams apēst tādu žāvēto ogu daudzumu, kas būtu vienāds ar vienu kilogramu. Tajā pat laikā svaigajās ogās C vitamīna daudzums ir 500 reizēs lielāks kā apelsīnos.</p>
                </div>
            </div>
        </div>
        <div class="section_5">
            <div class="container">
                <div class="right_side">
                    <h2 class="title">Eksperta viedoklis</h2>
                    <p>Mūsu kosmētikā mēs dodam priekšroku tikai naturāliem komponentiem. Kompānija “Hendel” izmanto inovatīvās metodes efektīvu kopšanas līdzekļu līnijas radīšanā.</p>
                    <p>Pirms iziet tirgū ar mūsu piedāvājamām precēm, mūsu kosmētika iziet striktu dermatoloģisku kontroli un klīnisku testēšanu. GOJI CREAM apliecināja savu augsto efektivitāti grumbu apkarošanai un ādas tvirtuma atjaunošanai.</p>
                    <br/>
                    <div class="picture">
                        <div class="before">Pirms</div>
                        <div class="after">Pēc</div>
                    </div>
                    <ul class="list-unstyled">
                        <li>97 no 100 no testēšanā iesaistītajiem rezultāts bija pamanāms jau pēc pirmā pielietojuma.</li>
                        <li>93% nelielas un nedziļas grumbas pazudušas pēc 10 maskas lietošanas dienam.</li>
                    </ul>
                    <button class="red_btn j-btn scroll-to">Uzsākt ārstēšanu</button>
                </div>
            </div>
            <div class="flower"></div>
        </div>
        <div class="section_6">
            <div class="container">
                <h2 class="title">Atsauksmes</h2>
            </div>
            <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/50/1.png"/>
                                        <span>Pirms</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/50/2.png"/>
                                        <span>Pēc</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">Es aizraujos ar ķīniešu medicīnu un mēģinu piemeklēt ķīniešu izcelsmes sejas krēmus, tie nepārprotami ir labāki par mūsu, ņemot vērā 5000 gadus ilgu Ķīnas vēsturi, kuras gaitā viņi iemācījušies saglabāt skaistumu. Bet man ir jau 50 un pēdējā laikā es sajutu, ka manas sejas ādas tvirtums un kvalitāte sāka sagādāt man raizes. Man parādījās arī lauvas kroka zem zoda un grumbiņas virs deguna kļuva arvien pamanāmas.</p>
                                    <p style="text-align: justify;">Mans kosmetologs ieteicis man izmēģināt GOJI CREAM, sakot, ka tas vienlīdz ir gan inovatīvs produkts, gan arī iemieso sevī tradicionālo ķīniešu ādas atjaunošanas līdzekli uz godži ogu pamata. Pēc divām lietošanas nedēļām āda tik tiešām kļuvusi tvirtāka, bet grumbiņas acīmredzami izlīdzinājās. Tagad es regulāri izmantoju šo krēmu.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/50/3.png"/>
                                        <span>Pirms</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/50/4.png"/>
                                        <span>Pēc</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">s mīlu visu jaunu un inovatīvu, tāpēc nevarēju palaist garām tādu jaunumu kā inovatīvo sejas atjaunošanas krēmu GOJI CREAM. Man ir gandrīz 60, bet seja vēl arvien izskatās ļoti labi, bet tikai pateicoties tam, ka es rūpējos par sevi un kopju savu seju.</p>
                                    <p style="text-align: justify;">Ko lai pasaka, krēma struktūra ir patīkama, viegla, krēms nesmērē pirkstus, ātri iesūcas ādā, bet grumbas tiek izgludinātas gluži kā ar karstu gludekli. Efekts kļūst pamanāms diezgan ātri, jau pēc pāris nedēļām. Šis krēms ideāli der manai ādai.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/50/5.png"/>
                                        <span>Pirms</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/mobile/img/50/6.png"/>
                                        <span>Pēc</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p style="text-align: justify;">Es mierīgi varu ieteikt GOJI CREAM sievietēm vecumā, tas tik tiešām palīdz. Godži ogas vispār ir ļoti interesants produkts, šis ogas satur sevī tik daudz brīnišķīgu komponentu, ka nav brīnums – tie var tikt galā pat ar grumbām.</p>
                                    <p style="text-align: justify;">Šo krēmu man uzdāvināja viena no mana skolnieka mamma un, ziniet, manā mūžā es saņēmu tik daudz dāvanu, bet nekad vēl nebiju redzējusi, ka āda var tik ātri izlīdzināties, pazūd “zoss pēdiņās” un mazās mīmiskās grumbiņas. Pagaidām lietoju to divas nedēļas un efekts ir burtiski saskatāms, tāpēc, domāju, ka tikšu galā arī ar lielajām grumbām.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Controls -->
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="2" data-target="#carousel-example-generic"></li>
                </ol>
                <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('prev');" role="button"></a>
                <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
            </div>
        </div>
        <div class="footer">
            <div class="container text-center">
                <h2>Tikai šodien jūs varat izdarīt pasūtījumu goji cream ar 50% atlaidi!</h2>
                <ul class="list-inline">
                    <li class="timmer_section">
                        <h3 class="old_price">Vecā cena: <span class="js_old_price_curs">48€</span></h3>
                        <h3 class="new_price">Jaunā cena: <br/><span class="js_new_price_curs">24€</span></h3>
                        <ul class="list-unstyled timme_wrap">
                            <li class="left">Līdz akcijas beigām palika:</li>
                            <li class="timmer">
                                <ul class="list-inline count">
                                    <li class="hours">01</li>
                                    <li class="minutes">01</li>
                                    <li class="seconds">01</li>
                                </ul>
                            </li>
                            <li class="text">
                                <ul class="list-inline">
                                    <li>stundas</li>
                                    <li>minūtes</li>
                                    <li>sekundes</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="form_wrap">
                        <h3>Steidzaties! Preču daudzums ir ierobežots!</h3>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIALYEwAAAAAAAAAAAARQPtDZAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="30.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIALYEwS8xSaKAAEAAQACQRMBAAJaHQK-AQEABFKvl3EA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Goji_cream_LV_red_cream">
                            <input type="hidden" name="price" value="24">
                            <input type="hidden" name="old_price" value="48">
                            <input type="hidden" name="al" value="7514">
                            <input type="hidden" name="total_price_wo_shipping" value="24.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="0">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="LV">
                            <input type="hidden" name="shipment_vat" value="0.0">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{}">
                            <input type="hidden" name="shipment_price" value="6">
                            <input type="hidden" name="price_vat" value="0.0">

                            <div class="inp select j-count">
                                <select class="country" id="country_code_selector" name="country">
                                    <option value="LV">Latvija</option>
                                </select>
                            </div>
                            <input name="name" placeholder="Vārds, Uzvārds" type="text"/>
                            <input class="only_number" name="phone" placeholder="Telefons" type="text"/>

                            <!--div class="form-price">
                                                  <span>Piegāde: 6 €</span> <br>
                                                  <span>Kopējā cena: 30 €</span>
                                                  </div-->

                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7514 -->
        <script>var locale = "lv";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALYEwS8xSaKAAEAAQACQRMBAAJaHQK-AQEABFKvl3EA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 6;
            var name_hint = 'Gokhan Karakoc';
            var phone_hint = '+105469655610';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("lv");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <title>GOJI CREAM - atjaunošanas krēm</title>
        <meta charset="utf-8"/>
        <link href="//st.acstnst.com/content/Goji_cream_LV_red_cream/css/bootstrap.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Goji_cream_LV_red_cream/css/style.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Goji_cream_LV_red_cream/js/bootstrap.min.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_LV_red_cream/js/scripts.js"></script>
    </head>
    <body>
    <div class="wrapper">
        <div class="header">
            <div class="container">
                <div class="logo text-center">
                    <a class="order" href="#">Goji Cream</a>
                    <p>atjaunošanas krēms</p>
                </div>
                <h1 class="text-center title">1000 ogu spēks 1 krēma pilienā</h1>
                <div class="cream"></div>
                <div class="right_side">
                    <h3>Nobriedušas godži ogas satur:</h3>
                    <ul class="list-unstyled">
                        <li><span>ap 20 visvērtīgāko aminoskābju</span></li>
                        <li><span>C vitamīnu (vitamīna koncentrācija 500 reizēs lielāka kā citrusaugos)</span></li>
                        <li><span>vitamīnu E</span></li>
                        <li><span>dzelzi (15 reizēs vairāk kā ābolos)</span></li>
                        <li><span>B grupas vitamīnu kompleksu</span></li>
                        <li><span>betaīnu</span></li>
                    </ul>
                    <div class="wrap">
                        <div class="rotate">
                            <p>Krēma formula nodrošina atbrīvošanos no grumbām</p>
                            <strong>tikai 14 dienu laikā!</strong>
                        </div>
                    </div>
                </div>
                <div class="animph">
                    <div class="textimg do active">Pirms</div>
                    <div class="wrapimg"><div class="textimg do active">Pēc</div></div>
                </div>
            </div>
        </div>
        <div class="section_1">
            <div class="container text-center">
                <h3>Paspējiet pasūtīt inovatīvu Goji Cream kosmētikas līdzekli par vēl nebijušo zemo cenu!</h3>
                <div class="wrap_1"><span>50% atlaide</span></div>
                <div class="wrap_2"><span>noliktavā palika <label>15</label> tikai preces vienības akcijas ietvaros</span></div>
                <div class="price">
                    <span class="new js_new_price_curs">24€</span>
                    <span class="old js_old_price_curs">48€</span>
                </div>
                <div class="green_btn j-btn order scroll-to">Pasūtīt</div>
            </div>
        </div>
        <div class="section_2">
            <div class="container">
                <h2 class="title">Kā darbojās Goji Cream?</h2>
                <ul class="list-inline">
                    <li class="letter">
                        <p>Ir klīniski pierādīs, ka GOJI CREAM palēnina šūnu novecošanas procesus un normalizē to funkcionēšanu.</p>
                        <p>Plašais vitamīnu un minerālu spektrs, kā arī krēma sastāvā ietilpstošais BIOTĪNS veido smago molekulu, kura nokļūst ādas pašās dziļākajās kārtās, lai padarītu GOJI CREAM iedarbību maksimāli efektīvu.</p>
                        <div class="text-center">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/element.jpg"/>
                        </div>
                        <p>Godži ogu aminoskābēm piemīt spēcīgas dabīgo antioksidantu īpašības. Tas ļauj krēmam aktīvi darboties visu 24 stundu laikā. Aminoskābes darbojās līdzīgi švammei, kura notver transepidermālu ūdens zudumu, kas rezultātā sniedz dziļu ādas mitrinājumu un acīmredzamo grumbu mazinājumu.</p>
                        <div class="red_wrap">Nesatur hormonus</div>
                    </li>
                    <li class="picture">
                        <ul class="list-unstyled">
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/img_1.jpg"/></div>
                                <p>Satur savā sastāvā dažādus komponentus, kuri aizsargā no ultravioletiem stariem, kā arī bioloģiski aktīvus atjaunojušus elementus, kuri palīdz ādas šūnu reģenerācijai un restrukturizācijai.</p>
                            </li>
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/img_2.jpg"/></div>
                                <p>Palīdz novērst atkārtotu grumbu parādīšanos bagātinot sejas, kakla un plecu zonas ādu ar kompleksu, kurš satur plašu vitamīnu, mikroelementu un aminoskābju kompleksu.</p>
                            </li>
                            <li>
                                <div class="img_wrap"><img alt="" class="round" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/img_3.jpg"/></div>
                                <p>Goji Cream, aktivizējot dabīgu kolagēna izstrādi ādas dziļākajos slāņos, atjauno ādas elastību un svaigumu visīsākā laikā!</p>
                            </li>
                        </ul>
                        <h3 class="text-center">Kļūstiet jaunāki par 10 gadiem tikai 14 dienu laikā! tas ir iespējams!</h3>
                        <button class="green_btn j-btn order scroll-to">Gribu būt jauna!</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_3">
            <div class="container">
                <h2 class="title">Uzklāšanas etapi</h2>
                <ul class="list-inline">
                    <li class="step_1">
                        <div class="img_wrap">
                            <h3>1. Solis</h3>
                        </div>
                        <p>Maigi un saudzīgi attīriet ādu no kosmētikas paliekām un piesārņojuma ar sejas tīrīšanas līdzekli vai toniku.</p>
                    </li>
                    <li class="step_2">
                        <div class="img_wrap">
                            <h3>2. Solis</h3>
                        </div>
                        <p>Uzklājiet krēmu uz attīrītu ādu. Ar apļveida masāžas kustībām iemasējiet to ādā, izvairoties pieskarties ādai ap acīm.</p>
                    </li>
                    <li class="step_3">
                        <div class="img_wrap">
                            <h3>3. Solis</h3>
                        </div>
                        <p>Āda ir momentāni savilkusies un dziļi samitrināta. Krēms turpina aktīvi darboties 24 stundu laikā kopš tā uzklāšanas brīža!</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wiki_section">
            <div class="container">
                <ul class="list-inline">
                    <li><img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/wiki.jpg"/></li>
                    <li class="text">
                        <h3>Kāpēc grumbas ir bīstamas?</h3>
                        <p>Grumbas – ādas redzamās krokas, kuras rodas dēļ pārliekas sejas muskuļu darbības, zaudētas ādas elastības un citiem faktoriem. Elastību ādai piešķir dermā esošās kolagēna šķiedras, bet tvirtumu – starp tām esošās hialurona skābes molekulas.</p>
                        <p>Grumbas pārklāj un nospiež asinsvadus, tādējādi pasliktinot asins un limfas cirkulāciju. Ādā nepiekļūst skābeklis un parādās arvien jaunas un jaunas grumbas, bet vecās kļūst dziļākas un pamanāmākas. Gadījumā, ja savlaicīgi netiek uzsākta pretgrumbu ārstēšana process kļūst neatgriezenisks.</p>
                        <p>Grumbu apkarošanas metodes: krēmi, maskas, botoska injekcijas, ķīmiskais pīlings un plastiskās operācijas. Pēdējas trīs metodes tiek uzskatītas par visbīstamākajiem.</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="section_4">
            <div class="container">
                <div class="left_side">
                    <h2 class="title">Godži ogu labums</h2>
                    <p>Pretēji marketologu apgalvojumiem, žāvētās ogas praktiski nesatur C vitamīnu. Lai organisms saņemtu tādu antioksidantu daudzumu, kādu satur parastais sarkanais ābols, ir nepieciešams apēst tādu žāvēto ogu daudzumu, kas būtu vienāds ar vienu kilogramu. Tajā pat laikā svaigajās ogās C vitamīna daudzums ir 500 reizēs lielāks kā apelsīnos.</p>
                </div>
            </div>
        </div>
        <div class="section_5">
            <div class="container">
                <div class="right_side">
                    <h2 class="title">Eksperta viedoklis</h2>
                    <p>Mūsu kosmētikā mēs dodam priekšroku tikai naturāliem komponentiem. Kompānija “Hendel” izmanto inovatīvās metodes efektīvu kopšanas līdzekļu līnijas radīšanā.</p>
                    <p>Pirms iziet tirgū ar mūsu piedāvājamām precēm, mūsu kosmētika iziet striktu dermatoloģisku kontroli un klīnisku testēšanu. GOJI CREAM apliecināja savu augsto efektivitāti grumbu apkarošanai un ādas tvirtuma atjaunošanai.</p>
                    <br/>
                    <div class="picture">
                        <div class="before">Pirms</div>
                        <div class="after">Pēc</div>
                    </div>
                    <ul class="list-unstyled">
                        <li>97 no 100 no testēšanā iesaistītajiem rezultāts bija pamanāms jau pēc pirmā pielietojuma.</li>
                        <li>93% nelielas un nedziļas grumbas pazudušas pēc 10 maskas lietošanas dienam.</li>
                    </ul>
                    <button class="red_btn j-btn scroll-to">Uzsākt ārstēšanu</button>
                </div>
                <div class="doctor">
                    <div class="wrap">
                        <div class="rotate">
                            <h4>Jacobs Vanags</h4>
                            <p>Uzņēmuma “Hendel” vadošais kosmētiskās līnijas izstrādes speciālists</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flower"></div>
        </div>
        <div class="section_6">
            <div class="container">
                <h2 class="title">Atsauksmes</h2>
                <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
                    <ol class="carousel-indicators">
                        <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                        <li data-slide-to="1" data-target="#carousel-example-generic"></li>
                        <li data-slide-to="2" data-target="#carousel-example-generic"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/50/1.png"/>
                                        <span>Pirms</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/50/2.png"/>
                                        <span>Pēc</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p>Es aizraujos ar ķīniešu medicīnu un mēģinu piemeklēt ķīniešu izcelsmes sejas krēmus, tie nepārprotami ir labāki par mūsu, ņemot vērā 5000 gadus ilgu Ķīnas vēsturi, kuras gaitā viņi iemācījušies saglabāt skaistumu. Bet man ir jau 50 un pēdējā laikā es sajutu, ka manas sejas ādas tvirtums un kvalitāte sāka sagādāt man raizes. Man parādījās arī lauvas kroka zem zoda un grumbiņas virs deguna kļuva arvien pamanāmas.</p>
                                    <p>Mans kosmetologs ieteicis man izmēģināt GOJI CREAM, sakot, ka tas vienlīdz ir gan inovatīvs produkts, gan arī iemieso sevī tradicionālo ķīniešu ādas atjaunošanas līdzekli uz godži ogu pamata. Pēc divām lietošanas nedēļām āda tik tiešām kļuvusi tvirtāka, bet grumbiņas acīmredzami izlīdzinājās. Tagad es regulāri izmantoju šo krēmu.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="item">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/50/3.png"/>
                                        <span>Pirms</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/50/4.png"/>
                                        <span>Pēc</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p>Es mīlu visu jaunu un inovatīvu, tāpēc nevarēju palaist garām tādu jaunumu kā inovatīvo sejas atjaunošanas krēmu GOJI CREAM. Man ir gandrīz 60, bet seja vēl arvien izskatās ļoti labi, bet tikai pateicoties tam, ka es rūpējos par sevi un kopju savu seju.</p>
                                    <p>Ko lai pasaka, krēma struktūra ir patīkama, viegla, krēms nesmērē pirkstus, ātri iesūcas ādā, bet grumbas tiek izgludinātas gluži kā ar karstu gludekli. Efekts kļūst pamanāms diezgan ātri, jau pēc pāris nedēļām. Šis krēms ideāli der manai ādai.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="item">
                            <ul class="list-inline">
                                <li class="photo">
                                    <div class="img_before round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/50/5.png"/>
                                        <span>Pirms</span>
                                    </div>
                                    <div class="img_after round">
                                        <img alt="" src="//st.acstnst.com/content/Goji_cream_LV_red_cream/img/50/6.png"/>
                                        <span>Pēc</span>
                                    </div>
                                </li>
                                <li class="text">
                                    <p>Es mierīgi varu ieteikt GOJI CREAM sievietēm vecumā, tas tik tiešām palīdz. Godži ogas vispār ir ļoti interesants produkts, šis ogas satur sevī tik daudz brīnišķīgu komponentu, ka nav brīnums – tie var tikt galā pat ar grumbām.</p>
                                    <p>Šo krēmu man uzdāvināja viena no mana skolnieka mamma un, ziniet, manā mūžā es saņēmu tik daudz dāvanu, bet nekad vēl nebiju redzējusi, ka āda var tik ātri izlīdzināties, pazūd “zoss pēdiņās” un mazās mīmiskās grumbiņas. Pagaidām lietoju to divas nedēļas un efekts ir burtiski saskatāms, tāpēc, domāju, ka tikšu galā arī ar lielajām grumbām.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('prev');" role="button"></a>
                    <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" onclick="$('#carousel-example-generic').carousel('next');" role="button"></a>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container text-center">
                <h2>Tikai šodien jūs varat izdarīt pasūtījumu goji cream ar 50% atlaidi!</h2>
                <ul class="list-inline">
                    <li class="timmer_section">
                        <h3 class="old_price">Vecā cena: <span class="js_old_price_curs">48€</span></h3>
                        <h3 class="new_price">Jaunā cena: <br/><span class="js_new_price_curs">24€</span></h3>
                        <ul class="list-unstyled timme_wrap">
                            <li class="left">Līdz akcijas beigām palika:</li>
                            <li class="timmer">
                                <ul class="list-inline count">
                                    <li class="hours">01</li>
                                    <li class="minutes">01</li>
                                    <li class="seconds">01</li>
                                </ul>
                            </li>
                            <li class="text">
                                <ul class="list-inline">
                                    <li>stundas</li>
                                    <li>minūtes</li>
                                    <li>sekundes</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="form_wrap">
                        <h3>Steidzaties! Preču daudzums ir ierobežots!</h3>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIALYEwAAAAAAAAAAAARQPtDZAA"></iframe>

                            <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="total_price" value="30.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIALYEwS8xSaKAAEAAQACQRMBAAJaHQK-AQEABFKvl3EA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Goji_cream_LV_red_cream">
                            <input type="hidden" name="price" value="24">
                            <input type="hidden" name="old_price" value="48">
                            <input type="hidden" name="al" value="7514">
                            <input type="hidden" name="total_price_wo_shipping" value="24.0">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="0">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="LV">
                            <input type="hidden" name="shipment_vat" value="0.0">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="package_prices" value="{}">
                            <input type="hidden" name="shipment_price" value="6">
                            <input type="hidden" name="price_vat" value="0.0">

                            <div class="inp select j-count">
                                <select class="country" id="country_code_selector" name="country">
                                    <option value="LV">Latvija</option>
                                </select>
                            </div>
                            <input name="name" placeholder="Vārds, Uzvārds" type="text"/>
                            <input class="only_number" name="phone" placeholder="Telefons" type="text"/>
                            <!--div class="form-price">
                                                  <span>Piegāde: 6 €</span> <br>
                                                  <span>Kopējā cena: 30 €</span>
                                                  </div-->

                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </body>
    </html>
<?php } ?>