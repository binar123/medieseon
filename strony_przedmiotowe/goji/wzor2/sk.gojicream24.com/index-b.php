<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7232 -->
        <script>var locale = "sk";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAL_EgQj-ACMAAEAAQACZhIBAAJAHAIGAQEABLahGn8A";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Martin Mičiak';
            var phone_hint = '0908267142';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("sk");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <title> GOJI CREAM </title>
        <link href="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/css/bxslider.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/css/style_long.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/css/style_second.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/js/jquery.bxslider.min.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/js/script.js"></script>
        <script src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/js/main.js"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    $(".js_errorMessage2").remove();
                    $(".js_errorMessage").remove();
                    var errors = 0,
                        form = $(this).closest('form'),
                        name = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        countryp = form.find('[id="country_code_selector"]').val(),
                        namep = name.val(),
                        phonep = phone.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;
                    if(name.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(name, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(name, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        var mas={};
                        form.find('input,textatea,select').each(function(){
                            mas[$(this).attr('name')]=$(this).val();
                        });
                        $.post('/order/create_temporary/', mas);
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
    <div class="corb-wrapper">
        <header id="s1">
            <div class="container">
                <img alt="" class="s1_l1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s1_l1.jpg"/>
                <div class="s1_txt_1 upper cnd"> NAJLEPSÍ PROSTRIEDOK
                    PROTI VRASKAM
                </div>
                <img alt="" class="s1i2" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s1i2.jpg"/>
            </div>
        </header>
        <div class="clearfix"></div>
        <section class="s13" id="s13">
            <div class="container">
                <div class="s13_items ">
                    <div class="s13_item today">
                        <div class="s13_sale">ZĽAVA 50%</div>
                        <div class="s13_desc">Ponuka končí za:</div>
                        <div class="count_div">
                            <ul class="count_line">
                                <li>
                                    <span class="h"></span>
                                    <p> Hodín </p>
                                </li>
                                <li>:</li>
                                <li>
                                    <span class="m"></span>
                                    <p> Sekúnd </p>
                                </li>
                                <li>:</li>
                                <li>
                                    <span class="s"></span>
                                    <p> Minút </p>
                                </li>
                                <br class="cb"/>
                            </ul>
                        </div>
                        <div class="btn pre_toform" data-href=".s100">Objednat</div>
                    </div>
                    <div class="cl"></div>
                </div>
            </div>
        </section>
        <section class="s2" id="s2">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> TIETO OTRAVNÉ NEDOSTATKY </span>
                    </div>
                </div>
                <img alt="" class="s2_i1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s2_i1.jpg"/>
                <p class="text-center">
                    Vrasky - to ryhy a zahyby koze, ktoré sa objavujú na tvari, krku, rukach a iných castiach tela. Objavujú
                    sa
                    v dôsledku prasknutia a poskodenia pruzných spojivových vlakien kolagénu a elastínu - dvoch
                    najdôlezitejsích
                    zloziek ludskej koze.
                </p>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s3 rel" id="s3">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> PRECO VZNIKAJÚ VRASKY? </span>
                    </div>
                </div>
                <div class="s3_inner rel">
                    <div class="basic_tit s3_tit_1 upper cnd rel"> HLAVNÝM DÔVODOM PRE VZNIK VRASOK JE NEGATÍVNY VPLYV
                        VONKAJSIEHO PROSTREDIA
                    </div>
                    <div class="s3_tit_2 upper cnd rel">
                        SLNECNÉ ZIARENIE PRISPIEVA K PREDCASNÉMU STARNUTIU KOZE
                    </div>
                    <p class="basic_p text-center">
                        Významnú úlohu vo vzhlade vrasok u ludí hra celý rad vonkajsích faktorov: Dlhotrvajúce vystavenie
                        slnku
                        a vetru, teplotné výkyvy vzduchu, jeho prílisna suchost alebo vlhkost. V snahe zachovat mladistvost
                        pleti je treba sa o nu aj spravne starat. Nevhodna kozmetika môze mat horsí vplyv na pokozku nez
                        ziadna
                        starostlivost. </p>
                    <img alt="" class="s3_i1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s3_i1.jpg"/>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s4" id="s4">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> AKO SA VYSPORIADAT S VRASKAMI?</span>
                    </div>
                </div>
                <div class="s4_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> INOVATÍVNA FORMULA NA VYSPORIADANIE SA S PROBLÉMAMI
                        STARNUTIA
                        PLETI
                    </div>
                    <p class="basic_p text-center">
                        Typické produkty k odstraneniu vrasok bojujú iba s vonkajsími prejavami problému, Goji krém sa
                        vyraba
                        pomocou vyspelej technológie, ktora umoznuje uzitocným latkam prenikat do hlbsích vrstiev koze,
                        odstranovanie zmeny súvisiace s vekom, ktoré majú vplyv na dôlezité procesy omladenia a regeneracie.
                    </p>
                    <div class="s4_i s4_i_1 upper text-center">
                        <span class="cnd"> HLADKA </span> PLET
                    </div>
                    <div class="s4_i s4_i_2 upper text-center">
                        <span class="cnd"> BEZ </span> NEDOSTATKOV
                    </div>
                    <div class="s4_i s4_i_3 upper text-center">
                        <span class="cnd"> ELIMINACIA </span> ZVNÚTRA
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s5" id="s5">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> ÚCINNA ÚLAVA OD VRASOK </span>
                    </div>
                </div>
                <div class="s5_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> INOVATÍVNY VÝVOJ V NEMECKU</div>
                    <p class="basic_p text-center">
                        Vrasky sa môzu objavit v dôsledku oslabenia imunity, klimatických zmien, UV ziarenia, prirodzeného
                        starnutia pokozky, dehydratacie, pouzitia nekvalitnej kozmetiky, ako aj nespravneho cistenia tvare.
                        Goji
                        Cream odstranuje vrasky bez ohladu na prícinu a vyriesi problém natrvalo.
                    </p>
                    <div class="s5_b">
                        <div class="s5_b_i rel">
                            <span> OCHRANNÉ VLASTNOSTI </span>
                            <p>
                                Zahŕna rôzne prísady na ochranu proti ultrafialovému ziareniu, ako aj znízuje biologicky
                                aktívne
                                prvky pre restrukturalizaciu kozných buniek.
                            </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span> HLBOKÉ PRENIKNUTIE </span>
                            <p>
                                Aktivaciou prirodzenej tvorby kolagénu v hlbsích vrstvach koze vracia Goji Cream pruznost a
                                mladost v okamihu!
                            </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span> JEMNA HYDRATACIA </span>
                            <p>
                                Hydratuje pokozku, neupchava póry a zaroven nespôsobuje nadmernú aktivitu kozných zliaz.
                                Goji
                                Cream neblokuje prístup kyslíka do buniek epidermis a zlepsuje krvný obeh v týchto
                                oblastiach.
                            </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span> ROVNOVAHA </span>
                            <p>
                                Kombinuje cistenie, hydrataciu, tonizaciu a výzivu, co vedie k rovnovahe aktivity mazových
                                zliaz, odstranuje dehydrataciu.
                            </p>
                        </div>
                    </div>
                    <img alt="" class="s5_i2" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s5_i2.jpg"/>
                    <div class="s5_b s5_b2">
                        <div class="s5_b_i rel">
                            <span> ÚCINNOST POUZITIA </span>
                            <p>
                                Pomaha zabranit opätovnému vzniku vrasok tým, ze obohacuje pokozku tvare a krku sirokým
                                spektrom
                                vitamínov, mineralov a aminokyselín.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s5_bottom">
                <div class="container">
                    <div class="s5_img_l">
                        <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s5_i3.jpg"/>
                    </div>
                    <div class="s5_img_r">
                        <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s5_i4.jpg"/>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s7" id="s7">
            <div class="s7_2">
                <div class="container">
                    <div class="rel">
                        <div class="ribbon abs upper cnd">
                            <span>NA ROZDIEL OD TRADICNÝCH SYSTÉMOV DOCHADZA K VYHLADENIU PLETI </span>
                        </div>
                    </div>
                    <div class="s7_inner rel">
                        <div class="basic_tit s4_tit_1 upper cnd rel">
                            RÝCHLO SA ZBAVTE VRASOK POMOCOU GOJI CREAM - NATRVALO
                        </div>
                        <div class="s5_b">
                            <div class="s5_b_i rel">
                                <p>
                                    Typické produkty na odstranenie vrasok majú vplyv len na povrchové vrstvy koze, takze sú
                                    neúcinné, na rozdiel od Goji Cream
                                </p>
                            </div>
                            <div class="s5_b_i rel">
                                <p>
                                    Goji Cream prenika do hlbsích vrstiev koze, kde odstranuje zmeny súvisiace s vekom, a
                                    nastartuje dôlezité procesy obnovy buniek a omladenie tvare.
                                </p>
                            </div>
                            <div class="s5_b_i rel">
                                <p>
                                    Goji Cream v kratkej dobe vrací spät pruznost pleti. Pokozka získava vnútorný elan,
                                    objaví
                                    sa prirodzený rumenec, vrasky sa vyhladzujú, pokozka je hydratovana a tónovana a vyzera
                                    uvolnene.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s9" id="s9">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span>VYHRATE NAD VRASKAMI </span>
                    </div>
                </div>
                <div class="s9_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> NAJLEPSIE RIESENIE PRE VASU POKOZKU</div>
                </div>
                <div class="s9_b1">
                    <div class="s9_item">
                        <div class="s9_item_tit">Aktívne zlozky</div>
                        <div class="s9_item_txt">
                            Krém pozostava z bobul kustovnice, lykopén, kyseliny hyaluronovej, bunecných antioxidantov,
                            ktoré
                            podporujú obnovu kozných buniek a z komplexu mineralov.
                        </div>
                    </div>
                    <div class="s9_item">
                        <div class="s9_item_tit">Úcinok</div>
                        <div class="s9_item_txt">
                            Vdaka mimoriadne bohatému zlozeniu krém prispieva k dosiahnutiu stabilného výsledku, vrasky sa
                            nevracajú a ak ano, sú takmer neznatelné. Plet tvare a krku sa stava hladka, pruzna a plna
                            vitalnej
                            energie, v dobrej kondícii.
                        </div>
                    </div>
                    <div class="s9_item">
                        <div class="s9_item_tit"> Pre koho je urcený</div>
                        <div class="s9_item_txt">zeny s drobnými a velkými vraskami.
                        </div>
                    </div>
                    <div class="s9_price">
                        Pôvodná cena: <span class="td-l">98 € </span><br/>
                        <span>cena: 49 € </span>
                    </div>
                    <div class="s9_action">
                        <button class="s9_buy toform pre_toform" data-href=".s100" type="button"> Objednajte</button>
                    </div>
                </div>
            </div>
        </section>
        <section class="s10" id="s10">
            <div class="s10_top">
                <div class="container">
                    <div class="rel">
                        <div class="ribbon abs upper cnd">
                            <span> VSETKY POZNATKY NEMECKÝCH ODBORNÍKOV V JEDNOM BALENÍ KRÉMU</span>
                        </div>
                    </div>
                    <div class="s10_inner rel text-center">
                        <div class="basic_tit s4_tit_1 upper cnd rel"> GOJI CREAM OBSAHUJE</div>
                    </div>
                </div>
            </div>
            <div class="s10_bottom">
                <div class="container">
                    <div class="slider">
                        <ul class="bxslider">
                            <li>
                                <div class="s10_b1_item s10_b1_item_1">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s10_i1.png"/>
                                    <div class="s10_b1_item_tit upper"> AKTÍVNA FORMA VITAMÍNU A</div>
                                    <p class="s10_b1_item_txt">
                                        je prítomný vo vsetkých maskach na tvar. Vitamín A znizuje aktivitu mazových zliaz,
                                        vysusuje mastnú plet, stahuje rozsírené póry, vyrovnava a vyhladzuje pokozku.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="s10_b1_item s10_b1_item_2">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s10_i2.png"/>
                                    <div class="s10_b1_item_tit upper"> DRASLÍK</div>
                                    <p class="s10_b1_item_txt"> zvlhcuje pokozku a chrani ju pred vysúsaním od
                                        predchadzajúcej
                                        zlozky.</p>
                                </div>
                            </li>
                            <li>
                                <div class="s10_b1_item s10_b1_item_3">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s10_i3.png"/>
                                    <div class="s10_b1_item_tit upper">VITAMÍN C</div>
                                    <p class="s10_b1_item_txt"> stimulator imunity, antioxidant, chrani pokozku proti
                                        baktériam.</p>
                                </div>
                            </li>
                            <li>
                                <div class="s10_b1_item s10_b1_item_4">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s10_i4.png"/>
                                    <div class="s10_b1_item_tit upper"> VITAMÍN PP ( B3)</div>
                                    <p class="s10_b1_item_txt">Zlepsuje krv. Tento vitamín je nevyhnutný pre regeneraciu
                                        buniek. </p>
                                </div>
                            </li>
                            <li>
                                <div class="s10_b1_item s10_b1_item_5">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s10_i5.png"/>
                                    <div class="s10_b1_item_tit upper"> VITAMÍN K</div>
                                    <p class="s10_b1_item_txt">podporuje syntézu spojivových tkanív, pomaha k vyhladeniu
                                        pokozky. </p>
                                </div>
                            </li>
                        </ul>
                        <div class="slider-prev slider-prev2"></div>
                        <div class="slider-next slider-next2"></div>
                    </div>
                    <div class="s10_b2 rel">
                        <div class="ribbon abs upper cnd">
                            <span> TESTOVANIE LIECEBNÝCH ÚCINKOV GOJI CREAM </span>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel hpt">
                                DÔKAZY O ÚCINNOSTI OMLADZUJÚCICH VLASTNOSTÍ
                            </div>
                        </div>
                        <div class="s10_i3 s10_i3_1">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s10_i6.jpg"/>
                            <div class="s10_i3_block">
                                <div class="s10_i3_tit upper"> METÓDA ANALÝZY VÝROBKU</div>
                                <div class="s10_i3_txt"> cista kultúra Staphylococcus aureus bola umiestnena v prostredí
                                    proteínu. Potom sa na sterilný filtracný papier naniesol Goji Cream. Nakoniec sa
                                    dosticka
                                    obsahujúce vysetrované komponenty zahreje na 37 ° C a ponecha po dobu 24 hodín.
                                </div>
                            </div>
                            <div class="s10_i3_block">
                                <div class="s10_i3_tit upper"> VÝSLEDKY ANALÝZY</div>
                                <div class="s10_i3_txt">
                                    Na zaklade výsledkov mozno vyvodit zaver, ze Goji Cream ma vysokú úcinnost pri pouzití.
                                    To
                                    je dané predovsetkým tým, ze obsahuje vitamíny a mineraly, rovnako ako biotín, tieto
                                    latky
                                    sú spojené v molekule, a vdaka tomu prenikajú do hlbsích vrstiev, dodavanie zivín do
                                    epidermis je velmi efektívne a tam dochadza k omladeniu pleti.
                                </div>
                            </div>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel">
                                TESTOVANIE KRÉMU PROTI VRASKAM GOJI CREAM
                            </div>
                        </div>
                        <div class="s10_i3 s10_i3_2">
                            <div class="s10_i3_block s10_i3_block_3">
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s10_i7.jpg"/>
                                </div>
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> SKÚSOBNA METÓDA</div>
                                    <div class="s10_i3_txt"> Glycerín je zakladna súcast mazu. Goji Cream a ostatné
                                        prostriedky
                                        boli umiestnené na kvapku glycerolu a v priebehu 2 minút vidíme, ako dobre Goji
                                        Cream a
                                        ostatné prostriedky prenikajú dnu.
                                    </div>
                                </div>
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> VÝSLEDKY TESTOV</div>
                                    <div class="s10_i3_txt">
                                        O dve minúty neskôr sa kvapka bezných prostriedkov stale drzí na povrchu glycerínu,
                                        zatial co Goji Cream je rovnomerne rozdelený po povrchu glycerolu, rýchlo rozpustený
                                        a
                                        vDaka tomu, ze obsahuje sensitinogen, nedrazdi kozu, co je dôlezité najmä pre
                                        citlivú
                                        pokozku.
                                    </div>
                                </div>
                            </div>
                            <div class="basic_tit s4_tit_1 upper cnd rel">
                                TESTOVANIE HYDRATACNÝCH VLASTNOSTÍ
                            </div>
                            <div class="s10_i3_block s10_i3_block_4">
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s10_i10.jpg"/>
                                </div>
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> SKÚSOBNA METÓDA</div>
                                    <div class="s10_i3_txt">
                                        Je zobrazený obsah vlhkosti v pokozke pred a po pouzití. Pred a po aplikacii
                                        prostriedkov sa odeberie vzoriek, ktorý ukazuje na stupen nasýtenia pokozky
                                        vlhkostou;
                                        tento test ukazuje, ako dobre Goji Cream zvlhcuje pokozku.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="s10_b2 rel">
                        <div class="ribbon abs upper cnd">
                            <span> ZENY PO CELOM SVETE ODPORÚCAJÚ</span>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel hpt"> RECENZIE PRODUKTOV GOJI CREAM</div>
                        </div>
                    </div>
                    <div class="s10_b4 rel">
                        <div class="slider">
                            <ul class="bxslider">
                                <li>
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/slide_1.jpg"/>
                                    <div class="slide_tit"> Sofia Lehocká, 33 rokov</div>
                                    <p class="slider_p italic">
                                        Vzdy som vyzerala mladsia a bola som na to velmi hrda. Po vaznom ochorení som
                                        dostala
                                        depresie a nestarala som sa o seba, co ovplyvnilo moj vzhlad. Aby ma nejako
                                        podporil,
                                        priatel mi ponukol pracu v jeho spolocnosti. To zahŕnalo pracu s luDmi a vzhlad hra
                                        významnú úlohu. Bolo nutné získat spät atraktívny a mladistvý vzhlad tak rýchlo, ako
                                        to
                                        bolo mozné. Pomohol mi Goji Cream! Klienti, s ktorými som komunikovala si mysleli,
                                        ze
                                        som o 10 rokov mladsia!
                                    </p>
                                </li>
                                <li>
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/slide_2.jpg"/>
                                    <div class="slide_tit"> Živa Krempaská, 56 rokov</div>
                                    <p class="slider_p italic">
                                        Pozrite sa to, ako vela hodnotení ma tento krém. A to z dobrého dôvodu. Pouzívam
                                        krém
                                        asi rok a nikdy som nelutovala, ze som ho kúpila on-line. Je mi 56 rokov. Ale verte
                                        mi,
                                        vyzeram o dost mladsia. A musím, pretoze môj manzel je o 14 rokov mladsí. Takze,
                                        milí
                                        priatelia, nesetrite na sebe.
                                    </p>
                                </li>
                            </ul>
                            <div class="slider-prev"></div>
                            <div class="slider-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s12" id="s12">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> NEDAVNE HODNOTENIA </span>
                    </div>
                </div>
                <div class="s12_inner">
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> ANNA KAISEROVA, 42 ROKOV</div>
                            <div class="s12_tim">
                                <script type="text/javascript">dtime_nums(-5, true)</script>
                            </div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Po 30 sa mi zacali objavovat vrasky a bolo ich viac a viac kazdý rok. Obratila som sa na
                            kliniku,
                            pouzívala som drahé lieky a postupy, ale ich úcinok nebol trvalý. Hlavnou výhodou tohto produktu
                            je
                            jeho prístupnost. To je pre mna velmi dôlezité, umoznuje mi to pouzívat krém dôsledne.
                        </div>
                    </div>
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> MICHAELA GAVENDOVÁ, 34 ROKOV</div>
                            <div class="s12_tim">
                                <script type="text/javascript">dtime_nums(-8, true)</script>
                            </div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star_half.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Kamaratka mi poslala odkaz na internetové stranky a ja som si objednala. Spociatku som si
                            nevsimla
                            odlisnosti od bezných krémov (Ipouzívala som iba kozmetiku z lekarne). Ale ... teraz uplynul
                            mesiac.
                            Tak krasna moja plet nebola nikdy! Vyhodila som vsetku ostatnú kozmetiku. Nie som vôbec mastna!
                            Vsetky vrasky zmizli a nové sa neobjavili!
                        </div>
                    </div>
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> ELISKA NOVOTNA, 38 ROKOV</div>
                            <div class="s12_tim">
                                <script type="text/javascript">dtime_nums(-10, true)</script>
                            </div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Objednala som si mesiac pred Novým rokom. Úcinok je uspokojivý. Ak to tak bude aj nadalej,
                            myslím,
                            ze som si vybrala ten pravý krém. Dakujem. Odporúcam zenam po 35 rokoch
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s100">
            <div class="container">
                <div class="text-center s100_tit white"> OBJEDNAT</div>
                <form action="" id="order_form" class="order_form" method="post">
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abcdn.pro/forms/?target=-4AAIJIAL_EgAAAAAAAAAAAAQd6T5KAA"></iframe>

                    <div class="price-wrapp">
                        <p class="old-price">98€</p>
                        <p class="new-price">Cena: 49€</p>
                        <div class="clf"></div>
                        <div class="shipment_price" style="display: none;">Dodanie: 0 €</div>
                        <div class="total_price">Celkom: <span class="red">49 €</span>
                        </div>
                    </div>

                    <div class="toform"></div>
                </form>
            </div>
        </section>
        <div class="clearfix"></div>
    </div>
    <div class="hidden-window">
        <div class="new-header_goji">
            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/img/s1_l1.jpg"/>
        </div>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet"/>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">PLATBA NA DOBIERKU
                            </div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/imageSecond/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/imageSecond/dtc1.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/imageSecond/dtc2.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/imageSecond/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/imageSecond/dtc3.png"/></div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">OBJEDNÁVKA</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">POŠTOVNÉ</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">DODANIE A <span>PLATBA</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">FAIR OBCHOD</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 balenie</option>
                                <option data-slide-index="1" selected="selected" value="3">2 balení + darček!</option>
                                <option data-slide-index="2" value="5">3 balení + 2 darčeky!</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs" style="left: 45px;"><img alt="zt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/imageSecond/1.png"/></div>
                                            <div class="zHeader" style="font-size: 24px;padding-top: 30px;"><b>1</b> balenie
                                            </div>
                                            <br/>
                                        </div>
                                        <div class="pack_descr">
                                            Viditeľné omladenie už po mesiaci užívania
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Cena:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">49</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>98
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile"></div>
                                                    <img alt="zt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/imageSecond/2+1.png"/></div>
                                                <div class="zHeader"><b>2</b> balení
                                                    <br/> <span class="dib zplus">darček</span></div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">
                                            Kúra na mladosť! Liftingový účinok a vyhladenie najhlbších vrások len do troch
                                            mesiacov. Zaručený výsledok.
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Cena:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">98</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>196
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <div class="giftmobile"></div>
                                                <img alt="zt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/mobile/imageSecond/3+2.png" width=""/></div>
                                            <div class="zHeader"><b>3</b> balení
                                                <br/> <span class="dib zplus sec">darčeky</span></div>
                                        </div>
                                        <div class="pack_descr">
                                            Maximálne omladenie! Polročná kúra vás nielen zbaví zmien súvisiacich so
                                            starnutím, ale tiež zabráni ich znovuobjaveniu.
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Prezzo:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">147</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>294
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager"><a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a><a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a><a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a></div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAL_EgAAAAAAAAAAAAQd6T5KAA"></iframe>

                            <div class="js-ship-pr">+Poštovné <span class="js_delivery_price">0</span> €
                            </div>
                            <div class="text-center totalpriceForm">Celkom: <span class="js_total_price js_full_price">49 </span>
                                €
                            </div>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">GARANTUJEME</div>
                            <ul>
                                <li><b>100%</b> kvalita</li>
                                <li><b>Kontrola</b> produktu po obdržaní</li>
                                <li><b>Bezpečnosť </b> vašich dát</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7232 -->
        <script>var locale = "sk";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAL_EgQy5_-LAAEAAQACZhIBAAJAHAIGAQEABLGN39kA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Martin Mičiak';
            var phone_hint = '0908267142';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("sk");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="width=device-width, initial-scale=0.80" name="viewport"/>
        <title> GOJI CREAM </title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Goji_cream_SK_Long/css/bxslider.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Goji_cream_SK_Long/css/style_long.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Goji_cream_SK_Long/css/styleSecond.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Goji_cream_SK_Long/js/secondPage.js"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    $(".js_errorMessage2").remove();
                    $(".js_errorMessage").remove();
                    var errors = 0,
                        form = $(this).closest('form'),
                        name = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        countryp = form.find('[id="country_code_selector"]').val(),
                        namep = name.val(),
                        phonep = phone.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;
                    if(name.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(name, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(name, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        var mas={};
                        form.find('input,textatea,select').each(function(){
                            mas[$(this).attr('name')]=$(this).val();
                        });
                        $.post('/order/create_temporary/', mas);
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
    <div class="wrapper">
        <header id="s1">
            <div class="container">
                <div class="s1_inner">
                    <div class="fix pre_toform">
                        Objednajte
                    </div>
                    <img alt="" class="s1_l1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s1_l1.jpg"/>
                    <div class="s1_txt_1 upper cnd"> Najlepsí prostriedok
                        <br/> proti vraskam
                    </div>
                    <img alt="" class="s1i2" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s1i2.jpg"/>
                </div>
            </div>
        </header>
        <div class="clearfix"></div>
        <section class="s2" id="s2">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Tieto otravné nedostatky   </span>
                </div>
                <div class="s2_inner rel">
                    <img alt="" class="s2_i1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s2_i1.jpg"/>
                    <p class="text-center"> Vrasky - to ryhy a zahyby koze, ktoré sa objavujú na tvari, krku, rukach a iných
                        castiach tela. Objavujú sa v dôsledku prasknutia a poskodenia pruzných spojivových vlakien kolagénu
                        a
                        elastínu - dvoch najdôlezitejsích zloziek ludskej koze. </p>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s3 rel" id="s3">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Preco vznikajú vrasky?   </span>
                </div>
                <div class="s3_inner rel">
                    <div class="basic_tit s3_tit_1 upper cnd rel"> Hlavným dôvodom pre vznik vrasok je negatívny vplyv
                        vonkajsieho prostredia
                    </div>
                    <div class="s3_tit_2 upper cnd rel"> Slnecné ziarenie prispieva k predcasnému starnutiu koze</div>
                    <p class="basic_p text-center"> Významnú úlohu vo vzhlade vrasok u ludí hra celý rad vonkajsích
                        faktorov:
                        Dlhotrvajúce vystavenie slnku a vetru, teplotné výkyvy vzduchu, jeho prílisna suchost alebo vlhkost.
                        V
                        snahe zachovat mladistvost pleti je treba sa o nu aj spravne starat. Nevhodna kozmetika môze mat
                        horsí
                        vplyv na pokozku nez ziadna starostlivost. </p>
                    <img alt="" class="s3_i1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s3_i1.jpg"/>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s4" id="s4">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Ako sa vysporiadat s vraskami?   </span>
                </div>
                <div class="s4_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> Inovatívna formula na vysporiadanie sa s problémami
                        starnutia
                        pleti
                    </div>
                    <p class="basic_p text-center"> Typické produkty k odstraneniu vrasok bojujú iba s vonkajsími prejavami
                        problému, Goji krém sa vyraba pomocou vyspelej technológie, ktora umoznuje uzitocným latkam prenikat
                        do
                        hlbsích vrstiev koze, odstranovanie zmeny súvisiace s vekom, ktoré majú vplyv na dôlezité procesy
                        omladenia a regeneracie. </p>
                    <div class="s4_i s4_i_1 upper abs text-center">
                        <span class="cnd">   Hladka   </span> plet
                    </div>
                    <div class="s4_i s4_i_2 upper abs text-center">
                        <span class="cnd">   Bez   </span> nedostatkov
                    </div>
                    <div class="s4_i s4_i_3 upper abs text-center">
                        <span class="cnd">   Eliminacia   </span> zvnútra
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s5" id="s5">
            <div class="container">
                <div class="s5_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> Inovatívny vývoj v Nemecku</div>
                    <p class="basic_p text-center"> Vrasky sa môzu objavit v dôsledku oslabenia imunity, klimatických zmien,
                        UV
                        ziarenia, prirodzeného starnutia pokozky, dehydratacie, pouzitia nekvalitnej kozmetiky, ako aj
                        nespravneho cistenia tvare. Goji Cream odstranuje vrasky bez ohladu na prícinu a vyriesi problém
                        natrvalo. </p>
                    <div class="ribbon abs upper cnd">
                        <span>   Úcinna úlava od vrasok   </span>
                    </div>
                    <div class="s5_img_1 rel">
                        <img alt="" class="s5_i1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s5_i1.png"/>
                        <span class="abs s5_span_1 upper">   Do   </span>
                        <span class="abs s5_span_2 upper">   V priebehu   </span>
                        <span class="abs s5_span_3 upper">   Po   </span>
                    </div>
                    <div class="s5_b">
                        <div class="s5_b_i rel">
                            <span>   Ochranné vlastnosti   </span>
                            <p> Zahŕna rôzne prísady na ochranu proti ultrafialovému ziareniu, ako aj znízuje biologicky
                                aktívne
                                prvky pre restrukturalizaciu kozných buniek. </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span>   Hlboké preniknutie   </span>
                            <p> Aktivaciou prirodzenej tvorby kolagénu v hlbsích vrstvach koze vracia Goji Cream pruznost a
                                mladost v okamihu! </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span>   Jemna hydratacia   </span>
                            <p> Hydratuje pokozku, neupchava póry a zaroven nespôsobuje nadmernú aktivitu kozných zliaz.
                                Goji
                                Cream neblokuje prístup kyslíka do buniek epidermis a zlepsuje krvný obeh v týchto
                                oblastiach. </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span>   Rovnovaha   </span>
                            <p> Kombinuje cistenie, hydrataciu, tonizaciu a výzivu, co vedie k rovnovahe aktivity mazových
                                zliaz, odstranuje dehydrataciu. </p>
                        </div>
                    </div>
                    <img alt="" class="s5_i2" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s5_i2.png"/>
                    <div class="s5_b s5_b2">
                        <div class="s5_b_i rel">
                            <span>   Úcinnost pouzitia   </span>
                            <p> Pomaha zabranit opätovnému vzniku vrasok tým, ze obohacuje pokozku tvare a krku sirokým
                                spektrom
                                vitamínov, mineralov a aminokyselín. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s5_bottom">
                <div class="container">
                    <div class="s5_img_l">
                        <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s5_i3.jpg"/>
                    </div>
                    <div class="s5_img_r">
                        <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s5_i4.jpg"/>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s6" id="s6">
            <div class="s6_1">
                <div class="container">
                    <div class="ribbon abs upper cnd">
                        <span>   Ako pouzívat Goji Cream?   </span>
                    </div>
                    <div class="s6_inner rel">
                        <div class="basic_tit s6_tit_1 upper cnd rel"> Individualny program na omladenie pleti</div>
                        <p class="basic_p text-center"> Ak chcete zvolit ten spravny program pre omladenie pleti, je
                            potrebné
                            urcit umiestnenie vrasok. Goji Cream odborníci preskúmali vsetky mozné formy vrasok, a pre vase
                            pohodlie vytvorili jasnú schému. </p>
                        <span class="abs s6_span">   Na zaklade dlhorocných klinických skúseností, dermatológovia Goji Cream delia formy vrasok do troch typov: celo, oblast okolo ocí a nasolabialna oblast, z ktorých kazdý ma svoje vlastné charakteristiky.   </span>
                    </div>
                </div>
            </div>
            <div class="s6_2 rel">
                <div class="container">
                    <div class="s6_btns abs text-center">
                        <div class="s6_btn s6_btn_good upper cnd"><span>  Nasolabiálna oblasť  </span></div>
                    </div>
                    <p> Vodorovné ciary, priamy dôsledok nasich výrazov tvare. Hovorí sa im "alarmujúce", pretoze
                        sa
                        objavujú ako dôsledok emócií ako je úzkost a zlost; </p>
                    <div class="s6_2_inner">
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s6_2_i3.jpg"/>
                            <span>   Pred pouzitím   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s6_2_i2.jpg"/>
                            <span>   V priebehu pouzívania   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s6_2_i1.jpg"/>
                            <span>   Po mesiaci pouzívania   </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s6_2 s6_3 rel">
                <div class="container">
                    <div class="s6_btns abs text-center">
                        <div class="s6_btn s6_btn_good upper cnd"><span>   Oblast okolo ocí   </span></div>
                    </div>
                    <p>   "Vranie nohy" na vonkajsích rohoch ocí. Sú tvorené v dôsledku napätia ocných svalov,
                        napríklad úsmevom. Vzhladom k tomu, ze je koza okolo ocí velmi tenka, casto sú tam prvé viditelné
                        vrasky, ktoré vam davajú unavený alebo negatívny výraz. </p>
                    <div class="s6_2_inner">
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s66_3_i1.png"/>
                            <span>   Pred pouzitím   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s66_3_i2.png"/>
                            <span>   V priebehu pouzívania   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s66_3_i3.png"/>
                            <span>   Po mesiaci pouzívania   </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="s6_2 s6_4 rel">
                    <div class="s6_btns abs text-center">
                        <div class="s6_btn s6_btn_good upper cnd"><span>   čelo   </span></div>
                    </div>
                    <p> Nasolabialna oblast - velké zahyby od krídel nosa ku kútikom úst; retazec vrasok v okolí pier -
                        vertikalne vrasky nad hornou perou, a niekedy dokonca aj v spodnej casti; vrasky v kútikoch
                        úst. </p>
                    <div class="s6_2_inner">
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s6_3_i2.png"/>
                            <span>   Pred pouzitím   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s6_3_i3.png"/>
                            <span>   V priebehu pouzívania   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s6_3_i1.png"/>
                            <span>   Po mesiaci pouzívania   </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s7" id="s7">
            <div class="s7_2">
                <div class="container">
                    <div class="s7_inner rel">
                        <div class="basic_tit s4_tit_1 upper cnd rel"> Rýchlo sa zbavte vrasok pomocou Goji Cream - natrvalo
                        </div>
                        <div class="s7_subtit bold upper"> Na rozdiel od tradicných systémov dochadza k vyhladeniu pleti
                        </div>
                        <div class="s5_b">
                            <div class="s5_b_i rel">
                                <p> Typické produkty na odstranenie vrasok majú vplyv len na povrchové vrstvy koze, takze sú
                                    neúcinné, na rozdiel od Goji Cream </p>
                            </div>
                            <div class="s5_b_i rel">
                                <p> Goji Cream prenika do hlbsích vrstiev koze, kde odstranuje zmeny súvisiace s vekom, a
                                    nastartuje dôlezité procesy obnovy buniek a omladenie tvare. </p>
                            </div>
                            <div class="s5_b_i rel">
                                <p> Goji Cream v kratkej dobe vrací spät pruznost pleti. Pokozka získava vnútorný elan,
                                    objaví
                                    sa prirodzený rumenec, vrasky sa vyhladzujú, pokozka je hydratovana a tónovana a vyzera
                                    uvolnene. </p>
                            </div>
                        </div>
                    </div>
                    <div class="s7_b1 rel upper">
                        <div class="s7_i2 abs"></div>
                        <div class="s7_b1__bubble s7_i3 upper bold abs"> Samostatna liecba</div>
                        <div class="s7_b1__bubble s7_i4 upper bold abs"> Goji Cream</div>
                    </div>
                </div>
                <div class="s7_b2"></div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s9" id="s9">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Vyhrate nad vraskami   </span>
                </div>
                <div class="s9_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> Najlepsie riesenie pre vasu pokozku</div>
                </div>
                <div class="s9_b1">
                    <div class="s9_item">
                        <div class="s9_item_tit"> Aktívne zlozky</div>
                        <div class="s9_item_txt"> Krém pozostava z bobul kustovnice, lykopén, kyseliny hyaluronovej,
                            bunecných
                            antioxidantov, ktoré podporujú obnovu kozných buniek a z komplexu mineralov.
                        </div>
                    </div>
                    <div class="s9_item">
                        <div class="s9_item_tit"> Úcinok</div>
                        <div class="s9_item_txt"> Vdaka mimoriadne bohatému zlozeniu krém prispieva k dosiahnutiu stabilného
                            výsledku, vrasky sa nevracajú a ak ano, sú takmer neznatelné. Plet tvare a krku sa stava hladka,
                            pruzna a plna vitalnej energie, v dobrej kondícii.
                        </div>
                    </div>
                    <div class="s9_item">
                        <div class="s9_item_tit"> Pre koho je urcený</div>
                        <div class="s9_item_txt"> zeny s drobnými a velkými vraskami</div>
                    </div>
                    <div class="s9_price">
                        <div>Pôvodná cena: <span class="td-l">98 €</span></div>
                        <div class="newp">Cena: 49 €</div>
                    </div>
                    <div class="s9_action">
                        <button class="s9_buy pre_toform btn" type="button"> Kúpit</button>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s10" id="s10">
            <div class="s10_top">
                <div class="container">
                    <div class="ribbon abs upper cnd">
                        <span>   Vsetky poznatky nemeckých odborníkov v jednom balení krému   </span>
                    </div>
                    <div class="s10_inner rel text-center">
                        <div class="basic_tit s4_tit_1 upper cnd rel"> Goji Cream obsahuje</div>
                    </div>
                </div>
            </div>
            <div class="s10_bottom">
                <div class="container">
                    <div class="s10_b1">
                        <div class="s10_b1_item s10_b1_item_1">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i1.png"/>
                            <div class="s10_b1_item_tit upper"> Aktívna forma vitamínu A</div>
                            <p class="s10_b1_item_txt"> Je prítomný vo vsetkých maskach na tvar. Vitamín A znizuje aktivitu
                                mazových zliaz, vysusuje mastnú plet, stahuje rozsírené póry, vyrovnava a vyhladzuje
                                pokozku. </p>
                        </div>
                        <div class="s10_b1_item s10_b1_item_2">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i2.png"/>
                            <div class="s10_b1_item_tit upper"> Draslík</div>
                            <p class="s10_b1_item_txt"> Zvlhcuje pokozku a chrani ju pred vysúsaním od predchadzajúcej
                                zlozky. </p>
                        </div>
                        <div class="s10_b1_item s10_b1_item_3">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i3.png"/>
                            <div class="s10_b1_item_tit upper"> Vitamín C</div>
                            <p class="s10_b1_item_txt"> Stimulator imunity, antioxidant, chrani pokozku proti
                                baktériam. </p>
                        </div>
                        <div class="s10_b1_item s10_b1_item_4">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i4.png"/>
                            <div class="s10_b1_item_tit upper"> Vitamín PP ( B3)</div>
                            <p class="s10_b1_item_txt"> Zlepsuje krv. Tento vitamín je nevyhnutný pre regeneraciu
                                buniek. </p>
                        </div>
                        <div class="s10_b1_item s10_b1_item_5">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i5.png"/>
                            <div class="s10_b1_item_tit upper"> Vitamín K</div>
                            <p class="s10_b1_item_txt"> Podporuje syntézu spojivových tkanív, pomaha k vyhladeniu
                                pokozky. </p>
                        </div>
                    </div>
                    <div class="s10_b2 rel">
                        <div class="ribbon abs upper cnd">
                            <span>   Testovanie liecebných úcinkov Goji Cream   </span>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel"> Dôkazy o úcinnosti omladzujúcich vlastností</div>
                        </div>
                        <div class="s10_i3 s10_i3_1">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i6.jpg"/>
                            <div class="s10_i3_block">
                                <div class="s10_i3_tit upper"> Metóda analýzy výrobku</div>
                                <div class="s10_i3_txt"> Cista kultúra Staphylococcus aureus bola umiestnena v prostredí
                                    proteínu. Potom sa na sterilný filtracný papier naniesol Goji Cream. Nakoniec sa
                                    dosticka
                                    obsahujúce vysetrované komponenty zahreje na 37 ° C a ponecha po dobu 24 hodín.
                                </div>
                            </div>
                            <div class="s10_i3_block">
                                <div class="s10_i3_tit upper"> Výsledky analýzy</div>
                                <div class="s10_i3_txt"> Na zaklade výsledkov mozno vyvodit zaver, ze Goji Cream ma vysokú
                                    úcinnost pri pouzití. To je dané predovsetkým tým, ze obsahuje vitamíny a mineraly,
                                    rovnako
                                    ako biotín, tieto latky sú spojené v molekule, a vdaka tomu prenikajú do hlbsích
                                    vrstiev,
                                    dodavanie zivín do epidermis je velmi efektívne a tam dochadza k omladeniu pleti.
                                </div>
                            </div>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel"> Testovanie krému proti vraskam Goji Cream</div>
                        </div>
                        <div class="s10_i3 s10_i3_2">
                            <div class="s10_i3_block s10_i3_block_3">
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> Skúsobna metóda</div>
                                    <div class="s10_i3_txt"> Glycerín je zakladna súcast mazu. Goji Cream a ostatné
                                        prostriedky
                                        boli umiestnené na kvapku glycerolu a v priebehu 2 minút vidíme, ako dobre Goji
                                        Cream a
                                        ostatné prostriedky prenikajú dnu.
                                    </div>
                                </div>
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i7.jpg"/>
                                </div>
                            </div>
                            <div class="s10_i3_block s10_i3_block_4">
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> Výsledky testov</div>
                                    <div class="s10_i3_txt"> O dve minúty neskôr sa kvapka bezných prostriedkov stale drzí
                                        na
                                        povrchu glycerínu, zatial co Goji Cream je rovnomerne rozdelený po povrchu
                                        glycerolu,
                                        rýchlo rozpustený a vDaka tomu, ze obsahuje sensitinogen, nedrazdi kozu, co je
                                        dôlezité
                                        najmä pre citlivú pokozku.
                                    </div>
                                </div>
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i8.jpg"/>
                                </div>
                            </div>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel"> Testovanie hydratacných vlastností</div>
                        </div>
                        <div class="s10_i3 s10_i3_4">
                            <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i9.jpg"/>
                            <div class="s10_i3_block s10_i3_block_4">
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> Skúsobna metóda</div>
                                    <div class="s10_i3_txt"> Je zobrazený obsah vlhkosti v pokozke pred a po pouzití. Pred a
                                        po
                                        aplikacii prostriedkov sa odeberie vzoriek, ktorý ukazuje na stupen nasýtenia
                                        pokozky
                                        vlhkostou; tento test ukazuje, ako dobre Goji Cream zvlhcuje pokozku.
                                    </div>
                                </div>
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/s10_i10.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="s10_b2 rel">
                        <div class="ribbon abs upper cnd">
                            <span>   zeny po celom svete odporúcajú   </span>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel"> Recenzie produktov Goji Cream</div>
                        </div>
                    </div>
                    <div class="s10_b4 rel">
                        <div class="slider">
                            <div class="slider-prev"></div>
                            <ul class="bxslider">
                                <li>
                                    <div class="slide_tit"> Sofia Lehocká, 33 rokov</div>
                                    <p class="slider_p italic"> Vzdy som vyzerala mladsia a bola som na to velmi hrda. Po
                                        vaznom
                                        ochorení som dostala depresie a nestarala som sa o seba, co ovplyvnilo moj vzhlad.
                                        Aby
                                        ma nejako podporil, priatel mi ponukol pracu v jeho spolocnosti. To zahŕnalo pracu s
                                        luDmi a vzhlad hra významnú úlohu. Bolo nutné získat spät atraktívny a mladistvý
                                        vzhlad
                                        tak rýchlo, ako to bolo mozné. Pomohol mi Goji Cream! Klienti, s ktorými som
                                        komunikovala si mysleli, ze som o 10 rokov mladsia! </p>
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/slide_1.jpg"/>
                                </li>
                                <li>
                                    <div class="slide_tit"> Živa Krempaská, 56 rokov</div>
                                    <p class="slider_p italic"> Pozrite sa to, ako vela hodnotení ma tento krém. A to z
                                        dobrého
                                        dôvodu. Pouzívam krém asi rok a nikdy som nelutovala, ze som ho kúpila on-line. Je
                                        mi 56
                                        rokov. Ale verte mi, vyzeram o dost mladsia. A musím, pretoze môj manzel je o 14
                                        rokov
                                        mladsí. Takze, milí priatelia, nesetrite na sebe. </p>
                                    <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/slide_2.jpg"/>
                                </li>
                            </ul>
                            <div class="slider-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s12" id="s12">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Nedavne hodnotenia   </span>
                </div>
                <div class="s12_inner">
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> Anna Kaiserova, 42 rokov</div>
                            <div class="s12_tim">11/12/15</div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Po 30 sa mi zacali objavovat vrasky a bolo ich viac a viac kazdý rok. Obratila som sa na
                            kliniku,
                            pouzívala som drahé lieky a postupy, ale ich úcinok nebol trvalý. Hlavnou výhodou tohto produktu
                            je
                            jeho prístupnost. To je pre mna velmi dôlezité, umoznuje mi to pouzívat krém dôsledne.
                        </div>
                    </div>
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> Michaela Gavendová, 34 rokov</div>
                            <div class="s12_tim">11/12/15</div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Kamaratka mi poslala odkaz na internetové stranky a ja som si objednala. Spociatku som si
                            nevsimla
                            odlisnosti od bezných krémov (Ipouzívala som iba kozmetiku z lekarne). Ale ... teraz uplynul
                            mesiac.
                            Tak krasna moja plet nebola nikdy! Vyhodila som vsetku ostatnú kozmetiku. Nie som vôbec mastna!
                            Vsetky vrasky zmizli a nové sa neobjavili!
                        </div>
                    </div>
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> Eliska Novotna, 38 rokov</div>
                            <div class="s12_tim">11/12/15</div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/Goji_cream_SK_Long/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Objednala som si mesiac pred Novým rokom. Úcinok je uspokojivý. Ak to tak bude aj nadalej,
                            myslím,
                            ze som si vybrala ten pravý krém. Dakujem. Odporúcam zenam po 35 rokoch
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s100">
            <div class="container">
                <div class="text-center s100_tit white"> Objednat</div>
                <form action="" id="order_form" class="order_form" method="post">

                    <p class="old-price">98€</p>
                    <p class="new-price">Cena: 49€</p>
                    <div class="clf"></div>
                    <div class="shipment_price" style="display: none;">Dodanie: 0 €</div>
                    <div class="total_price">Celkom: <span class="red">49 €</span>
                    </div>
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abcdn.pro/forms/?target=-4AAIJIAL_EgAAAAAAAAAAAAQd6T5KAA"></iframe>

                    <div class="toform"></div>
                </form>
            </div>
            <div class="clearfix"></div>
        </section>
    </div>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet"/>
    <div class="hidden-window">
        <div class="h-headerj">
            <div class="containerz clearfix">
                <p>
                    GOJI CREAM
                </p>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader"><b>Platba<br/>na dobierku</b></div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img_second/dtc1.png"/></div>
                                    <div class="dtable-cell">Objednávka</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img_second/dtc2.png"/></div>
                                    <div class="dtable-cell">Poštovné</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img alt="alt1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img_second/dtc3.png"/></div>
                                    <div class="dtable-cell">Dodanie a <span>platba</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Fair obchod</div>
                        <form action="" class="js_scrollForm" method="post"><input type="hidden" name="total_price" value="49.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAL_EgQy5_-LAAEAAQACZhIBAAJAHAIGAQEABLGN39kA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Goji_cream_SK_Long">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="SK">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-pl">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.2">

                            <select style="display: none;" class="select inp" id="country_code_selector" name="country_code">
                                <option value="SK">Slovakia</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1 balenie</option>
                                <option selected="selected" value="3">2 balení + darček</option>
                                <option value="5">3 balení + 2 darčeky</option>
                            </select>
                            <input style="display: none;" class="inp j-inp" name="name" placeholder="Zadajte meno" type="text" value=""/>
                            <input style="display: none;" class="only_number inp j-inp" name="phone" placeholder="Zadajte telefónne číslo" type="text" value=""/>
                            <!--<input class="inp" name="city" placeholder="Zadajte mesto" style="display: none" type="text"/>-->
                            <input class="inp" name="address" placeholder="Zadajte adresu" type="text"/>
                            <div class="text-center totalpriceForm">Celkom: <span class="js_total_price js_full_price">49 </span>
                                €
                            </div>
                            <div class="zbtn js_submit transitionmyl">Objednávka</div>
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Garantujeme</div>
                            <ul>
                                <li><b>100%</b> kvalita</li>
                                <li><b> Kontrola</b> produktu po obdržaní</li>
                                <li><b> Bezpečnosť</b> vašich dát</li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Upozornenie</b>:<br/>
                                <span>Zľava 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader"><b>1</b> balenie</div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Cena:</div>
                                            <div class="dtable-cell"><b class="js-pp-new">
                                                    49 </b> <b>€</b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"> <span class="dib old-pricedecoration"><i></i>
<text class="js-pp-old"> 98
                                                    </text>
</span></div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Pôvodná cena</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Poštovné:</div>
                                            <div class="dtable-cell"><b class="js-pp-ship">
                                                    0 </b> <b>€</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Celkom:</div>
                                            <div class="dtable-cell prtotal"><b>
                                                    <text class="js-pp-full"> 49

                                                    </text>
                                                </b>€
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button " data-package-id="1"></div>
                                <div class="img-wrapp"><img alt="z1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img_second/1.png" style="margin-left: 75px;"/></div>
                                <div class="zHeader"><span> </span></div>
                                <text>
                                    Viditeľné omladenie už po mesiaci užívania
                                </text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 active">
                            <div class="zstick"></div>
                            <div class="zDiscount">
                                <b>Upozornenie</b>:<br/>
                                <span>Zľava 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> balení <span class="dib zplus">darček</span></div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Cena:</div>
                                            <div class="dtable-cell"><b>
                                                    <text class="js-pp-new"> 98
                                                    </text>
                                                    € </b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"><span class="dib old-pricedecoration"><i></i><span class="js-pp-old">
   196 </span></span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Pôvodná cena</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Poštovné:</div>
                                            <div class="dtable-cell"><b>
                                                    <text class="js-pp-ship">
                                                        0
                                                    </text>
                                                    €</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Celkom:</div>
                                            <div class="dtable-cell prtotal"><b><span class="js-pp-full"> 98 </span>
                                                </b> €
                                            </div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3"></div>
                                <div class="img-wrapp">
                                    <div class="gift"></div>
                                    <img alt="z1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img_second/2+1.png"/></div>
                                <div class="zHeader"><span> </span></div>
                                <text>
                                    Kúra na mladosť! Liftingový účinok a vyhladenie najhlbších vrások len do troch mesiacov.
                                    Zaručený výsledok.
                                </text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Upozornenie</b>:<br/>
                                <span>Zľava 50%</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> balení <span class="dib zplus sec">darčeky</span></div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst"> Cena:</div>
                                            <div class="dtable-cell"><b>
                                                    <text class="js-pp-new"> 147
                                                    </text>
                                                    €</b></div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell"><span class="dib old-pricedecoration"><i></i><text class="js-pp-old"> 294
                                                </text></span></div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Pôvodná cena</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Poštovné:</div>
                                            <div class="dtable-cell"><b>
                                                    <text class="js-pp-ship"> 0
                                                    </text>
                                                    €</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Celkom:</div>
                                            <div class="dtable-cell prtotal"><b>
                                                    <text class="js-pp-full"> 147

                                                    </text>
                                                    €</b></div>
                                            <div class="dtable-cell text-center mw30"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5"></div>
                                <div class="img-wrapp">
                                    <div class="gift th"></div>
                                    <img alt="z1" src="//st.acstnst.com/content/Goji_cream_SK_Long/img_second/3+2.png"/></div>
                                <div class="zHeader sm"><span> </span></div>
                                <text>
                                    Maximálne omladenie! Polročná kúra vás nielen zbaví zmien súvisiacich so starnutím, ale
                                    tiež zabráni ich znovuobjaveniu.
                                </text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Goji_cream_SK_Long/js/bxslider.js"></script>
    <script>
        $(document).ready(function () {
            $('.bxslider').bxSlider({
                speed: 1000,
                pause: 10000,
                minSlides: 1,
                maxSlides: 1,
                slideMargin: 15,
                slideWidth: 790,
                nextSelector: '.slider-next',
                prevSelector: '.slider-prev',
                auto: true,
                autoStart: true,
                pager: false,
            });
        });
    </script>
    </body>
    </html>
<?php } ?>