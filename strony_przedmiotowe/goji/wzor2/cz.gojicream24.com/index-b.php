<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7269 -->
        <script>var locale = "cz";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAINEwTnjACMAAEAAQACchIBAAJlHAIGAQEABPBCPS4A";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":1300,"old_price":2600,"shipment_price":0},"3":{"price":2600,"old_price":5200,"shipment_price":0},"5":{"price":3900,"old_price":7800,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Ivana Michalkova';
            var phone_hint = '+420774115732';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cz");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <title> Hlavní strana </title>
        <link href="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/css/bxslider.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/css/styleSecond.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/js/secondPage.js"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    $(".js_errorMessage2").remove();
                    $(".js_errorMessage").remove();
                    var errors = 0,
                        form = $(this).closest('form'),
                        name = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        countryp = form.find('[id="country_code_selector"]').val(),
                        namep = name.val(),
                        phonep = phone.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;
                    if(name.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(name, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(name, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        var mas={};
                        form.find('input,textatea,select').each(function(){
                            mas[$(this).attr('name')]=$(this).val();
                        });
                        $.post('/order/create_temporary/', mas);
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
    <div class="hide-it">
        <header id="s1">
            <div class="container">
                <img alt="" class="s1_l1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s1_l1.jpg"/>
                <div class="s1_txt_1 upper cnd">  NEJLEPŠÍ PROSTŘEDEK
                    <br/>    PROTI VRÁSKÁM </div>
                <img alt="" class="s1i2" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s1i2.jpg"/>
            </div>
        </header>
        <div class="clearfix"></div>
        <section class="s2" id="s2">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> TYTO OTRAVNÉ NEDOSTATKY </span>
                    </div>
                </div>
                <img alt="" class="s2_i1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s2_i1.jpg"/>
                <p class="text-center">Vrásky je to označení pro záhyb v kůži, který vzniká stahováním pokožky, které se objevují na obličeji, krku, rukou a jiných částech těla. Objevují se v důsledku prasknutí a poškození pružných pojivových vláken kolagenu a elastinu, které jsou dvě nejdůležitější složky lidské kůže.</p>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s3 rel" id="s3">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> PROČ VZNIKAJÍ VRÁSKY? </span>
                    </div>
                </div>
                <div class="s3_inner rel">
                    <div class="basic_tit s3_tit_1 upper cnd rel"> HLAVNÍM DŮVODEM PRO VZNIK VRÁSEK JE NEGATIVNÍ VLIV VNĚJŠÍHO PROSTŘEDÍ </div>
                    <div class="s3_tit_2 upper cnd rel"> SLUNEČNÍ ZÁŘENÍ PŘISPÍVÁ K PŘEDČASNÉMU STÁRNUTÍ KŮŽE </div>
                    <p class="basic_p text-center">Významnou roli ve vzhledu vrásek ovlivňuje řada vnějších faktorů: dlouhodobé vystavení slunci a větru, teplotní výkyvy vzduchu, jeho přílišná suchost nebo vlhkost. Ve snaze zachovat mladistvost pleti je třeba se o ni i správně starat. Nevhodná kosmetika může mít horší vliv na pokožku než žádná péče. </p>
                    <img alt="" class="s3_i1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s3_i1.jpg"/>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s4" id="s4">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> JAK SE VYPOŘÁDAT S VRÁSKAMI? </span>
                    </div>
                </div>
                <div class="s4_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> INOVATIVNÍ FORMULE NA VYPOŘÁDÁNÍ SE S PROBLÉMY STÁRNUTÍ PLETI </div>
                    <p class="basic_p text-center">Běžné produkty na odstranění vrásek bojují proti vnějším projevům problém, Goji Cream se vyrábí pomocí vyspělých technologií, která umožňuje užitečným látkám pronikat do hlubších vrstev kůže, odstraňovat změny související s věkem, které mají vliv na důležité procesy omlazení a regenerace. </p>
                    <div class="s4_i s4_i_1 upper text-center">
                        <span class="cnd"> HLADKÁ </span> PLEŤ
                    </div>
                    <div class="s4_i s4_i_2 upper text-center">
                        <span class="cnd"> BEZ </span> NEDOSTATKŮ
                    </div>
                    <div class="s4_i s4_i_3 upper text-center">
                        <span class="cnd">ODSTRAŇENÍ</span> PROBLÉMU ZEVNITŘ</div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s5" id="s5">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> ÚČINNÁ ÚLEVA OD VRÁSEK</span>
                    </div>
                </div>
                <div class="s5_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> INOVATIVNÍ VÝVOJ V NĚMECKU </div>
                    <p class="basic_p text-center"> Vrásky se mohou objevit v důsledku oslabené imunity, UV záření, přirozeného stárnutí pokožky, dehydratace, použití nekvalitní kosmetiky, jakož i nesprávného čištění pleti. Goji Cream odstraňuje vrásky bez ohledu na příčinu a vyřeší problém trvale. </p>
                    <div class="s5_b">
                        <div class="s5_b_i rel">
                            <span> OCHRANNÉ VLASTNOSTI </span>
                            <p> Zahrnuje různé přísady na ochranu proti ultrafialovému záření, a biologicky aktivní prvky pro restrukturalizaci kožní tkáně. </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span> HLUBOKÉ PRONIKNUTÍ </span>
                            <p>Goji Cream aktivuje přirozenou tvorbu kolagenu v hlubších vrstvách kůže, vrátí pokožce pružnost a rychle Vás omladí!</p>
                        </div>
                        <div class="s5_b_i rel">
                            <span> JEMNÁ HYDRATACE </span>
                            <p> Hydratuje pokožku, neucpává póry a zároveň nezpůsobuje nadměrnou aktivitu kožních žláz. Goji Cream neblokuje přístup kyslíku do buněk epidermis a zlepšuje krevní oběh v těchto oblastech. </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span> ROVNOVÁHA </span>
                            <p>Kombinuje čištění, hydrataci, tonizaci a výživu pleti, což vede k rovnováze aktivity mazových žláz, odstraňuje dehydrataci. </p>
                        </div>
                    </div>
                    <img alt="" class="s5_i2" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s5_i2.jpg"/>
                    <div class="s5_b s5_b2">
                        <div class="s5_b_i rel">
                            <span>ÚČINKY PŘI POUŽÍVÁNÍ</span>
                            <p> Pomáhá zabránit opětovnému vzniku vrásek tím, že obohacuje pokožku obličeje a krku širokým spektrem vitamínů, minerálů a aminokyselin. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s5_bottom">
                <div class="container">
                    <div class="s5_img_l">
                        <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s5_i3.jpg"/>
                    </div>
                    <div class="s5_img_r">
                        <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s5_i4.jpg"/>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s7" id="s7">
            <div class="s7_2">
                <div class="container">
                    <div class="rel">
                        <div class="ribbon abs upper cnd">
                            <span> JAK POUŽÍVAT GOJI CREAM? </span>
                        </div>
                    </div>
                    <div class="s7_inner rel">
                        <div class="basic_tit s4_tit_1 upper cnd rel"> RYCHLE SE ZBAVTE VRÁSEK POMOCÍ GOJI CREAM - NATRVALO </div>
                        <div class="s7_subtit bold upper"> ROZDÍL OD TRADIČNÍCH SYSTÉMŮ VYHLAZENÍ PLETI </div>
                        <div class="s5_b">
                            <div class="s5_b_i rel">
                                <p> Typické produkty na odstranění vrásek mají vliv pouze na povrchové vrstvy kůže, takže jsou neúčinné, na rozdíl od Goji Cream. </p>
                            </div>
                            <div class="s5_b_i rel">
                                <p> Goji Cream proniká do hlubších vrstev kůže, kde odstraňuje změny související s věkem, a nastartuje důležité procesy obnovy buněk a omlazení obličeje. </p>
                            </div>
                            <div class="s5_b_i rel">
                                <p> Goji Cream v krátké době vrací zpět pružnost pleti. Pokožka získává vnitřní elán, objeví se přirozený ruměnec, vrásky se vyhlazují, pokožka je hydratovaná a tónovaná a vypadá uvolněně. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s9" id="s9">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span>VYHRAJETE BOJ NAD VRÁSKAMI</span>
                    </div>
                </div>
                <div class="s9_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel"> NEJLEPŠÍ ŘEŠENÍ PRO VAŠI POKOŽKU </div>
                </div>
                <div class="s9_b1">
                    <div class="s9_item">
                        <div class="s9_item_tit"> Aktivní složky </div>
                        <div class="s9_item_txt"> Krém se skládá z bobulí kustovnice, lykopen, kyseliny hyaluronové, buněčných antioxidantů, které podporují obnovu kožních buněk a z komplexu minerálů. </div>
                    </div>
                    <div class="s9_item">
                        <div class="s9_item_tit"> Účinek </div>
                        <div class="s9_item_txt"> Díky mimořádně bohatému složení krém přispívá k dosažení stabilního výsledku, vrásky se nevracejí a pokud ano, jsou téměř neznatelné. Pleť obličeje a krku se stává hladká, pružná a plná vitální energie, v dobré kondici. </div>
                    </div>
                    <div class="s9_item">
                        <div class="s9_item_tit"> Pro koho je určen </div>
                        <div class="s9_item_txt"> Ženy s drobnými a velkými vráskami </div>
                    </div>
                    <div class="s9_price old_price">
                        Původní cena: <span class="td-l">2600 Kč</span>
                    </div>
                    <div class="s9_price">
                        Cena: <span>1300 Kč</span>
                    </div>
                    <div class="s9_action">
                        <button class="s9_buy pre_toform" type="button">    Koupit    </button>
                    </div>
                </div>
            </div>
        </section>
        <section class="s10" id="s10">
            <div class="s10_top">
                <div class="container">
                    <div class="rel">
                        <div class="ribbon abs upper cnd">
                            <span> VŠECHNY POZNATKY NĚMECKÝCH ODBORNÍKŮ </span>
                        </div>
                    </div>
                    <div class="s10_inner rel text-center">
                        <div class="basic_tit s4_tit_1 upper cnd rel"> GOJI CREAM OBSAHUJE </div>
                    </div>
                </div>
            </div>
            <div class="s10_bottom">
                <div class="container">
                    <div class="slider">
                        <ul class="bxslider bxslider2">
                            <li>
                                <div class="s10_b1_item s10_b1_item_1">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s10_i1.png"/>
                                    <div class="s10_b1_item_tit upper"> AKTIVNÍ FORMA VITAMINU A </div>
                                    <p class="s10_b1_item_txt"> je přítomen ve všech maskách na obličej. Vitamín A snižuje aktivitu mazových žláz, vysušuje mastnou pleť, stahuje rozšířené póry, vyrovnává a vyhlazuje pokožku. </p>
                                </div>
                            </li>
                            <li>
                                <div class="s10_b1_item s10_b1_item_2">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s10_i2.png"/>
                                    <div class="s10_b1_item_tit upper"> DRASLÍK ZVLHČUJE POKOŽKU </div>
                                    <p class="s10_b1_item_txt"> zvlhčuje pokožku a chrání ji před vysoušením od předchozí složky. </p>
                                </div>
                            </li>
                            <li>
                                <div class="s10_b1_item s10_b1_item_3">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s10_i3.png"/>
                                    <div class="s10_b1_item_tit upper"> VITAMÍN C </div>
                                    <p class="s10_b1_item_txt"> stimulátor imunity, antioxidant, chrání pokožku proti bakteriím. </p>
                                </div>
                            </li>
                            <li>
                                <div class="s10_b1_item s10_b1_item_4">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s10_i4.png"/>
                                    <div class="s10_b1_item_tit upper"> VITAMÍN PP (B3) </div>
                                    <p class="s10_b1_item_txt"> Zlepšuje krev. Tento vitamín je nezbytný pro regeneraci buněk </p>
                                </div>
                            </li>
                            <li>
                                <div class="s10_b1_item s10_b1_item_5">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s10_i5.png"/>
                                    <div class="s10_b1_item_tit upper"> VITAMÍN K </div>
                                    <p class="s10_b1_item_txt"> podporuje syntézu pojivových tkání, pomáhá k vyhlazení pokožky. </p>
                                </div>
                            </li>
                        </ul>
                        <div class="slider-prev slider-prev2"></div>
                        <div class="slider-next slider-next2"></div>
                    </div>
                    <div class="s10_b2 rel">
                        <div class="ribbon abs upper cnd">
                            <span> TESTOVÁNÍ LÉČEBNÝCH ÚČINKŮ GOJI CREAM </span>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel hpt"> DŮKAZY O ÚČINNOSTI OMLAZUJÍCÍCH VLASTNOSTÍ </div>
                        </div>
                        <div class="s10_i3 s10_i3_1">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s10_i6.jpg"/>
                            <div class="s10_i3_block">
                                <div class="s10_i3_tit upper"> METODA ANALÝZY VÝROBKU </div>
                                <div class="s10_i3_txt"> Čistá kultura Staphylococcus aureus byla umístěna v prostředí proteinu. Pak se na sterilní filtrační papír nanesl Goji Cream. Nakonec se destička obsahující vyšetřovány komponenty zahřeje na 37 ° C a ponechá po dobu 24 hodin. </div>
                            </div>
                            <div class="s10_i3_block">
                                <div class="s10_i3_tit upper"> VÝSLEDKY ANALÝZY </div>
                                <div class="s10_i3_txt"> Na základě výsledků lze vyvodit závěr, že Goji Cream má vysokou účinnost při použití. To je dáno především tím, že obsahuje vitaminy a minerály, stejně jako biotin, tyto látky jsou spojeny v molekule, a díky tomu pronikají do hlubších vrstev, dodávání živin do epidermis je velmi efektivní a tam dochází k omlazení pleti. </div>
                            </div>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel"> TESTOVÁNÍ KRÉMU PROTI VRÁSKÁM GOJI CREAM </div>
                        </div>
                        <div class="s10_i3 s10_i3_2">
                            <div class="s10_i3_block s10_i3_block_3">
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s10_i7.jpg"/>
                                </div>
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> ZKUŠEBNÍ METODA </div>
                                    <div class="s10_i3_txt"> Glycerin je základní součást mazu. Goji Cream a ostatní prostředky byly umístěny na kapku glycerolu a během 2 minut vidíme, jak dobře goji Cream a ostatní prostředky pronikají dovnitř. </div>
                                </div>
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> VÝSLEDKY TESTŮ </div>
                                    <div class="s10_i3_txt"> O dvě minuty později se kapka běžných prostředků stále drží na povrchu glycerinu, zatímco Goji Cream je rovnoměrně rozdělen po povrchu glycerolu, rychle rozpuštěný a díky tomu, že obsahuje sensitinogen, nedráždí kůži, což je důležité zejména pro citlivou pokožku. </div>
                                </div>
                            </div>
                            <div class="basic_tit s4_tit_1 upper cnd rel"> TESTOVÁNÍ HYDRATAČNÍCH VLASTNOSTÍ </div>
                            <div class="s10_i3_block s10_i3_block_4">
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s10_i10.jpg"/>
                                </div>
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper"> ZKUŠEBNÍ METODA </div>
                                    <div class="s10_i3_txt"> Je zobrazený obsah vlhkosti v pokožce před a po použití. Před a po aplikaci prostředků se odebírají vzorky, který ukazuje na stupeň nasycení pokožky vlhkostí; tento test ukazuje, jak dobře Goji Cream zvlhčuje pokožku. </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="s10_b2 rel">
                        <div class="ribbon abs upper cnd">
                            <span> ŽENY PO CELÉM SVĚTĚ DOPORUČUJÍ </span>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel hpt"> RECENZE PRODUKTŮ GOJI CREAM </div>
                        </div>
                    </div>
                    <div class="s10_b4 rel">
                        <div class="slider">
                            <ul class="bxslider">
                                <li>
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/slide_1.jpg"/>
                                    <div class="slide_tit"> Alena Petrová, 45 let </div>
                                    <p class="slider_p italic"> Vždycky jsem vypadala mladší a byla jsem na to velmi hrdá. Po vážném onemocnění jsem měla deprese a nestarala jsem se o sebe, což ovlivnilo můj vzhled. Aby mě nějak podpořil, přítel mi nabídl práci v jeho společnosti. To zahrnovalo práci s lidmi a vzhled hraje významnou roli. Bylo nutné získat zpět atraktivní a mladistvý vzhled tak rychle, jak to bylo možné. Pomohl mi Goji Cream! Klienti, s nimiž jsem komunikovala si mysleli, že jsem o 10 let mladší! </p>
                                </li>
                                <li>
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/slide_2.jpg"/>
                                    <div class="slide_tit"> Jana Pávková, 56 let </div>
                                    <p class="slider_p italic"> Podívejte se to, jak mnoho hodnocení má tento krém. A to z dobrého důvodu. Používám krém asi rok a nikdy jsem nelitovala, že jsem ho koupila on-line. Je mi 56 let. Ale věřte mi, vypadám o dost mladší. A musím, protože můj manžel je o 14 let mladší. Takže, milí přátelé, nešetřete na sobě. </p>
                                </li>
                            </ul>
                            <div class="slider-prev"></div>
                            <div class="slider-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s12" id="s12">
            <div class="container">
                <div class="rel">
                    <div class="ribbon abs upper cnd">
                        <span> NEDÁVNÉ HODNOCENÍ </span>
                    </div>
                </div>
                <div class="s12_inner">
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> ANNA KAISEROVÁ, 42 LET </div>
                            <div class="s12_tim"><script type="text/javascript">dtime_nums(-5, true)</script></div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Po 30 se mi začaly objevovat vrásky a bylo jich více a více každý rok. Obrátila jsem se na kliniku, používala jsem drahé léky a postupy, ale jejich účinek nebyl trvalý. Hlavní výhodou tohoto produktu je jeho přístupnost. To je pro mě velmi důležité, umožňuje důsledně používat ten krém.
                        </div>
                    </div>
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> OLGA NAUMOVÁ, 34 </div>
                            <div class="s12_tim"><script type="text/javascript">dtime_nums(-8, true)</script></div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star_half.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Kamarádka mi poslala odkaz na internetové stránky a já jsem si objednala. Zpočátku jsem si nevšimla odlišnosti od běžných krémů (používala jsem pouze kosmetiku z lékárny). Ale ... teď uplynul měsíc. Tak krásná moje pleť nebyla nikdy! Vyhodila jsem veškerou další kosmetiku. Nejsem vůbec mastná! Všechny vrásky zmizely a nové se neobjevily!
                        </div>
                    </div>
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit"> ELIŠKA NOVOTNÁ, 38 </div>
                            <div class="s12_tim"><script type="text/javascript">dtime_nums(-10, true)</script></div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Objednala jsem si měsíc před Novým rokem. Účinek je uspokojivý. Pokud to tak bude i nadále, myslím, že jsem si vybrala ten pravý krém. Děkuji. Doporučuji ženám po 35. roce věku.
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s100">
            <div class="container">
                <div class="text-center s100_tit white"> Objednat </div>
                <form action="" id="order_form" class="orderform" method="post">
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abdcn.pro/forms/?target=-4AAIJIAINEwAAAAAAAAAAAATe1-u9AA"></iframe>

                    <div class="price-wrapp">
                        <p class="old-price">2600 Kč</p>
                        <p class="new-price">Nová cena: 1300 Kč</p>
                        <div class="cl"></div>
                    </div>
                 
                    <div class="toform"></div>
                </form>
            </div>
        </section>
        <div class="clearfix"></div>
        <footer></footer>
    </div>
    <div class="hidden-window">
        <div class="header_new">
            <div class="containerz">
                <div class="h-w__inner">
                    <div class="dtable">
                        <div class="dtable-cell text-center"><img alt="logo" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/img/s1_l1.jpg"/></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader"><b>Goji cream - NEJLEPŠÍ PROSTŘEDEK
                                    PROTI VRÁSKÁM</b></div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/imagesSec/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/imagesSec/dtc1.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/imagesSec/dtc2.png"/></div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/imagesSec/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/imagesSec/dtc3.png"/></div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">Objednávka</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Poštovné</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">Dodání a platba</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">Výhodná nabídka</div>
                            <select class="corbselect select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 Balení!</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 Balení!  </option>
                                <option data-slide-index="2" value="5">3+2 Balení!  </option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right for_table">
                                            <div class="ssimgabs"><img alt="zt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/imagesSec/pack-1.png"/></div>
                                            <div class="zHeader onepack"><b>1</b> Balení </div>
                                        </div>
                                        <div class="pack_descr">Viditelné omlazení už po měsíci používání.</div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Cena:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 1300 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i><text class="js-pp-old"> 2600 </text>
                                                </div> Kč
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile">+ dárek</div>
                                                    <img alt="zt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/imagesSec/pack-2.png"/>
                                                </div>
                                                <div class="zHeader"><b>2</b> Balení   <br/> <span class="dib zplus">Dárek</span></div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">Cesta k mládí! Liftingový efekt a vyhlazení i těch nejhlubších vrásek už za tři měsíce. Garantované výsledky. </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Cena:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 2600 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i><text class="js-pp-old"> 5200 </text>
                                                </div> Kč
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <div class="giftmobile">+ dárek</div>
                                                <img alt="zt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/imagesSec/pack-3.png"/>
                                            </div>
                                            <div class="zHeader"><b>3</b> Balení <br/> <span class="dib zplus sec">Dárky</span></div>
                                        </div>
                                        <div class="pack_descr">Maximální omlazení! Celá půlroční kůra vás nejen zbaví změn způsobených věkem, ale také pomůže předejít jejich opětovnému vzniku.</div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Cena:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 3900 </text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i><text class="js-pp-old"> 7800 </text>
                                                </div> Kč
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="bx-pager">
                            <a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a>
                            <a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a>
                            <a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                        </div>
                        <form action="" class="js_scrollForm" method="post"

                            <!--
                            <input class="inp" name="city" placeholder="Introducir ciudad" style="display: none" type="text"/>
                            <input class="inp" name="house" placeholder="Página principal" style="display: none" type="text"/>
                            <input class="inp" name="address" placeholder="Introducir dirección" type="text"/>
                              -->
                            <div class="text-center"></div>
                            <div class="text-center totalpriceForm">Celková cena: <span class="js_total_price js_full_price">1300 </span> Kč</div>
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abdcn.pro/forms/?target=-4AAIJIAINEwAAAAAAAAAAAATe1-u9AA"></iframe>

                        </form>
                        <div class="zGarant">
                            <div class="zHeader">GARANTUJEME:</div>
                            <ul>
                                <li><b>100% </b> kvalitu</li>
                                <li><b>Kontrolu</b> produktu při převzetí</li>
                                <li><b>Bezpečnost </b> vašich údajů</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/js/bxslider.js"></script>
    <script src="//st.acstnst.com/content/GojiCream_CZ_Pink/mobile/js/script.js"></script>
    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 7269 -->
        <script>var locale = "cz";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAINEwRLTQCMAAEAAQACchIBAAJlHAIGAQEABLpztYMA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {"1":{"price":1300,"old_price":2600,"shipment_price":0},"3":{"price":2600,"old_price":5200,"shipment_price":0},"5":{"price":3900,"old_price":7800,"shipment_price":0}};
            var shipment_price = 0;
            var name_hint = 'Ivana Michalkova';
            var phone_hint = '+420774115732';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cz");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <title> Hlavní strana </title>
        <link href="//st.acstnst.com/content/GojiCream_CZ_Pink/css/bxslider.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/GojiCream_CZ_Pink/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/GojiCream_CZ_Pink/css/styleSecond.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/GojiCream_CZ_Pink/js/secondPage.js"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script>
            $(document).ready(function(){
                function errorMs(elem, msg) {
                    $(".js_errorMessage2").remove();
                    $('<div class="js_errorMessage2">' + msg + '</div>').appendTo('body').css({
                        'left': $(elem).offset().left,
                        'top': $(elem).offset().top - 30,
                        'background-color':'#e74c3c',
                        'border-radius': '5px',
                        'color': '#fff',
                        'font-family': 'Arial',
                        'font-size': '14px',
                        'margin': '3px 0 0 0px',
                        'padding': '6px 5px 5px',
                        'position': 'absolute',
                        'z-index': '9999'
                    });
                    $(elem).focus();
                }
                $(".pre_toform").on("touchend, click", function (e) {
                    e.preventDefault();
                    $('body,html').animate({scrollTop: $('#order_form,.scrollform').offset().top}, 400);
                });
                $('.js_pre_toform').on("touchend, click", function (e) {
                    e.preventDefault();
                    $(".js_errorMessage2").remove();
                    $(".js_errorMessage").remove();
                    var errors = 0,
                        form = $(this).closest('form'),
                        name = form.find('[name="name"]'),
                        phone = form.find('[name="phone"]'),
                        countryp = form.find('[id="country_code_selector"]').val(),
                        namep = name.val(),
                        phonep = phone.val(),
                        rename = /^[\D+ ]*$/i,
                        rephone = /^[0-9\-\+\(\) ]*$/i;
                    if(name.attr('data-count-length') == "2+"){
                        var rename = /^\D+\s[\D+\s*]+$/i;
                    }
                    if(!namep.length){
                        errors++;
                        errorMs(name, defaults.get_locale_var('set_fio'));
                    }else if(!rename.test(namep)){
                        errors++;
                        errorMs(name, defaults.get_locale_var('error_fio'));
                    }else if(!phonep || !phonep.length){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('set_phone'));
                    }else if(!rephone.test(phonep) || phonep.length < 5){
                        errors++;
                        errorMs(phone, defaults.get_locale_var('error_phone'));
                    }
                    if(!errors>0){
                        var mas={};
                        form.find('input,textatea,select').each(function(){
                            mas[$(this).attr('name')]=$(this).val();
                        });
                        $.post('/order/create_temporary/', mas);
                        $('.hidden-window').find('input').each(function(){
                            var nm = $(this).attr('name');
                            if(nm == 'name')$(this).val(namep);
                            if(nm == 'phone')$(this).val(phonep);
                        });
                        $('.hidden-window select#country_code_selector option[value=' + countryp + ']').prop("selected", true);
                        $('.toform').click();
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
    <div class="hide-it">
        <header id="s1">
            <div class="container">
                <div class="s1_inner">
                    <img alt="" class="s1_l1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s1_l1.jpg"/>
                    <div class="s1_txt_1 upper cnd">   Nejlepší prostředek
                        <br/>   proti vráskám   </div>
                    <img alt="" class="s1i2" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s1i2.jpg"/>
                </div>
            </div>
        </header>
        <div class="clearfix"></div>
        <section class="s2" id="s2">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Tyto otravné nedostatky   </span>
                </div>
                <div class="s2_inner rel">
                    <img alt="" class="s2_i1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s2_i1.jpg"/>
                    <p class="text-center">Vrásky je to označení pro záhyb v kůži, který vzniká stahováním pokožky, které se objevují na obličeji, krku, rukou a jiných částech těla. Objevují se v důsledku prasknutí a poškození pružných pojivových vláken kolagenu a elastinu, které jsou dvě nejdůležitější složky lidské kůže.</p>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s3 rel" id="s3">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Proč vznikají vrásky?   </span>
                </div>
                <div class="s3_inner rel">
                    <div class="basic_tit s3_tit_1 upper cnd rel">   Hlavním důvodem pro vznik vrásek je negativní vliv vnějšího prostředí   </div>
                    <div class="s3_tit_2 upper cnd rel">   Sluneční záření přispívá k předčasnému stárnutí kůže   </div>
                    <p class="basic_p text-center">Významnou roli ve vzhledu vrásek ovlivňuje řada vnějších faktorů: dlouhodobé vystavení slunci a větru, teplotní výkyvy vzduchu, jeho přílišná suchost nebo vlhkost. Ve snaze zachovat mladistvost pleti je třeba se o ni i správně starat. Nevhodná kosmetika může mít horší vliv na pokožku než žádná péče.   </p>
                    <img alt="" class="s3_i1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s3_i1.jpg"/>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s4" id="s4">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Jak se vypořádat s vráskami?   </span>
                </div>
                <div class="s4_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel">   Inovativní formule na vypořádání se s problémy stárnutí pleti   </div>
                    <p class="basic_p text-center">Běžné produkty na odstranění vrásek bojují proti vnějším projevům problém, Goji Cream se vyrábí pomocí vyspělých technologií, která umožňuje užitečným látkám pronikat do hlubších vrstev kůže, odstraňovat změny související s věkem, které mají vliv na důležité procesy omlazení a regenerace.   </p>
                    <div class="s4_i s4_i_1 upper abs text-center">
                        <span class="cnd">   Hladká   </span>   pleť
                    </div>
                    <div class="s4_i s4_i_2 upper abs text-center">
                        <span class="cnd">   Bez   </span>   nedostatků
                    </div>
                    <div class="s4_i s4_i_3 upper abs text-center">
                        <span class="cnd">ODSTRAŇENÍ</span> PROBLÉMU ZEVNITŘ</div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s5" id="s5">
            <div class="container">
                <div class="s5_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel">   Inovativní vývoj v Německu   </div>
                    <p class="basic_p text-center">   Vrásky se mohou objevit v důsledku oslabené imunity, UV záření, přirozeného stárnutí pokožky, dehydratace, použití nekvalitní kosmetiky, jakož i nesprávného čištění pleti. Goji Cream odstraňuje vrásky bez ohledu na příčinu a vyřeší problém trvale.   </p>
                    <div class="ribbon abs upper cnd">
                        <span>   Účinná úleva od vrásek   </span>
                    </div>
                    <div class="s5_img_1 rel">
                        <img alt="" class="s5_i1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s5_i1.png"/>
                        <span class="abs s5_span_1 upper">   Před   </span>
                        <span class="abs s5_span_2 upper">   Během   </span>
                        <span class="abs s5_span_3 upper">   Po   </span>
                    </div>
                    <div class="s5_b">
                        <div class="s5_b_i rel">
                            <span>   Ochranné vlastnosti   </span>
                            <p>   Zahrnuje různé přísady na ochranu proti ultrafialovému záření, a biologicky aktivní prvky pro restrukturalizaci kožní tkáně.</p>
                        </div>
                        <div class="s5_b_i rel">
                            <span>   Hluboké proniknutí   </span>
                            <p>Goji Cream aktivuje přirozenou tvorbu kolagenu v hlubších vrstvách kůže, vrátí pokožce pružnost a rychle Vás omladí!</p>
                        </div>
                        <div class="s5_b_i rel">
                            <span>   Jemná hydratace   </span>
                            <p>   Hydratuje pokožku, neucpává póry a zároveň nezpůsobuje nadměrnou aktivitu kožních žláz. Goji Cream neblokuje přístup kyslíku do buněk epidermis a zlepšuje krevní oběh v těchto oblastech.   </p>
                        </div>
                        <div class="s5_b_i rel">
                            <span>   Rovnováha   </span>
                            <p>Kombinuje čištění, hydrataci, tonizaci a výživu pleti, což vede k rovnováze aktivity mazových žláz, odstraňuje dehydrataci.   </p>
                        </div>
                    </div>
                    <img alt="" class="s5_i2" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s5_i2.png"/>
                    <div class="s5_b s5_b2">
                        <div class="s5_b_i rel">
                            <span>ÚČINKY PŘI POUŽÍVÁNÍ</span>
                            <p>   Pomáhá zabránit opětovnému vzniku vrásek tím, že obohacuje pokožku obličeje a krku širokým spektrem vitamínů, minerálů a aminokyselin.   </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s5_bottom">
                <div class="container">
                    <div class="s5_img_l">
                        <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s5_i3.jpg"/>
                    </div>
                    <div class="s5_img_r">
                        <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s5_i4.jpg"/>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s6" id="s6">
            <div class="s6_1">
                <div class="container">
                    <div class="ribbon abs upper cnd">
                        <span>   Jak používat Goji Cream?   </span>
                    </div>
                    <div class="s6_inner rel">
                        <div class="basic_tit s6_tit_1 upper cnd rel">   Individuální program na omlazení pleti   </div>
                        <p class="basic_p text-center">   Chcete-li zvolit ten správný program pro omlazení pleti, je třeba určit umístění vrásek. Goji Cream odborníci prozkoumali všechny možné formy vrásek, a pro vaše pohodlí vytvořili jasné schéma.   </p>
                        <span class="abs s6_span">   Na základě dlouholetých klinických zkušeností, dermatologové Goji Cream dělí formy vrásek do tří typů: čelo, oblast kolem očí a nasolabiální oblast, z nichž každý má své vlastní charakteristiky.   </span>
                    </div>
                </div>
            </div>
            <div class="s6_2 rel">
                <div class="container">
                    <div class="s6_btns abs text-center">
                        <div class="s6_btn s6_btn_good upper cnd"><span>   Čelo   </span></div>
                    </div>
                    <p>   Vodorovné čáry, přímý důsledek našich výrazů obličeje. Říká se jim "alarmující", protože se objevují jako důsledek emocí jako je úzkost a vztek;   </p>
                    <div class="s6_2_inner">
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_2_i1.png"/>
                            <span>   Před použitím   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_2_i2.png"/>
                            <span>   V průběhu používání   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_2_i3.png"/>
                            <span>   Po měsíci používání   </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s6_2 s6_3 rel">
                <div class="container">
                    <div class="s6_btns abs text-center">
                        <div class="s6_btn s6_btn_good upper cnd"><span>   Oblast kolem očí   </span></div>
                    </div>
                    <p>   "Vraní nohy" na vnějších koutcích očí. Jsou tvořeny v důsledku napětí očních svalů, například úsměvem. Vzhledem k tomu, že je kůže kolem očí velmi tenká, často jsou tam první viditelné vrásky, které vám dodávají unavený nebo negativní výraz.   </p>
                    <div class="s6_2_inner">
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_3_i1.png"/>
                            <span>   Před použitím   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_3_i2.png"/>
                            <span>   V průběhu používání   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_3_i3.png"/>
                            <span>   Po měsíci používání   </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="s6_2 s6_4 rel">
                    <div class="s6_btns abs text-center">
                        <div class="s6_btn s6_btn_good upper cnd"><span>   Nasolabiální oblast   </span></div>
                    </div>
                    <div class="text-fixier"></div>
                    <div class="s6_2_inner">
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_7_i1.png"/>
                            <span>   Před použitím   </span>
                        </div>
                    </div>
                </div>
                <div class="s6_2 s6_5 rel">
                    <div class="s6_btns abs text-center">
                    </div>
                    <p>   Nasolabiální oblast - velké záhyby od křídel nosu ke koutkům úst; řetězec vrásek v okolí rtů - vertikální vrásky nad horním rtem, a někdy dokonce i ve spodní části; vrásky v koutcích úst.   </p>
                    <div class="s6_2_inner">
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_7_i2.png"/>
                            <span>   V průběhu používání   </span>
                        </div>
                        <div class="s6_2_i">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s6_7_i3.png"/>
                            <span>   Po měsíci používání   </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s7" id="s7">
            <div class="s7_2">
                <div class="container">
                    <div class="s7_inner rel">
                        <div class="basic_tit s4_tit_1 upper cnd rel">   Rychle se zbavte vrásek pomocí Goji Cream - natrvalo   </div>
                        <div class="s7_subtit bold upper">   Rozdíl od tradičních systémů vyhlazení pleti   </div>
                        <div class="s5_b">
                            <div class="s5_b_i rel">
                                <p>   Typické produkty na odstranění vrásek mají vliv pouze na povrchové vrstvy kůže, takže jsou neúčinné, na rozdíl od Goji Cream   </p>
                            </div>
                            <div class="s5_b_i rel">
                                <p>   Goji Cream proniká do hlubších vrstev kůže, kde odstraňuje změny související s věkem, a nastartuje důležité procesy obnovy buněk a omlazení obličeje.   </p>
                            </div>
                            <div class="s5_b_i rel">
                                <p>   Goji Cream v krátké době vrací zpět pružnost pleti. Pokožka získává vnitřní elán, objeví se přirozený ruměnec, vrásky se vyhlazují, pokožka je hydratovaná a tónovaná a vypadá uvolněně.   </p>
                            </div>
                        </div>
                    </div>
                    <div class="s7_b1 rel upper">
                        <div class="s7_i2 abs"></div>
                        <div class="s7_b1__bubble s7_i3 upper bold abs">   Samostatná léčba   </div>
                        <div class="s7_b1__bubble s7_i4 upper bold abs">   Goji Cream   </div>
                    </div>
                </div>
                <div class="s7_b2"></div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s9" id="s9">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>VYHRAJETE BOJ NAD VRÁSKAMI</span>
                </div>
                <div class="s9_inner rel">
                    <div class="basic_tit s4_tit_1 upper cnd rel">   Nejlepší řešení pro vaši pokožku   </div>
                </div>
                <div class="s9_b1">
                    <div class="s9_item">
                        <div class="s9_item_tit">   Aktivní složky   </div>
                        <div class="s9_item_txt">   Krém se skládá z bobulí kustovnice, lykopen, kyseliny hyaluronové, buněčných antioxidantů, které podporují obnovu kožních buněk a z komplexu minerálů.   </div>
                    </div>
                    <div class="s9_item">
                        <div class="s9_item_tit">   Účinek   </div>
                        <div class="s9_item_txt">   Díky mimořádně bohatému složení krém přispívá k dosažení stabilního výsledku, vrásky se nevracejí a pokud ano, jsou téměř neznatelné. Pleť obličeje a krku se stává hladká, pružná a plná vitální energie, v dobré kondici.   </div>
                    </div>
                    <div class="s9_item">
                        <div class="s9_item_tit">   Pro koho je určen   </div>
                        <div class="s9_item_txt">   Ženy s drobnými a velkými vráskami   </div>
                    </div>
                    <div class="s9_price">
                        Původní cena: <span class="td-l">2600 Kč</span><br/>
                        Nová cena:    <span>1300 Kč</span>
                    </div>
                    <div class="s9_action">
                        <button class="s9_buy pre_toform" type="button">   Koupit   </button>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s10" id="s10">
            <div class="s10_top">
                <div class="container">
                    <div class="ribbon abs upper cnd">
                        <span>   Všechny poznatky německých odborníků v jednom balení krému   </span>
                    </div>
                    <div class="s10_inner rel text-center">
                        <div class="basic_tit s4_tit_1 upper cnd rel">   Goji Cream obsahuje   </div>
                    </div>
                </div>
            </div>
            <div class="s10_bottom">
                <div class="container">
                    <div class="s10_b1">
                        <div class="s10_b1_item s10_b1_item_1">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i1.png"/>
                            <div class="s10_b1_item_tit upper">   Aktivní forma vitaminu A   </div>
                            <p class="s10_b1_item_txt">   je přítomen ve všech maskách na obličej. Vitamín A snižuje aktivitu mazových žláz, vysušuje mastnou pleť, stahuje rozšířené póry, vyrovnává a vyhlazuje pokožku.   </p>
                        </div>
                        <div class="s10_b1_item s10_b1_item_2">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i2.png"/>
                            <div class="s10_b1_item_tit upper">   Draslík
                                zvlhčuje pokožku.   </div>
                            <p class="s10_b1_item_txt">   zvlhčuje pokožku a chrání ji před vysoušením od předchozí složky.   </p>
                        </div>
                        <div class="s10_b1_item s10_b1_item_3">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i3.png"/>
                            <div class="s10_b1_item_tit upper">   Vitamín C   </div>
                            <p class="s10_b1_item_txt">   stimulátor imunity, antioxidant, chrání pokožku proti bakteriím.   </p>
                        </div>
                        <div class="s10_b1_item s10_b1_item_4">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i4.png"/>
                            <div class="s10_b1_item_tit upper">   Vitamín PP (B3)   </div>
                            <p class="s10_b1_item_txt">   Zlepšuje krev. Tento vitamín je nezbytný pro regeneraci buněk.   </p>
                        </div>
                        <div class="s10_b1_item s10_b1_item_5">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i5.png"/>
                            <div class="s10_b1_item_tit upper">   Vitamín K   </div>
                            <p class="s10_b1_item_txt">   podporuje syntézu pojivových tkání, pomáhá k vyhlazení pokožky.   </p>
                        </div>
                    </div>
                    <div class="s10_b2 rel">
                        <div class="ribbon abs upper cnd">
                            <span>   Testování léčebných účinků Goji Cream   </span>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel">   Důkazy o účinnosti omlazujících vlastností   </div>
                        </div>
                        <div class="s10_i3 s10_i3_1">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i6.jpg"/>
                            <div class="s10_i3_block">
                                <div class="s10_i3_tit upper">   Metoda analýzy výrobku   </div>
                                <div class="s10_i3_txt">   Čistá kultura Staphylococcus aureus byla umístěna v prostředí proteinu. Pak se na sterilní filtrační papír nanesl Goji Cream. Nakonec se destička obsahující vyšetřovány komponenty zahřeje na 37 ° C a ponechá po dobu 24 hodin.   </div>
                            </div>
                            <div class="s10_i3_block">
                                <div class="s10_i3_tit upper">   Výsledky analýzy   </div>
                                <div class="s10_i3_txt">   Na základě výsledků lze vyvodit závěr, že Goji Cream má vysokou účinnost při použití. To je dáno především tím, že obsahuje vitaminy a minerály, stejně jako biotin, tyto látky jsou spojeny v molekule, a díky tomu pronikají do hlubších vrstev, dodávání živin do epidermis je velmi efektivní a tam dochází k omlazení pleti.   </div>
                            </div>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel">   Testování krému proti vráskám Goji Cream   </div>
                        </div>
                        <div class="s10_i3 s10_i3_2">
                            <div class="s10_i3_block s10_i3_block_3">
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper">   Zkušební metoda   </div>
                                    <div class="s10_i3_txt">   Glycerin je základní součást mazu. Goji Cream a ostatní prostředky byly umístěny na kapku glycerolu a během 2 minut vidíme, jak dobře goji Cream a ostatní prostředky pronikají dovnitř.   </div>
                                </div>
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i7.jpg"/>
                                </div>
                            </div>
                            <div class="s10_i3_block s10_i3_block_4">
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper">   Výsledky testů   </div>
                                    <div class="s10_i3_txt">   O dvě minuty později se kapka běžných prostředků stále drží na povrchu glycerinu, zatímco Goji Cream je rovnoměrně rozdělen po povrchu glycerolu, rychle rozpuštěný a díky tomu, že obsahuje sensitinogen, nedráždí kůži, což je důležité zejména pro citlivou pokožku.  </div>
                                </div>
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i8.jpg" style="margin-top: 80px;"/>
                                </div>
                            </div>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel">   Testování hydratačních vlastností   </div>
                        </div>
                        <div class="s10_i3 s10_i3_4">
                            <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i9.jpg"/>
                            <div class="s10_i3_block s10_i3_block_4">
                                <div class="s10_i3_block__inner">
                                    <div class="s10_i3_tit upper">   Zkušební metoda   </div>
                                    <div class="s10_i3_txt">   Je zobrazený obsah vlhkosti v pokožce před a po použití. Před a po aplikaci prostředků se odebírají vzorky, který ukazuje na stupeň nasycení pokožky vlhkostí; tento test ukazuje, jak dobře Goji Cream zvlhčuje pokožku.   </div>
                                </div>
                                <div class="s10_i3_block__img">
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/s10_i10.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="s10_b2 rel">
                        <div class="ribbon abs upper cnd">
                            <span>   Ženy po celém světě doporučují   </span>
                        </div>
                        <div class="s10_inner rel text-center">
                            <div class="basic_tit s4_tit_1 upper cnd rel">   Recenze produktů Goji Cream   </div>
                        </div>
                    </div>
                    <div class="s10_b4 rel">
                        <div class="slider">
                            <div class="slider-prev"></div>
                            <ul class="bxslider">
                                <li>
                                    <div class="slide_tit">   Alena Petrová, 45 let   </div>
                                    <p class="slider_p italic">   Vždycky jsem vypadala mladší a byla jsem na to velmi hrdá. Po vážném onemocnění jsem měla deprese a nestarala jsem se o sebe, což ovlivnilo můj vzhled. Aby mě nějak podpořil, přítel mi nabídl práci v jeho společnosti. To zahrnovalo práci s lidmi a vzhled hraje významnou roli. Bylo nutné získat zpět atraktivní a mladistvý vzhled tak rychle, jak to bylo možné. Pomohl mi Goji Cream! Klienti, s nimiž jsem komunikovala si mysleli, že jsem o 10 let mladší!   </p>
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/slide_1.jpg"/>
                                </li>
                                <li>
                                    <div class="slide_tit">   Jana Pávková, 56 let   </div>
                                    <p class="slider_p italic">   Podívejte se to, jak mnoho hodnocení má tento krém. A to z dobrého důvodu. Používám krém asi rok a nikdy jsem nelitovala, že jsem ho koupila on-line. Je mi 56 let. Ale věřte mi, vypadám o dost mladší. A musím, protože můj manžel je o 14 let mladší. Takže, milí přátelé, nešetřete na sobě.   </p>
                                    <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/slide_2.jpg"/>
                                </li>
                            </ul>
                            <div class="slider-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s12" id="s12">
            <div class="container">
                <div class="ribbon abs upper cnd">
                    <span>   Nedávné hodnocení   </span>
                </div>
                <div class="s12_inner">
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit">   Anna Kaiserová, 42 let   </div>
                            <div class="s12_tim"><script type="text/javascript">dtime_nums(-5, true)</script></div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Po 30 se mi začaly objevovat vrásky a bylo jich více a více každý rok. Obrátila jsem se na kliniku, používala jsem drahé léky a postupy, ale jejich účinek nebyl trvalý. Hlavní výhodou tohoto produktu je jeho přístupnost. To je pro mě velmi důležité, umožňuje důsledně používat ten krém.
                        </div>
                    </div>
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit">   Olga Naumová, 34   </div>
                            <div class="s12_tim"><script type="text/javascript">dtime_nums(-8, true)</script></div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Kamarádka mi poslala odkaz na internetové stránky a já jsem si objednala. Zpočátku jsem si nevšimla odlišnosti od běžných krémů (používala jsem pouze kosmetiku z lékárny). Ale ... teď uplynul měsíc. Tak krásná moje pleť nebyla nikdy! Vyhodila jsem veškerou další kosmetiku. Nejsem vůbec mastná! Všechny vrásky zmizely a nové se neobjevily!
                        </div>
                    </div>
                    <div class="s12_item">
                        <div class="s12_item_l">
                            <div class="s12_tit">   Eliška Novotná, 38   </div>
                            <div class="s12_tim"><script type="text/javascript">dtime_nums(-10, true)</script></div>
                            <div class="s12_star">
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                                <img alt="" src="//st.acstnst.com/content/GojiCream_CZ_Pink/img/star.png"/>
                            </div>
                        </div>
                        <div class="s12_item_r">
                            Objednala jsem si měsíc před Novým rokem. Účinek je uspokojivý. Pokud to tak bude i nadále, myslím, že jsem si vybrala ten pravý krém. Děkuji. Doporučuji ženám po 35. roce věku
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="s100">
            <div class="container">
                <div class="text-center s100_tit white">   Objednat   </div>
                <form action="" id="order_form" class="orderform" method="post">

                    <div class="footer_price">
                        Původní cena: <span class="old_price">2600 Kč</span><br/>
                        Nová cena:    <span class="price">1300 Kč</span>
                    </div>
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abdcn.pro/forms/?target=-4AAIJIAINEwAAAAAAAAAAAATe1-u9AA"></iframe>
                    <div class="toform"></div>
                </form>
            </div>
        </section>
        <div class="clearfix"></div>
        <footer></footer>
        <!-- HIDE_IT-END -->
    </div>
    <!-- HIDDEN_WINDOW -->
    <div class="hidden-window">
        <div class="second-clips-header">
            <div class="containerz">
                <div class="h-w__inner">
                    <h1 class="logotop">Goji cream</h1>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Platba po dodání!
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>1
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/imagesSec/dtc1.png"/>
                                    </div>
                                    <div class="dtable-cell">Objednávka
                                    </div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>2
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/imagesSec/dtc2.png"/>
                                    </div>
                                    <div class="dtable-cell">Poštovné
                                    </div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>3
                                        </i>
                                        <img alt="alt1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/imagesSec/dtc3.png"/>
                                    </div>
                                    <div class="dtable-cell">Dodání a platba
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Výhodná nabídka
                        </div>
                        <form action="/order/create/" class="js_scrollForm" method="post"><input type="hidden" name="total_price" value="1300.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAINEwRLTQCMAAEAAQACchIBAAJlHAIGAQEABLpztYMA">
                            <input type="hidden" name="goods_id" value="24">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="GojiCream_CZ_Pink">
                            <input type="hidden" name="price" value="1300">
                            <input type="hidden" name="old_price" value="2600">
                            <input type="hidden" name="total_price_wo_shipping" value="1300.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 1300, 'old_price': 2600, 'shipment_price': 0}, u'3': {'price': 2600, 'old_price': 5200, 'shipment_price': 0}, u'5': {'price': 3900, 'old_price': 7800, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Kč">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="CZ">
                            <input type="hidden" name="shipment_vat" value="0.21">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-pl">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.21">
                            <div class="formHeader">
<span>Entrez vos données
                  </span>
                                <br/> pour passer commande:
                            </div>
                            <select style="display: none;" class="select inp" id="country_code_selector" name="country_code">
                                <option value="CZ">Česká republika
                                </option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option value="1">1 Balení!
                                </option>
                                <option selected="selected" value="3">2+1 Balení!
                                </option>
                                <option value="5">3+2 Balení!
                                </option>
                            </select>
                            <input style="display: none;" class="inp j-inp" data-count-length="2+" name="name" placeholder="Jméno a příjmení" type="text" value=""/>
                            <input style="display: none;" class="only_number inp j-inp" name="phone" placeholder="Telefonní číslo" type="text" value=""/>

                            <input class="inp" name="address" placeholder="Adresa" type="text"/>

                            <!--
                            <input class="inp" name="house" placeholder="Entrez votre House" style="display:none;" type="text"/>
                            <input class="inp" name="city" placeholder="Entrez votre ville" style="display:none;" type="text"/>
                            <input class="inp" name="address" placeholder="Entrer votre adresse" type="text"/>
                            -->
                            <div class="text-center totalpriceForm">Celková cena:
                                <span class="js_total_price js_full_price">1300
                  </span> Kč
                            </div>
                            <div class="zbtn transitionmyl js_submit">Objednať
                            </div>
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Garantujeme:
                            </div>
                            <ul>
                                <li>
                                    <b>100%
                                    </b> kvalitu
                                </li>
                                <li>
                                    <b>Kontrolu
                                    </b> produktu při převzetí
                                </li>
                                <li>
                                    <b>Bezpečnost
                                    </b> vašich údajů
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1 ">
                            <div class="zDiscount">
                                <b>Pozor:
                                </b> Máme nyní
                                <br/>
                                <span>50% slevu
                  </span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader">
                                    <b>1
                                    </b> Balení
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Cena:
                                            </div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new">  1300
                                                    </text>
                                                    Kč
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old"> 2600
                                </text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Stará
                                <br/>cena
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Verzending:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship">  0
                                                    </text>
                                                    Kč
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Celková cena:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 1300
                                                    </text>
                                                </b>
                                                Kč
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="1">
                                </div>
                                <div class="img-wrapp">
                                    <img alt="z1 z1-1" class="z1-1" src="//st.acstnst.com/content/GojiCream_CZ_Pink/imagesSec/pack-1.png"/>
                                </div>
                                <div class="zHeader">
<span>
</span>
                                </div>
                                <text>Viditelné omlazení už po měsíci používání.
                                    <br/>
                                    <br/>
                                </text>
                            </div>
                        </div>
                        <div class="item hot transitionmyl1 active ">
                            <div class="zstick">
                            </div>
                            <div class="zDiscount">
                                <b>Pozor:
                                </b> Máme nyní
                                <br/>
                                <span>50% slevu
                  </span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec">
                                    <b>2
                                    </b> Balení
                                    <span class="dib zplus">Dárek
                    </span>
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Cena:
                                            </div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new">  2600
                                                    </text>
                                                    Kč
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old"> 5200
                                </text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Stará
                                <br/> cena
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Verzending:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship">  0
                                                    </text>
                                                    Kč
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Celková cena:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full"> 2600
                                                    </text>
                                                </b>
                                                Kč
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3">
                                </div>
                                <div class="img-wrapp">
                                    <img alt="z1 z1-2" class="z1-2" src="//st.acstnst.com/content/GojiCream_CZ_Pink/imagesSec/pack-2.png"/>
                                </div>
                                <div class="zHeader">
<span>
</span>
                                </div>
                                <text>Cesta k mládí! Liftingový efekt a vyhlazení i těch nejhlubších vrásek už za tři měsíce. Garantované výsledky.
                                    <br/>
                                </text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Pozor:
                                </b>Máme nyní
                                <br/>
                                <span>50% slevu
                  </span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec">
                                    <b>3
                                    </b> Balení
                                    <span class="dib zplus sec">Dárky
                    </span>
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Cena:
                                            </div>
                                            <div class="dtable-cell nowrap">
                                                <b>
                                                    <text class="js-pp-new">  3900
                                                    </text>
                                                    Kč
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old"> 7800
                                </text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Stará
                                <br/>cena
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Verzending:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>
<span>
<text class="js-pp-ship">
                                0

                              </text>
</span>
                                                    Kč
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">Celková cena:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full">
                                                        3900

                                                    </text>
                                                </b>
                                                Kč
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5">
                                </div>
                                <div class="img-wrapp">
                                    <img alt="z1 z1-3" class="z1-3" src="//st.acstnst.com/content/GojiCream_CZ_Pink/imagesSec/pack-3.png"/>
                                </div>
                                <div class="zHeader sm">
                                </div>
                                <text>
                                    Maximální omlazení! Celá půlroční kůra vás nejen zbaví změn způsobených věkem, ale také pomůže předejít jejich opětovnému vzniku.
                                </text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script src="//st.acstnst.com/content/GojiCream_CZ_Pink/js/bxslider.js"></script>
    <script>
        $(document).ready(function () {
            $('.bxslider').bxSlider({
                speed: 1000,
                pause: 10000,
                minSlides: 1,
                maxSlides: 1,
                slideMargin: 15,
                slideWidth: 790,
                nextSelector: '.slider-next',
                prevSelector: '.slider-prev',
                auto: false,
                autoStart: true,
                pager: false,
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.toform').click(function() {
                $("html, body").animate({
                    scrollTop: $(".orderform").offset().top - 300
                }, 2000);
                return false;
            });
        });
    </script>
    </body>
    </html>
<?php } ?>