<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com -->
<!--  Last Published: Sat Dec 17 2016 10:54:33 GMT+0000 (UTC)  -->
<html data-wf-page="5810971962cf032f7b79945a" data-wf-site="580634c7011fc44651f15e61">
<head>
    <meta charset="utf-8">
    <title>Parasite-detox.info </title>
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <link href="./css/normalize.css" rel="stylesheet" type="text/css">
    <link href="./css/webflow.css" rel="stylesheet" type="text/css">
    <link href="./css/leadbit.webflow.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
    <script type="text/javascript">
        WebFont.load({
            google: {
                families: ["Montserrat:400,700","Roboto:300,regular,500"]
            }
        });
    </script>
    <script src="./js/modernizr.js" type="text/javascript"></script>

</head>
<body>
<div class="navigation-bar w-nav " style="padding-top: 0px" data-animation="default" data-collapse="medium"
     data-duration="400">

    <div style="width:60%;float:left;padding-top: 30px; padding-left:50px"><a class="" href="./index.php"><p style="font-weight: bold; font-size:50px;"><img src="./images/logo.png" style="width:100px;margin-top:-30px;"> Parasite-Detox</p></a>


    </div>
    <div style="width:40%;float:left;padding-top: 40px;padding-right: 50px">
        <div class="w-container">


            <nav class="navigation-menu w-nav-menu" role="navigation">

                <a class="navigation-link w-nav-link" href="./">Zdrowie</a>

                <!--  <a class="navigation-link w-nav-link" href="./detoxic.php"> E-shop</a>-->
                <a class="navigation-link w-nav-link" href="./contact.php">Kontakt</a>

            </nav>
            <div class="hamburger-button w-nav-button">
                <div class="w-icon-nav-menu"></div>
            </div>
        </div>
    </div>
</div>
<div class="product-section">
    <div class="w-container w-row">

        <div class="product-row w-col w-col-8" style="padding-bottom: 20px;padding-top:20px">
            <div class=" w-col w-col-1"></div>
            <div class="w-col w-col-12" style="padding-bottom: 20px">

                <div class=" w-col w-col-12">

                    <div class="w-col w-col-6">
                        <img style=";padding-right: 5px;padding-top:20px;" src="./images/kaszel.jpg" >
                    </div>
                    <div class="w-col w-col-6">
                        <h2>Domowe sposoby na kaszel

                        </h2>
                        <p>Coraz częściej rezygnujemy ze stosowania różnych lekarstw i środków medycznych. Swoją popularność zyskują naturalne sposoby, które mają nam pomóc w przypadku różnych chorób. Oczywiście warto po nie sięgnąć, np. podczas uporczywego kaszlu. W końcu taki domowy sposób na kaszel nie musi być gorszy od drogich lekarstw. A będzie on całkowicie naturalny i na pewno nam nie zaszkodzi
                        </p>
                        <a href="./health/cough.html">czytaj więcej </a>

                    </div>


                </div>


            </div>
            <hr>
            <div class="w-col w-col-12" style="padding-bottom: 20px">

                <div class=" w-col w-col-12">
                    <div class="w-col w-col-6">
                        <img style=";padding-right: 5px;padding-top:20px;" src="./images/grypa.jpg">
                    </div>
                    <div class="w-col w-col-6">
                        <h2>Domowa walka z przeziębieniem

                        </h2>
                        <p>
                            Gdy pogoda gwałtownie zaczyna się zmieniać, wszyscy jesteśmy narażeni na przeziębienie. Jest to dość łagodna choroba, jednak niewyleczona, może grozić powikłaniami. W tym przypadku nie musimy od razu odwiedzać specjalistycznego gabinetu lekarskiego. Skorzystajmy z domowych sposobów, które wcale nie są gorsze od najdroższych lekarstw.
                        </p>
                        <a href="./health/cold.html">czytaj więcej </a>

                    </div>


                </div>
                <div class=" w-col w-col-2"></div>

            </div>
            <hr>

            <div class="w-col w-col-12" style="padding-bottom: 20px">

                <div class=" w-col w-col-12">
                    <div class="w-col w-col-6">
                        <img style="padding-right: 5px;padding-top:20px;" src="./images/cold123.jpg">
                    </div>
                    <div class="w-col w-col-6">
                        <h2>Rodzaje Kaszlu

                        </h2>
                        <p>W dzisiejszych czasach nietrudno o przeziębienie lub grypę. A te choroby znacznie utrudniają nam całe życie. Często towarzyszy im męczący kaszel. Kupujemy wtedy różnorodne lekarstwa, które na dłuższą metę wcale nam nie pomagają. Dlaczego? Ponieważ nie określiliśmy rodzaju swojego kaszlu. A to bardzo cenna informacja, która pozwoli nam całkowicie się wyleczyć.  </p>
                        <a href="./health/cough2.html">czytaj więcej </a>

                    </div>


                </div>
                <div class=" w-col w-col-2"></div>

            </div>
        <hr>

            <div class="w-col w-col-6">
                <div class="w-col w-col-12" style="padding-bottom: 30px">

                    <img style=";padding-right: 5px"  height="214" src="./images/leki.jpg"><br>
                    <a href="./health/drugs.html" style="color: rgba(0, 0, 0, .74);"><b>Lek na przeziębienie</b></a>

                </div>
                </div>

            <div class="w-col w-col-6">
                <div class="w-col w-col-12" style="padding-bottom: 30px">

                    <img style=";padding-right: 5px" height="214" src="./images/cold_flu.jpg">
                    <a href="./health/cold2.html" style="color: rgba(0, 0, 0, .74);"><b>Na przeziębienie</b></a>

                </div>
            </div>


</div>

        <div class="w-col w-col-4">

            <div class="w-col w-col-12" style="padding-bottom: 30px">

                <img style=";padding-right: 5px" src="./images/natu.jpg">
                <a href="./health/natural_drugs.html" style="color: rgba(0, 0, 0, .74);"><b>Naturalne leki na przeziębienie</b></a>

            </div>

            <div class="w-col w-col-12" style="padding-bottom: 30px">

                <img style=";padding-right: 5px" src="./images/cought42.jpg">
                <a href="./health/be_health.html" style="color: rgba(0, 0, 0, .74);"><b>W jaki sposób uchronić się przed przeziębieniem?</b></a>

            </div>
            <div class="w-col w-col-12" style="padding-bottom: 30px">

                <img style=";padding-right: 5px" src="./images/cough2.jpg">
                <a href="./health/gardlo.html" style="color: rgba(0, 0, 0, .74);"><b>Ból gardła</b></a>

            </div>

            <div class="w-col w-col-12" style="padding-bottom: 30px">

                <img style=";padding-right: 5px" src="./images/aaaa.jpg">
                <a href="./health/drugs2.html" style="color: rgba(0, 0, 0, .74);"><b>
                        Naturalne lekarstwa na gardło</b></a>

            </div>

            </div>
        </div>
<div class="footer ">

    <div class="footer-text"><a href="./images/policy.png"> Privacy policy</a>  | <a href="./images/terms.png">Terms of Use </a>

        <br>© <script language="JavaScript" type="text/javascript">



            now = new Date
            theYear = now.getYear()
            if (theYear < 1900)
                theYear = theYear + 1900
            document.write(theYear)
        </script>  parasite-detox.info </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
<script src="./js/webflow.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>