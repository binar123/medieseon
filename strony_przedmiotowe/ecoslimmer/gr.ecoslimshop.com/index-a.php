<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 11602 -->
    <script>var locale = "cy";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQEvAnoWBJE9DogAAAEAAvgVAQACUi0CBgEBAATKdNbwAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript"
            src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height: 0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }

        .ac_footer a {
            color: #A12000;
        }

        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {
            "1": {"price": 49, "old_price": 98, "shipment_price": 0},
            "3": {"price": 98, "old_price": 196, "shipment_price": 0},
            "5": {"price": 147, "old_price": 294, "shipment_price": 0}
        };
        var shipment_price = 0;
        var name_hint = 'Nyiri Andrea';
        var phone_hint = '+36303408947';

        $(document).ready(function () {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a><br><a class="download" href="#">Download our Tips!</a></div>');

            moment.locale("cy");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));
            $('.download').click(function(e) {
                e.preventDefault();  //stop the browser from following
                window.location.href = 'out_tips.pdf';
            });

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet"
          media="all">


    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', '//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->


    <meta charset="utf-8"/>
    <meta content="width=1360" name="viewport"/>
    <title>EcoSlim</title>
    <link href="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/css/style.css" rel="stylesheet"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.toform').click(function () {
                $("html, body").animate({scrollTop: $(".block_9 form").offset().top - 300}, 1000);
                return false;
            });
        });
    </script>
    <style>
        .product div:before{
            background: none;
        }
        .block_3 .container:before{
            background: none;
        }
        .block_4{
            background: none;
        }
        .block_6{
            background: none;
        }
        .block_6:before{
            background: none;
        }
    </style>
</head>
<body>
<div class="main body">
    <div class="block_1">
        <div class="elements">
            <div class="container">
                <div class="title">
                    <p>Τι είναι το Hoodia </b></p>
                    <p><span class="logo"> </span></p>
                </div>

                <div class="product">
                    <div>
                              <small><span><b>2016</b> </span></small>
                    </div>
                </div>
                <div class="form_pr">
                    <div class="form">
                        <p> Μερικοί άνθρωποι πιστεύουν ό, οτι το φυτό Hoodia ανακαλύφτηκε μόνο μερικά χρόνια πριν. Άλλα αυτό δεν αληθεύει. Το Hoodia ανακαλύφθηκε πολλούς αιώνες πριν από τις φυλές San Bushman της Νότιας Αφρικής.

                            Αυτές οι φυλές καταναλώνανε την Hoodia επειδή κατέστειλε την πείνα και δεν προκαλούσε μείωση της ενέργειας τους. Το χρησιμοποιούσαν όταν πηγαίνανε για μεγάλα ταξίδια κυνηγιού. Το Hoodia μείωνε όχι μόνο την όρεξη άλλα και την δίψα, πράγμα που τους βοηθούσε να αντέξουν στην έρημο.
                        </p>


                    </div>
                         </div>
            </div>
        </div>
        <div class="bottom">
            <div class="container">
                <p></p></div>
        </div>
    </div>

    <div class="block_3">
        <div class="container">
            <div class="woman">
                 </div>
               <div class="text">
                <div class="why">

                    <p> Οι φυλές Bushmen ξεφλούδισαν το δέρμα και τα αγκάθια και τρώγανε την καρδιά του φυτού. Με αυτό τον τρόπο παίρνοντας ενέργεια και θρεπτικά συστατικά για να αντέξουν την πείνα και την έλλειψη νερού.</p></div>
                <div class="warning">
                    <h3> Το Hoodia μεγαλώνει μόνο στην Έρημο της Καλαχάρης. Το Hoodia μεγαλώνει καλύτερα σε θερμοκρασίες μεταξύ 51 και 61 βαθμού Κέλσιος. Αυτές είναι η μέση θερμοκρασία στην Ερήμου της Καλαχάρης. </h3>
                    <p> Ως ένα φυτό που μεγαλώνει στην έρημο η Hoodia μειάζει με έναν κάκτο και έχει αγκάθια σε όλο το σώμα τους. Το Hoodia χρειάζεται πάνω από πέντε χρόνια για να μεγαλώσει. Αυτό δεν είναι καθόλου μικρό χρονικό διάστημα.</p>
                </div>
            </div>
        </div>
        <div class="line"></div>
    </div>
    <div class="block_4">
        <div class="top">
            <div class="container">
                <h3> </h3>
                <div class="letter">
                    <p> Το Hoodia είναι ένα προστατευμένο φυτό και η παραγωγή του κάτω από την στενή επίβλεψη της Νοτιοαφρικανικής κυβέρνησης. Η παραγωγή του είναι περιορισμένη. Για αυτό το λόγο τα περισσότερα συμπληρώματα περιέχουν μικρή ποσότητα Hoodia.</p>
                    <p>Για αυτό το λόγο πριν αγοράσετε κάποιο συμπλήρωμα Hoodia πρέπει να σιγουρευτείτε ότι είναι το Hoodia προέρχεται από την Νότια Αφρική, από πιστοποιημένες φάρμες και ο, τι περιέχουν καθαρό Hoodia.
                    </p>

                </div>
                 </div>
        </div>
        <div class="bottom">
            <div class="container">
                <div class="eko">
                    <div>
                        <h3><b> Πως δουλεύει η Hoodia</h3>
                        <p></p>
                    </div>
                </div>
                <div class="woman">
                        </div>
                <div class="eko2">
                    <div><span> Οι δυνατότητες που έχει τη Hoodia Gordonii για να κατεστάλη την όρεξη είναι γνωστές για αιώνες στις φυλές Bushmen στην Νότια Αφρική. Την χρησιμοποιούσαν όταν κάνανε μεγάλα ταξίδια για κυνήγι στην έρημο.</span></div>
                </div>
            </div>
        </div>
        <div class="line"></div>
        <ul class="list">
            <li></li>
            <li></li>
        </ul>
    </div>

    <div class="block_6">
        <div class="container">
            <h3> Οι Bushmen την χρησιμοποιούσαν για να σβήσουν την δίψα και να καταστείλουν την πείνα, επειδή τα ταξίδια διαρκούσαν αρκετές φορές πολλές εβδομάδες. Σε αυτά τα ταξίδια το νερό και η τροφή ήταν σε μικρή ποσότητα. </h3>
            <p> Αυτή η γνώση των Bushmen έχει γίνει κτήμα όλού του κόσμού και έχει κάνει την Hoodia ένα από τα πιο δημοφιλές συμπληρώματα διατροφής.

            </p>
            <p>Πρώτα πρέπει να κατανοήσετε πως λειτουργεί η πείνα και ο χορτασμός. Η πείνα είναι η αίσθηση που αυξάνει την επιθυμία για φαγητό. Η πείνα ξεκινάει μετά από μερικές ώρες που δεν έχετε φάει. Η πείνα προκαλείτε από τις συστολές που συμβαίνουν στο στομάχι μετά από 12 έως 24 ώρες χωρίς να φάτε. Αυτός ο μηχανισμός είναι οι απαντήσεις που δημιουργούνται από το σώμα για να σας υπενθυμίσει να τρώτε.
            </p>
        </div>
        <div class="line"></div>
    </div>
    <div class="block_7">
        <div class="container">
            <div class="left">
                <h3>Πως μειώνει την όρεξη η Hoodia

                </h3>
                <p> Ενώ αυτός είναι ένα μηχανισμός για την επιβίωση του σώματος, πολλά άτομα δεν μπορούν να ελέγχουν την πείνα τους και τείνουν να παρατρώνε. Υπάρχου διάφοροι σύνθετοι μηχανισμοί που λαμβάνουν χώρα σε διάφορα όργανα όπως είναι το στομάχι, το ήπαρ και στο εγκέφαλο που δημιουργούν την αίσθηση της πληρότητας και της πείνας στον καθένα. Οι κατανάλωση τροφής δημιουργεί ορισμένα ερεθίσματα στο στομάχι που στέλνει σήματα στο εγκέφαλο που δημιουργεί την αίσθηση της πληρότητας ή του κορεσμού. </p>


            </div>
            <div class="right">
                <div class="img">
                        </div>
            </div>
        </div>
        <div class="line"></div>
    </div>
    <div class="block_8">
        <div class="container">
            <h3>Τα ενεργά συστατικά στο Hoodia και οι δράσει τους

            </h3>
            <ul>
                <li>
<span>
</span>
                    <span>Τα ενώσεις που υπάρχουν στο Hoodia Gordonii μιμούνται τις ουσίες που δημιουργούν την αίσθηση του κορεσμού στο σώμα. Μερικές μελέτες έχουν γίνει για να εξακριβωθεί το δραστικό συστατικό που ευθύνεται για την μείωση της όρεξης που προκαλεί η Hoodia.
   Με ένα μεγάλο αριθμό εταιρειών να πουλάνε προϊόντα που περιέχουν Hoodia πρέπει να προσέχετε για να αγοράσετε μόνο συμπληρώματα που περιέχουν αποκλειστικά μόνο Hoodia. Η χρήση συμπληρωμάτων που περιέχουν επαρκές ποσότητες Hoodia στην πιο αγνή μορφή του μπορεί να σας βοηθήσουν να χάσετε βάρος αποτελεσματικά και με φυσικό τρόπο.

                    </span>
                </li>
                <li>
<span>
<b>  Gustare. 2 caise proaspete </b>

						</span>
                    <span> Η μείωση της όρεξης που προκαλεί το Hoodia αποδίδετε σε μια ένωση που ονομάζεται P57. Η ελλείψει της αίσθησης της πείνας ή η αίσθηση του κορεσμού μειώνει την κατανάλωση της τροφής. Η μείωση της κατανάλωσης τροφής έχει θετική επίδραση στην μείωση του βάρους.
   Πολυάριθμες μελέτες που έγιναν σε ποντίκια επιβεβαιώνουν αυτή την ιδιότητα της Hoodia όπου διαπιστώθηκε ότι τα ποντίκια που είχαν πάρει Hoodia τρώγανε λιγότερη τροφή και χάσανε σημαντική ποσότητα βάρους. Επιπλέων, σε μια άλλη μελέτη, επιβεβαιώθηκε ότι το P57, το ενεργό συστατικό του Hoodia, επηρεάζει τον εγκέφαλο έτσι που να μειώνει την κατανάλωση της τροφής από 40% έως 60%
    </span>
                </li>
            </ul>
        </div>
        <div class="bottom">
            <div class="container">
                <div class="left">
                    <p> Hoodia, δίαιτα και ασκήσεις

                    </p>

                </div>
                <div class="product3">
                 </div>
                <div class="right">
                    <p>Η ιδιότητες της Hoodia για να καταστέλλει την όρεξη, μπορεί να βοηθήσουν κάποιον να ελέγχει την διατροφή του και να μπορεί να χάσει γρήγορα βάρος, όταν συνδυάζεται με μια κατάλληλη διατροφή και φυσικές ασκήσεις.
                         </p>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>