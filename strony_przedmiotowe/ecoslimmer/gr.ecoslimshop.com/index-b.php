<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 11602 -->
        <script>var locale = "cy";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQEvAnoWBCggDogAAAEAAvgVAQACUi0CBgEBAAQ0X-qMAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript"
                src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height: 0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }

            .ac_footer a {
                color: #A12000;
            }

            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {
                "1": {"price": 49, "old_price": 98, "shipment_price": 0},
                "3": {"price": 98, "old_price": 196, "shipment_price": 0},
                "5": {"price": 147, "old_price": 294, "shipment_price": 0}
            };
            var shipment_price = 0;
            var name_hint = 'Nyiri Andrea';
            var phone_hint = '+36303408947';

            $(document).ready(function () {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cy");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet"
              media="all">


        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', '//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <title>EcoSlim</title>
        <link href="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/css/style.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/js/jquery.bxslider.min.js"
                type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/js/common.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({scrollTop: $("form").offset().top - 300}, 1000);
                    return false;
                });
                $('.block_8 ul').bxSlider({
                    adaptiveHeight: true,
                    infiniteLoop: true,
                    touchEnabled: true,
                    controls: false
                });
            });
        </script>
    </head>
    <body>

    <div class="main first-page">
        <div class="block_1">
            <div class="container">
                <div class="title">
                    <p>Καινοτόμα Επιστημονική <b>Ανακάλυψη</b></p>
                    <p><span class="logo"><b>Eco</b>Slim</span></p>
                </div>
                <ul>
                    <li>ΑΣΦΑΛΕΣ ΑΔΥΝΑΤΙΣΜΑ</li>
                    <li>ΤΟ ΒΑΡΟΣ ΜΕΙΩΝΕΤΑΙ ΚΑΤΑ 10-12 KG ΤΟΝ ΜΗΝΑ</li>
                </ul>
                <div class="product">
                    <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/ecoslim.png"/>
                </div>

                <button class="jsOpenWindow"><span>Παραγγελια</span></button>

            </div>
        </div>
        <div class="block_2">
            <div class="container">
                <p>Παρασκευασμένο με <b>βαση τις βιταμινες Β</b> που προκαλούν ΤΗΝ ΔΙΑΛΥΣΗ ΤΟΥ ΛΙΠΟΥΣ</p>
                <ul>
                    <li><span>Μείον 10-12 kg τον μήνα</span></li>
                    <li><span>Δεν επηρεάζει την καρδιά και το νευρικό σύστημα</span></li>
                    <li><span>Εγγύηση ποιότητας, πλήρης περιβαλλοντική προστασία</span></li>
                    <li><span>100% φυσική σύνθεση</span></li>
                    <li><span>Επιδρά δραστικά στα αποθέματα λίπους</span></li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_3">
            <div class="container">
                <div class="why">
                    <h3>Γιατί παίρνουμε βάρος;</h3>
                    <p>Έχετε σίγουρα ακούσει πολλές φορές ότι τα αιτία που οδηγούν στην παχυσαρκία είναι ορμονικές
                        διαταραχές, η καθιστική ζωή και το περιβάλλον; Αυτοί ωστόσο είναι μόνο έμμεσοι παράγοντες.</p>
                </div>
                <div class="warning">
                    <h3>Στο 90% των περιπτώσεων, το περιττό βάρος αποτελεί έμμεση συνέπεια της μη φυσικής
                        διατροφής!</h3>
                </div>
            </div>
        </div>
        <div class="block_4">
            <div class="top">
                <div class="container">
                    <h3>το σώμα σας χρειάζεται βοήθεια!</h3>
                    <div class="signature"><span>Σχολιο ειδικου στον τομεα της διαιτολογιας, Μαρία Σαράντη</span></div>
                    <div class="letter">
                        <p>«Για να καθαρίσει το σώμα σας από μόνο του, χρησιμοποιήστε σταγόνες <b>EcoSlim</b> σε ένα
                            ποτήρι νερό και πιείτε το κατά τη διάρκεια του γεύματός σας ή μετά από αυτό, μία φορά την
                            ημέρα. Σύντομα θα αισθανθείτε τη διαφορά - μέσα σε μόλις μία εβδομάδα το βάρος σας θα
                            μειωθεί και θα εξαφανιστούν τα προβλήματα με το στομάχι και την πέψη σας. Το πιο
                            καθησυχαστικό είναι το εξής: η μοναδική σύσταση του προϊόντος σχεδιάστηκε σε συνεργασία με
                            το Ινστιτούτο Διατροφής. Τα φυσικά συμπλέγματα βιταμινών είναι πολύ πιο αποδοτικά και ασφαλή
                            από οποιοδήποτε άλλο χημικό συστατικό. Ο συνδυασμός ταυρίνης, καφεΐνης και εκχυλίσματα
                            γκουαρανά μπορεί να θεωρηθεί με ασφάλεια ένα πραγματικό δώρο για όσους αδυνατίζουν, καθώς το
                            σύμπλεγμα <b>διαλύει τα αποθέματα λίπους στις προβληματικές περιοχές μέσα σε λίγες μόλις
                                ημέρες.</b>»</p>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <h3>EcoSlim</h3>
                    <p>100% φυσικο και οικολογικο <br/>αγνο προϊον</p>
                </div>
            </div>
        </div>
        <div class="block_5">
            <div class="top">
                <div class="container">
                    <h3><font>Μοναδικά επιλεγμένη σύνθεση</font> για να επιτύχετε το μέγιστο αποτέλεσμα:</h3>
                    <ul>
                        <li><span>L-Καρνιτίνη</span></li>
                        <li><span>Καφεΐνη</span></li>
                        <li><span>Χιτοζάνη</span></li>
                        <li><span>Εκχύλισμα Ινδικός Κολεός</span></li>
                        <li><span>Εκχύλισμα γκουαρανά</span></li>
                        <li><span>Ηλεκτρικό οξύ</span></li>
                        <li><span>Εκχύλισμα αλγών Fucus</span></li>
                        <li><span>Βιταμίνες В2, В5, В6, В8, В12</span></li>
                    </ul>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <h3>EcoSlim</h3>
                    <p>Σχεδιασμένο με βάση Ευρωπαϊκές τεχνολογίες</p>
                </div>
            </div>
        </div>
        <div class="block_8">
            <div class="container">
                <h3>Σχόλια από τους πελάτες μας</h3>
                <ul>
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/block_8_li1.jpg"/>
<b>Μάρθα Σούλη</b>
							24 ετών
						</span>
                        <span>Μια ζωή χοντρή και αδυνατίζεις σε έναν μήνα - είναι εκπληκτικό! <b>Είμαι πολύ ικανοποιημένη με το Eco Slim.</b> Είναι γευστικό, περιορίζει την όρεξη και το βάρος στο στομάχι εξαφανίζεται. Πρόκειται για το πρώτο προϊόν με το οποίο κατάφερα να χάσω 13 κιλά! Στην αρχή το βάρος άρχισε να φεύγει αργά, στη συνέχεια άρχισα να χάνω 1 κιλό την ημέρα.</span>
                    </li>
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/block_8_li2.jpg"/>
<b>Σταύρος Βασιλακάκης</b>
							32 ετών
						</span>
                        <span>Για να είμαι ειλικρινής, αδυνάτισα για χάρη της κόρης μου, γιατί δεν μπορούσα να τρέξω και να παίξω μαζί της, μου ήταν δύσκολο ακόμη και να σκύψω. Έχασα πάνω από 10 κιλά εύκολα και στη συνέχεια ξεκίνησα το γυμναστήριο. <b>Το Eco Slim είναι ένα σπουδαίο προϊόν.</b> Εξακολουθώ να το πίνω, όταν δεν πηγαίνω γυμναστήριο ή όταν τρώω λίγο παραπάνω στις διακοπές.</span>
                    </li>
                </ul>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="left">
                        <p>Έχουμε ήδη πουλήσει <br/>πάνω από 20 χιλιάδες πακέτα του</p>
                    </div>
                    <div class="product3">
                        <img height="200" src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/ecoslim.png"/>
                    </div>
                    <div class="right">
                        <p><b>Βιαστείτε να το παραγγείλετε</b> σε μειωμένη τιμή <b>τώρα</b> για να αποκτήσετε τέλεια
                            σιλουέτα <b>σε λίγες εβδομάδες!</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_9">
            <div class="container">
                <p>Παραγγείλετε το EcoSlim από εμάς</p>
                <div class="form_pr">
                    <div class="price">
                        <span class="precent">έκπτωση <span>50%</span></span>
                        <span class="old_price">98 €</span>
                        <span class="new_price"><span>49</span> <span>€</span></span>
                    </div>
                    <div class="form">
                        <form action="/order/create/" method="post">

                            <div class="prices">
                            </div>
                            <button class="jsOpenWindow"><span>Παραγγελια</span></button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_10">
            <div class="container"></div>
        </div>
    </div>


    <link href="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/css/stylesecond.css" rel="stylesheet"/>
    <div class="hidden-window">
        <div class="containerz">
            <section class="h-w__inner">
                <div class="h-w__header">
                    <span>EcoSlim</span> <br/>oοναδική προσφορά μόνο για σήμερα!
                </div>
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Πληρωμή με <b>αντικαταβολή</b></div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div><img alt="arrow" class="zarrr"
                                                      src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/imageSecond/Zarr.png"/><img
                                                    alt="alt1"
                                                    src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/imageSecond/dtc1.png"/>
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div><img alt="alt1"
                                                      src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/imageSecond/dtc2.png"/>
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div><img alt="arrow" class="zarrrLeft"
                                                      src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/imageSecond/Zarr.png"/><img
                                                    alt="alt1"
                                                    src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/imageSecond/dtc3.png"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div>Η παραγγελία</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div>Μεταφορικά</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div>Παράδοση και <span>πληρωμή</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">Καλή ευκαιρία</div>
                            <select class="corbselect select inp select_count change-package-selector"
                                    name="count_select">
                                <option data-slide-index="0" value="1">1 πακέτο</option>
                                <option data-slide-index="1" selected="selected" value="3">2 πακέτο + ένα δώρο</option>
                                <option data-slide-index="2" value="5">3 πακέτο + 2 δώρα</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right side-right--columns">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile"></div>
                                                    <img alt="zt1"
                                                         src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/imageSecond/zit1.png"/>
                                                </div>
                                                <div class="zHeader" style="padding-top: 30px;"><b>1</b> πακέτο</div>
                                            </div>
                                            <div class="pack_descr pack_descr--columns">Ταχύτατο αδυνάτισμα με 50%
                                                έκπτωση. Άμεσο αποτέλεσμα!
                                            </div>
                                        </div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Τιμή:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">49</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>98</div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right side-right--columns">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile">δώρο</div>
                                                    <img alt="zt1"
                                                         src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/imageSecond/zit2.png"/>
                                                </div>
                                                <div class="zHeader"><b>2</b> πακέτο
                                                    <br/> <span class="dib zplus">δώρο</span></div>
                                            </div>
                                            <div class="pack_descr pack_descr--columns">Σταθερό αποτέλεσμα. Το πρόγραμμα
                                                με το Eco Slim θα σας βοηθήσει να ξεφορτωθείτε μία για πάντα το περιττό
                                                σας βάρος!
                                            </div>
                                        </div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Τιμή:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">98</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>196</div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right side-right--columns">
                                            <div class="ssimgabs">
                                                <div class="giftmobile">δώρα</div>
                                                <img alt="zt1"
                                                     src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/mobile/img/imageSecond/zit3.png"/>
                                            </div>
                                            <div class="zHeader"><b>3</b> πακέτο
                                                <br/> <span class="dib zplus sec">δώρα</span></div>
                                        </div>
                                        <div class="pack_descr pack_descr--columns">Επαγγελματική προσέγγιση. Ένα
                                            εξάμηνο πρόγραμμα με το Eco Slim σας βοηθά ακόμη και στην περίπτωση πολύ
                                            μεγάλου βάρους!
                                        </div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Τιμή:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">147</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>294</div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div id="bx-pager"><a class="change-package-button" data-package-id="1" data-slide-index="0"
                                                  href="">1</a><a class="change-package-button" data-package-id="3"
                                                                  data-slide-index="1" href="">3</a><a
                                    class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                            </div>
                        </div>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIAJ6FgAAAAAAAAAAAASKDbgJAA"></iframe>

                            <!--<input type="hidden" name="total_price" value="49.0">
                            <input type="hidden" name="esub" value="-4A25sMQEvAnoWBCggDogAAAEAAvgVAQACUi0CBgEBAAQ0X-qMAA">
                            <input type="hidden" name="goods_id" value="217">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Eco_Slim_CY_GR_Green">
                            <input type="hidden" name="price" value="49">
                            <input type="hidden" name="old_price" value="98">
                            <input type="hidden" name="total_price_wo_shipping" value="49.0">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="€">
                            <input type="hidden" name="package_id" value="1">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="CY">
                            <input type="hidden" name="shipment_vat" value="0.0">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.24">
                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="GR">Ελλάδα</option>
                                <option value="CY">Κύπρος</option>
                            </select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 πακέτο</option>
                                <option data-slide-index="1" selected="selected" value="3">2 πακέτο + ένα δώρο</option>
                                <option data-slide-index="2" value="5">3 πακέτο + 2 δώρα</option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Εισάγετε όνομα" type="text"/>
                            <input class="only_number inp j-inp" name="phone" placeholder="Εισάγετε τηλέφωνο" type="text"/>
                            <div class="text-center totalpriceForm">Μεταφορικά: 0 €</div>
                            <div class="text-center totalpriceForm">σύνολο: <span class="js_total_price js_full_price">49 </span> €</div>
                            <div class="zbtn js_submit transitionmyl" style="cursor:pointer">Παραγγελια</div>-->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Εγγυούμαστε:</div>
                            <ul>
                                <li><b>100%</b> ποιότητα</li>
                                <li><b>Έλεγχος</b> του προϊόντος κατά την παράδοση</li>
                                <li><b>Ασφάλεια</b> των προσωπικών σας δεδομένων</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    </body>
    </html>
<?php } else { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 11602 -->
        <script>var locale = "cy";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQEvAnoWBJE9DogAAAEAAvgVAQACUi0CBgEBAATKdNbwAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript"
                src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height: 0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }

            .ac_footer a {
                color: #A12000;
            }

            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {
                "1": {"price": 49, "old_price": 98, "shipment_price": 0},
                "3": {"price": 98, "old_price": 196, "shipment_price": 0},
                "5": {"price": 147, "old_price": 294, "shipment_price": 0}
            };
            var shipment_price = 0;
            var name_hint = 'Nyiri Andrea';
            var phone_hint = '+36303408947';

            $(document).ready(function () {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cy");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet"
              media="all">


        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', '//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <meta charset="utf-8"/>
        <meta content="width=1360" name="viewport"/>
        <title>EcoSlim</title>
        <link href="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/css/style.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({scrollTop: $(".block_9 form").offset().top - 300}, 1000);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <p>

        <style>
            .form form p {
                font-size: 32px;
                margin-bottom: 50px;
            }
        </style>


    </p>

    <div class="main first-page">
        <div class="block_1">
            <div class="elements">
                <div class="container">
                    <div class="title">
                        <p>Καινοτόμα Επιστημονική <b>Ανακάλυψη</b></p>
                        <p><span class="logo"><b>Eco</b>Slim</span></p>
                    </div>
                    <ul>
                        <li>ΑΣΦΑΛΕΣ ΑΔΥΝΑΤΙΣΜΑ</li>
                        <li>ΤΟ ΒΑΡΟΣ ΜΕΙΩΝΕΤΑΙ ΚΑΤΑ 10-12 KG ΤΟΝ ΜΗΝΑ</li>
                    </ul>
                    <div class="product">
                        <div>
                            <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/ecoslim.png"/>
                            <small><span><b>2016</b>
								Νέα εξέλιξη
							</span></small>
                        </div>
                    </div>
                    <div class="form_pr">
                        <div class="form">
                            <form action="/order/create/" method="post">

                                <p>Παραγγείλετε το <span><b>Eco</b>Slim</span> από εμάς</p>
                                <button class="jsOpenWindow"><span>Παραγγελια</span></button>
                                <div class="prices_form">
                                    <span class="old_price">98 €</span>
                                    <span class="new_price"><span>49 </span> <span>€</span></span>
                                    <span class="precent">έκπτωση <span>50%</span></span>
                                </div>

                            </form>
                        </div>
                        <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/block_1_water2.png"/>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <p>Παρασκευασμένο με <b>βαση τις βιταμινες Β</b> που προκαλούν ΤΗΝ ΔΙΑΛΥΣΗ ΤΟΥ ΛΙΠΟΥΣ</p>
                </div>
            </div>
        </div>
        <div class="block_2">
            <div class="container">
                <ul>
                    <li><span>Μείον 10-12 kg τον μήνα</span></li>
                    <li><span>Δεν επηρεάζει την καρδιά και το νευρικό σύστημα</span></li>
                    <li><span>Εγγύηση ποιότητας, πλήρης περιβαλλοντική προστασία</span></li>
                    <li><span>100% φυσική σύνθεση</span></li>
                    <li><span>Επιδρά δραστικά στα αποθέματα λίπους</span></li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_3">
            <div class="container">
                <div class="woman">
                    <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/block_3_woman.png"/>
                </div>
                <img class="food" src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/block_3_food3.png"/>
                <div class="text">
                    <div class="why">
                        <h3>Γιατί παίρνουμε βάρος;</h3>
                        <p>Έχετε σίγουρα ακούσει πολλές φορές ότι τα αιτία που οδηγούν στην παχυσαρκία είναι ορμονικές
                            διαταραχές, η καθιστική ζωή και το περιβάλλον; Αυτοί ωστόσο είναι μόνο έμμεσοι
                            παράγοντες.</p>
                    </div>
                    <div class="warning">
                        <h3>Στο 90% των περιπτώσεων, το περιττό βάρος αποτελεί έμμεση συνέπεια της μη φυσικής
                            διατροφής!</h3>
                        <p>Τα προϊόντα που αγοράζετε περιέχουν τεχνητά συντηρητικά, συνθετικές γεύσεις και χρώματα. Αυτά
                            τα χημικά συστατικά προκαλούν προβλήματα κατά την πέψη, οδηγούν στην αποθήκευση λίπους και
                            στην κατακράτηση βλαβερών ουσιών από τον οργανισμό.</p>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_4">
            <div class="top">
                <div class="container">
                    <h3>το σώμα σας χρειάζεται <font>βοήθεια!</font></h3>
                    <div class="letter">
                        <p>Σχολιο ειδικου στον τομεα της διαιτολογιας</p>
                        <p>«Για να καθαρίσει το σώμα σας από μόνο του, χρησιμοποιήστε σταγόνες <b>EcoSlim</b> σε ένα
                            ποτήρι νερό και πιείτε το κατά τη διάρκεια του γεύματός σας ή μετά από αυτό, μία φορά την
                            ημέρα. Σύντομα θα αισθανθείτε τη διαφορά - μέσα σε μόλις μία εβδομάδα το βάρος σας θα
                            μειωθεί και θα εξαφανιστούν τα προβλήματα με το στομάχι και την πέψη σας. Το πιο
                            καθησυχαστικό είναι το εξής: η μοναδική σύσταση του προϊόντος σχεδιάστηκε σε συνεργασία με
                            το Ινστιτούτο Διατροφής. Τα φυσικά συμπλέγματα βιταμινών είναι πολύ πιο αποδοτικά και ασφαλή
                            από οποιοδήποτε άλλο χημικό συστατικό. Ο συνδυασμός ταυρίνης, καφεΐνης και εκχυλίσματα
                            γκουαρανά μπορεί να θεωρηθεί με ασφάλεια ένα πραγματικό δώρο για όσους αδυνατίζουν, καθώς το
                            σύμπλεγμα <b>διαλύει τα αποθέματα λίπους στις προβληματικές περιοχές μέσα σε λίγες μόλις
                                ημέρες.</b>»</p>
                        <div class="signature">Μαρία Σαράντη</div>
                    </div>
                    <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/block_4_doc.png"/>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="eko">
                        <div>
                            <h3><b>Eco</b>Slim</h3>
                            <p>100% φυσικο και οικολογικο <br/>αγνο προϊον</p>
                        </div>
                    </div>
                    <div class="woman">
                        <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/block_4_woman.png"/>
                    </div>
                    <div class="eko2">
                        <div><span>Χωρίς χημικά και συνθετικές ουσίες!</span></div>
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <ul class="list">
                <li></li>
                <li></li>
            </ul>
        </div>
        <div class="block_5">
            <div class="top">
                <div class="container">
                    <h3><font>Μοναδικά επιλεγμένη σύνθεση</font> για να επιτύχετε το μέγιστο αποτέλεσμα:</h3>
                    <ul>
                        <li><span>L-Καρνιτίνη</span></li>
                        <li><span>Καφεΐνη</span></li>
                        <li><span>Χιτοζάνη</span></li>
                        <li><span>Εκχύλισμα <br/>Ινδικός Κολεός</span></li>
                        <li><span>Εκχύλισμα <br/>γκουαρανά</span></li>
                        <li><span>Ηλεκτρικό οξύ</span></li>
                        <li><span>Εκχύλισμα <br/>αλγών Fucus</span></li>
                        <li><span>Βιταμίνες <br/>В2, В5, В6, В8, В12</span></li>
                    </ul>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <p><b>Eco</b>Slim</p>
                    <div class="approved">Σχεδιασμένο με βάση <br/>Ευρωπαϊκές τεχνολογίες</div>
                    <div class="stamp">Εγκεκριμενο με
                        <small>υγειονομικούς έλεγχους</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_6">
            <div class="container">
                <h3>Η έλλειψη μεταλλικών στοιχείων γίνεται λόγος πρόσληψης περιττού βάρους!</h3>
                <p>Ενδεχομένως να πιστεύετε ότι τα φυτά<br/>δεν είναι αρκετά ισχυρά ώστε να διαλύσουν το
                    λίπος...<br/><b>Ωστόσο επιστημονικά δεδομένα αναφέρουν το αντίθετο!</b></p>
                <ul>
                    <li>
                        <small>B2</small>
                        <span><b>Ομολοποιεί</b> την ορμονική ισορροπία στο σώμα</span></li>
                    <li>
                        <small>B6</small>
                        <span><b>Ρυθμίζει</b> τις μεταβολικές διαδικασίες, ενδυναμώνει το δέρμα, τα μαλλιά, τα νύχια</span>
                    </li>
                    <li>
                        <small>B12</small>
                        <span><b>Τονώνει τον</b> μεταβολισμό</span></li>
                    <li>
                        <small>B5</small>
                        <span><b>Συμβάλει στην</b> καύση και διάσπαση υδατανθράκων και λίπους</span></li>
                    <li>
                        <small>B8</small>
                        <span><b>Περιορίζει</b> τη χοληστερόλη, ρυθμίζει την λειτουργία του γαστρεντερικού συστήματος</span>
                    </li>
                    <li>
                        <small>Ταυρίνη</small>
                        <span><b>Ενεργοποιεί</b> μεταβολικές διαδικασίες, καίει το λίπος</span></li>
                    <li>
                        <small>Καφεΐνη</small>
                        <small>Ηλεκτρικό οξύ</small>
                        <span><b>Διώχνουν τις τοξίνες και τα περιττά υγρά,</b> καίνε το λίπος και ενεργοποιούν τη διαδικασία ανανέωσης των σωματικών κυττάρων.</span>
                    </li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_7">
            <div class="container">
                <div class="left">
                    <h3>Το μελλοντικό σου <big>αποτέλεσμα:</big></h3>
                    <p>Μόνο 1 ποτήρι με σταγόνες <b>EcoSlim</b> και η σιλουέτα σου θα αρχίσει να αλλάζει μέρα με τη
                        μέρα!</p>
                    <ul>
                        <li>έως -0,5 kg την ημερα</li>
                        <li>έως -3,5 kg την εβδομάδα</li>
                        <li> έως - 10-12 kg τον μήνα</li>
                    </ul>
                    <div class="safely">
                        <b>Ασφαλες</b> Δεν επηρεάζει το καρδιαγγειακό και νευρικό σύστημα
                    </div>
                </div>
                <div class="right">
                    <div class="img">
                        <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/block_7_woman.png"/>
                        <img class="product2" src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/ecoslim.png"/>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_8">
            <div class="container">
                <h3><font>Σχόλια</font> από τους πελάτες μας</h3>
                <ul>
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/block_8_li1.jpg"/>
<b>Μάρθα Σούλη</b>
							24 ετών
						</span>
                        <span>Μια ζωή χοντρή και αδυνατίζεις σε έναν μήνα - είναι εκπληκτικό! <b>Είμαι πολύ ικανοποιημένη με το Eco Slim.</b> Είναι γευστικό, περιορίζει την όρεξη και το βάρος στο στομάχι εξαφανίζεται. Πρόκειται για το πρώτο προϊόν με το οποίο κατάφερα να χάσω 13 κιλά! Στην αρχή το βάρος άρχισε να φεύγει αργά, στη συνέχεια άρχισα να χάνω 1 κιλό την ημέρα.</span>
                    </li>
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/block_8_li2.jpg"/>
<b>Σταύρος Βασιλακάκης</b>
							32 ετών
						</span>
                        <span>Για να είμαι ειλικρινής, αδυνάτισα για χάρη της κόρης μου, γιατί δεν μπορούσα να τρέξω και να παίξω μαζί της, μου ήταν δύσκολο ακόμη και να σκύψω. Έχασα πάνω από 10 κιλά εύκολα και στη συνέχεια ξεκίνησα το γυμναστήριο. <b>Το Eco Slim είναι ένα σπουδαίο προϊόν.</b> Εξακολουθώ να το πίνω, όταν δεν πηγαίνω γυμναστήριο ή όταν τρώω λίγο παραπάνω στις διακοπές.</span>
                    </li>
                </ul>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="left">
                        <p>Έχουμε ήδη πουλήσει <br/>πάνω από 20 χιλιάδες πακέτα του</p>
                        <p><b>Eco</b>Slim</p>
                    </div>
                    <div class="product3">
                        <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/ecoslim.png"/>
                    </div>
                    <div class="right">
                        <p><b>Βιαστείτε να το παραγγείλετε</b> σε μειωμένη τιμή <b>τώρα</b> για να αποκτήσετε τέλεια
                            σιλουέτα <b>σε λίγες εβδομάδες!</b></p>

                        <button class="jsOpenWindow"><span>Παραγγειλετε τωρα</span></button>

                    </div>
                </div>
            </div>
        </div>
        <div class="block_9">
            <div class="container">
                <div class="product">
                    <div>
                        <img src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/ecoslim.png"/>
                        <small><span><b>Το προϊον</b> είναι πιστοποιημένο</span></small>
                    </div>
                </div>
                <div class="form_pr">
                    <div class="form">
                        <form action="/order/create/" method="post">

                            <p>Παραγγείλετε το <span><b>Eco</b>Slim</span> από εμάς</p>
                            <button class="jsOpenWindow"><span>Παραγγελια</span></button>
                            <div class="prices_form">
                                <span class="old_price">98 €</span>
                                <span class="new_price"><span>49 </span> <span>€</span></span>
                                <span class="precent">έκπτωση <span>50%</span></span>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_10">
            <div class="container"></div>
        </div>
    </div>

    <link href="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/css/styleSecond.css" rel="stylesheet"/>
    <script src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/js/secondPage.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet"
          type="text/css"/>
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic"
        rel="stylesheet"/>
    <div class="hidden-window">
        <div class="second-clips-header">
            <div class="h-w__inner">
                <div class="yop-right__side">
                    <div class="dtable">
                        <div class="dtable-cell">EcoSlim - οναδική προσφορά μόνο για σήμερα!</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Πληρωμή με <b>αντικαταβολή</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>1
                                        </i>
                                        <img alt="alt1"
                                             src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/imgSecond/dtc1.png"/>
                                    </div>
                                    <div class="dtable-cell">Η παραγγελία</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>2
                                        </i>
                                        <img alt="alt1"
                                             src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/imgSecond/dtc2.png"/>
                                    </div>
                                    <div class="dtable-cell">Μεταφορικά</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>3
                                        </i>
                                        <img alt="alt1"
                                             src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/imgSecond/dtc3.png"/>
                                    </div>
                                    <div class="dtable-cell">Παράδοση και
                                        <br/>
                                        <span>πληρωμή</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Καλή
                            <span>ευκαιρία</span>
                        </div>
                        <form action="" class="js_scrollForm" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abdcn.pro/forms/?target=-4AAIJIAJ6FgAAAAAAAAAAAASKDbgJAA"></iframe>


                            <!--  <input type="hidden" name="total_price" value="49.0">
                              <input type="hidden" name="esub" value="-4A25sMQEvAnoWBJE9DogAAAEAAvgVAQACUi0CBgEBAATKdNbwAA">
                              <input type="hidden" name="goods_id" value="217">
                              <input type="hidden" name="title" value="">
                              <input type="hidden" name="ip_city" value="Toruń">
                              <input type="hidden" name="template_name" value="Eco_Slim_CY_GR_Green">
                              <input type="hidden" name="price" value="49">
                              <input type="hidden" name="old_price" value="98">
                              <input type="hidden" name="total_price_wo_shipping" value="49.0">
                              <input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
                              <input type="hidden" name="currency" value="€">
                              <input type="hidden" name="package_id" value="1">
                              <input type="hidden" name="protected" value="None">
                              <input type="hidden" name="ip_country_name" value="Poland">
                              <input type="hidden" name="country_code" value="CY">
                              <input type="hidden" name="shipment_vat" value="0.0">
                              <input type="hidden" name="ip_country" value="PL">
                              <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                              <input type="hidden" name="shipment_price" value="0">
                              <input type="hidden" name="price_vat" value="0.24">
                              <select class="select inp" id="country_code_selector" name="country_code">
                                  <option value="GR">Ελλάδα</option>
                                  <option value="CY">Κύπρος</option>
                              </select>
                              <select class="select inp select_count change-package-selector" name="count_select">
                                  <option value="1">1 πακέτο</option>
                                  <option selected="selected" value="3">2 πακέτο + ένα δώρο</option>
                                  <option value="5">3 πακέτο + 2 δώρα</option>
                              </select>
                              <input class="inp j-inp" name="name" placeholder="Εισάγετε όνομα" type="text" value=""/>
                              <input class="only_number inp j-inp" name="phone" placeholder="Εισάγετε τηλέφωνο" type="text"/>
                              <div class="text-center totalpriceForm">Μεταφορικά: 0 €</div>
                              <div class="text-center totalpriceForm">σύνολο: <span class="js_total_price js_full_price">49</span> €</div>
                              <div class="zbtn js_submit transitionmyl">Παραγγελια</div>
                        -->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Εγγυούμαστε:
                            </div>
                            <ul>
                                <li><b>100%</b> ποιότητα</li>
                                <li><b>Έλεγχος</b> του προϊόντος κατά την παράδοση</li>
                                <li><b>Ασφάλεια</b> των προσωπικών σας δεδομένων</li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Προσοχή</b>: Υπάρχει <span>50% έκπτωση</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader"><b>1</b> πακέτο</div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Τιμή:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>49 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>98
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Παλιά τιμή
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px"> Μεταφορικά:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>0 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">σύνολο:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>49
                                                </b> €
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="1">
                                </div>
                                <div class="img-wrapp">
                                    <img alt="z1"
                                         src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/imgSecond/zit1.png"
                                         width="170"/>
                                </div>
                                <div class="zHeader">
<span>ΝΕΟ
                    </span>
                                </div>
                                <text>Ταχύτατο αδυνάτισμα με 50% έκπτωση. Άμεσο αποτέλεσμα!</text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 active">
                            <div class="zstick">
                            </div>
                            <div class="zDiscount">
                                <b>Προσοχή</b>: Υπάρχει <span>50% έκπτωση</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec">
                                    <b>2
                                    </b> πακέτα
                                    <span class="dib zplus">δώρο
                    </span>
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Τιμή:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>98 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>196
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Παλιά τιμή
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px"> Μεταφορικά:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>0 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">σύνολο:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>98
                                                </b> €
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3">
                                </div>
                                <div class="img-wrapp">
                                    <div class="gift">
                                        δώρο
                                    </div>
                                    <img alt="z1"
                                         src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/imgSecond/zit2.png"
                                         width="190"/>
                                </div>
                                <div class="zHeader">
<span>ΠΡΟΗΓΜΕΝΟ
                    </span>
                                </div>
                                <text>Σταθερό αποτέλεσμα. Το πρόγραμμα με το Eco Slim θα σας βοηθήσει να ξεφορτωθείτε
                                    μία για πάντα το περιττό σας βάρος!
                                </text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount">
                                <b>Προσοχή</b>: Υπάρχει <span>50% έκπτωση</span>
                            </div>
                            <div class="item__right">
                                <div class="zHeader sec">
                                    <b>3
                                    </b> πακέτα
                                    <span class="dib zplus sec">δώρα
                    </span>
                                </div>
                                <div class="zPrices">
                                    <!--<div class="dtc ellipsDiscount">-->
                                    <!--Anwendungen<br>-->
                                    <!--<span class="js_discount_pack">1000</span> €-->
                                    <!--</div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Τιμή:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>147 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>294
                              </span>
                                                    </div>
                                                    <div class="dtable-cell text-left">
<span class="old-pr-descr">Παλιά τιμή
                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px"> Μεταφορικά:
                                            </div>
                                            <div class="dtable-cell">
                                                <b>0 €
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="zSep">
                                    </div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">σύνολο:
                                            </div>
                                            <div class="dtable-cell prtotal">
                                                <b>147
                                                </b> €
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5">
                                </div>
                                <div class="img-wrapp">
                                    <div class="gift">δώρα
                                    </div>
                                    <img alt="z1"
                                         src="//st.acstnst.com/content/Eco_Slim_CY_GR_Green/img/imgSecond/zit3.png"
                                         width="220"/>
                                </div>
                                <div class="zHeader">
<span>ΠΡΟΗΓΜΕΝΟ
                    </span>
                                </div>
                                <text>Επαγγελματική προσέγγιση. Ένα εξάμηνο πρόγραμμα με το Eco Slim σας βοηθά ακόμη και
                                    στην περίπτωση πολύ μεγάλου βάρους!
                                </text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>
    </div>


    </body>
    </html>
<?php } ?>