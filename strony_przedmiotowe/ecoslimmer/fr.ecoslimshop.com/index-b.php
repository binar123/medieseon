<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>

<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 13788 -->
    <script>var locale = "fr";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQEvAnAWBOD5DYgAAAEAAusVAQAC3DUCBgEBAATA6cCoAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
    .ac_footer {
        position: relative;
        top: 10px;
        height:0;
        text-align: center;
        margin-bottom: 70px;
        color: #A12000;
    }
    .ac_footer a {
        color: #A12000;
    }
    img[height="1"], img[width="1"] {
        display: none !important;
    }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
        var shipment_price = 6;
        var name_hint = 'Rémi Morel';
        var phone_hint = '+336314254';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
            '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
            ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("fr");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1666009176948198');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->



<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>
        EcoSlim
    </title>
<link href="https://fonts.googleapis.com/css?family=Exo+2:200,600" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=cyrillic-ext,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
<link href="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/css/main.css" rel="stylesheet"/>
<script src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/js/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/js/secondPage.js" type="text/javascript"></script>
<script src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/js/common.js"></script>
</head>
<body>
<div class="wraper s__main">
<div class="block_1">
<div class="container">
<div class="top" data-xd="text01">
<span class="logo">
<b>Eco</b>Slim</span>
<div class="description">
<span>
        Gouttes minceur
       </span>
</div>
</div>
<div class="title" data-xd="text02">
<b>
                    Votre sveltesse
                </b>
                et grâce naturelle
            </div>
<div class="price" data-skip="">
<div class="old_price">
                    98 €
                </div>
<div class="new_price">
                    49 €
                </div>
</div>
<div class="product">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/images/product_1.png"/>
<span class="discount" data-xd="text03">
<span>
        REMISE
        <b>
         -50%
        </b>
</span>
</span>
<div class="decor">
</div>
</div>
<div class="bottom">
<button class="toform" data-xd="text32">
<span>Commander</span>
</button>
</div>
<ul class="block_1__list" data-xd="text04">
<li>
<span>
<b>
         Effet liftant
        </b>
        de la peau
       </span>
</li>
<li>
<span>
<b>
         Blocage
        </b>
        des calories superflues
       </span>
</li>
<li>
<span>
        Puissant
        <b>
         regain d'énergie
        </b>
</span>
</li>
<li>
<span>
<b>
         Accélération
        </b>
        de la combustion des graisses
       </span>
</li>
</ul>
<div class="good">
<span data-xd="text05">
       Un bon début de la journée!
      </span>
</div>
</div>
</div>
<div class="block_2">
<div class="container">
<div class="description" data-xd="text06">
<b>
                    Puissance des baies
                    <span class="logo">
<b>Eco</b>Slim
       </span>
</b>
                agit localement dans les endroits avec une plus grande quantité de graisse corporelle en fractionnant la graisse viscérale
            </div>
<ul class="block_2__list">
<li data-xd="text07">
<span>
<big>
         Action ciblée
        </big>
<b>Le complexe de vitamines renforce la poitrine</b>, tout en conservant seulement 25% de tissus adipeux. Il protège contre l’affaissement prématuré et maintient la forme correcte
       </span>
</li>
<li data-xd="text08">
<span>
<big>
         Active la combustion des graisses
        </big>
<b><span class="logo"><b>Eco</b>Slim</span> améliore la micro-circulation sanguine</b> dans les endroits à problème, empêche l'absorption des acides gras
       </span>
</li>
<li data-xd="text09">
<span>
<big>Effet drainant</big><b> 85% de la circonférence de la taille est due à la présence de l’eau. Avec <span class="logo"><b>Eco</b>Slim</span></b> l’excès de fluide est évacué du corps
       </span>
</li>
<li data-xd="text10">
<span>
<big>
         Lipolyse
        </big>
<b>La lipolyse 3 fois plus rapide,</b> le fractionnement des graisses et leur acheminement vers le foie pour une élimination ultérieure
       </span>
</li>
</ul>
<div class="title" data-xd="text11">
                Produit
                <b>
                    100% BIO
                </b>
</div>
<div class="bottom">
<div class="over" data-xd="text12">
<b>
                        La formule d'
                        <span class="logo">
<b>Eco</b>Slim
        </span>
                        comprend
                    </b>
                    les baies recueillies dans les forêts écologiquement propres de la Scandinavie
                </div>
<div class="decor">
</div>
</div>
<div class="text" data-xd="text13">
                Les baies nordiques
                <b>
                    contiennent
                </b>
                le nombre
                <b>
                    record de vitamines
                </b>
                et de substances bioactives
            </div>
</div>
</div>
<div class="block_3">
<div class="container">
<div class="title" data-xd="text14">
                Composition
                <b>
                    naturelle
                </b>
</div>
<ul class="block_3__list">
<li data-xd="text15">
<big>
                        Plaquebières arctiques
                    </big>
<span>15 fois <b>plus de vitamine C</b> que  dans les oranges</span> <span>3 fois <b>plus de pro-vitamine A</b> que dans les carottes</span>
</li>
<li data-xd="text16">
<big>
                        Myrtilles sauvages
                    </big>
<span>contiennent <b>un nombre record de polyphénols</b> qui compensent les effets d'un régime alimentaire gras</span>
</li>
<li data-xd="text17">
<big>
                        Baies podophylles
                    </big>
<span><b>accélèrent</b> le métabolisme</span> <span><b>fractionnent</b> les graisses dans l'intestin</span> <span><b>diminuent</b> le niveau de cholestérol</span>
</li>
<li data-xd="text18">
<big>
                        Canneberges Erntekrone
                    </big>
<span>ces canneberge suédoises contiennent de la synéphrine <b>qui bloque la faim</b> après avoir consommé suffisamment de calories</span>
</li>
</ul>
</div>
</div>
<div class="block_4">
<div class="container">
<div class="title" data-xd="text19">
                Quelle est la particularité de la minceur
                <b>
                    avec les baies
                </b>
<span class="logo">
<b>Eco</b>Slim
      </span>
</div>
<div class="text" data-xd="text20">
<b>
                    Le secret est dans la combinaison de baies rares
                </b>
                qui poussent dans les latitudes arctiques et dans la concentration des substances, qui ont été choisies par les nutritionnistes à l’issue de deux ans de recherche.
            </div>
<div class="label" data-xd="text21">
<div class="formula">
<big>
                        1
                    </big>
<span class="logo">
<b>Eco</b>Slim
       </span>
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/images/product_2.png"/>
<small>
                        =
                    </small>
</div>
<div class="result">
<span>
<b>
         3 kg
        </b>
        fruits des bois frais
       </span>
</div>
</div>
</div>
</div>
<div class="block_5">
<div class="container">
<div class="text" data-xd="text22">
<span>
<b>
        Toutes les baies sont récoltées à la main
       </b>
       et en petites quantités afin d'éviter tout dommage pendant le transport.
      </span>
</div>
</div>
</div>
<div class="block_7">
<div class="container">
<div class="title" data-xd="text23">
                Effet
                <b>
                    global
                </b>
                des phyto-substances
            </div>
<ul class="block_7__list">
<li>
<span>
</span>
<big data-xd="text24">
<b>
                            La vitamine C
                        </b>
                        accélère le métabolisme
                    </big>
</li>
<li>
<span>
</span>
<big data-xd="text25">
<b>
                            Les flavonoïdes
                        </b>
                        diminuent l'appétit
                    </big>
</li>
<li>
<span>
</span>
<big data-xd="text26">
<b>
                            Les bio-flavonoïdes
                        </b>
                        renforcent l’organisme
                    </big>
</li>
<li>
<span>
</span>
<big data-xd="text27">
<b>
                            La riboflavine
                        </b>
                        détoxifie
                    </big>
</li>
</ul>
</div>
</div>
<div class="block_8">
<div class="container">
<div class="title" data-xd="text28">
                Le phyto-cocktail
                <b>
                    détoxifiant
                </b>
                de tous les jours
            </div>
<div class="title" data-xd="text29">
<b>
                    Le secret est
                </b>
                dans sont action bien précise
            </div>
<div class="center">
<div class="honors">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/images/honors1.png"/>
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/images/honors2.png"/>
</div>
<img class="product" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/images/product_1.png"/>
</div>
<div class="text" data-xd="text30">
                Le jus de baies avec du bicarbonate de sodium
                <b>
                    lance le processus alcalinisant naturel</b>.
                <b>
                    EcoSlim est enrichi avec des acides de fruits naturels
                </b>
                et par conséquent il n’est pas nocif pour l'estomac.
            </div>
<div class="text2" data-xd="text31">
<b>
                    Les substances bioactives
                </b>
                enrichissent l'eau avec de l'oxygène, en transformant le fluide ordinaire en
                <b>
                    phyto-cocktail amincissant
                </b>
</div>
<div class="btn">
<button class="toform" data-xd="text32">
<span>Commander</span>
</button>
</div>
</div>
</div>
<div class="block_10">
<div class="container">
<div class="left">
<div class="title" data-xd="text33">
<b>
                        L'approche scientifique
                    </b>
                    vers la perte de poids
                </div>
<ul class="block_10__list" data-xd="text34">
<li>
<b>
                            La préparation du corps:
                        </b>
                        la purification des toxines et des impuretés, l’enrichissement en vitamines et oligo-éléments
                    </li>
<li>
<b>
                            La perte de poids active
                        </b>
                        grâce aux réserves d'énergie et l’action
                        <b>
                            ciblée sur les dépôts de graisse
                        </b>
</li>
<li>
<b>
                            Le résultat pérenne
                        </b>
                        la sveltesse est maintenue dans le temps
                    </li>
</ul>
</div>
</div>
</div>
<div class="block_11">
<div class="container">
<div class="top">
<div class="author" data-xd="text35">
<span>
<b>
         Valérie Hidalgo,
        </b>
        Chercheur en chef à l'Institut de biologie et de médecine:
       </span>
</div>
</div>
<div class="bottom">
<span class="logo">
<b>Eco</b>Slim
      </span>
<small data-xd="text36">
                    est un trésor en terme de nutriments
                </small>
<div class="text" data-xd="text37">
                    Il transforme le corps dans les plus brefs délais. Il est vendu sous forme de teinture, ce qui le rend plus efficace comparé à des cachets et des crèmes. La formule que nous avons créée nous permet de dissoudre les toxines et les graisses du corps et de les éliminer en toute sécurité tout en préservant l'équilibre hydro-électrolytique.
                </div>
</div>
</div>
</div>
<div class="block_12">
<div class="container">
<div class="title" data-xd="text38">
                Rapide et
                <b>
                    efficace
                </b>
</div>
<div class="topic" data-xd="text39">
                Le mode d’emploi:
            </div>
<ul class="block_12__list" data-xd="text40">
<li>
<span>
        Ajoutez
        <b>
         quelques gouttes d’EcoSlim
        </b>
        dans 250 ml d'eau pure
       </span>
</li>
<li>
<span>
        Prenez
        <b>
         1 fois par jour
        </b>
        au petit déjeuner
       </span>
</li>
</ul>
<div class="bottom">
<div class="text" data-xd="text41">
                    Pour de meilleurs résultats, continuez la cure d’
                    <span class="logo">
<b>Eco</b>Slim
       </span>
                    durant 28 jours
                </div>
</div>
</div>
</div>
<div class="block_13">
<div class="container">
<div class="title" data-xd="text42">
<b>
                    Les avis des acheteurs au sujet d’
                </b>
                EcoSlim<b>.
            </b>
</div>
<ul class="block_13__list">
<li class="l1">
<span class="woman" data-xd="text43">
<label>
<b>
          -10 kg
         </b>
         en 27 jours
        </label>
<small>
<b>
          Marianne, Lille
         </b>
         Le mode de vie sédentaire et une mauvaise alimentation ont fait que j’ai pris du poids. Ma vie sentimentale est partie en vrille.
         <b>
          J’ai décidé de  faire un régime.
         </b>
         J'ai eu de la chance de ne pas avoir à choisir parmi des tas de produits minceur car j’ai immédiatement essayé
         <b>
          EcoSlim</b>.
         <b>
          Je ne suis pas seulement devenue svelte, mais j’ai aussi retrouvé mon sourire et la joie de vivre!
         </b>
</small>
<big>
</big>
</span>
</li>
<li class="l2">
<span class="man" data-xd="text44">
<label>
<b>-16kg entre nous</b> deux en 14 jours
        </label>
<small>
<b>
          Pascal et Claire, Blois
         </b>
         Nous nous sommes mis d’accord avec mon copain de se remettre en forme pour le mariage, histoire de faire de belles photos. Un copain bodybuilder nous a conseillé EcoSlim.
         <b>
          Le résultat était spectaculaire. Au moment du mariage, tout le monde nous a dit que nous sommes  un très beau couple!
         </b>
</small>
<big>
</big>
</span>
</li>
<li class="l3">
<span class="man" data-xd="text45">
<label>
<b>
          -9 kg
         </b>
         en 29 jours
        </label>
<small>
<b>
          Antoine, Brest
         </b>
         La bière, le canapé et le frigo étaient mes meilleurs amis pendant les  trois dernières années. J’ai essayé plusieurs fois de commencer la gym, mais cela n’a jamais duré, je n’ai jamais perdu ne serait-ce que 3 kilos. La dernière fois que j’y suis  allé, j’ai remarqué que tout le monde en salle prenaient EcoSlim.
         <b>
          Il donne la pêche et aide à perdre du poids. Regardez le résultat par vous-mêmes!
         </b>
</small>
<big>
</big>
</span>
</li>
<li class="l4">
<span class="woman" data-xd="text46">
<label>
<b>
          -12 kg
         </b>
         en 35 jours
        </label>
<small>
<b>
          Aline, Toulouse
         </b>
         J’ai toujours été ronde, même à l'école. J'ai essayé de faire du sport, mais je ne pouvais pas résister aux bonbons, donc cela a donne zéro résultat.
         <b>
          Ma vie a changé seulement grâce à EcoSlim. Les kilos partaient vite.
         </b>
         J'ai perdu 7 kilos en 29 jours. Malgré mon amour de sucreries, je perds toujours du poids!
        </small>
<big>
</big>
</span>
</li>
</ul>
</div>
</div>
<div class="block_14">
<div class="container">
<div class="top">
<span class="logo">
<b>Eco</b>Slim
      </span>
<div class="description" data-xd="text47">
<span>
        votre formule minceur
       </span>
</div>
<div class="price" data-skip="">
<div class="old_price">
                        98 €
                    </div>
<div class="new_price">
                        49 €
                    </div>
</div>
<div class="product">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/images/product_1.png"/>
<span class="discount" data-xd="text03">
<span>
         REMISE
         <b>
          -50%
         </b>
</span>
</span>
<div class="decor">
</div>
</div>
</div>
<div class="form">
<span data-xd="text48">
<p>
        Formulaire de commande
       </p>
<!-- -->
<div class="s__of">
<button class="toform">
<span>Commander</span>
</button>
</div>
<!---->
</span>
</div>
</div>
</div>
</div>
<!--  -->
<link href="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/css/styleSecond.css" rel="stylesheet"/>
<div class="hidden-window">
<div class="second-clips-header">
<div class="containerz">
<div class="h-w__inner">
<div class="yop-right__side">
<div class="dtable">
<div class="dtable-cell">EcoSlim
                        </div>
</div>
</div>
</div>
</div>
</div>
<div class="containerz">
<section class="h-w__inner">
<div class="cf">
<div class="w-h__left">
<div class="w-h_vis_mob">
<select class="select inp select_count change-package-selector" name="count_select">
<option data-slide-index="0" value="1">1 paquet
                            </option>
<option data-slide-index="1" selected="selected" value="3">2+1 paquets
                            </option>
<option data-slide-index="2" value="5">3+2 paquets
                            </option>
</select>
</div>
<ul class="bx-bx">
<li class="item transitionmyl1" data-value="1">
<div class="item__left">
<div class="js_changer change-package-button" data-package-id="1">
</div>
<div class="img-wrapp">
<img alt="z1" height="210" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/imagesSec/zit1.png"/>
</div>
<div class="zHeader">
<span>
</span>
</div>
<div class="pack_descr">EcoSlim méthode minceur №1. Offre unique valable qu'aujourd'hui. Minceur expresse à moins 50%! Effet immédiat! 1 paquet de EcoSlim au prix nouveau!</div>
</div>
<div class="item__right fsti">
<div class="zHeader">
<b>1
                                    </b> paquet
                                </div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prix:
                                            </div>
<div class="dtable-cell nowrap">
<b>
<text class="js-pp-new">49
                                                    </text> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">98
                                  </text>
</span>
</div>
<div class="dtable-cell text-left">
<span class="old-pr-descr">Ancien prix
                                </span>
</div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Livraison:
                                            </div>
<div class="dtable-cell">
<b>
<text class="js-pp-ship">6
                                                    </text> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
<div class="zSep">
</div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Total:
                                            </div>
<div class="dtable-cell prtotal">
<b>
<text class="js-pp-full">55
                                                    </text> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
</div>
</div>
<div class="zDiscount">
<b>Attention</b>: <span>Rabais de 50%</span>
</div>
</li>
<li class="item hot transitionmyl1 active" data-value="3">
<div class="zstick">
</div>
<div class="item__left">
<div class="js_changer change-package-button active" data-package-id="3">
</div>
<div class="img-wrapp">
<img alt="z1" height="210" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/imagesSec/zit2.png"/>
</div>
<div class="zHeader">
<span>
</span>
</div>
<div class="pack_descr">Le résultat Stable! Une cure de EcoSlim vous aidera à éliminer les kilos en trop pour toujours! 3 paquets de EcoSlim pour le prix de 2!</div>
</div>
<div class="item__right">
<div class="zHeader sec">
<b>2
                                    </b> paquets
                                    <span class="dib zplus">paquet
                      </span>
</div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prix:
                                            </div>
<div class="dtable-cell nowrap">
<b>
<text class="js-pp-new">98
                                                    </text> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">196
                                  </text>
</span>
</div>
<div class="dtable-cell text-left">
<span class="old-pr-descr">Ancien prix
                                </span>
</div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Livraison:
                                            </div>
<div class="dtable-cell">
<b>
<text class="js-pp-ship">6
                                                    </text> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
<div class="zSep">
</div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Total:
                                            </div>
<div class="dtable-cell prtotal">
<b>
<text class="js-pp-full">104
                                                    </text> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
</div>
</div>
<div class="zDiscount">
<b>Attention</b>: <span>Rabais de 50%</span>
</div>
</li>
<li class="item transitionmyl1" data-value="5">
<div class="item__left">
<div class="js_changer change-package-button" data-package-id="5">
</div>
<div class="img-wrapp">
<img alt="z1" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/imagesSec/zit3.png" width="210"/>
</div>
<div class="zHeader">
<span>
</span>
</div>
<div class="pack_descr">L'approche professionnelle! Une cure de 6 mois de EcoSlim efficace même pour un surpoids très important! Une véritable transformation! Résultat garanti. 5 paquets de EcoSlim pour le prix de 3!</div>
</div>
<div class="item__right">
<div class="zHeader sec">
<b>3
                                    </b> paquets
                                    <span class="dib zplus sec">paquets
                      </span>
</div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prix:
                                            </div>
<div class="dtable-cell nowrap">
<b>
<text class="js-pp-new">147
                                                    </text> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">294
                                  </text>
</span>
</div>
<div class="dtable-cell text-left">
<span class="old-pr-descr">Ancien prix
                                </span>
</div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Livraison:
                                            </div>
<div class="dtable-cell">
<b>
<span>
<text class="js-pp-ship">6
                                </text>
</span> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
<div class="zSep">
</div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Total:
                                            </div>
<div class="dtable-cell prtotal">
<b>
<text class="js-pp-full">153
                                                    </text> €
                                                </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
</div>
</div>
<div class="zDiscount">
<b>Attention</b>: <span>Rabais de 50%</span>
</div>
</li>
</ul>
</div>
<div class="w-h__right">
<div class="w-h__right-row1">
<div class="zHeader">Paiement à la livraison</div>
<div class="zContent">
<div class="dtable">
<div class="dtable-cell text-right">
<i>1
                                    </i>
<img alt="alt1" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/imagesSec/dtc1.png"/>
</div>
<div class="dtable-cell">La commande</div>
</div>
<div class="dtable">
<div class="dtable-cell text-right">
<i>2
                                    </i>
<img alt="alt1" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/imagesSec/dtc2.png"/>
</div>
<div class="dtable-cell">Livraison</div>
</div>
<div class="dtable">
<div class="dtable-cell text-right">
<i>3
                                    </i>
<img alt="alt1" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/mobile/imagesSec/dtc3.png"/>
</div>
<div class="dtable-cell">Livraison
                                    <span>et paiement</span>
</div>
</div>
</div>
</div>
<div class="printbg">Offre avantageuse</div>
<form action="" class="js_scrollForm" method="post"><input type="hidden" name="total_price" value="55.0">


                        <div class="formHeader">Entrez vos données pour passer commande</div>
<div class="clearfix">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAJwFgAAAAAAAAAAAASH8XRgAA"></iframe>
</div>
<div class="text-center totalpriceForm">Livraison:
                            <span class="">
</span>6 €
                        </div>
<div class="text-center totalpriceForm">Total:
                            <span class="js_total_price js_full_price"> 55
                  </span> €
                        </div>
</form>
<div class="zGarant">
<div class="zHeader">Nous garantissons
                        </div>
<ul>
<li><b>100%</b> qualité</li>
<li><b>Vérification</b> du produit à la réception</li>
<li><b>Sécurité</b> de vos données</li>
</ul>
</div>
</div>
</div>
</section>
</div>
</div>
<!--  -->
</body>
</html>
<?php } else { ?>

<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 13788 -->
    <script>var locale = "fr";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQEvAnAWBCOODYgAAAEAAusVAQAC3DUCBgEBAATxZMoBAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
    .ac_footer {
        position: relative;
        top: 10px;
        height:0;
        text-align: center;
        margin-bottom: 70px;
        color: #A12000;
    }
    .ac_footer a {
        color: #A12000;
    }
    img[height="1"], img[width="1"] {
        display: none !important;
    }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
        var shipment_price = 6;
        var name_hint = 'Rémi Morel';
        var phone_hint = '+336314254';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
            '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
            ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("fr");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1666009176948198');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->



<meta charset="utf-8"/>
<meta content="width=1024, initial-scale=1, user-scalable=yes" name="viewport"/>
<title>EcoSlim</title>
<link href="https://fonts.googleapis.com/css?family=Exo+2:200,600" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Roboto:300,700&amp;subset=cyrillic-ext,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
<link href="//st.acstnst.com/content/EcoSlim_FR_Green_sp/css/main.css" rel="stylesheet"/>
<script src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/js/jquery.bxslider.min.js"></script>
<script src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/js/secondPage.js"></script>
</head>
<body>
<div class="wraper s__main">
<div class="block_1">
<div class="container">
<div class="top" data-xd="text01">
<span class="logo">
<b>Eco</b>Slim
      </span>
<div class="description">
<span>
        Gouttes minceur
       </span>
</div>
</div>
<div class="title" data-xd="text02">
<b>
       Votre sveltesse
      </b>
      et grâce naturelle
     </div>
<ul class="block_1__list" data-xd="text04">
<li>
<span>
<b>
         Effet liftant
        </b>
        de la peau
       </span>
</li>
<li>
<span>
<b>
         Blocage
        </b>
        des calories superflues
       </span>
</li>
<li>
<span>
        Puissant
        <b>
         regain d'énergie
        </b>
</span>
</li>
<li>
<span>
<b>
         Accélération
        </b>
        de la combustion des graisses
       </span>
</li>
</ul>
<div class="good">
<div class="price" data-skip="">
<div class="old_price">
        98 €
       </div>
<div class="new_price">
        49 €
       </div>
</div>
<span data-xd="text05">
       Un bon début de la journée!
      </span>
</div>
<div class="bottom">
<div class="product">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/product_1.png"/>
<span class="discount" data-xd="text03">
<span>
         REMISE
         <b>
          -50%
         </b>
</span>
</span>
<div class="decor">
</div>
</div>
<button class="toform">
<span>
        Commander maintenant
       </span>
</button>
</div>
</div>
</div>
<div class="block_2">
<div class="container">
<div class="title">
      L'approche globale
      <b>
       pour la perte de poids
      </b>
</div>
<div class="description" data-xd="text06">
<b>
       Puissance des baies
       <span class="logo">
<b>Eco</b>Slim
       </span>
</b>
      agit localement dans les endroits avec une plus grande quantité de graisse corporelle en fractionnant la graisse viscérale
     </div>
<ul class="block_2__list">
<li>
<div class="line_block" data-xd="text07">
<span>
<big>
          Action ciblée
         </big>
<b>
          Le complexe de vitamines renforce la poitrine
         </b>, tout en conservant seulement 25% de tissus adipeux. Il protège contre l’affaissement prématuré et maintient la forme correcte
        </span>
</div>
<div class="line_block" data-xd="text08">
<span>
<big>
          Active la combustion des graisses
         </big>
<b>
<span class="logo">
<b>Eco</b>Slim
          </span>
          améliore la micro-circulation sanguine
         </b>
         dans les endroits à problème, empêche l'absorption des acides gras
        </span>
</div>
</li>
<li>
<div class="line_block" data-xd="text09">
<span>
<big>
          Effet drainant
         </big>
<b>
          85% de la circonférence de la taille est due à la présence de l’eau. Avec
          <span class="logo">
<b>Eco</b>Slim
          </span>
</b>
         l’excès de fluide est évacué du corps
        </span>
</div>
<div class="line_block" data-xd="text10">
<span>
<big>
          Lipolyse
         </big>
<b>
          La lipolyse 3 fois plus rapide,
         </b>
         le fractionnement des graisses et leur acheminement vers le foie pour une élimination ultérieure
        </span>
</div>
</li>
</ul>
<div class="bottom">
<div class="over">
<div class="left" data-xd="text12">
<b>
         La formule d'
         <span class="logo">
<b>Eco</b>Slim
         </span>
         comprend
        </b>
        les baies recueillies dans les forêts écologiquement propres de la Scandinavie
       </div>
<div class="right" data-xd="text11">
        Produit
        <b>
         100% BIO
        </b>
</div>
</div>
<div class="decor">
</div>
</div>
<div class="text" data-xd="text13">
      Les baies nordiques
      <b>
       contiennent
      </b>
      le nombre
      <b>
       record de vitamines
      </b>
      et de substances bioactives
     </div>
</div>
</div>
<div class="block_3">
<div class="container">
<div class="product">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/product_2.png"/>
</div>
<div class="title" data-xd="text14">
      Composition <b>naturelle</b>
</div>
<ul class="block_3__list">
<li>
<div class="line_block" data-xd="text16">
<big>
         Myrtilles sauvages
        </big>
<span>
         contiennent
         <b>
          un nombre record de polyphénols
         </b>
         qui compensent les effets d'un régime alimentaire gras
        </span>
</div>
<div class="line_block" data-xd="text18">
<big>
         Canneberges Erntekrone
        </big>
<span>
         ces canneberge suédoises contiennent de la synéphrine
         <b>
          qui bloque la faim
         </b>
         après avoir consommé suffisamment de calories
        </span>
</div>
</li>
<li>
<div class="line_block" data-xd="text15">
<big>Plaquebières arctiques</big> <span>15 fois <b>plus de vitamine C</b> que  dans les oranges</span> <span>3 fois <b>plus de pro-vitamine A</b> que dans les carottes</span>
</div>
<div class="line_block" data-xd="text17">
<big>Baies podophylles</big> <span><b>accélèrent</b> le métabolisme</span> <span><b>fractionnent</b> les graisses dans l'intestin</span> <span><b>diminuent</b>le niveau de cholestérol</span>
</div>
</li>
</ul>
</div>
</div>
<div class="block_4">
<div class="container">
<div class="title" data-xd="text19">
      Quelle est la particularité de la minceur <b>avec les baies</b>
<span class="logo">
<b>Eco</b>Slim
      </span>
</div>
<div class="right">
<div class="text" data-xd="text20">
<b>
        Le secret est dans la combinaison de baies rares
       </b>
       qui poussent dans les latitudes arctiques et dans la concentration des substances, qui ont été choisies par les nutritionnistes à l’issue de deux ans de recherche.
      </div>
<ul>
<li>
<span>
         Restauration
         <b>
          complète
         </b>
         du métabolisme acido-basique
        </span>
</li>
<li>
<span>
         Contient
         <b>
          86.3%
         </b>
         de la norme journalière de vitamines et d'antioxydants
        </span>
</li>
</ul>
<div class="text">
       Chaque paquet d'
       <span class="logo">
<b>Eco</b>Slim
       </span>
       contient
       <b>
        les extraits riches en vitamines et
       </b>
       microéléments équivalents à 3 kg de baies
      </div>
</div>
<div class="label" data-xd="text21">
<div class="formula">
<big>
        1
       </big>
<span class="logo">
<b>Eco</b>Slim
       </span>
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/product_2.png"/>
<small>
        =
       </small>
</div>
<div class="result">
<span>
<b>
         3 kg
        </b>
        fruits des bois frais
       </span>
</div>
</div>
</div>
</div>
<div class="block_5">
<div class="container">
<div class="left">
<div class="text">
       Le substrat utilisé  pour la fabrication d'
       <div class="shaddow">
<span class="logo">
<b>Eco</b>Slim
        </span>
</div>
<b>
        est complètement libre d’anthocyanes
       </b>
       qui sont des allergènes naturels. Il est donc
       <b>
        parfaitement inoffensif
       </b>
       pour le corps
      </div>
<div class="stamp">
<span>
<b>
         Cliniquement prouvé
        </b>
        Il ne provoque pas de réactions allergiques
       </span>
</div>
</div>
<ul class="block_5__list">
<li data-xd="text22">
<span>
<b>
         Toutes les baies sont récoltées à la main
        </b>
        et en petites quantités afin d'éviter tout dommage pendant le transport.
       </span>
</li>
<li>
<span>
        La matière première est soigneusement
        <b>
         livrée au laboratoire dans des paniers en osier
        </b>
        suite à quoi le jus est extrait des baies d’une manière spéciale.
       </span>
</li>
</ul>
</div>
</div>
<div class="block_6">
<div class="container">
<div class="left">
<div class="title">
       Pourquoi
       <b>
        le processus
       </b>
       de collecte
       <b>
        des baies
       </b>
       est-il si
       <b>
        important?
       </b>
</div>
</div>
<div class="right">
<div class="text">
       Pour que
       <b>
        les baies
       </b>
       puissent avoir un effet sur l'organisme,
       <b>
        elles doivent être traitées
       </b>
       d'une manière spéciale.
       <b>
        C'est un gage
       </b>
       de l’efficacité de la préparation
       <b>
        en terme
       </b>
       d’élimination de surpoids.
      </div>
<div class="shaddow">
</div>
</div>
</div>
<div class="container">
<div class="bottom">
<span>
<span class="logo">
<b>Eco</b>Slim
       </span>
       est le produit unique
       <b>
        à base de fruits naturels
       </b>
       ce qui garantit que tous les
       <b>
        vitamines et les enzymes qu’il contient
       </b>
       seront mis à disposition de l’organisme.
      </span>
</div>
</div>
</div>
<div class="block_7">
<div class="container">
<div class="title" data-xd="text23">
      Effet
      <b>
       global
      </b>
      des phyto-substances
     </div>
<ul class="block_7__list">
<li>
<span>
</span>
<big data-xd="text24">
<b>
         La vitamine C
        </b>
        accélère le métabolisme
       </big>
<small>
        La vitamine C est une charge énergétique naturelle qui ajuste le fonctionnement  de tous les organes internes,
        <b>
         il accélère le processus de perte de poids jusqu'à 60%</b>. Le corps se libère facilement des graisses mises en réserve.
       </small>
</li>
<li>
<span>
</span>
<big data-xd="text25">
<b>
         Les flavonoïdes
        </b>
        diminuent l'appétit
       </big>
<small>
        Les flavonoïdes ont un impact sur les "hormones de la faim» dans le corps,
        <b>
         en diminuant l'appétit</b>. Les fibres normalisent le transit intestinal, abaisse le niveau de cholestérol et
        <b>
         protègent l'estomac
        </b>
        de la formation des cellules cancéreuses.
       </small>
</li>
<li>
<span>
</span>
<big data-xd="text27">
<b>
         La riboflavine
        </b>
        détoxifie
       </big>
<small>
        Les molécules des composés de riboflavine
        <b>
         détruisent la matrice des substances toxiques
        </b>
        Elles se lient aux sels des métaux lourds et éliminent les toxines par la voie naturelle.
       </small>
</li>
<li>
<span>
</span>
<big data-xd="text26">
<b>
         Les bio-flavonoïdes
        </b>
        renforcent l’organisme
       </big>
<small>
        Ils normalisent l'élasticité des parois des vaisseaux sanguins,
        <b>
         et éliminent les infections fongiques</b>. Ces composés précipitent les protéines et fondent activement les graisses.
       </small>
</li>
</ul>
</div>
</div>
<div class="block_8">
<div class="container">
<div class="left">
<div class="title" data-xd="text28">
       Le phyto-cocktail
       <b>
        détoxifiant
       </b>
       de tous les jours
      </div>
<div class="honors">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/honors1.png"/>
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/honors2.png"/>
</div>
</div>
<div class="right">
<div class="text" data-xd="text30">
       Le jus de baies avec du bicarbonate de sodium
       <b>
        lance le processus alcalinisant naturel</b>.
       <b>
        EcoSlim est enrichi avec des acides de fruits naturels
       </b>
       et par conséquent il n’est pas nocif pour l'estomac.
      </div>
<div class="text2" data-xd="text31">
<b>
        Les substances bioactives
       </b>
       enrichissent l'eau avec de l'oxygène, en transformant le fluide ordinaire en
       <b>
        phyto-cocktail amincissant
       </b>
</div>
</div>
</div>
<div class="container">
<div class="bottom">
<div class="title" data-xd="text29">
<b>
        Le secret est
       </b>
       dans sont action bien précise
      </div>
<div class="product">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/product_1.png"/>
</div>
<div class="btn">
<button class="toform" data-xd="text32">
<span>
         Commander maintenant
        </span>
</button>
</div>
</div>
</div>
</div>
<div class="block_9">
<div class="container">
<div class="left">
<div class="label">
<span>
<b>
         Perte de poids
        </b>
        révolutionnaire
       </span>
</div>
<div class="label">
<span>
<b>
         Perte de poids
        </b>
        révolutionnaire
       </span>
</div>
<div class="text">
<span>
<b>
         Vitalité maximale,
        </b>
        excellent état de santé et perte de poids rapide
       </span>
</div>
</div>
<div class="right">
<ul class="block_9__list">
<li>
<span>
<big>
<small>
<b>
            Les besoins
           </b>
           du corps en
           <b>
            calories est réduit de 67%
           </b>
</small>
</big>
</span>
</li>
<li>
<span>
<big>
<small>
<b>
            Le taux de cholestérol descend
           </b>
           de 3,3 mmol / l à 2,4 mmol / l
          </small>
</big>
</span>
</li>
<li>
<span>
<big>
<small>
<b>
            La disparition de la cellulite
           </b>
           en moyenne 15 jours après le début de la prise  d'EcoSlim
          </small>
</big>
</span>
</li>
<li>
<span>
<big>
<small>
           Les excès
           <b>
            de graisse brûlent
           </b>
           89% plus rapidement
          </small>
</big>
</span>
</li>
</ul>
</div>
</div>
</div>
<div class="block_10">
<div class="container">
<div class="left">
<div class="title" data-xd="text33">
<b>
        L'approche scientifique
       </b>
       vers la perte de poids
      </div>
<ul class="block_10__list" data-xd="text34">
<li>
<b>
         La préparation du corps:
        </b>
        la purification des toxines et des impuretés, l’enrichissement en vitamines et oligo-éléments
       </li>
<li>
<b>
         La perte de poids active
        </b>
        grâce aux réserves d'énergie et l’action
        <b>
         ciblée sur les dépôts de graisse
        </b>
</li>
<li>
<b>
         Le résultat pérenne
        </b>
        la sveltesse est maintenue dans le temps
       </li>
</ul>
</div>
<div class="text">
<b>
       Le corps fonctionne correctement
      </b>
      et brûle les calories excessives, même après un dîner de fête
     </div>
<div class="left">
<span class="logo">
<b>Eco</b>Slim
      </span>
</div>
</div>
</div>
<div class="block_11">
<div class="container">
<div class="left">
<div class="author" data-xd="text35">
<span>
<b>
         Valérie Hidalgo,
        </b>
        Chercheur en chef à l'Institut de biologie et de médecine:
       </span>
</div>
</div>
<div class="right">
<div class="top">
<span class="logo">
<b>Eco</b>Slim
       </span>
<small data-xd="text36">
        est un trésor en terme de nutriments
       </small>
</div>
<div class="text" data-xd="text37">
       Il transforme le corps dans les plus brefs délais. Il est vendu sous forme de teinture, ce qui le rend plus efficace comparé à des cachets et des crèmes. La formule que nous avons créée nous permet de dissoudre les toxines et les graisses du corps et de les éliminer en toute sécurité tout en préservant l'équilibre hydro-électrolytique.
      </div>
</div>
</div>
<div class="container">
<div class="bottom">
<span>
<b>
        La formule
       </b>
       antioxydants + vitamines + minéraux, ce qui garantit une perte de poids et l'amélioration de l’état de santé
      </span>
</div>
</div>
</div>
<div class="block_12">
<div class="container">
<div class="left">
<div class="title" data-xd="text38">
       Rapide et
       <b>
        efficace
       </b>
</div>
</div>
<div class="bottom">
<div class="tr">
<div class="topic" data-xd="text39">
        Le mode d’emploi:
       </div>
<ul class="block_12__list" data-xd="text40">
<li>
<span>
          Ajoutez
          <b>
           quelques gouttes d’EcoSlim
          </b>
          dans 250 ml d'eau pure
         </span>
</li>
<li>
<span>
          Prenez
          <b>
           1 fois par jour
          </b>
          au petit déjeuner
         </span>
</li>
</ul>
<div class="text" data-xd="text41">
        Pour de meilleurs résultats, continuez la cure d’
        <span class="logo">
<b>Eco</b>Slim
        </span>
        durant 28 jours
       </div>
</div>
</div>
</div>
</div>
<div class="block_13">
<div class="container">
<div class="title" data-xd="text42">
<b>
       Les avis des acheteurs au sujet d’
      </b> EcoSlim.
     </div>
<ul class="block_13__list">
<li>
<span class="woman" data-xd="text43">
<label>
<b>
          -10 kg
         </b>
         en 27 jours
        </label>
<small>
<b>
          Marianne, Lille
         </b>
         Le mode de vie sédentaire et une mauvaise alimentation ont fait que j’ai pris du poids. Ma vie sentimentale est partie en vrille.
         <b>
          J’ai décidé de  faire un régime.
         </b>
         J'ai eu de la chance de ne pas avoir à choisir parmi des tas de produits minceur car j’ai immédiatement essayé
         <b>
          EcoSlim</b>.
         <b>
          Je ne suis pas seulement devenue svelte, mais j’ai aussi retrouvé mon sourire et la joie de vivre!
         </b>
</small>
<big>
</big>
</span>
<span class="indent">
</span>
<span class="man" data-xd="text44">
<label>
<b>
          -16kg  entre nous deux
         </b>
         en 14 jours
        </label>
<small>
<b>
          Pascal et Claire, Blois
         </b>
         Nous nous sommes mis d’accord avec mon copain de se remettre en forme pour le mariage, histoire de faire de belles photos. Un copain bodybuilder nous a conseillé EcoSlim.
         <b>
          Le résultat était spectaculaire. Au moment du mariage, tout le monde nous a dit que nous sommes  un très beau couple!
         </b>
</small>
<big>
</big>
</span>
</li>
<li class="indent">
</li>
<li>
<span class="man" data-xd="text45">
<label>
<b>
          -9 kg
         </b>
         en 29 jours
        </label>
<small>
<b>
          Antoine, Brest
         </b>
         La bière, le canapé et le frigo étaient mes meilleurs amis pendant les  trois dernières années. J’ai essayé plusieurs fois de commencer la gym, mais cela n’a jamais duré, je n’ai jamais perdu ne serait-ce que 3 kilos. La dernière fois que j’y suis  allé, j’ai remarqué que tout le monde en salle prenaient EcoSlim.
         <b>
          Il donne la pêche et aide à perdre du poids. Regardez le résultat par vous-mêmes!
         </b>
</small>
<big>
</big>
</span>
<span class="indent">
</span>
<span class="woman" data-xd="text46">
<label>
<b>
          -12 kg
         </b>
         en 35 jours
        </label>
<small>
<b>
          Aline, Toulouse
         </b>
         J’ai toujours été ronde, même à l'école. J'ai essayé de faire du sport, mais je ne pouvais pas résister aux bonbons, donc cela a donne zéro résultat.
         <b>
          Ma vie a changé seulement grâce à EcoSlim. Les kilos partaient vite.
         </b>
         J'ai perdu 7 kilos en 29 jours. Malgré mon amour de sucreries, je perds toujours du poids!
        </small>
<big>
</big>
</span>
</li>
</ul>
</div>
</div>
<div class="block_14">
<div class="container">
<div class="top">
<div class="honors">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/honors2.png"/>
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/honors1.png"/>
<span class="logo">
<b>Eco</b>Slim
       </span>
</div>
<div class="description" data-xd="text47">
<span>
        votre formule minceur
       </span>
</div>
</div>
<div class="bottom">
<div class="left">
<div class="price" data-skip="">
<div class="old_price">
         98 €
        </div>
<div class="new_price">
         49 €
        </div>
</div>
<div class="product">
<img src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/images/product_1.png"/>
<span class="discount">
<span>
          Remise
          <b>
           -50%
          </b>
</span>
</span>
<div class="decor">
</div>
</div>
</div>
<div class="right">
<div class="form">
<span data-xd="text48">
<p>
          Formulaire de commande
         </p>
<!-- -->
<div class="s__of">
<button class="toform">
<span>
            Commander maintenant
           </span>
</button>
</div>
<!---->
</span>
</div>
</div>
</div>
</div>
</div>
</div>
<!--  -->
<link href="//st.acstnst.com/content/EcoSlim_FR_Green_sp/css/styleSecond.css" rel="stylesheet"/>
<div class="hidden-window">
<div class="second-clips-header">
<div class="containerz">
<div class="h-w__inner">
<div class="yop-right__side">
<div class="dtable">
<div class="dtable-cell">EcoSlim
        </div>
</div>
</div>
</div>
</div>
</div>
<div class="containerz">
<section class="h-w__inner">
<div class="cf">
<div class="w-h__left">
<div class="w-h_vis_mob">
<select class="select inp select_count change-package-selector" name="count_select">
<option data-slide-index="0" value="1">1 paquet
         </option>
<option data-slide-index="1" selected="selected" value="3">2+1 paquets
         </option>
<option data-slide-index="2" value="5">3+2 paquets
         </option>
</select>
</div>
<ul class="bx-bx">
<li class="item transitionmyl1" data-value="1">
<div class="item__left">
<div class="js_changer change-package-button" data-package-id="1">
</div>
<div class="img-wrapp">
<img alt="z1" height="210" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/imagesSec/zit1.png"/>
</div>
<div class="zHeader">
<span>
</span>
</div>
<div class="pack_descr">EcoSlim méthode minceur №1. Offre unique valable qu'aujourd'hui. Minceur expresse à moins 50%! Effet immédiat! 1 paquet de EcoSlim au prix nouveau!</div>
</div>
<div class="item__right">
<div class="zHeader">
<b>1
           </b> paquet
          </div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prix:
             </div>
<div class="dtable-cell nowrap">
<b>
<text class="js-pp-new">49
               </text> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">98
                                  </text>
</span>
</div>
<div class="dtable-cell text-left">
<span class="old-pr-descr">Ancien prix
                                </span>
</div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Livraison:
             </div>
<div class="dtable-cell">
<b>
<text class="js-pp-ship">6
               </text> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
<div class="zSep">
</div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Total:
             </div>
<div class="dtable-cell prtotal">
<b>
<text class="js-pp-full">55
               </text> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
</div>
</div>
<div class="zDiscount">
<b>Attention</b>:
          <span>Rabais de 50%
                    </span>
</div>
</li>
<li class="item hot transitionmyl1 active" data-value="3">
<div class="zstick">
</div>
<div class="item__left">
<div class="js_changer change-package-button active" data-package-id="3">
</div>
<div class="img-wrapp">
<img alt="z1" height="210" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/imagesSec/zit2.png"/>
</div>
<div class="zHeader">
<span>
</span>
</div>
<div class="pack_descr">Le résultat Stable! Une cure de EcoSlim vous aidera à éliminer les kilos en trop pour toujours! 3 paquets de EcoSlim pour le prix de 2!</div>
</div>
<div class="item__right">
<div class="zHeader sec">
<b>2
           </b> paquets
           <span class="dib zplus">paquet
                      </span>
</div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prix:
             </div>
<div class="dtable-cell nowrap">
<b>
<text class="js-pp-new">98
               </text> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">196
                                  </text>
</span>
</div>
<div class="dtable-cell text-left">
<span class="old-pr-descr">Ancien prix
                                </span>
</div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Livraison:
             </div>
<div class="dtable-cell">
<b>
<text class="js-pp-ship">6
               </text> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
<div class="zSep">
</div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Total:
             </div>
<div class="dtable-cell prtotal">
<b>
<text class="js-pp-full">104
               </text> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
</div>
</div>
<div class="zDiscount">
<b>Attention</b>:
          <span>Rabais de 50%
                    </span>
</div>
</li>
<li class="item transitionmyl1" data-value="5">
<div class="item__left">
<div class="js_changer change-package-button" data-package-id="5">
</div>
<div class="img-wrapp">
<img alt="z1" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/imagesSec/zit3.png" width="210"/>
</div>
<div class="zHeader">
<span>
</span>
</div>
<div class="pack_descr">L'approche professionnelle! Une cure de 6 mois de EcoSlim efficace même pour un surpoids très important! Une véritable transformation! Résultat garanti. 5 paquets de EcoSlim pour le prix de 3!</div>
</div>
<div class="item__right">
<div class="zHeader sec">
<b>3
           </b> paquets
           <span class="dib zplus sec">paquets
                      </span>
</div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prix:
             </div>
<div class="dtable-cell nowrap">
<b>
<text class="js-pp-new">147
               </text> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">294
                                  </text>
</span>
</div>
<div class="dtable-cell text-left">
<span class="old-pr-descr">Ancien prix
                                </span>
</div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Livraison:
             </div>
<div class="dtable-cell">
<b>
<span>
<text class="js-pp-ship">6
                                </text>
</span> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
<div class="zSep">
</div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Total:
             </div>
<div class="dtable-cell prtotal">
<b>
<text class="js-pp-full">153
               </text> €
              </b>
</div>
<div class="dtable-cell text-center mw30">
</div>
</div>
</div>
</div>
</div>
<div class="zDiscount">
<b>Attention</b>:
          <span>Rabais de 50%
                    </span>
</div>
</li>
</ul>
</div>
<div class="w-h__right">
<div class="w-h__right-row1">
<div class="zHeader">Paiement à la livraison</div>
<div class="zContent">
<div class="dtable">
<div class="dtable-cell text-right">
<i>1
           </i>
<img alt="alt1" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/imagesSec/dtc1.png"/>
</div>
<div class="dtable-cell">La commande</div>
</div>
<div class="dtable">
<div class="dtable-cell text-right">
<i>2
           </i>
<img alt="alt1" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/imagesSec/dtc2.png"/>
</div>
<div class="dtable-cell">Livraison</div>
</div>
<div class="dtable">
<div class="dtable-cell text-right">
<i>3
           </i>
<img alt="alt1" src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/imagesSec/dtc3.png"/>
</div>
<div class="dtable-cell" style="line-height: 25px;">Livraison
           <span>et paiement</span>
</div>
</div>
</div>
</div>
<div class="printbg">Offre avantageuse</div>
<form action="" class="js_scrollForm" method="post"><input type="hidden" name="total_price" value="55.0">
<div class="formHeader">Entrez vos données pour passer commande</div>
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAJwFgAAAAAAAAAAAASH8XRgAA"></iframe>
<div class="text-center totalpriceForm">Livraison:
         <span class="">
</span>6 €
        </div>
<div class="text-center totalpriceForm">Total:
         <span class="js_total_price js_full_price"> 55
                  </span> €
        </div>
</form>
<div class="zGarant">
<div class="zHeader">Nous garantissons
        </div>
<ul>
<li><b>100%</b> qualité</li>
<li><b>Vérification</b> du produit à la réception</li>
<li><b>Sécurité</b> de vos données</li>
</ul>
</div>
</div>
</div>
</section>
</div>
</div>
<!--  -->
</body>
</html>
<?php } ?>