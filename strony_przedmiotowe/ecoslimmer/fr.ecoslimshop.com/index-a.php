
<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 13788 -->
    <script>var locale = "fr";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQEvAnAWBFSJDogAAAEAAusVAQAC3DUCBgEBAASPoGjpAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":6},"3":{"price":98,"old_price":196,"shipment_price":6},"5":{"price":147,"old_price":294,"shipment_price":6}};
        var shipment_price = 6;
        var name_hint = 'Rémi Morel';
        var phone_hint = '+336314254';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a><br><a class="download" href="#">Download our Tips!</a></div>');

            moment.locale("fr");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));
            $('.download').click(function(e) {
                e.preventDefault();  //stop the browser from following
                window.location.href = 'out_tips.pdf';
            });

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
    <!-- End Facebook Pixel Code -->



    <meta charset="utf-8"/>
    <meta content="width=1024, initial-scale=1, user-scalable=yes" name="viewport"/>
    <title>EcoSlim</title>
    <link href="https://fonts.googleapis.com/css?family=Exo+2:200,600" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Roboto:300,700&amp;subset=cyrillic-ext,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
    <link href="//st.acstnst.com/content/EcoSlim_FR_Green_sp/css/main.css" rel="stylesheet"/>
    <script src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/js/jquery.bxslider.min.js"></script>
    <script src="//st.acstnst.com/content/EcoSlim_FR_Green_sp/js/secondPage.js"></script>
    <style>
        .block_1 .bottom .product:after{
            background: none;
        }
        .block_2__list:before{
            background: none;
        }
        .block_7__list li:nth-child(1):before{
            background: none;
        }
        .block_7__list li:nth-child(2):before{
            background: none;
        }
        .block_7__list li:nth-child(3):before{
            background: none;
        }
        .block_7__list li:nth-child(4):before{
            background: none;
        }
    </style>
</head>
<body>
<div class="wraper s__main">
    <div class="block_1">
        <div class="container">
            <div class="top" data-xd="text01">
<span class="logo">
<b>Choisir une méthode efficace pour perdre du poids</b>
      </span>
                <div class="description">
<span>
        Presque tous les programmes de perte de poids programmes peuvent fonctionner, mais seulement si vous êtes suffisamment motivé pour diminuer la quantité de calories que vous mangez ou augmenter la quantité de calories que vous brûlez chaque jour (ou les deux).
       </span>
                </div>
            </div>
            <div class="title" data-xd="text02">
                <b>
                    chaque jour
                </b>
                perte de
            </div>
            <div class="good">
<span data-xd="text05">
       Un programme responsable
      </span>
            </div>
            <div class="bottom">
                <div class="product">
                 <span class="discount" data-xd="text03">

        Commander maintenant
       </span>
                </button>
            </div>
        </div>
    </div>
    <div class="block_2">
        <div class="container">
            <div class="title">
                Le régime alimentaire doit être sain.
                <b>
                    Il devrait inclure tous les apports journaliers recommandés
                </b>
            </div>
            <div class="description" data-xd="text06">
                <b>
                    minéraux et protéines. Le régime alimentaire de perte
       <span class="logo">
<b>Eco</b>Slim
       </span>
                </b>
                Le programme de perte de poids doit être dirigé vers une perte
            </div>
            <ul class="block_2__list">
                <li>
                    <div class="line_block" data-xd="text07">
<span>
<big>
    Si vous avez l’intention de perdre plus de 15 à 20 kg
</big>
<b>
    Si vous avez l’intention de perdre plus de 15 à 20 kg
</b>de résoudre des problèmes de santé, ou si vous prenez des médicaments sur une base régulière
        </span>
                    </div>
                    <div class="line_block" data-xd="text08">
<span>
<b>
<span>
<span class="logo">
<b>Eco</b>Slim
       </span>
       Si vous avez l’intention de perdre plus de 15 à 20 kg, de résoudre des problèmes de santé, ou si vous prenez des médicaments sur une base régulière, vous devriez être évalué par votre médecin avant de
      </span>
            </div>
        </div>
    </div>
    <div class="block_7">
        <div class="container">
            <div class="title" data-xd="text23">
                commencer votre programme d’amaigrissement. Un médecin peut évaluer votre état de santé général et les conditions médicales qui pourraient être touchés par les régimes et la perte de poids
            </div>
            <ul class="block_7__list">
                <li>
<span>
</span>

                    Votree programme doit inclure des plans pour le maintien du poids une fois que la phase de perte de poids est terminée. Il y a peu d’utilité à perdre une grande quantité de poids et de le reprendre de suite. Le maintien du poids est la partie la plus difficile pour contrôler son poids et n’est pas systématiquement mises en œuvre par les programmes réserve.

                </li>
                <li>
<span>
</span>
                    <big data-xd="text25">
                        Le contrôle du poids doit être considéré comme un effort long et désiré
                    </big>
                    <small>
                        L’obésité est une maladie chronique. Elle est trop souvent perçue comme un problème temporaire qui peut être traité pendant quelques mois avec un régime intense. Cependant, comme la plupart des personnes en surpoids savent, le contrôle du poids doit être considéré comme un effort à long terme. Pour être sûr et efficace, tout programme de perte de poids doit tenir compte de l’approche à long terme ou sinon le progr
                    </small>
                </li>
                <li>
<span>
</span>
                    <big data-xd="text27">
                        <b>
                            L’obésité affecte
                        </b>
                    </big>
                    <small>
                        L’obésité affecte environ une personne sur 8 d’âge adulte, et au cours d’une année, plus de la moitié de ces personnes font un régime pour perdre du poids ou tentent de maintenir leur poids. Pour beaucoup de gens qui essaient de perdre du poids, il est difficile de perdre plus de quelques kilos! Et peu réussissent à maintenir leurs kg perdu, cette difficulté à perdre du poids et le garder pousse les gens à se tourner vers un professionnel ou un programme d’amaigrissement.
                    </small>
                </li>
                <li>
<span>
</span>
                </li>
            </ul>
        </div>
    </div>
    </div>
</div>
<!--  -->
</body>
</html>