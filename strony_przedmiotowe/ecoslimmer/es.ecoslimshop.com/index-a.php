<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 11359 -->
    <script>var locale = "es";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQEvAmUWBIBmC4gAAAEAAuAVAQACXywCBgEBAARJmAEOAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript"
            src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height: 0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }

        .ac_footer a {
            color: #A12000;
        }

        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {
            "1": {"price": 49, "old_price": 98, "shipment_price": 0},
            "3": {"price": 98, "old_price": 196, "shipment_price": 0},
            "5": {"price": 147, "old_price": 294, "shipment_price": 0}
        };
        var shipment_price = 0;
        var name_hint = 'Gabriela Torres';
        var phone_hint = '+34914123456';

        $(document).ready(function () {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a><br><a class="download" href="#">Download our Tips!</a></div>');

            moment.locale("es");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            $('.download').click(function(e) {
                e.preventDefault();  //stop the browser from following
                window.location.href = 'out_tips.pdf';
            });

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet"
          media="all">


    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', '//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->


    <script src="//st.acstnst.com/content/Eco_Slim_ES_new/js/secondPage.js" type="text/javascript"></script>
    <script src="//st.acstnst.com/content/Eco_Slim_ES_new/js/jquery.bxslider.min.js"
            type="text/javascript"></script>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <title>EcoSlim</title>
    <link href="//st.acstnst.com/content/Eco_Slim_ES_new/css/main.css" rel="stylesheet"/>
    <style>
        .product div:before{
            background: none;
        }
        .block_3 .container:before{
            background: none;
        }
        .block_4{
            background: none;
        }
        .block_6{
            background: none;
        }
        .block_6:before{
            background: none;
        }
    </style>
</head>
<body>
<div class="main body">
    <div class="block_1">
        <div class="elements">
            <div class="container">
                <div class="title">
                    <p>Un Simple Giro En Tu Mentalidad Quemará Más Grasa Más Rápido </b></p>
                    <p><span class="logo"> </span></p>
                </div>

                <div class="product">
                    <div>
                           <small><span><b>2016</b>   </span></small>
                    </div>
                </div>
                <div class="form_pr">
                    <div class="form">
                        <p> La mayoría de la gente con la que platico sobre salud y condición física habla sobre
                            “ponerse en forma”. Sin embargo, no creo que se den cuenta de que en realidad sus palabras
                            resultan contraproducentes para sus aspiraciones de quemar grasa. </p>


                    </div>
                       </div>
            </div>
        </div>
        <div class="bottom">
            <div class="container">
                <p></p></div>
        </div>
    </div>

    <div class="block_3">
        <div class="container">
            <div class="woman">
                 </div>
              <div class="text">
                <div class="why">

                    <p> Escuchas a mucha gente lamentarse por estar fuera de forma y decir, “Necesito ponerme en forma y
                        quemar grasa”, o “Quiero ponerme en forma y quemar grasa”. Lo que nunca escuchas es, “Quiero
                        ponerme en forma, quemar grasa y mantenerme de esa manera.”</p></div>
                <div class="warning">

                    <p> Quizás quieran estar en forma, pero su mentalidad se concentra en ponerse en forma. Esto los
                        vuelve más susceptibles a las dietas de moda y al último aparato de ejercicio en el mercado.
                        Están tan concentrados en ponerse en forma que prácticamente probarán cualquier cosa sin
                        importar lo ridícula o difícil que pudiera ser, todo porque están enfocados totalmente en la
                        meta a corto plazo.</p>
                </div>
            </div>
        </div>
        <div class="line"></div>
    </div>
    <div class="block_4">
        <div class="top">
            <div class="container">

                <div class="letter">
                    <p> Un simple cambio de una mentalidad de “solución rápida” a una que busque el éxito a largo plazo
                        te llevará por el camino correcto. Te sentirás menos atraído por la publicidad que aparece en la
                        televisión en la madrugada, y estarás más concentrado en hacer las cosas adecuadas la mayor
                        parte del tiempo. ¿Estarías de acuerdo con que una buena salud y condición física que dure toda
                        la vida es lo más importante?</p>
                    <p>Por supuesto, la creación de un ambiente en tu cuerpo que te permita quemar grasa rápido es
                        esencial para esta meta. Una persona que está en forma a los 50 y 60 años es mucho más
                        impresionante que alguien que está delgado y en forma a los 20 y 30 años. Eso se debe a que la
                        dedicación que se necesita para mantener un plan de ejercicio adecuado y una estrategia sensible
                        de nutrición por el resto de la vida, es raro de encontrar. Es una mentalidad inusual. </p>

                </div>
                 </div>
        </div>
        <div class="bottom">
            <div class="container">
                <div class="eko">
                    <div>
                        <h3><b></h3>
                        <p></p>
                    </div>
                </div>
                <div class="woman">
                     </div>
                <div class="eko2">
                    <div></div>
                </div>
            </div>
        </div>
        <div class="line"></div>
        <ul class="list">
            <li></li>
            <li></li>
        </ul>
    </div>

    <div class="block_6">
        <div class="container">

            <p> Si realmente deseas transformar tu físico y tu salud para siempre, debes de convertirlo un estilo de
                vida, un hábito. Y eso comienza en tu mente. Eso es lo que hace la gente que está delgada de por vida.
                De hecho, si pienso en toda la gente que conozco personalmente que posee una grandiosa forma desde hace
                algún tiempo, comienzo a comprender que aplican sus mentes en estar en forma.</p>
            <p>Piensan al respecto día y noche, no obsesivamente, pero con cada cosa que hacen… subconscientemente
                piensan, “¿Esto va a afectar positiva o negativamente mi salud y condición física a largo plazo?”.
                “¿Razonablemente estoy haciendo todo lo que puedo para quemar grasa y vivir un estilo de vida
                saludable?”
            </p>
        </div>
        <div class="line"></div>
    </div>
    <div class="block_7">
        <div class="container">
            <div class="left">
                <h3>
                </h3>
                <p> La buena noticia es que la investigación muestra que puedes desarrollar hábitos sencillos como los
                    involucrados en la alimentación y el ejercicio apropiado, en menos de un mes… a veces más pronto.
                    Además, con el tipo de ejercicio y consejos de dieta que les recomiendo a mis estudiantes, no
                    necesitas renunciar a tus actividades o a tu vida por estar entrenando todo el día, ni necesitas
                    seguir una dieta loca que de ninguna manera puedes incorporar a tu estilo de vida.
                </p>


            </div>
            <div class="right">
                <div class="img">
                   </div>
            </div>
        </div>
        <div class="line"></div>
    </div>
    <div class="block_8">
        <div class="container">
            <h3>
            </h3>
            <ul>
                <li>
<span>
</span>
                    <span> Asimismo, he descubierto que después de unos cuantos meses practicando estos métodos, tu cuerpo comenzará a decirte que desea seguir haciendo ejercicio y comer adecuadamente… no te sentirás bien si no lo haces. ¿Por qué? Porque tu cuerpo naturalmente desea estar sano y delgado. </span>
                </li>
                <li>
<span>
<b> A qué esperas para empezar a quemar tu grasa!! </b>

						</span>
                    <span>
Por lo tanto, te insto a abandonar la mentalidad de “ponerse en forma”, y en su lugar adoptar la idea de estar en forma y crear ese incinerador de grasa que es tan esencial para tener una salud y condición física que dure toda la vida.
                    
                     Imagínate en el cuerpo de tus sueños, hoy, mañana y dentro de 5, 10, 20 o más años a partir de ahora. ¿Qué estarás haciendo para entonces, en tu cuerpo delgado, fuerte y sano? Es muy probable que no estarás intentando “ponerte en forma”. Más bien, estarás quemando grasa 24/7, disfrutando de la vida como se debe… lleno de vitalidad y pasión.
Rob Poulos es un celebrado autor de acondicionamiento físico, experto en pérdida de grasa, fundador y presidente de Zero to Hero Fitness. Rob creó el método más eficiente del mundo para una pérdida rápida y permanente de grasa con su sistema “Incinerador de Grasa” para ayudar a quienes deseen poner un punto final a las restrictivas dietas de moda, a los largos y aburridos entrenamientos con ejercicios cardiovasculares y a la necesidad de poseer una voluntad sobrehumana para siempre.

                    </span>
                </li>
            </ul>
        </div>
        <div class="bottom">
            <div class="container">
                <div class="left">
                    <p>
                    </p>

                </div>
                <div class="product3">
                    </div>
                <div class="right">
                    <p>
                        No obstante, si sientes muchos deseos de comenzar a construir tu nuevo cuerpo, también puedes
                        hacer click en el siguiente botón para que arranques ahora mismo. Recuerda, tienes 60 días
                        completos para decidir si esto es lo adecuado para ti. En caso de que no sea así, solo envíame
                        un correo electrónico y te daremos un reembolso completo, y te daré las gracias por haberlo
                        probado… ¡Así de simple!</p>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>