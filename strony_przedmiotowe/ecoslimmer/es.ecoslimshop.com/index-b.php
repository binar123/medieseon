<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 11359 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQEvAmUWBIGSC4gAAAEAAuAVAQACXywCBgEBAASTzDOhAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript"
                src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height: 0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }

            .ac_footer a {
                color: #A12000;
            }

            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {
                "1": {"price": 49, "old_price": 98, "shipment_price": 0},
                "3": {"price": 98, "old_price": 196, "shipment_price": 0},
                "5": {"price": 147, "old_price": 294, "shipment_price": 0}
            };
            var shipment_price = 0;
            var name_hint = 'Gabriela Torres';
            var phone_hint = '+34914123456';

            $(document).ready(function () {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet"
              media="all">


        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', '//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <title>EcoSlim</title>
        <link href="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/css/main.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/js/second_page.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/js/jquery.bxslider.min.js"
                type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({scrollTop: $("form").offset().top - 300}, 1000);
                    return false;
                });
                $('.block_8 ul').bxSlider({
                    adaptiveHeight: true,
                    infiniteLoop: true,
                    touchEnabled: true,
                    controls: false
                });
            });
        </script>
    </head>
    <body>
    <div class="main s__main">
        <div class="block_1">
            <div class="container">
                <div class="title">
                    <p data-xd="text1">Descubrimiento<b>científico innovador</b></p>
                    <p><span class="logo"><b>Eco</b>Slim</span></p>
                </div>
                <ul data-xd="text2">
                    <li>ADELGAZAMIENTO SEGURO</li>
                    <li>PIERDE 10-12 KILOS DE PESO AL MES</li>
                </ul>
                <div class="product">
                    <img src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/images/ecoslim.png"/>
                </div>
                <!-- -->
                <button class="s__of" data-scroll="true"><span>Realizar pedido</span></button>
                <!---->
            </div>
        </div>
        <div class="block_2">
            <div class="container">
                <p data-xd="text4">Elaborado <b>a base de vitaminas del grupo B</b> que promueven LA DESCOMPOSICIÓN DE
                    GRASAS</p>
                <ul data-xd="text5">
                    <li><span>10-12 kilos menos al mes</span></li>
                    <li><span>No afecta al corazón ni al sistema nervioso</span></li>
                    <li><span>Garantía de calidad, completamente respetuoso con el medio ambiente</span></li>
                    <li><span>Composición 100 % natural</span></li>
                    <li><span>Impacto preciso sobre los depósitos de grasa</span></li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_3">
            <div class="container">
                <div class="why" data-xd="text6">
                    <h3>¿Por qué engordamos?</h3>
                    <p>Seguro que te han dicho más de una vez que el principal motivo del sobrepeso son los trastornos
                        hormonales, un estilo de vida sedentario y una ecología deficiente. Pero esos son solo factores
                        indirectos.</p>
                </div>
                <div class="warning">
                    <h3 data-xd="text7"><b>En el 90 % de los casos</b> el exceso de peso es la consecuencia directa de
                        una nutrición poco natural.</h3>
                </div>
            </div>
        </div>
        <div class="block_4">
            <div class="top">
                <div class="container">
                    <h3 data-xd="text8">¡Tu cuerpo <font>necesita ayuda!</font></h3>
                    <div class="signature"><span><font
                                data-xd="text9">OPINIÓN DE UN ESPECIALISTA DIETÓLOGO</font>, <font data-xd="text10">Carolina Macías</font></span>
                    </div>
                    <div class="letter" data-xd="text11">
                        <p>Para favorecer la limpieza de tu organismo, disuelve de <b> EcoSlim </b> en un vaso de agua y
                            tómatelo a la hora de comer o después de la comida, una vez al día. Notarás mejorías en poco
                            tiempo. En una semana, habrás perdido peso y tus problemas estomacales y de digestión habrán
                            desaparecido. Un dato muy tranquilizador: su composición exclusiva ha sido diseñada en
                            colaboración con el Instituto de Nutrición. Los complejos de vitaminas naturales son mucho
                            más eficaces y seguros que cualquier compuesto químico. La combinación de taurina, cafeína y
                            extracto de guaraná se pueden considerar, con total confianza, un auténtico regalo para
                            aquellas personas que quieran adelgazar, ya que este complejo <b> destruye los depósitos de
                                grasa en las zonas problemáticas en cuestión de días</b>. »</p>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <h3>Eco Slim</h3>
                    <p data-xd="text12">ES UN PRODUCTO PURO, 100 % NATURAL <br/>Y ECOLÓGICO</p>
                </div>
            </div>
        </div>
        <div class="block_5">
            <div class="top">
                <div class="container">
                    <h3 data-xd="text13"><font>Composición exclusivamente seleccionada</font> para conseguir los mejores
                        resultados:</h3>
                    <ul data-xd="text14">
                        <li><span>L-carnitina</span></li>
                        <li><span>Cafeína</span></li>
                        <li><span>Quitosano</span></li>
                        <li><span>Extracto de<br/>coleus forskohlii  </span></li>
                        <li><span>Extracto de<br/> guaraná</span></li>
                        <li><span>Ácido succínico</span></li>
                        <li><span>Extracto de <br/>  algas Fucus  </span></li>
                        <li><span>Vitaminas <br/>  В2, В5, В6, В8, В12  </span></li>
                    </ul>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <h3>Eco Slim</h3>
                    <p data-xd="text15">Diseñado de acuerdo <br/>con la tecnología europea</p>
                </div>
            </div>
        </div>
        <div class="block_8">
            <div class="container">
                <h3 data-xd="text16">Comentarios de nuestros clientes</h3>
                <ul data-xd="text17">
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/images/block_8_li1.jpg"/>
<b>Marta Martín</b>
							24 años
						</span>
                        <span>  ¡He pasado de ser gorda toda mi vida a ser delgada en solo un mes! ¡Es increíble!   <b>  Estoy muy contenta con el producto Eco Slim.  </b>   Tiene buen sabor, reduce el apetito y acaba con la sensación de pesadez en el estómago. Es el primer producto con el que he conseguido perder 13 kilos. Al principio, perdía peso muy lentamente, pero después empecé a perder 1 kilo al día.  </span>
                    </li>
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/images/block_8_li2.jpg"/>
<b>Tomás Guzmán</b>
							32 años
						</span>
                        <span>  Para ser honesta, decidí perder peso por mi hija, porque no podía correr ni jugar con ella. Me resultaba difícil hasta agacharme. Perdí más de 10 kilos con mucha facilidad y después empecé a ir al gimnasio.   <b>  Eco Slim es un producto increíble.  </b>   Sigo tomándolo cuando no puedo ir al gimnasio o cuando como demasiado si voy de vacaciones.  </span>
                    </li>
                </ul>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="left">
                        <p><font data-xd="text18">Ya hemos vendido <br/>más de 20 mil unidades de</font> <b>EcoSlim</b>
                        </p>
                    </div>
                    <div class="product3">
                        <img src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/images/ecoslim2.png"/>
                    </div>
                    <div class="right">
                        <p data-xd="text19"><b>Date prisa y realiza tu pedido</b> a un precio reducido <b>ahora</b> para
                            conseguir una figura perfecta <b>en un par de semanas</b>!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_9">
            <div class="container">
                <p data-xd="text20">Pide <span><b>ECO</b>SLIM</span> desde nuestra página </p>
                <div class="form_pr">
                    <div class="price">
                        <span class="precent" data-xd="text22">50% de descuento</span>
                        <span class="shipment" data-xd="text23">+ gastos de envío</span>
                        <span class="old_price js_old_price">98</span>
                        <span class="new_price"><span class="js_new_price">49</span> <span
                                class="js_curs">€</span></span>
                    </div>
                    <!-- -->
                    <div class="form2 s__of">
                        <button data-scroll="true"><span>Realizar pedido</span></button>
                    </div>
                    <!---->
                </div>
            </div>
        </div>
    </div>
    <!--  -->
    <link href="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/css/styleSecond.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet"
          type="text/css"/>
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic"
        rel="stylesheet"/>
    <div class="hidden-window">
        <div class="header_new">
            <div class="containerz">
                <div class="h-w__inner">
                    <div class="dtable">
                        <div class="dtable-cell text-center">
                            <span class="logo_s"><b>Eco</b>Slim</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Pago al ser entregado</div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr"
                                                               src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/imagesSec/Zarr.png"/><img
                                                    alt="alt1"
                                                    src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/imagesSec/dtc1.png"/>
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1"
                                                               src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/imagesSec/dtc2.png"/>
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft"
                                                               src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/imagesSec/Zarr.png"/><img
                                                    alt="alt1"
                                                    src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/imagesSec/dtc3.png"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">EL PEDIDO</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">ENVÍO</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">ENTREGA Y <span>PAGO</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext">TRANSACCIÓN SEGURA</div>
                            <select class="corbselect select inp select_count change-package-selector" name="quantity">
                                <option data-slide-index="0" value="1">1 paquete</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 paquetes</option>
                                <option data-slide-index="2" value="5">3+2 paquetes</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="side-right">
                                            <div class="ssimgabs"><img alt="zt1"
                                                                       src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/imagesSec/zit1.png"/>
                                            </div>
                                            <div class="zHeader onepack"><b>1</b> paquetes</div>
                                            <br/>
                                        </div>
                                        <div class="pack_descr">¡Adelgazamiento exprés con un 50% de descuento! ¡Efecto
                                            inmediato!
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Precio</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 49</text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 98</text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile">+ un regalo</div>
                                                    <img alt="zt1"
                                                         src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/imagesSec/zit2.png"/>
                                                </div>
                                                <div class="zHeader"><b>2</b> paquets
                                                    <br/> <span class="dib zplus">un regalo</span></div>
                                            </div>
                                        </div>
                                        <div class="pack_descr">¡Resultados estables! El tratamiento de Eco Slim te
                                            ayudará a deshacerte del exceso de peso para siempre!
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Precio</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 98</text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 196</text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right">
                                            <div class="ssimgabs">
                                                <div class="giftmobile">+ dos regalos</div>
                                                <img alt="zt1"
                                                     src="//st.acstnst.com/content/Eco_Slim_ES_new/mobile/imagesSec/zit3_n3.png"/>
                                            </div>
                                            <div class="zHeader"><b>3</b> paquets
                                                <br/> <span class="dib zplus sec">dos regalos</span></div>
                                        </div>
                                        <div class="pack_descr">¡Enfoque profesional! ¡Un tratamiento de medio año con
                                            Eco Slim te ayudará incluso en casos de obesidad! ¡Una transformación real!
                                            Resultados garantizados.
                                        </div>
                                        <div class="dtable" style="width: 85%;margin: 0 auto;">
                                            <div class="dtable-cell red text-right">Precio</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">
                                                    <text class="js-pp-new"> 147</text>
                                                </div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price">
                                                    <i></i>
                                                    <text class="js-pp-old"> 294</text>
                                                </div>
                                                €
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAJlFgAAAAAAAAAAAATf00i9AA"></iframe>
                                  
                            <!--
                            <input type="hidden" name="total_price" value="280">
                            <input type="hidden" name="esub" value="-4AAEvAgoXAQAAAAEAAqoWAQACti0CBgEBAAQRC0cOAA">
                            <input type="hidden" name="goods_id" value="217">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Eco_Slim_RO1">
                            <input type="hidden" name="price" value="280">
                            <input type="hidden" name="old_price" value="280">
                            <input type="hidden" name="total_price_wo_shipping" value="280">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 140, 'old_price': 280, 'shipment_price': 0}, u'3': {'price': 280, 'old_price': 560, 'shipment_price': 0}, u'5': {'price': 420, 'old_price': 840, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Lei">
                            <input type="hidden" name="package_id" value="3">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="PL">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.09">

                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="RO">România</option>
                                <option value="PL" selected="selected">Poland</option></select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 pachet</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 pachete</option>
                                <option data-slide-index="2" value="5">3+2 pachete</option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Introdu numele" type="text" value="">
                            <input class="only_number inp j-inp" name="phone" placeholder="Introdu numărul de telefon" type="text" value="">

                            <div class="text-center totalpriceForm">Livrare: 0 Lei</div>
                            <div class="text-center totalpriceForm">total:<br> <span class="js_total_price js_full_price">280</span> Lei</div>
                            <div class="zbtn js_submit transitionmyl" style="cursor:pointer"> Comandă </div>
                            <input type="hidden" name="time_zone" value="1">
                           -->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">TE GARANTIZAMOS</div>
                            <ul>
                                <li><b>100%</b> calidad</li>
                                <li><b>Comprobar </b> el producto tras recibirlo</li>
                                <li><span>Seguridad </span>de tus datos</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!--  -->
    </body>
    </html>
<?php } else { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 11359 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQEvAmUWBIBmC4gAAAEAAuAVAQACXywCBgEBAARJmAEOAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript"
                src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height: 0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }

            .ac_footer a {
                color: #A12000;
            }

            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {
                "1": {"price": 49, "old_price": 98, "shipment_price": 0},
                "3": {"price": 98, "old_price": 196, "shipment_price": 0},
                "5": {"price": 147, "old_price": 294, "shipment_price": 0}
            };
            var shipment_price = 0;
            var name_hint = 'Gabriela Torres';
            var phone_hint = '+34914123456';

            $(document).ready(function () {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet"
              media="all">


        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', '//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <script src="//st.acstnst.com/content/Eco_Slim_ES_new/js/secondPage.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Eco_Slim_ES_new/js/jquery.bxslider.min.js"
                type="text/javascript"></script>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <title>EcoSlim</title>
        <link href="//st.acstnst.com/content/Eco_Slim_ES_new/css/main.css" rel="stylesheet"/>
    </head>
    <body>
    <div class="main s__main">
        <div class="block_1">
            <div class="elements">
                <div class="container">
                    <div class="title">
                        <p data-xd="text1">Descubrimiento <b>científico innovador</b></p>
                        <p><span class="logo"><b>Eco</b>Slim</span></p>
                    </div>
                    <ul data-xd="text2">
                        <li>ADELGAZAMIENTO SEGURO</li>
                        <li>PIERDE 10-12 KILOS DE PESO AL MES</li>
                    </ul>
                    <div class="product">
                        <div>
                            <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/ecoslim.png"/>
                            <small><span><b class="ryear"></b>Descubrimiento innovador </span></small>
                        </div>
                    </div>
                    <div class="form_pr">
                        <div class="form">
                            <!-- -->
                            <div class="form_2 s__of">
                                <p> Pide <span><b>  ECO  </b>  SLIM  </span> desde nuestra página </p>
                                <span class="old_price js_old_price">98</span>
                                <span class="new_price"><span class="js_new_price">49</span> <span
                                        class="js_curs">€ </span></span>
                                <span class="precent">50% de descuento</span>
                                <button class="toform"><span>Realizar pedido</span></button>
                            </div>
                            <!---->
                        </div>
                        <!-- -->
                        <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_1_water2.png"/>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <p data-xd="text4">Elaborado <b>a base de vitaminas del grupo B</b> que promueven LA DESCOMPOSICIÓN
                        DE GRASAS</p>
                </div>
            </div>
        </div>
        <div class="block_2">
            <div class="container">
                <ul data-xd="text5">
                    <li><span>  10-12 kilos menos al mes  </span></li>
                    <li><span>  No afecta al corazón ni al sistema nervioso  </span></li>
                    <li><span>  Garantía de calidad, completamente respetuoso con el medio ambiente  </span></li>
                    <li><span>  Composición 100 % natural  </span></li>
                    <li><span>  Impacto preciso sobre los depósitos de grasa  </span></li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_3">
            <div class="container">
                <div class="woman">
                    <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_3_woman.png"/>
                </div>
                <img class="food" src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_3_food3.png"/>
                <div class="text">
                    <div class="why" data-xd="text6">
                        <h3> ¿Por qué engordamos? </h3>
                        <p> Seguro que te han dicho más de una vez que el principal motivo del sobrepeso son los
                            trastornos hormonales, un estilo de vida sedentario y una ecología deficiente. Pero esos son
                            solo factores indirectos. </p>
                    </div>
                    <div class="warning">
                        <h3> En el 90 % de los casos, el exceso de peso es la consecuencia directa de una nutrición poco
                            natural. </h3>
                        <p> Los productos que compramos en las tiendas contienen conservantes artificiales, saborizantes
                            y colorantes sintéticos. Estos componentes químicos provocan problemas en la digestión,
                            favorecen la creación de depósitos de grasas y la acumulación de sustancias perjudiciales en
                            el organismo. </p>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_4">
            <div class="top">
                <div class="container">
                    <h3 data-xd="text8">¡Tu cuerpo necesita <font>ayuda!</font></h3>
                    <div class="letter">
                        <p data-xd="text9">OPINIÓN DE UN ESPECIALISTA DIETÓLOGO</p>
                        <div data-xd="text11">
                            <p>Para favorecer la limpieza de tu organismo, disuelve de <b> EcoSlim </b> en un vaso de
                                agua y tómatelo a la hora de comer o después de la comida, una vez al día. Notarás
                                mejorías en poco tiempo. En una semana, habrás perdido peso y tus problemas estomacales
                                y de digestión habrán desaparecido. Un dato muy tranquilizador: su composición exclusiva
                                ha sido diseñada en colaboración con el Instituto de Nutrición. Los complejos de
                                vitaminas naturales son mucho más eficaces y seguros que cualquier compuesto químico. La
                                combinación de taurina, cafeína y extracto de guaraná se pueden considerar, con total
                                confianza, un auténtico regalo para aquellas personas que quieran adelgazar, ya que este
                                complejo <b> destruye los depósitos de grasa en las zonas problemáticas en cuestión de
                                    días</b>. »</p>
                        </div>
                        <div class="signature" data-xd="text10">Carolina Macías</div>
                    </div>
                    <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_4_doc.png"/>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="eko">
                        <div>
                            <h3><b>Eco</b>Slim</h3>
                            <p data-xd="text12">es un producto puro,<br/>100 % natural y ecológico</p>
                        </div>
                    </div>
                    <div class="woman">
                        <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_4_woman.png"/>
                    </div>
                    <div class="eko2">
                        <div><span>¡No contiene químicos ni sustancias sintéticas!</span></div>
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <ul class="list">
                <li></li>
                <li></li>
            </ul>
        </div>
        <div class="block_5">
            <div class="top">
                <div class="container">
                    <h3 data-xd="text13"><font>Composición exclusivamente seleccionada</font> para conseguir los mejores
                        resultados:</h3>
                    <ul data-xd="text14">
                        <li><span>L-carnitina</span></li>
                        <li><span>Cafeína</span></li>
                        <li><span>Quitosano</span></li>
                        <li><span>Extracto de<br/>coleus forskohlii  </span></li>
                        <li><span>Extracto de<br/> guaraná</span></li>
                        <li><span>Ácido succínico</span></li>
                        <li><span>Extracto de <br/>  algas Fucus  </span></li>
                        <li><span>Vitaminas <br/>  В2, В5, В6, В8, В12  </span></li>
                    </ul>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <p><b>Eco</b>Slim</p>
                    <div class="approved" data-xd="text15">Diseñado de acuerdo<br/> con la tecnología europea</div>
                    <div class="stamp">Aprobado por
                        <small>inspección sanitaria</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_6">
            <div class="container">
                <h3>¡La falta de minerales es la razón del sobrepeso!</h3>
                <p> Quizás pienses que las plantas <br/> no tienen la suficiente potencia para quemar grasas... <br/><b>
                        ¡Pero las investigaciones científicas demuestran lo contrario! </b></p>
                <ul>
                    <li>
                        <small> B2</small>
                        <span><b>  Normaliza  </b>   el equilibrio hormonal del cuerpo  </span></li>
                    <li>
                        <small> B6</small>
                        <span><b>  Regula  </b>   los procesos metabólicos, fortalece la piel, el cabello y las uñas</span>
                    </li>
                    <li>
                        <small> B12</small>
                        <span><b>  Acelera  </b>   el metabolismo, ayuda a asimilar elementos nutritivos  </span></li>
                    <li>
                        <small> B5</small>
                        <span><b>  Contribuye a  </b>   la descomposición de carbohidratos y grasas  </span></li>
                    <li>
                        <small> B8</small>
                        <span><b>  Reduce  </b>   el colesterol, regula el funcionamiento del tracto gastrointestinal  </span>
                    </li>
                    <li>
                        <small> Taurina</small>
                        <span><b>  Mejora  </b>   los procesos metabólicos, quema grasas  </span></li>
                    <li>
                        <small> Cafeína</small>
                        <small> Ácido succínico</small>
                        <span><b>  Elimina las toxinas y el exceso de líquidos,  </b>   quema grasas y favorece el proceso de renovación de las células del cuerpo.  </span>
                    </li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_7">
            <div class="container">
                <div class="left">
                    <h3> Tus resultados <big> futuros: </big></h3>
                    <p> Solo 1 vaso de bebida <b> EcoSlim </b> y tu figura empezará a cambiar día a día. </p>
                    <ul>
                        <li> hasta 0,5 kilos menos al día</li>
                        <li> HASTA 3,5 KILOS MENOS A LA SEMANA</li>
                        <li> HASTA 10-12 KILOS MENOS AL MES</li>
                    </ul>
                    <div class="safely">
                        <b> Seguro </b>
                        No afecta al sistema cardiovascular ni al sistema nervioso
                    </div>
                </div>
                <div class="right">
                    <div class="img">
                        <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_7_woman.png"/>
                        <img class="product2" src="//st.acstnst.com/content/Eco_Slim_ES_new/images/ecoslim2.png"/>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_8">
            <div class="container">
                <h3><font data-xd="text16">Comentarios</font> de nuestros clientes</h3>
                <ul data-xd="text17">
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_8_li1.jpg"/>
<b>  Marta Martín  </b>
							  24 años
						</span>
                        <span>  ¡He pasado de ser gorda toda mi vida a ser delgada en solo un mes! ¡Es increíble!   <b>  Estoy muy contenta con el producto Eco Slim.  </b>   Tiene buen sabor, reduce el apetito y acaba con la sensación de pesadez en el estómago. Es el primer producto con el que he conseguido perder 13 kilos. Al principio, perdía peso muy lentamente, pero después empecé a perder 1 kilo al día.  </span>
                    </li>
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_8_li2.jpg"/>
<b>  Tomás Guzmán  </b>
							  32 años
						</span>
                        <span>  Para ser honesta, decidí perder peso por mi hija, porque no podía correr ni jugar con ella. Me resultaba difícil hasta agacharme. Perdí más de 10 kilos con mucha facilidad y después empecé a ir al gimnasio.   <b>  Eco Slim es un producto increíble.  </b>   Sigo tomándolo cuando no puedo ir al gimnasio o cuando como demasiado si voy de vacaciones.  </span>
                    </li>
                </ul>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="left">
                        <p data-xd="text18">Ya hemos vendido<br/>más de 20 mil unidades de</p>
                        <p><b>Eco</b>Slim</p>
                    </div>
                    <div class="product3">
                        <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/ecoslim2.png"/>
                    </div>
                    <div class="right">
                        <p data-xd="text19"><b> Date prisa y realiza tu pedido </b> a un precio reducido <b> ahora </b>
                            para conseguir una figura perfecta <b> en un par de semanas. </b></p>
                        <button class="toform"><span>Realizar pedido ahora</span></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_9">
            <div class="container">
                <div class="product">
                    <div>
                        <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/ecoslim.png"/>
                        <small><span><b>ES UN PRODUCTO</b> certificado</span></small>
                    </div>
                </div>
                <div class="form_pr">
                    <div class="form">
                        <!-- -->
                        <div class="form_2 s__of">
                            <p> Pide <span><b>  ECO  </b>  SLIM  </span> desde nuestra página </p>
                            <span class="old_price js_old_price">98</span>
                            <span class="new_price"><span class="js_new_price">49</span> <span class="js_curs">€ </span></span>
                            <span class="precent">50% de descuento</span>
                            <button class="toform"><span>Realizar pedido</span></button>
                        </div>
                        <!---->
                    </div>
                    <!-- -->
                    <img src="//st.acstnst.com/content/Eco_Slim_ES_new/images/block_1_water2.png"/>
                </div>
            </div>
        </div>
    </div>
    <!--  -->
    <link href="//st.acstnst.com/content/Eco_Slim_ES_new/css/styleSecond.css" rel="stylesheet"/>
    <div class="hidden-window">
        <div class="second-clips-header">
            <div class="containerz">
                <div class="h-w__inner">
                    <h1><b>Eco</b>
                        <small>Slim</small>
                    </h1>
                    <div class="yop-right__side">
                        <div class="dtable">
                            <div class="dtable-cell">Eco Slim - ¡el sistema adelgazante Nº1!<br/>¡Oferta única solo hoy!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__left">
                        <div class="w-h_vis_mob">
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 pachet
                                </option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 pachete
                                </option>
                                <option data-slide-index="2" value="5">3+2 pachete
                                </option>
                            </select>
                        </div>
                        <ul class="bx-bx">
                            <li class="item transitionmyl1" data-value="1">
                                <div class="item__left">
                                    <div class="js_changer change-package-button" data-package-id="1">
                                    </div>
                                    <div class="img-wrapp">
                                        <img alt="z1"
                                             src="//st.acstnst.com/content/Eco_Slim_ES_new/imagesSec/zit1.png"/>
                                    </div>
                                    <div class="zHeader">
<span>
</span>
                                    </div>
                                    <div class="pack_descr">¡Adelgazamiento exprés con un 50% de descuento! ¡Efecto
                                        inmediato!
                                    </div>
                                </div>
                                <div class="item__right">
                                    <div class="zHeader">
                                        <b>1
                                        </b> paquete
                                    </div>
                                    <div class="zPrices">
                                        <div class="dtable">
                                            <div class="dtr">
                                                <div class="dtable-cell fst">Precio
                                                </div>
                                                <div class="dtable-cell nowrap">
                                                    <b>
                                                        <text class="js-pp-new">49
                                                        </text>
                                                        €
                                                    </b>
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                    <div class="dtable">
                                                        <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">98
                                  </text>
</span>
                                                        </div>
                                                        <div class="dtable-cell text-left">
<span class="old-pr-descr">Precio anterior
                                </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dtr">
                                                <div class="dtable-cell fst padding-top10px">Envío
                                                </div>
                                                <div class="dtable-cell">
                                                    <b>
                                                        <text class="js-pp-ship">0
                                                        </text>
                                                        €
                                                    </b>
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="zSep">
                                        </div>
                                        <div class="dtable">
                                            <div class="dtr">
                                                <div class="dtable-cell fst fs17">Total:
                                                </div>
                                                <div class="dtable-cell prtotal">
                                                    <b>
                                                        <text class="js-pp-full">49
                                                        </text>
                                                    </b> €
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="zDiscount">
                                    <b>Atención
                                    </b>:
                                    <span>Beneficio 1000 euros
                    </span>
                                </div>
                            </li>
                            <li class="item hot transitionmyl1 active" data-value="3">
                                <div class="zstick">
                                </div>
                                <div class="item__left">
                                    <div class="js_changer change-package-button active" data-package-id="3">
                                    </div>
                                    <div class="img-wrapp">
                                        <img alt="z1"
                                             src="//st.acstnst.com/content/Eco_Slim_ES_new/imagesSec/zit2.png"/>
                                    </div>
                                    <div class="zHeader">
<span>
</span>
                                    </div>
                                    <div class="pack_descr">¡Resultados estables! El tratamiento de Eco Slim te ayudará
                                        a deshacerte del exceso de peso para siempre!
                                    </div>
                                </div>
                                <div class="item__right">
                                    <div class="zHeader sec">
                                        <b>2
                                        </b> paquetes
                                        <span class="dib zplus">un regalo
                      </span>
                                    </div>
                                    <div class="zPrices">
                                        <div class="dtable">
                                            <div class="dtr">
                                                <div class="dtable-cell fst">Precio
                                                </div>
                                                <div class="dtable-cell nowrap">
                                                    <b>
                                                        <text class="js-pp-new">98
                                                        </text>
                                                        €
                                                    </b>
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                    <div class="dtable">
                                                        <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">196
                                  </text>
</span>
                                                        </div>
                                                        <div class="dtable-cell text-left">
<span class="old-pr-descr">Precio anterior
                                </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dtr">
                                                <div class="dtable-cell fst padding-top10px">Envío
                                                </div>
                                                <div class="dtable-cell">
                                                    <b>
                                                        <text class="js-pp-ship">0
                                                        </text>
                                                        €
                                                    </b>
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="zSep">
                                        </div>
                                        <div class="dtable">
                                            <div class="dtr">
                                                <div class="dtable-cell fst fs17">Total:
                                                </div>
                                                <div class="dtable-cell prtotal">€
                                                    <b>
                                                        <text class="js-pp-full">98
                                                        </text>
                                                    </b>
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="zDiscount">
                                    <b>Atención
                                    </b>:
                                    <span>Hay un 50 % de descuento
                    </span>
                                </div>
                            </li>
                            <li class="item transitionmyl1" data-value="5">
                                <div class="item__left">
                                    <div class="js_changer change-package-button" data-package-id="5">
                                    </div>
                                    <div class="img-wrapp">
                                        <img alt="z1"
                                             src="//st.acstnst.com/content/Eco_Slim_ES_new/imagesSec/zit3_n3.png"/>
                                    </div>
                                    <div class="zHeader">
<span>
</span>
                                    </div>
                                    <div class="pack_descr">¡Enfoque profesional! ¡Un tratamiento de medio año con Eco
                                        Slim te ayudará incluso en casos de obesidad!
                                        ¡Una transformación real! Resultados garantizados.
                                    </div>
                                </div>
                                <div class="item__right">
                                    <div class="zHeader sec">
                                        <b>3
                                        </b> paquetes
                                        <span class="dib zplus sec">dos regalos
                      </span>
                                    </div>
                                    <div class="zPrices">
                                        <div class="dtable">
                                            <div class="dtr">
                                                <div class="dtable-cell fst">Precio
                                                </div>
                                                <div class="dtable-cell nowrap">
                                                    <b>
                                                        <text class="js-pp-new">147
                                                        </text>
                                                        €
                                                    </b>
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                    <div class="dtable">
                                                        <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i>
</i>
<text class="js-pp-old">294
                                  </text>
</span>
                                                        </div>
                                                        <div class="dtable-cell text-left">
<span class="old-pr-descr">Precio anterior
                                </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dtr">
                                                <div class="dtable-cell fst padding-top10px">Envío
                                                </div>
                                                <div class="dtable-cell">
                                                    <b>
<span>
<text class="js-pp-ship">0
                                </text>
</span> €
                                                    </b>
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="zSep">
                                        </div>
                                        <div class="dtable">
                                            <div class="dtr">
                                                <div class="dtable-cell fst fs17">Total:
                                                </div>
                                                <div class="dtable-cell prtotal">
                                                    <b>
                                                        <text class="js-pp-full">147
                                                        </text>
                                                    </b> €
                                                </div>
                                                <div class="dtable-cell text-center mw30">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="zDiscount">
                                    <b>Atención
                                    </b>:
                                    <span>Hay un 50 % de descuento
                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Pago al ser entregado
                            </div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>1
                                        </i>
                                        <img alt="alt1"
                                             src="//st.acstnst.com/content/Eco_Slim_ES_new/imagesSec/dtc1.png"/>
                                    </div>
                                    <div class="dtable-cell">El pedido
                                    </div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>2
                                        </i>
                                        <img alt="alt1"
                                             src="//st.acstnst.com/content/Eco_Slim_ES_new/imagesSec/dtc2.png"/>
                                    </div>
                                    <div class="dtable-cell">Envío
                                    </div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right">
                                        <i>3
                                        </i>
                                        <img alt="alt1"
                                             src="//st.acstnst.com/content/Eco_Slim_ES_new/imagesSec/dtc3.png"/>
                                    </div>
                                    <div class="dtable-cell">Entrega y
                                        <span>pago
                      </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Transacción segura
                        </div>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAJlFgAAAAAAAAAAAATf00i9AA"></iframe>
                            <!--
                            <input type="hidden" name="total_price" value="280">
                            <input type="hidden" name="esub" value="-4AAEvAgoXAQAAAAEAAqoWAQACti0CBgEBAAQRC0cOAA">
                            <input type="hidden" name="goods_id" value="217">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Eco_Slim_RO1">
                            <input type="hidden" name="price" value="280">
                            <input type="hidden" name="old_price" value="280">
                            <input type="hidden" name="total_price_wo_shipping" value="280">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 140, 'old_price': 280, 'shipment_price': 0}, u'3': {'price': 280, 'old_price': 560, 'shipment_price': 0}, u'5': {'price': 420, 'old_price': 840, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Lei">
                            <input type="hidden" name="package_id" value="3">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="PL">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.09">

                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="RO">România</option>
                                <option value="PL" selected="selected">Poland</option></select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 pachet</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 pachete</option>
                                <option data-slide-index="2" value="5">3+2 pachete</option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Introdu numele" type="text" value="">
                            <input class="only_number inp j-inp" name="phone" placeholder="Introdu numărul de telefon" type="text" value="">

                            <div class="text-center totalpriceForm">Livrare: 0 Lei</div>
                            <div class="text-center totalpriceForm">total:<br> <span class="js_total_price js_full_price">280</span> Lei</div>
                            <div class="zbtn js_submit transitionmyl" style="cursor:pointer"> Comandă </div>
                            <input type="hidden" name="time_zone" value="1">
                           -->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Te garantizamos
                            </div>
                            <ul>
                                <li>
                                    <b>100%
                                    </b> calidad
                                </li>
                                <li>
                                    <b>Comprobar
                                    </b> el producto tras recibirlo
                                </li>
                                <li>
<span>Seguridad
                    </span>de tus datos
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!--  -->
    </body>
    </html>
<?php } ?>