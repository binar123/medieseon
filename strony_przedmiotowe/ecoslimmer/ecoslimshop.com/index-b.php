<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>
    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 11702 -->
        <script async="" src="//connect.facebook.net/en_US/fbevents.js"></script>
        <script>var locale = "ro";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4AAEvAgoXAQAAAAEAAqoWAQACti0CBgEBAAQRC0cOAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript"
                src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height: 0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }

            .ac_footer a {
                color: #A12000;
            }

            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {
                "1": {"price": 140, "old_price": 280, "shipment_price": 0},
                "3": {"price": 280, "old_price": 560, "shipment_price": 0},
                "5": {"price": 420, "old_price": 840, "shipment_price": 0}
            };
            var shipment_price = 0;
            var name_hint = 'Toma Remus';
            var phone_hint = '+40740525322';

            $(document).ready(function () {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ro");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet"
              media="all">


        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', '//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript>&lt;img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=1666009176948198&amp;ev=PageView&amp;noscript=1"
            /&gt;</noscript>
        <!-- End Facebook Pixel Code -->


        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>EcoSlim</title>
        <link href="//st.acstnst.com/content/Eco_Slim_RO1/mobile/css/style.css" rel="stylesheet">
        <script src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/js/jquery.bxslider.min.js"
                type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/js/common.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.block_8 ul').bxSlider({
                    adaptiveHeight: true,
                    infiniteLoop: true,
                    touchEnabled: true,
                    controls: false
                });
            });
        </script>
    </head>
    <body>
    <div class="main first-page body">
        <div class="block_1">
            <div class="container">
                <div class="title">
                    <p>Descoperirea inovativă a
                        <b>oamenilor de știință</b></p>
                    <p><span class="logo"><b>Eco</b>Slim</span></p>
                </div>
                <ul>
                    <li>SLĂBIRE SIGURĂ</li>
                    <li>SCĂDEREA GREUTĂȚII CU 10-12 KG ÎNTR-O LUNĂ</li>
                </ul>
                <div class="product">
                    <img src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/ecoslim.png">
                </div>
                <button class="jsOpenWindow"><span>  Comandă  </span></button>
            </div>
        </div>
        <div class="block_2">
            <div class="container">
                <p> Dezvoltat pe baza <b> vitaminelor B </b> care accelerează DESCOMPUNEREA GRĂSIMII </p>
                <ul>
                    <li><span>Minus 10-12 kg într-o lună</span></li>
                    <li><span>Nu afectează inima și sistemul nervos</span></li>
                    <li><span>Compoziție 100% naturală</span></li>
                    <li><span>Impact precis asupra depozitelor de grăsime</span></li>
                    <li><span>Calitate garantată, perfect sigur pentru mediu</span></li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_3">
            <div class="container">
                <div class="why">
                    <h3>De ce te îngrași?</h3>
                    <p>Produsele pe care le cumperi din magazin conțin agenți de conservare artificiali, arome sintetice
                        și coloranți. Aceste componente chimice provoacă probleme cu digestia, duc la constituirea
                        depozitelor de grăsime și sistemele organismului
                        se congestionează cu reziduuri.</p>
                </div>
                <div class="warning">
                    <h3>În 90% din cazuri, excesul de greutate este o consecință directă a alimentației nenaturale!</h3>
                </div>
            </div>
        </div>
        <div class="block_4">
            <div class="top">
                <div class="container">
                    <h3>Corpul tău are nevoie de un asistent!</h3>
                    <div class="signature"><span>COMENTARIUL UNUI SPECIALIST ÎN NUTRIȚIE, Cora Tonca</span></div>
                    <div class="letter">
                        <p> «Pentru a-i permite corpului tău să se curețe, dizolvă o picături <b> EcoSlim </b> într-un
                            pahar cu apă și bea-l în timpul sau după masă o dată pe zi. Vei simți curând diferența – în
                            doar o săptămână greutatea
                            se va reduce, problemele cu stomacul și digestia dispar. Ceea ce e liniștitor în mod
                            special: compoziția unică a fost concepută în colaborare cu Institutul de Nutriție.
                            Complexele naturale de vitamine sunt mai eficace și mai
                            sigure decât orice componente chimice. Combinația de taurină, cafeină și extract de guarana
                            poate fi considerată cu încredere un cadou adevărat pentru toți cei care slăbesc, din moment
                            ce acest complex <b> descompune depozitele de grăsime din zonele cu probleme în câteva
                                zile.</b>»
                        </p>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <h3>Eco Slim</h3>
                    <p>E UN PRODUS 100% NATURAL ȘI PUR DIN PUNCT DE VEDERE ECOLOGIC</p>
                </div>
            </div>
        </div>
        <div class="block_5">
            <div class="top">
                <div class="container">
                    <h3>Compoziție unică selectată pentru a obține rezultatul maxim:</h3>
                    <ul>
                        <li><span>Cafeină</span></li>
                        <li><span>L-carnitină</span></li>
                        <li><span>Chitosan</span></li>
                        <li><span>Extract de guarana</span></li>
                        <li><span>Extract de alge marine brune</span></li>
                        <li><span>Vitamine В2, В5, В6, В8, В12</span></li>
                        <li><span>Acid succinic</span></li>
                        <li><span>Extract de urzică indiană</span></li>
                    </ul>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <h3>Eco Slim</h3>
                    <p>Creat în conformitate cu tehnologia europeană</p>
                </div>
            </div>
        </div>
        <div class="block_8">
            <div class="container">
                <h3>Recenziile clienților noștri</h3>
                <div class="bx-wrapper" style="max-width: 100%;">
                    <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 432px;">
                        <ul style="width: 415%; position: relative; transition-duration: 0s; transform: translate3d(-290px, 0px, 0px);">
                            <li style="float: left; list-style: none; position: relative; width: 290px;"
                                class="bx-clone">
<span>
<img src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/block_8_li2.jpg">
<b>Tomas Lucaci</b>
							32 de ani
						</span>
                                <span>Să fiu sincer, am slăbit de dragul fiicei mele, deoarece nu puteam alerga și mă joc cu ea, era dificil chiar și mă să aplec. Am slăbit mai mult de 10 kilograme ușor și după aceea am început să mă duc la sală.   <b>  Eco Slim e un produs excelent.  </b>   Încă îl beau, când nu mă duc la antrenament sau mănânc prea mult în vacanțe.</span>
                            </li>
                            <li style="float: left; list-style: none; position: relative; width: 290px;">
<span>
<img src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/block_8_li1.jpg">
<b>Corina Horje</b>
							24 de ani
						</span>
                                <span>Să fii gras toată viața și să devii slab într-o lună – e super!   <b>  Sunt foarte mulțumită de produsul Eco Slim.  </b>   E delicios, apetitul e redus și senzația de greutate din stomac dispare. Acesta e primul remediu care a dus la slăbitul real a 13 kilograme! La început greutatea s-a topit încet, apoi a început să scadă cu 1 kilogram pe zi.</span>
                            </li>
                            <li style="float: left; list-style: none; position: relative; width: 290px;">
<span>
<img src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/block_8_li2.jpg">
<b>Tomas Lucaci</b>
							32 de ani
						</span>
                                <span>Să fiu sincer, am slăbit de dragul fiicei mele, deoarece nu puteam alerga și mă joc cu ea, era dificil chiar și mă să aplec. Am slăbit mai mult de 10 kilograme ușor și după aceea am început să mă duc la sală.   <b>  Eco Slim e un produs excelent.  </b>   Încă îl beau, când nu mă duc la antrenament sau mănânc prea mult în vacanțe.</span>
                            </li>
                            <li style="float: left; list-style: none; position: relative; width: 290px;"
                                class="bx-clone">
<span>
<img src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/block_8_li1.jpg">
<b>Corina Horje</b>
							24 de ani
						</span>
                                <span>Să fii gras toată viața și să devii slab într-o lună – e super!   <b>  Sunt foarte mulțumită de produsul Eco Slim.  </b>   E delicios, apetitul e redus și senzația de greutate din stomac dispare. Acesta e primul remediu care a dus la slăbitul real a 13 kilograme! La început greutatea s-a topit încet, apoi a început să scadă cu 1 kilogram pe zi.</span>
                            </li>
                        </ul>
                    </div>
                    <div class="bx-controls bx-has-pager">
                        <div class="bx-pager bx-default-pager">
                            <div class="bx-pager-item"><a href="" data-slide-index="0"
                                                          class="bx-pager-link active">1</a></div>
                            <div class="bx-pager-item"><a href="" data-slide-index="1" class="bx-pager-link">2</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="left">
                        <p>Am vândut deja mai mult de 20 de mii de pachete de <b>EcoSlim</b></p>
                    </div>
                    <div class="product3">
                        <img height="200" src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/ecoslim.png">
                    </div>
                    <div class="right">
                        <p><b> Grăbește-te și comandă </b> la un preț redus <b> acum </b> pentru a obține un corp
                            perfect <b> în două săptămâni! </b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_9">
            <div class="container">
                <p>comanda acum</p>
                <div class="form_pr">
                    <div class="price">
                        <span class="precent">reducere <span>50%</span></span>
                        <span class="old_price">280</span>
                        <span class="new_price"><span>140</span> <span>Lei</span></span>
                    </div>
                    <div class="form">
                        <form action="/order/create/" method="post"><input type="hidden" name="total_price" value="280">
                            <input type="hidden" name="esub" value="-4AAEvAgoXAQAAAAEAAqoWAQACti0CBgEBAAQRC0cOAA">
                            <input type="hidden" name="goods_id" value="217">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Eco_Slim_RO1">
                            <input type="hidden" name="price" value="280">
                            <input type="hidden" name="old_price" value="280">
                            <input type="hidden" name="total_price_wo_shipping" value="280">
                            <input type="hidden" name="package_prices"
                                   value="{u'1': {'price': 140, 'old_price': 280, 'shipment_price': 0}, u'3': {'price': 280, 'old_price': 560, 'shipment_price': 0}, u'5': {'price': 420, 'old_price': 840, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Lei">
                            <input type="hidden" name="package_id" value="3">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="PL">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.09">

                            <button class="jsOpenWindow"><span>  Comandă  </span></button>
                            <input type="hidden" name="time_zone" value="1"></form>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_10">
            <div class="container"></div>
        </div>
    </div>
    <link href="//st.acstnst.com/content/Eco_Slim_RO1/mobile/css/stylesecond.css" rel="stylesheet">
    <div class="hidden-window">
        <div class="containerz">
            <section class="h-w__inner">
                <div class="h-w__header">
                    <span>Eco Slim</span> <br>Garantăm 100% calitate
                </div>
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader"><b>PLATA LA LIVRARE</b>
                            </div>
                            <div class="zContent">
                                <div class="dtable text-center">
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrr"
                                                               src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/imageSecond/Zarr.png"><img
                                                    alt="alt1"
                                                    src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/imageSecond/dtc1.png">
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="alt1"
                                                               src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/imageSecond/dtc2.png">
                                            </div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class=""><img alt="arrow" class="zarrrLeft"
                                                               src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/imageSecond/Zarr.png"><img
                                                    alt="alt1"
                                                    src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/imageSecond/dtc3.png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dtr">
                                        <div class="dtable-cell">
                                            <div class="">COMANDĂ</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">TRANSPORT</div>
                                        </div>
                                        <div class="dtable-cell">
                                            <div class="">LIVRARE ȘI <span>PLATĂ</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-block">
                            <div class="text-center selecttext"> Comandă</div>
                            <select class="corbselect select inp select_count change-package-selector"
                                    name="count_select">
                                <option data-slide-index="0" value="1">1 pachet</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 pachete</option>
                                <option data-slide-index="2" value="5">3+2 pachete</option>
                            </select>
                        </div>
                        <div class="epicslider">
                            <ul class="bx-bx">
                                <li data-value="1">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right side-right--columns">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile"></div>
                                                    <img alt="zt1"
                                                         src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/imageSecond/zit1.png">
                                                </div>
                                                <div class="zHeader" style="padding-top: 30px;"><b>1</b> pachet</div>
                                            </div>
                                            <div class="pack_descr pack_descr--columns">1 pachet la reducere 50%!</div>
                                        </div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Preț:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">140</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>280</div>
                                                Lei
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="3">
                                    <div class="zItem list transitionmyl">
                                        <div class="double-sider cf">
                                            <div class="side-right side-right--columns">
                                                <div class="ssimgabs">
                                                    <div class="giftmobile"></div>
                                                    <img alt="zt1"
                                                         src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/imageSecond/zit2.png">
                                                </div>
                                                <div class="zHeader"><b>2</b> pachete
                                                    <br> <span class="dib zplus">un cadou</span></div>
                                            </div>
                                            <div class="pack_descr pack_descr--columns">Uită de sforăit! Pentru a
                                                consolida efectul, Nasal Strips trebuie să fie folosit în mod regulat!
                                                Comandă un curs complet la un preț plăcut! 3 pachete de Nasal Strips la
                                                preț de două.
                                            </div>
                                        </div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Preț:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">280</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>560</div>
                                                Lei
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-value="5">
                                    <div class="zItem list transitionmyl ">
                                        <div class="side-right side-right--columns">
                                            <div class="ssimgabs">
                                                <div class="giftmobile"></div>
                                                <img alt="zt1"
                                                     src="//st.acstnst.com/content/Eco_Slim_RO1/mobile/img/imageSecond/zit3.png">
                                            </div>
                                            <div class="zHeader"><b>3</b> pachete
                                                <br> <span class="dib zplus sec">2 cadouri</span></div>
                                        </div>
                                        <div class="pack_descr pack_descr--columns">Cursul extensiv te va ajuta să scapi
                                            chiar și de cel mai zgomotos sforăit odată pentru totdeauna! 5 pachete la
                                            preț de trei.
                                        </div>
                                        <div class="dtable">
                                            <div class="dtable-cell red text-right">Preț:</div>
                                            <div class="dtable-cell">
                                                <div class="js_pack_price">420</div>
                                            </div>
                                            <div class="dtable-cell currencycell">
                                                <div class="pack_old_price"><i></i>840</div>
                                                Lei
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div id="bx-pager"><a class="change-package-button" data-package-id="1" data-slide-index="0"
                                                  href="">1</a><a class="change-package-button" data-package-id="3"
                                                                  data-slide-index="1" href="">3</a><a
                                    class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
                            </div>
                        </div>
                        <form action="" method="post">
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAIKFwAAAAAAAAAAAAQKz3kXAA"></iframe>
                            <!--
                            <input type="hidden" name="total_price" value="280">
                            <input type="hidden" name="esub" value="-4AAEvAgoXAQAAAAEAAqoWAQACti0CBgEBAAQRC0cOAA">
                            <input type="hidden" name="goods_id" value="217">
                            <input type="hidden" name="title" value="">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Eco_Slim_RO1">
                            <input type="hidden" name="price" value="280">
                            <input type="hidden" name="old_price" value="280">
                            <input type="hidden" name="total_price_wo_shipping" value="280">
                            <input type="hidden" name="package_prices" value="{u'1': {'price': 140, 'old_price': 280, 'shipment_price': 0}, u'3': {'price': 280, 'old_price': 560, 'shipment_price': 0}, u'5': {'price': 420, 'old_price': 840, 'shipment_price': 0}}">
                            <input type="hidden" name="currency" value="Lei">
                            <input type="hidden" name="package_id" value="3">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="PL">
                            <input type="hidden" name="shipment_vat" value="0.2">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.09">

                            <select class="select inp" id="country_code_selector" name="country_code">
                                <option value="RO">România</option>
                                <option value="PL" selected="selected">Poland</option></select>
                            <select class="select inp select_count change-package-selector" name="count_select">
                                <option data-slide-index="0" value="1">1 pachet</option>
                                <option data-slide-index="1" selected="selected" value="3">2+1 pachete</option>
                                <option data-slide-index="2" value="5">3+2 pachete</option>
                            </select>
                            <input class="inp j-inp" name="name" placeholder="Introdu numele" type="text" value="">
                            <input class="only_number inp j-inp" name="phone" placeholder="Introdu numărul de telefon" type="text" value="">

                            <div class="text-center totalpriceForm">Livrare: 0 Lei</div>
                            <div class="text-center totalpriceForm">total:<br> <span class="js_total_price js_full_price">280</span> Lei</div>
                            <div class="zbtn js_submit transitionmyl" style="cursor:pointer"> Comandă </div>
                            <input type="hidden" name="time_zone" value="1">
                           -->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">GARANTĂM</div>
                            <ul>
                                <li><b>100%</b> calitate</li>
                                <li><b>Verificarea </b> produsului la primire</li>
                                <li><b>Siguranța </b> datelor tale</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 11702 -->
        <script async="" src="//connect.facebook.net/en_US/fbevents.js"></script>
        <script>var locale = "ro";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4AAEvAgoXAQAAAAEAAqoWAQACti0CBgEBAAQRC0cOAA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript"
                src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height: 0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }

            .ac_footer a {
                color: #A12000;
            }

            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {
                "1": {"price": 140, "old_price": 280, "shipment_price": 0},
                "3": {"price": 280, "old_price": 560, "shipment_price": 0},
                "5": {"price": 420, "old_price": 840, "shipment_price": 0}
            };
            var shipment_price = 0;
            var name_hint = 'Toma Remus';
            var phone_hint = '+40740525322';

            $(document).ready(function () {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ro");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet"
              media="all">


        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', '//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript>&lt;img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=1666009176948198&amp;ev=PageView&amp;noscript=1"
            /&gt;</noscript>
        <!-- End Facebook Pixel Code -->


        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title> EcoSlim </title>
        <link href="//st.acstnst.com/content/Eco_Slim_RO1/css/main.css" rel="stylesheet">
        <script src="//st.acstnst.com/content/Eco_Slim_RO1/cdn/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.toform').click(function () {
                    $("html, body").animate({
                        scrollTop: $(".block_9 form").offset().top - 300
                    }, 1000);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <div class="main body">
        <div class="block_1">
            <div class="elements">
                <div class="container">
                    <div class="title">
                        <p> Descoperirea inovativă a <b> oamenilor de știință </b></p>
                        <p><span class="logo"><b>  Eco  </b>  Slim  </span></p>
                    </div>
                    <ul>
                        <li> SLĂBIRE SIGURĂ</li>
                        <li> SCĂDEREA GREUTĂȚII CU 10-12 KG ÎNTR-O LUNĂ</li>
                    </ul>
                    <div class="product">
                        <div>
                            <img src="//st.acstnst.com/content/Eco_Slim_RO1/images/ecoslim.png">
                            <small><span><b>2016</b>  Dezvoltare nou-nouță  </span></small>
                        </div>
                    </div>
                    <div class="form_pr">
                        <div class="form">
                            <p> Comandă <span><b>  ECO  </b>  SLIM  </span> de la noi </p>
                            <button class="jsOpenWindow"><span>  Comandă  </span></button>
                            <div class="price">
                                <div>
                                    <span class="old_price js_old_price">280</span>
                                    <span class="new_price"><span class="js_new_price">140</span> <span class="js_curs">  Lei  </span></span>
                                    <span class="precent">  50%   <span
                                            class="js_percent_sign">  reducere  </span></span>
                                </div>
                            </div>
                        </div>
                        <img src="//st.acstnst.com/content/Eco_Slim_RO1/images/block_1_water2.png">
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <p> Dezvoltat pe baza <b> vitaminelor B </b> care accelerează DESCOMPUNEREA GRĂSIMII </p>
                </div>
            </div>
        </div>
        <div class="block_2">
            <div class="container">
                <ul>
                    <li><span>  Minus 10-12 kg într-o lună  </span></li>
                    <li><span>  Nu afectează inima și sistemul nervos  </span></li>
                    <li><span>  Calitate garantată, perfect sigur pentru mediu  </span></li>
                    <li><span>  Compoziție 100% naturală  </span></li>
                    <li><span>  Impact precis asupra depozitelor de grăsime  </span></li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_3">
            <div class="container">
                <div class="woman">
                    <img src="//st.acstnst.com/content/Eco_Slim_RO1/images/block_3_woman.png">
                </div>
                <img class="food" src="//st.acstnst.com/content/Eco_Slim_RO1/images/block_3_food3.png">
                <div class="text">
                    <div class="why">
                        <h3> De ce te îngrași? </h3>
                        <p> Ți s-a spus cel puțin o dată că motivele principale ale excesului de greutate sunt
                            dereglările hormonale, stilul de viață sedentar și ecologia precară? Dar aceștia sunt doar
                            factori indirecți. </p>
                    </div>
                    <div class="warning">
                        <h3> În 90% din cazuri, excesul de greutate este o consecință directă a alimentației
                            nenaturale! </h3>
                        <p> Produsele pe care le cumperi din magazin conțin agenți de conservare artificiali, arome
                            sintetice și coloranți. Aceste componente chimice provoacă probleme cu digestia, duc la
                            constituirea depozitelor de grăsime și sistemele organismului se congestionează
                            cu reziduuri. </p>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_4">
            <div class="top">
                <div class="container">
                    <h3> Corpul tău are nevoie de <font> un asistent! </font></h3>
                    <div class="letter">
                        <p> Comentariul unui specialist în nutriție </p>
                        <p> «Pentru a-i permite corpului tău să se curețe, dizolvă o picături <b> EcoSlim </b> într-un
                            pahar cu apă și bea-l în timpul sau după masă o dată pe zi. Vei simți curând diferența – în
                            doar o săptămână greutatea se va reduce,
                            problemele cu stomacul și digestia dispar. Ceea ce e liniștitor în mod special: compoziția
                            unică a fost concepută în colaborare cu Institutul de Nutriție. Complexele naturale de
                            vitamine sunt mai eficace și mai sigure decât orice componente chimice.
                            Combinația de taurină, cafeină și extract de guarana poate fi considerată cu încredere un
                            cadou adevărat pentru toți cei care slăbesc, din moment ce acest complex <b> descompune
                                depozitele de grăsime din zonele cu probleme în câteva zile.</b>»
                        </p>
                        <div class="signature"> Cora Tonca</div>
                    </div>
                    <img src="//st.acstnst.com/content/Eco_Slim_RO1/images/block_4_doc.png">
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="eko">
                        <div>
                            <h3><b> Eco </b> Slim </h3>
                            <p> e un produs 100% natural <br> și pur din punct de vedere ecologic </p>
                        </div>
                    </div>
                    <div class="woman">
                        <img src="//st.acstnst.com/content/Eco_Slim_RO1/images/block_4_woman.png">
                    </div>
                    <div class="eko2">
                        <div><span>  Fără substanțe chimice și sintetice!  </span></div>
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <ul class="list">
                <li></li>
                <li></li>
            </ul>
        </div>
        <div class="block_5">
            <div class="top">
                <div class="container">
                    <h3><font> Compoziție unică selectată </font> pentru a obține rezultatul maxim: </h3>
                    <ul>
                        <li><span>  L-carnitină  </span></li>
                        <li><span>  Cafeină  </span></li>
                        <li><span>  Chitosan  </span></li>
                        <li><span>  Extract de   <br>  urzică indiană  </span></li>
                        <li><span>  Extract de guarana  </span></li>
                        <li><span>  Acid succinic  </span></li>
                        <li><span>  Extract de   <br>  alge marine brune  </span></li>
                        <li><span>  Vitamine   <br>  В2, В5, В6, В8, В12  </span></li>
                    </ul>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <p><b> Eco </b> Slim </p>
                    <div class="approved"> Creat în conformitate cu <br> tehnologia europeană</div>
                    <div class="stamp"> Aprobat de
                        <small> inspecția sanitară</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_6">
            <div class="container">
                <h3> Deficiența de minerale devine motivul excesului de greutate! </h3>
                <p> Poți crede că plantele <br> nu au suficientă putere de a arde grăsimile <br><b> Dar adevărurile
                        științifice ne spun altfel! </b></p>
                <ul>
                    <li>
                        <small> B2</small>
                        <span><b>  Normalizează  </b>   echilibrul hormonal din organism  </span></li>
                    <li>
                        <small> B6</small>
                        <span><b>  Reglează  </b>   procesele metabolice, fortifică pielea, părul, unghiile  </span>
                    </li>
                    <li>
                        <small> B12</small>
                        <span><b>  Ajustează  </b>   metabolismul, ajută la asimilarea elementelor nutritive  </span>
                    </li>
                    <li>
                        <small> B5</small>
                        <span><b>  Contribuie la  </b>   descompunerea carbohidraților și a grăsimilor  </span></li>
                    <li>
                        <small> B8</small>
                        <span><b>  Reduce  </b>   colesterolul, reglează funcționarea tractului gastrointestinal  </span>
                    </li>
                    <li>
                        <small> Taurina</small>
                        <span><b>  Declanșează  </b>   procesele metabolice, arde grăsimile  </span></li>
                    <li>
                        <small> Cofeina</small>
                        <small> Acidul succinic</small>
                        <span><b>  Elimină toxinele și lichidul în exces,  </b>   arde grăsimea și declanșează procesul de înnoire a celulelor corpului.  </span>
                    </li>
                </ul>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_7">
            <div class="container">
                <div class="left">
                    <h3> Rezultatul tău <big> viitor: </big></h3>
                    <p> Doar 1 pahar de <b>EcoSlim</b> și corpul tău va începe să se schimbe în fiecare zi! </p>
                    <ul>
                        <li> PÂNĂ LA – 0,5 KG PE ZI</li>
                        <li> PÂNĂ LA – 3,5 KG PE SĂPTĂMÎNĂ</li>
                        <li> PÂNĂ LA – 10-12 KG PE LUNĂ</li>
                    </ul>
                    <div class="safely">
                        <b> Sigur </b> Nu afectează sistemele cardiovascular și nervos
                    </div>
                </div>
                <div class="right">
                    <div class="img">
                        <img src="//st.acstnst.com/content/Eco_Slim_RO1/images/block_7_woman.png">
                        <img class="product2" src="//st.acstnst.com/content/Eco_Slim_RO1/images/ecoslim2.png">
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="block_8">
            <div class="container">
                <h3><font> Recenziile </font> clienților noștri </h3>
                <ul>
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_RO1/images/block_8_li1.jpg">
<b>  Corina Horje  </b>
							  24 de ani
						</span>
                        <span>  Să fii gras toată viața și să devii slab într-o lună – e super!   <b>  Sunt foarte mulțumită de produsul Eco Slim.  </b>   E delicios, apetitul e redus și senzația de greutate din stomac dispare. Acesta e primul remediu care a dus la slăbitul real a 13 kilograme! La început greutatea s-a topit încet, apoi a început să scadă cu 1 kilogram pe zi.  </span>
                    </li>
                    <li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_RO1/images/block_8_li2.jpg">
<b>  Tomas Lucaci  </b>
							  32 de ani
						</span>
                        <span>  Să fiu sincer, am slăbit de dragul fiicei mele, deoarece nu puteam alerga și mă joc cu ea, era dificil chiar și mă să aplec. Am slăbit mai mult de 10 kilograme ușor și după aceea am început să mă duc la sală.   <b>  Eco Slim e un produs excelent.  </b>   Încă îl beau, când nu mă duc la antrenament sau mănânc prea mult în vacanțe.  </span>
                    </li>
                </ul>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="left">
                        <p> Am vândut deja <br> mai mult de 20 de mii de pachete de </p>
                        <p><b> Eco </b> Slim </p>
                    </div>
                    <div class="product3">
                        <img src="//st.acstnst.com/content/Eco_Slim_RO1/images/ecoslim2.png">
                    </div>
                    <div class="right">
                        <p><b> Grăbește-te și comandă </b> la un preț redus <b> acum </b> pentru a obține un corp
                            perfect <b> în două săptămâni! </b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_9">
            <div class="container">
                <div class="product">
                    <div>
                        <img src="//st.acstnst.com/content/Eco_Slim_RO1/images/ecoslim.png">
                        <small><span><b>  Produsul  </b>  este certificat  </span></small>
                    </div>
                </div>
                <div class="form_pr">
                    <div class="form">
                        <p> Comandă <span><b>  ECO  </b>  SLIM  </span> de la noi </p>
                        <button class="jsOpenWindow"><span>  Comandă  </span></button>
                        <div class="price">
                            <div>
                                <span class="old_price js_old_price">280</span>
                                <span class="new_price"><span class="js_new_price">140</span> <span class="js_curs">  Lei  </span></span>
                                <span class="precent">  50%   <span class="js_percent_sign">  reducere  </span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Second Page-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet"
          type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic"
        rel="stylesheet">
    <link href="//st.acstnst.com/content/Eco_Slim_RO1/css/styleSecond.css" rel="stylesheet">
    <script src="//st.acstnst.com/content/Eco_Slim_RO1/js/secondPage.js"></script>
    <!--/Second Page-->
    <div class="hidden-window">
        <div class="h-headerj">
            <div class="containerz clearfix"><span class="headerSecondPage">ecoslim</span></div>
        </div>
        <div class="containerz">
            <section class="h-w__inner">
                <div class="cf">
                    <div class="w-h__right">
                        <div class="w-h__right-row1">
                            <div class="zHeader">Plata <br> <b>la livrare</b></div>
                            <div class="zContent">
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>1</i><img alt="alt1"
                                                                                     src="//st.acstnst.com/content/Eco_Slim_RO1/imageSecond/dtc1.png">
                                    </div>
                                    <div class="dtable-cell">Comandă</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>2</i><img alt="alt1"
                                                                                     src="//st.acstnst.com/content/Eco_Slim_RO1/imageSecond/dtc2.png">
                                    </div>
                                    <div class="dtable-cell">Transport</div>
                                </div>
                                <div class="dtable">
                                    <div class="dtable-cell text-right"><i>3</i><img alt="alt1"
                                                                                     src="//st.acstnst.com/content/Eco_Slim_RO1/imageSecond/dtc3.png">
                                    </div>
                                    <div class="dtable-cell">Livrare și <span>plată</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="printbg">Un târg corect</div>
                        <form action="" id="sform" method="post">

                            <!--<input type="hidden"
                                                                                                 name="total_price"
                                                                                                 value="140.0">
                            <input type="hidden" name="esub" value="-4AAEvAgoXAQAAAAEAAqoWAQACti0CBgEBAAQRC0cOAA">-->
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdn.pro/forms/?target=-4AAIJIAIKFwAAAAAAAAAAAAQKz3kXAA"></iframe>


                            <!--   <input type="hidden" name="goods_id" value="217">
                               <input type="hidden" name="title" value="">
                               <input type="hidden" name="ip_city" value="Toruń">
                               <input type="hidden" name="template_name" value="Eco_Slim_RO1">
                               <input type="hidden" name="price" value="140">
                               <input type="hidden" name="old_price" value="280">
                               <input type="hidden" name="total_price_wo_shipping" value="140.0">
                               <input type="hidden" name="package_prices"
                                      value="{u'1': {'price': 140, 'old_price': 280, 'shipment_price': 0}, u'3': {'price': 280, 'old_price': 560, 'shipment_price': 0}, u'5': {'price': 420, 'old_price': 840, 'shipment_price': 0}}">
                               <input type="hidden" name="currency" value="Lei">
                               <input type="hidden" name="package_id" value="1">
                               <input type="hidden" name="protected" value="None">
                               <input type="hidden" name="ip_country_name" value="Poland">
                               <input type="hidden" name="country_code" value="PL">
                               <input type="hidden" name="shipment_vat" value="0.2">
                               <input type="hidden" name="ip_country" value="PL">
                               <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                               <input type="hidden" name="shipment_price" value="0">
                               <input type="hidden" name="price_vat" value="0.09">

                               <select class="select inp" id="country_code_selector" name="country_code">
                                   <option value="RO">România</option>
                                   <option value="PL" selected="selected">Poland</option>
                               </select>
                               <select class="select inp select_count change-package-selector" name="count_select">
                                   <option value="1">1 pachet</option>
                                   <option selected="selected" value="3">2+1 pachete</option>
                                   <option value="5">3+2 pachete</option>
                               </select>
                               <input class="inp j-inp" name="name" placeholder="Introdu numele" type="text" value="">
                               <input class="only_number inp j-inp" name="phone" placeholder="Introdu numărul de telefon"
                                      type="text" value="">
                               <div class="text-center totalpriceForm">total: <span
                                       class="js_total_price js_full_price">83 </span> Lei
                               </div>
                               <button class="zbtn js_submit transitionmyl"> Comandă</button>
                               <input type="hidden" name="time_zone" value="1">-->
                        </form>
                        <div class="zGarant">
                            <div class="zHeader">Garantăm</div>
                            <ul>
                                <li><b>100%</b> calitate</li>
                                <li><b>Verificarea </b> produsului la primire</li>
                                <li><b>Siguranța </b> datelor tale</li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-h__left">
                        <div class="item transitionmyl1">
                            <div class="zDiscount"><b>Atenție!</b>
                                <br><span>Există o reducere de 50%</span></div>
                            <div class="item__right">
                                <div class="zHeader"><b>1</b> pachet la reducere 50%</div>
                                <div class="zPrices">
                                    <!-- ЭТО НЕ ТРОГАЕМ!!!
                                                                        <div class="dtc ellipsDiscount">
                                                                        Anwendungen<br>
                                                                        <span class="js_discount_pack">1000</span> Lei
                                                                        </div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Preț</div>
                                            <div class="dtable-cell"><b class="js-pp-new">

                                                    140

                                                </b> <b>Lei</b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old">

280

</text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Preț vechi </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livrare</div>
                                            <div class="dtable-cell">
                                                <b class="js-pp-ship">

                                                    0

                                                </b>
                                                <b>Lei</b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full">

                                                        140

                                                    </text>
                                                </b>Lei
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button " data-package-id="1"></div>
                                <div class="img-wrapp"><img alt="z1" class="img"
                                                            src="//st.acstnst.com/content/Eco_Slim_RO1/imageSecond/1.png">
                                </div>
                                <div class="zHeader"><span>&nbsp;</span></div>
                                <text>1 pachet la reducere 50%</text>
                            </div>
                        </div>
                        <!--item2-->
                        <div class="item hot transitionmyl1 active">
                            <div class="zstick"></div>
                            <div class="zDiscount"><b>Atenție!</b>
                                <br><span>Există o reducere de 50%</span></div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>2</b> pachete <span class="dib zplus">un cadou</span></div>
                                <div class="zPrices">
                                    <!-- ЭТО НЕ ТРОГАЕМ
                                                                        <div class="dtc ellipsDiscount">
                                                                        Anwendungen<br>
                                                                        <span class="js_discount_pack">1000</span> Lei
                                                                        </div>-->
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst">Preț</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-new">

                                                        280

                                                    </text>
                                                    Lei
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i></i>
<span class="js-pp-old">

560

</span>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Preț vechi </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livrare</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship">

                                                        0

                                                    </text>
                                                    Lei
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
<span class="js-pp-full">

280
</span>
                                                </b> Lei
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button active" data-package-id="3"></div>
                                <div class="img-wrapp">
                                    <div class="gift">cadouri</div>
                                    <img alt="z1" src="//st.acstnst.com/content/Eco_Slim_RO1/imageSecond/2+1.png"></div>
                                <div class="zHeader"><span></span></div>
                                <text>Uită de sforăit! Pentru a consolida efectul, Nasal Strips trebuie să fie folosit
                                    în mod regulat! Comandă un curs complet la un preț plăcut! 3 pachete de Nasal Strips
                                    la preț de două.
                                </text>
                            </div>
                        </div>
                        <!--end of item2-->
                        <!--item3-->
                        <div class="item transitionmyl1">
                            <div class="zDiscount"><b>Atenție!</b>
                                <br><span>Există o reducere de 50%</span></div>
                            <div class="item__right">
                                <div class="zHeader sec"><b>3</b> pachete <span class="dib zplus sec">2 cadouri</span>
                                </div>
                                <div class="zPrices">
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst"> Preț</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-new">

                                                        420

                                                    </text>
                                                    Lei
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">
                                                <div class="dtable">
                                                    <div class="dtable-cell">
<span class="dib old-pricedecoration">
<i></i>
<text class="js-pp-old">

840

</text>
</span>
                                                    </div>
                                                    <div class="dtable-cell text-left"><span class="old-pr-descr">Preț vechi </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtr">
                                            <div class="dtable-cell fst padding-top10px">Livrare</div>
                                            <div class="dtable-cell">
                                                <b>
                                                    <text class="js-pp-ship">

                                                        0

                                                    </text>
                                                    Lei
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="zSep"></div>
                                    <div class="dtable">
                                        <div class="dtr">
                                            <div class="dtable-cell fst fs17">total:</div>
                                            <div class="dtable-cell prtotal">
                                                <b>
                                                    <text class="js-pp-full">

                                                        420

                                                    </text>
                                                    Lei
                                                </b>
                                            </div>
                                            <div class="dtable-cell text-center mw30">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item__left">
                                <div class="js_changer change-package-button" data-package-id="5"></div>
                                <div class="img-wrapp">
                                    <div class="gift th">cadouri</div>
                                    <img alt="z1" src="//st.acstnst.com/content/Eco_Slim_RO1/imageSecond/3+2.png"
                                         width="200"></div>
                                <div class="zHeader sm"><span>&nbsp;</span></div>
                                <text>Cursul extensiv te va ajuta să scapi chiar și de cel mai zgomotos sforăit odată
                                    pentru totdeauna! 5 pachete la preț de trei.
                                </text>
                            </div>
                        </div>
                        <!--end-of-item3-->
                    </div>
                </div>
            </section>
        </div>
    </div>


    </body>
    </html>
<?php } ?>