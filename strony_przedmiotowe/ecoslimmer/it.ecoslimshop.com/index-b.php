<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>

<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 11404 -->
    <script>var locale = "it";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQEvAnEWBMouDIgAAAEAAuwVAQACjCwCBgEBAAQgSKmfAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
    .ac_footer {
        position: relative;
        top: 10px;
        height:0;
        text-align: center;
        margin-bottom: 70px;
        color: #A12000;
    }
    .ac_footer a {
        color: #A12000;
    }
    img[height="1"], img[width="1"] {
        display: none !important;
    }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
        var shipment_price = 0;
        var name_hint = 'Bruno Bellini';
        var phone_hint = '+393476736735';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
            '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
            ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("it");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1666009176948198');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->



<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>EcoSlim</title>
<link href="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/css/main.css" rel="stylesheet"/>
<script src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/js/second_page.js" type="text/javascript"></script>
<script src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/js/jquery.bxslider.min.js" type="text/javascript"></script>
<script type="text/javascript">
     $(document).ready(function() {
       $('.toform').click(function(){
        $("html, body").animate({scrollTop: $("form").offset().top-300}, 1000);
        return false;
       });
	   $('.block_8 ul').bxSlider({
					adaptiveHeight: true,
					infiniteLoop: true,
					touchEnabled: true ,
					controls: false
     });
	 });
   </script>
</head>
<body>
<div class="main">
<div class="block_1">
<div class="container">
<div class="title">
<p>Un'innovativa <b>scoperta scientifica</b></p>
<p><span class="logo"><b>Eco</b>Slim</span></p>
</div>
<ul>
<li>DIMAGRIRE IN MODO SICURO</li>
<li>SI PERDONO DAI 10 AI 12 KG AL MESE</li>
</ul>
<div class="product">
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/images/ecoslim.png"/>
</div>
<button class="toform" data-scroll="true"><span>Ordina</span></button>
</div>
</div>
<div class="block_2">
<div class="container">
<p> Sviluppato sulla   <b>  base delle vitamine del gruppo B  </b>  che promuovono LA SCISSIONE DEI GRASSI</p>
<ul>
<li><span>Meno 10-12 kg al mese</span></li>
<li><span>Non intacca il cuore o il sistema nervoso</span></li>
<li><span>Qualità garantita, sicurezza ambientale completa</span></li>
<li><span>Composizione naturale al 100%</span></li>
<li><span>Impatto localizzato sui depositi di grasso</span></li>
</ul>
</div>
<div class="line"></div>
</div>
<div class="block_3">
<div class="container">
<div class="why">
<h3> Perché si ingrassa?</h3>
<p>Ti è stato detto più di una volta che le principali cause del sovrappeso sono disfunzioni ormonali, uno stile di vita sedentario e un ambiente nocivo? Ma questi sono solo fattori indiretti.</p>
</div>
<div class="warning">
<h3> Nel 90% dei casi, il peso in eccesso è una conseguenza diretta di un'alimentazione poco naturale! </h3>
</div>
</div>
</div>
<div class="block_4">
<div class="top">
<div class="container">
<h3> Il corpo ha bisogno <font> di un assistente!</font></h3>
<div class="signature"><span>Monica Giuliani</span></div>
<div class="letter">
<p> "Per permettere al corpo di purificarsi da solo, fate sciogliere alcune gocce di   <b>  EcoSlim  </b>   in un bicchiere d'acqua e bevetelo durante o dopo i pasti una volta al giorno. Presto sentirete la differenza. Già in una settimana, il peso diminuirà e i problemi allo stomaco e alla digestione scompariranno. La cosa più rassicurante è che questa composizione unica è stata sviluppata in collaborazione con l'Istituto di Nutrizione. I complessi vitaminici naturali sono molto più efficaci e sicuri di qualsiasi composto chimico. La combinazione di taurina, caffeina e estratto di guaranà può davvero essere considerata un dono del cielo per tutti coloro che cercano di dimagrire, dato che questo complesso   <b>  scinde i depositi di grasso delle aree problematiche nel giro di pochi giorni.  </b>  » </p>
</div>
</div>
</div>
<div class="bottom">
<div class="container">
<h3>Eco Slim</h3>
<p>è un prodotto   <br/>  al 100% naturale ed ecologicamente puro</p>
</div>
</div>
</div>
<div class="block_5">
<div class="top">
<div class="container">
<h3><font>Composizione accuratamente selezionata  </font>   per ottenere i migliori risultati possibili:</h3>
<ul>
<li><span>Caffeina</span></li>
<li><span>L-carnitina </span></li>
<li><span>Chitosano</span></li>
<li><span>Estratto di guaranà</span></li>
<li><span>Estratto di alghe Fucus</span></li>
<li><span>Vitamine В2, В5, В6, В8, В12</span></li>
<li><span>Acido succinico</span></li>
<li><span>Estratto di Coleus forskohlii</span></li>
</ul>
</div>
</div>
<div class="bottom">
<div class="container">
<h3>Eco Slim</h3>
<p>Progettato in accordo con le   <br/>  tecnologie europee</p>
</div>
</div>
</div>
<div class="block_8">
<div class="container">
<h3>Feedback dei nostri clienti</h3>
<ul>
<li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/images/block_8_li1.jpg"/>
<b> Martina Nibali</b>
							24 anni
						</span>
<span>Essere grassi per una vita intera e poi dimagrire in un mese, fantastico!   <b>  Sono molto contenta del prodotto EcoSlim.  </b>   Ha un ottimo gusto, riduce l'appetito e elimina il senso di pesantezza allo stomaco. Questo è il primo rimedio che mi ha permesso di perdere 13 kg! All'inizio ho perso poco peso, ma poi ho cominciato a perdere 1 kg al giorno.</span>
</li>
<li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/images/block_8_li2.jpg"/>
<b>Daniele Prandi </b>
							32 anni
						</span>
<span>Ad essere sinceri, sono dimagrito per mia figlia: non riuscivo a correre o a giocare con lei, mi era anche difficile piegarmi. Ho perso più di 10 kg e poi ho cominciato ad andare in palestra.   <b>  EcoSlim è un prodotto fantastico.  </b>   Lo bevo ancora quando non mi alleno o mangio troppo durante le feste.</span>
</li>
</ul>
</div>
<div class="bottom">
<div class="container">
<div class="left">
<p> Abbiamo già venduto più di 20.000 confezioni di <b>EcoSlim</b></p>
</div>
<div class="product3">
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/images/ecoslim2.png" width="150"/>
</div>
<div class="right">
<p>Affrettati ad ordinare     ad un prezzo scontato   <b>  ora  </b>  per avere un corpo perfetto   <b>  in un paio di settimane!  </b></p>
</div>
</div>
</div>
</div>
<div class="block_9">
<div class="container">
<p> <b>Ordina</b> ECO SLIM da noi</p>
<div class="form_pr">
<div class="price">
<span class="precent"><span class="js_percent_sign">  di sconto  </span>
<span class="shipment">+ spese di spedizione  </span>
<span class="old_price js_old_price">98</span>
<span class="new_price"><span class="js_new_price">49</span> <span class="js_curs">€</span></span>
</span></div>
<div class="form">
							<button data-scroll="true"><span>Ordine</span></button>
						</div>
</div>
</div>
</div>
</div>

<link href="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/css/styleSecond.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet"/>
<div class="hidden-window">
<div class="header_new">
<div class="containerz">
<div class="h-w__inner">
<div class="dtable">
<div class="dtable-cell text-center">
<span class="logo_s"><b>Eco</b>Slim</span>
</div>
</div>
</div>
</div>
</div>
<div class="containerz">
<section class="h-w__inner">
<div class="cf">
<div class="w-h__right">
<div class="w-h__right-row1">
<div class="zHeader">PAGAMENTO IN CONTRASSEGNO</div>
<div class="zContent">
<div class="dtable text-center">
<div class="dtr">
<div class="dtable-cell">
<div class=""><img alt="arrow" class="zarrr" src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/imagesSec/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/imagesSec/dtc1.png"/></div>
</div>
<div class="dtable-cell">
<div class=""><img alt="alt1" src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/imagesSec/dtc2.png"/></div>
</div>
<div class="dtable-cell">
<div class=""><img alt="arrow" class="zarrrLeft" src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/imagesSec/Zarr.png"/><img alt="alt1" src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/imagesSec/dtc3.png"/></div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell">
<div class="">ORDINE</div>
</div>
<div class="dtable-cell">
<div class="">SPEDIZIONE</div>
</div>
<div class="dtable-cell">
<div class="">CONSEGNA E <span>PAGAMENTO</span></div>
</div>
</div>
</div>
</div>
</div>
<div class="select-block">
<div class="text-center selecttext">OTTIMO AFFARE</div>
<select class="corbselect select inp select_count change-package-selector" name="count_select">
<option data-slide-index="0" value="1">1 confezione</option>
<option data-slide-index="1" selected="selected" value="3">2+1 confezioni</option>
<option data-slide-index="2" value="5">3+2 confezioni</option>
</select>
</div>
<div class="epicslider">
<ul class="bx-bx">
<li data-value="1">
<div class="zItem list transitionmyl">
<div class="side-right">
<div class="ssimgabs"><img alt="zt1" src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/imagesSec/zit1.png"/>
</div>
<div class="zHeader onepack"><b>1</b> confezione</div>
<br/>
</div>
<div class="pack_descr">Eco Slim - il sistema dimagrante №1!
Offerta unica, solo per oggi! Metodo di dimagrimento rapido al 50% di sconto! Effetto immediato!</div>
<div class="dtable" style="width: 85%;margin: 0 auto;">
<div class="dtable-cell red text-right">Prezzo</div>
<div class="dtable-cell">
<div class="js_pack_price">
<text class="js-pp-new"> 49 </text>
</div>
</div>
<div class="dtable-cell currencycell">
<div class="pack_old_price">
<i></i>
<text class="js-pp-old"> 98 </text></div>€</div>
</div>
</div>
</li>
<li data-value="3">
<div class="zItem list transitionmyl">
<div class="double-sider cf">
<div class="side-right">
<div class="ssimgabs">
<div class="giftmobile">+ regalo</div>
<img alt="zt1" src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/imagesSec/zit2.png"/>
</div>
<div class="zHeader"><b>2</b> confezioni
												<br/> <span class="dib zplus">regalo!</span></div>
</div>
</div>
<div class="pack_descr">Risultati stabili! Il trattamento Eco Slim vi aiuterà a sbarazzarvi del peso in eccesso per sempre! 3 confezioni di Eco Slim al prezzo di 2!</div>
<div class="dtable" style="width: 85%;margin: 0 auto;">
<div class="dtable-cell red text-right">Prezzo</div>
<div class="dtable-cell">
<div class="js_pack_price">
<text class="js-pp-new"> 98 </text>
</div>
</div>
<div class="dtable-cell currencycell">
<div class="pack_old_price">
<i></i>
<text class="js-pp-old"> 196 </text>
</div>
											€
										</div>
</div>
</div>
</li>
<li data-value="5">
<div class="zItem list transitionmyl ">
<div class="side-right">
<div class="ssimgabs">
<div class="giftmobile">+ regalo</div>
<img alt="zt1" src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/imagesSec/zit3.png"/>
</div>
<div class="zHeader"><b>3</b> confezioni
											<br/> <span class="dib zplus sec">regalo!</span></div>
</div>
<div class="pack_descr">Un approccio professionale! Il trattamento di sei mesi a base di Eco Slim vi aiuterà anche in caso di una grande quantità di chili in eccesso!Trasformazioni reali!
									</div>
<div class="dtable" style="width: 85%;margin: 0 auto;">
<div class="dtable-cell red text-right">Prezzo</div>
<div class="dtable-cell">
<div class="js_pack_price">
<text class="js-pp-new"> 147 </text>
</div>
</div>
<div class="dtable-cell currencycell">
<div class="pack_old_price">
<i></i>
<text class="js-pp-old"> 294 </text></div>€</div>
</div>
</div>
</li>
</ul>
</div>
<div id="bx-pager">
<a class="change-package-button" data-package-id="1" data-slide-index="0" href="">1</a>
<a class="change-package-button" data-package-id="3" data-slide-index="1" href="">3</a>
<a class="change-package-button" data-package-id="5" data-slide-index="2" href="">5</a>
</div>
<div class="form-second">
<form action="" class="js_scrollForm" method="post"><input type="hidden" name="total_price" value="49.0">
<input type="hidden" name="esub" value="-4A25sMQEvAnEWBMouDIgAAAEAAuwVAQACjCwCBgEBAAQgSKmfAA">
<input type="hidden" name="goods_id" value="217">
<input type="hidden" name="title" value="">
<input type="hidden" name="ip_city" value="Lublin">
<input type="hidden" name="template_name" value="Eco_Slim_IT_Green">
<input type="hidden" name="price" value="49">
<input type="hidden" name="old_price" value="98">
<input type="hidden" name="total_price_wo_shipping" value="49.0">
<input type="hidden" name="package_prices" value="{u'1': {'price': 49, 'old_price': 98, 'shipment_price': 0}, u'3': {'price': 98, 'old_price': 196, 'shipment_price': 0}, u'5': {'price': 147, 'old_price': 294, 'shipment_price': 0}}">
<input type="hidden" name="currency" value="€">
<input type="hidden" name="package_id" value="1">
<input type="hidden" name="protected" value="None">
<input type="hidden" name="ip_country_name" value="Poland">
<input type="hidden" name="country_code" value="IT">
<input type="hidden" name="shipment_vat" value="0.22">
<input type="hidden" name="ip_country" value="PL">
<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
<input type="hidden" name="shipment_price" value="0">
<input type="hidden" name="price_vat" value="0.1">
<div class="formHeader"><span>INSERISCI I TUOI DATI PER  </span>
<br/> EFFETTUARE L’ORDINE:
						</div>
            <iframe scrolling="no"
                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                    src="http://abcdg.pro/forms/?target=-4AAIJIAJxFgAAAAAAAAAAAAQUAkqqAA"></iframe>
<div class="text-center"></div>
<div class="text-center totalpriceForm shipmentt">+Spedizione: <b class="js_delivery_price">
							0 €</b>
</div>
<div class="text-center totalpriceForm">Totale: <span class="js_total_price js_full_price">49 </span> € </div>
</form>
</div>
<div class="zGarant">
<div class="zHeader">GARANTIAMO</div>
<ul>
<li>Qualità al <b>100%</b></li>
<li><b>Controllo </b> del prodotto una volta ricevuto</li>
<li><b>Sicurezza </b> dei tuoi dati</li>
</ul>
</div>
</div>
</div>
</section>
</div>
</div>
<script src="//st.acstnst.com/content/Eco_Slim_IT_Green/mobile/js/jquery.bxslider.min.js"></script>






</body>
</html>
<?php } else { ?>

<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 11404 -->
    <script>var locale = "it";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQEvAnEWBIDYC4gAAAEAAuwVAQACjCwCBgEBAASNqzhDAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
    .ac_footer {
        position: relative;
        top: 10px;
        height:0;
        text-align: center;
        margin-bottom: 70px;
        color: #A12000;
    }
    .ac_footer a {
        color: #A12000;
    }
    img[height="1"], img[width="1"] {
        display: none !important;
    }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {"1":{"price":49,"old_price":98,"shipment_price":0},"3":{"price":98,"old_price":196,"shipment_price":0},"5":{"price":147,"old_price":294,"shipment_price":0}};
        var shipment_price = 0;
        var name_hint = 'Bruno Bellini';
        var phone_hint = '+393476736735';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
            '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
            ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("it");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1666009176948198');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->



<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=0.75" name="viewport"/>
<title> EcoSlim </title>
<link href="//st.acstnst.com/content/Eco_Slim_IT_Green/css/main.css" rel="stylesheet"/>
<link href="//st.acstnst.com/content/Eco_Slim_IT_Green/css/second_page.css" rel="stylesheet"/>
<script src="//st.acstnst.com/content/Eco_Slim_IT_Green/js/secondPage.js" type="text/javascript"></script>
<script type="text/javascript">
     $(document).ready(function() {
       $('.toform').click(function(){
        $("html, body").animate({scrollTop: $(".block_9 form").offset().top-300}, 1000);
        return false;
       });
     });
   </script></head><body><p>


 <style>

  .price>div {
  	display: none;
  }

  .price-second {
  	display: block;
  }

   </style>




</p>
<div class="main">
<div class="block_1">
<div class="elements">
<div class="container">
<div class="title">
<p>  Un'innovativa   <b>  scoperta scientifica  </b></p>
<p><span class="logo"><b>  Eco  </b>  Slim  </span></p>
</div>
<ul>
<li>  DIMAGRIRE IN MODO SICURO  </li>
<li>  SI PERDONO DAI 10 AI 12 KG AL MESE  </li>
</ul>
<div class="product">
<div>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/ecoslim.png"/>
<small><span><b>2016</b>  Novità assoluta  </span></small>
</div>
</div>
<div class="form_pr">
<div class="form">

						    <div class="s__of">
<p class="defens-text">Ordina <span><b>ECO</b>SLIM</span> da noi</p>
<button data-scroll="true"><span>Ordina</span></button>
<div class="price-second">
<span class="old_price js_old_price">98</span>
<span class="new_price"><span class="js_new_price">49</span> <span class="js_curs"> €  </span></span>
<span class="precent">  50%   <span class="js_percent_sign">  di sconto  </span></span>
</div>
</div>

						</div>
<div class="price">
<div>
<span class="old_price js_old_price">98</span>
<span class="new_price"><span class="js_new_price">49</span> <span class="js_curs"> €  </span></span>
<span class="precent">  50%   <span class="js_percent_sign">  di sconto  </span></span>
</div>
</div>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/block_1_water2.png"/>
</div>
</div>
</div>
<div class="bottom">
<div class="container">
<p>  Sviluppato sulla   <b>  base delle vitamine del gruppo B  </b>  che promuovono LA SCISSIONE DEI GRASSI  </p>
</div>
</div>
</div>
<div class="block_2">
<div class="container">
<ul>
<li><span>  Meno 10-12 kg al mese  </span></li>
<li><span>  Non intacca il cuore o il sistema nervoso  </span></li>
<li><span>  Qualità garantita, sicurezza ambientale completa  </span></li>
<li><span>  Composizione naturale al 100%  </span></li>
<li><span>  Impatto localizzato sui depositi di grasso  </span></li>
</ul>
</div>
<div class="line"></div>
</div>
<div class="block_3">
<div class="container">
<div class="woman">
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/block_3_woman.png"/>
</div>
<img class="food" src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/block_3_food3.png"/>
<div class="text">
<div class="why">
<h3>  Perché si ingrassa?  </h3>
<p>  Ti è stato detto più di una volta che le principali cause del sovrappeso sono disfunzioni ormonali, uno stile di vita sedentario e un ambiente nocivo? Ma questi sono solo fattori indiretti.  </p>
</div>
<div class="warning">
<h3>  Nel 90% dei casi, il peso in eccesso è una conseguenza diretta di un'alimentazione poco naturale!  </h3>
<p>  I prodotti che si acquistano al supermercato contengono degli agenti conservanti artificiali, degli aromi sintetici e dei coloranti. Questi componenti chimici causano problemi alla digestione e fanno sì che i sistemi corporei siano congestionati dagli scarti. In questo modo, si formano i depositi di grasso.  </p>
</div>
</div>
</div>
<div class="line"></div>
</div>
<div class="block_4">
<div class="top">
<div class="container">
<h3>  Il corpo ha bisogno   <font>  di un assistente!  </font></h3>
<div class="letter">
<p>  Commento di uno specialista di dietologia  </p>
<p>  "Per permettere al corpo di purificarsi da solo, fate sciogliere alcune gocce di   <b>  EcoSlim  </b>   in un bicchiere d'acqua e bevetelo durante o dopo i pasti una volta al giorno. Presto sentirete la differenza. Già in una settimana, il peso diminuirà e i problemi allo stomaco e alla digestione scompariranno. La cosa più rassicurante è che questa composizione unica è stata sviluppata in collaborazione con l'Istituto di Nutrizione. I complessi vitaminici naturali sono molto più efficaci e sicuri di qualsiasi composto chimico. La combinazione di taurina, caffeina e estratto di guaranà può davvero essere considerata un dono del cielo per tutti coloro che cercano di dimagrire, dato che questo complesso   <b>  scinde i depositi di grasso delle aree problematiche nel giro di pochi giorni.  </b>  »  </p>
<div class="signature">  Monica Giuliani  </div>
</div>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/block_4_doc.png"/>
</div>
</div>
<div class="bottom">
<div class="container">
<div class="eko">
<div>
<h3><b>  Eco  </b>  Slim  </h3>
<p>  è un prodotto   <br/>  al 100% naturale ed ecologicamente puro  </p>
</div>
</div>
<div class="woman">
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/block_4_woman.png"/>
</div>
<div class="eko2">
<div><span>  Non contiene sostanze chimiche o sintetiche!  </span></div>
</div>
</div>
</div>
<div class="line"></div>
<ul class="list">
<li></li>
<li></li>
</ul>
</div>
<div class="block_5">
<div class="top">
<div class="container">
<h3><font>  Composizione accuratamente selezionata  </font>   per ottenere i migliori risultati possibili:  </h3>
<ul>
<li><span>  L-carnitina  </span></li>
<li><span>  Caffeina  </span></li>
<li><span>  Chitosano  </span></li>
<li><span>  Estratto di   <br/>  Coleus forskohlii  </span></li>
<li><span>  Estratto di guaranà  </span></li>
<li><span>  Acido succinico  </span></li>
<li><span>  Estratto di   <br/>  alghe Fucus  </span></li>
<li><span>  Vitamine   <br/>  В2, В5, В6, В8, В12  </span></li>
</ul>
</div>
</div>
<div class="bottom">
<div class="container">
<p><b>  Eco  </b>  Slim  </p>
<div class="approved">  Progettato in accordo con le   <br/>  tecnologie europee  </div>
<div class="stamp">  Approvato dalle  <small>  ispezioni sanitarie  </small></div>
</div>
</div>
</div>
<div class="block_6">
<div class="container">
<h3>  La carenza di minerali diventa la causa del peso in eccesso!  </h3>
<p>  Potreste pensare che le piante  <br/>   non siano abbastanza potenti per bruciare i grassi...  <br/><b>  Ma le scoperte scientifiche dicono il contrario!  </b></p>
<ul>
<li><small>  B2  </small><span><b>  Normalizza  </b>   l'equilibrio ormonale del corpo  </span></li>
<li><small>  B6  </small><span><b>  Regola  </b>   i processi metabolici, rafforza la pelle, i capelli e le unghie  </span></li>
<li><small>  B12  </small><span><b>  Accelera  </b>   il metabolismo, aiuta ad assimilare le sostanze nutritive  </span></li>
<li><small>  B5  </small><span><b>  Contribuisce a  </b>   scindere i carboidrati e i grassi  </span></li>
<li><small>  B8  </small><span><b>  Riduce  </b>   il colesterolo, regola il funzionamento del tratto gastrointestinale  </span></li>
<li><small>  Taurina  </small><span><b>  Attiva  </b>   i processi metabolici e brucia i grassi  </span></li>
<li><small>  Caffeina  </small><small>  Acido succinico  </small><span><b>  Elimina le tossine e i liquidi in eccesso,  </b>   brucia i grassi e attiva il processo di rinnovamento cellulare.  </span></li>
</ul>
</div>
<div class="line"></div>
</div>
<div class="block_7">
<div class="container">
<div class="left">
<h3>  I tuoi futuri   <big>  risultati:  </big></h3>
<p>  Un solo bicchiere d'acqua con qualche goccia di   <b>  EcoSlim  </b>   e il tuo corpo comincerà a cambiare giorno per giorno!  </p>
<ul>
<li>  fino a 0,5kg in meno al giorno  </li>
<li>  fino a 3,5kg in meno al giorno  </li>
<li>  fino a 10-12kg in meno al giorno  </li>
</ul>
<div class="safely">
<b>  Sicuro  </b>
						  Non intacca il sistema cardiovascolare o il sistema nervoso
					</div>
</div>
<div class="right">
<div class="img">
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/block_7_woman.png"/>
<img class="product2" src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/ecoslim2.png"/>
</div>
</div>
</div>
<div class="line"></div>
</div>
<div class="block_8">
<div class="container">
<h3><font>  Feedback  </font>   dei nostri clienti  </h3>
<ul>
<li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/block_8_li1.jpg"/>
<b>  Martina Nibali  </b>
							  24 anni
						</span>
<span>  Essere grassi per una vita intera e poi dimagrire in un mese, fantastico!   <b>  Sono molto contenta del prodotto EcoSlim.  </b>   Ha un ottimo gusto, riduce l'appetito e elimina il senso di pesantezza allo stomaco. Questo è il primo rimedio che mi ha permesso di perdere 13 kg! All'inizio ho perso poco peso, ma poi ho cominciato a perdere 1 kg al giorno.  </span>
</li>
<li>
<span>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/block_8_li2.jpg"/>
<b>  Daniele Prandi  </b>
							  32 anni
						</span>
<span>  Ad essere sinceri, sono dimagrito per mia figlia: non riuscivo a correre o a giocare con lei, mi era anche difficile piegarmi. Ho perso più di 10 kg e poi ho cominciato ad andare in palestra.   <b>  EcoSlim è un prodotto fantastico.  </b>   Lo bevo ancora quando non mi alleno o mangio troppo durante le feste.  </span>
</li>
</ul>
</div>
<div class="bottom">
<div class="container">
<div class="left">
<p>  Abbiamo già venduto   <br/>  più di 20.000 confezioni di  </p>
<p><b>  Eco  </b>  Slim  </p>
</div>
<div class="product3">
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/ecoslim2.png"/>
</div>
<div class="right">
<p><b>  Affrettati ad ordinare  </b>   ad un prezzo scontato   <b>  ora  </b>  per avere un corpo perfetto   <b>  in un paio di settimane!  </b></p>
<button data-scroll="true"><span>  Ordina subito  </span></button>
</div>
</div>
</div>
</div>
<div class="block_9">
<div class="container">
<div class="product">
<div>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/images/ecoslim.png"/>
<small><span><b>  Il prodotto  </b>  è certificato  </span></small>
</div>
</div>
<div class="form_pr">
<div class="form">


							<p class="defens-text">Ordina <span><b>ECO</b>SLIM</span> da noi</p>
<button data-scroll="true"><span>Ordina</span></button>
<div class="price-second">
<span class="old_price js_old_price">98</span>
<span class="new_price"><span class="js_new_price">49</span> <span class="js_curs"> €  </span></span>
<span class="precent">  50%   <span class="js_percent_sign">  di sconto  </span></span>
</div>


						</div>
<div class="price">
<div>
<span class="precent">  50%   <span class="js_percent_sign">  di sconto  </span></span>
<span class="old_price js_old_price">98</span>
<span class="new_price"><span class="js_new_price">49</span> <span class="js_curs"> €  </span></span>
<span class="shipment">  + spese di spedizione  </span>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="hidden-window">
<div class="second-clips-header">
<div class="containerz">
<div class="h-w__inner">
<span class="logo_s"><b>Eco</b>Slim</span>
<div class="yop-right__side">
<div class="dtable">
<div class="dtable-cell">Ordinalo subito!
</div>
</div>
</div>
</div>
</div>
</div>
<div class="containerz">
<section class="h-w__inner">
<div class="cf">
<div class="w-h__right">
<div class="w-h__right-row1">
<div class="zHeader">PAGAMENTO IN CONTRASSEGNO</div>
<div class="zContent">
<div class="dtable">
<div class="dtable-cell text-right"><i>1</i><img src="//st.acstnst.com/content/Eco_Slim_IT_Green/imageSecond/dtc1.png"/></div>
<div class="dtable-cell">ORDINE</div>
</div>
<div class="dtable">
<div class="dtable-cell text-right"><i>2</i><img src="//st.acstnst.com/content/Eco_Slim_IT_Green/imageSecond/dtc2.png"/></div>
<div class="dtable-cell">SPEDIZIONE</div>
</div>
<div class="dtable">
<div class="dtable-cell text-right"><i>3</i><img src="//st.acstnst.com/content/Eco_Slim_IT_Green/imageSecond/dtc3.png"/></div>
<div class="dtable-cell">CONSEGNA E<span> PAGAMENTO</span></div>
</div>
</div>
</div>
<div class="printbg">OTTIMO AFFARE</div>
<form action="" class="js_scrollForm" method="post"><input type="hidden" name="total_price" value="49.0">

<div class="formHeader"><span>INSERISCI I TUOI DATI PER  </span><br/> EFFETTUARE L’ORDINE</div>
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAJxFgAAAAAAAAAAAAQUAkqqAA"></iframe>
</form>
<div class="zGarant">
<div class="zHeader">GARANTIAMO</div>
<ul>
<li><b>Qualità al</b> 100%</li>
<li><b>Controllo </b> del prodotto una volta ricevuto</li>
<li><b>Sicurezza </b> dei tuoi dati</li>
</ul>
</div>
</div>
<div class="w-h__left">
<div class="item transitionmyl1 ">
<div class="zDiscount"><b>Attenzione</b>:<br/><span>SCONTO DEL 50%</span></div>
<div class="item__right">
<div class="zHeader"><b>1</b> confezione</div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prezzo:</div>
<div class="dtable-cell nowrap"><b class="js-pp-new"> 49  </b> <b>€</b></div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell">
<span class="dib old-pricedecoration"><i></i><text class="js-pp-old"> 98 </text></span>
</div>
<div class="dtable-cell text-left"><span class="old-pr-descr">Vecchio<br/>prezzo</span></div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Spedizione:</div>
<div class="dtable-cell"><b class="js-pp-ship"> 0  </b> <b>€</b></div>
<div class="dtable-cell text-center mw30"></div>
</div>
</div>
<div class="zSep"></div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Totale:</div>
<div class="dtable-cell prtotal"><b><text class="js-pp-full">  49  </text></b>€</div>
<div class="dtable-cell text-center mw30"></div>
</div>
</div>
</div>
</div>
<div class="item__left">
<div class="js_changer change-package-button" data-package-id="1"></div>
<div class="img-wrapp"><img src="//st.acstnst.com/content/Eco_Slim_IT_Green/imageSecond/zit1.png"/></div>
<div class="zHeader"><span> </span></div><text>Eco Slim - il sistema dimagrante №1!
Offerta unica, solo per oggi! Metodo di dimagrimento rapido al 50% di sconto! Effetto immediato!<br/><br/></text>
</div>
</div>
<div class="item hot transitionmyl1 active ">
<div class="zstick"></div>
<div class="zDiscount"><b>Attenzione</b>:<br/><span>SCONTO DEL 50%</span></div>
<div class="item__right">
<div class="zHeader sec"><b>2</b> confezioni <span class="dib zplus">regalo</span></div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prezzo:</div>
<div class="dtable-cell nowrap"><b><text class="js-pp-new">  98  </text> €</b></div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell"><span class="dib old-pricedecoration"><i></i><span class="js-pp-old"> 196  </span></span></div>
<div class="dtable-cell text-left"><span class="old-pr-descr">Vecchio<br/>prezzo</span></div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Spedizione:</div>
<div class="dtable-cell"><b><text class="js-pp-ship">  0  </text> €</b></div>
<div class="dtable-cell text-center mw30"></div>
</div>
</div>
<div class="zSep"></div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Totale:</div>
<div class="dtable-cell prtotal"><b><span class="js-pp-full">  98  </span></b> €</div>
<div class="dtable-cell text-center mw30"></div>
</div>
</div>
</div>
</div>
<div class="item__left">
<div class="js_changer change-package-button active" data-package-id="3"></div>
<div class="img-wrapp">
<div class="gift">+ regalo</div>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/imageSecond/zit2.png"/>
</div>
<div class="zHeader"><span> </span></div>
<text>Risultati stabili! Il trattamento Eco Slim vi aiuterà a sbarazzarvi del peso in eccesso per sempre! 3 confezioni di Eco Slim al prezzo di 2!<br/></text>
</div>
</div>
<!--end of item2-->
<!--item3-->
<div class="item transitionmyl1">
<div class="zDiscount"><b>Attenzione</b>:<br/><span>SCONTO DEL 50%</span></div>
<div class="item__right">
<div class="zHeader sec"><b>3</b> confezioni <span class="dib zplus sec">regalo</span></div>
<div class="zPrices">
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst">Prezzo:</div>
<div class="dtable-cell nowrap"><b><text class="js-pp-new">  147  </text> €</b></div>
<div class="dtable-cell text-center mw30">
<div class="dtable">
<div class="dtable-cell"><span class="dib old-pricedecoration"><i></i><text class="js-pp-old">  294  </text></span></div>
<div class="dtable-cell text-left"><span class="old-pr-descr">Vecchio<br/>prezzo</span></div>
</div>
</div>
</div>
<div class="dtr">
<div class="dtable-cell fst padding-top10px">Spedizione:</div>
<div class="dtable-cell"><b><text class="js-pp-ship">  0  </text> €</b></div>
<div class="dtable-cell text-center mw30"></div>
</div>
</div>
<div class="zSep"></div>
<div class="dtable">
<div class="dtr">
<div class="dtable-cell fst fs17">Totale:</div>
<div class="dtable-cell prtotal"><b><text class="js-pp-full">  147  </text></b> €</div>
<div class="dtable-cell text-center mw30"></div>
</div>
</div>
</div>
</div>
<div class="item__left">
<div class="js_changer change-package-button" data-package-id="5"></div>
<div class="img-wrapp">
<div class="gift">+ regalo</div>
<img src="//st.acstnst.com/content/Eco_Slim_IT_Green/imageSecond/zit3.png"/>
</div>
<div class="zHeader sm"></div>
<text>Un approccio professionale! Il trattamento di sei mesi a base di Eco Slim vi aiuterà anche in caso di una grande quantità di chili in eccesso!Trasformazioni reali! Risultati garantiti. 5 confezioni di Eco Slim al prezzo di 3!</text>
</div>
</div>
<!--end-of-item3-->
</div>
</div>
</section>
</div>
</div>




</body></html>
<?php } ?>