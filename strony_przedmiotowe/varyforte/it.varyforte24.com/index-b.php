<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()){ ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 6851 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALIEgRRrsyLAAEAAQACMRIBAALDGgEKAQAEPKWD7wA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Bruno Bellini';
            var phone_hint = '+393476736735';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <title>Varyforte</title>
        <meta content="" name="keywords"/>
        <meta content="" name="description"/>
        <meta content="width=device-width" name="viewport"/>
        <link href="//st.acstnst.com/content/Varyforte_IT/mobile/css/style.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Varyforte_IT/mobile/css/owl.carousel.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Varyforte_IT/mobile/css/owl.theme.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Varyforte_IT/mobile/js/owl.carousel.min.js"></script>
        <script src="//st.acstnst.com/content/Varyforte_IT/mobile/js/jquery.placeholder.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Varyforte_IT/mobile/js/common.js" type="text/javascript"></script>

    </head>
    <body class="root">
    <div class="wrapper ">
        <header>
            <div class="page-wrapper__inner transitionall">
                <a class="show" href="#">
                    <img alt="gsdgsd" class="center-block" src="//st.acstnst.com/content/Varyforte_IT/mobile/img/header1_logo.png"/>
                </a>
                <img alt="" class="product_img" src="//st.acstnst.com/content/Varyforte_IT/mobile/img/product.png" width="100"/>
                <div class="icoblock"></div>
                <h1>Sbarazzati delle vene varicose</h1>
                <div class="c-blue text-center">senza chirurgia né procedure mediche</div>
                <div class="mt20"></div>
                <div class="text-center">Prezzo normale: <span class="tdlh js_old_price">78 </span> €</div>
                <div class="newprice text-center">EFFETTUA IL TUO ORDINE A UN PREZZO INCREDIBILE:
                    <span class="js_new_price">39 </span> € </div>
                <div class="for-event button scroll-to-order">ORDINA</div>
            </div>
        </header>
        <div class="slide2">
            <div class="page-wrapper__inner">
                <ul>
                    <li>Aiuta contro le ulcere</li>
                    <li>Favorisce una corretta circolazione del sangue</li>
                    <li>Rinforza le pareti venose</li>
                    <li>Elimina la pesantezza ai piedi</li>
                    <li>Riduce la formazione di coaguli di sangue</li>
                    <li>Migliora il funzionamento delle valvole venose</li>
                    <li>Migliora il drenaggio dei tessuti</li>
                </ul>
            </div>
        </div>
        <div class="slide3">
            <div class="page-wrapper__inner">
                <h2 class="c-blue">
                    <b>Crema anti-vene varicose <br/>Varyforte</b>
                </h2>
                <!--<img alt="gg" class="imgs3" src="img/imgs3.png">-->
                <p>Trattamento completo che previene la comparsa delle vene varicose. Basta solo usarla! Elimina la comparsa delle vene varicose, tonifica la pelle e rinforza le pareti venose in modo delicato ed efficace. Un metodo eccellente per prevenire e curare quelle già visibili. È l'ancora di salvezza che avresti dovuto trovare prima!</p>
            </div>
        </div>
        <div class="slide4">
            <div class="page-wrapper__inner">
                <h2 class="text-center"><b>COME FUNZIONA </b></h2>
                <div class="text-center s4">
                    <img alt="s4" src="//st.acstnst.com/content/Varyforte_IT/mobile/img/s4i.jpg"/>
                </div>
                <p><span class="s4arrow"></span>Grazie alla sua formula unica, Varyforte funziona in modo sicuro ed efficace eliminando i sintomi generali dei processi infiammatori e alleviando il dolore. L'uso regolare della crema migliora la circolazione sanguigna, oltre a rinforzare e a tonificare le pareti venose.<br/>Varyforte elimina anche altri sintomi come la pelle d'oca, le mani e i piedi freddi e la stanchezza; il peso è molto importante anche in questi casi.</p>
            </div>
        </div>
        <div class="slide5">
            <div class="page-wrapper__inner">
                <h2 class="c-blue text-center">
                    <b>
                        Il parere degli esperti:
                    </b>
                </h2>
                <div class="mt20"></div>
                <ul class="bxslider">
                    <li>
                        <div class="dtable">
                            <!--<div class="dtc">
                                                            <img src="img/client3.jpg" alt="gdsg">
                                                        </div>-->
                            <div class="dtc revname">
                                Lucia Collasanti
                                <br/> 28 anni
                            </div>
                        </div>
                        <p>
                            Dopo la gravidanza e il parto ho iniziato ad avere problemi di circolazione alle gambe: mi facevano molto male e sono comparse le varici. Sono andata dal flebologo, che mi ha consigliato una crema anti-varici da applicare due volte al giorno.

                            Dopo cinque giorni le mie vene erano migliorate, la pelle era tornata alla normalità e le varici erano scomparse. Non mi facevano più male e il gonfiore era andato giù. Continuo con il trattamento e va molto bene, ora sto meglio.
                        </p>
                        <div class="text-center">
                            <img alt="ggg" src="//st.acstnst.com/content/Varyforte_IT/mobile/img/block_10_foto1.jpg"/>
                        </div>
                        <div class="mt20"></div>
                    </li>
                    <li>
                        <div class="dtable">
                            <!--<div class="dtc">
                                                            <img src="img/client2.jpg" alt="gdsg">
                                                        </div>-->
                            <div class="dtc revname">
                                Benedetta Savalli
                                <br/>47 anni
                            </div>
                        </div>
                        <p>
                            Nel mio caso le vene varicose sono un problema cronico. Ho provato pillole, creme e gel per dieci anni, ma l'unica cosa che ha funzionato su di me è stata Varyforte. Si tratta di una crema molto buona che si assorbe rapidamente e agisce velocemente. Ora non ho più nessuna sensazione di bruciore. Inoltre, non causa reazioni allergiche, e io sono incline alle allergie!
                            Il gonfiore e l'infiammazione sono scomparsi a poco a poco, non tanto velocemente quanto avrei voluto, ma posso dimenticarli per sempre! Senza contare che adesso le mie gambe sono più leggere. Anche se viene usata molto, gli effetti sono sempre visibili, il corpo non si assuefa.
                        </p>
                        <div class="text-center">
                            <img alt="ggg" src="//st.acstnst.com/content/Varyforte_IT/mobile/img/block_10_foto2.jpg"/>
                        </div>
                        <div class="mt20"></div>
                    </li>
                    <li>
                        <div class="dtable">
                            <!--<div class="dtc">
                                                            <img src="img/client1.png" alt="gdsg">
                                                        </div>-->
                            <div class="dtc revname">
                                Gabriella Natoli
                                <br/>58 anni
                            </div>
                        </div>
                        <p>
                            Io ero al secondo stadio di varici, le vene erano molto visibili, le gambe mi si gonfiavano e la pelle era molto secca. Mia figlia mi ha comprato questa crema, ma non credevo che sarebbe stata così efficace, visto che gli altri metodi non mi avevano aiutata.

                            Uso continuamente la crema da quasi un anno e i risultati sono impressionanti! La mia pelle sta molto meglio, è più viva e ben idratata. A poco a poco il dolore e il gonfiore sono scomparsi e dopo un mese le vene visibili sono sparite, mentre quelle in rilievo sono diventate lisce. Sono molto soddisfatta dei risultati!
                        </p>
                        <div class="text-center">
                            <img alt="ggg" src="//st.acstnst.com/content/Varyforte_IT/mobile/img/block_10_foto3.jpg"/>
                        </div>
                        <div class="mt20"></div>
                    </li>
                    <li>
                        <div class="dtable">
                            <!--<div class="dtc">
                                                            <img src="img/client1.png" alt="gdsg">
                                                        </div>-->
                            <div class="dtc revname">
                                Ettore Modigliano
                                <br/>65 anni
                            </div>
                        </div>
                        <p>Mia moglie mi dava un sacco di strigliate perché ho le vene visibili e le ignoro. Ce le ho sulle braccia, molto stanche e con una sensazione di freddo continua. Così mi ha comprato questa crema ed è sempre lei a spalmarmela.

                            Mi piace quando me la mette, perché mi fa un massaggio e la crema ha un effetto caldo, il che mi dà sollevo, visto che le mie braccia sono sempre fredde. Dopo 10 giorni il tono della mia pelle è cambiato e le vene si notano meno. Sono molto felice perché sto molto meglio, il dolore è scomparso!</p>
                        <div class="text-center">
                            <img alt="ggg" src="//st.acstnst.com/content/Varyforte_IT/mobile/img/block_10_foto4.jpg"/>
                        </div>
                        <div class="mt20"></div>
                    </li>
                </ul>
            </div>
        </div>
        <footer>
            <div class="page-wrapper__inner transitionall">
                <h2><b>EFFETTUA IL TUO ORDINE A <br/> UN PREZZO INCREDIBILE:</b></h2>
                <form action="" id="form" method="post">
                    <iframe scrolling="no" style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;" src="http://pt.new-power-life.com/forms/?target=-4AAIJIALIEgAAAAAAAAAAAARbdpWHAA"></iframe>

                    <!--<input type="hidden" name="total_price" value="39.0">
                    <input type="hidden" name="esub" value="-4A25sMQIJIALIEgRRrsyLAAEAAQACMRIBAALDGgEKAQAEPKWD7wA">
                    <input type="hidden" name="goods_id" value="92">
                    <input type="hidden" name="title" value="">
                    <input type="hidden" name="ip_city" value="Toruń">
                    <input type="hidden" name="template_name" value="Varyforte_IT">
                    <input type="hidden" name="price" value="39">
                    <input type="hidden" name="old_price" value="78">
                    <input type="hidden" name="total_price_wo_shipping" value="39.0">
                    <input type="hidden" name="package_prices" value="{}">
                    <input type="hidden" name="currency" value="€">
                    <input type="hidden" name="package_id" value="0">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="ip_country_name" value="Poland">
                    <input type="hidden" name="country_code" value="IT">
                    <input type="hidden" name="shipment_vat" value="0.0">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="shipment_price" value="0">
                    <input type="hidden" name="price_vat" value="0.0">

                    <select class="inp" id="country_code_selector">
                        <option value="IT">Italy</option>
                    </select>
                    <input class="inp" name="name" placeholder="nome completo" type="text"/>
                    <input class="inp only_number" name="phone" placeholder="numero di telefono" type="text"/>-->
                    <div class="dtable">
                        <div class="w50 dtc">
                            Prezzo normale:
                            <br/>
                            <span class="tdlh js_old_price">78  </span> €
                        </div>
                        <div class="dtc text-right fs24f">
                            PREZZO INCREDIBILE:  <span class="js_new_price">39 </span> €
                        </div>
                    </div>

                </form>
            </div>
        </footer>
    </div>
    <!-- .wrapper -->
    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 6851 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALIEgRRrsyLAAEAAQACMRIBAALDGgEKAQAEPKWD7wA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Bruno Bellini';
            var phone_hint = '+393476736735';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>Varyforte</title>
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <meta content="Buy Varikosette , instrucțiuni, preț. Varikosette  – pagina oficială, Varikosette  cumpărați în drogherii, cumpărați Varikosette " name="keywords"/>
        <meta content="" name="description"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="//st.acstnst.com/content/Varyforte_IT/images/header_logo.png" rel="shortcut icon" type="image/x-icon"/>
        <link href="css/normalize.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Varyforte_IT/css/styles.css" rel="stylesheet"/>
        <script src="//st.acstnst.com/content/Varyforte_IT/js/scroll.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <div class="wrapper cf">
        <div id="popWindow"></div>
        <header>
            <div class="center_wrapper">
                <span class="logo"></span>
            </div>
        </header>
        <!-- end of #header -->
        <div class="block_1">
            <div class="center_wrapper">
                <img alt="" class="for_header_product" src="//st.acstnst.com/content/Varyforte_IT/images/product.png" width="180"/>
                <p class="bubble arrow_box">
                    senza chirurgia né<br/>procedure mediche
                </p>
                <h1 class="slogon">
                    Sbarazzati delle vene varicose
                </h1>
                <div class="stmp stmp1"><p>Per tutte <br/> le età</p></div>
                <div class="stmp stmp2"><p>Prodotto 100% naturale</p></div>
                <div class="stmp stmp3"><p>Risultati garantiti</p></div>
                <div class="list">
                    <p>
                        L'uso continuativo della crema:
                    </p>
                    <ul>
                        <li>Aiuta contro le ulcere</li>
                        <li>Favorisce una corretta circolazione del sangue</li>
                        <li>Rinforza le pareti venose</li>
                        <li>Elimina la pesantezza ai piedi</li>
                        <li>Riduce la formazione di coaguli di sangue</li>
                        <li>Migliora il funzionamento delle valvole venose</li>
                        <li>Migliora il drenaggio dei tessuti</li>
                    </ul>
                </div>
                <p>
                    <b>
                        Smetti di vergognarti
                    </b>
                    <br/> (non avere paura di mostrare il tuo corpo, pelle senza segni)
                </p>
                <div class="form for_scroll">
                    <div class="discount">
                        <p>
                            <b>50% </b>
                            <br/>
                            di sconto

                        </p>
                    </div>
                    <p>
                        EFFETTUA IL TUO ORDINE A UN PREZZO INCREDIBILE:
                    </p>
                    <p>
<span class="js_new_price_curs">
<strong class="js_new_price">39</strong> <strong> €</strong>
</span>
                    </p>
                    <p>
                        Prezzo normale:
                        <span class="js_old_price_curs">
<strong class="js_old_price">78</strong> <strong> €</strong>
</span>
                    </p>
                    <a class="lv-order-button rushOrder" href="#form">ORDINA</a>
                </div>
            </div>
        </div>
        <div class="block_2">
            <div class="center_wrapper">
                <p class="headline">
                    Crema anti-vene varicose
                    <br/>
                    <span><span>Varyforte</span></span>
                </p>
                <p>
                    Trattamento completo che previene la comparsa delle vene varicose. Basta solo usarla! Elimina la comparsa delle vene varicose, tonifica la pelle e rinforza le pareti venose in modo delicato ed efficace.
                </p>
                <p>
                    Un metodo eccellente per prevenire e curare quelle già visibili. È l'ancora di salvezza che avresti dovuto trovare prima!
                </p>
            </div>
        </div>
        <div class="block_3">
            <div class="center_wrapper">
                <i class="bg"></i>
                <div class="lable">
                    <p>
                        Primi <br/>sintomi
                    </p>
                </div>
                <div class="description">
                    <p>Dolore</p>
                    <p>Sensazione di bruciore, botte</p>
                    <p>Malattia</p>
                    <p>Ipostasi</p>
                    <p>Spasmi notturni</p>
                </div>
                <p class="headline">
                    Cause della comparsa delle vene varicose:
                </p>
                <ul>
                    <li>
                        Genetica, ereditate dai genitori o dalla famiglia
                    </li>
                    <li>
                        Vita sedentaria e mancanza di movimento e di attività fisica
                    </li>
                    <li>
                        Diete moderne prive di cellulosa
                    </li>
                    <li>
                        Vestiti stretti e tacchi alti che impediscono una corretta circolazione del sangue
                    </li>
                    <li>
                        Cattive abitudini quando si è seduti
                    </li>
                    <li>
                        Eccesso di sport
                    </li>
                    <li>
                        Trattamenti ormonali come gli anticoncezionali, farmaci per il raffreddore, ecc.
                    </li>
                </ul>
            </div>
        </div>
        <div class="block_4">
            <div class="center_wrapper cf">
                <div class="block_4_bg"></div>
                <div class="ss">
                    <p>Vena varicosa</p>
                    <p>Vena sana</p>
                </div>
                <p class="headline">
                    <span>COME FUNZIONA <span>LA CREMA:</span></span> <br/>
                </p>
                <p class="headlinep">
                    Grazie alla sua formula unica, Varyforte funziona in modo sicuro ed efficace eliminando i sintomi generali dei processi infiammatori e alleviando il dolore. L'uso regolare della crema migliora la circolazione sanguigna, oltre a rinforzare e a tonificare le pareti venose.

                </p>
                <p class="headlinep">
                    Varyforte elimina anche altri sintomi come la pelle d'oca, le mani e i piedi freddi e la stanchezza; il peso è molto importante anche in questi casi.
                </p>
            </div>
        </div>
        <div class="block_5">
            <div class="center_wrapper">
                <p class="headline">
                    Il parere degli esperti:
                </p>
                <div>
                    <img alt="block_5_foto1" src="//st.acstnst.com/content/Varyforte_IT/images/block_5_foto1.jpg"/>
                    <p>
                        Non mi stancherò mai di ripetere quanto sia importante usare una crema anti-varici per curare questa malattia! È difficile sopravvalutare la sua efficacia, soprattutto se si tratta della formula esclusiva di Varyforte. Questa crema è stata clinicamente testata e ha portato risultati inaspettati nei pazienti.
                    </p>
                    <p>
                        Si può adoperare in casa, meglio se quotidianamente, ed è facile da usare: basta solo applicarla massaggiando delicatamente i punti interessati. Il suo effetto è visibile molto rapidamente. I pazienti affermano che il dolore, la stanchezza di mani e pieci, ecc. diminuiscono. Consiglio Varyforte a tutti!
                    </p>
                    <p class="">
<span>
<b>Renato Semenzato</b><br/> Chirurgo, 15 anni di esperienza in campo circolatorio
                </span>
                    </p>
                </div>
                <div>
                    <img alt="block_5_foto2" src="//st.acstnst.com/content/Varyforte_IT/images/block_5_foto2.jpg"/>
                    <p>
                        MSpesso mi chiedo quale sia la crema anti-varici più efficace, delicata e sicura per me, che consiglio anche alle mamme giovani e alle signore anziane. Quindi vi dico di provare la crema Varyforte!
                    </p>
                    <p>
                        In pratica, solo questa crema dà miglioramenti reali, eliminando i sintomi dolorosi e migliorando l'aspetto delle vene giorno dopo giorno. Molte pomate chimiche possono causare allergie, ma Varyforte contiene solo ingredienti naturali. Dovreste provarla!
                    </p>
                    <p class="">
<span>
<b>Elena Trifirò</b><br/> Flebologa
                </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="block_6">
            <div class="center_wrapper">
                <p class="headline">
                    Formula 100% naturale
                    <br/>
                    <span>
<span>Varyforte</span>
</span>
                </p>
                <p>
                    <b>
                        Presta attenzione ai suoi componenti attivi!
                    </b>
                    <br/>
                    Per trovare una crema che funzioni, è necessario sapere quali sono i suoi ingredienti: sapere che effetto hanno ti può aiutare.
                </p>
                <div class="cf">
                    <div class="clearup">
                        <p class="clear for_consist">
                            <b>Propoli</b> <br/>Il propoli è un ingrediente efficace per rinforzare i vasi sanguigni e, quindi, può ridurre i sintomi delle vene varicose. Oltre ad essere astringente, contiene anche sostanze naturali che possono aiutare a ridurre l'infiammazione e il dolore.
                        </p>
                    </div>
                    <div class="left">
                        <div>
                            <img alt="block_6_pic1" src="//st.acstnst.com/content/Varyforte_IT/images/img2.png" width="110"/>
                            <p>
                                <b>Olio d'oliva</b><br/> Aumentare la circolazione è essenziale per curare le vene varicose e massaggiare l'area interessata con olio d'oliva può aiutare a migliorarla, nonché a ridurre il dolore e l'infiammazione.
                            </p>
                        </div>
                        <!-- <div>
                        <img alt="block_6_pic2" src="images/block_6_pic2.png"/>
                        <p>
                        <b>Extracto de productos de abeja </b><br/> Fortalece la pared venosa y la limpia, la vuelve más elástica y ayuda al “sistema circulatorio”.
                                            </p>
                                        </div> -->
                        <!-- <div>
                        <p>
                        <b>Esencia de abeja</b><br/> Elimina la inflamación, los dolores y la hinchazón.
                                            </p>
                                        </div> -->
                    </div>
                    <div class="right">
                        <!-- <div>
                        <img alt="block_6_pic4" src="images/block_6_pic4.png"/>
                        <p>
                        <b>Cera de abeja</b><br/> Acelera la regeneración y ayuda a coagular antes las heridas.
                                            </p>
                                        </div> -->
                        <div>
                            <img alt="block_6_pic5" src="//st.acstnst.com/content/Varyforte_IT/images/img3.png" width="110"/>
                            <p>
                                <b>Olio di cedro</b><br/> La crema Varyforte con olio di cedro è un trattamento meraviglioso per le varici. Si tratta di un detergente naturale che migliora anche la circolazione. Quando il sangue comincia a circolare naturalmente, la pesantezza e il gonfiore si riducono significativamente.
                            </p>
                        </div>
                        <!-- <div>
                        <img alt="block_6_pic3" src="images/block_6_pic3.png"/>
                        <p>
                        <b>Veneno de abeja </b><br/> Mejora la circulación de la sangre y quita los espasmos musculares.
                                            </p>
                                        </div> -->
                    </div>
                    <div class="cf"></div>
                </div>
                <p>
                    Questo prodotto contiene le vitamine B1, B5 e C, che rinforzano le pareti delle vene e le rigenerano.

                </p>
                <p>
                    Varyforte allarga i capillari affinché il sangue circoli meglio e rimuove il dolore che provoca la cattiva circolazione delle vene varicose. Inoltre, penetra le vene più efficacemente dei farmaci, perché agisce direttamente sul problema.

                </p>
            </div>
        </div>
        <div class="block_7">
            <div class="center_wrapper">
                <p class="headline">
                    Principali benefici contro le varici:
                </p>
                <p>
                    Rinforza l'organismo, elimina i sintomi della stanchezza, elimina la sensazione di bruciore e dà sollievo alle gambe pesanti.
                </p>
                <div>
                    <div>
                        <p>Migliora la circolazione</p>
                        <p>Riduce la tensione delle vene</p>
                        <p>Rapido recupero dei capillari</p>
                    </div>
                    <div>
                        <p>Riduce il gonfiore </p>
                        <p>Migliora l'elasticità delle vene</p>
                        <p>Pulisce e risana le vene </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_8">
            <div class="center_wrapper">
                <div>
                    <img alt="block_8_ico1" src="//st.acstnst.com/content/Varyforte_IT/images/block_8_ico1.png"/>
                    <p>
                        SENZA INTERVENTI CHIRURGICI
                    </p>
                </div>
                <div>
                    <img alt="block_8_ico2" src="//st.acstnst.com/content/Varyforte_IT/images/block_8_ico2.png"/>
                    <p>
                        SENZA FARMACI COSTOSI
                    </p>
                </div>
                <div>
                    <img alt="block_8_ico3" src="//st.acstnst.com/content/Varyforte_IT/images/block_8_ico3.png"/>
                    <p>
                        SENZA DOLORI NÉ EFFETTI COLLATERALI PER LA SALUTE
                    </p>
                </div>
            </div>
        </div>
        <div class="block_9">
            <div class="center_wrapper">
                <p class="headline">
                    Le varici sono una malattia seria che va curata il prima possibile
                </p>
                <div>
                    <img alt="block_9_foto1" src="//st.acstnst.com/content/Varyforte_IT/images/block_9_foto1.jpg"/>
                    <p>Ragni vascolari</p>
                </div>
                <div>
                    <img alt="block_9_foto2" src="//st.acstnst.com/content/Varyforte_IT/images/block_9_foto2.jpg"/>
                    <p>Varici reticolari</p>
                </div>
                <div>
                    <img alt="block_9_foto3" src="//st.acstnst.com/content/Varyforte_IT/images/block_9_foto3.jpg"/>
                    <p>Varici tronculari e collaterali</p>
                </div>
                <div>
                    <img alt="block_9_foto4" src="//st.acstnst.com/content/Varyforte_IT/images/block_9_foto4.jpg"/>
                    <p>Problemi venosi cronici</p>
                </div>
                <div>
                    <img alt="block_9_foto5" src="//st.acstnst.com/content/Varyforte_IT/images/block_9_foto5.jpg"/>
                    <p>Ulcere trofiche</p>
                </div>
            </div>
        </div>
        <div class="block_10">
            <div class="center_wrapper">
                <p class="headline">
                    Il parere degli utenti:
                </p>
                <div>
                    <div>
                        <i>APrima Dopo</i>
                        <img alt="block_10_foto1" src="//st.acstnst.com/content/Varyforte_IT/images/block_10_foto1.jpg"/>
                        <p>Lucia Collasanti <br/>28 anni</p>
                    </div>
                    <p>
                        Dopo la gravidanza e il parto ho iniziato ad avere problemi di circolazione alle gambe: mi facevano molto male e sono comparse le varici. Sono andata dal flebologo, che mi ha consigliato una crema anti-varici da applicare due volte al giorno.

                    </p>
                    <p>
                        Dopo cinque giorni le mie vene erano migliorate, la pelle era tornata alla normalità e le varici erano scomparse. Non mi facevano più male e il gonfiore era andato giù. Continuo con il trattamento e va molto bene, ora sto meglio.

                    </p>
                </div>
                <div>
                    <div>
                        <i>Prima Dopo</i>
                        <img alt="block_10_foto2" src="//st.acstnst.com/content/Varyforte_IT/images/block_10_foto2.jpg"/>
                        <p>
                            Benedetta Savalli<br/>47 anni
                        </p>
                    </div>
                    <p>
                        Nel mio caso le vene varicose sono un problema cronico. Ho provato pillole, creme e gel per dieci anni, ma l'unica cosa che ha funzionato su di me è stata Varyforte. Si tratta di una crema molto buona che si assorbe rapidamente e agisce velocemente. Ora non ho più nessuna sensazione di bruciore. Inoltre, non causa reazioni allergiche, e io sono incline alle allergie!

                    </p>
                    <p>
                        Il gonfiore e l'infiammazione sono scomparsi a poco a poco, non tanto velocemente quanto avrei voluto, ma posso dimenticarli per sempre! Senza contare che adesso le mie gambe sono più leggere. Anche se viene usata molto, gli effetti sono sempre visibili, il corpo non si assuefa.

                    </p>
                </div>
                <div>
                    <div>
                        <i>Prima Dopo</i>
                        <img alt="block_10_foto3" src="//st.acstnst.com/content/Varyforte_IT/images/block_10_foto3.jpg"/>
                        <p>
                            Gabriella Natoli <br/>58 anni
                        </p>
                    </div>
                    <p>
                        Io ero al secondo stadio di varici, le vene erano molto visibili, le gambe mi si gonfiavano e la pelle era molto secca. Mia figlia mi ha comprato questa crema, ma non credevo che sarebbe stata così efficace, visto che gli altri metodi non mi avevano aiutata.

                    </p>
                    <p>
                        Uso continuamente la crema da quasi un anno e i risultati sono impressionanti! La mia pelle sta molto meglio, è più viva e ben idratata. A poco a poco il dolore e il gonfiore sono scomparsi e dopo un mese le vene visibili sono sparite, mentre quelle in rilievo sono diventate lisce. Sono molto soddisfatta dei risultati!
                    </p>
                </div>
                <div>
                    <div>
                        <i>Prima Dopo</i>
                        <img alt="block_10_foto4" src="//st.acstnst.com/content/Varyforte_IT/images/block_10_foto4.jpg"/>
                        <p>
                            Ettore Modigliano <br/>65 anni
                        </p>
                    </div>
                    <p>
                        Mia moglie mi dava un sacco di strigliate perché ho le vene visibili e le ignoro. Ce le ho sulle braccia, molto stanche e con una sensazione di freddo continua. Così mi ha comprato questa crema ed è sempre lei a spalmarmela.

                    </p>
                    <p>
                        Mi piace quando me la mette, perché mi fa un massaggio e la crema ha un effetto caldo, il che mi dà sollevo, visto che le mie braccia sono sempre fredde. Dopo 10 giorni il tono della mia pelle è cambiato e le vene si notano meno. Sono molto felice perché sto molto meglio, il dolore è scomparso!

                    </p>
                </div>
            </div>
        </div>
        <div class="block_11">
            <div class="center_wrapper">
                <i class="bg"></i>
                <img alt="" class="for_footer_product" src="//st.acstnst.com/content/Varyforte_IT/images/product.png" width="180"/>
                <div class="dsc_box">
                    <div class="discount">
                        <p>
                            <b>50%</b>
                            <br/>
                            di sconto
                        </p>
                    </div>
                    <p>EFFETTUA IL TUO ORDINE A </p>
                    <p>
                        UN PREZZO INCREDIBILE:
                        <span class="js_new_price_curs">
<strong class="js_new_price">39</strong> <strong> €</strong>
</span>
                    </p>
                    <p>
                        Prezzo normale:
                        <span class="js_old_price_curs">
<strong class="js_old_price js_price">78</strong> <strong> €</strong>
</span>
                    </p>
                    <form action="" id="form" method="post">
                        <iframe scrolling="no" style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;" src="http://pt.new-power-life.com/forms/?target=-4AAIJIALIEgAAAAAAAAAAAARbdpWHAA"></iframe>

                        <!--<input type="hidden" name="total_price" value="39.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIALIEgRRrsyLAAEAAQACMRIBAALDGgEKAQAEPKWD7wA">
                        <input type="hidden" name="goods_id" value="92">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Varyforte_IT">
                        <input type="hidden" name="price" value="39">
                        <input type="hidden" name="old_price" value="78">
                        <input type="hidden" name="total_price_wo_shipping" value="39.0">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="IT">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <select class="inp" id="country_code_selector">
                            <option value="IT">Italy</option>
                        </select>
                        <input class="inp" name="name" placeholder="nome completo" type="text"/>
                        <input class="inp only_number" name="phone" placeholder="numero di telefono" type="text"/>
                        <input class="lv-order-button rushOrder js_submit" name="yt0" type="submit" value="ORDINA"/>
                        --->
                    </form>
                </div>
            </div>
        </div>
        <div class="bffoot">
            Prodotto testato clinicamente, contiene le istruzioni per l'uso e dei suoi componenti. L'apparenza inganna: a volte il prodotto che vediamo in foto è diverso da quello che riceviamo. Non è questo il caso.

            <br/>
            <br/>
        </div>
        <div class="wrapper cf">
            <div class="wrapper cf">
                <div class="footer cf">

                </div>
            </div>
            <!-- end of footer -->
        </div>
    </div>
    <div></div>
    </body>
    </html>
<?php } ?>