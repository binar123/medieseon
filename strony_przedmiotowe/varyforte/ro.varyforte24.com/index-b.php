<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()){ ?>

<!DOCTYPE html>
<html>
<head>

    <!-- [pre]land_id = 2004 -->
    <script>var locale = "ro";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAIbBwQb3ySEAAABAAKkBgEAAtQHAQoBDwReCSeaAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
    .ac_footer {
        position: relative;
        top: 10px;
        height:0;
        text-align: center;
        margin-bottom: 70px;
        color: #A12000;
    }
    .ac_footer a {
        color: #A12000;
    }
    img[height="1"], img[width="1"] {
        display: none !important;
    }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 20;
        var name_hint = 'Mogos viorica';
        var phone_hint = '+40722559568';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
            '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
            ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("ro");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1666009176948198');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->


    <title>Tratarea Varicozei fără operații și proceduri medicale</title>
    <meta charset="UTF-8">

    <meta name="description" content="Cumpărați Varyforte – tratament simplu și eficient al varicelor în confortul propriei locuințe">
    <meta name="keywords" content="Buy Varyforte, instrucțiuni, preț. Varyforte – pagina oficială, Varyforte cumpărați în drogherii, cumpărați Varyforte">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="no">

    <link rel="stylesheet" href="//st.acstnst.com/content/Varicofix_RO/mobile/css/styles.css" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="http://st.acstnst.com/content/Varicofix_RO/mobile/images/logo.png">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300,700,700italic&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lobster&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <script src="//st.acstnst.com/content/Varicofix_RO/mobile/js/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function () {

            $(".owl-carousel").owlCarousel({

                navigation: true, // Show next and prev buttons
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true

                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

        });
    </script>
    <script>
        $(function () {
            $('[data-scroll]').on('click', function () {
                var target = $(this).data('scroll');

                $('html,body').animate({
                    scrollTop: $(target).offset().top
                });
            });

            $('.js_toggle_reviews').on('click', function (event) {
                var $t = $(this);
                $('.js_show_reviews').slideToggle('slow', function () {
                    $t.hide()
                });
                return false;
            });
        });
    </script>
    <script type="text/javascript" src="//st.acstnst.com/content/Varicofix_RO/mobile/js/scroll.js"></script>
</head>

<body>
    <article class="header">
        <div class="container">
            <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/logo.png" alt="img" class="logoImg"><span class="logo-txt">Varyforte</span>
            <!-- /TELEPHONE -->
        </div>
    </article>
    <br>
    <br>
    <section id="sec1">
        <div class="container">
            <br>
            <h1 class="text-center headingTop">Scăpați de varice</h1>
            <div class="col-4 first">
                <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/product.png" alt="img" class="product1">
            </div>
            <br>
            <div class="col-4 orderBox">
                <div class="form">
                    <div class="discount">
                        <p>
                            <b>24%</b>
                            <br> REDUCERE
                        </p>
                    </div>
                    <p class="title">COMANDAȚI ACUM LA PREȚ PROMOȚIONAL:</p>
                    <p>
                        <span class="new-price">
                            <strong>159&nbsp;Lei</strong>
                        </span>
                    </p>
                    <p class="title">preț obișnuit:
                        <span class="js_old_price_curs linethrough">
                            <strong>318&nbsp;Lei</strong>
                        </span>
                    </p>

                    <input class="submitForm rushOrder scroll" placeholder="" data-scroll="#order_form" type="submit" value="COMANDA ACUM" />
                </div>
            </div>

        </div>
    </section>
    <section class="col-12" id="sec2">
        <div class="container">
            <div class="col-4">
                <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/product.png" alt="img" class="productImg2">
            </div>
        </div>
        <div class="clear"></div>
    </section>
    <section id="sec3">
        <div class="container">
            <div class="col-4 bg2">

            </div>
        </div>
        <div class="clear"></div>
    </section>
    <section id="sec4">
        <div class="container">
            <div class="col-4">
            </div>
            <div class="col-8">


                <h2 class="headline text-center">
                    <span class="white">
                        <span>Cum funcționează crema <span>Varyforte</span>?</span> <br>
                        <!-- <span><span>VaricoFix</span></span> -->
                    </span>
                </h2>
                <p class="white">
                    Varicofix datorită compoziției sale unice, este eficientă și nu provoacă reacții adverse. Aceasta este capabilă de a înlătura umflăturile locale, sentimentele de durere. Folosirea permanentă a cremei îmbunătățește starea vaselor de sânge, crește tonusul lor, acționează ca un tonic. Foarte important mai este faptul că Varicofix înlătură simptomele subiective, ca furnicăturile, senzație de frig în picioare și mâini, obosire și greutate
                </p>


                <!--   <h2 class="headline text-center">
                    <span class="white">
                    <Cum funcționează VaricoFix? <br>
                        <span>Aplicare și păstrare</span>
                    </span>
                </h2>
                <p class="white">
                    Se recomandă aplicarea de 2 ori pe zi. Utilizați-l obligatoriu la sfârșitul zilei, deoarece picioarele sunt cele mai obosite în acest moment al zilei. Puteți aplica fără probleme VaricoFix dimineața înainte de a pleca la serviciu, deoarece nu lasă urme sau pete pe haine, este absorbit imediat de piele. Pentru profilaxie, aplicați o dată pe zi. Datorită ingredientelor sale 100% organice, VaricoFix nu generează efecte secundare și poate fi utilizat de fiecare dată când simțiți discomfort, durere sau greutate în picioare. Păstrați VaricoFix în loc răcoros.
                </p> -->


            </div>
        </div>
        <div class="clear"></div>
    </section>
    <section id="sec5" class="col-12">
        <div class="container">
            <h2 class="headline text-center">Opiniile experților:</h2>
            <div class="comments">
                <div class="comment">
                    <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_5_foto1.jpg" alt="img" class="commentImg">
                    <p>
                        Această cremă s-a recomandat foarte bine, demonstrând rezultate reale, la care pacienții nici nu mai sperau. Se poate folosi și acasă, cel mai bine zilnic, masând ușor porțiunile de piele afectate.
                    </p>
                    <p class="second">
                        Pacienții confirmă, că durerea dispare, precum și oboseala din mâini și picioare, umflăturile sunt ameliorate. Confirm și eu același lucru și recomand Varicofix tuturor!
                    </p>
                    <p class="author"><span><b>Daniel Udrea</b><br>chirurg cu 15 ani de experiență</span></p>
                </div>
            </div>
            <br>

        </div>
        <div class="clear"></div>
    </section>
    <div class="clear"></div>
    <section id="sec6">
        <div class="container">
            <div class="col-12">
                <h2 class="headline text-center">
                    <span class="white">
                        100% compoziție  naturală<br>
                        <span>Varyforte</span>
                    </span>
                </h2>
            </div>

            <div class="clearfix cf"></div>
            <div class="text-center white">
                <p>
                    Ingredientele cremei garantează un rezultat adecvat pentru prevenirea bolii.
                </p>
                <br>
            </div>
        </div>
        <div class="clear"></div>
    </section>
    <section id="sec7" class="col-12">
        <div class="container cont7">
            <h1 class="headline text-center">Caracteristicile cremei pentru tratarea varicozei:</h1>
            <br>
            <div class="col-6 white">
                <div class="task">
                    <b>Îmbunătățește circulația sângelui</b>
                </div>
                <div class="task">
                    <b>Reduce presiunea în vene</b>
                </div>
                <div class="task">
                    <b>Ajută la recuperarea pereților vaselor sangvine</b>
                </div>
                <div class="task">
                    <b>Crește tonusul venelor</b>
                </div>
            </div>
            <div class="col-6 white">
                <div class="task">
                    <b>Crește tonusul venelor</b>
                </div>
                <div class="task">
                    <b>Reduce permeabilitatea capilară și fragilitatea</b>
                </div>
                <div class="task">
                    <b>Reduce permeabilitatea și fragilitatea capilarelor</b>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </section>

    <div class="clear"></div>
    <section id="sec9" class="col-12">
        <div class="container">
            <br>
            <h1 class="text-center white"><b>Varicoza este una dintre cele mai grave boli <br>care trebuie tratată imediat.</b></h1>
            <br>
            <div class="col-3">
                <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_9_foto1.jpg" alt="img" class="img9">
                <p>Vene spider</p>
            </div>
            <div class="col-3">
                <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_9_foto2.jpg" alt="img" class="img9">
                <p>Varice reticulare</p>
            </div>
            <div class="col-3">
                <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_9_foto3.jpg" alt="img" class="img9">
                <p>Venele principale varicoase</p>
            </div>
            <div class="col-3">
                <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_9_foto4.jpg" alt="img" class="img9">
                <p>Răni</p>
            </div>
        </div>
        <div class="clear"></div>
    </section>
    <div class="clear"></div>
    <section id="sec10" class="col-12">
        <div class="container">
            <h1 class="headline text-center">Recenziile clienților:</h1>
            <br>
            <div class="slider-container">
                <div class="owl-carousel">
                    <div class="userComment">
                        <div>
                            <div class="text-center">
                                <i>înainte &amp; după</i>
                            </div>
                            <div class="text-center"><img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_10_foto4.jpg" alt="img" class="userCommentImg"></div>
                            <p class="text-center"><b>Bogdan  Vasilescu <br>65 ani</b></p>
                        </div>
                        <div class="info">
                            <p>
                                Soția mereu îmi spune că am o problemă cu venele. Observ și simt asta și eu. Mâinile îmi sunt acoperite de astfel de plase venoase, că e groaznic să privești. Toată asta provoacă durere, greutate în mâini, senzație permanentă de frig în mâini. Soția mi-a cumpărat această cremă. Am început utilizarea, la început nu observam nici o schimbare, doar o senzație plăcută de căldură. Aproximativ peste 10 zile venele s-au retras și pielea mi-a devenit mai “luminoasă”.
                            </p>
                            <p>
                                Eu și de asta mă bucuram mult! După 1,5 luni am observat că nu mai simțeam durerea obișnuită. Rezultă că umflăturile și durerea a fost înlăturată datorită cremei! Venele mi s-au micșorat și retras și mai mult acum. Plasa nu mai se observă aproape deloc!
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="userComment">
                        <div>
                            <div class="text-center">
                                <i>înainte &amp; după</i>
                            </div>
                            <div class="text-center">
                                <img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_10_foto1.jpg" alt="img" class="userCommentImg">
                            </div>
                            <p class="text-center">
                                <b>Mariana  Mănescu<br>28 ani</b>
                            </p>
                        </div>
                        <div class="info">
                            <p>
                                După perioada în care am fost însărcinată și mai ales după naștere au apărut și la mine problemele cu picioarele. Pe lângă asta că mă dureau mereu, au mai început să-mi apară și venele. La sfârșitul zilei picioarele îmi erau acoperite de adunături de vene! Am vizitat un medic, și anume el m-a sfătuit să folosesc această cremă pentru tratarea varicozei.
                            </p>
                            <p>
                                Mi-a recomandat să o folosesc zilnic, de două ori în 24 de ore. După o lună de folosire am observat că nu mai simțeam durere, picioarele nu îmi mai erau la fel de obosite și aproape că scăpasem de vazicoza.
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="userComment">
                        <div>
                            <div class="text-center">
                                <i>înainte &amp; după</i>
                            </div>
                            <div class="text-center"><img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_10_foto2.jpg" alt="img" class="userCommentImg"></div>
                            <p class="text-center"><b>Floarea Voicu<br>47 ani</b></p>
                        </div>
                        <div class="info">
                            <p>
                                Varicoza – problema mea de o veșnicie! De 10 ani tot folosesc unguente, creme, pastile. Din tot ce am încercat cel mai bun rămâne Varicofix. Nu doar este plăcut, se absoarbe repede, dar și ajută. După folosirea lui nu mai am senzație de mâncărimi, umflături, cum aveam după folosirea altor creme
                            </p>
                            <p>
                                Eu chiar observ că o parte din simptome au fost înlăturate. Da, nu repede, dar dispar toate cu timpul toate! Plus, cu această cremă simt ușurare în picioare. Și încă un plus – nu devine obișnuință. Efect este și atunci când folosești regulat.
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="userComment">
                        <div>
                            <div class="text-center">
                                <i>înainte &amp; după</i>
                            </div>
                            <div class="text-center"><img src="http://st.acstnst.com/content/Varicofix_RO/mobile/images/block_10_foto3.jpg" alt="img" class="userCommentImg"></div>
                            <p class="text-center"><b>Andreea Gutu<br>58 ani</b></p>
                        </div>
                        <div class="info">
                            <p>
                                Mă aflam în a doua stadie a varicozei. Venele pe picioare erau foarte proeminente, pielea uscată. Umflăturile și durerile erau mereu. Fiica mi-a cumpărat această cremă chiar dacă eu nu mai speram la un rezultat.
                            </p>
                            <p>
                                Am folosit-o aproape un an, zilnic, de câteva ori pe zi. Rezultatul m-a șocat. Primul lucru care l-am observat – pielea a devenit moale și catifelată. După, succesiv au început să dispară umflăturile și durerile. Mai târziu, după o lună, a început să dispară și proeminența venelor. Continui să folosesc crema, pentru că văd rezultate care mă bucură!
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </section>

    <section id="footer">
        <div class="col-12 orderBox">
            <div class="form">
                <div class="discount">
                    <p>
                        <b>24%</b>
                        <br> REDUCERE
                    </p>
                </div>
                <p class="title">COMANDAȚI ACUM LA PREȚ PROMOȚIONAL:</p>

                <p>
                    <span class="js_new_price_curs new-price">
                    <strong>159&nbsp;Lei</strong>
                </span>
                </p>
                <p class="title">preț obișnuit:
                    <span class="js_old_price_curs linethrough">
                    <strong>318&nbsp;Lei</strong>
                </span>
                </p>
                <form id="order_form" action="" method="post">
                  <div>
                         <iframe scrolling="no" style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;" src="http://pt.new-power-life.com/forms/?target=-4AAIJIAIbBwAAAAAAAAAAAASrdlQjAA"></iframe>
                    </div>

                </form>
                <div class="clear"></div>
            </div>

        </div>
        <div class="clear"></div>
    </section>
    <footer class="col-12"></footer>
</body>

</html>
<?php } else { ?>
<!DOCTYPE html>
<html>

<head>

    <!-- [pre]land_id = 2004 -->
    <script>var locale = "ro";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAIbBwT9ErV9AAABAAKkBgEAAtQHAQoBDwTA458uAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 20;
        var name_hint = 'Mogos viorica';
        var phone_hint = '+40722559568';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("ro");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">


    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
    <!-- End Facebook Pixel Code -->


    <meta charset="UTF-8">
    <title>Tratarea Varicozei fără operații și proceduri medicale</title>
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta name="keywords" content="Buy VaricoFix, instrucțiuni, preț. VaricoFix – pagina oficială, VaricoFix cumpărați în drogherii, cumpărați VaricoFix">
    <meta name="description" content="Cumpărați VaricoFix – tratament simplu și eficient al varicelor în confortul propriei locuințe">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="http://st.acstnst.com/content/Varicofix_RO/images/logo.png">
    <link rel="stylesheet" href="//st.acstnst.com/content/Varicofix_RO/css/normalize.css">
    <link rel="stylesheet" href="//st.acstnst.com/content/Varicofix_RO/css/styles.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <script>
        $(function () {
            $('[data-scroll]').on('click', function () {
                var target = $(this).data('scroll');

                $('html,body').animate({
                    scrollTop: $(target).offset().top
                });
            });

            $('.js_toggle_reviews').on('click',function(event){
                var $t = $(this);
                $('.js_show_reviews').slideToggle('slow', function(){ $t.hide() });
                return false;
            });
        });
    </script>
    <script type="text/javascript" src="//st.acstnst.com/content/Varicofix_RO/js/scroll.js"></script>
</head>
<body>

<div class="wrapper cf">
    <div id="popWindow"></div>

    <header>
        <div class="center_wrapper">
            <span class="logo"></span>
        </div>
    </header>
    <!-- end of #header -->

    <div class="block_1">
        <div class="center_wrapper">

            <p class="bubble arrow_box">
                fără operații, tratamente<br> medicale inutile
            </p>

            <h1 class="slogon">
                Scăpați de varice
            </h1>
            <div class="stmp stmp1"><p>pentru orice <br> vârstă</p></div>
            <div class="stmp stmp4"></div>
            <div class="stmp stmp2"><p>100% produs natural</p></div>
            <div class="stmp stmp3"><p>Rezultat garantat</p></div>

            <div class="list">
                <p>
                    Folosirea permanentă a cremei, <br> ajută la
                </p>
                <ul>
                    <li>vindecă răni</li>
                    <li>consolidează pereții vaselor sangvine</li>
                    <li>întărirea pereților vaselor de sânge; </li>
                    <li>ameliorează durerea în picioare </li>
                    <li>reduce formarea cheagurilor de sânge</li>
                    <li>îmbunătățește valvele venoase</li>
                    <li>reduce umflăturile țesuturilor moi</li>
                </ul>
            </div>
            <p>
                <b>
                    Lipsa disconfortului
                </b>
                <br>
                (haine pătate, piele lipicioasă)
            </p>
            <div class="form">
                <div class="discount">
                    <p>
                        <b>50%</b>
                        <br>
                        REDUCERE
                    </p>
                </div>
                <p>
                    COMANDAȚI ACUM LA PREȚ PROMOȚIONAL:
                </p>
                <p>
                        <span class="js_new_price_curs">
                            <strong>159&nbsp;Lei</strong>
                        </span>
                </p>
                <p>
                    preț obișnuit:
                        <span class="js_old_price_curs">
                            <strong>318&nbsp;Lei</strong>
                        </span>
                </p>


                <input class="lv-order-button rushOrder scroll" type="submit" value="COMANDA ACUM" data-scroll="#order_form"/>
            </div>
        </div>
    </div>

    <div class="block_2">
        <div class="center_wrapper">
            <p class="headline">
                Crema  împotriva varicozei
                <br>
                <span><span>Varyforte</span></span>
            </p>
            <p>Tratarea complexă – garanția tratării eficiente a varicozei. Și unul din componenții cei mai importanți ai aceste complexități este folosirea cremei împotriva varicozei. Anume ea, ușor și eficient înlătură apariția bolii, ameliorează simptomele, tonifiază, consolidează vasele sangvine.
            </p>
            <p>
                Varyforte este o cremă folosită împotriva varicozei, se folosește extern, se consideră o modalitate extraordinară pentru profilactică și tratare. Pentru dvs va deveni crema  acel “colac de salvare”, care merită folosit mai întâi!
            </p>
        </div>
    </div>

    <div class="block_3">
        <div class="center_wrapper">
            <i class="bg"></i>
            <div class="lable">
                <p>
                    Primele <br>simptome
                </p>
            </div>

            <div class="description">
                <p>Durere </p>
                <p>sentiment de <br> ardere, căldură </p>
                <p>Mâncărime </p>
                <p>Umflături </p>
                <p>Crampe nocturne </p>
                <p>Furnicături </p>
            </div>

            <p class="headline">
                Cauza apariției varicozei (venelor proeminente):
            </p>
            <ul>
                <li>
                    Varicoza moștenită ereditar de la unul din părinți sau rude;
                </li>
                <li>
                    Mod de viață sedentar, pe care sunteți obligat să-l duceți, la muncă;
                </li>
                <li>
                    Diete sărace în fibre inedite;
                </li>
                <li>
                    Purtarea deasă a bikinilor, pantaloni strâmți, pantofi cu tocuri înalte, care afectează negativ fluxul de sânge;
                </li>
                <li>
                    Obișnuința de sta picior peste picior;
                </li>
                <li>
                    Eforturi prea intensive în sala de sport;
                </li>
                <li>
                    Utilizarea preparatelor pe bază de hormoni, exemplu fiind contraceptivele sau medicamentele pentru tratarea răcelii;
                </li>
            </ul>
        </div>
    </div>

    <div class="block_4">
        <div class="center_wrapper cf">
            <div class="block_4_bg"></div>
            <div class="ss">
                <p>Venă sănătoasă</p>
                <p>Venă varicoasă</p>
            </div>

            <p class="headline">
                <span>Cum funcționează crema <span>Varyforte</span>?</span> <br>
            </p>
            <p class="headlinep">
                Varyforte datorită compoziției sale unice, este eficientă și nu provoacă reacții adverse. Aceasta este capabilă de a înlătura umflăturile locale, sentimentele de durere. Folosirea permanentă a cremei îmbunătățește  starea vaselor de sânge, crește tonusul lor, acționează ca un tonic.
                Foarte important mai este faptul că Varyforte înlătură simptomele subiective, ca furnicăturile, senzație de frig în picioare și mâini, obosire și greutate

            </p>

            <!-- <p class="headline">
                <span>Aplicare și păstrare</span>
            </p>
            <p class="headlinep">
                Se recomandă aplicarea de 2 ori pe zi. Utilizați-l obligatoriu la sfârșitul zilei, deoarece picioarele sunt cele mai obosite în acest moment al zilei. Puteți aplica fără probleme VaricoFix dimineața înainte de a pleca la serviciu, deoarece nu lasă urme sau pete pe haine, este absorbit imediat de piele. Pentru profilaxie, aplicați o dată pe zi. Datorită ingredientelor sale 100% organice, VaricoFix nu generează efecte secundare și poate fi utilizat de fiecare dată când simțiți discomfort, durere sau greutate în picioare. Păstrați VaricoFix în loc răcoros.
            </p> -->
        </div>
    </div>

    <div class="block_5">
        <div class="center_wrapper">
            <p class="headline">
                Recenziile specialiștilor:
            </p>
            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_5_foto1.jpg" alt="block_5_foto1">
                <p>
                    nu obosesc să concretizez importanța folosirii cremelor împotriva varicozei într-o complexitate mai amplă pentru tratarea Varicozei! Eficiența lor este greu de supraestimat, mai ales, dacă merge vorba despre preparatul modern Varyforte. Această cremă s-a recomandat foarte bine, demonstrând rezultate reale, la care pacienții nici nu mai sperau. Se poate folosi și acasă, cel mai bine zilnic, masând ușor porțiunile de piele afectate.
                </p>
                <p>
                    Rezultatele apar rapid. Pacienții confirmă, că durerea dispare, precum și oboseala din mâini și picioare, umflăturile sunt ameliorate. Confirm și eu același lucru și recomand Varyforte tuturor!
                </p>
                <p class="">
                        <span>
                            <b>Daniel Udrea</b><br>chirurg 15 de experiență
                        </span>
                </p>
            </div>
            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_5_foto2.jpg" alt="block_5_foto2">
                <p>
                    Sunt des întrebată, care din cremele folosite pentru tratarea Varicozei consider mai eficientă, ușor de folosit și sigur. Care cremă aș putea-o recomanda din suflet pentru proaspetele mămici și bătrânilor. Răspund așa – încercați crema Varyforte!
                </p>
                <p>
                    În practica mea m-am ciocnit deseori, cu faptul că doar această cremă dădea rezultate reale și bune, doar ea înlătura simptomele de durere, și de la o zi la alta, aducea venele la starea lor normală. Multe unguente conțin chimicale care pot provoca alergii, însă Varyforte nu a înregistrat nici un astfel de caz. Merită de încercat pentru a vedea efectul!
                </p>
                <p class="">
                    <span>
                        <b>Elisaveta Codrescu</b><br>medic
                    </span>
                </p>
            </div>
        </div>
    </div>

    <div class="block_6">
        <div class="center_wrapper">
            <p class="headline">
                100% compoziție  naturală
                <br>
                <span>
                    <span>Varyforte</span>
                </span>
            </p>
            <p>
                <b>
                    Cremă pentru vasele varicoase
                </b>
                <br>
                atrageți atenția la ingredientele active din componența acesteia!
            </p>
            <div class="cf" style="margin-bottom: 20px;">

                <div class="clearup" style="margin-bottom: 120px;">
                    <p class="clear">
                        Pentru a alege o cremă pentru tratarea venelor varicoase e nevoie să fiți atenți la componența preparatului. Eficiența preparatului depinde de ingredientele active din componența ei
                    </p>
                </div>

                <div class="left">
                    <div>
                        <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_6_pic1.png" alt="block_6_pic1">
                        <p>
                            <b>Arnică (Arnica Montana)</b><br>
                            Ameliorează umflăturile și durerea articulațiilor, deține un efect puternic de tonifiere a venelor, și participă la absorbția cheagurilor din vene.
                        </p>
                    </div>

                    <div>
                        <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_6_pic2.png" alt="block_6_pic2">
                        <p>
                            <b>Splinuța (Solidago Virgaurea)</b><br> Crește viteza circulației de sânge în vene, reduce umflăturile, crește fluxul de sânge, ajută la eliminarea cheagurilor de sînge formate.
                        </p>
                    </div>


                </div>

                <div class="right">
                    <!--   <div>
                           <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_6_pic4.png" alt="block_6_pic4">
                           <p>
                               <b>Apitoxină</b><br> îmbunătățește circulația sangvină și reduce spasmele musculare.
                           </p>
                       </div>
                   -->
                    <div>
                        <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_6_pic5.png" alt="block_6_pic5">
                        <p>
                            <b>Ruscus Aculeatus(Ghimpe) </b><br> au un efect regenerator și hemostatic.
                        </p>
                    </div>
                    <div>
                        <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_6_pic3.png" alt="block_6_pic3">
                        <p>
                            <b>Extract din coajă de lămâie </b><br> Crește tonusul pereților venelor, făcându-i mai elastici, de altfel participă la creșterea vitezei schimburilor de substanțe.
                        </p>
                    </div>
                    <!--
                        <div>
                            <p>
                                <b>Ulei de măsline </b><br>ingredien natural, stimulează fluxul de sânge.
                            </p>
                        </div>

                        <div>
                            <p>
                                <b>Extractul de albine Podmore </b><br>înlătură umflăturile, durerea.
                            </p>
                        </div>
                    -->
                </div>

                <div class="cf"></div>

            </div>
            <p>
                Ingredientele cremei garantează un rezultat adecvat pentru prevenirea bolii.
            </p>
            <p>
                Scopul principal al acestei creme – micșorarea și înlăturarea durerii și a simptomelor, subțierea sângelui, tonifiere, întărirea venelor și prevenirea formării cheagurilor.
            </p>
        </div>
    </div>

    <div class="block_7">
        <div class="center_wrapper">
            <p class="headline">
                Caracteristicile cremei pentru tratarea varicozei:
            </p>
            <p>
                Oferă omului rezistență și performanță mai ridicată, înlătură simptomele ce dau senzație de slăbiciune  : reduce umflăturile, greutatea în picioare, mâncărimile, furnicăturile, protejează de sindromul picioarelor obosite.
            </p>
            <div>
                <div>
                    <p>Îmbunătățește circulația sângelui</p>
                    <p>Reduce presiunea în vene</p>
                    <p>Ajută la recuperarea pereților vaselor sangvine</p>
                    <p>Crește tonusul venelor</p>
                </div>
                <div>
                    <p>Crește tonusul venelor </p>
                    <p>Reduce permeabilitatea capilară și fragilitatea </p>
                    <p>Reduce permeabilitatea și fragilitatea capilarelor </p>
                </div>
            </div>
        </div>
    </div>

    <div class="block_8">
        <div class="center_wrapper">
            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_8_ico1.png" alt="block_8_ico1">
                <p>
                    Fără  <br>intervenții chirurgicale
                </p>

            </div>

            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_8_ico2.png" alt="block_8_ico2">
                <p>
                    Fără <br>medicamente scumpe
                </p>
            </div>

            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_8_ico3.png" alt="block_8_ico3">
                <p>
                    Fără durere și prejudicii pentru <br>sănătate
                </p>
            </div>
        </div>
    </div>

    <div class="block_9">
        <div class="center_wrapper">
            <p class="headline">
                Varicoza este una dintre cele mai grave boli <br>care trebuie tratată imediat.
            </p>
            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_9_foto1.jpg" alt="block_9_foto1">
                <p>vene spider</p>

            </div>
            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_9_foto2.jpg" alt="block_9_foto2">
                <p>varice reticulare </p>

            </div>
            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_9_foto3.jpg" alt="block_9_foto3">
                <p>Venele principale varicoase </p>

            </div>
            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_9_foto4.jpg" alt="block_9_foto4">
                <p>insuficiență venoasă cronică </p>
            </div>
            <div>
                <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_9_foto5.jpg" alt="block_9_foto5">
                <p>răni </p>
            </div>
        </div>
    </div>
    <div class="block_10">
        <div class="center_wrapper">
            <p class="headline">
                Recenziile  clienților:
            </p>
            <div>
                <div>
                    <i>înainte &amp; după</i>
                    <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_10_foto1.jpg" alt="block_10_foto1">
                    <p>Mariana  Mănescu<br>28 ani</p>
                </div>
                <p>
                    După perioada în care am fost însărcinată și mai ales după naștere au apărut și la mine problemele cu picioarele. Pe lângă asta că mă dureau mereu, au mai început să-mi apară și venele. La sfârșitul zilei picioarele îmi erau acoperite de adunături de vene! Arăta dezastruos asta. Am vizitat un medic, și anume el m-a sfătuit să folosesc această cremă pentru tratarea varicozei.
                </p>
                <p>
                    Mi-a recomandat să o folosesc zilnic, de două ori în 24 de ore. Primul rezultat l-am văzut după 5 zile. Venele la sfârșitul zilei nu mai erau la fel de proeminente, arătau aproape normal. După o lună de folosire am observat că nu mai simțeam durere, picioarele nu îmi mai erau la fel de obosite și aproape că scăpasem de vazicoza. Continui tratarea!
                </p>
            </div>
            <div>
                <div>
                    <i>înainte &amp; după</i>
                    <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_10_foto2.jpg" alt="block_10_foto2">
                    <p>
                        Floarea Voicu<br>47 ani
                    </p>
                </div>

                <p>
                    Varicoza – problema mea de o veșnicie! De 10 ani tot folosesc unguente, creme, pastile. Din tot ce am încercat cel mai bun rămâne Varyforte. Nu doar este plăcut, se absoarbe repede, dar și ajută. După folosirea lui nu mai am senzație de mâncărimi, umflături, cum aveam după folosirea altor creme
                <p>
                    Eu chiar observ că o parte din simptome au fost înlăturate. Da, nu repede, dar dispar toate cu timpul toate!
                    Plus, cu această cremă simt ușurare în picioare. Și încă un plus – nu devine obișnuință. Efect este și atunci când folosești regulat.

                </p>
            </div>

            <div>
                <div>
                    <i>înainte &amp; după</i>
                    <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_10_foto3.jpg" alt="block_10_foto3">
                    <p>
                        Andreea Gutu<br>58 ani
                    </p>
                </div>
                <p>
                    Mă aflam în a doua stadie a varicozei. Venele pe picioare erau foarte proeminente, pielea uscată. Umflăturile și durerile erau mereu. Fiica mi-a cumpărat această cremă chiar dacă eu nu mai speram la un rezultat.
                </p>
                <p>
                    Am folosit-o aproape un an, zilnic, de câteva ori pe zi. Rezultatul m-a șocat. Primul lucru care l-am observat – pielea a devenit moale și catifelată. După, succesiv au început să dispară umflăturile și durerile. Mai târziu, după o lună, a început să dispară și proeminența venelor. Continui să folosesc crema, pentru că văd rezultate care mă bucură!

                </p>
            </div>

            <div>
                <div>
                    <i>înainte &amp; după</i>
                    <img src="http://st.acstnst.com/content/Varicofix_RO/images/block_10_foto4.jpg" alt="block_10_foto4">
                    <p>
                        Bogdan  Vasilescu <br>65 ani
                    </p>
                </div>

                <p>
                    Soția mereu îmi spune că am o problemă cu venele. Observ și simt asta și eu. Mâinile îmi sunt acoperite de astfel de plase venoase, că e groaznic să privești. Toată asta provoacă durere, greutate în mâini, senzație permanentă de frig în mâini. Soția mi-a cumpărat această cremă. Am început utilizarea, la început nu observam nici o schimbare, doar o senzație plăcută de căldură. Aproximativ peste 10 zile venele s-au retras și pielea mi-a devenit mai “luminoasă”.
                </p>
                <p>
                    Eu și de asta mă bucuram mult! După 1,5 luni am observat că nu mai simțeam durerea obișnuită. Rezultă că umflăturile și durerea a fost înlăturată datorită cremei! Venele mi s-au micșorat și retras și mai mult acum. Plasa nu mai se observă aproape deloc!
                </p>
            </div>
        </div>
    </div>

    <div class="block_11">
        <div class="center_wrapper">
            <i class="bg"></i>

            <div class="dsc_box">
                <div class="discount">
                    <p>
                        <span class="js_percent_sign"><b>50%</b></span>
                        <br>
                        REDUCERE
                    </p>
                </div>
                <p>Comandați acum</p>
                <p>
                    la preț promoțional:
                    <span class="js_new_price_curs">
                        <strong>159&nbsp;Lei</strong>
                    </span>
                </p>
                <p>
                    Preț obișnuit:
                    <span class="js_old_price_curs">
                        <strong> 318&nbsp;Lei</strong>
                    </span>
                </p>
                <form id="order_form"  action="" method="post">
                    <iframe scrolling="no" style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;" src="http://pt.new-power-life.com/forms/?target=-4AAIJIAIbBwAAAAAAAAAAAASrdlQjAA"></iframe>
                </form>
            </div>
        </div>
    </div>
    <div class="bffoot">
        Produsul este certificat, componența, modul și instrucțiunile de folosire sunt prezentate pe ambalaj.
        <br>
        <br>
    </div>
</div>
<div></div>
</body>
</html>
<?php } ?>