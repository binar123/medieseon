<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 1961 -->
    <script>var locale = "hu";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAIcBwRjZrV9AAABAAKlBgEAAqkHAQoBDwRn9y1TAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
    <!-- End Facebook Pixel Code -->

    <meta charset="utf-8"/>
    <title>Varyforte</title>
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta content="Buy Varyforte , instrucțiuni, preț. Varyforte  – pagina oficială, Varyforte  cumpărați în drogherii, cumpărați Varyforte " name="keywords"/>
    <meta content="Cumpărați Varyforte  – tratament simplu și eficient al varicelor în confortul propriei locuințe" name="description"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <link href="//st.acstnst.com/content/Variforte_HU/images/logo.png" rel="shortcut icon" type="image/x-icon"/>
    <link href="//st.acstnst.com/content/Variforte_HU/css/normalize.css" rel="stylesheet"/>
    <link href="//st.acstnst.com/content/Variforte_HU/css/styles.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css"/>
    <script src="//st.acstnst.com/content/Variforte_HU/js/privacy.js"></script>
    <script>
        $(function(){
            $('[data-scroll]').on('click', function(){
                $('html,body').animate({
                    scrollTop: $('#order_form').offset().top
                }, 700);

                return false;
            });
        });
    </script>
</head>
<body>
<div class="wrapper cf">
    <div id="popWindow"></div>
    <header>
        <div class="center_wrapper">
            <span class="logo"></span>
        </div>
        <!--<span class="phone_number">
                <img src="images/phone.png" alt="phone">
                Contactati-ne: &nbsp;
                40312295125
            </span>-->
    </header>
    <!-- end of #header -->
    <div class="block_1">
        <div class="center_wrapper">
            <img alt="" class="for_product" src="//st.acstnst.com/content/Variforte_HU/images/product_new.png"/>
            <div class="list">
                <p>
                    What is a Varicose Vein?
                </p>
                Vein Anatomy:
                As the arteries carry blood away from the heart, the veins return blood to the heart. They are very thin walled and subsequently are prone to stretching and swelling. This swelling is what is often referred to as varicosities. Because the veins have nerves associated with them, as they stretch, they become painful. Additionally, there are valves which support the blood on its journey back up to the heart. In varicose veins, these valves are defective which causes the swelling associated with this condition.
            </div>
            <p>
                <b>
                    Why do I have Varicose Veins?
                </b>
                <br/>
                Often, varicose veins are inherited from your parents.  Many times we see patients whose parents and grandparents have varicose veins.  Other causes of varicose veins include pregnancy, obesity, repeated abdominal straining, as with heavy lifting, and long periods of standing.
            </p>
        </div>
    </div>
    <div class="block_2">
        <div class="center_wrapper">
            <p class="headline">
                EVLT
            </p>
            <p>
                Endovascular Laser Therapy is an excellent method for treating primary thigh varicosities in an office setting. In this innovative treatment, we begin by numbing up the skin with a small amount of lidocaine. Then we place a small I.V. in the vein and a tiny laser is placed inside the varicose vein and into the defective valve. The laser is turned on and the vein is treated. The vein remains in place but is collapsed and effectively obliterated. The procedure is associated with very little pain and the patient may return to work the next day. You may return to normal activity within hours including exercise and normal working conditions. We do ask that you wear fitted compression stockings for 5 days after the procedure. We have these in our office and will fit you for them at the end of the procedure.
            </p>
        </div>
        <div class="center_wrapper">
            <p class="headline">
                <b>How Are Varicose Veins Treated?</b>
            </p>
            <p>
                There are many ways to treat varicose veins depending on the severity of the disease and the size and location of the varicose veins as well as the individual patient. Here at The Tennessee Vein and Aesthetic Laser Center we individualize treatment based on each patient's needs and vein anatomy.
                Historically varicose veins were treated with a vein stripping operation. This operation involved taking the patient to the operating room, putting them to sleep, making 20-30 incisions on their leg, and ripping the vein out. This operation was rather brutal with a significant amount of pain and bruising and usually required at least a week off work. The worst part about this operation is that up to 40% of people had recurrences of their veins.
            </p>

            <p>
                The reason that vein stripping has such a high recurrence rate is two fold. Primarily, it is because the underlying problem is not addressed. Most people develop varicose veins because a valve in their saphenous vein fails. The vein stripping operation did not do anything to address this failed valve and consequently the veins recurred. Secondly, veins tend to re-grow. This is a good feature if, for instance, you have injured yourself. In this case you would want blood vessels to grow into the injury to nourish the area that was injured and allow it to heal faster. However, in the case of varicose veins this allows the veins
            </p>
            <p>
                New Technique
                Fortunately, there is a new technique that has revolutionalized vein treatment. The Endovenous Laser Therapy or EVLT utilizes an 880 nm diode laser the effectively seal the defective valve, thus correcting the underlying problem.
            </p>
            <p>
                to re-grow and hook back up to the defective valve and cause the varicose veins to recur.  While we used to perform this procedure, we have abandoned it in favor of newer more effective techniques.
            </p>
        </div>
        <div class="center_wrapper">
            <p class="headline">
                Varicose Veins and Spider Veins
            </p>
            <p>
            <p>
                Varicose veins and spider veins affect 25% of women and 15% men. These people may have pain, swelling, or a heavy feeling in their legs in addition to the unsightly bulging veins in their legs.
            </p>
            </p>
        </div>
        <div class="center_wrapper">
            <p>
                Laser therapy utilizes a monochromatic light source that shines through the skin and causes the vein to collapse. It is relatively painless and can be accomplished in an office setting in three or four sessions. We use the latest Laserscope Lyra laser which effortlessly treats spider and reticular veins. The laser is especially effective for facial veins.
            </p>
            <p>
                Compression Therapy
                Compression therapy is of some short term benefit and is usually employed for those patients who do not want further treatment or as an adjunctive treatment in concert with other more effective treatments.
                To learn more about treatment options for varicose veins or to schedule your individualized treatment plan, please order our Guide!
                Order Now
            </p>
        </div>
        <div class="center_wrapper">
            <p class="headline">
                TRIVEX Therapy
            </p>
            <p>
                Transilluminated Powered Phlebectomy or TRIVEX is an exciting new treatment option for large varicosities of the leg. This innovative new therapy uses several tiny incisions to remove the vein under general anesthesia. The patient is able to return to work in a few days and has very little pain that it usually controlled with ibuprofen.
                Ambulatory Phlebectomy is an office procedure that is effective for varicose veins that either have a normal valve or one that has previously been repaired. In this procedure, we numb the skin and use a tiny needle like hook to tease the vein out. The procedure is very effective and you can return to work the same day. We do ask that you wear your compression hose for three days.
            </p>
        </div>
    </div>

    <div class="block_4">
        <div class="center_wrapper cf">
            <div class="block_4_bg"></div>

            <p>Sclerotherapy
                Sclerotherapy is effective for small spider veins. These veins are just underneath the skin and respond well to sclerotherapy. The technique involves placing a medication into the vein which causes it to collapse. We use a needle that is so small it is difficult to see without magnification. Many people use hypertonic saline as their primary sclerosant. While we used saline for many years, we found it to be rather painful and very ineffective. Additionally, saline has some serious side effects such as ulceration and staining of the skin which can be permanent. We use a naturally occurring substance which is gentle and much more effective. It generally requires four treatments about one month apart to completely erase your spider veins.</p>

        </div>
    </div>
</div>
<div id="privacy-policy" style="display:none;">
    <div class="block_more_info">
        <style scoped="">
            body, html{
                min-height: 100%;
                margin:0px;
                padding: 0px;

            }
            .block_more_info{
                width: 800px;
                margin: 8px auto 20px;
                background: #fff;
                font-family: Arial;
                padding: 20px 40px 40px 40px;
                border: 1px solid #DADADA;
                line-height: 20px;
            }
            .block_more_info h1{
                color: #3B6A7C;
                margin-bottom: 30px;
                margin-top: 21px;
                text-align: center;
                font-size: 32px;
                font-weight: bold;
            }
            .block_more_info p {
                color: #000000;
                font-size: 16px;
                margin: 16px 0;
            }
            .s1{
                font-style: italic;
                text-align: center;
                margin: 40px 0 0 0;
                font-weight: bold;
            }
            h2{
                font-size: 16px;
                margin-top: 26px;
                font-weight: bold;
            }
        </style>
    </div>
</div>
</body>
</html>