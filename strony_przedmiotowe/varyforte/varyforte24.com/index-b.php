<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()){ ?>

<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 1961 -->
    <script>var locale = "hu";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAIcBwTbQCWEAAABAAKlBgEAAqkHAQoBDwT2AEd0AA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
    .ac_footer {
        position: relative;
        top: 10px;
        height:0;
        text-align: center;
        margin-bottom: 70px;
        color: #A12000;
    }
    .ac_footer a {
        color: #A12000;
    }
    img[height="1"], img[width="1"] {
        display: none !important;
    }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 1500;
        var name_hint = 'Farkas gyula';
        var phone_hint = '+36309489465';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
            '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
            ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("hu");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1666009176948198');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->



<meta charset="utf-8"/>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<title>Varyforte</title>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<meta content="width=device-width" name="viewport"/>
<link href="//st.acstnst.com/content/Variforte_HU/mobile/css/style.css" rel="stylesheet"/>
<link href="//st.acstnst.com/content/Variforte_HU/mobile/css/owl.carousel.css" rel="stylesheet"/>
<link href="//st.acstnst.com/content/Variforte_HU/mobile/css/owl.theme.css" rel="stylesheet"/>
<script src="//st.acstnst.com/content/Variforte_HU/mobile/js/owl.carousel.min.js"></script>
<script src="//st.acstnst.com/content/Variforte_HU/mobile/js/jquery.placeholder.js" type="text/javascript"></script>
<script src="//st.acstnst.com/content/Variforte_HU/mobile/js/common.js" type="text/javascript"></script>
<script>
        $(function () {
            $('.scroll-to-order').on('click', function (event) {
                var e = event || window.event;
                e.returnValue = false;
                e.preventDefault();

                $('html,body').animate({
                    scrollTop: $('#orderForm').offset().top
                }, 700);
            });
        });
    </script>
</head>
<body class="root">
<div class="wrapper ">
<header>
<div class="page-wrapper__inner transitionall">
<a class="show" href="#">
<img alt="gsdgsd" class="center-block" src="//st.acstnst.com/content/Variforte_HU/mobile/img/header_logo_1.png"/>
</a>
<img alt="" class="product_img" src="//st.acstnst.com/content/Variforte_HU/mobile/img/product_new.png" width="100"/>
<div class="icoblock"></div>
<h1>Megszabadulni Visszér</h1>
<div class="c-blue text-center">Megszabadulni a visszerektől mütét és orvosi beavatkozás nélkül</div>
<div class="mt20"></div>
<div class="text-center">Rendes ár: <span class="tdlh">99 eur.</span></div>
<div class="newprice text-center"> Rendelés kedvező áron: 60 eur </div>
<div class="for-event button scroll-to-order">Megrendelni</div>
</div>
</header>
<div class="slide2">
<div class="page-wrapper__inner">
<ul>
<li>gyógyítani sebeket;</li>
<li>helyreállítani a természetes vér áramlását;</li>
<li>erősíti az érfalakat;</li>
<li>csökkenti a fájdalmat a lábakban;</li>
<li>csökkenti a vérrögök kialakulását;</li>
<li>javítja az vénaszelepeket;</li>
<li>megszüntetni a láb duzzadást.</li>
</ul>
</div>
</div>
<div class="slide3">
<div class="page-wrapper__inner">
<h2 class="c-blue">
<b>Visszér elleni krém<br/>Varyforte</b>
</h2>
<!--<img alt="gg" class="imgs3" src="img/imgs3.png">-->
<p>Átfogó alkalmazása - sikeres megszabadulás visszerektől, és a legfontosabb összetevője ennek  a komplexusnak -  visszérelleni krém használata. Pontosan ez a krém gyengéden és hatékonyan megszünteti a tüneteket  és erősíti a vérereket.</p>
</div>
</div>
<div class="slide4">
<div class="page-wrapper__inner">
<h2 class="text-center"><b>Hogyan működik </b></h2>
<div class="text-center s4">
<img alt="s4" src="//st.acstnst.com/content/Variforte_HU/mobile/img/s4i.jpg"/>
</div>
<p><span class="s4arrow"></span>Varyforte Az egyedülálló összetételének köszönhetően hatékonyan és biztonságosan működik. Képes eltüntetni helyi gyulladást c sökkentetti a duzzanatát, fájdalmát. Ha rendszeresen használja, a krém javítja a vérerek állapotát, növeli a rugalmasságukat frissítő hatású.A krém megszünteti a szubjektív tüneteket is mint a bizsergés, tűszúrás szerű fájdalom, hideg kéz és láb érzést, a végtagok fáradtságát, az ún. nehéz láb érzést..</p>
</div>
</div>
<div class="slide5">
<div class="page-wrapper__inner">
<h2 class="c-blue text-center">
<b>
                         Vásárlói vélemények:
                    </b>
</h2>
<div class="mt20"></div>
<ul class="bxslider">
<li>
<div class="dtable">
<!--<div class="dtc">
                                <img src="img/client3.jpg" alt="gdsg">
                            </div>-->
<div class="dtc revname">
                                Bedő Zsuzsi
                                <br/> 28 éves
                            </div>
</div>
<p>
                            A terhesség és a szülés után kezdöttek problémák a lábammal. Nem csak hogy állandóan fájt ,egyre jóbban fájt ,és az erek kezdett kidudorodni. A lábaim tele voltak vénás "dudorokkal" a nap végére! Ez szörnyen nézett ki.

Elmentem az orvoshoz. Ezt a visszér kenőcsőt ajánlotta nekem.Naponta 2-szer kellett használni.. Az első eredményt 5 nap múlva láttam. Az erek nap végére már alig látszottak. És egy hónap használata után teljesen eltüntek, már nem fájtak a lábak, és eltüntek a duzzanatok. Folytatom a kezelést!
                        </p>
<div class="text-center">
<img alt="ggg" src="//st.acstnst.com/content/Variforte_HU/mobile/img/block_10_foto1.jpg"/>
</div>
<div class="mt20"></div>
</li>
<li>
<div class="dtable">
<!--<div class="dtc">
                                <img src="img/client2.jpg" alt="gdsg">
                            </div>-->
<div class="dtc revname">
                               Németh Béláné
                                <br/>47 éves
                            </div>
</div>
<p>
                             A visszértágulat – az örök problémám! Csaknem 10 éve kenőcsöket, krémeket, tablettákat használok. Mindenből, amit kipróbáltam, a legjobb Varyforte krém. Gyorsan felszívódik , utána nincs viszketés, kiütések a bőrön, mint a más krémektől. Én látom, hogy a duzzanat és gyulladás is megszünik. Nem gyorsan , de eltünnek.

Ezen kívül könnyebnek érzem a lábamat. És még egy dolog - nincs megszokási effektusa. A krém hatásos akkor is, ha h rendszeresen használom.
                        </p>
<div class="text-center">
<img alt="ggg" src="//st.acstnst.com/content/Variforte_HU/mobile/img/block_10_foto2.jpg"/>
</div>
<div class="mt20"></div>
</li>
<li>
<div class="dtable">
<!--<div class="dtc">
                                <img src="img/client1.png" alt="gdsg">
                            </div>-->
<div class="dtc revname">
                               Maria S.
                                <br/>58 éves
                            </div>
</div>
<p>
                            Második stádiumu visszérbetegségem volt. Az erek a lábamon nagyon láthatóak voltak. Be voltak duzzadva és fájtak folyamatosan. Lányom vette nekem ezt a krémet, de én már nem hittem semmiben .

Kentem majdnem egy évig, minden nap, naponta többször. Az eredmény meglepett. Az első, ami feltűnt - a bőröm hidratált és fiatalabb lett. Majd fokozatosan eltünt fájdalom és a duzzanatok. Később, körülbelül egy havi használata után csökkentek a dudorok az ereken. Én továbbra is ezt a krémet használom, mert látom az eredményt, és ez nagyon boldogít!
                        </p>
<div class="text-center">
<img alt="ggg" src="//st.acstnst.com/content/Variforte_HU/mobile/img/block_10_foto3.jpg"/>
</div>
<div class="mt20"></div>
</li>
<li>
<div class="dtable">
<!--<div class="dtc">
                                <img src="img/client1.png" alt="gdsg">
                            </div>-->
<div class="dtc revname">
                               Sóma László
                                <br/>65 éves
                            </div>
</div>
<p> A feleségem mindig mondta, hogy problémásak az ereim. Én magam is ezt érzem. A kezem tele vannak seprűérrel, rossz rá nézni. Nagyon fáj, és mindig hideg a kezem.

Ezt a kenőcsőt a feleségem vásárolta. Elkeztem használni. Eleinte nem volt semmi, csak egy kellemes melegség érzés. Körülbelül 10 nap múlva enyhén csökkentek és halványultak az erek. Örültem! Aztán másfél hónap múlva elmúlt a fájdalom . Úgy látszik a krém levette a gyulladást ! Az erek még kisebbek lettek.. A söprűerek már nem néznek ki olyan félelmetesnek és eltünt a fájdalom. </p>
<div class="text-center">
<img alt="ggg" src="//st.acstnst.com/content/Variforte_HU/mobile/img/block_10_foto4.jpg"/>
</div>
<div class="mt20"></div>
</li>
</ul>
</div>
</div>
<footer style="background: #1b69aa !important;">
<div class="page-wrapper__inner transitionall">
<h2><b>Rendelés kedvező áron <br/> 50% kedvezmény!</b></h2>
<form action="" id="orderForm" method="post"><input type="hidden" name="esub" >


<div class="dtable">
<div class="w50 dtc">
    Rendes  ár:
<br/>
<span class="tdlh">24440 Ft.</span>
</div>
<div class="dtc text-right fs24f">
  Kedvező áron: 12220 Ft.
</div>

<iframe scrolling="no" style="border:0; left:0; right:0; bottom:0; width:100%; height:230px;" src="http://pt.new-power-life.com/forms/?target=-4AAIJIAIcBwAAAAAAAAAAAATyAn20AA"></iframe>
</div>
</form>
</div>
</footer>
</div>
<!-- .wrapper -->

</body>
</html>

<?php } else { ?>


<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 1961 -->
    <script>var locale = "hu";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAIcBwRjZrV9AAABAAKlBgEAAqkHAQoBDwRn9y1TAA";</script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 1500;
        var name_hint = 'Farkas gyula';
        var phone_hint = '+36309489465';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("hu");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
    <!-- End Facebook Pixel Code -->



    <meta charset="utf-8"/>
    <title>Varyforte</title>
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta content="Buy Varyforte , instrucțiuni, preț. Varyforte  – pagina oficială, Varyforte  cumpărați în drogherii, cumpărați Varyforte " name="keywords"/>
    <meta content="Cumpărați Varyforte  – tratament simplu și eficient al varicelor în confortul propriei locuințe" name="description"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <link href="//st.acstnst.com/content/Variforte_HU/images/logo.png" rel="shortcut icon" type="image/x-icon"/>
    <link href="//st.acstnst.com/content/Variforte_HU/css/normalize.css" rel="stylesheet"/>
    <link href="//st.acstnst.com/content/Variforte_HU/css/styles.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css"/>
    <script src="//st.acstnst.com/content/Variforte_HU/js/privacy.js"></script>
    <script>
        $(function(){
            $('[data-scroll]').on('click', function(){
                $('html,body').animate({
                    scrollTop: $('#order_form').offset().top
                }, 700);

                return false;
            });
        });
    </script>
</head>
<body>
<div class="wrapper cf">
    <div id="popWindow"></div>
    <header>
        <div class="center_wrapper">
            <span class="logo"></span>
        </div>
        <!--<span class="phone_number">
                <img src="images/phone.png" alt="phone">
                Contactati-ne: &nbsp;
                40312295125
            </span>-->
    </header>
    <!-- end of #header -->
    <div class="block_1">
        <div class="center_wrapper">
            <img alt="" class="for_product" src="//st.acstnst.com/content/Variforte_HU/images/product_new.png"/>
            <p class="bubble arrow_box">
                Megszabadulni a visszerektől
                <br/>
                mütét és orvosi beavatkozás nélkül
            </p>
            <h1 class="slogon">
                Megszabadulni Visszér
            </h1>
            <div class="stmp stmp1"><p>Alkalmas <br/> minden <br/> korosztály <br/> számára</p></div>
            <div class="stmp stmp4"></div>
            <div class="stmp stmp2"><p>Természetes</p></div>
            <div class="stmp stmp3">
                <p>
                    garantált az eredmény
                </p>
            </div>
            <div class="list">
                <p>
                    A krém rendszeres  használata után segít:
                </p>
                <ul>
                    <li>gyógyítani sebeket;</li>
                    <li>helyreállítani a természetes vér áramlását;</li>
                    <li>erősíti az  érfalakat;</li>
                    <li>csökkenti a fájdalmat a lábakban;</li>
                    <li>csökkenti a vérrögök kialakulását;</li>
                    <li>javítja az vénaszelepeket;</li>
                    <li>megszüntetni a láb duzzadást.</li>
                </ul>
            </div>
            <p>
                <b>
                    probléma mentes
                </b>
                <br/>
                (nem foltos a ruha, mivoltának a bőrt)
            </p>
            <div class="form">
                <div class="discount">
                    <p>
                        <b>50%</b>
                        <br/>
                        kedvezmény
                    </p>
                </div>
                <p>
                    Rendelés kedvező áron:
                </p>
                <p>
<span class="js_new_price_curs">
<strong>12220 ƒ</strong>
</span>
                </p>
                <p>
                    rendes  ár:
                    <span class="js_old_price_curs">
<strong>24440 ƒ</strong>
</span>
                </p>
                <form action="/order/create/" class="lv-order-form lv-order-form-css" id="form-index" method="post"><input type="hidden" name="esub" value="-4A25sMQIJIAIcBwRjZrV9AAABAAKlBgEAAqkHAQoBDwRn9y1TAA">
                    <input type="hidden" name="shipment_vat" value="0.0">
                    <input type="hidden" name="ip_city" value="Lublin">
                    <input type="hidden" name="template_name" value="Variforte_HU">
                    <input type="hidden" name="price" value="12220">
                    <input type="hidden" name="old_price" value="24440">
                    <input type="hidden" name="currency" value="ƒ">
                    <input type="hidden" name="package_id" value="0">
                    <input type="hidden" name="country_code" value="HU">
                    <input type="hidden" name="ip_country" value="PL">
                    <input type="hidden" name="total_price" value="13720.0">
                    <input type="hidden" name="shipment_price" value="1500">
                    <input type="hidden" name="goods_id" value="92">
                    <input type="hidden" name="title" value="Variforte - HU">
                    <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                    <input type="hidden" name="total_price_wo_shipping" value="12220.0">
                    <input type="hidden" name="package_prices" value="{}">
                    <input type="hidden" name="protected" value="None">
                    <input type="hidden" name="price_vat" value="0.0">

                    <input class="lv-order-button rushOrder" data-scroll="" type="submit" value="Megrendelni"/>
                </form>
            </div>
        </div>
    </div>
    <div class="block_2">
        <div class="center_wrapper">
            <p class="headline">
                Visszér elleni krém
                <br/>
                <span><span>Varyforte</span></span>
            </p>
            <p>
                Átfogó alkalmazása - sikeres megszabadulás visszerektől, és a legfontosabb összetevője ennek  a komplexusnak -  visszérelleni krém használata. Pontosan ez a krém gyengéden és hatékonyan megszünteti a tüneteket  és erősíti a vérereket.
            </p>
            <p>
                Visszérelleni krém Varyforte . Külsőleg alkalmazzuk. Kiváló  módszer a betegség megelőzésre és a kezelésre.
            </p>
        </div>
    </div>
    <div class="block_3">
        <div class="center_wrapper">
            <i class="bg"></i>
            <div class="lable">
                <p>
                    Első <br/> tünetek
                </p>
            </div>
            <div class="description">
                <p>fájdalom</p>
                <p>érzések égő, melegség</p>
                <p>viszketés</p>
                <p>Duzzanat</p>
                <p>Éjszakai görcsök</p>
                <p>bizsergés</p>
            </div>
            <p class="headline">
                Visszérnek az okai:
            </p>
            <ul>
                <li>
                    "Visszér öröklődés"
                </li>
                <li>
                </li>
                <li>
                    örökölt a szüleitől vagy más hozzátartozóitól;
                </li>
                <li>
                    a mozgásszegény életmód,
                </li>
                <li>
                    lelkesedése a  hipermodern diéták után,
                </li>
                <li>
                    a szíjak, szűk nadrág és a magas sarkú cipő viselése veszélybe sodorja a vér áramlását;
                </li>
                <li>
                    szokás – keresztbe tedt lábakkal ülni.
                </li>
            </ul>
        </div>
    </div>
    <div class="block_4">
        <div class="center_wrapper cf">
            <div class="block_4_bg"></div>
            <div class="ss">
                <p>visszér</p>
                <p>egészséges ér</p>
            </div>
            <p class="headline">
                <span>Hogyan működik a krém?</span> <br/>
            </p>
            <p class="headlinep">
                Varyforte  Az  egyedülálló összetételének köszönhetően   hatékonyan és biztonságosan működik. Képes eltüntetni helyi gyulladást  c
                sökkentetti a duzzanatát, fájdalmát. Ha rendszeresen használja,  a krém javítja  a vérerek állapotát, növeli a rugalmasságukat frissítő hatású.A krém megszünteti a szubjektív tüneteket is mint a bizsergés, tűszúrás szerű fájdalom, hideg kéz és láb érzést, a végtagok fáradtságát, az ún. nehéz láb érzést.
            </p>
            <!-- <p class="headline">
                        <span>Aplicare și păstrare</span>
                    </p>
                    <p class="headlinep">
                        Se recomandă aplicarea de 2 ori pe zi. Utilizați-l obligatoriu la sfârșitul zilei, deoarece picioarele sunt cele mai obosite în acest moment al zilei. Puteți aplica fără probleme Varyforte  dimineața Előtt de a pleca la serviciu, deoarece nu lasă urme sau pete pe haine, este absorbit imediat de piele. Pentru profilaxie, aplicați o dată pe zi. Datorită ingredientelor sale 100% organice, Varyforte  nu generează efecte secundare și poate fi utilizat de fiecare dată când simțiți discomfort, durere sau greutate în picioare. Păstrați Varyforte  în loc răcoros.
                    </p>-->
        </div>
    </div>
    <div class="block_5">
        <div class="center_wrapper">
            <p class="headline">
                A szakembernek a véleménye:
            </p>
            <div>
                <img alt="block_5_foto1" src="//st.acstnst.com/content/Variforte_HU/images/block_5_foto1.jpg"/>
                <p>Mindig  hangsúlyozom a visszérelleni krém  alkalmazásának fontosságát  a  betegség komplex kezelésben! A hatékonyságát nehéz túlbecsülni, különösen, ha Varyforte  ról van szó. Ez a krém bebizonyította, megmutatta valódi a valódi eredményeket. Minden nap használja  otthon, egyszerűen be kell dörzsölini . A jótékony hatás meglepően gyors. A betegek azt mondják, hogy a fájdalom eltünik, a fáradtság a lábakban és a kezekben, a duzzanat csökken. Ajánlom Varyforte   mindenkinek!</p>
                <p class="">
<span>
<b>Victor V.</b><br/>sebész 15 éves tapasztalattal
                </span>
                </p>
            </div>
            <div>
                <img alt="block_5_foto2" src="//st.acstnst.com/content/Variforte_HU/images/block_5_foto2.jpg"/>
                <p>Gyakran kérdezik tőlem,melyik  visszér elleni  krémet ajánlom , melyik  a leghatékonyabb, biztonságos és kíméletes főleg  a fiatal anyákszámára. Én csak a Varyforte-et ajánlom !</p>
                <p>
                    A szakmai gyakorlatom alapján szembesültem azzal a ténnyel, hogy csak ez a krém adott  valódi javulást,  csak ez a krém szüntette meg a fájdalom tüneteit, rendbe hozta az ereket. Számos vegyi kenőcs allergiát okozhat, de a Varyforte  nem. Érdemes kipróbálni!
                </p>
                <p class="">
<span>
<b>Dr.Verebes Sándorné Krisztina</b><br/>orvos phlebologist
                </span>
                </p>
            </div>
        </div>
    </div>
    <div class="block_6">
        <div class="center_wrapper">
            <p class="headline">
                100% - ban természetes összetételű
                <br/>
<span>
<span>Visszérelleni krém</span>
</span>
            </p>
            <p>
                <b>
                    Ügyeljen az aktiv hatóanyagokra!
                </b>
                <br/>
                Meg kell ismerni a krém az összetételét .A hatékonysága az aktiv hatóanyagoktól függ.
            </p>
            <div class="cf mb1">

                <div class="left">
                    <div>
                        <img alt="block_6_pic1" style="width:100px" src="//st.acstnst.com/content/Variforte_HU/images/composition-1.png"/>
                        <p>
                            <b>Propolisz</b><br/> A propolisz egy rendkívül hatékony összetevő megerősítését erek és ezért csökkenti a tüneteket a visszerek. Eltekintve attól, hogy egy fanyar, ez is tartalmaz a természetes anyagok, amelyek segítenek csökkenteni a duzzanatot, valamint a fájdalom.</p>
                    </div>
                    <div>
                        <img alt="block_6_pic2" style="width:100px" src="//st.acstnst.com/content/Variforte_HU/images/composition-2.png"/>
                        <p>
                            <b>Olívaolaj</b><br/> A vérkeringés elengedhetetlen kezelésére a visszereknek. Masszírozó olívaolajjal növelheti a vérkeringést, ezáltal csökkenti a fájdalmat és a gyulladást. </p>
                    </div>
                </div>
                <div class="right">
                    <div>
                        <img alt="block_6_pic4" style="width:100px" src="//st.acstnst.com/content/Variforte_HU/images/composition-3.png"/>
                        <p>
                            <b>Cedar Oil</b><br/> Alma Varyforte krém Cedar olaj egy csodálatos kezelés visszér ellen. Ez egy természetes testtisztító termék, és ez is javítja a vér áramlását és a vérkeringésre. Amikor a vér elkezd folyik természetesen a levertség és a duzzanat visszér csökken nagy mértékben.</p>
                    </div>

                </div>
                <div class="cf"></div>
            </div>
            <p>
                A  krém összetevői garantálják a megfelelő eredményeket a betegség megelőzése érdekében.
            </p>
            <p>
                A fő célja a krémek hogy csökkentse és megszüntesse a fájdalmas tüneteket, hígítja a vért, feszesít, erősíti az ereket és csökkenti a vérrög kialakulásának esélyét.
            </p>
        </div>
    </div>
    <div class="block_7">
        <div class="center_wrapper">
            <p class="headline">
                A  krém jellemzői:
            </p>
            <p>
                Increases stamina and performance, improves symptoms caused by bad közérzetet.
            </p>
            <div>
                <div>
                    <p>csökkenti swelling</p>
                    <p>relieve cramps</p>
                    <p>restless legs syndrome hides</p>
                    <p>javítja a vérkeringés</p>
                </div>
                <div>
                    <p>Csökkenti a nyomást az erekben</p>
                    <p>Elősegíti   érfalak gyógyulását csökkenti a duzzanatokat</p>
                    <p>Növeli  a erek a rugalmasságát Csökkenti az erek pangását</p>
                    <p>Tisztítja és javítja a visszereket</p>
                </div>
            </div>
        </div>
    </div>
    <div class="block_8">
        <div class="center_wrapper">
            <div>
                <img alt="block_8_ico1" src="//st.acstnst.com/content/Variforte_HU/images/block_8_ico1.png"/>
                <p>
                    Felesleges sebészi beavatkozás
                </p>
            </div>
            <div>
                <img alt="block_8_ico2" src="//st.acstnst.com/content/Variforte_HU/images/block_8_ico2.png"/>
                <p>
                    költséges gyógyszer összetevők nélkül
                </p>
            </div>
            <div>
                <img alt="block_8_ico3" src="//st.acstnst.com/content/Variforte_HU/images/block_8_ico3.png"/>
                <p>
                    fájdalom és egeszségkárosodás nélkül
                </p>
            </div>
        </div>
    </div>
    <div class="block_9">
        <div class="center_wrapper">
            <p class="headline">
                Visszértágulat - súlyos betegség, amelyet  kezelni kell
            </p>
            <div>
                <img alt="block_9_foto1" src="//st.acstnst.com/content/Variforte_HU/images/block_9_foto1.jpg"/>
                <p>
                    seprűvénák
                </p>
            </div>
            <div>
                <img alt="block_9_foto2" src="//st.acstnst.com/content/Variforte_HU/images/block_9_foto2.jpg"/>
                <p>retikuláris visszér</p>
            </div>
            <div>
                <img alt="block_9_foto3" src="//st.acstnst.com/content/Variforte_HU/images/block_9_foto3.jpg"/>
                <p>Viszeres főerek</p>
            </div>
            <div>
                <img alt="block_9_foto4" src="//st.acstnst.com/content/Variforte_HU/images/block_9_foto4.jpg"/>
                <p>
                    Krónikus vénáselégtelenség
                </p>
            </div>
            <div>
                <img alt="block_9_foto5" src="//st.acstnst.com/content/Variforte_HU/images/block_9_foto5.jpg"/>
                <p>
                    tropikus sebek
                </p>
            </div>
        </div>
    </div>
    <div class="block_10">
        <div class="center_wrapper">
            <p class="headline">
                Vásárlói vélemények:
            </p>
            <div>
                <div>
                    <i>Előtt &amp; utána</i>
                    <img alt="block_10_foto1" src="//st.acstnst.com/content/Variforte_HU/images/block_10_foto1.jpg"/>
                    <p>Bedő Zsuzsi<br/>28 éves</p>
                </div>
                <p>A terhesség és a szülés után kezdöttek problémák a lábammal. Nem csak hogy állandóan fájt ,egyre jóbban fájt ,és az erek  kezdett kidudorodni. A lábaim tele voltak vénás  "dudorokkal"  a nap végére! Ez szörnyen nézett ki.</p>
                <p>Elmentem az orvoshoz.  Ezt a visszér kenőcsőt ajánlotta nekem.Naponta 2-szer kellett használni.. Az első eredményt 5 nap múlva láttam. Az erek nap végére már alig látszottak. És egy hónap használata után teljesen eltüntek, már nem fájtak a lábak, és eltüntek a duzzanatok.  Folytatom a kezelést!</p>
            </div>
            <div>
                <div>
                    <i>Előtt &amp; utána</i>
                    <img alt="block_10_foto2" src="//st.acstnst.com/content/Variforte_HU/images/block_10_foto2.jpg"/>
                    <p>
                        Németh Béláné<br/>47 éves
                    </p>
                </div>
                <p>
                    A visszértágulat – az  örök problémám! Csaknem 10 éve kenőcsöket, krémeket, tablettákat használok. Mindenből, amit kipróbáltam, a legjobb Varyforte  krém.  Gyorsan felszívódik , utána nincs viszketés, kiütések a bőrön, mint a más krémektől. Én  látom, hogy a duzzanat és gyulladás  is  megszünik. Nem gyorsan , de eltünnek.
                </p>
                <p>
                    Ezen kívül könnyebnek érzem a lábamat. És még egy dolog - nincs megszokási effektusa.  A krém hatásos  akkor is, ha h  rendszeresen használom.
                </p>
            </div>
            <div>
                <div>
                    <i>Előtt &amp; utána</i>
                    <img alt="block_10_foto3" src="//st.acstnst.com/content/Variforte_HU/images/block_10_foto3.jpg"/>
                    <p>
                        Maria S.<br/>58 éves
                    </p>
                </div>
                <p>Második stádiumu visszérbetegségem volt. Az erek a lábamon nagyon láthatóak voltak.   Be voltak duzzadva és fájtak folyamatosan. Lányom vette nekem ezt a krémet, de én már nem hittem semmiben .</p>
                <p>
                    Kentem majdnem egy évig, minden nap, naponta többször. Az eredmény meglepett. Az első, ami feltűnt - a bőröm hidratált és fiatalabb lett. Majd fokozatosan eltünt fájdalom és a duzzanatok. Később,  körülbelül egy havi használata után csökkentek a dudorok az ereken. Én továbbra is ezt a krémet használom, mert látom az eredményt, és ez nagyon boldogít!
                </p>
            </div>
            <div>
                <div>
                    <i>Előtt &amp; utána</i>
                    <img alt="block_10_foto4" src="//st.acstnst.com/content/Variforte_HU/images/block_10_foto4.jpg"/>
                    <p>
                        Sóma László <br/>65 éves
                    </p>
                </div>
                <p>
                    A feleségem mindig mondta, hogy problémásak az ereim. Én magam is  ezt érzem. A kezem tele vannak seprűérrel,  rossz rá nézni.  Nagyon  fáj, és mindig hideg a kezem.
                </p>
                <p>
                    Ezt a kenőcsőt  a feleségem vásárolta. Elkeztem használni. Eleinte nem volt semmi, csak egy kellemes melegség érzés. Körülbelül 10 nap múlva  enyhén csökkentek és halványultak  az erek. Örültem! Aztán  másfél hónap múlva  elmúlt       a fájdalom . Úgy látszik a krém levette a gyulladást  ! Az erek még kisebbek lettek.. A söprűerek már  nem néznek ki olyan félelmetesnek és eltünt a fájdalom.
                </p>
            </div>
        </div>
    </div>
    <div class="block_11">
        <div class="center_wrapper">
            <i class="bg"></i>
            <img alt="" class="for_footer_product" src="//st.acstnst.com/content/Variforte_HU/images/product_new.png"/>
            <div class="dsc_box">
                <div class="discount">
                    <p>
                        <b>50%</b>
                        <br/>
                        kedvezmény
                    </p>
                </div>
                <p>Megrendelni</p>
                <p>
                    Kedvező áron:
                <span class="js_new_price_curs">
<strong>12220 ƒ</strong>
</span>
                </p>
                <p>
                    Rendes  ár:
                <span class="js_old_price_curs">
<strong>24440 ƒ</strong>
</span>
                </p>
                <form action="" id="order_form" method="post"><input type="hidden" name="esub">
                    <iframe scrolling="no" style="border:0; left:0; right:0; bottom:0; width:100%; height:230px;" src="http://pt.new-power-life.com/forms/?target=-4AAIJIAIcBwAAAAAAAAAAAATyAn20AA"></iframe>
                </form>
            </div>
        </div>
    </div>
    <div class="bffoot" style="padding-top:50px;">
        A termék tanúsítvány, összetétel, a használati és kezelési útmutató a doboz  címkén található. A kép – illusztráció.
        Tanusítvány. Fizetés utánvéttel.
        <br/>
        <br/>
    </div>
    <div class="wrapper cf">
        <div class="wrapper cf">
            <div class="footer cf"><!--
        <p>
            <a href="" target="_blank">Termeni și condiții</a> <span class="black">|</span>
            <a href="" target="_blank">Politica de confidențialitate</a> <span class="black">|</span>
            <a href="" target="_blank">Date de contact</a>
            <br>
            <span class="copy">
                Copyright &copy; 
                <span class="ryear"></span> 
                Varyforte , Ltd. Toate drepturile rezervate.
            </span>
        </p>
    --></div>
        </div>
        <!-- end of footer -->
    </div>
</div>
<div id="privacy-policy" style="display:none;">
    <div class="block_more_info">
        <style scoped="">
            body, html{
                min-height: 100%;
                margin:0px;
                padding: 0px;

            }
            .block_more_info{
                width: 800px;
                margin: 8px auto 20px;
                background: #fff;
                font-family: Arial;
                padding: 20px 40px 40px 40px;
                border: 1px solid #DADADA;
                line-height: 20px;
            }
            .block_more_info h1{
                color: #3B6A7C;
                margin-bottom: 30px;
                margin-top: 21px;
                text-align: center;
                font-size: 32px;
                font-weight: bold;
            }
            .block_more_info p {
                color: #000000;
                font-size: 16px;
                margin: 16px 0;
            }
            .s1{
                font-style: italic;
                text-align: center;
                margin: 40px 0 0 0;
                font-weight: bold;
            }
            h2{
                font-size: 16px;
                margin-top: 26px;
                font-weight: bold;
            }
        </style>
        <h1>Privacy Policy</h1>
        <h2>Üzemeltetői adatok:</h2>
        <p>Adószám: 25417740-2-43<br/>
            Cégjegyzékszám: 01-09-273963<br/>
            Kibocsátó cégbíróság: Fővárosi Törvényszék Cégbírósága nyilvántartásában<br/>
            E-mail: <a href="mailto:gls.aversum@outlook.com">gls.aversum@outlook.com</a><br/>
            Telefonszám: +3630/ 3032134</p>
        <h2>Rendeléssel kapcsolatos információk:</h2>
        <p>1. Nevének és telefonszámának megadása a termék megrendeléseként minősül. Munkaidőn belül kollégáink fel fogják venni önnel a kapcsolatot további adategyeztetés céljából. A megrendelés feldolgozása, visszaigazolása, teljesítése 1-2 munkanapot vesz igénybe. </p>
        <p>2. Webshopunk felhasználóinak adatait nem adjuk ki harmadik félnek, 6 évig őrizzük meg, majd automata program törli őket.</p>
        <p>3. A termék megrendeléséhez elegendő online formában megadnia nevét és telefonszámát. Ezután kollégánk felveszi önnel a kapcsolatot a cím és más adatok egyeztetésének céljából.</p>
        <p>4. A megrendelést csak készpénzzel, utánvétként lehet kifizetni a csomagküldő szolgálat futárának. A termék árához 1500 Ft szállítási díjat számolunk fel.</p>
        <p>5. Garanciális feltételek: Sérült termék esetén kérjük hívja fel ügyfészolgálatunkat a további teendők pontosítása végett. Hibásan leadott, vagy teljesített megrendelések estén is kérjük vegye fel ügyfélszolgálatunkkal a kapcsolatot.</p>
        <p>6.1. Az elállási joggal élő vásárlónak kötelessége saját költségén, 8 munkanapon belül visszajuttatni a terméket a webáruház tulajdonosának. Fontos! A vásárló nem kötelezheti az eladót a szállítási díj kifizetésére, ez minden esetben a vásárlót terheli!</p>
        <p>6.2. A vásárlót nem terheli semmilyen más költség, a termék visszaszállításának díján kívül, azonban kötelezhető a nem rendeltetésszerű használatból eredő kár megtérítésére!</p>
        <p>6.3. A vételár visszatérítése minden esetben banki átutalás formájában történik. Ezért kérjük, a számlához csatolja: bankjának nevét, bankszámlaszámát és saját nevét.</p>
        <p>6.4. Mint webáruház tulajdonosnak, 30 nap áll rendelkezésére, hogy visszatérítse a vásárló számára a teljes vételárat, amennyiben élni kíván az elállási jogával a vásárló, és visszajuttatta a megrendelt terméket.</p>
        <p>6.5. A vásárlót nem illeti meg az elállási jog, ha: Higiéniai termékek, szexuális segédeszközök, kellékek, kozmetikumok esetén amennyiben már kibontásra került a termék! (Ugyanakkor ha bontatlan a termék, élhet a vásárló az elállási jogával!)</p>
    </div>
</div>
</body>
</html>
<?php } ?>