<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- [pre]land_id = 2452 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIgBgT8PGiLAAEAAQACpwUBAAKUCQEKAQ8ERHuRfQA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 6;
            var name_hint = 'Bruno Bellini';
            var phone_hint = '+393476736735';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Osteoren_IT/mobile/css/reset.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Osteoren_IT/mobile/css/main.css" rel="stylesheet"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <script>
            $(document).ready(function() {
                $('.toform').click(function(){
                    $("html, body").animate({scrollTop: $("form").offset().top-300}, 1000);
                    return false;
                });
            });

        </script>
    </head>
    <body>
    <div class="wrapper">
        <section class="p1">
            <div class="logo"><img src="//st.acstnst.com/content/Osteoren_IT/mobile/i/logo.svg"/></div>
            <div class="lobster title">Soffrite di mal di schiena o di dolori articolari?</div>
            <div class="subtext">OSTEOREN è un prodotto che allevia il dolore causato dall'osteoartrite e dall'osteocondrosi</div>
        </section>
        <div class="line-light"></div>
        <section class="p2">
            <div class="title">PREZZO PROMOZIONALE <span class="discount lobster">50%</span></div>
            <div class="prices">
                <div class="box">
                    <div class="new-price lobster" style="text-decoration: line-through;">78 €</div>
                    <div class="old-price lobster">39 €</div>
                    <div class="btn toform"><button>  TEMPO RIMASTO PRIMA DELLA FINE DELLA PROMOZIONE  </button></div>
                </div>
            </div>
        </section>
        <section class="p3">
            <div class="list">Allevia la fibromalgia</div>
            <div class="list">Aiuta la rigenerazione cartilaginea</div>
            <div class="list">Diminuisce sensibilmente<br/> l'ipertonia muscolare</div>
            <div class="list">Combatte il gonfiore</div>
            <div class="list">Allevia l'infiammazione</div>
        </section>
        <div class="line-bold">
            <section class="p4">
                <div class="prod"><img src="//st.acstnst.com/content/Osteoren_IT/mobile/i/osteoren_new_mini.png"/></div>
                <div class="text">OSTEOREN è anche efficace nel trattamento dell'osteoartrite e dell'osteocondrosi, poiché rallenta il processo degenerativo della cartilagine e stimola il metabolismo dei tessuti</div>
            </section>
        </div>
        <section class="p5">
            <h2 class="lobster">Commenti dei compratori</h2>
            <div class="comments">
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_IT/mobile/i/photo1.png"/> </div>
                    <div class="text">
                        <div class="author">Elisa, 31</div>
                        <div class="comment">Con l'aiuto di OSTEOREN sono tornata al lavoro dopo 4 giorni ed ero in grado di camminare dalla prima applicazione.</div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_IT/mobile/i/photo2.png"/> </div>
                    <div class="text">
                        <div class="author">Gianni, 58</div>
                        <div class="comment">Mia moglie ha trovato OSTEOREN online. E' diventata la mia salvezza: prima ero incurvato dalla mattina, ma ora sto bene. E soprattutto, il dolore è scomparso.</div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_IT/mobile/i/photo3.png"/> </div>
                    <div class="text">
                        <div class="author">Stefania, 35</div>
                        <div class="comment">Ma ho avuto fortuna comprando OSTEOREN: nella prima settimana, la mia caviglia è guarita, il dolore è sparito e camminavo normalmente, quindi mi hanno dimessa. Ora la usa tutta la famiglia: allevia il dolore alla schiena e alle articolazioni.</div>
                    </div>
                </div>
            </div>
        </section>
        <div class="bottom">
            <section class="p6">
                <div class="bot-prices">
                    <div class="bot-prod"><img src="//st.acstnst.com/content/Osteoren_IT/mobile/i/img8_mini.png"/></div>
                    <div class="discount lobster">
                        SOLO PER OGGI!<br/>
                        Sconto del 50%
                    </div>
                    <div class="new-price lobster">39 €</div>
                    <div class="old-price lobster" style="text-decoration: line-through;">78 €</div>
                </div>
                <div class="order-form">
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIAIgBgAAAAAAAAAAAATUjpExAA"></iframe>

                        <!-- <input type="hidden" name="total_price" value="45.0">
                         <input type="hidden" name="esub" value="-4A25sMQIJIAIgBgT8PGiLAAEAAQACpwUBAAKUCQEKAQ8ERHuRfQA">
                         <input type="hidden" name="goods_id" value="79">
                         <input type="hidden" name="title" value="Osteoren - IT">
                         <input type="hidden" name="ip_city" value="Toruń">
                         <input type="hidden" name="template_name" value="Osteoren_IT">
                         <input type="hidden" name="price" value="39">
                         <input type="hidden" name="old_price" value="78">
                         <input type="hidden" name="total_price_wo_shipping" value="39.0">
                         <input type="hidden" name="package_prices" value="{}">
                         <input type="hidden" name="currency" value="€">
                         <input type="hidden" name="package_id" value="0">
                         <input type="hidden" name="protected" value="None">
                         <input type="hidden" name="ip_country_name" value="Poland">
                         <input type="hidden" name="country_code" value="IT">
                         <input type="hidden" name="shipment_vat" value="0.0">
                         <input type="hidden" name="ip_country" value="PL">
                         <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                         <input type="hidden" name="shipment_price" value="6">
                         <input type="hidden" name="price_vat" value="0.0">
                         <div class="row">
                             <label>NOME E COGNOME</label>
                             <input id="name" name="name" placeholder="" type="text"/>
                         </div>
                         <div class="row">
                             <label>NUMERO DI TELEFONO</label>
                             <input class="only_number" name="phone" placeholder="" type="text"/>
                         </div>
                         <div class="row btn-row">
                             <button class="js_submit order-btn">Ordine</button>
                         </div>-->
                    </form>
                </div>
            </section>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>






    <!DOCTYPE >
    <html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- [pre]land_id = 2452 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIgBgT8PGiLAAEAAQACpwUBAAKUCQEKAQ8ERHuRfQA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 6;
            var name_hint = 'Bruno Bellini';
            var phone_hint = '+393476736735';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <!--123-->
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title> Osteoren </title>
        <link href="//st.acstnst.com/content/Osteoren_IT/css/cdn0.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_IT/css/style.min.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_IT/css/modal.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_IT/font/RobotoCondensed-Light/styles.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_IT/font/RobotoCondensed-Bold/styles.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_IT/font/RobotoCondensed-Regular/styles.css" media="all" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Osteoren_IT/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="section wrap-block block1">
        <div class="wrapper-hidden1">
            <div class="wrapper">
                <div class="block1__top">
                    <span class="lt1"> DI SCONTO   </span> <span class="block1__top-sale">-50%</span> <span class="lt2">  PER GLI ULTIMI  </span> <span class="lastpack">9</span> <span class="lt3">  TUBETTI   </span>
                    <span class="block1__top-action lt4">  PREZZO PROMOZIONALE  </span>
                </div>
                <div class="block1__body">
                    <div class="block1__left">
                        <img alt="" src="//st.acstnst.com/content/Osteoren_IT/i/logo.svg"/>
                        <!--<img alt="" src="//st.acstnst.com/content/Osteoren_IT/i/img1_a1.png"/>-->
                        <div class="block1__title lt5">  Soffrite di mal di schiena
                            <br/>   o di dolori articolari?  </div>
                    </div>
                    <div class="block1__right">
                        <div class="block1__price">
                            <span class="newprice price_main">39  €  </span>
                            <span class="oldprice price_old"><s>78 €</s></span>
                        </div>
                        <div class="block__timer">
                            <div class="block__timer-text"><a class="akciya1 scrollto lt7" href="#form">  TEMPO RIMASTO PRIMA DELLA FINE DELLA PROMOZIONE  </a></div>
                            <div class="timer">
                                <div class="landing__countdown is-countdown"><span class="hours">16</span><span class="minutes">24</span><span class="seconds">18</span></div>
                                <div class="countdown__entities">
                                    <span class="lt8">  ore  </span>
                                    <span class="lt9">  minuti  </span>
                                    <span class="lt10">  secondi  </span>
                                </div>
                            </div>
                        </div>
                        <ul class="unstyled">
                            <li class="lt11">  Allevia la fibromalgia  </li>
                            <li class="lt12">  Aiuta la rigenerazione cartilaginea  </li>
                            <li class="lt13">  Diminuisce sensibilmente l'ipertonia muscolare  </li>
                            <li class="lt14">  Combatte il gonfiore  </li>
                            <li class="lt15">  Allevia l'infiammazione  </li>
                        </ul>
                    </div>
                </div>
                <img alt="" class="imlogo" src="//st.acstnst.com/content/Osteoren_IT/i/u1.png"/>
            </div>
        </div>
    </div>
    <div class="section wrap-block block2">
        <div class="wrapper-hidden">
            <div class="block2__top">
                <div class="wrapper lt16">  OSTEOREN è un prodotto che allevia il dolore causato dall'osteoartrite e dall'osteocondrosi. Allevia gli spasmi muscolari e arresta l'infiammazione. OSTEOREN è anche efficace nel trattamento dell'osteoartrite e dell'osteocondrosi, poiché rallenta il processo degenerativo della cartilagine e stimola il metabolismo dei tessuti, contribuendo alla rigenerazione della cartilagine articolare. Gli effetti si vedono dalla prima applicazione. L'uso regolare del prodotto può rallentare la progressione di malattie articolari e alla colonna.  </div>
            </div>
            <div class="block2__bottom">
                <div class="wrapper">
                    <div class="block2__title lt17">  Osteoren contiene solo componenti attivi!  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block3">
        <div class="wrapper">
            <div class="block3__title lt18">  Vantaggi dello gel stick Osteoren per la schiena ed articolazioni </div>
            <div class="block3__list">
                <ul class="inline">
                    <li class="item1">
                        <div class="block3__list_title lt19">  AMPIO
                            <br/>  RAGGIO   </div>
                        <span class="lt20">  Cura le malattie traumatiche e degenerative della colonna e delle articolazioni   </span>
                    </li>
                    <li class="item2">
                        <div class="block3__list_title lt21">  NON PROVOCA
                            <br/>   EFFETTI COLLATERALI  </div>
                        <span class="lt22">  Sicuro sia per uso occasionale che regolare  </span>
                    </li>
                    <li class="item3">
                        <div class="block3__list_title lt23">  100% COMPOSIZIONE
                            <br/>  NATURALE  </div>
                        <span class="lt24">  Solo componenti attive da piante medicinali  </span>
                    </li>
                    <li class="item4">
                        <div class="block3__list_title lt25">  ALTAMENTE EFFICACE  </div>
                        <span class="lt26">  Migliora la condizione generale e allieva il dolore dalla prima applicazione  </span>
                    </li>
                    <li class="item5">
                        <div class="block3__list_title lt27">  TESTATA DAGLI ESPERTI  </div>
                        <span class="lt28">  Prodotto certificato che rispetta tutte gli standard di qualità  </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section wrap-block block4">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block4__title lt29">  Commenti dei compratori  </div>
                <ul class="inline">
                    <li>
                        <div class="block4__top lt30">  Il roll-on gel Osteoren mi ha aiutata con un trauma alle articolazioni del ginocchio. Il ginocchio era gonfio, faceva molto male e quasi non riuscivo a muovere la gamba. Con l'aiuto di OSTEOREN sono tornata al lavoro dopo 4 giorni ed ero in grado di camminare dalla prima applicazione.  </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_IT/i/img3.7.1.png"/>
                            <div class="block4__bottom_name lt31">  Elisa, 31, insegnante di corsi di fitness   </div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt32">  Prima la schiena mi faceva molto male a causa dell'osteocondrosi, che è molto comune alla mia età. Mia moglie ha trovato OSTEOREN  online. E' diventata la mia salvezza: prima ero incurvato dalla mattina, ma ora sto bene. E soprattutto, il dolore è scomparso.  </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_IT/i/img3.7.2.png"/>
                            <div class="block4__bottom_name lt33">  Gianni, 58, pensionato  </div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt34">  Io mi ero fatta male alla caviglia e i medici dicevano che sarebbe stata una lunga guarigione. Ma ho avuto fortuna comprando OSTEOREN: nella prima settimana, la mia caviglia è guarita, il dolore è sparito e camminavo normalmente, quindi mi hanno dimessa. Ora la usa tutta la famiglia: allevia il dolore alla schiena e alle articolazioni.   </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_IT/i/img3.7.3.png"/>
                            <div class="block4__bottom_name lt35">  Stefania, 35, commessa  </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section wrap-block block5">
        <div class="wrapper">
            <div class="block5__title lt36">  Il roll-on gel Osteoren è raccomandata dagli esperti  </div>
            <img alt="" src="//st.acstnst.com/content/Osteoren_IT/i/img4.2_a2.png"/>
            <div class="block5__wrap">
                <div class="block5__text lt37">  Il roll-on gel OSTEOREN è un prodotto grandioso per l'osteoartrite e l'osteocondrosi. La raccomando a tutti i pazienti con dolori di schiena o articolari. OSTEOREN allevia in fretta il dolore e l'infiammazione, rigenera le articolazioni e i tendini, aiutandovi a tornare ad una vita attiva. Il roll-on gel è anche efficace contro i processi degenerativi associati all'età. Io la uso contro l'osteocondrosi e mi piace il suo effetto rapido e di lunga durata. L'uso continuato vi aiuterà a dimenticarvi dei problemi alla schiena.  </div>
                <div class="block5__name lt38">  Carlo Ponti, specialista di medicina sportiva  </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block6">
        <div class="wrapper">
            <div class="block6__title lt39">  Come applicare Osteoren  </div>
            <ul class="inline">
                <li class="lt40">  Applicare su pelle asciutta e massaggiare fino al completo assorbimento   </li>
                <li class="lt41">  Usare 2-3 volte al giorno  </li>
                <li class="lt42">  Non risciacquare fino a un'ora dopo l'applicazione  </li>
            </ul>
        </div>
    </div>
    <div class="section wrap-block block7">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block7__delivery">
                    <div class="block7__delivery_title lt43">  Consegna e Pagamento  </div>
                    <span class="lt44">  Riceverete il roll-on gel OSTEOREN per la schiena e le articolazioni entro 4-7 giorni dall'ordine. Potrete pagare in contanti alla consegna!  </span>
                </div>
                <div class="block7__warning">
                    <div class="block7__warning_title lt45">  State attenti ai falsi!  </div>
                    <span class="lt46">  Nel territorio europeo, il roll-on gel OSTEOREN è venduta in una confezione originale con un codice unico!   <br/><br/>  Verificate subito il codice sul tubetto   </span>
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block8">
        <div class="wrapper">
            <div class="block8__title lt48">  Come ordinare il roll-on gel OSTEOREN per la schiena e le articolazioni  </div>
            <ul class="inline">
                <li class="lt49">  Compilare il formulario  </li>
                <li class="lt50">  Scegliere il metodo di pagamento  </li>
                <li class="lt51">  Pagamento in contrassegno  </li>
            </ul>
        </div>
    </div>
    <div class="section wrap-block block10">
        <div class="wrapper">
            <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                <tbody>
                <tr>
                    <td class="block10__left" valign="top" width="500">
                        <div class="block10__title lt52">  SOLO PER OGGI!
                            <br/>   Sconto del 50%  </div>
                        <div class="block10__price">
                            <span class="newprice price_main"><span class="price_main_value">39</span><span class="price_main_currency">  €  </span></span>
                            <span class="oldprice price_old"><span class="price_main_value">78</span><span class="price_main_currency">  €  </span></span>
                        </div>
                        <div class="block10__sale">
                            <span class="block10__count">15</span>
                            <div class="lt53">  TUBETTI
                                <br/>  ORDINATI NELL'ULTIMA ORA  </div>
                        </div>
                        <div class="block10__allsale landing__maxpurcashe"> La maggior parte delle vendite (2419) sono state realizzate IL 26 LUGLIO 2016.</div>
                    </td>
                    <td valign="top" width="500">
                        <div class="block10-form" id="form">
                            <form action="" class="form-horizontal" id="order_form0" method="post">
                               <!-- <input type="hidden" name="total_price" value="45.0">
                                <input type="hidden" name="esub" value="-4A25sMQIJIAIgBgT8PGiLAAEAAQACpwUBAAKUCQEKAQ8ERHuRfQA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="Osteoren - IT">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Osteoren_IT">
                                <input type="hidden" name="price" value="39">
                                <input type="hidden" name="old_price" value="78">
                                <input type="hidden" name="total_price_wo_shipping" value="39.0">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="currency" value="€">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="IT">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="shipment_price" value="6">
                                <input type="hidden" name="price_vat" value="0.0">
                                <div class="row">
                                    <select class="country" id="country_code_selector" name="country">
                                        <option value="IT">Italy</option>
                                    </select>
                                </div>
                                <div class="row ">
                                    <label for="label1"><span class="lt55">  Nome e Cognome  </span> <small class="name_helper">  ad es. Caterina Grandi  </small></label>
                                    <input id="label1" name="name" type="text"/>
                                </div>
                                <div class="row ">
                                    <label for="lebel2"><span class="lt56">  Numero di telefono  </span> <small class="info_phone phone_helper">  ad es. +39 345 8877123  </small></label>
                                    <input class="only_number" id="lebel2" name="phone" type="text"/>
                                </div>
                                <div class="row row-submit">
                                    <button class="block10-btn lt57 js_submit" type="submit">  Ordine  </button>
                                </div>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abcdn.pro/forms/?target=-4AAIJIAIgBgAAAAAAAAAAAATUjpExAA"></iframe>

                            </form>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Osteoren_IT/js/ouibounce.min.js" type="text/javascript"></script>
    </body>
    </html>
<?php } ?>