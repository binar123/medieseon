<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- [pre]land_id = 2420 -->
        <script>var locale = "cy";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIfBgRmOWWLAAEAAQACpgUBAAJ0CQEKAQ8EWBxGaQA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'A. Παπαγεωργιου';
            var phone_hint = '+302310326065';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cy");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Osteoren_GR1/mobile/css/reset.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Osteoren_GR1/mobile/css/main.css" rel="stylesheet"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <script>
            $(document).ready(function() {
                $('.toform').click(function(){
                    $("html, body").animate({scrollTop: $("form").offset().top-300}, 1000);
                    return false;
                });
            });

        </script>
    </head>
    <body>
    <div class="wrapper">
        <section class="p1">
            <div class="logo"><img src="http://st.acstnst.com/content/Osteoren_GR1/mobile/i/logo.png"/></div>
            <div class="lobster title">Πονάτε στην πλάτη ή σε άλλα σημεία</div>
            <div class="subtext">Η OSTEOREN είναι ένα αποτελεσματικό προϊόν κατά της οστεοχόνδρωσης, της οστεοαρθρίτιδας και των τραυμάτων!</div>
        </section>
        <div class="line-light"></div>
        <section class="p2">
            <div class="title">ΜΙΑ ΕΚΠΤΩΤΙΚΗ ΤΙΜΗ <span class="discount lobster">50%</span></div>
            <div class="prices">
                <div class="box">
                    <div class="new-price lobster">39 €</div>
                    <div class="old-price lobster">78 €</div>
                    <div class="btn toform"><button>Απομένουν μέχρι το τέλος της προσφοράς</button></div>
                </div>
            </div>
        </section>
        <section class="p3">
            <div class="list">Ανακουφίζει από το μυαλγικό σύνδρομο</div>
            <div class="list">Συντελεί στην αναγέννηση του<br/>χόνδρου</div>
            <div class="list">Ελαττώνει με αποτελεσματικό<br/>τρόπο τη μυϊκή υπερτονία</div>
            <div class="list">Καταπολεμά το πρήξιμο</div>
            <div class="list">Ανακουφίζει από τις<br/>φλεγμονές</div>
        </section>
        <div class="line-bold">
            <section class="p4">
                <div class="prod"><img src="http://st.acstnst.com/content/Osteoren_GR1/mobile/i/produkt.png" style="width:105px;"/></div>
                <div class="text">Το OSTEOREN είναι ένα προϊόν που παρέχει γρήγορη ανακούφιση από τον πόνο της οστεοαρθρίτιδας και της οστεοχόνδρωσης. Ανακουφίζει τον μυϊκό σπασμό και επιταχύνει το τέλος της φλεγμονής</div>
            </section>
        </div>
        <section class="p5">
            <h2 class="lobster">Σχόλια αγοραστών</h2>
            <div class="comments">
                <div class="item">
                    <div class="photo"><img src="http://st.acstnst.com/content/Osteoren_GR1/mobile/i/photo1.png"/> </div>
                    <div class="text">
                        <div class="author">Μαρία, 31 ετών</div>
                        <div class="comment">Χάρη στη βοήθεια του OSTEOREN, μετά από 4 μέρες επέστρεψα στη δουλειά, όντας σε θέση να περπατήσω από την πρώτη κιόλας μέρα χρήσης. </div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="http://st.acstnst.com/content/Osteoren_GR1/mobile/i/photo2.png"/> </div>
                    <div class="text">
                        <div class="author">Μιγκέλ, 58 ετών</div>
                        <div class="comment">Η γυναίκα μου βρήκε το OSTEOREN στο διαδίκτυο. Αποδείχθηκε η σωτηρία μου: προηγουμένως, μερικές φορές, από το πρωί καμπούριαζα, αλλά τώρα είμαι καλά. Και το πιο σημαντικό είναι ότι εξαφανίστηκε ο πόνος στην πλάτη. </div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="http://st.acstnst.com/content/Osteoren_GR1/mobile/i/photo3.png"/> </div>
                    <div class="text">
                        <div class="author">Σπεράντζα, 35 ετών</div>
                        <div class="comment">Αλλά είχα την τύχη να αγοράσω το OSTEOREN και μέσα στην πρώτη εβδομάδα, ο αστράγαλος ανάρρωσε, ο πόνος εξαφανίστηκε, περπατούσα κανονικά, μου έδωσαν εξιτήριο. Τώρα το χρησιμοποιεί όλη η οικογένειά μας: βοηθά κατά του πόνου της πλάτης και της άρθρωσης</div>
                    </div>
                </div>
            </div>
        </section>
        <div class="bottom">
            <section class="p6">
                <div class="bot-prices">
                    <div class="bot-prod"><img src="http://st.acstnst.com/content/Osteoren_GR1/mobile/i/produkt.png" style="width:105px;"/></div>
                    <div class="discount lobster">
                        ΜΟΝΟ ΣΗΜΕΡΑ!<br/>
                        Έκπτωση  50%
                    </div>
                    <div class="new-price lobster">39 €</div>
                    <div class="old-price lobster">78 €</div>
                </div>
                <div class="order-form">
                    <form action="" method="post">
                       <!-- <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="39.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAIfBgRmOWWLAAEAAQACpgUBAAJ0CQEKAQ8EWBxGaQA">
                        <input type="hidden" name="goods_id" value="79">
                        <input type="hidden" name="title" value="Osteoren - GR">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Osteoren_GR1">
                        <input type="hidden" name="price" value="39">
                        <input type="hidden" name="old_price" value="78">
                        <input type="hidden" name="al" value="2420">
                        <input type="hidden" name="total_price_wo_shipping" value="39.0">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="CY">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <div class="row">
                            <select class="country" id="country_code_selector" name="country">
                                <option value="GR">
                                    ΕΛΛΑΔΑ
                                </option>
                                <option value="GR">
                                    Κύπρος
                                </option>
                            </select>
                        </div>
                        <div class="row">
                            <label>ΠΛΉΡΕΣ ΌΝΟΜΑ</label>
                            <input id="name" name="name" placeholder="" type="text"/>
                        </div>
                        <div class="row">
                            <label>ΤΗΛΈΦΩΝΟ</label>
                            <input class="only_number" name="phone" placeholder="" type="text"/>
                        </div>
                        <div class="row btn-row">
                            <button class="js_submit order-btn">Παραγγελία</button>
                        </div>-->
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdn.pro/forms/?target=-4AAIJIAIfBgAAAAAAAAAAAAQz3fjNAA"></iframe>

                    </form>
                </div>
            </section>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE >
    <html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- [pre]land_id = 2420 -->
        <script>var locale = "cy";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIfBgRmOWWLAAEAAQACpgUBAAJ0CQEKAQ8EWBxGaQA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'A. Παπαγεωργιου';
            var phone_hint = '+302310326065';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("cy");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Osteoren_GR1/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_GR1/css/styles.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_GR1/css/styles_002.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_GR1/css/styles_003.css" media="all" rel="stylesheet" type="text/css"/>
        <!--<script type="text/javascript" src="//st.acstnst.com/content/Osteoren_GR1/js/countrieslist_cy-gr.js"></script>-->
        <link href="//st.acstnst.com/content/Osteoren_GR1/css/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_GR1/css/modal.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Osteoren_GR1/js/ouibounce.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Osteoren_GR1/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <section class="wrap-block block1">
        <div class="wrapper-hidden1">
            <div class="wrapper">
                <div class="block1__top">
                    <span class="lt1">ΕΚΠΤΩΣΗ</span> <span class="block1__top-sale">-50%</span> <span class="lt2">ΓΙΑ ΤΑ ΤΕΛΕΥΤΑΙΑ </span> <span class="lastpack">9</span> <span class="lt3">ΔΟΧΕΙΑ </span>
                    <span class="block1__top-action lt4">ΜΙΑ ΕΚΠΤΩΤΙΚΗ ΤΙΜΗ</span>
                </div>
                <div class="block1__body">
                    <div class="block1__left">
                        <img alt="" src="http://st.acstnst.com/content/Osteoren_GR1/i/img1.png"/>
                        <div class="block1__title lt5">Πονάτε στην πλάτη<br/> ή σε άλλα σημεία</div>
                        <div class="block1__sub lt6">Η OSTEOREN είναι ένα αποτελεσματικό προϊόν κατά της οστεοχόνδρωσης, της οστεοαρθρίτιδας και των τραυμάτων!</div>
                    </div>
                    <div class="block1__right">
                        <div class="block1__price">
                            <span class="newprice price_main"><span class="price_main_value">39</span> <span class="price_main_currency">€</span></span>
                            <span class="oldprice price_old"><span class="price_main_value">78</span><span class="price_main_currency">€</span></span>
                        </div>
                        <div class="block__timer">
                            <div class="block__timer-text2"><a class="akciya2 scrollto" href="#block-form">απομένουν μέχρι το τέλος της προσφοράς</a></div>
                            <div class="timer landing__countdown"></div>
                        </div>
                        <ul class="unstyled">
                            <li class="lt11">Ανακουφίζει από το μυαλγικό σύνδρομο</li>
                            <li class="lt12">Συντελεί στην αναγέννηση του χόνδρου</li>
                            <li class="lt13">Ελαττώνει με αποτελεσματικό τρόπο τη μυϊκή υπερτονία</li>
                            <li class="lt14">Καταπολεμά το πρήξιμο</li>
                            <li class="lt15">Ανακουφίζει από τις φλεγμονές</li>
                        </ul>
                    </div>
                </div>
                <img alt="" class="imlogo" src="http://st.acstnst.com/content/Osteoren_GR1/i/u1.png"/>
            </div>
        </div>
    </section>
    <section class="wrap-block block2">
        <div class="wrapper-hidden">
            <div class="block2__top">
                <div class="wrapper lt16">Το OSTEOREN είναι ένα προϊόν
                    που παρέχει γρήγορη ανακούφιση από τον πόνο της οστεοαρθρίτιδας και της
                    οστεοχόνδρωσης. Ανακουφίζει τον μυϊκό σπασμό και επιταχύνει το τέλος της
                    φλεγμονής. Το OSTEOREN είναι επίσης αποτελεσματικό στη θεραπεία της
                    οστεοαρθρίτιδας και της οστεοχόνδρωσης, καθώς επιβραδύνει την
                    εκφυλιστική διαδικασία του ιστού του χόνδρου και διεγείρει τον
                    μεταβολισμό του ιστού, συντελώντας στην αναγέννηση του χόνδρου των
                    αρθρώσεων. Το αποτέλεσμα φαίνεται μετά την πρώτη χρήση. Η τακτική χρήση
                    του προϊόντος επιτρέπει το σταμάτημα της εξάπλωσης της ασθένειας των
                    αρθρώσεων και της σπονδυλικής στήλης.
                </div>
            </div>
            <div class="block2__bottom">
                <div class="wrapper">
                    <div class="block2__title lt17">Το Osteoren περιέχει μοναδικά ενεργά συστατικά!</div>
                </div>
            </div>
        </div>
    </section>
    <section class="wrap-block block3">
        <div class="wrapper">
            <div class="block3__title lt18">Πλεονέκτημα για την πλάτη και τις αρθρώσεις Osteoren</div>
            <div class="block3__list">
                <ul class="inline">
                    <li class="item1">
                        <div class="block3__list_title lt19">ΕΥΡΥ <br/>ΦΑΣΜΑ</div>
                        <span class="lt20">θεραπεύει τραυματικές και εκφυλιστικές ασθένειες της σπονδυλικής στήλης και των αρθρώσεων </span>
                    </li>
                    <li class="item2">
                        <div class="block3__list_title lt21">ΔΕΝ ΕΧΕΙ<br/> ΠΑΡΕΝΕΡΓΕΙΕΣ</div>
                        <span class="lt22">απόλυτα ασφαλές τόσο σε περιστασιακή όσο και σε τακτική χρήση</span>
                    </li>
                    <li class="item3">
                        <div class="block3__list_title lt23">100% ΦΥΣΙΚΗ <br/>ΣΥΣΤΑΣΗ</div>
                        <span class="lt24">μόνο ενεργά συστατικά από φαρμακευτικά φυτά</span>
                    </li>
                    <li class="item4">
                        <div class="block3__list_title lt25">ΜΕΓΑΛΗ ΑΠΟΤΕΛΕΣΜΑΤΙΚΟΤΗΤΑ</div>
                        <span class="lt26">βελτιώνει την κατάσταση και ανακουφίζει τον πόνο από την πρώτη χρήση</span>
                    </li>
                    <li class="item5">
                        <div class="block3__list_title lt27">ΔΟΚΙΜΑΣΜΕΝΟ ΑΠΟ ΕΙΔΗΜΟΝΕΣ</div>
                        <span class="lt28">πιστοποιημένο προϊόν που πληροί τα στάνταρ ποιότητας</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="wrap-block block4">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block4__title lt29">Σχόλια αγοραστών</div>
                <ul class="inline">
                    <li>
                        <div class="block4__top lt30">Η OSTEOREN με
                            βοήθησε πολύ με το τραύμα της άρθρωσης του γονάτου μετά την προπόνηση.
                            Το γόνατο είχε φλεγμονή, με πονούσε πολύ, ουσιαστικά δεν μπορούσα να
                            κουνήσω το πόδι. Χάρη στη βοήθεια του OSTEOREN, μετά από 4 μέρες
                            επέστρεψα στη δουλειά, όντας σε θέση να περπατήσω από την πρώτη κιόλας
                            μέρα χρήσης.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="http://st.acstnst.com/content/Osteoren_GR1/i/img3_002.png"/>
                            <div class="block4__bottom_name lt31">Μαρία, 31 ετών, προπονήτρια σε ομαδικά τμήματα γυμναστικής</div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt32">Προηγουμένως με
                            πονούσε πολύ η πλάτη μου, οστεοχόνδρωση, που στην ηλικία μου είναι πολύ
                            συχνή. Η γυναίκα μου βρήκε το OSTEOREN στο διαδίκτυο. Αποδείχθηκε η
                            σωτηρία μου: προηγουμένως, μερικές φορές, από το πρωί καμπούριαζα, αλλά
                            τώρα είμαι καλά. Και το πιο σημαντικό είναι ότι εξαφανίστηκε ο πόνος
                            στην πλάτη.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="http://st.acstnst.com/content/Osteoren_GR1/i/img3.png"/>
                            <div class="block4__bottom_name lt33">Μιγκέλ, 58 ετών, συνταξιούχος</div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt34">Κάποια φορά είχα έναν
                            τραυματισμό στον αστράγαλο,οι γιατροί μου είπαν ότι η θεραπεία θα ήταν
                            μακροχρόνια. Αλλά είχα την τύχη να αγοράσω το OSTEOREN και μέσα στην
                            πρώτη εβδομάδα, ο αστράγαλος ανάρρωσε, ο πόνος εξαφανίστηκε, περπατούσα
                            κανονικά, μου έδωσαν εξιτήριο. Τώρα το χρησιμοποιεί όλη η οικογένειά
                            μας: βοηθά κατά του πόνου της πλάτης και της άρθρωσης.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="http://st.acstnst.com/content/Osteoren_GR1/i/img3_003.png"/>
                            <div class="block4__bottom_name lt35">Σπεράντζα, 35 ετών, πωλήτρια</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="wrap-block block5">
        <div class="wrapper">
            <div class="block5__title lt36">Η Osteoren συνιστάται από τους ειδήμονες</div>
            <img alt="" src="http://st.acstnst.com/content/Osteoren_GR1/i/img4.png"/>
            <div class="block5__wrap">
                <div class="block5__text lt37">Η OSTEOREN είναι ένα
                    προϊόν κατά της οστεοαρθρίτιδας και της οστεοχόνδρωσης. Τη συνιστώ σε
                    όλους τους ασθενείς που υφίστανται τους πόνους της πλάτης ή των
                    αρθρώσεων. Το OSTEOREN ανακουφίζει γρήγορα τον πόνο και τη φλεγμονή,
                    αναγεννά τις αρθρώσεις και τους τέντονες, και σας επιτρέπει να
                    επιστρέψετε στην ζωή ενεργά. Η κρέμα είναι επίσης αποτελεσματική κατά
                    των εκφυλιστικών διαδικασιών που σχετίζονται με την ηλικία, εγώ
                    προσωπικά τη χρησιμοποιώ κατά της οστεοχόνδρωσης, μου αρέσει πολύ η
                    γρήγορη δράση του, που παραμένει μόνιμα, ενώ η συνεχής χρήση της σας
                    κάνει να ξεχάσετε τα προβλήματα της πλάτης ενώ επιτρέπει τη συνέχιση της χρήσης ξεχνάμε προβλήματα στην πλάτη.
                </div>
                <div class="block5__name lt38">Χουανφράν Σεγκόβια., αθλίατρος, PhD</div>
            </div>
        </div>
    </section>
    <section class="wrap-block block6">
        <div class="wrapper">
            <div class="block6__title lt39">Τρόπος χρήσης του Osteoren</div>
            <ul class="inline">
                <li class="lt40">Απλώστε πάνω σε στεγνό δέρμα και κάντε μασάζ μέχρι να απορροφηθεί πλήρως</li>
                <li class="lt41">Χρησιμοποιήστε 2-3 φορές την ημέρα</li>
                <li class="lt42">Μην ξεπλένετε με νερό για μια ώρα μετά τη χρήση</li>
            </ul>
        </div>
    </section>
    <section class="wrap-block block7">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block7__delivery">
                    <div class="block7__delivery_title lt43">Αποστολή και πληρωμή</div>
                    <span class="lt44">Θα λάβετε το προϊον για την πλάτη και τις αρθρώσεις  , OSTEOREN , σε διάστημα 4-7 ημερών απο την παραγγελία.</span>
                </div>
                <div class="block7__warning">
                    <div class="block7__warning_title lt45">Προσοχή στις απομιμήσεις!</div>
                    <span class="lt46">Στις χώρες της ΕΕ η κρέμα OSTEOREN πωλείται σε αυθεντική συσκευασία με τον μοναδικό κωδικό!<br/>Βρείτε τον κωδικό στη συσκευασία και αγοράστε την τώρα</span>
                </div>
            </div>
        </div>
    </section>
    <section class="wrap-block block8">
        <div class="wrapper">
            <div class="block8__title lt48">Πως να παραγγείλετε το προϊον για την πλάτη αι τις αρθρώσεις Osteoren</div>
            <ul class="inline">
                <li class="lt49">συμπληρώστε τη φόρμα παραγγελίας</li>
                <li class="lt50">Επιλέξτε τον τρόπο αποστολής</li>
                <li class="lt51">Πληρώστε με αντικαταβολή</li>
            </ul>
        </div>
    </section>
    <section class="wrap-block block10">
        <div class="wrapper">
            <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                <tbody>
                <tr>
                    <td class="block10__left" valign="top" width="500">
                        <div class="block10__title lt52">ΜΟΝΟ ΣΗΜΕΡΑ! <br/> Έκπτωση 50%</div>
                        <div class="block10__price">
                            <span class="newprice price_main"><span class="price_main_value">39</span> <span class="price_main_currency">€</span></span>
                            <span class="oldprice price_old"><span class="price_main_value">78</span><span class="price_main_currency">€</span></span>
                        </div>
                        <div class="block10__sale">
                            <span class="block10__count">15</span>
                            <div class="lt53">ΠΑΡΑΓΓΕΛΘΕΙΣΕΣ ΣΥΣΚΕΥΑΣΙΕΣ <br/>ΚΑΤΑ ΤΗΝ ΤΕΛΕΥΤΑΙΑ ΩΡΑ</div>
                        </div>
                        <div class="block10__allsale landing__maxpurcashe">
                        </div>
                    </td>
                    <td style="" valign="top" width="500">
                        <div class="block10-form" id="block-form">
                            <form action="" class="form-horizontal order_form" id="order_form" method="POST">
                               <!-- <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="total_price" value="39.0">
                                <input type="hidden" name="esub" value="-4A25sMQIJIAIfBgRmOWWLAAEAAQACpgUBAAJ0CQEKAQ8EWBxGaQA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="Osteoren - GR">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Osteoren_GR1">
                                <input type="hidden" name="price" value="39">
                                <input type="hidden" name="old_price" value="78">
                                <input type="hidden" name="al" value="2420">
                                <input type="hidden" name="total_price_wo_shipping" value="39.0">
                                <input type="hidden" name="currency" value="€">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="CY">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="shipment_price" value="0">
                                <input type="hidden" name="price_vat" value="0.0">
                                <div class="row">
                                    <select class="country" id="country_code_selector" name="country">
                                        <option value="GR">
                                            ΕΛΛΑΔΑ
                                        </option>
                                        <option value="GR">
                                            Κύπρος
                                        </option>
                                    </select>
                                </div>
                                <div class="row ">
                                    <label for="label1"><span class="lt55">Πλήρες Όνομα</span>
                                        <small class="name_helper"></small>
                                    </label>
                                    <input id="label1" name="name" type="text"/>
                                </div>
                                <div class="row ">
                                    <label for="lebel2"><span class="lt56">Τηλέφωνο</span>
                                        <small class="phone_helper"></small>
                                    </label>
                                    <input class="only_number" id="lebel2" name="phone" type="text"/>
                                </div>
                                <div class="row row-submit">
                                    <button class="block10-btn lt57 js_submit" type="submit">Παραγγελία</button>

                                </div>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abcdn.pro/forms/?target=-4AAIJIAIfBgAAAAAAAAAAAAQz3fjNAA"></iframe>


                            </form>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
    <div id="ouibounce-modal" style="display: none;">
        <div class="underlay"></div>
        <div class="over-window">
            <div class="none">
                <div class="popup">
                    <div class="icon-close close-over close-ex"></div>
                    <div class="title"><span class="lt62">Η ειδική προσφορά είναι διαθέσιμη για όλους τους επισκέπτες της ιστοσελίδας σήμερα -</span> <br/><span class="lt63" style="color:#C00;">50% έκπτωση για το OSTEOREN</span>
                    </div>
                    <div class="content">
                        <div class="padding">
                            <form action="" class="order-form order_form" id="order_form0" method="POST">
                                <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="total_price" value="39.0">
                                <input type="hidden" name="esub" value="-4A25sMQIJIAIfBgRmOWWLAAEAAQACpgUBAAJ0CQEKAQ8EWBxGaQA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="Osteoren - GR">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Osteoren_GR1">
                                <input type="hidden" name="price" value="39">
                                <input type="hidden" name="old_price" value="78">
                                <input type="hidden" name="al" value="2420">
                                <input type="hidden" name="total_price_wo_shipping" value="39.0">
                                <input type="hidden" name="currency" value="€">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="CY">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="shipment_price" value="0">
                                <input type="hidden" name="price_vat" value="0.0">
                                <select class="country" id="country_code_selector" name="country">
                                    <option value="GR">
                                        ΕΛΛΑΔΑ
                                    </option>
                                    <option value="GR">
                                        Κύπρος
                                    </option>
                                </select><br/>
                                <input name="name" placeholder="Πλήρες Όνομα" type="text"/>
                                <input class="only_number" name="phone" placeholder="Τηλέφωνο" type="text"/><br/>
                                <input class="js_submit" type="submit" value="Πάρε με τηλέφωνο"/>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abcdn.pro/forms/?target=-4AAIJIAIfBgAAAAAAAAAAAAQz3fjNAA"></iframe>

                            </form>
                            <p class="bold lt64">Μπορείτε να πάρετε περισσότερες πληροφορίες για την ειδική μας προσφορά, έτσι ώστε να παραγγείλετε το HONDOCREAM μέσω της <strong>της δωρεάν τηλεφωνικής
                                    μας κλήσης</strong>.<br/>Μέσος χρόνος αναμονής: 5-15 λεπτά.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Osteoren_GR1/js/com.js" type="text/javascript"></script>
    </body>
    </html>
<?php } ?>