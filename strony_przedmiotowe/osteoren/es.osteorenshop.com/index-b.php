<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- [pre]land_id = 2419 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIhBgQS32eLAAEAAQACqAUBAAJzCQEKAQ8E6pbMuQA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 6;
            var name_hint = 'Judit Flores';
            var phone_hint = '+34681435813';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Osteoren_ES/mobile/css/reset.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Osteoren_ES/mobile/css/main.css" rel="stylesheet"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <script>
            $(document).ready(function() {
                $('.toform').click(function(){
                    $("html, body").animate({scrollTop: $("form").offset().top-300}, 1000);
                    return false;
                });
            });

        </script>
    </head>
    <body>
    <div class="wrapper">
        <section class="p1">
            <div class="logo"><img src="//st.acstnst.com/content/Osteoren_ES/mobile/i/logo.png"/></div>
            <div class="lobster title">¿Le duele la espalda<br/>o tiene el dolor articular?</div>
            <div class="subtext">¡La crema OSTEOREN es un producto eficaz contra la osteocondrosis, la artrosis y los traumas!</div>
        </section>
        <div class="line-light"></div>
        <section class="p2">
            <div class="title">UN PRECIO PROMOCIONAL <span class="discount lobster">50%</span></div>
            <div class="prices">
                <div class="box">
                    <div class="new-price lobster">39 €</div>
                    <div class="old-price lobster"><s>78 €</s></div>
                    <div class="btn toform"><button>QUEDAN HASTA EL FINAL DE LA PROMOCIÓN</button></div>
                </div>
            </div>
        </section>
        <section class="p3">
            <div class="list">Alivia el síndrome miálgico</div>
            <div class="list">Contribuye a la regeneración del<br/>cartílago</div>
            <div class="list">Disminuye de forma eficaz el<br/>hipertono muscular</div>
            <div class="list">Lucha contra la tumefacción</div>
            <div class="list">Alivia la inflamación</div>
        </section>
        <div class="line-bold">
            <section class="p4">
                <div class="prod"><img style="width:105px;" src="//st.acstnst.com/content/Osteoren_ES/mobile/i/produkt.png"/></div>
                <div class="text">OSTEOREN es un producto que proporciona un alivio rápido del dolor de la artrosis y steocondrosis. Alivia el espasmo muscular y acaba con la inflamación</div>
            </section>
        </div>
        <section class="p5">
            <h2 class="lobster">Comentarios de los compradores</h2>
            <div class="comments">
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_ES/mobile/i/photo1.png"/> </div>
                    <div class="text">
                        <div class="author">María, 31 años</div>
                        <div class="comment">Gracias a la ayuda de OSTEOREN, después de 4 días volví al trabajo, pudiendo andar desde el primer día de aplicación</div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_ES/mobile/i/photo2.png"/> </div>
                    <div class="text">
                        <div class="author">Miguel, 58 años</div>
                        <div class="comment">Mi mujer encontró HONDROCREAM en internet. Se convirtió en mi salvación: antes, a veces, andaba encorvado desde la mañana, pero ahora estoy bien. Y lo más importante es que el </div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_ES/mobile/i/photo3.png"/> </div>
                    <div class="text">
                        <div class="author">Esperanza, 35 años</div>
                        <div class="comment">En la primera semana, el tobillo se recuperó, el dolor desapareció, ando con normalidad, me dieron el alta. Ahora lo usa toda nuestra familia: ayuda contra el dolor de espalda y articular.</div>
                    </div>
                </div>
            </div>
        </section>
        <div class="bottom">
            <section class="p6">
                <div class="bot-prices">
                    <div class="bot-prod"><img style="width:125px;" src="//st.acstnst.com/content/Osteoren_ES/mobile/i/produkt.png"/></div>
                    <div class="discount lobster">
                        ¡SOLO HOY!<br/>
                        Descuento del 50%
                    </div>
                    <div class="new-price lobster">39 €</div>
                    <div class="old-price lobster"><s>78 €</s></div>
                </div>
                <div class="order-form">
                    <form action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abdcn.pro/forms/?target=-4AAIJIAIhBgAAAAAAAAAAAARHfa_7AA"></iframe>

                        <!--  <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                          <input type="hidden" name="total_price" value="45.0">
                          <input type="hidden" name="esub" value="-4A25sMQIJIAIhBgQS32eLAAEAAQACqAUBAAJzCQEKAQ8E6pbMuQA">
                          <input type="hidden" name="goods_id" value="79">
                          <input type="hidden" name="title" value="Osteoren - ES">
                          <input type="hidden" name="ip_city" value="Toruń">
                          <input type="hidden" name="template_name" value="Osteoren_ES">
                          <input type="hidden" name="price" value="39">
                          <input type="hidden" name="old_price" value="78">
                          <input type="hidden" name="al" value="2419">
                          <input type="hidden" name="total_price_wo_shipping" value="39.0">
                          <input type="hidden" name="currency" value="€">
                          <input type="hidden" name="package_id" value="0">
                          <input type="hidden" name="protected" value="None">
                          <input type="hidden" name="ip_country_name" value="Poland">
                          <input type="hidden" name="country_code" value="ES">
                          <input type="hidden" name="shipment_vat" value="0.0">
                          <input type="hidden" name="ip_country" value="PL">
                          <input type="hidden" name="package_prices" value="{}">
                          <input type="hidden" name="shipment_price" value="6">
                          <input type="hidden" name="price_vat" value="0.0">
                          <div class="row">
                              <select class="country" id="country_code_selector" name="country">
                                  <option value="ES">Spain</option>
                              </select>
                          </div>
                          <div class="row">
                              <label>NOMBRE COMPLETO</label>
                              <input id="name" name="name" placeholder="" type="text"/>
                          </div>
                          <div class="row">
                              <label>TELÉFONO</label>
                              <input class="only_number" name="phone" placeholder="" type="text"/>
                          </div>
                          <div class="row btn-row">
                              <button class="js_submit order-btn">Ordenar</button>
                          </div>-->
                        <div class="form_annotation">
                            * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>





    <!DOCTYPE >
    <html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>
        <!-- [pre]land_id = 2419 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIhBgQS32eLAAEAAQACqAUBAAJzCQEKAQ8E6pbMuQA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 6;
            var name_hint = 'Judit Flores';
            var phone_hint = '+34681435813';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Osteoren_ES/css/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_ES/css/modal.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Lobster&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_ES/css/styles_003.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_ES/css/styles_002.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_ES/css/styles.css" media="all" rel="stylesheet" type="text/css"/>
        <!--<script type="text/javascript" src="js/es.js"></script>-->
        <script src="//st.acstnst.com/content/Osteoren_ES/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="section wrap-block block1">
        <div class="wrapper-hidden1">
            <div class="wrapper">
                <div class="block1__top">
                    <span class="lt1">DESCUENTO del 50% </span> <span class="block1__top-sale">-50%</span> <span class="lt2">PARA LOS ÚLTIMOS</span> <span class="lastpack">9</span> <span class="lt3">ENVASES </span>
                    <span class="block1__top-action lt4">UN PRECIO PROMOCIONAL</span>
                </div>
                <div class="block1__body">
                    <div class="block1__left">
                        <img alt="" src="//st.acstnst.com/content/Osteoren_ES/i/img1.png"/>
                        <div class="block1__title lt5">¿Le duele la espalda
                            <br/> o tiene el dolor articular?</div>
                        <div class="block1__sub lt6">¡La crema OSTEOREN es un producto eficaz contra la osteocondrosis, la artrosis y los traumas!</div>
                    </div>
                    <div class="block1__right">
                        <div class="block1__price">
                            <span class="newprice price_main">39 €</span>
                            <span class="oldprice price_old"><s>78 €</s></span>
                        </div>
                        <div class="block__timer">
                            <div class="block__timer-text"><a class="akciya1 scrollto lt7" href="#form">QUEDAN HASTA EL FINAL DE LA PROMOCIÓN</a></div>
                            <div class="timer">
                                <div class="landing__countdown is-countdown"><span class="hours">16</span><span class="minutes">24</span><span class="seconds">18</span></div>
                                <div class="countdown__entities">
                                    <span class="lt8">horas</span>
                                    <span class="lt9">minutos</span>
                                    <span class="lt10">segundos</span>
                                </div>
                            </div>
                        </div>
                        <ul class="unstyled">
                            <li class="lt11">Alivia el síndrome miálgico</li>
                            <li class="lt12">Contribuye a la regeneración del cartílago</li>
                            <li class="lt13">Disminuye de forma eficaz el hipertono muscular</li>
                            <li class="lt14">Lucha contra la tumefacción</li>
                            <li class="lt15">Alivia la inflamación</li>
                        </ul>
                    </div>
                </div>
                <img alt="" class="imlogo" src="//st.acstnst.com/content/Osteoren_ES/i/u1.png"/>
            </div>
        </div>
    </div>
    <div class="section wrap-block block2">
        <div class="wrapper-hidden">
            <div class="block2__top">
                <div class="wrapper lt16">OSTEOREN es un producto que
                    proporciona un alivio rápido del dolor de la artrosis y osteocondrosis.
                    Alivia el espasmo muscular y acaba con la inflamación. OSTEOREN
                    también es eficaz en el tratamiento de osteocondrosis y artrosis, ya que
                    ralentiza el proceso degenerativo del tejido cartílago y estimula el
                    metabolismo tisular, contribuyendo a la regeneración del cartílago de la
                    articulación. El efecto se ve después de la primera aplicación; el uso
                    regular del producto permite frenar la progresión de la enfermedad de
                    las articulaciones y de la columna.</div>
            </div>
            <div class="block2__bottom">
                <div class="wrapper">
                    <div class="block2__title lt17">¡Osteoren contiene únicamente componentes activos!</div>
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block3">
        <div class="wrapper">
            <div class="block3__title lt18">Ventajas de la crema para la espalda y las articulaciones Osteoren</div>
            <div class="block3__list">
                <ul class="inline">
                    <li class="item1">
                        <div class="block3__list_title lt19">AMPLIO
                            <br/>ESPECTRO </div>
                        <span class="lt20">cura enfermedades traumáticas y degenerativas de la columna y articulaciones </span>
                    </li>
                    <li class="item2">
                        <div class="block3__list_title lt21">NO TIENE EFECTOS
                            <br/> SECUNDARIOS</div>
                        <span class="lt22">totalmente seguro tanto en el uso puntual, como regular</span>
                    </li>
                    <li class="item3">
                        <div class="block3__list_title lt23">COMPOSICIÓN 100%
                            <br/>NATURAL</div>
                        <span class="lt24">solamente componentes activos de plantas medicinales</span>
                    </li>
                    <li class="item4">
                        <div class="block3__list_title lt25">ALTA EFICACIA</div>
                        <span class="lt26">mejora el estado y alivia el dolor desde la primera aplicación</span>
                    </li>
                    <li class="item5">
                        <div class="block3__list_title lt27">PROBADO POR LOS EXPERTOS</div>
                        <span class="lt28">producto certificado que cumple con los estándares de calidad.</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section wrap-block block4">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block4__title lt29">Comentarios de los compradores</div>
                <ul class="inline">
                    <li>
                        <div class="block4__top lt30">La crema
                            OSTEOREN me ayudó mucho con el trauma de la articulación de la
                            rodilla después del entrenamiento. La rodilla estaba inflamada, me dolía
                            mucho, prácticamente no podía mover la pierna. Gracias a la ayuda de
                            OSTEOREN, después de 4 días volví al trabajo, pudiendo andar desde el
                            primer día de aplicación.</div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_ES/i/img3.7.1.png"/>
                            <div class="block4__bottom_name lt31">María, 31 años, entrenadora de clases colectivas de clubs de fitness </div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt32">Antes me dolía
                            mucho la espalda, osteocondrósis, que a mi edad es muy frecuente. Mi
                            mujer encontró OSTEOREN en internet. Se convirtió en mi salvación:
                            antes, a veces, andaba encorvado desde la mañana, pero ahora estoy bien.
                            Y lo más importante es que el dolor de espalda desapareció.</div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_ES/i/img3.7.2.png"/>
                            <div class="block4__bottom_name lt33">Miguel, 58 años, jubilado</div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt34">Una vez tuve una
                            lesión en el tobillo, los médicos me dijeron que el tratamiento iba a
                            ser largo. Pero tuve suerte al comprar OSTEOREN y en la primera
                            semana, el tobillo se recuperó, el dolor desapareció, ando con
                            normalidad, me dieron el alta. Ahora lo usa toda nuestra familia: ayuda
                            contra el dolor de espalda y articular. </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_ES/i/img3.7.3.png"/>
                            <div class="block4__bottom_name lt35">Esperanza, 35 años, dependienta</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section wrap-block block5">
        <div class="wrapper">
            <div class="block5__title lt36">La crema Osteoren es recomendada por los expertos</div>
            <img alt="" src="//st.acstnst.com/content/Osteoren_ES/i/img4.2_a2.png"/>
            <div class="block5__wrap">
                <div class="block5__text lt37">La crema OSTEOREN es
                    un magnifico producto contra la artrosis y la osteocondrosis. Lo
                    recomiendo a todos los pacientes que acuden con dolores de espalda o
                    articulaciones. OSTEOREN alivia rápidamente el dolor y la
                    inflamación, regenera las articulaciones y los tendones, permite volver a
                    la vida activa. La crema también es eficaz contra los procesos
                    degenerativos asociados a la edad, la uso personalmente contra la
                    osteocondrosis, me gusta mucho su rápido efecto, que se mantiene de
                    forma duradera; mientras que el uso continuado permite olvidar de los
                    problemas de espalda.</div>
                <div class="block5__name lt38">Juanfran Segovia., medico deportivo, PhD</div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block6">
        <div class="wrapper">
            <div class="block6__title lt39">Modo de aplicación de Osteoren</div>
            <ul class="inline">
                <li class="lt40">Aplicar sobre la piel seca y masajear hasta su completa absorción </li>
                <li class="lt41">Usar 2-3 veces al día</li>
                <li class="lt42">No aclarar con agua hasta una hora después de la aplicación</li>
            </ul>
        </div>
    </div>
    <div class="section wrap-block block7">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block7__delivery">
                    <div class="block7__delivery_title lt43">Entrega y pago</div>
                    <span class="lt44">Recibirá la crema para la espalda
 y articulaciones, OSTEOREN, en el plazo de 4-7 días desde el
encargo. ¡El producto se paga contra reembolso!</span>
                </div>
                <div class="block7__warning">
                    <div class="block7__warning_title lt45">¡Tengan cuidado con imitaciones!</div>
                    <span class="lt46">¡En el territorio de la UE la crema OSTEOREN se vende en un envase original con un código único! <br/><br/>Encuentre el código en el envase y compruébelo ahora </span>
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block8">
        <div class="wrapper">
            <div class="block8__title lt48">Cómo encargar la crema para la espalda y articulaciones Osteoren</div>
            <ul class="inline">
                <li class="lt49">Rellene el formulario de pedido</li>
                <li class="lt50">Seleccione el modo de entrega</li>
                <li class="lt51">Pague contra reembolso</li>
            </ul>
        </div>
    </div>
    <div class="section wrap-block block10">
        <div class="wrapper">
            <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                <tbody>
                <tr>
                    <td class="block10__left" valign="top" width="500">
                        <div class="block10__title lt52">¡SOLO HOY!
                            <br/> Descuento del 50%</div>
                        <div class="block10__price">
                            <span class="newprice price_main"><span class="price_main_value">39</span> <span class="price_main_currency">€</span></span>
                            <span class="oldprice price_old"><span class="price_main_value">78</span> <span class="price_main_currency">€</span></span>
                        </div>
                        <div class="block10__sale">
                            <span class="block10__count">15</span>
                            <div class="lt53">ENCARGADO
                                <br/>ENVASES DURANTE LA ÚLTIMA HORA</div>
                        </div>
                        <div class="block10__allsale landing__maxpurcashe"></div>
                    </td>
                    <td valign="top" width="500">
                        <div class="block10-form" id="form">
                            <form action="" class="form-horizontal" id="order_form0" method="post">
                              <!--  <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="total_price" value="45.0">
                                <input type="hidden" name="esub" value="-4A25sMQIJIAIhBgQS32eLAAEAAQACqAUBAAJzCQEKAQ8E6pbMuQA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="Osteoren - ES">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Osteoren_ES">
                                <input type="hidden" name="price" value="39">
                                <input type="hidden" name="old_price" value="78">
                                <input type="hidden" name="al" value="2419">
                                <input type="hidden" name="total_price_wo_shipping" value="39.0">
                                <input type="hidden" name="currency" value="€">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="ES">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="shipment_price" value="6">
                                <input type="hidden" name="price_vat" value="0.0">
                                <div class="row">
                                    <select class="country" id="country_code_selector" name="country">
                                        <option value="ES">Spain</option>
                                    </select>
                                </div>
                                <div class="row ">
                                    <label for="label1"><span class="lt55">Nombre completo</span> <small class="name_helper"></small></label>
                                    <input id="label1" name="name" type="text"/>
                                </div>
                                <div class="row ">
                                    <label for="lebel2"><span class="lt56">Teléfono</span> <small class="info_phone phone_helper"></small></label>
                                    <input class="only_number" id="lebel2" name="phone" type="text"/>
                                </div>
                                <div class="row row-submit">
                                    <button class="block10-btn lt57 js_submit" type="submit">Ordenar</button>
                                </div>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abdcn.pro/forms/?target=-4AAIJIAIhBgAAAAAAAAAAAARHfa_7AA"></iframe>

                                <div class="form_annotation">
                                    * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                                </div>
                            </form>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="ouibounce-modal">
        <div class="underlay"></div>
        <div class="over-window">
            <div class="none">
                <div class="popup">
                    <div class="icon-close close-over close-ex"></div>
                    <div class="title">
                        <span class="lt62">Para todos los visitantes de la página web hoy está disponible la oferta especial</span>
                        <br/>
                        <span class="price_reduce lt63">50% del descuento para OSTEOREN</span>
                    </div>
                    <div class="content">
                        <div class="padding">
                            <form action="" class="order-form" id="order_form2" method="post">
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abdcn.pro/forms/?target=-4AAIJIAIhBgAAAAAAAAAAAARHfa_7AA"></iframe>

                                <!--  <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                  <input type="hidden" name="total_price" value="45.0">
                                  <input type="hidden" name="esub" value="-4A25sMQIJIAIhBgQS32eLAAEAAQACqAUBAAJzCQEKAQ8E6pbMuQA">
                                  <input type="hidden" name="goods_id" value="79">
                                  <input type="hidden" name="title" value="Osteoren - ES">
                                  <input type="hidden" name="ip_city" value="Toruń">
                                  <input type="hidden" name="template_name" value="Osteoren_ES">
                                  <input type="hidden" name="price" value="39">
                                  <input type="hidden" name="old_price" value="78">
                                  <input type="hidden" name="al" value="2419">
                                  <input type="hidden" name="total_price_wo_shipping" value="39.0">
                                  <input type="hidden" name="currency" value="€">
                                  <input type="hidden" name="package_id" value="0">
                                  <input type="hidden" name="protected" value="None">
                                  <input type="hidden" name="ip_country_name" value="Poland">
                                  <input type="hidden" name="country_code" value="ES">
                                  <input type="hidden" name="shipment_vat" value="0.0">
                                  <input type="hidden" name="ip_country" value="PL">
                                  <input type="hidden" name="package_prices" value="{}">
                                  <input type="hidden" name="shipment_price" value="6">
                                  <input type="hidden" name="price_vat" value="0.0">
                                  <select class="country" id="country_code_selector" name="country">
                                      <option value="ES">Spain</option>
                                  </select>
                                  <input name="name" placeholder="Su nombre" type="text"/>
                                  <input class="only_number" name="phone" placeholder="TELÉFONO" type="text"/>
                                  <br/>
                                  <input class="js_submit" type="submit" value="Pedir una llamada"/>-->
                                <div class="form_annotation">
                                    * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                                </div>
                            </form>
                            <p class="bold lt64">Se puede obtener más
                                información sobre la oferta especial, así como encargar OSTEOREN a
                                través de nuestro servicio de llamada <strong>gratuita</strong>.
                                <br/>Tiempo medio de espera: 5-15 minutos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Osteoren_ES/js/ouibounce.js" type="text/javascript"></script>
    </body></html>
<?php } ?>