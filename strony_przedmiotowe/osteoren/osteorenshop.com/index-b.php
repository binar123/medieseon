<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- [pre]land_id = 2772 -->
        <script>var locale = "hu";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIkBgSlU2SLAAEAAQACqwUBAALUCgEKAQ8EEpuJrQA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 1500;
            var name_hint = 'Kerekes Péter';
            var phone_hint = '+36705383026';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("hu");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Ostioren_HU/mobile/css/reset.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Ostioren_HU/mobile/css/main.css" rel="stylesheet"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <script>
            $(document).ready(function() {
                $('.toform').click(function(){
                    $("html, body").animate({scrollTop: $("form").offset().top-300}, 1000);
                    return false;
                });
            });

        </script>
    </head>
    <body>
    <div class="wrapper">
        <section class="p1">
            <div class="logo"><img src="http://st.acstnst.com/content/Ostioren_HU/mobile/i/logo.png"/></div>
            <div class="lobster title">Fáj a háta - vagy az ízületei?</div>
            <div class="subtext">A OSTEOREN krém hatásos szer az osteochondrosis, az artrózis és a traumák ellen!</div>
        </section>
        <div class="line-light"></div>
        <section class="p2">
            <div class="title">ÁRENGEDMÉNY <span class="discount lobster">50%</span></div>
            <div class="prices">
                <div class="box">
                    <div class="new-price lobster">10500 Ft.</div>
                    <div class="old-price lobster">21000 Ft.</div>
                    <div class="btn toform"><button>AZ AKCIÓ VÉGÉIG MÉG MARADT</button></div>
                </div>
            </div>
        </section>
        <section class="p3">
            <div class="list">Megszünteti a fájdalmas szindrómát</div>
            <div class="list">Elősegíti a porcszövet regenerációját</div>
            <div class="list">Hatásosan szünteti meg az<br/>izmok hipertonikusságát</div>
            <div class="list">Küzd a vizenyővel</div>
            <div class="list">Megszünteti a gyulladást</div>
        </section>
        <div class="line-bold">
            <section class="p4">
                <div class="prod"><img src="http://st.acstnst.com/content/Ostioren_HU/mobile/i/produkt.png" style="width:105px;"/></div>
                <div class="text">A OSTEOREN olyan szer, ami gyorsan megszünteti az artrózisból és az oszteo-chondrózisból eredő  fájdalmat. Megszünteti az izomgörcsöt és a gyulladást.</div>
            </section>
        </div>
        <section class="p5">
            <h2 class="lobster">Vásárlói vélemények</h2>
            <div class="comments">
                <div class="item">
                    <div class="photo"><img src="http://st.acstnst.com/content/Ostioren_HU/mobile/i/photo1.png"/> </div>
                    <div class="text">
                        <div class="author">Jázmin, 31 éves</div>
                        <div class="comment">A OSTEOREN krém nagyon segített nekem az edzés utáni térdízület-sérüléskor. De a OSTEOREN segítségével már 4 nap múlva visszatértem a munkába. Járni pedig már az első napon tudtam.</div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="http://st.acstnst.com/content/Ostioren_HU/mobile/i/photo2.png"/> </div>
                    <div class="text">
                        <div class="author">Ádám, 58 éves</div>
                        <div class="comment">A feleségem a OSTEOREN krémet az interneten találta. Számomra ez igazi megmentővé vált, korábban néha reggeltől még kiegyenesedni sem tudtam, most viszont minden rendben van.</div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="http://st.acstnst.com/content/Ostioren_HU/mobile/i/photo3.png"/> </div>
                    <div class="text">
                        <div class="author">Viktória, eladó, 35 éves, eladó</div>
                        <div class="comment">De sikerült megvennem a OSTEOREN krémet, és egy hét múlva már rendbejöttek a szalagok, a fájdalom eltűnt, normálisan járok, és a betegállományból visszatértem a munkába. Most már az egész család használja ezt a krémet, akinek fáj a háta vagy az ízületei, annak nagyon segít</div>
                    </div>
                </div>
            </div>
        </section>
        <div class="bottom">
            <section class="p6">
                <div class="bot-prices">
                    <div class="bot-prod"><img style="width:125px;" src="http://st.acstnst.com/content/Ostioren_HU/mobile/i/produkt.png"/></div>
                    <div class="discount lobster">
                        Árengedmény  50%
                        <br/><br/>
                    </div>
                    <div class="new-price lobster">10500 Ft.</div>
                    <div class="old-price lobster">21000 Ft.</div>
                </div>
                <div class="order-form">
                    <form action="" method="post">
                       <!-- <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="total_price" value="12000.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAIkBgSlU2SLAAEAAQACqwUBAALUCgEKAQ8EEpuJrQA">
                        <input type="hidden" name="goods_id" value="79">
                        <input type="hidden" name="title" value="Osteoren - HU">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Ostioren_HU">
                        <input type="hidden" name="price" value="10500">
                        <input type="hidden" name="old_price" value="21000">
                        <input type="hidden" name="al" value="2772">
                        <input type="hidden" name="total_price_wo_shipping" value="10500.0">
                        <input type="hidden" name="currency" value="Ft.">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="HU">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="1500">
                        <input type="hidden" name="price_vat" value="0.0">
                        <div class="row">
                            <!--<label for="label0" class="lt54">Ország</label>-->
                           <!-- <select class="country_select" id="label0" name="country"><option selected="selected" value="HU">Magyarország</option></select>
                        </div>
                        <div class="row">
                            <!--<label>Teljes név</label>-->

                <iframe scrolling="no"
                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                        src="http://abdcn.pro/forms/?target=-4AAIJIAIjBgAAAAAAAAAAAASXAiF1AA"></iframe>

                </form>
               </div>

            </section>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>
        <!-- [pre]land_id = 2772 -->
        <script>var locale = "hu";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIkBgSlU2SLAAEAAQACqwUBAALUCgEKAQ8EEpuJrQA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 1500;
            var name_hint = 'Kerekes Péter';
            var phone_hint = '+36705383026';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("hu");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Ostioren_HU/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Ostioren_HU/css/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Ostioren_HU/css/styles.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Ostioren_HU/css/styles_002.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Ostioren_HU/css/styles_003.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Ostioren_HU/css/btn-trans.css" media="all" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Ostioren_HU/js/main.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="section wrap-block block1">
        <div class="wrapper-hidden1">
            <div class="wrapper">
                <div class="block1__top">
                    <span class="lt1">ÁRENGEDMÉNY</span> <span class="block1__top-sale">-50%</span> <span class="lt2">AZUTOLSÓ </span> <span class="lastpack">9</span> <span class="lt3">CSOMAGRA</span>
                    <span class="block1__top-action lt4">AKCIÓS ÁR</span>
                </div>
                <div class="block1__body">
                    <div class="block1__left">
                        <img alt="" src="http://st.acstnst.com/content/Ostioren_HU/i/img1.png"/>
                        <div class="block1__title lt5">Fáj a háta<br/> - vagy az ízületei?</div>
                        <div class="block1__sub lt6">A OSTEOREN krém hatásos szer az osteochondrosis, az artrózis és a traumák ellen!</div>
                    </div>
                    <div class="block1__right">
                        <div class="block1__price">
                            <span class="newprice price_main"><span class="price_main_value">10500</span><span class="price_main_currency">Ft.</span></span>
                            <span class="oldprice price_old"><s><span class="price_main_value">21000</span></s><span class="price_main_currency">Ft.</span></span>
                        </div>
                        <div class="block__timer">
                            <div class="block__timer-text"><a class="akciya1 scrollto lt7" href="#form">AZ AKCIÓ VÉGÉIG MÉG MARADT</a></div>
                            <div class="timer">
                                <div class="landing__countdown is-countdown"><span class="hours">13</span><span class="minutes">36</span><span class="seconds">31</span></div>
                                <div class="countdown__entities">
                                    <span class="lt8">karóra</span>
                                    <span class="lt9">perc</span>
                                    <span class="lt10">másodperc</span>
                                </div>
                            </div>
                        </div>
                        <ul class="unstyled">
                            <li class="lt11">Megszünteti a fájdalmas szindrómát</li>
                            <li class="lt12">Elősegíti a porcszövet regenerációját</li>
                            <li class="lt13">Hatásosan szünteti meg az izmok hipertonikusságát</li>
                            <li class="lt14">Küzd a vizenyővel</li>
                            <li class="lt15">Megszünteti a gyulladást</li>
                        </ul>
                    </div>
                </div>
                <img alt="" class="imlogo" src="http://st.acstnst.com/content/Ostioren_HU/i/u1.png"/>
            </div>
        </div>
    </div>
    <div class="section wrap-block block2">
        <div class="wrapper-hidden">
            <div class="block2__top">
                <div class="wrapper lt16">A OSTEOREN olyan szer, ami
                    gyorsan megszünteti az artrózisból és az oszteo-chondrózisból eredő
                    fájdalmat. Megszünteti az izomgörcsöt és a gyulladást. A OSTEOREN
                    ugyancsak hatásosan gyógyítja az oszteo-chondrózist és az artrózist,
                    mivel lassítja a porcszövet degenerációs folyamatát, és stimulálja benne
                    az anyagcserét,lehetővé téve az ízületi porc helyrejöttét. Pozitív
                    hatás észlelhető az első alkalmazás után, míg rendszeres használata
                    esetén jelentősen lehet fékezni az ízületek és a gerincoszlop
                    megbetegedésének progresszióját.</div>
            </div>
            <div class="block2__bottom">
                <div class="wrapper">
                    <div class="block2__title lt17">A Osteoren csak aktív összetevőkből áll!</div>
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block3">
        <div class="wrapper">
            <div class="block3__title lt18">A Osteoren krém előnyei a hát és az ízületek számára</div>
            <div class="block3__list">
                <ul class="inline">
                    <li class="item1">
                        <div class="block3__list_title lt19">SZÉLES <br/>HATÁSSPEKTRUM</div>
                        <span class="lt20">gyógyítja a gerincoszlop és az ízületek traumatikus és korral járó betegségeit</span>
                    </li>
                    <li class="item2">
                        <div class="block3__list_title lt21">NINCS <br/>MELLÉKHATÁSA</div>
                        <span class="lt22">teljes mértékben biztonságos alkalmi vagy rendszeres használat esetén</span>
                    </li>
                    <li class="item3">
                        <div class="block3__list_title lt23">100%-IG TERMÉSZETES <br/> ÖSSZETÉTEL</div>
                        <span class="lt24">csak gyógynövények aktív összetevői</span>
                    </li>
                    <li class="item4">
                        <div class="block3__list_title lt25">MAGAS SZINTŰ HATÁSOSSÁG</div>
                        <span class="lt26">állapotjavulás és fájdalomcsökkenés az első alkalmazás után</span>
                    </li>
                    <li class="item5">
                        <div class="block3__list_title lt27">SZAKÉRTŐK ÁLTAL ELLENŐRZÖTT</div>
                        <span class="lt28">tanúsított termék, és megfelel a minőségi szabványoknak</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section wrap-block block4">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block4__title lt29">Vásárlói vélemények</div>
                <ul class="inline">
                    <li>
                        <div class="block4__top lt30">A OSTEOREN krém
                            nagyon segített nekem az edzés utáni térdízület-sérüléskor. A térdem
                            bedagadt, nagyon fájt, gyakorlatilag nem tudtam megmozdítani a lábam. De
                            a OSTEOREN segítségével már 4 nap múlva visszatértem a munkába.
                            Járni pedig már az első napon tudtam.</div>
                        <div class="block4__bottom">
                            <img alt="" src="http://st.acstnst.com/content/Ostioren_HU/i/img3_002.png"/>
                            <div class="block4__bottom_name lt31">Jázmin, 31 éves, egy fitneszklub hálózat csoportos programjainak trénere. </div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt32">Korábban nagyon fájt a
                            hátam, oszteo-chondrózis, ami az én koromban nem ritka. A feleségem a
                            OSTEOREN krémet az interneten találta. Számomra ez igazi megmentővé
                            vált, korábban néha reggeltől még kiegyenesedni sem tudtam, most viszont
                            minden rendben van. S ami a legfőbb? eltűnt a hátfájás.</div>
                        <div class="block4__bottom">
                            <img alt="" src="http://st.acstnst.com/content/Ostioren_HU/i/img3.png"/>
                            <div class="block4__bottom_name lt33">Ádám, 58 éves, nyugdíjas</div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt34">Valahogy meghúzódtak a
                            szalagok a lábamon, az orvosok azt mondták, hogy sokáig fog tartan a
                            gyógyulás. De sikerült megvennem a OSTEOREN krémet, és egy hét múlva
                            már rendbejöttek a szalagok, a fájdalom eltűnt, normálisan járok, és a
                            betegállományból visszatértem a munkába. Most már az egész család
                            használja ezt a krémet, akinek fáj a háta vagy az ízületei, annak nagyon
                            segít.</div>
                        <div class="block4__bottom">
                            <img alt="" src="http://st.acstnst.com/content/Ostioren_HU/i/img3_003.png"/>
                            <div class="block4__bottom_name lt35">Viktória, eladó, 35 éves, eladó</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section wrap-block block5">
        <div class="wrapper">
            <div class="block5__title lt36">A Osteoren krémet szakértők ajánlják</div>
            <img alt="" src="http://st.acstnst.com/content/Ostioren_HU/i/img4.png"/>
            <div class="block5__wrap">
                <div class="block5__text lt37">A OSTEOREN krém kitűnő
                    szer az artrózis és az oszteo-chondrózis ellen. Minden olyan páciensnek
                    ajánlom, aki gerincfájással vagy ízületi fájdalmakkal fordul hozzám. A
                    OSTEOREN a legrövidebb időn belül megszünteti a fájdalomérzetet és a
                    vizenyőt, helyreállítja az ízületet és a szalagokat, lehetővé teszi az
                    aktív életmódhoz való visszatérést. A krém hatásos a korral járó
                    változások esetén is – én magam is használom az oszteo-chondrózis ellen,
                    nagyon tetszik a gyors hatás, s mi több az eredmény sokáig megmarad, és
                    rendszeres használat esetén teljesen el lehet feledkezni a
                    hátproblémákról.</div>
                <div class="block5__name lt38">Beketov, N.A. sportorvos, az orvostudományok kandidátusa</div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block6">
        <div class="wrapper">
            <div class="block6__title lt39">A Osteoren alkalmazási módja</div>
            <ul class="inline">
                <li class="lt40">Vigye fel a száraz bőrre, és dörzsölje be a szer teljes felszívódásáig </li>
                <li class="lt41">Naponta kétszer – háromszor alkalmazza</li>
                <li class="lt42">A felvitel után egy óráig ne mossa le vízzel</li>
            </ul>
        </div>
    </div>
    <div class="section wrap-block block7">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block7__delivery">
                    <div class="block7__delivery_title lt43">Kiszállítás és fizetés</div>
                    <span class="lt44">A Osteoren hát- és ízületi
fájdalmak elleni krémet a rendelés után két-három nappal kapja meg
(moszkvai és szentpétervári lakosok esetén). A többi régióba a
kiszállításra 4-7 napon belül kerül sor. A rendelésért az átvételkor
kell fizetni!</span>
                </div>
                <div class="block7__warning">
                    <div class="block7__warning_title lt45">Óvakodjanak az utánzatoktól!</div>
                    <span class="lt46">Az Orosz Köztársaság területén a
valódi OSTEOREN krém forgalmazása egyedi azonosítókóddal ellátott
eredeti csomagolásban történik!</span>
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block8">
        <div class="wrapper">
            <div class="block8__title lt48">Hogy lehet megrendelni OSTEOREN hát- és ízületi fájdalmak elleni krémet</div>
            <ul class="inline">
                <li class="lt49">Töltse ki a megrendelőlapot</li>
                <li class="lt50">Válassza ki a kiszállítás módját</li>
                <li class="lt51">Fizessen az áruért átvételkor</li>
            </ul>
        </div>
    </div>
    <div class="section wrap-block block10">
        <div class="wrapper">
            <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                <tbody>
                <tr>
                    <td class="block10__left" valign="top" width="500">
                        <div class="block10__title lt52">Árengedmény: <br/> 50%</div>
                        <div class="block10__price">
                            <span class="newprice price_main"><span class="price_main_value">10500</span><span class="price_main_currency">Ft.</span></span>
                            <span class="oldprice price_old"><span class="price_main_value">21000</span><span class="price_main_currency">Ft.</span></span>
                        </div>
                        <div class="block10__sale">
                            <span class="block10__count">15</span>
                            <div class="lt53">A LEGUTÓBBI ÓRÁBAN <br/>RENDELT CSOMAGOK SZÁMA</div>
                        </div>
                    </td>
                    <td valign="top" width="500">
                        <div class="block10-form" id="form">
                            <form action="" class="form-horizontal" id="order_form0" method="post">
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abdcn.pro/forms/?target=-4AAIJIAIjBgAAAAAAAAAAAASXAiF1AA"></iframe>

                                <div class="row">
                                    <div class="block10-delivery">
                                        <div class="delivery_price">Szállítás: <span class="newprice price_main"><span class="price_main_value">1500</span><span class="price_main_currency">Ft.</span></span></div>
                                        <div>Összesen: <span class="newprice price_main"><span class="price_main_value">12000.0</span><span class="price_main_currency">Ft.</span></span></div>
                                    </div>
                                </div>


                        </form>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="ouibounce-modal">
        <div class="underlay"></div>
        <div class="over-window">
            <div class="none">
                <div class="popup">
                    <div class="icon-close close-over close-ex"></div>
                    <div class="title">
                        <span class="lt62">Minden látogató az oldalon ma egy különleges ajánlat -</span>
                        <br/>
                        <span class="price_reduce lt63">50% árengedmény a OSTEOREN krémre</span>
                    </div>
                    <div class="content">
                        <div class="padding">
                            <form action="" class="order-form" id="order_form2" method="post">
                               <!-- <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="total_price" value="12000.0">
                                <input type="hidden" name="esub" value="-4A25sMQIJIAIkBgSlU2SLAAEAAQACqwUBAALUCgEKAQ8EEpuJrQA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="Osteoren - HU">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Ostioren_HU">
                                <input type="hidden" name="price" value="10500">
                                <input type="hidden" name="old_price" value="21000">
                                <input type="hidden" name="al" value="2772">
                                <input type="hidden" name="total_price_wo_shipping" value="10500.0">
                                <input type="hidden" name="currency" value="Ft.">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="HU">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="shipment_price" value="1500">
                                <input type="hidden" name="price_vat" value="0.0">
                                <input name="name" placeholder="TELJES NÉV" type="text"/>
                                <input class="only_number" name="phone" placeholder="AZ ÖN TELEFONSZÁMA" type="text"/>
                                <br/>
                                <input class="js_submit" type="submit" value="Hívjon fel engem"/>
                                <div class="hidden_select_wrapper">
                                    <select class="country_select" id="country_id_cb" name="country"><option selected="selected" value="HU">Magyarország</option></select>
                                </div>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abdcn.pro/forms/?target=-4AAIJIAIjBgAAAAAAAAAAAASXAiF1AA"></iframe>

                            </form>
                            <p class="bold lt64">A különleges ajánlatról
                                többet meg tudni és a OSTEOREN krémet akciósan megrendelni ingyenes
                                visszahívási szolgáltatásunkon keresztül lehet. <br/>Átlagos várakozási idő: 5-15 perc</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Ostioren_HU/js/ouibounce.js" type="text/javascript"></script>
    </body></html>
<?php } ?>