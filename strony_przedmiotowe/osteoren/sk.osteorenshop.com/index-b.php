<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>




    <!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- [pre]land_id = 6382 -->
        <script>var locale = "sk";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAL6EQRcHmeLAAEAAQACaxEBAALuGAEKAQ8EDncoEgA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Martin Mičiak';
            var phone_hint = '0908267142';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("sk");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Osteoren_SK_Purple/mobile/css/reset.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Osteoren_SK_Purple/mobile/css/main.css" rel="stylesheet"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <script>
            $(document).ready(function() {
                $('.toform').click(function(){
                    $("html, body").animate({scrollTop: $("form").offset().top-300}, 1000);
                    return false;
                });
            });

        </script>
    </head>
    <body>
    <div class="wrapper">
        <section class="p1">
            <div class="logo"><img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/mobile/i/logo.svg" width="90"/></div>
            <div class="lobster title">Bolí vás kĺby  <br/>
                alebo chrbát?</div>
            <div class="subtext">Krém Osteoren je účinný liek na osteochondritídu, artritídu a zranenia! </div>
        </section>
        <div class="line-light"></div>
        <section class="p2">
            <div class="title">ZNÍŽENÁ CENA <span class="discount lobster">50%</span></div>
            <div class="prices">
                <div class="box">
                    <div class="new-price lobster">35 €</div>
                    <div class="old-price lobster"><s>70 €</s></div>
                    <div class="btn toform"><button>Ponuka končí za</button></div>
                </div>
            </div>
        </section>
        <section class="p3">
            <div class="list">  Úľava od bolesti </div>
            <div class="list">  Spúšťa proces regenerácie chrupaviek </div>
            <div class="list">  Odstraňuje svalové napätie </div>
            <div class="list">  Zmenšuje opuchy </div>
            <div class="list">  Odstraňuje zápaly </div>
        </section>
        <div class="line-bold">
            <section class="p4">
                <div class="prod"><img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/mobile/i/tovar-new-1.png" width="90"/></div>
                <div class="text">OSTEOREN je liek, ktorý môže rýchlo odstrániť bolesť z artrózy a osteochondrózy.
                    Odstraňuje svalové kŕče a zápal. Osteoren je tiež účinný pri liečbe osteochondrózy a artrózy,
                    pretože spomaľuje proces degenerácie tkaniva chrupaviek a zvyšuje ich metobolismus a napomáha
                    regenerácii kĺbových chrupaviek. </div>
            </section>
        </div>
        <section class="p5">
            <h2 class="lobster">Hodnotenie zákazníkov</h2>
            <div class="comments">
                <div class="item">
                    <div class="photo"><img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/mobile/i/photo1.png"/> </div>
                    <div class="text">
                        <div class="author">Mária, 31, trenér pre skupinové programy v sieti fitness klubov </div>
                        <div class="comment">  Krém Osteoren mi veľmi pomohol, keď som si zranila kolenné kĺby pri cvičení. Moje koleno bolo opuchnuté a bolelo naozaj zle, ťažko som sa mohla pohybovať. Ale vďaka Osteoren som mohla chodiť hneď na druhý deň a do práce som sa vrátila o 4 dni neskôr.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/mobile/i/photo2.png"/> </div>
                    <div class="text">
                        <div class="author">Michal, 58 rokov, dôchodca </div>
                        <div class="comment">Chrbát ma veľmi silno bolel, mal som osteochondrózu, čo je celkom typické v mojom veku. Moja žena našla Osteoren na internete. Bola to pre mňa skutočná záchrana - ráno som mal problémy sa narovnaťh, a teraz je všetko v poriadku! Najdôležitejšou vecou je, že bolesť chrbta je preč.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/mobile/i/photo3.png"/> </div>
                    <div class="text">
                        <div class="author">Jana, 35, predavačka</div>
                        <div class="comment"> Raz som si vymkla členok a lekári mi povedali, že to bude trvať veľa času, aby sa vyliečil. Ale maa som šťastie, že som našla Osteoren. O týždeň neskôr sa kĺby zregenerovali, bolesť zmizla, mohla som chodiť bez problémov a vrátiť sa do práce.
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="bottom">
            <section class="p6">
                <div class="bot-prices">
                    <div class="bot-prod"><img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/mobile/i/img8-1.png" width="140"/></div>
                    <div class="discount lobster">
                        Iba dnes! <br/>
                        Zľava 50%
                    </div>
                    <div class="new-price lobster">35 €</div>
                    <div class="old-price lobster"><s>70 €</s></div>
                </div>
                <div class="order-form">
                    <form action="" method="post">
                        <!--<input type="hidden" name="total_price" value="35.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAL6EQRcHmeLAAEAAQACaxEBAALuGAEKAQ8EDncoEgA">
                        <input type="hidden" name="goods_id" value="79">
                        <input type="hidden" name="title" value="">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Osteoren_SK_Purple">
                        <input type="hidden" name="price" value="35">
                        <input type="hidden" name="old_price" value="70">
                        <input type="hidden" name="total_price_wo_shipping" value="35.0">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="currency" value="€">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="SK">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">

                        <div class="row">
                            <label>Krajina</label>
                            <select class="country_select" id="country_code_selector">
                                <option value="SK">
                                    Slovensko
                                </option>
                            </select>
                        </div>
                        <div class="row">
                            <label>Meno/Priezvisko</label>
                            <input id="name" name="name" placeholder="" type="text"/>
                        </div>
                        <div class="row">
                            <label>Telefón</label>
                            <input class="only_number" name="phone" placeholder="" type="text"/>
                        </div>
                        <div class="row btn-row">
                            <button class="js_submit order-btn">Objednať</button>
                        </div>-->
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAL6EQAAAAAAAAAAAASsPV6yAA"></iframe>

                    </form>
                </div>
            </section>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 6382 -->
        <script>var locale = "sk";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAL6EQRcHmeLAAEAAQACaxEBAALuGAEKAQ8EDncoEgA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Martin Mičiak';
            var phone_hint = '0908267142';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("sk");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Osteoren
        </title>
        <link href="//st.acstnst.com/content/Osteoren_SK_Purple/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_SK_Purple/css/style.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_SK_Purple/css/modal.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Lobster&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_SK_Purple/css/styles_003.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_SK_Purple/css/styles_002.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_SK_Purple/css/styles.css" media="all" rel="stylesheet" type="text/css"/>
        <!--<script type="text/javascript" src="js/es.js"></script>-->
        <script src="//st.acstnst.com/content/Osteoren_SK_Purple/js/main.js" type="text/javascript">
        </script>
    </head>
    <body>
    <div class="section wrap-block block1">
        <div class="wrapper-hidden1">
            <div class="wrapper">
                <div class="block1__top">
                    zľava
                    <span class="block1__top-sale">-50%
            </span>  na posledných
                    <span class="lastpack">9
            </span>
                    balení
                    <span class="block1__top-action">  znížená cena
            </span>
                </div>
                <div class="block1__body">
                    <div class="block1__left">
                        <img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/i/logo.svg" width="180"/>
                        <div class="block1__title">
                            Bolí vás kĺby
                            <br/>
                            alebo chrbát?
                        </div>
                        <div class="block1__sub">
                            Krém Osteoren je účinný liek na osteochondritídu, artritídu a zranenia!
                        </div>
                    </div>
                    <div class="block1__right">
                        <div class="block1__price">
<span class="newprice price_main">35 €
                </span>
                            <span class="oldprice price_old">
<s>70 €
                  </s>
</span>
                        </div>
                        <div class="block__timer">
                            <div class="block__timer-text">
                                <a class="akciya1 scrollto lt7" href="#form"> Ponuka končí za
                                </a>
                            </div>
                            <div class="timer">
                                <div class="landing__countdown is-countdown">
<span class="hours">16
                    </span>
                                    <span class="minutes">24
                    </span>
                                    <span class="seconds">18
                    </span>
                                </div>
                                <div class="countdown__entities">
<span class="lt8">hodín
                    </span>
                                    <span class="lt9">minút
                    </span>
                                    <span class="lt10">sekúnd
                    </span>
                                </div>
                            </div>
                        </div>
                        <ul class="unstyled">
                            <li>  Úľava od bolesti
                            </li>
                            <li>  Spúšťa proces regenerácie chrupaviek
                            </li>
                            <li>  Odstraňuje svalové napätie
                            </li>
                            <li>  Zmenšuje opuchy
                            </li>
                            <li>  Odstraňuje zápaly
                            </li>
                        </ul>
                    </div>
                </div>
                <img alt="" class="imlogo" src="//st.acstnst.com/content/Osteoren_SK_Purple/i/u1.png"/>
            </div>
        </div>
    </div>
    <div class="section wrap-block block2">
        <div class="wrapper-hidden">
            <div class="block2__top">
                <div class="wrapper lt16">OSTEOREN je liek, ktorý môže rýchlo odstrániť bolesť z artrózy a osteochondrózy. Odstraňuje svalové kŕče a zápal. Osteoren je tiež účinný pri liečbe osteochondrózy a artrózy, pretože spomaľuje proces degenerácie tkaniva chrupaviek a zvyšuje ich metobolismus a napomáha regenerácii kĺbových chrupaviek. Pozitívny efekt možno vidieť hneď po prvej aplikácii. Ak sa používa pravidelne, choroby kĺbov a chrbtice nepostupujú.
                </div>
            </div>
            <div class="block2__bottom">
                <div class="wrapper">
                    <div class="block2__title lt17">Osteoren je vyrobený výhradne z aktívnych zložiek
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block3">
        <div class="wrapper">
            <div class="block3__title lt18">Výhody krému Osteoren pre chrbát a kĺby
            </div>
            <div class="block3__list">
                <ul class="inline">
                    <li class="item1">
                        <div class="block3__list_title lt19"> Široké spektrum
                            <br/>účinkov
                        </div>
                        <span class="lt20"> lieči traumatické ochorení chrbtice a tie súvisiace s vekom
              </span>
                    </li>
                    <li class="item2">
                        <div class="block3__list_title lt21"> Bez vedľajších
                            <br/>  účinkov
                        </div>
                        <span class="lt22"> Bezpečné pre jednorazové alebo pravidelné používanie
              </span>
                    </li>
                    <li class="item3">
                        <div class="block3__list_title lt23"> 100% prírodné
                            <br/>
                            zloženie
                        </div>
                        <span class="lt24">len aktívne bylinné a rastlinné komponenty
              </span>
                    </li>
                    <li class="item4">
                        <div class="block3__list_title lt25">Vysoká účinnosť
                        </div>
                        <span class="lt26">zlepšuje zdravotný stav a znižuje bolesť hneď po prvej aplikácii
              </span>
                    </li>
                    <li class="item5">
                        <div class="block3__list_title lt27">Overené odborníkmi
                        </div>
                        <span class="lt28"> Výrobok je certifikovaný a spĺňa požiadavky noriem
              </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section wrap-block block4">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block4__title lt29">Hodnotenie zákazníkov
                </div>
                <ul class="inline">
                    <li>
                        <div class="block4__top lt30"> Krém Osteoren mi veľmi pomohol, keď som si zranila kolenné kĺby pri cvičení. Moje koleno bolo opuchnuté a bolelo naozaj zle, ťažko som sa mohla pohybovať. Ale vďaka Osteoren som mohla chodiť hneď na druhý deň a do práce som sa vrátila o 4 dni neskôr.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/i/img3.7.1.png"/>
                            <div class="block4__bottom_name lt31">Mária, 31, trenér pre skupinové programy v sieti fitness klubov
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt32">Chrbát ma veľmi silno bolel, mal som osteochondrózu, čo je celkom typické v mojom veku. Moja žena našla Osteoren na internete. Bola to pre mňa skutočná záchrana - ráno som mal problémy sa narovnaťh, a teraz je všetko v poriadku! Najdôležitejšou vecou je, že bolesť chrbta je preč.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/i/img3.7.2.png"/>
                            <div class="block4__bottom_name lt33"> Michal, 58 rokov, dôchodca
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top lt34"> Raz som si vymkla členok a lekári mi povedali, že to bude trvať veľa času, aby sa vyliečil. Ale maa som šťastie, že som našla Osteoren. O týždeň neskôr sa kĺby zregenerovali, bolesť zmizla, mohla som chodiť bez problémov a vrátiť sa do práce.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/i/img3.7.3.png"/>
                            <div class="block4__bottom_name lt35"> Jana, 35, predavačka
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section wrap-block block5">
        <div class="wrapper">
            <div class="block5__title lt36">Odborníci odporúčajú krém Osteoren
            </div>
            <img alt="" src="//st.acstnst.com/content/Osteoren_SK_Purple/i/img4.2_a2.png"/>
            <div class="block5__wrap">
                <div class="block5__text lt37">   Krém Osteoren je skvelý liek na artritídu a osteochondrózu. Odporúčam ho všetkým svojim pacientom, ktorí prichádzajú ku mne s problémami chrbtice a bolestmi kĺbov. Osteoren zmierňuje bolesti a opuchy v najkratšom možnom čase, obnovuje kĺby a väzy, umožňuje návrat k aktívnemu životnému štýlu. Krém je účinný aj pri zmenách súvisiacich so starnutím,  používam ho sám pri osteochondróze, mám rád jeho rýchly a dlhotrvajúci účinok. A ak sa používa pravidelne, môžete zabudnúť na problémy s chrbtom nadobro!
                </div>
                <div class="block5__name lt38"> MUDr. Alexander Eduard
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block6">
        <div class="wrapper">
            <div class="block6__title lt39">Ako používať Osteoren
            </div>
            <ul class="inline">
                <li>
                    Naneste krém na suchú pokožku a votrite ho až do úplného vstrebania
                </li>
                <li>
                    Používajte 2-3 krát denne
                </li>
                <li>
                    Neoplachujte vodou počas prvej hodiny po aplikácii
                </li>
            </ul>
        </div>
    </div>
    <div class="section wrap-block block7">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block7__delivery">
                    <div class="block7__delivery_title lt43">Poštovné a platba
                    </div>
                    <span class="lt44">Obdržíte krém na chrbát a kĺby Osteoren 2-3 dní po objednaní. Platba na dobierku!
            </span>
                </div>
                <div class="block7__warning">
                    <div class="block7__warning_title lt45">Pozor na falzifikáty!
                    </div>
                    <span class="lt46"> Na Slovensku sa skutočný Osteoren predáva v originálnom balení s jedinečným registračným číslom!
              <br/>
<br/>
              Nájdite kód na obale a skontrolujte
            </span>
                </div>
            </div>
        </div>
    </div>
    <div class="section wrap-block block8">
        <div class="wrapper">
            <div class="block8__title lt48">Ako objednať krém na chrbtici a kĺby Osteoren
            </div>
            <ul class="inline">
                <li class="lt49">Vyplniť objednávkový formulár
                </li>
                <li class="lt50">Vyberte si spôsob dodania
                </li>
                <li class="lt51">Platíte za výrobok pri dodaní
                </li>
            </ul>
        </div>
    </div>
    <div class="section wrap-block block10">
        <div class="wrapper">
            <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                <tbody>
                <tr>
                    <td class="block10__left" valign="top" width="500">
                        <div class="block10__title lt52">Iba dnes!
                            <br/> Zľava 50%
                        </div>
                        <div class="block10__price">
<span class="newprice price_main">
<span class="price_main_value">35
                    </span>
<span class="price_main_currency">€
                    </span>
</span>
                            <span class="oldprice price_old">
<span class="price_main_value">70
                    </span>
<span class="price_main_currency">€
                    </span>
</span>
                        </div>
                        <div class="block10__sale">
<span class="block10__count">15
                  </span>
                            <div>  BALENÍ OBJEDNANÝCH
                                <br/>
                                POČAS POSLEDNEJ HODINY
                            </div>
                        </div>
                        <div class="block10__allsale landing__maxpurcashe">
                        </div>
                    </td>
                    <td valign="top" width="500">
                        <div class="block10-form" id="form">
                            <form action="" class="form-horizontal" id="order_form0" method="post">
                                <!--<input type="hidden" name="total_price" value="35.0">
                                <input type="hidden" name="esub" value="-4A25sMQIJIAL6EQRcHmeLAAEAAQACaxEBAALuGAEKAQ8EDncoEgA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Osteoren_SK_Purple">
                                <input type="hidden" name="price" value="35">
                                <input type="hidden" name="old_price" value="70">
                                <input type="hidden" name="total_price_wo_shipping" value="35.0">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="currency" value="€">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="SK">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="shipment_price" value="0">
                                <input type="hidden" name="price_vat" value="0.0">
                                <div class="row ">
                                    <label for="country_code_selector">  Krajina
                                    </label>
                                    <div class="row-select">
                                        <select class="country_select" id="country_code_selector">
                                            <option value="SK">
                                                Slovensko
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row ">
                                    <label for="label1">
<span class="lt55">Meno/Priezvisko
                        </span>
                                    </label>
                                    <input id="label1" name="name" type="text"/>
                                </div>
                                <div class="row ">
                                    <label for="lebel2">
<span class="lt56">Telefón
                        </span>
                                    </label>
                                    <input class="only_number" id="lebel2" name="phone" type="text"/>
                                </div>
                                <div class="row row-submit">
                                    <button class="block10-btn lt57 js_submit" type="submit">Objednať
                                    </button>
                                </div>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abcdg.pro/forms/?target=-4AAIJIAL6EQAAAAAAAAAAAASsPV6yAA"></iframe>

                            </form>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="ouibounce-modal">
        <div class="underlay">
        </div>
        <div class="over-window">
            <div class="none">
                <div class="popup">
                    <div class="icon-close close-over close-ex">
                    </div>
                    <div class="title">
<span class="lt62">Pre všetkých návštevníkov dnes máme špeciálnu ponuku
              </span>
                        <br/>
                        <span class="price_reduce lt63">50% zľava na Osteoren
              </span>
                    </div>
                    <div class="content">
                        <div class="padding">
                            <form action="" class="order-form" id="order_form2" method="post">
                                <!--<input type="hidden" name="total_price" value="35.0">
                                <input type="hidden" name="esub" value="-4A25sMQIJIAL6EQRcHmeLAAEAAQACaxEBAALuGAEKAQ8EDncoEgA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Osteoren_SK_Purple">
                                <input type="hidden" name="price" value="35">
                                <input type="hidden" name="old_price" value="70">
                                <input type="hidden" name="total_price_wo_shipping" value="35.0">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="currency" value="€">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="SK">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="shipment_price" value="0">
                                <input type="hidden" name="price_vat" value="0.0">
                                <select class="country_select" id="country_code_selector">
                                    <option value="SK">
                                        Slovensko
                                    </option>
                                </select>
                                <input name="name" placeholder="Meno/Priezvisko" type="text"/>
                                <input class="only_number" name="phone" placeholder="Telefón" type="text"/>
                                <br/>
                                <input class="js_submit" type="submit" value="Objednať"/>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abcdg.pro/forms/?target=-4AAIJIAL6EQAAAAAAAAAAAASsPV6yAA"></iframe>

                            </form>
                            <p class="bold lt64"> Môžete sa dozvedieť o našich špeciálnych ponukách a objednávke krému Osteoren za zvýhodnenú cenu s použitím
                                <strong>  našej služby
                                </strong>  spätného volaní zadarmo.
                                <br/>
                                Priemerná čakacia doba: 5-15 minút
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Osteoren_SK_Purple/js/ouibounce.js" type="text/javascript">
    </script>
    </body>
    </html>
<?php } ?>