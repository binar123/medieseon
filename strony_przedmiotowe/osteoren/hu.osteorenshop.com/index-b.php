<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 2421 -->
        <script>var locale = "ro";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4AAIJIAIjBgRRBAxaAAEAAQACqgUBAAJ1CQEKAQ8EBFDmegA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 20;
            var name_hint = 'Zoltan Pecsi';
            var phone_hint = '+40744531888';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ro");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Osteoren_Ro_New/mobile/css/reset.css" rel="stylesheet"/>
        <link href="//st.acstnst.com/content/Osteoren_Ro_New/mobile/css/main.css" rel="stylesheet"/>
        <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport"/>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.toform').click(function(){
                    $("html, body").animate({scrollTop: $(".js_submit").offset().top-300}, 1000);
                    return false;
                });
            });
        </script>
    </head>
    <body>
    <div class="wrapper">
        <section class="p1">
            <div class="logo"><img src="//st.acstnst.com/content/Osteoren_Ro_New/mobile/i/s_logo.png"/></div>
            <div class="lobster title">Dureri de spate
                sau<br/>ale articulaţiilor?</div>
            <div class="subtext">Gel Osteoren – un remediu eficient împotriva osteohondrozei, artrozei şi traumelor!</div>
        </section>
        <div class="line-light"></div>
        <section class="p2">
            <div class="title">REDUCERE <span class="discount lobster">50%</span></div>
            <div class="prices">
                <div class="box">
                    <div class="new-price lobster">159 RON</div>
                    <div class="old-price lobster">318 RON</div>
                    <div class="btn toform"><button>PÂNĂ LA SFÂRŞITUL CAMPANIEI AU MAI RĂMAS</button></div>
                </div>
            </div>
        </section>
        <section class="p3">
            <div class="list">Reduce durerea</div>
            <div class="list">Ajută la regenerarea cartilajului</div>
            <div class="list">Elimină în mod eficient hipertonicitatea musculară</div>
            <div class="list">Luptă cu edemele</div>
            <div class="list">Elimină inflamaţia</div>
        </section>
        <div class="line-bold">
            <section class="p4">
                <div class="prod"><img src="//st.acstnst.com/content/Osteoren_Ro_New/mobile/i/produkt.png" style="width:105px;"/></div>
                <div class="text">Osteoren – un remediu care elimină rapid durerea provocată de artrite şi osteohondroză. Reduce spasmele musculare şi elimină inflamaţia.
                </div>
            </section>
        </div>
        <section class="p5">
            <h2 class="lobster">Recenziile clienţilor</h2>
            <div class="comments">
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_Ro_New/mobile/i/photo1.png"/> </div>
                    <div class="text">
                        <div class="author">Lidia, 31 de ani </div>
                        <div class="comment">
                            Gel Osteoren m-a ajutat foarte mult în legătură cu trauma genunchiului în urma antrenamentului. Peste 4 zile m-am întors la locul de muncă. Şi am putut merge chiar din prima zi de utilizare.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_Ro_New/mobile/i/photo2.png"/> </div>
                    <div class="text">
                        <div class="author">Mihai, 58 de ani </div>
                        <div class="comment">
                            Soţia mea a găsit gel Osteoren pe internet. Gel a fost pentru mine o adevărată salvare – înainte nu-mi puteam îndrepta spatele de dimineaţă, iar acum totul este bine. Şi cel mai important – durerile de spate au dispărut.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo"><img src="//st.acstnst.com/content/Osteoren_Ro_New/mobile/i/photo3.png"/> </div>
                    <div class="text">
                        <div class="author">Nadejda, 35 de ani </div>
                        <div class="comment">
                            Cu puţin timp în urmă mi-am luxat un ligament al gleznei. Dar am fost norocoasă că am cumpărat gel Osteoren şi deja peste o săptămână ligamentele s-au restabilit, durerea a trecut, merg în mod normal, am revenit din concediul medical.
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="bottom">
            <section class="p6">
                <div class="bot-prices">
                    <div class="bot-prod"><img style="width:125px;" src="//st.acstnst.com/content/Osteoren_Ro_New/mobile/i/produkt.png"/></div>
                    <div class="discount lobster">Doar astăzi! <br/>Reducere 50%</div>
                    <div class="new-price lobster">159 RON</div>
                    <div class="old-price lobster">318 RON</div>
                </div>
                <div class="order-form">
                    <form action="" method="post"><!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                       <!-- <input type="hidden" name="total_price" value="179.0">
                        <input type="hidden" name="esub" value="-4AAIJIAIjBgRRBAxaAAEAAQACqgUBAAJ1CQEKAQ8EBFDmegA">
                        <input type="hidden" name="goods_id" value="79">
                        <input type="hidden" name="title" value="Osteoren - RO">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Osteoren_Ro_New">
                        <input type="hidden" name="price" value="159">
                        <input type="hidden" name="old_price" value="318">
                        <input type="hidden" name="al" value="2421">
                        <input type="hidden" name="total_price_wo_shipping" value="159.0">
                        <input type="hidden" name="currency" value="RON">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="RO">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="shipment_price" value="20">
                        <input type="hidden" name="price_vat" value="0.0">

                        <div class="row">
                            <label>Țara:</label>
                            <select class="country_select" id="country" name="country">
                                <option selected="selected" value="RO">România</option>
                            </select>
                        </div>
                        <div class="row">
                            <label>NUMELE, PRENUMELE</label>
                            <input id="name" name="name" placeholder="" type="text"/>
                        </div>
                        <div class="row">
                            <label>TELEFONUL DVS</label>
                            <input class="only_number" name="phone" placeholder="" type="text"/>
                        </div>
                        <div class="row btn-row">
                            <button class="js_submit order-btn">comandaţi</button>
                        </div>-->
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAIkBgAAAAAAAAAAAARjEBPwAA"></iframe>

                    </form>
                </div>
            </section>
        </div>
    </div>
    </body>
    </html>
<?php } else { ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 2421 -->
        <script>var locale = "ro";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4AAIJIAIjBgRRBAxaAAEAAQACqgUBAAJ1CQEKAQ8EBFDmegA";</script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/jquery.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/placeholders.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dr.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/dtime.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/js.cookie.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/validation.js"></script>
        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="//st.acstnst.com/content/!common_files/js/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 20;
            var name_hint = 'Zoltan Pecsi';
            var phone_hint = '+40744531888';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("ro");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="//st.acstnst.com/content/!common_files/css/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Osteoren</title>
        <link href="//st.acstnst.com/content/Osteoren_Ro_New/css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_Ro_New/css/style.min.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="//st.acstnst.com/content/Osteoren_Ro_New/css/modal.css" rel="stylesheet" type="text/css"/>
        <script src="//st.acstnst.com/content/Osteoren_Ro_New/js/jqueryplugin.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Osteoren_Ro_New/js/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Osteoren_Ro_New/js/main.js" type="text/javascript"></script>
        <script src="//st.acstnst.com/content/Osteoren_Ro_New/js/main2.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
    <div class="wrap-block block1">
        <div class="wrapper-hidden1">
            <div class="wrapper">
                <div class="block1__top">
                    REDUCERE <span class="block1__top-sale">-50%</span> LA ULTIMELE <span class="lastpack" id="countp">9</span> CUTII RĂMASE
                    <span class="block1__top-action">PREŢ PROMOŢIONAL</span>
                </div>
                <div class="block1__body">
                    <div class="block1__left">
                        <img alt="" src="//st.acstnst.com/content/Osteoren_Ro_New/images/log1.png"/>
                        <div class="block1__title">
                            Dureri de spate
                            <br/> sau ale articulaţiilor?
                        </div>
                        <div class="block1__sub">
                            Gel Osteoren – un remediu eficient împotriva osteohondrozei, artrozei şi traumelor!
                        </div>
                    </div>
                    <div class="block1__right">
                        <div class="block1__price">
                            <span class="newprice price_main">159 RON</span>
                            <span class="oldprice price_old">318 RON</span>
                        </div>
                        <div class="block__timer">
                            <div class="block__timer-text"><a class="akciya1 scrollto" href="#bottom_form" id="scroll_to_bottom">PÂNĂ LA SFÂRŞITUL CAMPANIEI AU MAI RĂMAS</a></div>
                            <div class="timer">
                                <div class="landing__countdown timeqwqw"></div>
                            </div>
                        </div>
                        <ul class="unstyled">
                            <li>Reduce durerea</li>
                            <li>Ajută la regenerarea cartilajului</li>
                            <li>Elimină în mod eficient hipertonicitatea musculară</li>
                            <li>Luptă cu edemele</li>
                            <li>Elimină inflamaţia</li>
                        </ul>
                    </div>
                </div>
                <img alt="" class="imlogo" src="//st.acstnst.com/content/Osteoren_Ro_New/images/u1.png"/>
            </div>
        </div>
    </div>
    <div class="wrap-block block2">
        <div class="wrapper-hidden">
            <div class="block2__top">
                <div class="wrapper">
                    Osteoren – un remediu care elimină rapid durerea provocată de artrite şi osteohondroză. Reduce spasmele musculare şi elimină inflamaţia. Osteoren tratează în mod eficient osteohondroza și artroza, deoarece încetineşte degenerarea cartilajului şi stimulează metabolismul acestuia, contribuind la refacerea cartilajului articular. Efectul curativ este vizibil după prima aplicare, iar în cazul unei utilizări regulate poate încetini semnificativ progresul bolii articulaţiilor şi coloanei vertebrale.
                </div>
            </div>
            <div class="block2__bottom">
                <div class="wrapper">
                    <div class="block2__title">Osteoren este compus doar din ingrediente active!</div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-block block3">
        <div class="wrapper">
            <div class="block3__title">
                Beneficiile gel pentru spate şi articulaţii Osteoren
            </div>
            <div class="block3__list">
                <ul class="inline">
                    <li class="item1">
                        <div class="block3__list_title">
                            SPECTRU LARG
                            <br/> DE ACŢIUNE
                        </div>
                        tratează traumele si bolile legate de vârstă ale coloanei vertebrale și articulațiilor
                    </li>
                    <li class="item2">
                        <div class="block3__list_title">
                            NU ARE EFECTE
                            <br/> ADVERSE
                        </div>
                        absolut sigur pentru utilizare ocazională sau regulată
                    </li>
                    <li class="item3">
                        <div class="block3__list_title">
                            100% COMPOZIȚIE<br/> NATURALĂ
                        </div>
                        doar ingrediente active din plante medicinale
                    </li>
                    <li class="item4">
                        <div class="block3__list_title">EFICIENŢĂ ÎNALTĂ</div>
                        ameliorarea simptomelor și reducerea durerii chiar după prima aplicare
                    </li>
                    <li class="item5">
                        <div class="block3__list_title">VERIFICAT DE EXPERŢI</div>
                        produsul este certificat și îndeplinește standardele de calitate
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="wrap-block block4">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block4__title">Recenziile clienţilor</div>
                <ul class="inline">
                    <li>
                        <div class="block4__top">
                            Gel Osteoren m-a ajutat foarte mult în legătură cu trauma genunchiului în urma antrenamentului. Genunchiul era umflat, mă durea foarte tare, aproape că nu puteam să-mi mişc piciorul. Dar, cu ajutorul Osteoren peste 4 zile m-am întors la locul de muncă. Şi am putut merge chiar din prima zi de utilizare.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_Ro_New/images/img3.7.1.png"/>
                            <div class="block4__bottom_name">
                                Lidia, 31 de ani antrenoare într-o reţea de fitness cluburi
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top">
                            Înainte mă durea foarte tare spatele, aveam osteohondroză, care la vârsta mea se întâlneşte fecvent. Soţia mea a găsit gel Osteoren pe internet. Gel a fost pentru mine o adevărată salvare – înainte nu-mi puteam îndrepta spatele de dimineaţă, iar acum totul este bine. Şi cel mai important – durerile de spate au dispărut.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_Ro_New/images/img3.7.2.png"/>
                            <div class="block4__bottom_name">
                                Mihai, 58 de ani pensionar
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="block4__top">
                            Cu puţin timp în urmă mi-am luxat un ligament al gleznei, doctorii mi-au spus că este necesar un tratament de durată. Dar am fost norocoasă că am cumpărat gel Osteoren şi deja peste o săptămână ligamentele s-au restabilit, durerea a trecut, merg în mod normal, am revenit din concediul medical. Acum folosim această gel întreaga familie – este foarte eficientă în cazul durerilor de spate sau de articulaţii.
                        </div>
                        <div class="block4__bottom">
                            <img alt="" src="//st.acstnst.com/content/Osteoren_Ro_New/images/img3.7.3.png"/>
                            <div class="block4__bottom_name">
                                Nadejda, 35 de ani agent de vânzări
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="wrap-block block5">
        <div class="wrapper">
            <div class="block5__title">Gel Osteoren este recomandată de către experţi</div>
            <img alt="" src="//st.acstnst.com/content/Osteoren_Ro_New/images/img4.2.png"/>
            <div class="block5__wrap">
                <div class="block5__text">
                    Gel Osteoren – un remediu excelent împotriva artrozei şi osteohondrozei. O recomand tuturor pacienţilor, care se plâng de dureri de coloană şi dureri ale articulaţiilor. Osteoren elimină foarte rapid durerile şi inflamaţia, regenerează articulaţiile şi ligamentele, vă permite să vă întoarceţi la un regim de viaţă activ. Gel este eficientă şi în cazul modificărilor legate de vârstă – o folosesc personal ca remediu împotriva osteohondrozei, îmi place acţiunea ei rapidă, cu toate acestea efectul durează mult timp, iar în cazul unei utilizări regulate, puteţi uita de problemele de spate.
                </div>
                <div class="block5__name">
                    Beketov N. A., medic sportiv, doctor în medicină
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-block block6">
        <div class="wrapper">
            <div class="block6__title">
                Instrucţiunile de utilizare ale gel Osteoren
            </div>
            <ul class="inline">
                <li>
                    Aplicaţi pe pielea uscată şi masaţi până când gel se absoarbe complet
                </li>
                <li>
                    Utilizaţi de 2-3 ori pe zi
                </li>
                <li>
                    Nu spălaţi cu apă timp de o oră după aplicare
                </li>
            </ul>
        </div>
    </div>
    <div class="wrap-block block7">
        <div class="wrapper-hidden">
            <div class="wrapper">
                <div class="block7__delivery">
                    <div class="block7__delivery_title">Livrarea şi plata</div>
                    Veţi primi gel pentru spate şi articulaţii Osteoren peste două-trei zile după efectuarea comenzii. În celelalte regiuni, livrarea se efectuează în termen de 7 zile. Plata se face la primirea comenzii!
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-block block8">
        <div class="wrapper">
            <div class="block8__title">Cum poate fi comandată gel pentru spate şi articulaţii Osteoren </div>
            <ul class="inline">
                <li>Completați formularul de comandă</li>
                <li>Selectați metoda de livrare</li>
                <li>Plătiţi pentru produs la livrare</li>
            </ul>
        </div>
    </div>
    <div class="wrap-block block10">
        <div class="wrapper">
            <table border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
                <tbody>
                <tr>
                    <td class="block10__left" valign="top" width="500">
                        <div class="block10__title">
                            Doar astăzi!
                            <br/> Reducere 50%
                        </div>
                        <div class="block10__price">
                            <span class="newprice price_main">159 RON</span>
                            <span class="oldprice price_old">318 RON</span>
                        </div>
                        <div class="block10__sale">
                            <span class="block10__count">15</span>
                            <div>CUTII COMANDATE ÎN DECURSUL ULTIMEI ORE</div>
                        </div>
                        <div class="block10__allsale landing__stockinfo">
                        </div>
                    </td>
                    <td valign="top" width="500">
                        <div class="block10-form" id="bottom_form">
                            <form action="" class="form-horizontal" id="order_form" method="POST">
                                <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="total_price" value="179.0">
                                <input type="hidden" name="esub" value="-4AAIJIAIjBgRRBAxaAAEAAQACqgUBAAJ1CQEKAQ8EBFDmegA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="Osteoren - RO">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Osteoren_Ro_New">
                                <input type="hidden" name="price" value="159">
                                <input type="hidden" name="old_price" value="318">
                                <input type="hidden" name="al" value="2421">
                                <input type="hidden" name="total_price_wo_shipping" value="159.0">
                                <input type="hidden" name="currency" value="RON">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="RO">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="shipment_price" value="20">
                                <input type="hidden" name="price_vat" value="0.0">

                                <div class="row ">
                                    <label class="block_form_label">Țara:</label>
                                    <select class="country_select" id="country" name="country">
                                        <option selected="selected" value="RO">România</option>
                                    </select>
                                </div>
                                <div class="row ">
                                    <label for="label1">Numele, Prenumele, Patronimicul <small class="name_helper">De exemplu: Ion Enache</small></label>
                                    <input id="label1" name="name" type="text"/>
                                </div>
                                <div class="row ">
                                    <label for="lebel2">Telefonul DVS <small class="info_phone phone_helper">De exemplu: +40 123456789</small></label>
                                    <input class="only_number" id="lebel2" name="phone" type="text"/>
                                </div>
                                <div class="row row-submit">
                                    <button class="block10-btn js_submit" type="submit">comandaţi</button>
                                </div>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abcdg.pro/forms/?target=-4AAIJIAIkBgAAAAAAAAAAAARjEBPwAA"></iframe>

                            </form>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="wrap-block block11">
        <div class="wrapper">
        </div>
    </div>
    <div class="ouibounce-modal" id="ouibounce-modal" style="display: none;">
        <div class="underlay"></div>
        <div class="over-window">
            <div class="none">
                <div class="popup">
                    <div class="icon-close close-over close-ex"></div>
                    <div class="title">
                        Pentru toţi vizitatorii site-ului astăzi funcţionează o ofertă specială -
                        <br/><span class="reduced_price">gel OSTEOREN cu o reducere de 50%</span>
                    </div>
                    <div class="content">
                        <div class="padding">
                            <form action="" class="order-form" id="order_form2" method="POST">
                                <!--<input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                                <input type="hidden" name="total_price" value="179.0">
                                <input type="hidden" name="esub" value="-4AAIJIAIjBgRRBAxaAAEAAQACqgUBAAJ1CQEKAQ8EBFDmegA">
                                <input type="hidden" name="goods_id" value="79">
                                <input type="hidden" name="title" value="Osteoren - RO">
                                <input type="hidden" name="ip_city" value="Toruń">
                                <input type="hidden" name="template_name" value="Osteoren_Ro_New">
                                <input type="hidden" name="price" value="159">
                                <input type="hidden" name="old_price" value="318">
                                <input type="hidden" name="al" value="2421">
                                <input type="hidden" name="total_price_wo_shipping" value="159.0">
                                <input type="hidden" name="currency" value="RON">
                                <input type="hidden" name="package_id" value="0">
                                <input type="hidden" name="protected" value="None">
                                <input type="hidden" name="ip_country_name" value="Poland">
                                <input type="hidden" name="country_code" value="RO">
                                <input type="hidden" name="shipment_vat" value="0.0">
                                <input type="hidden" name="ip_country" value="PL">
                                <input type="hidden" name="package_prices" value="{}">
                                <input type="hidden" name="shipment_price" value="20">
                                <input type="hidden" name="price_vat" value="0.0">

                                <select class="country_select" id="country" name="country">
                                    <option selected="selected" value="RO">România</option>
                                </select>
                                <input name="name" placeholder="NUMELE, PRENUMELE" type="text" value=""/>
                                <input class="only_number" name="phone" placeholder="TELEFONUL DVS" type="text" value=""/>
                                <br/>
                                <input class="js_submit" type="submit" value="comandaţi"/>-->
                                <iframe scrolling="no"
                                        style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                        src="http://abcdg.pro/forms/?target=-4AAIJIAIkBgAAAAAAAAAAAARjEBPwAA"></iframe>

                            </form>
                            <p class="bold">Aflaţi mai multe despre oferta specială, şi comandaţi OSTEOREN în baza ofertei speciale, prin intermediul serviciului nostru gratuit de apeluri inverse.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//st.acstnst.com/content/Osteoren_Ro_New/js/ouibounce.min.js" type="text/javascript"></script>
    </body>
    </html>
<?php } ?>