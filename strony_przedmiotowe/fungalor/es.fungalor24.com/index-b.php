<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()){ ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 3332 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAK-CwRqvNaLAAEAAQACXgsBAAIEDQEKAQ8EqBccMgA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Judit flores';
            var phone_hint = '+34681435813';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Fresh Fingers</title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <!--  Удалить  при заливке  -->
        <!--  Удалить  при заливке  -->
        <link href="./assets/mobile/style.css" rel="stylesheet"/>
    </head>
    <body>
    <!--HEADER-->

    <main>
        <header>
            <div class="container_gradient">
                <div class="warning">
                    <div class="main_container">
                        <p>
                            <b>WARNING:</b> Debido a la extremadamente alta demanda en TV, hay un suministro limitado del Limpiador y Purificador así como del
                            <script>dtime_nums(-1, true)</script>
                        </p>
                    </div>
                </div>
                <div class="main_container">
                    <div class="row">
                        <div class="logo">
                            <img src=./assets/mobile/logo_03.png"/>
                        </div>
                        <h1>AGENTE ANTIMICÓTICO Fresh Fingers</h1>
                        <h6>
                            ¡Cuide la salud de sus pies!
                        </h6>
                    </div>
                    <div class="row">
                        <ul class="header_ul">
                            <li>
                                destrucción de la <br/>
                                <b>infección micótica</b>
                            </li>
                            <li>
                                <b>recuperación de<br/>la estructura</b><br/>
                                de la placa de la uña
                            </li>
                            <li>
                                <b>eliminación</b>del <br/>
                                proceso inflamatorio
                            </li>
                        </ul>
                        <form action="./process.php" method="post">

                            <div class="price text_l">
<span>
                                39 €
                            </span>
                                <span>
<s>78 €</s>
</span>
                            </div>
                            <div class="inputs">
                                <p> Díganos dónde enviar  <span> su paquete </span></p>
                                <select id="country_code_selector">
                                    <option value="ES">España</option>

                                <input name="name" placeholder="Nombre" type="text">
                                <input class="only_number" name="phone" placeholder="Teléfono" type="text">
                                <!---->
                                <input class="js_pre_toform btn" type="submit" value="OBTENER MI PACK">
                                <div class="toform">

                                </div>
                                <!---->
                            </div>
                            <div class="form_annotation">
                                * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <h4>¿CÓMO SE PUEDE SABER</h4>
                        <h5>que se ha contagiado de micosis?</h5>
                        <div class="li_before">
                            <ul>
                                <li>la piel de los pies parece como si se hubiera pintado con tiza </li>
                                <li>pequeñas porciones de piel muerta en las plantas de los pies y entre los dedos</li>
                                <li>callosidad en la placa de la uña y desconchamiento </li>
                                <li>intumescencia en la pared de la uña, sonrojada y con picor</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--MYCO-->
        <section class="myco">
            <div class="main_container">
                <h2>¡ EL TRATAMIENTO DE LA MICOSIS <b>ES NECESARIO!!</b></h2>
                <img src=./assets/mobile/li-icons_03.png"/>
                <p>Defecto <b>cosmético</b></p>
                <img src=./assets/mobile/li-icons_06.png"/>
                <p>
                    The site of entry for other
                    <b> infectious diseases</b>
                </p>
                <img src=./assets/mobile/li-icons_08.png"/>
                <p>
                    Explícitamente, la  <b> micosis </b><br/>
                    es un  <b> problema serio </b>  para los demás
                </p>
                <div class="textin">
<span>
                 diabetes
            </span>
                    <span>
                 alergia dermatológica
            </span>
                    <span>
                 asma
            </span>
                </div>
                <p> Complicación significativa para  <b> otras enfermedades </b></p>
                <a class="button btn2" href="#form">Conseguir mi paquete</a>
            </div>
        </section>
        <section class="effect">
            <div class="main_container">
                <h2>
                    El  <span> efecto </span>  instantáneo <br/>
                    del Fresh Fingers en las enfermedades de hongos
                </h2>
                <ul>
                    <li>
                        Inhibe el crecimiento de los hongos y la micosis. Alivio del picor. Tiene un efecto
                        destructivo en las células en las que ya existe la micosis y bloquea el surgimiento de otras nuevas
                        colonias.
                    </li>
                    <li>
                        Excluye la glándula de sudoración e inhibe la actividad bacteriana, que provoca un desagradable
                        olor a sudor incluso en las personas sanas. Actúa como antiséptico y suaviza la piel,
                        proporcionando un aroma floral durante un periodo prolongado.
                    </li>
                    <li>
                        Suaviza la piel y elimina la descamación.
                    </li>
                    <li>
                        Enfría confortablemente los pies, y proporciona una fragancia agradable.
                    </li>
                </ul>
            </div>
        </section>
        <!--REMOVING-->
        <section class="removing">
            <div class="main_container">
                <h2><b> ELIMINACIÓN INTENSA </b>  DE TODAS LAS CONSECUENCIAS DE LA MICOSIS </h2>
                <div class="row">
                    <p> Desaparición del picor </p>
                    <p>
                        Disminución <br/>
                        del sudor excesivo en los pies
                    </p>
                    <p>
                        Al lograr <br/>
                        una piel suave y segura
                    </p>
                    <p>
                        Eliminación minuciosa <br/>
                        de la infección
                    </p>
                    <p> Se detiene la descamación de la piel </p>
                    <p>
                        Encarnación <br/>
                        del área afectada de la piel
                    </p>
                    <p style="padding-top: 27px;">
                        Destrucción segura <br/>
                        de la infección micótica
                    </p>
                    <p style="padding-top: 45px;">
                        Prevención de la <br/>
                        reinfección micótica
                    </p>
                </div>
                <a class="button" href="#form">Conseguir mi paquete</a>
            </div>
        </section>
        <!--COMMENT-->
        <section class="comment">
            <div class="main_container">
                <h3><b> ESPECIALISTA EN LA PIEL </b>  COMENTARIO </h3>
                <p class="rev">
                    Para un tratamiento contra la micosis satisfactoria, receto a mis pacientes Fresh Fingers, independientemente de las fases de la enfermedad. Fresh Fingers tiene la capacidad exclusiva de penetrar incluso en las capas más rugosas de la piel, disuelve rápidamente las antiguas, la crema puede ser recetada en caso de un progreso agresivo de la enfermedad y además para uso profiláctico ya que no posee contraindicaciones o efectos secundarios.
                </p>
                <h3><span> opinión del </span>  cliente </h3>
                <div class="row">
                    <div class="image">
                        <img src=./assets/mobile/av-1.jpg" style="width: 100%"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span>Valeria, 31 años</span>
                        </p>
                        <p class="user_com">
                            Recientemente fui a la piscina y me contagié de una infección micótica allí. El picor era muy fuerte. Fui al hospital y el especialista me recetó Fresh Fingers, y es buenísimo. El médico me recomendó esa crema y yo estoy totalmente en contra de tomar medicamentos.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="image">
                        <img src=./assets/mobile/av-2.jpg" style="width: 100%"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span> Pedro,  29 años</span>
                        </p>
                        <p class="user_com">
                            Soy una mujer de la limpieza y siempre llevo zapatos especiales para ese trabajo. Mis pies sudan profundamente y eso causa el progreso de enfermedades de hongos. Pedí Fresh Fingers inmediatamente y desinfecté mis zapatos con zumo de limón. Esta crema me ha ayudado, el talón seco ha desaparecido y la piel se ha recuperado por completo.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="image">
                        <img src=./assets/mobile/user_3.png" style="width: 100%"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span>Petr, 29 years old</span>
                        </p>
                        <p class="user_com">
                            Hola, soy nadador. Siempre he tenido riesgo de contagiarme y por supuesto he implementado las medidas necesarias para prevenir la infección micótica. Pero en algunas ocasiones olvidé las chanclas de la ducha y en alguna parte al final me contagié. Mi mujer me pidió Fresh Fingers, lo he usado durante un mes. La descamación de la piel y el picor se han terminado.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer id="form">
            <div class="main_container">
                <div class="row">
                    <form action="./process.php" method="post">

                        <div class="price">
<span>
                          39 €
                        </span>
                            <span>
<s>78 €</s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Díganos dónde enviar  <span> su paquete </span></p>
                            <select id="country_code_selector">
                                <option value="ES">España</option>

                            <input name="name" placeholder="Nombre" type="text">
                            <input class="only_number" name="phone" placeholder="Teléfono" type="text">
                            <!---->
                            <input class="js_pre_toform btn" type="submit" value="OBTENER MI PACK">
                            <div class="toform">

                            </div>
                            <!---->
                        </div>
                        <div class="form_annotation">
                            * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                        </div>
                    </form>
                </div>
            </div>
        </footer>
        <script src=""></script>
    </main>
    </body>
    </html>
<?php } else { ?>




    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 3332 -->
        <script>var locale = "es";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAK-CwRqvNaLAAEAAQACXgsBAAIEDQEKAQ8EqBccMgA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Judit flores';
            var phone_hint = '+34681435813';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("es");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> Fresh Fingers </title>
        <meta charset="utf-8"/>
        <meta content="initial-scale=.75, maximum-scale=1" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:500,400,300,700,900,100&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
        <!--  Удалить  при заливке  -->
        <!--  Удалить  при заливке  -->
        <link href="./assets/style.css" rel="stylesheet"/>
    </head>
    <body>
    <!--HEADER-->
    <style>
        .inputs{
            padding-bottom: 80px;
        }
    </style>
    <main>
        <header>
            <div class="warning">
                <div class="main_container">
                    <p>
                        <b> ADVERTENCIA: </b>  Debido a la extremadamente alta demanda en TV, hay un suministro limitado del Limpiador y Purificador  así como del
                        <script>dtime_nums(-1, true)</script>
                    </p>
                </div>
            </div>
            <div class="main_container">
                <div class="row">
                    <div class="logo">
                        <img src="./assets/logo_03.png"/>
                    </div>
                    <h1><span> agente antimicótico Fresh Fingers </span></h1>
                    <h6>
                        ¡Cuide la salud de sus pies!
                    </h6>
                </div>
                <div class="row">
                    <ul class="header_ul pull_bottom">
                        <li>
                            destrucción de la <br/>
                            <b> infección micótica </b>
                        </li>
                        <li>
                            <b> recuperación de la estructura </b><br/>
                            de la placa de la uña
                        </li>
                        <li>
                            <b> eliminación </b>
                            del<br/>proceso inflamatorio
                        </li>
                    </ul>
                    <form action="./process.php" class="pull_bottom" method="post">

                        <div class="price text_l">
<span>
<b> 39 € </b>
</span>
                            <span>
<i>
                                precio antiguo
                            </i>
<s> 78 € </s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Díganos dónde enviar  <span> su paquete </span></p>
                            <select id="country_code_selector">
                                <option value="ES">España</option>

                            <input name="name" placeholder="Nombre" type="text">
                            <input class="only_number" name="phone" placeholder="Teléfono" type="text">
                            <!---->
                            <input class="js_pre_toform btn" type="submit" value="OBTENER MI PACK">
                            <div class="toform">

                            </div>
                            <!---->
                        </div>
                        <div class="form_annotation">
                            * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                        </div>
                    </form>
                </div>
                <h4> ¿Cómo se puede saber </h4>
                <h5> que se ha contagiado de micosis? </h5>
                <div class="col_2 li_before">
                    <ul>
                        <li> la piel de los pies parece como si se hubiera pintado con tiza </li>
                        <li> pequeñas porciones de piel muerta en las plantas de los pies<br/>y entre los dedos </li>
                    </ul>
                </div>
                <div class="col_2 li_before">
                    <ul>
                        <li> callosidad en la placa de la uña y desconchamiento </li>
                        <li> intumescencia en la pared de la uña, sonrojada y con picor </li>
                    </ul>
                </div>
            </div>
        </header>
        <!--MYCO-->
        <section class="myco">
            <div class="main_container">
                <h2> ¡ EL TRATAMIENTO DE LA MICOSIS  <b> ES NECESARIO! </b></h2>
                <img src="./assets/img-myco_03.png"/>
                <p class="col_4"> Defecto  <b> cosmético </b></p>
                <p class="col_4">
                    La entrada para otras
                    <b>  enfermedades infecciosas </b>
                </p>
                <p class="col_4">
                    Explícitamente, la  <b> micosis </b><br/>
                    es un  <b> problema serio </b>  para los demás
                </p>
                <p class="col_4">  Complicación significativa para  <b> otras enfermedades </b></p>
                <div class="textin">
<span>
                 diabetes
            </span>
                    <span>
                 alergia dermatológica
            </span>
                    <span>
                 asma
            </span>
                </div>
                <a class="button btn" href="#form"> Conseguir mi paquete </a>
            </div>
        </section>
        <section class="effect">
            <div class="main_container">
                <h2>
                    El  <span> efecto </span>  instantáneo <br/>
                    del Fresh Fingers en las enfermedades de hongos
                </h2>
                <ul>
                    <li>
                        inhibe el crecimiento de los hongos y la micosis. Alivio del picor. Tiene un efecto <br/>
                        destructivo en las células en las que ya existe la micosis y bloquea el surgimiento de otras nuevas <br/>
                        colonias.
                        <span>
 Climbazole
                    </span>
                    </li>
                    <li>
                        excluye la glándula de sudoración e inhibe la actividad bacteriana, que provoca un desagradable <br/>
                        olor a sudor incluso en las personas sanas. Actúa como antiséptico y suaviza la piel, <br/>
                        proporcionando un aroma floral durante un periodo prolongado.
                        <span>
 Farnesol
                    </span>
                    </li>
                    <li>
                        suaviza la piel y elimina la descamación.
                        <span>
 Vitamina E
                    </span>
                    </li>
                    <li>
                        enfría confortablemente los pies, y proporciona una fragancia agradable.
                        <span>
 Aceites esenciales
                    </span>
                    </li>
                </ul>
            </div>
        </section>
        <!--REMOVING-->
        <section class="removing">
            <div class="main_container">
                <h2><b> ELIMINACIÓN INTENSA </b>  DE TODAS LAS CONSECUENCIAS DE LA MICOSIS </h2>
                <div class="col_2 left">
                    <p> Desaparición del picor </p>
                    <p>
                        Disminución <br/>
                        del sudor excesivo en los pies
                    </p>
                    <p>
                        Al lograr <br/>
                        una piel suave y segura
                    </p>
                    <p>
                        Eliminación minuciosa <br/>
                        de la infección
                    </p>
                </div>
                <div class="col_2 right">
                    <p> Se detiene la descamación de la piel </p>
                    <p>
                        Encarnación <br/>
                        del área afectada de la piel
                    </p>
                    <p>
                        Destrucción segura <br/>
                        de la infección micótica
                    </p>
                    <p>
                        Prevención de la <br/>
                        reinfección micótica
                    </p>
                </div>
                <a class="button btn" href="#form"> Conseguir mi paquete </a>
            </div>
        </section>
        <!--COMMENT-->
        <section class="comment">
            <div class="main_container">
                <h3><b> ESPECIALISTA EN LA PIEL </b>  COMENTARIO </h3>
                <p class="rev">
                    Para un tratamiento contra la micosis satisfactoria, receto a mis pacientes Fresh Fingers, independientemente de las fases de la enfermedad. Fresh Fingers tiene la capacidad exclusiva de penetrar incluso en las capas más rugosas de la piel, disuelve rápidamente las antiguas, la crema puede ser recetada en caso de un progreso agresivo de la enfermedad y además para uso profiláctico ya que no posee contraindicaciones o efectos secundarios.
                </p>
                <h3><span> opinión del </span>  cliente </h3>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/av-1.jpg"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Valeria, </span>  33 años
                        </p>
                        <p class="user_com">
                            Recientemente fui a la piscina y me contagié de una infección micótica allí. El picor era muy fuerte. Fui al hospital y el especialista me recetó Fresh Fingers, y es buenísimo. El médico me recomendó esa crema y yo estoy totalmente en contra de tomar medicamentos.
                        </p>
                    </div>
                </div>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/av-2.jpg"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Clara </span>  30 años
                        </p>
                        <p class="user_com">
                            Soy una mujer de la limpieza y siempre llevo zapatos especiales para ese trabajo. Mis pies sudan profundamente y eso causa el progreso de enfermedades de hongos. Pedí Fresh Fingers inmediatamente y desinfecté mis zapatos con zumo de limón. Esta crema me ha ayudado, el talón seco ha desaparecido y la piel se ha recuperado por completo.
                        </p>
                    </div>
                </div>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/user_3.png"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Pedro, </span>  29 años
                        </p>
                        <p class="user_com">
                            Hola, soy nadador. Siempre he tenido riesgo de contagiarme y por supuesto he implementado las medidas necesarias para prevenir la infección micótica. Pero en algunas ocasiones olvidé las chanclas de la ducha y en alguna parte al final me contagié. Mi mujer me pidió Fresh Fingers, lo he usado durante un mes. La descamación de la piel y el picor se han terminado.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer id="form">
            <div class="main_container">
                <div class="row">
                    <h1><span> Agente antimicótico </span> </h1>
                    <h1><span> Fresh Fingers </span></h1>
                    <h6>
                        ¡Cuide la salud de sus pies!
                    </h6>
                    <form action="./process.php" class="pull_bottom" method="post">

                        <div class="price">
<span>
                            39 €
                        </span>
                            <span>
<i>
                                precio antiguo
                            </i>
<s> 78 € </s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Díganos dónde enviar  <span> su paquete </span></p>
                            <select id="country_code_selector">
                                <option value="ES">España</option>

                            <input name="name" placeholder="Nombre" type="text">
                            <input class="only_number" name="phone" placeholder="Teléfono" type="text">
                            <!---->
                            <input class="js_pre_toform btn" type="submit" value="OBTENER MI PACK">
                            <div class="toform">

                            </div>
                            <!---->
                        </div>
                        <div class="form_annotation">
                            * Sobre las ofertas, condiciones y procedimientos para la entrega le informan nuestro operador
                        </div>
                    </form>
                </div>
            </div>
            <div class="warning">
                <div class="main_container">
                    <p>
                        <b> ADVERTENCIA: </b>  Debido a la extremadamente alta demanda en TV, hay un suministro limitado del Limpiador y Purificador  así como del
                        <script>dtime_nums(-1, true)</script>
                    </p>
                </div>
            </div>
        </footer>
    </main>

    </body>
    </html>
<?php } ?>