<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()){ ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 6314 -->
        <script>var locale = "sk";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALDCwThL9eLAAEAAQACYwsBAAKqGAEKAQ8EWo2eFAA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Martin Mičiak';
            var phone_hint = '0908267142';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("sk");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Fresh fingers</title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="./assets/mobile/style.css" rel="stylesheet"/>
    </head>
    <body>
    <div class="main">
        <header>
            <div class="container_gradient">
                <div class="warning">
                    <div class="main_container">
                        <p>
                            <b> VAROVANIE: </b>  Vzhľadom k extrémne vysokému dopytu TV  máme na sklade iba obmedzený prísun Fresh fingers ku dňu
                            <script>dtime_nums(-1, true)</script>
                        </p>
                    </div>
                </div>
                <div class="main_container">
                    <div class="row">
                        <div class="logo">
                            <img src="./assets/mobile/logo_03.png"/>
                        </div>
                        <h1><span> Antimykotický prípravok Fresh fingers </span></h1>
                        <h6>
                            Starajte sa o zdravie vašich nôh
                        </h6>
                    </div>
                    <div class="row">
                        <ul class="header_ul">
                            <li>
                                plesňové infekcie <br/>
                                <b> zničenie </b>
                            </li>
                            <li>
                                <b> zotavenie štruktúry </b><br/>
                                nechtovej platničky
                            </li>
                            <li>
                                <b> odstránenie </b><br/>
                                zápalového procesu
                            </li>
                        </ul>
                        <form action="./process.php" method="post">

                            <div class="price text_l">
<span>
                           33 €
                           </span>
                                <span>
<s>66 €</s>
</span>
                            </div>
                            <div class="inputs">
                                <p> Povedzte nám, kam poslať  <span> váš balíček </span></p>

                                <select id="country_code_selector">
                                    <option value="SK">Slovenská republika</option>

                                    <input name="name" placeholder="Meno" type="text">
                                    <input class="only_number" name="phone" placeholder="Telefón" type="text">
                                    <!---->
                                    <input class="js_pre_toform" type="submit" value="Objednajte si balíček">
                                    <div class="toform">
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <h4> Ako poznáte, </h4>
                        <h5> že máte plesňové ochorenie? </h5>
                        <div class="li_before">
                            <ul>
                                <li> koža na nohách vyzerá ako natretá kriedou </li>
                                <li> začína odlupovanie kože medzi prstami </li>
                                <li> mozolovitost a drobenie nechtovej platničky </li>
                                <li> opuch okolia nechtov, začervenanie a svrbenie </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="myco">
            <div class="main_container">
                <h2> LIEČBA PLESŇOVÝCH OCHORENÍ  <b> JE NEZBYTNÁ! </b></h2>
                <img src="./assets/mobile/li-icons_03.png"/>
                <p>Kozmetická  <b> vada </b></p>
                <img src="./assets/mobile/li-icons_06.png"/>
                <p>
                    Miesto vstupu pre ostatné
                    <b>  infekčné choroby </b>
                </p>
                <img src="./assets/mobile/li-icons_08.png"/>
                <p>
                    Vaša plieseň  <b> je </b><br/>
                    nepriamo  <b> vážný problém </b>  aj pre ostatných
                </p>
                <div class="textin">
<span>
                  diabetes
                  </span>
                    <span>
                  alergická dermatitída
                  </span>
                    <span>
                  astma
                  </span>
                </div>
                <a class="button" href="#form"> Dostat balíček</a>
            </div>
        </section>
        <section class="effect">
            <div class="main_container">
                <h2>
                    Okamžitý  <span> bezodkladný </span>  účinok <br/>
                    Fresh fingersu na pliesňové ochorenie
                </h2>
                <ul>
                    <li>
                        obmedzuje rast kvasiniek a plesne. Oslobozuje od svrbenia. Má ničivý účinok na bunky už existujúcich plesní a blokuje vznik nových kolónií.
                    </li>
                    <li>
                        obmedzuje funkciu potnej žľazy a inhibuje aktivitu baktérií, ktoré tvoria prepotený zápach nepríjemný aj u zdravej osoby. Čistí a zjemňuje pokožku, má ľahkú kvetinovú vôňu po dlhú dobu.
                    </li>
                    <li>
                        zjemňuje pokožku, eliminuje odlupovanie.
                    </li>
                    <li>
                        pohodlne ochladzuje nohu, poskytuje príjemnú vôňu.
                    </li>
                </ul>
            </div>
        </section>
        <section class="removing">
            <div class="main_container">
                <h2><b> INTENZÍVNE ODSTRANENIE </b>  VŠETKÝCH DÔSLEDKOV PLESNE </h2>
                <div class="row">
                    <p> Odstránenie svrbenia </p>
                    <p>
                        Odstránenie <br/>
                        nedmerného potenia nôh
                    </p>
                    <p>
                        Zjemnenie <br/>
                        pleti
                    </p>
                    <p>
                        Odstránenie <br/>
                        infekcie
                    </p>
                    <p> Konie odlupovania </p>
                    <p>
                        Zhojenie <br/>
                        postihnutej oblasti kože
                    </p>
                    <p>
                        Bezpečné odstránenie <br/>
                        plesňovej infekcie
                    </p>
                    <p>
                        Prevencia <br/>
                        opätovnej nákazy
                    </p>
                </div>
                <a class="button" href="#form">Objednajte si balíček </a>
            </div>
        </section>
        <!--COMMENT-->
        <section class="comment">
            <div class="main_container">
                <h3><b> SPECIALISTA NA KOŽU </b>  KOMENTÁR </h3>
                <p class="rev">
                    Pre úspešnú liečbu plesňových ochorení odporúčam svojim pacientom Fresh fingers, nezávisle na vývojových štádiách plesňového ochorení. Fresh fingers má jedinečnú schopnosť preniknúť aj do hrubej vrstvy kože, rýchlo ju zmäkčuje, krém môže liečiť aj v prípade agresívneho priebehu ochorenia a tiež pre profylaktické použitie, nemá žiadne kontraindikácie a vedľajšie účinky.
                </p>
                <h3><span> hodnotenie </span>  zákazníkov </h3>
                <div class="row">
                    <div class="image">
                        <img src="./assets/mobile/av-1.jpg" style="width: 100%"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span> Valéria, </span>  24 rokov
                        </p>
                        <p class="user_com">
                            Nedávno som išla do bazéna a tam som chytila plesňovú infekciu. Svrbenie bolo veľmi silné. Išla som do nemocnice, a kožný špecialista mi prdpísal Fresh fingers, a je to tak dobré, že mi lekár odporučil krém, nechcela by som brať lieky.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="image">
                        <img src="./assets/mobile/av-2.jpg" style="width: 100%"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span> Katarína </span>  28 rokov
                        </p>
                        <p class="user_com">
                            Nosím v práci topánky každý deň. Moje nohy sa veľmi potí a som tak unavená, a to všetko spôsobuje plesňové ochorenia. Okamžite som objednala Fresh fingers a vyčistila topánky citrónovou šťavou. Tento krém mi pomohol, suché päty zmizli a pokožka bola kompletne obnovená.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="image">
                        <img src="./assets/mobile/user_3.png" style="width: 100%"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span> Peter </span>  29 rokov
                        </p>
                        <p class="user_com">
                            Dobrý deň, som plavec. Vždy som bol v ohrození, a samozrejme aj robil potrebné opatrenia, aby som zabránil vzniku plesňovej infekcie. Ale niekoľkokrát som si zabudol topánky do sprchy a niekde chytil pleseň. Moja žena objednala Fresh fingers, používal som ho cez mesiac. Odlupovanie kože a svrbenie prestalo.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer id="form">
            <div class="main_container">
                <div class="row">
                    <form action="" method="post">

                        <div class="price">
<span>
                        33 €
                        </span>
                            <span>
<s>66 €</s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Povedzte nám, kam máme poslať  <span> váš balíček </span></p>


                            <select id="country_code_selector">
                                <option value="SK">Slovenská republika</option>

                                <input name="name" placeholder="Meno" type="text">
                                <input class="only_number" name="phone" placeholder="Telefón" type="text">
                                <!---->
                                <input class="js_pre_toform" type="submit" value="Objednajte si balíček">
                                <div class="toform">

                        </div>
                    </form>
                </div>
            </div>
        </footer>
        <script src="./assets/animations.js"></script>
    </div>
    </body>
    </html>
<?php } else { ?>





    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 6314 -->
        <script>var locale = "sk";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALDCwThL9eLAAEAAQACYwsBAAKqGAEKAQ8EWo2eFAA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Martin Mičiak';
            var phone_hint = '0908267142';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("sk");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> Fresh fingers </title>
        <meta charset="utf-8"/>
        <meta content="initial-scale=.8, maximum-scale=1, user-scalable=no" name="viewport"/>
        <link href="./assets/style.css" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:500,400,300,700,900,100&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
        <script src="./assets/animations.js"></script>
    </head>
    <body>
    <style>
        .inputs{
            padding-bottom: 80px;
        }
    </style>
    <main>

        <header>
            <div class="warning">
                <div class="main_container">
                    <p>
                        <b> VAROVANIE: </b>  Vzhľadom k extrémne vysokému dopytu TV  máme na sklade iba obmedzený prísun Fresh fingers ku dňu
                        <script>dtime_nums(-1, true)</script>
                    </p>
                </div>
            </div>
            <div class="main_container">
                <div class="row">
                    <div class="logo">
                        <img src="./assets/logo_03.png"/>
                    </div>
                    <h1><span> Antimykotický prípravok Fresh fingers </span></h1>
                    <h6>
                        Starajte sa o zdravie vašich nôh
                    </h6>
                </div>
                <div class="row">
                    <ul class="header_ul pull_bottom">
                        <li>
                            plesňové infekcie <br/>
                            <b> zničenie </b>
                        </li>
                        <li>
                            <b> zotavenie štruktúry </b><br/>
                            nechtovej platničky
                        </li>
                        <li>
                            <b> odstránenie </b><br/>
                            zápalového procesu
                        </li>
                    </ul>
                    <form action="" class="pull_bottom" method="post">
                        <div class="price text_l">
<span>
<b>33 €</b>
</span>
                            <span>
<i>
                        stará cena
                        </i>
<s>66 €</s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Povedzte nám, kam poslať  <span> váš balíček </span></p>

                                <select id="country_code_selector">
                                    <option value="SK">Slovenská republika</option>

                                    <input name="name" placeholder="Meno" type="text">
                                    <input class="only_number" name="phone" placeholder="Telefón" type="text">
                                    <!---->
                                    <input class="js_pre_toform" type="submit" value="Objednajte si balíček">
                                    <div class="toform">

                                    </div>
                                    <!---->

                        </div>
                    </form>
                </div>
                <h4> Ako poznáte, </h4>
                <h5> že máte plesňové ochorenie? </h5>
                <div class="col_2 li_before">
                    <ul>
                        <li> koža na nohách vyzerá ako natretá kriedou </li>
                        <li> začína odlupovanie kože medzi prstami </li>
                    </ul>
                </div>
                <div class="col_2 li_before">
                    <ul>
                        <li> mozolovitost a drobenie nechtovej platničky </li>
                        <li> opuch okolia nechtov, začervenanie a svrbenie </li>
                    </ul>
                </div>
            </div>
        </header>
        <section class="myco">
            <div class="main_container">
                <h2> LIEČBA PLESŇOVÝCH OCHORENÍ  <b> JE NEZBYTNÁ! </b></h2>
                <img src="./assets/img-myco_03.png"/>
                <p class="col_4"> Kozmetická  <b> vada </b></p>
                <p class="col_4">
                    Miesto vstupu pre ostatné
                    <b>  infekčné choroby </b>
                </p>
                <p class="col_4">
                    Vaša plieseň  <b> je </b><br/>
                    nepriamo  <b> vážný problém </b>  aj pre ostatných
                </p>
                <p class="col_4">  Závažná komplikácia  <b> ostatných chorôb </b></p>
                <div class="textin">
<span>
                  diabetes
                  </span>
                    <span>
                  alergická dermatitída
                  </span>
                    <span>
                  astma
                  </span>
                </div>
                <a class="button" href="#form"> Dostat balíček </a>
            </div>
        </section>
        <section class="effect">
            <div class="main_container">
                <h2>
                    Okamžitý  <span> bezodkladný </span>  účinok <br/>
                    Fresh fingersu na pliesňové ochorenie
                </h2>
                <ul>
                    <li>
                        obmedzuje rast kvasiniek a plesne. Oslobozuje od svrbenia. Má ničivý účinok na bunky už<br/> existujúcich plesní a blokuje vznik nových kolónií.
                        <span>
                     Climbazola
                     </span>
                    </li>
                    <li>
                        obmedzuje funkciu potnej žľazy a inhibuje aktivitu baktérií, ktoré tvoria prepotený zápach<br/> nepríjemný aj u zdravej osoby. Čistí a zjemňuje pokožku, má ľahkú kvetinovú vôňu po dlhú dobu.
                        <span>
                     Farnesol
                     </span>
                    </li>
                    <li>
                        zjemňuje pokožku, eliminuje odlupovanie.
                        <span>
                     Vitamín E
                     </span>
                    </li>
                    <li>
                        pohodlne ochladzuje nohu, poskytuje príjemnú vôňu.
                        <span>
                     Éterický olej
                     </span>
                    </li>
                </ul>
            </div>
        </section>
        <section class="removing">
            <div class="main_container">
                <h2><b> INTENZÍVNE ODSTRANENIE </b>  VŠETKÝCH DÔSLEDKOV PLESNE </h2>
                <div class="col_2 left">
                    <p> Odstránenie svrbenia </p>
                    <p>
                        Odstránenie <br/>
                        nedmerného potenia nôh
                    </p>
                    <p>
                        Zjemnenie <br/>
                        pleti
                    </p>
                    <p>
                        Odstránenie <br/>
                        infekcie
                    </p>
                </div>
                <div class="col_2 right">
                    <p> Konie odlupovania </p>
                    <p>
                        Zhojenie <br/>
                        postihnutej oblasti kože
                    </p>
                    <p>
                        Bezpečné odstránenie <br/>
                        plesňovej infekcie
                    </p>
                    <p>
                        Prevencia <br/>
                        opätovnej nákazy
                    </p>
                </div>
                <a class="button" href="#form"> Objednajte si balíček </a>
            </div>
        </section>
        <section class="comment">
            <div class="main_container">
                <h3><b> SPECIALISTA NA KOŽU </b>  KOMENTÁR </h3>
                <p class="rev">
                    Pre úspešnú liečbu plesňových ochorení odporúčam svojim pacientom Fresh fingers, nezávisle na vývojových štádiách plesňového ochorení. Fresh fingers má jedinečnú schopnosť preniknúť aj do hrubej vrstvy kože, rýchlo ju zmäkčuje, krém môže liečiť aj v prípade agresívneho priebehu ochorenia a tiež pre profylaktické použitie, nemá žiadne kontraindikácie a vedľajšie účinky.
                </p>
                <h3><span> hodnotenie </span>  zákazníkov </h3>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/1.jpg"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Valéria, </span>  24 rokov
                        </p>
                        <p class="user_com">
                            Nedávno som išla do bazéna a tam som chytila plesňovú infekciu. Svrbenie bolo veľmi silné. Išla som do nemocnice, a kožný špecialista mi prdpísal Fresh fingers, a je to tak dobré, že mi lekár odporučil krém, nechcela by som brať lieky.
                        </p>
                    </div>
                </div>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/2.jpg"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Katarína </span>  28 rokov
                        </p>
                        <p class="user_com">
                            Nosím v práci topánky každý deň. Moje nohy sa veľmi potí a som tak unavená, a to všetko spôsobuje plesňové ochorenia. Okamžite som objednala Fresh fingers a vyčistila topánky citrónovou šťavou. Tento krém mi pomohol, suché päty zmizli a pokožka bola kompletne obnovená.
                        </p>
                    </div>
                </div>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/user_3.png"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Peter </span>  29 rokov
                        </p>
                        <p class="user_com">
                            Dobrý deň, som plavec. Vždy som bol v ohrození, a samozrejme aj robil potrebné opatrenia, aby som zabránil vzniku plesňovej infekcie. Ale niekoľkokrát som si zabudol topánky do sprchy a niekde chytil pleseň. Moja žena objednala Fresh fingers, používal som ho cez mesiac. Odlupovanie kože a svrbenie prestalo.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer id="form">
            <div class="main_container">
                <div class="row">
                    <h1><span> Antimykotický </span> </h1>
                    <h1><span> prípravok Fresh fingers </span></h1>
                    <h6>
                        Starajte sa o zdravie vašich nôh!
                    </h6>
                    <form action="./process.php" class="pull_bottom" method="post">
                        <div class="price">
<span>
                        33 €
                        </span>
                            <span>
<i>
                        stará cena
                        </i>
<s>66 €</s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Povedzte nám, kam máme poslať  <span> váš balíček </span></p>


                                <select id="country_code_selector">
                                    <option value="SK">Slovenská republika</option>

                                <input name="name" placeholder="Meno" type="text">
                                <input class="only_number" name="phone" placeholder="Telefón" type="text">
                                <!---->
                                <input class="js_pre_toform" type="submit" value="Objednajte si balíček">
                                <div class="toform">


                                <!---->
                            </div>



                        </div>
                    </form>
                </div>
            </div>
            <div class="warning">
                <div class="main_container">
                    <p>
                        <b> VAROVANIE: </b>  Vzhľadom k extrémne vysokému dopytu TV  máme na sklade iba obmedzený prísun Fresh fingers ku dňu
                        <script>dtime_nums(-1, true)</script>
                    </p>
                </div>
            </div>
        </footer>
    </main>
    </body>
    </html>
<?php } ?>