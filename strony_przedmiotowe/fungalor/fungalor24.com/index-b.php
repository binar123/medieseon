<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()){ ?>

<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 3362 -->
    <script>var locale = "ro";</script>        <!-- country code -->
    <script>var lang_locale = "pl";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAIECgS7eLx_AAABAAKGCQEAAiINAQoBDwQP-vO6AA";</script>
    <script type="text/javascript" src="./assets/jquery.min.js"></script>
    <script type="text/javascript" src="./assets/placeholders.min.js"></script>
    <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="./assets/dr.js"></script>
    <script type="text/javascript" src="./assets/dtime.js"></script>
    <script type="text/javascript" src="./assets/js.cookie.js"></script>
    <script type="text/javascript" src="./assets/validation.js"></script>
    <script type="text/javascript" src="./assets/order_me.js"></script>
    <style>
    .ac_footer {
        position: relative;
        top: 10px;
        height:0;
        text-align: center;
        margin-bottom: 70px;
        color: #A12000;
    }
    .ac_footer a {
        color: #A12000;
    }
    img[height="1"], img[width="1"] {
        display: none !important;
    }
    </style>

    <script type="text/javascript" src="./assets/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 0;
        var name_hint = 'Toma Remus';
        var phone_hint = '+40740525322';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
            '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
            ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("ro");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1666009176948198');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->



<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Fresh Fingers</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://fonts.googleapis.com/css?family=Roboto:500,400,300,700,900,100&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<link href="./assets/mobile/style.css" rel="stylesheet"/>
</head>
<body>
<!--HEADER-->
<main>
<header>
<div class="container_gradient">
<div class="warning">
<div class="main_container">
<p>
<b>ATENȚIONARE:</b>
                        Ca urmare a cererii extrem de mare, există un stoc limitat de Fresh Fingers disponibil începând de

                        <script>dtime_nums(-1, true)</script>
</p>
</div>
</div>
<div class="main_container">
<div class="row">
<div class="logo">
<img src="./assets/mobile/logo_03.png"/>
</div>
<h1>AGENT ANTIMICOTIC Fresh Fingers</h1>
<h6>
                        Ai grijă de sănătatea picioarelor tale!

                    </h6>
</div>
<div class="row">
<ul class="header_ul">
<li>
                            infecția micotică<br/>
<b>este distrusă</b>
</li>
<li>
<b>recuperarea structurii</b><br/>
                            plăcii unghiei
                        </li>
<li>
<b>eliminarea </b> <br/>
                            inflamațiilor
                        </li>
</ul>
<form action="./process.php" method="post"><input type="hidden" name="esub" value="">


<div class="price text_l">
<span>
                                159 RON
                            </span>
<span>
<s>318 RON</s>
</span>
</div>
<div class="inputs">
    <div class="inputs">
        <p> Spune-ne unde să trimitem  <span> COMANDA TA </span></p>
        <select id="country_code_selector">
            <option value="Ro"> România </option>

        <input name="name" placeholder="Nume" type="text">
        <input class="only_number" name="phone" placeholder="Telefon" type="text">
        <!---->
        <input class="js_pre_toform" type="submit" value="Comanda">
        <div class="toform">

        </div>
        <!---->
    </div>

</form>
</div>
<div class="row">
<h4>CUM POȚI SIMȚI</h4>
<h5>„COLONIZAREA” MICOTICĂ?</h5>
<div class="li_before">
<ul>
<li>pielea picioarelor arată ca și cum ar fi dată cu cretă
                            </li>
<li>închiderea la culoare a pielii tălpii și între degete
                            </li>
<li>înăsprirea unghiei și exfolierea sa
                            </li>
<li>inflamarea pereților unghiei, înroșirea lor și mâncărime
                            </li>
</ul>
</div>
</div>
</div>
</div>
</header>
<!--MYCO-->
<section class="myco">
<div class="main_container">
<h2>TRATAMENTUL MICOZEI
                <b>ESTE NECESAR!</b></h2>
<img src="./assets/mobile/li-icons_03.png"/>
<p>Defect  <b>COSMETIC</b></p>
<img src="./assets/mobile/li-icons_06.png"/>
<p>
                Loc de intrare pentru alte
                <b> BOLI INFECȚIOASE</b>
</p>
<img src="./assets/mobile/li-icons_08.png"/>
<p>
                Implicit, <b>MICOZA</b><br/>
                este o <b>PROBLEMĂ GRAVĂ</b> pentru alții
            </p>
<div class="textin">
<span>
                DIABET
            </span>
<span>
               DERMATITĂ ALERGICĂ

            </span>
<span>
                ASTM
            </span>
</div>
<p> Complicații semnificative pentru <b>ALTE BOLI</b></p>
<a class="button" href="#form">Primește pachetul tău</a>
</div>
</section>
<section class="effect">
<div class="main_container">
<h2>
                EFECTUL <span>INSTANTANEU</span> AL Fresh Fingers
                ASUPRA BOLILOR FUNGICE
            </h2>
<ul>
<li>
<span><small>Extract de floare</small> de Chamomilla recutita</span>
                    inhibă dezvoltarea ciupercii și a micozei. Te scapă de mâncărime. Are un efect
                    distructiv asupra celulelor micotice deja existente și blochează apariția unor
                    colonii noi.
                </li>
<li>
<span><small>Extract de frunză</small> de Salvia officinalis</span>
                    elimină transpirația glandelor și inhibă activitatea bacteriilor care produc mirosul
                    neplăcut de transpirație chiar și la persoanele sănătoase. Aseptizează și înmoaie pielea,
                    oferindu-i o aromă ușoară florală pentru mult timp.
                </li>
<li>
<span><small>Pudră din sucul frunzei</small> de Aloe barbabensis</span>
                    înmoaie pielea, elimină exfolierea.
                </li>
<li>
<span><small>Extract din uleiul fructului</small> de Olea europea</span>
                    răcorește confortabil piciorul, oferind un parfum plăcut.
                </li>
</ul>
</div>
</section>
<!--REMOVING-->
<section class="removing">
<div class="main_container">
<h2>ELIMINAREA EFICIENTĂ A TUTUROR CONSECINȚELOR MICOZEI</h2>
<div class="row">
<p>Exfolierea pielii încetează</p>
<p>Dispariția mâncărimii</p>
<p>
                    Încarnarea<br/>
                    zonei afectate a pielii
                </p>
<p>
                    Scăderea cantității<br/>
                    de transpirație a piciorului
                </p>
<p>
                    Autodistrugerea<br/>
                    infecției micotice
                </p>
<p>
                    Face pielea<br/>
                    moale și fină
                </p>
<p>
                    Prevenția<br/>
                    infecției micotice
                </p>
<p>
                    Eliminarea eficientă<br/>
                    a zonelor cu probleme
                </p>
</div>
<a class="button" href="#form">Primește pachetul tău</a>
</div>
</section>
<!--COMMENT-->
<section class="comment">
<div class="main_container">
<h3>SPECIALIST ÎN DERMATOLOGIE COMENTARIU</h3>
<p class="rev">
                Pentru tratamentul reușit al micozei, le prescriu pacienților mei Fresh Fingers, indiferent de stadiul de dezvoltare a bolii fungice. Fresh Fingers are capacitatea unică de a penetra chiar și în straturile dure ale pielii, le dizolvă rapid pe cele vechi, gel poate fi prescrisă chiar și în cazul progresului agresiv al bolii și pentru utilizare profilactică, nu are contraindicații și efecte secundare.
            </p>
<h3><span>customer</span> review</h3>
<div class="row">
<div class="image">
<img src="./assets/mobile/1.jpg" width="70"/>
</div>
<div class="com">
<p class="user_name">
<span>Valeria,</span> 24 de ani
                    </p>
<p class="user_com">
                        Recent, am mers la bazin și am luat infecție micotică de acolo. Mâncărimea era foarte puternică. Am mers la spital, iar specialistul în dermatologie mi-a prescris Fresh Fingers. E atât de bine că medicul mi-a recomandat gel, sunt total împotriva luării de medicamente.
                    </p>
</div>
</div>
<div class="row">
<div class="image">
<img src="./assets/mobile/2.jpg" width="70"/>
</div>
<div class="com">
<p class="user_name">
<span>Camelia,</span> 28 de ani
                    </p>
<p class="user_com">
                        Sunt femeie de serviciu și port papuci de serviciu zilnic. Picioarele mele transpiră puternic și sunt atât de obosită, ceea ce a provocat progresul bolii fungice. Am comandat Fresh Fingers imediat și mi-am aseptizat papucii cu suc de lămâie. Gel m-a ajutat, călcâiele uscate au dispărut, iar pielea este complet recuperată.
                    </p>
</div>
</div>
<div class="row">
<div class="image">
<img src="./assets/mobile/icons_36.png"/>
</div>
<div class="com">
<p class="user_name">
<span>Petre,</span> 29 de ani
                    </p>
<p class="user_com">
                        Salut, sunt înotător. Întotdeauna am fost supus riscului și, desigur, am luat toate măsurile necesare pentru a preveni infecția micotică. Dar de câteva ori am uitat papucii de duș și am luat cumva infecție fungică. Soția mea a comandat Fresh Fingers, l-am folosit mai bine de o lună. Exfolierea pielii și mâncărimea au dispărut.
                    </p>
</div>
</div>
</div>
</section>
<footer id="form">
<div class="main_container">
<div class="row">
<form action="./process.php" method="post"><input type="hidden" name="esub" value="">


<div class="price">
<span>
                          159 RON
                        </span>
<span>
<s>318 RON</s>
</span>
</div>
    <div class="inputs">
        <p> Spune-ne unde să trimitem  <span> COMANDA TA </span></p>
        <select id="country_code_selector">
            <option value="Ro"> România </option>

        <input name="name" placeholder="Nume" type="text">
        <input class="only_number" name="phone" placeholder="Telefon" type="text">
        <!---->
        <input class="js_pre_toform" type="submit" value="Comanda">
        <div class="toform">

        </div>
        <!---->
    </div>
</form>
</div>
</div>
</footer>
<script src="./assets/animations.js"></script>
</main>
</body>
</html>
<?php } else { ?>
<!DOCTYPE html>
<html>
<head>
    <!-- [pre]land_id = 3362 -->
    <script>var locale = "ro";</script>        <!-- country code -->
    <script>var lang_locale = "ro";</script>   <!-- browser locale -->
    <script>var esub = "-4A25sMQIJIAIECgTEKAR9AAABAAKGCQEAAiINAQoBDwQ4_CZjAA";</script>
    <script type="text/javascript" src="./assets/jquery.min.js"></script>
    <script type="text/javascript" src="./assets/placeholders.min.js"></script>
    <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="./assets/dr.js"></script>
    <script type="text/javascript" src="./assets/dtime.js"></script>
    <script type="text/javascript" src="./assets/js.cookie.js"></script>
    <script type="text/javascript" src="./assets/validation.js"></script>
    <script type="text/javascript" src="./assets/order_me.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height:0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }
        .ac_footer a {
            color: #A12000;
        }
        img[height="1"], img[width="1"] {
            display: none !important;
        }
    </style>

    <script type="text/javascript" src="./assets/sender.js"></script>

    <script>

        var package_prices = {};
        var shipment_price = 0;
        var name_hint = 'Toma Remus';
        var phone_hint = '+40740525322';

        $(document).ready(function() {

            $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

            moment.locale("ro");
            $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
            $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

        });
    </script>
    <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1666009176948198');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
    <!-- End Facebook Pixel Code -->



    <title> Fresh Fingers </title>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <link href="http://fonts.googleapis.com/css?family=Roboto:500,400,300,700,900,100&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
    <link href="./assets/style.css" rel="stylesheet"/>
</head>
<body>
<!--HEADER-->
<main>
    <header>
        <div class="warning">
            <div class="main_container">
                <p>
                    <b> ATENȚIONARE: </b>  Ca urmare a cererii extrem de mare, există un stoc limitat de Fresh Fingers disponibil începând de
                    <script>dtime_nums(-1, true)</script>
                </p>
            </div>
        </div>
        <div class="main_container">
            <div class="row">
                <div class="logo">
                        <img src="./assets/logo_03.png"/>
                </div>
                <h1><span> Agent antimicotic Fresh Fingers </span></h1>
                <h6>
                    Ai grijă de sănătatea picioarelor tale!
                </h6>
            </div>
            <div class="row">
                <ul class="header_ul pull_bottom">
                    <li>
                        infecția micotică <br/>
                        <b> este distrusă </b>
                    </li>
                    <li>
                        <b> recuperarea structurii </b><br/>
                        plăcii unghiei
                    </li>
                    <li>
                        <b> eliminarea </b> <br/>
                        inflamațiilor
                    </li>
                </ul>
                <form action="./process.php" class="pull_bottom" method="post">


                    <div class="price text_l">
    <span>
    <b> 159 RON </b>
    </span>
    <span>
    <i>
        preț vechi
    </i>
    <s> 318 RON </s>
    </span>
                    </div>
                    <div class="inputs">
                        <p> Spune-ne unde să trimitem  <span> COMANDA TA </span></p>
                        <select id="country_code_selector">
                            <option value="Ro"> România </option>

                        <input name="name" placeholder="Nume" type="text">
                        <input class="only_number" name="phone" placeholder="Telefon" type="text">
                        <!---->
                        <input class="js_pre_toform" type="submit" value="Comanda">
                        <div class="toform">

                        </div>
                        <!---->
                    </div>
                </form>
            </div>
            <h4> Cum poți simți </h4>
            <h5> „colonizarea” micotică? </h5>
            <div class="col_2 li_before">
                <ul>
                    <li> pielea picioarelor arată ca și cum ar fi dată cu cretă </li>
                    <li> închiderea la culoare a pielii tălpii și între degete </li>
                </ul>
            </div>
            <div class="col_2 li_before">
                <ul>
                    <li> înăsprirea unghiei și exfolierea sa </li>
                    <li> inflamarea pereților unghiei, înroșirea lor și mâncărime </li>
                </ul>
            </div>
        </div>
    </header>
    <!--MYCO-->
    <section class="myco">
        <div class="main_container">
            <h2> TRATAMENTUL MICOZEI  <b> ESTE NECESAR! </b></h2>
            <img src="./assets/img-myco_03.png"/>
            <p class="col_4"> Defect  <b> cosmetic </b></p>
            <p class="col_4">
                Loc de intrare pentru alte
                <b>  boli infecțioase </b>
            </p>
            <p class="col_4">
                Implicit,  <b> micoza </b><br/>
                este o  <b> problemă gravă </b>  pentru alții
            </p>
            <p class="col_4">  Complicații semnificative pentru  <b> alte boli </b></p>
            <div class="textin">
    <span>
                     diabet
                </span>
    <span>
                     dermatită alergică
                </span>
    <span>
                     astm
                </span>
            </div>
            <a class="button" href="#form"> Primește pachetul tău </a>
        </div>
    </section>
    <section class="effect">
        <div class="main_container">
            <h2>
                Efectul  <span> INSTANTANEU </span>  al Fresh Fingers <br/>
                asupra bolilor fungice
            </h2>
            <ul>
                <li>
                    inhibă dezvoltarea ciupercii și a micozei. Te scapă de mâncărime. Are un efect <br/>
                    distructiv asupra celulelor micotice deja existente și blochează apariția unor <br/>
                    colonii noi.
                    <span><small>Extract de floare</small> de Chamomilla recutita</span>
                </li>
                <li>
                    elimină transpirația glandelor și inhibă activitatea bacteriilor care produc mirosul <br/>
                    neplăcut de transpirație chiar și la persoanele sănătoase. Aseptizează și înmoaie pielea, <br/>
                    oferindu-i o aromă ușoară florală pentru mult timp.
                    <span><small>Extract de frunză</small> de Salvia officinalis</span>
                </li>
                <li>
                    înmoaie pielea, elimină exfolierea.
                    <span><small>Pudră din sucul frunzei</small> de Aloe barbabensis</span>
                </li>
                <li>
                    răcorește confortabil piciorul, oferind un parfum plăcut.
                    <span><small>Extract din uleiul fructului</small> de Olea europea</span>
                </li>
            </ul>
        </div>
    </section>
    <!--REMOVING-->
    <section class="removing">
        <div class="main_container">
            <h2><b> ELIMINAREA EFICIENTĂ </b>  A TUTUROR CONSECINȚELOR MICOZEI </h2>
            <div class="col_2 left">
                <p> Dispariția mâncărimii </p>
                <p>
                    Scăderea cantității <br/>
                    de transpirație a piciorului
                </p>
                <p>
                    Face <br/>
                    pielea moale și fină
                </p>
                <p>
                    Eliminarea eficientă <br/>
                    a zonelor cu probleme
                </p>
            </div>
            <div class="col_2 right">
                <p> Exfolierea pielii încetează </p>
                <p>
                    Încarnarea <br/>
                    zonei afectate a pielii
                </p>
                <p>
                    Autodistrugerea <br/>
                    infecției micotice
                </p>
                <p>
                    Prevenția <br/>
                    infecției micotice
                </p>
            </div>
            <a class="button" href="#form"> Primește pachetul tău </a>
        </div>
    </section>
    <!--COMMENT-->
    <section class="comment">
        <div class="main_container">
            <h3><b> SPECIALIST ÎN DERMATOLOGIE </b>  COMENTARIU </h3>
            <p class="rev">
                Pentru tratamentul reușit al micozei, le prescriu pacienților mei Fresh Fingers, indiferent de stadiul de dezvoltare a bolii fungice. Fresh Fingers are capacitatea unică de a penetra chiar și în straturile dure ale pielii, le dizolvă rapid pe cele vechi, gel poate fi prescrisă chiar și în cazul progresului agresiv al bolii și pentru utilizare profilactică, nu are contraindicații și efecte secundare.
            </p>
            <h3><span> PĂRERILE </span>  consumatorilor </h3>
            <div class="col_3">
                <div class="image">
                    <img src="./assets/1.jpg"/>
                </div>
                <div class="expert">
                    <p class="user_name">
                        <span> Valeria, </span>  24 de ani
                    </p>
                    <p class="user_com">
                        Recent, am mers la bazin și am luat infecție micotică de acolo. Mâncărimea era foarte puternică. Am mers la spital, iar specialistul în dermatologie mi-a prescris Fresh Fingers. E atât de bine că medicul mi-a recomandat gel, sunt total împotriva luării de medicamente.
                    </p>
                </div>
            </div>
            <div class="col_3">
                <div class="image">
                    <img src="./assets/2.jpg"/>
                </div>
                <div class="expert">
                    <p class="user_name">
                        <span> Camelia, </span>  28 de ani
                    </p>
                    <p class="user_com">
                        Sunt femeie de serviciu și port papuci de serviciu zilnic. Picioarele mele transpiră puternic și sunt atât de obosită, ceea ce a provocat progresul bolii fungice. Am comandat Fresh Fingers imediat și mi-am aseptizat papucii cu suc de lămâie. Gel m-a ajutat, călcâiele uscate au dispărut, iar pielea este complet recuperată.
                    </p>
                </div>
            </div>
            <div class="col_3">
                <div class="image">
                    <img src="./assets/user_3.png"/>
                </div>
                <div class="expert">
                    <p class="user_name">
                        <span> Petre, </span>  29 de ani
                    </p>
                    <p class="user_com">
                        Salut, sunt înotător. Întotdeauna am fost supus riscului și, desigur, am luat toate măsurile necesare pentru a preveni infecția micotică. Dar de câteva ori am uitat papucii de duș și am luat cumva infecție fungică. Soția mea a comandat Fresh Fingers, l-am folosit mai bine de o lună. Exfolierea pielii și mâncărimea au dispărut.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <footer id="form">
        <div class="main_container">
            <div class="row">
                <h1><span> Agent </span> </h1>
                <h1><span> antimicotic Fresh Fingers </span></h1>
                <h6>
                    Ai grijă de sănătatea picioarelor tale!
                </h6>
                <form action="./process.php" class="pull_bottom" method="post">

                    <div class="price">
    <span>
                                 159 RON
                            </span>
    <span>
    <i>
        preț vechi
    </i>
    <s> 318 RON </s>
    </span>
                    </div>
                    <div class="inputs">
                        <p> Spune-ne unde să trimitem  <span> COMANDA TA </span></p>
                        <select id="country_code_selector">
                            <option value="Ro"> România </option>

                        <input name="name" placeholder="Nume" type="text">
                        <input class="only_number" name="phone" placeholder="Telefon" type="text">
                        <!---->
                        <input class="js_pre_toform" type="submit" value="Comanda">
                        <div class="toform">

                        </div>
                        <!---->
                    </div>
                </form>
            </div>
        </div>
        <div class="warning">
            <div class="main_container">
                <p>
                    <b> ATENȚIONARE: </b>  Ca urmare a cererii extrem de mare, există un stoc limitat de Fresh Fingers disponibil începând de
                    <script>dtime_nums(-1, true)</script>
                </p>
            </div>
        </div>
    </footer>
</main>
<script src="./assets/animations.js"></script>
</body>
</html>
<?php } ?>