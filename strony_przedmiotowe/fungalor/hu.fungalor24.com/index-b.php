<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()){ ?>



    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 3400 -->
        <script>var locale = "hu";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIFCgQdzdKLAAEAAQAChwkBAAJIDQEKAQ8EHQkR4AA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Mezei józsef';
            var phone_hint = '+36309925528';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("hu");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>Fresh Fingers</title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/style.css" rel="stylesheet"/>
    </head>
    <body>
    <!--HEADER-->
    <main>
        <header>
            <div class="container_gradient">
                <div class="warning">
                    <div class="main_container">
                        <p>
                            <b>FIGYELEM:</b> A hatalmas TV-s érdeklődés miatt, korlátozott nagyságú készlet elérhető a jelenlegi dátum szerint:
                            <script>
                                dtime_nums(-1, true)
                            </script>
                        </p>
                    </div>
                </div>
                <div class="main_container">
                    <div class="row">
                        <div class="logo">
                            <img src="./assets/mobile/logo_03.png"/>
                        </div>
                        <h1>Fresh Fingers gombaölő</h1>
                        <h6>
                            Őrizd meg lábfejeid egészségét!
                        </h6>
                    </div>
                    <div class="row">
                        <ul class="header_ul">
                            <li>
                                gombás fertőzés<br/>
                                <b>elpusztítása</b>
                            </li>
                            <li>
                                <b>a körömlemez</b><br/> szerkezeti helyreállítása
                            </li>
                            <li>
                                <b>a gyulladásos folyamat</b> <br/> megszüntetése
                            </li>
                        </ul>
                        <form action="./process.php" method="post"><

                            <div class="price text_l">
<span>
                                11900 Ft.
                            </span>
                                <span>
<s>23800 Ft.</s>
</span>
                            </div>
                            <div class="inputs">
                                <p> Mondd meg, hova küldjük a <span> csomagod </span></p>
                                <select id="country_code_selector">
                                    <option value="HU"> Magyarország </option>

                                <input name="name" placeholder="Vezetéknév, Keresztnév" type="text">
                                <input class="only_number" name="phone" placeholder="Mobil telefon" type="text">
                                <input class="js_submit" type="submit" value="Megrendelés">
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <h4>Honnan érezheted a</h4>
                        <h5>a gombák «megtelepülését»?</h5>
                        <div class="li_before">
                            <ul>
                                <li>a lábfejbőr olyan, mintha meszes lenne</li>
                                <li>a lábujjak és a talp közötti bőr hámlani kezd</li>
                                <li>a körömlemez kérgessé válik és lepattogzódik</li>
                                <li>a körömfal megnagyobbodik, vörös lesz és viszketni kezd</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--MYCO-->
        <section class="myco">
            <div class="main_container">
                <h2>A GOMBÁS FERTŐZÉS KEZELÉSE <b>ELENGEDHETETLEN!</b></h2>
                <img src="./assets/mobile/li-icons_03.png"/>
                <p>Kozmetikai <b>hiba</b></p>
                <img src="./assets/mobile/li-icons_06.png"/>
                <p>
                    Belépési pont <b>más fertőzések számára</b>
                </p>
                <img src="./assets/mobile/li-icons_08.png"/>
                <p>
                    A <b>gombás fertőzés</b><br/> mások számára is <b>komoly problémát</b> jelent
                </p>
                <div class="textin">
<span>
                cukorbetegség
            </span>
                    <span>
                allergiás bőrgyulladás
            </span>
                    <span>
                asztma
            </span>
                </div>
                <p> Komoly komplikációk léphetnek fel <b>más betegségekkel kapcsolatban</b></p>
                <a class="button" href="#form">Megrendelés</a>
            </div>
        </section>
        <section class="effect">
            <div class="main_container">
                <h2>
                    A Fresh Fingers <span>azonnali</span> hatása
                    a körömgombán
                </h2>
                <ul>
                    <li>
                        megakadályozza a gomba és az élesztőbaktériumok szaporodását. Megszünteti a viszketést, és elpusztítja a gombás sejteket, illetve megakadályozza az újabb kolóniák kialakulását.
                    </li>
                    <li>
                        megakadályozza a baktériumok behatolását az izzadtságmirigyekbe, és korlátozza a kellemetlen, izzadt szag kialakulását még az egészséges embereknél is. Fertőtleníti és megpuhítja a bőrt,<br/> könnyű, hosszú ideig<br/> kitartó virágos
                        aromát varázsol neki.
                    </li>
                    <li>
                        megpuhítja a bőrt, megszünteti a lepattogzódást.
                    </li>
                    <li>
                        kényelmesen lehűti a lábfejet, kellemes illatot nyújt neki.
                    </li>
                </ul>
            </div>
        </section>
        <!--REMOVING-->
        <section class="removing">
            <div class="main_container">
                <h2><b>INTENZÍVEN ELTÁVOLÍTJA</b><br/> A GOMBÁS FERTŐZÉS ÖSSZES KÖVETKEZMÉNYÉT</h2>
                <div class="row">
                    <p>A bőr lepikkelyezése abbamarad</p>
                    <p>Megszűnik a viszketés</p>
                    <p>
                        A bőr érintett <br/>területei helyreállnak
                    </p>
                    <p>
                        Abbamarad <br/> az alsó lábfej túlzott izzadása
                    </p>
                    <p>
                        A gombás fertőzés<br/> biztonságosan kerül elpusztításra
                    </p>
                    <p>
                        Megpuhítja<br/> és kifinomítja a bőrt
                    </p>
                    <p>
                        Megelőzi a gombás<br/> visszafertőződést
                    </p>
                    <p>
                        Tüzetesen eltávolítja<br/> a fertőzés helyét
                    </p>
                </div>
                <a class="button" href="#form">Megrendelés</a>
            </div>
        </section>
        <!--COMMENT-->
        <section class="comment">
            <div class="main_container">
                <h3><b>BŐRGYÓGYÁSZ</b> SZAKVÉLEMÉNY</h3>
                <p class="rev">
                    A gombás fertőzések megfelelő kezelése céljából, minden páciensemnek felírom a Fresh Fingers -t, a fertőzés kiterjedtségétől függetlenül. A Fresh Fingers azzal az egyedi képességgel rendelkezik, hogy képes behatolni még a bőr legkeményebb rétegeibe is, majd a régi
                    bőrréteget feloldja, így a krém segítségével még a fertőzés agresszív folyamatai is gyógyíthatóak, illetve az anyag megelőzés céljából is használható. Mellékhatások és ellenjavallatok nincsenek.
                </p>
                <h3><span>Vásárlói</span> értékelések</h3>
                <div class="row">
                    <div class="image">
                        <img src="./assets/mobile/1.jpg" width="70"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span>Szalai,</span> 24 years old
                        </p>
                        <p class="user_com">
                            Nemrégiben, elmentem az uszodába, és elkaptam valamilyen gombás fertőzést. A viszketés nagyon erős volt. Elmentem a kórházba, a bőrgyógyászom felírta a Fresh Fingers -t, annyira jó, hogy ezt a krémet ajánlotta, mert nagyon gyógyszerellenes vagyok.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="image">
                        <img src="./assets/mobile/2.jpg" width="70"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span>Borsos,</span> 28 years old
                        </p>
                        <p class="user_com">
                            Karbantartónőként dolgozok, minden nap munkás cipő van a lábamon. A lábfejeim mindig túlontúl izzadtnak érződnek és állandóan fáradt vagyok, végül az egész egy gombás fertőzést eredményezett. Azonnal megrendeltem a Fresh Fingers -t, és citromlével fertőtlenítettem
                            a cipőim. A krém nekem bevált, a sarkaim szárazsága megszűnt, a bőröm tökéletesen regenerálódott.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="image">
                        <img src="./assets/mobile/icons_36.png"/>
                    </div>
                    <div class="com">
                        <p class="user_name">
                            <span>Varga,</span> 29 years old
                        </p>
                        <p class="user_com">
                            Helló mindenkinek, én egy úszó vagyok. Mindig is tudtam, hogy nagy a kockázata a gombás fertőzéseknek az én esetemben, ezért próbáltam megelőző intézkedéseket tenni. De néha előfordult, hogy otthon hagytam a tusolós papucsom, így valahol összeszedtem
                            egy fuzáriumos fertőzést. A feleségem megrendelte a Fresh Fingers-t, körülbelül egy hónapig használtam. A bőr lepattogzódása és a viszketés teljesen megszűnt.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer id="form">
            <div class="main_container">
                <div class="row">
                    <form action="./process.php" method="post">
                        <div class="price">
<span>
                          11900 Ft.
                        </span>
                            <span>
<s>23800 Ft.</s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Mondd meg, hova küldjük a <span> csomagod </span></p>
                            <select id="country_code_selector">
                                <option value="HU"> Magyarország </option>

                            <input name="name" placeholder="Vezetéknév, Keresztnév" type="text">
                            <input class="only_number" name="phone" placeholder="Mobil telefon" type="text">
                            <input class="js_submit" type="submit" value="Megrendelés">
                        </div>

                        </div>
                    </form>
                </div>
            </div>
        </footer>
        <script src="./assets/animations.js"></script>
    </main>
    </body>
    </html>
<?php } else { ?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 3400 -->
        <script>var locale = "hu";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIAIFCgQdzdKLAAEAAQAChwkBAAJIDQEKAQ8EHQkR4AA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'Mezei józsef';
            var phone_hint = '+36309925528';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("hu");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> Fresh Fingers </title>
        <meta charset="utf-8"/>
        <meta content="width=1200, initial-scale=1" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900" rel="stylesheet" type="text/css"/>
        <link href="./assets/style.css" rel="stylesheet"/>
    </head>
    <body>
    <style>
        .inputs{
            padding-bottom: 80px;
        }
        </style>
    <!--HEADER-->
    <main>
        <header>
            <div class="warning">
                <div class="main_container">
                    <p>
                        <b> FIGYELEM: </b> A hatalmas TV-s érdeklődés miatt, korlátozott nagyságú készlet elérhető a jelenlegi dátum szerint:
                        <script>
                            dtime_nums(-1, true)
                        </script>
                    </p>
                </div>
            </div>
            <div class="main_container">
                <div class="row">
                    <div class="logo">
                        <img src="./assets/logo_03.png"/>
                    </div>
                    <h1><span> Fresh Fingers gombaölő </span></h1>
                    <h6>
                        Őrizd meg lábfejeid egészségét!
                    </h6>
                </div>
                <div class="row">
                    <ul class="header_ul pull_bottom">
                        <li>
                            gombás fertőzés <br/>
                            <b> elpusztítása </b>
                        </li>
                        <li>
                            <b> a körömlemez </b><br/> szerkezeti helyreállítása
                        </li>
                        <li>
                            <b> a gyulladásos folyamat </b> <br/> megszüntetése
                        </li>
                    </ul>
                    <form action="./process.php" class="pull_bottom" method="post">
                        <!--<input type="hidden" name="total_price" value="11900.0">
                        <input type="hidden" name="esub" value="-4A25sMQIJIAIFCgQdzdKLAAEAAQAChwkBAAJIDQEKAQ8EHQkR4AA">
                        <input type="hidden" name="goods_id" value="127">
                        <input type="hidden" name="title" value="Fresh Fingers - HU">
                        <input type="hidden" name="ip_city" value="Toruń">
                        <input type="hidden" name="template_name" value="Fresh Fingers_HU">
                        <input type="hidden" name="price" value="11900">
                        <input type="hidden" name="old_price" value="23800">
                        <input type="hidden" name="total_price_wo_shipping" value="11900.0">
                        <input type="hidden" name="package_prices" value="{}">
                        <input type="hidden" name="currency" value="Ft.">
                        <input type="hidden" name="package_id" value="0">
                        <input type="hidden" name="protected" value="None">
                        <input type="hidden" name="ip_country_name" value="Poland">
                        <input type="hidden" name="country_code" value="HU">
                        <input type="hidden" name="shipment_vat" value="0.0">
                        <input type="hidden" name="ip_country" value="PL">
                        <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                        <input type="hidden" name="shipment_price" value="0">
                        <input type="hidden" name="price_vat" value="0.0">-->

                        <div class="price text_l">
<span>
<b> 11900 Ft. </b>
</span>
                            <span>
<i>
                                 régi ár
                            </i>
<s> 23800 Ft. </s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Mondd meg, hova küldjük a <span> csomagod </span></p>
                            <select id="country_code_selector">
                                <option value="HU"> Magyarország </option>

                            <input name="name" placeholder="Vezetéknév, Keresztnév" type="text">
                            <input class="only_number" name="phone" placeholder="Mobil telefon" type="text">
                            <input class="js_submit" type="submit" value="Megrendelés">
                        </div>
                    </form>
                </div>
                <h4> Honnan érezheted a </h4>
                <h5> a gombák «megtelepülését»? </h5>
                <div class="col_2 li_before">
                    <ul>
                        <li> a lábfejbőr olyan, mintha meszes lenne </li>
                        <li> a lábujjak és a talp közötti bőr hámlani kezd </li>
                    </ul>
                </div>
                <div class="col_2 li_before">
                    <ul>
                        <li> a körömlemez kérgessé válik és lepattogzódik </li>
                        <li> a körömfal megnagyobbodik, vörös lesz és viszketni kezd </li>
                    </ul>
                </div>
            </div>
        </header>
        <!--MYCO-->
        <section class="myco">
            <div class="main_container">
                <h2> A GOMBÁS FERTŐZÉS KEZELÉSE  <b> ELENGEDHETETLEN! </b></h2>
                <img src="./assets/img-myco_03.png"/>
                <p class="col_4"> Kozmetikai <b> hiba </b></p>
                <p class="col_4">
                    Belépési pont
                    <b>  más fertőzések számára </b>
                </p>
                <p class="col_4">
                    A <b> gombás fertőzés </b><br/> mások számára is <b> komoly problémát </b> jelent
                </p>
                <p class="col_4"> Komoly komplikációk léphetnek fel <b> más betegségekkel kapcsolatban </b></p>
                <div class="textin">
<span>
                 cukorbetegség
            </span>
                    <span>
                 allergiás bőrgyulladás
            </span>
                    <span>
                 asztma
            </span>
                </div>
                <a class="button" href="#form"> Megrendelés </a>
            </div>
        </section>
        <section class="effect">
            <div class="main_container">
                <h2>
                    A Fresh Fingers  <span> azonnali </span>  hatása <br/>
                    a körömgombán
                </h2>
                <ul>
                    <li>
                        megakadályozza a gomba és az élesztőbaktériumok szaporodását. Megszünteti a viszketést,<br/> és elpusztítja a gombás sejteket, illetve megakadályozza az újabb kolóniák kialakulását.
                        <span>
 Climbazole
                    </span>
                    </li>
                    <li>
                        megakadályozza a baktériumok behatolását az izzadtságmirigyekbe, és korlátozza a kellemetlen, <br/> izzadt szag kialakulását még az egészséges embereknél is. Fertőtleníti és megpuhítja a bőrt, <br/> könnyű, hosszú ideig kitartó
                        virágos aromát varázsol neki.
                        <span>
 Farnezol
                    </span>
                    </li>
                    <li>
                        megpuhítja a bőrt, megszünteti a lepattogzódást.
                        <span>
 E vitamin
                    </span>
                    </li>
                    <li>
                        kényelmesen lehűti a lábfejet, kellemes illatot nyújt neki.
                        <span>
 Mentás illóolaj
                    </span>
                    </li>
                </ul>
            </div>
        </section>
        <!--REMOVING-->
        <section class="removing">
            <div class="main_container">
                <h2><b> INTENZÍVEN ELTÁVOLÍTJA </b>  A GOMBÁS FERTŐZÉS ÖSSZES KÖVETKEZMÉNYÉT </h2>
                <div class="col_2 left">
                    <p> Megszűnik a viszketés </p>
                    <p>
                        Abbamarad az alsó lábfej <br/> túlzott izzadása
                    </p>
                    <p>
                        Megpuhítja és kifinomítja <br/> a bőrt
                    </p>
                    <p>
                        Tüzetesen eltávolítja <br/> a fertőzés helyét
                    </p>
                </div>
                <div class="col_2 right">
                    <p> A bőr lepikkelyezése abbamarad </p>
                    <p>
                        A bőr érintett területei <br/> helyreállnak
                    </p>
                    <p>
                        A gombás fertőzés <br/> biztonságosan kerül elpusztításra
                    </p>
                    <p>
                        Megelőzi <br/> a gombás visszafertőződést
                    </p>
                </div>
                <a class="button" href="#form"> Megrendelés </a>
            </div>
        </section>
        <!--COMMENT-->
        <section class="comment">
            <div class="main_container">
                <h3><b> BŐRGYÓGYÁSZ </b>  SZAKVÉLEMÉNY </h3>
                <p class="rev">
                    A gombás fertőzések megfelelő kezelése céljából, minden páciensemnek felírom a Fresh Fingers -t, a fertőzés kiterjedtségétől függetlenül. A Fresh Fingers azzal az egyedi képességgel rendelkezik, hogy képes behatolni még a bőr legkeményebb rétegeibe is, majd a régi
                    bőrréteget feloldja, így a krém segítségével még a fertőzés agresszív folyamatai is gyógyíthatóak, illetve az anyag megelőzés céljából is használható. Mellékhatások és ellenjavallatok nincsenek.
                </p>
                <h3><span> Vásárlói </span>  értékelések </h3>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/user_1.jpg"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Szalai, </span> 30 éves
                        </p>
                        <p class="user_com">
                            Nemrégiben, elmentem az uszodába, és elkaptam valamilyen gombás fertőzést. A viszketés nagyon erős volt. Elmentem a kórházba, a bőrgyógyászom felírta a Fresh Fingers -t, annyira jó, hogy ezt a krémet ajánlotta, mert nagyon gyógyszerellenes vagyok.
                        </p>
                    </div>
                </div>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/user_2.jpg"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Borsos, </span> 45 éves
                        </p>
                        <p class="user_com">
                            Karbantartónőként dolgozok, minden nap munkás cipő van a lábamon. A lábfejeim mindig túlontúl izzadtnak érződnek és állandóan fáradt vagyok, végül az egész egy gombás fertőzést eredményezett. Azonnal megrendeltem a Fresh Fingers -t, és citromlével fertőtlenítettem
                            a cipőim. A krém nekem bevált, a sarkaim szárazsága megszűnt, a bőröm tökéletesen regenerálódott.
                        </p>
                    </div>
                </div>
                <div class="col_3">
                    <div class="image">
                        <img src="./assets/user_3.jpg"/>
                    </div>
                    <div class="expert">
                        <p class="user_name">
                            <span> Varga, </span> 29 éves
                        </p>
                        <p class="user_com">
                            Helló mindenkinek, én egy úszó vagyok. Mindig is tudtam, hogy nagy a kockázata a gombás fertőzéseknek az én esetemben, ezért próbáltam megelőző intézkedéseket tenni. De néha előfordult, hogy otthon hagytam a tusolós papucsom, így valahol összeszedtem
                            egy fuzáriumos fertőzést. A feleségem megrendelte a Fresh Fingers-t, körülbelül egy hónapig használtam. A bőr lepattogzódása és a viszketés teljesen megszűnt.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer id="form">
            <div class="main_container">
                <div class="row">
                    <h1><span> Fresh Fingers </span> </h1>
                    <h1><span> gombaölő </span></h1>
                    <h6>
                        Őrizd meg lábfejeid egészségét!
                    </h6>
                    <form action="./process.php" class="pull_bottom" method="post">

                        <div class="price">
<span>
                             11900 Ft.
                        </span>
                            <span>
<i>
                                 régi ár
                            </i>
<s> 23800 Ft. </s>
</span>
                        </div>
                        <div class="inputs">
                            <p> Mondd meg, hova küldjük a <span> csomagod </span></p>
                            <select id="country_code_selector">
                                <option value="HU"> Magyarország </option>

                            <input name="name" placeholder="Vezetéknév, Keresztnév" type="text">
                            <input class="only_number" name="phone" placeholder="Mobil telefon" type="text">
                            <input class="js_submit" type="submit" value="Megrendelés">
                        </div>
                            <!--<input type="hidden" name="total_price" value="11900.0">
                            <input type="hidden" name="esub" value="-4A25sMQIJIAIFCgQdzdKLAAEAAQAChwkBAAJIDQEKAQ8EHQkR4AA">
                            <input type="hidden" name="goods_id" value="127">
                            <input type="hidden" name="title" value="Fresh Fingers - HU">
                            <input type="hidden" name="ip_city" value="Toruń">
                            <input type="hidden" name="template_name" value="Fresh Fingers_HU">
                            <input type="hidden" name="price" value="11900">
                            <input type="hidden" name="old_price" value="23800">
                            <input type="hidden" name="total_price_wo_shipping" value="11900.0">
                            <input type="hidden" name="package_prices" value="{}">
                            <input type="hidden" name="currency" value="Ft.">
                            <input type="hidden" name="package_id" value="0">
                            <input type="hidden" name="protected" value="None">
                            <input type="hidden" name="ip_country_name" value="Poland">
                            <input type="hidden" name="country_code" value="HU">
                            <input type="hidden" name="shipment_vat" value="0.0">
                            <input type="hidden" name="ip_country" value="PL">
                            <input type="hidden" name="accept_languages" value="pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4">
                            <input type="hidden" name="shipment_price" value="0">
                            <input type="hidden" name="price_vat" value="0.0">

                            <div class="price">
    <span>
                                 11900 Ft.
                            </span>
                                <span>
    <i>
                                     régi ár
                                </i>
    <s> 23800 Ft. </s>
    </span>
                            </div>
                            <div class="inputs">
                                <p> Mondd meg, hova küldjük a <span> csomagod </span></p>
                                <select id="country_code_selector">
                                    <option value="HU"> Magyarország </option>
                                </select>
                                <input name="name" placeholder="Vezetéknév, Keresztnév" type="text"/>
                                <input class="only_number" name="phone" placeholder="Mobil telefon" type="text"/>
                                <input class="js_submit" type="submit" value="Megrendelés"/>
                            </div>-->
                    </form>
                </div>
            </div>
            <div class="warning">
                <div class="main_container">
                    <p>
                        <b> FIGYELEM: </b> A hatalmas TV-s érdeklődés miatt, korlátozott nagyságú készlet elérhető a jelenlegi dátum szerint:
                        <script>
                            dtime_nums(-1, true)
                        </script>
                    </p>
                </div>
            </div>
        </footer>
    </main>
    <script src="./assets/animations.js"></script>
    </body>
    </html>
<?php } ?>