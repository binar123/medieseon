<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()){ ?>





    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 8233 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALACwQsR9aLAAEAAQACYAsBAAIpIAEKAQ8EzdiIGQA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'I Dont Know';
            var phone_hint = '+1www';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> Fresh Fingers </title>
        <meta charset="utf-8"/>
        <meta content="initial-scale=.8, maximum-scale=1, user-scalable=no" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:500,400,300,700,900,100&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
        <link href="./assets/mobile/style.css" rel="stylesheet"/>
    </head>
    <body>
    <!--HEADER-->
    <main>
        <header>
            <div class="warning">
                <div class="main_container">
                    <p><b> ATTENZIONE: </b> A causa dell'altissima domanda, la disponibilità di Fresh Fingers al giorno è limitata
                        <script>
                            dtime_nums(-1, true)
                        </script>
                    </p>
                </div>
            </div>
            <div class="main_container">
                <div class="row">
                    <h1><span> Agente antimicotico Fresh Fingers </span></h1>
                    <h6>Prendetevi cura della salute dei vostri piedi!</h6></div><img alt="" src="./assets/mobile/Fresh Fingers.png" style="width: 25%; margin-top: 10px;"/>
                <div class="row">
                    <ul class="header_ul pull_bottom">
                        <li>eliminazione<b> delle infezioni micotiche </b></li>
                        <li><b> guarigione della struttura </b> del corpo ungueale</li>
                        <li><b> rimozione </b> del processo infiammatorio</li>
                    </ul>
                </div>
                <h4> Quali sono i sintomi </h4>
                <h5> della micosi? </h5>
                <div class="col_2 li_before">
                    <ul>
                        <li> i piedi sembrano ingessati </li>
                        <li> la pelle della pianta e tra le dita del piede si desquama </li>
                        <li> il tessuto dell'unghia mostra callosità e rischia di staccarsi </li>
                        <li> la parete dell'unghia è gonfia, arrossata e prude </li>
                    </ul>
                </div>
                <div style="margin: 0 auto;"><a class="button buttonUp" href="#form"> Ordina una confezione </a></div>
            </div>
        </header>
        <!--MYCO-->
        <section class="effect">
            <div class="main_container">
                <div class="main_container_bg">
                    <h2>L'effetto  <span> istantaneo </span>  di <br/>Fresh Fingers sulle malattie fungine</h2>
                    <ul>
                        <li><span>Il climbazolo</span>inibisce la crescita dei lieviti e della micosi, allevia il prurito, distrugge le cellule già colpite dall'infezione e impedisce la formazione di nuove colonie batteriche.</li>
                        <li><span>Il farnesolo</span>agisce sulle ghiandole sudoripare e inibisce l'attività dei batteri, che causano cattivo odore anche nelle persone sane. Disinfetta e ammorbidisce la pelle e emana un aroma leggermente fiorito.</li>
                        <li><span>La vitamina E</span>ammorbidisce la pelle e contrasta la desquamazione.</li>
                        <li><span>L'olio essenziale di menta</span>calma l'infiammazione ed ha una fragranza piacevole.</li>
                    </ul>
                </div>
            </div>
        </section>
        <!--REMOVING-->
        <section class="removing">
            <div class="main_container">
                <h2><b> SUBITANEA ELIMINAZIONE </b>  DI TUTTE LE CONSEGUENZE DELLA MICOSI </h2>
                <div class="col_2 left">
                    <p> Il prurito scompare </p>
                    <p>L'eccessiva
                        <br/> sudorazione del piede diminuisce</p>
                    <p>La pelle
                        <br/> diventa sana e morbida</p>
                    <p>Eliminazione
                        <br/> dell'infezione</p>
                    <p> Arresto della desquamazione cutanea </p>
                    <p>guarigione
                        <br/> dell'area di pelle interessata</p>
                    <p>Eliminazione sicura
                        <br/> dell'infezione micotica</p>
                    <p>Prevenzione
                        <br/> di una nuova infezione micotica</p>
                </div><a class="button" href="#form"> Ordina subito </a></div>
        </section>
        <!--COMMENT-->
        <section class="comment">
            <div class="main_container">
                <h3><b> L'OPINIONE </b>  DI UN DERMATOLOGO </h3>
                <p class="rev">Raccomando spesso Fresh Fingers ai miei pazienti per il trattamento della micosi, indipendentemente dallo stadio dell'infezione. Fresh Fingers ha la capacità unica di penetrare anche attraverso gli strati più spessi di pelle, elimina le cellule morte ed èanche indicata per gli stadi più avanzati del disturbo e come mezzo preventivo. Non ha controindicazioni o effetti collaterali.</p>
                <h3><span> Commenti </span>  dei clienti </h3>
                <div class="col_3"><img src="./assets/mobile/1.jpg" style="float: left; margin: 10px;"/>
                    <p class="user_name"><span> Valeria, </span> 24 anni</p>
                    <p class="user_com">Di recente, ho contratto la micosi in piscina. Il prurito era intensissimo, allora sono andata al pronto soccorso e mi hanno prescritto Fresh Fingers. Funziona benissimo.</p>
                </div>
                <div class="col_3"><img src="./assets/mobile/2.jpg" style="float: left; margin: 10px;"/>
                    <p class="user_name"><span> Chiara, </span> 28 anni</p>
                    <p class="user_com">Sono una donna delle pulizie, e indosso scarpe chiuse tutto il giorno. I piedi sudano tantissimo e così le malattie fungine si propagano. Ho subito ordinato Fresh Fingers e ho disinfettato le mie scarpe con del succo di limone. La crema mi ha aiutata molto,la pelle secca è scomparsa e i piedi sono guariti.</p>
                </div>
                <div class="col_3"><img src="./assets/mobile/user_3.png" style="float: left; margin: 10px;"/>
                    <p class="user_name"><span> Pietro, </span> 29 anni</p>
                    <p class="user_com">Ciao, sono un nuotatore. Sono sempre a rischio di contrarre infezioni e quindi devo prendere le misure necessarie per prevenire le infezioni micotiche. Qualche volta, però, mi sono scordato le ciabatte e così mi sono preso un'infezione da fusarium.Mia moglie mi ha ordinato Fresh Fingers, l'ho usato per più di un mese. La desquamazione e il prurito sono scomparsi.</p>
                </div>
            </div>
        </section>
        <footer id="form">
            <div class="main_container">
                <div class="row">
                    <h1><span> Agente </span> </h1>
                    <h1><span> antimicotico Fresh Fingers </span></h1>
                    <h6>Prenditi cura della salute dei tuoi piedi!</h6>
                    <form action="./process.php" class="pull_bottom" method="post">

                        <div class="price"><span>39 €</span><span><i>vecchio prezzo</i><s>78 €</s></span></div>
                        <div class="inputs">
                            <p> Diteci dove spedire <span> il vostro pacco </span></p>
                            <select id="country_code_selector">
                                <option value="It"> Italia </option>


                            <input name="name" placeholder="Name" type="text">
                            <input class="only_number" name="phone" placeholder="Phone" type="text">
                            <!---->
                            <input class="js_pre_toform" type="submit" value="Ricevi il prodotto">
                            <div class="toform">

                            </div>
                            <!---->
                        </div>
                    </form>
                </div>
            </div>
            <div class="warning">
                <div class="main_container">
                    <p><b> ATTENZIONE: </b> A causa dell'altissima domanda, la disponibilità di Fresh Fingers al giorno è limitata
                        <script>
                            dtime_nums(-1, true)
                        </script>
                    </p>
                </div>
            </div>
        </footer>
    </main>
    <script src="./assets/animations.js"></script>
    </body>
    </html>
<?php } else { ?>


    <!DOCTYPE html>
    <html>
    <head>
        <!-- [pre]land_id = 8233 -->
        <script>var locale = "it";</script>        <!-- country code -->
        <script>var lang_locale = "pl";</script>   <!-- browser locale -->
        <script>var esub = "-4A25sMQIJIALACwQsR9aLAAEAAQACYAsBAAIpIAEKAQ8EzdiIGQA";</script>
        <script type="text/javascript" src="./assets/jquery.min.js"></script>
        <script type="text/javascript" src="./assets/placeholders.min.js"></script>
        <script type="text/javascript" src="./assets/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="./assets/dr.js"></script>
        <script type="text/javascript" src="./assets/dtime.js"></script>
        <script type="text/javascript" src="./assets/js.cookie.js"></script>
        <script type="text/javascript" src="./assets/validation.js"></script>
        <script type="text/javascript" src="./assets/order_me.js"></script>
        <style>
            .ac_footer {
                position: relative;
                top: 10px;
                height:0;
                text-align: center;
                margin-bottom: 70px;
                color: #A12000;
            }
            .ac_footer a {
                color: #A12000;
            }
            img[height="1"], img[width="1"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript" src="./assets/sender.js"></script>

        <script>

            var package_prices = {};
            var shipment_price = 0;
            var name_hint = 'I Dont Know';
            var phone_hint = '+1www';

            $(document).ready(function() {

                $('body').append('<div class="ac_footer"><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a></div>');

                moment.locale("it");
                $('.day-before').text(moment().subtract(1, 'day').format('D.MM.YYYY'));
                $('.day-after').text(moment().add(1, 'day').format('D.MM.YYYY'));

            });
        </script>
        <link type="text/css" href="./assets/order_me.css" rel="stylesheet" media="all">





        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1666009176948198');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1666009176948198&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->



        <meta charset="utf-8"/>
        <title> Fresh Fingers </title>
        <meta charset="utf-8"/>
        <meta content="initial-scale=.8, maximum-scale=1, user-scalable=no" name="viewport"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:500,400,300,700,900,100&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
        <link href="./assets/style.css" rel="stylesheet"/>
    </head>
    <body>
    <!--HEADER-->
    <style>
        .inputs{
            padding-bottom: 80px;
        }
    </style>
    <main>
        <header>
            <div class="warning">
                <div class="main_container">
                    <p><b> ATTENZIONE: </b> A causa dell'altissima domanda, la disponibilità di Fresh Fingers al giorno è limitata
                        <script>
                            dtime_nums(-1, true)
                        </script>
                    </p>
                </div>
            </div>
            <div class="main_container">
                <div class="row">
                    <div class="logo"><img src="./assets/logo_03.png"/></div>
                    <h1><span> Agente antimicotico Fresh Fingers </span></h1>
                    <h6 class="h6header">Prendetevi cura della salute dei vostri piedi!</h6></div>
                <div class="row">
                    <ul class="header_ul pull_bottom">
                        <li>eliminazione<b> delle infezioni micotiche </b></li>
                        <li><b> guarigione della struttura </b>del corpo ungueale</li>
                        <li><b> rimozione </b>del processo infiammatorio</li>
                    </ul>
                    <form action="./process.php" class="pull_bottom" method="post">

                        <div class="price text_l"><span><b>39 €</b></span><span><i>prezzo vecchio</i><s>78 €</s></span></div>
                        <div class="inputs">
                            <p> Diteci dove spedire <span> il vostro pacco </span></p>
                            <select id="country_code_selector">
                                <option value="It"> Italia </option>

                            <input name="name" placeholder="Name" type="text">
                            <input class="only_number" name="phone" placeholder="Phone" type="text">
                            <!---->
                            <input class="js_pre_toform" type="submit" value="Ricevi il prodotto">
                            <div class="toform">

                            </div>
                            <!---->
                        </div>
                    </form>
                </div>
                <h4> Quali sono i sintomi </h4>
                <h5> della micosi? </h5>
                <div class="col_2 li_before">
                    <ul>
                        <li> i piedi sembrano ingessati </li>
                        <li> la pelle della pianta e tra le dita del piede si desquama </li>
                    </ul>
                </div>
                <div class="col_2 li_before">
                    <ul>
                        <li> il tessuto dell'unghia mostra callosità e rischia di staccarsi </li>
                        <li> la parete dell'unghia è gonfia, arrossata e prude </li>
                    </ul>
                </div>
            </div>
        </header>
        <!--MYCO-->
        <section class="myco">
            <div class="main_container">
                <h2> CURARE LA MICOSI  <b> È NECESSARIO! </b></h2><img src="./assets/img-myco_03.png"/>
                <p class="col_4"> Difetti <b> estetici </b></p>
                <p class="col_4">Può causare l'insorgenza<b>  di altre malattie infettive </b></p>
                <p class="col_4">La <b> micosi </b>
                    <br/>è un <b> problema serio </b> che va curato</p>
                <p class="col_4"> Notevoli complicazioni per <b> altre malattie </b></p>
                <div class="textin"><span>diabete</span><span>dermatite allergica</span><span>asma</span></div><a class="button" href="#form"> Ordina una confezione </a></div>
        </section>
        <section class="effect">
            <div class="main_container">
                <h2>L'effetto  <span> istantaneo </span>  di <br/>Fresh Fingers sulle malattie fungine</h2>
                <ul>
                    <li>inibisce la crescita dei lieviti e della micosi, allevia il prurito, distrugge le cellule già colpite dall'infezione e impedisce la formazione di nuove colonie batteriche.<span>Il climbazolo</span></li>
                    <li>agisce sulle ghiandole sudoripare e inibisce l'attività dei batteri, che causano cattivo odore anche nelle persone sane. Disinfetta e ammorbidisce la pelle e emana un aroma leggermente fiorito.<span>Il farnesolo</span></li>
                    <li>ammorbidisce la pelle e contrasta la desquamazione.<span>La vitamina E</span></li>
                    <li>calma l'infiammazione ed ha una fragranza piacevole.<span>L'olio essenziale di menta</span></li>
                </ul>
            </div>
        </section>
        <!--REMOVING-->
        <section class="removing">
            <div class="main_container">
                <h2><b> SUBITANEA ELIMINAZIONE </b>  DI TUTTE LE CONSEGUENZE DELLA MICOSI </h2>
                <div class="col_2 left">
                    <p> Il prurito scompare </p>
                    <p>L'eccessiva
                        <br/>sudorazione del piede diminuisce</p>
                    <p>La pelle
                        <br/>diventa sana e morbida</p>
                    <p>Eliminazione
                        <br/>dell'infezione</p>
                </div>
                <div class="col_2 right">
                    <p> Arresto della desquamazione cutanea </p>
                    <p>guarigione
                        <br/>dell'area di pelle interessata</p>
                    <p>Eliminazione sicura
                        <br/>dell'infezione micotica</p>
                    <p>Prevenzione
                        <br/>di una nuova infezione micotica</p>
                </div><a class="button" href="#form"> Ordina subito </a></div>
        </section>
        <!--COMMENT-->
        <section class="comment">
            <div class="main_container">
                <h3><b> L'OPINIONE </b>  DI UN DERMATOLOGO </h3>
                <p class="rev">Raccomando spesso Fresh Fingers ai miei pazienti per il trattamento della micosi, indipendentemente dallo stadio dell'infezione. Fresh Fingers ha la capacità unica di penetrare anche attraverso gli strati più spessi di pelle, elimina le cellule morte ed è anche indicata per gli stadi più avanzati del disturbo e come mezzo preventivo. Non ha controindicazioni o effetti collaterali.</p>
                <h3><span> Commenti </span>  dei clienti </h3>
                <div class="col_3">
                    <div class="image"><img src="./assets/1.jpg"/></div>
                    <div class="expert">
                        <p class="user_name"><span> Valeria, </span> 24 anni</p>
                        <p class="user_com">Di recente, ho contratto la micosi in piscina. Il prurito era intensissimo, allora sono andata al pronto soccorso e mi hanno prescritto Fresh Fingers. Funziona benissimo.</p>
                    </div>
                </div>
                <div class="col_3">
                    <div class="image"><img src="./assets/2.jpg"/></div>
                    <div class="expert">
                        <p class="user_name"><span> Chiara, </span> 28 anni</p>
                        <p class="user_com">Sono una donna delle pulizie, e indosso scarpe chiuse tutto il giorno. I piedi sudano tantissimo e così le malattie fungine si propagano. Ho subito ordinato Fresh Fingers e ho disinfettato le mie scarpe con del succo di limone. La crema mi ha aiutata molto, la pelle secca è scomparsa e i piedi sono guariti.</p>
                    </div>
                </div>
                <div class="col_3">
                    <div class="image"><img src="./assets/user_3.png"/></div>
                    <div class="expert">
                        <p class="user_name"><span> Pietro, </span> 29 anni</p>
                        <p class="user_com">Ciao, sono un nuotatore. Sono sempre a rischio di contrarre infezioni e quindi devo prendere le misure necessarie per prevenire le infezioni micotiche. Qualche volta, però, mi sono scordato le ciabatte e così mi sono preso un'infezione da fusarium. Mia moglie mi ha ordinato Fresh Fingers, l'ho usato per più di un mese. La desquamazione e il prurito sono scomparsi.</p>
                    </div>
                </div>
            </div>
        </section>
        <footer id="form">
            <div class="main_container">
                <div class="row">
                    <h1><span> Agente </span> </h1>
                    <h1><span> antimicotico Fresh Fingers </span></h1>
                    <h6>Prenditi cura della salute dei tuoi piedi!</h6>
                    <form action="./process.php" class="pull_bottom" method="post">

                        <div class="price"><span>39 €</span><span><i>vecchio prezzo</i><s>78 €</s></span></div>
                        <div class="inputs">
                            <p> Diteci dove spedire <span> il vostro pacco </span></p>
                            <select id="country_code_selector">
                                <option value="It"> Italia </option>

                            <input name="name" placeholder="Name" type="text">
                            <input class="only_number" name="phone" placeholder="Phone" type="text">
                            <!---->
                            <input class="js_pre_toform" type="submit" value="Ricevi il prodotto">
                            <div class="toform">

                            </div>
                            <!---->
                        </div>
                    </form>
                </div>
            </div>
            <div class="warning">
                <div class="main_container">
                    <p><b> ATTENZIONE: </b> A causa dell'altissima domanda, la disponibilità di Fresh Fingers al giorno è limitata
                        <script>
                            dtime_nums(-1, true)
                        </script>
                    </p>
                </div>
            </div>
        </footer>
    </main>
    <script src="./assets/animations.js"></script>
    </body>
    </html>
<?php } ?>