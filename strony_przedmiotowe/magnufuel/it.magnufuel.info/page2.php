<div  id="step2"  style="display:none" >



    <div class="header" id="step2_ib">

        <div class="wrapper clearfix">
            <div class="step2 clearfix">

                <div class="holder-form">
                    <h1 class="center">Riduzione del consumo di carburante del <strong class="step2_ib1">20%!</strong>
                        <img src="images/head_ico1.png" class="new_logo4" alt="IMG">
                    </h1>
                </div>


                <div class="col-6">
                    <img src="images/prod_s2.png" class="prod_s2" alt="">

                    <br>


                    <div class="priceBoxContainer">
                        <img src="images/2_sticker.png" class="new_logo" alt="">
                        <img src="images/1_sticker.png" class="new_logo2" alt="">
                        <table class="priceBox">
                            <tbody>
                            <tr>
                                <td class="text-right">Prezzo normale:</td>
                                <td class="rPrice text-left lTh">
                                        <span class="bold" id="full_price" style="color:red">
                                        €&nbsp;55.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Prezzo:</td>
                                <td class="price text-left" style="font-size:26px">
                                        <span class="bold" id="promoTotal">
                                        €&nbsp;45.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Spedizione:</td>
                                <td class="fShipping text-left">GRATIS</td>
                            </tr>
                            <tr>
                                <td class="text-right">Risparmi:</td>
                                <td class="uSave text-left" id="discount">€&nbsp;10.00</td>
                            </tr>
                            </tbody>
                        </table>
                        <select name="priceSelect" class="priceSelect">
                            <option value="0">Quantità: 1</option>
                            <option value="1">Quantità: 2</option>
                            <option value="2">Quantità: 3</option>
                        </select>
                        <p class="more_text_privacy">* Consigliato dalla European Automobile Manufacturers Association</p>
                    </div>

                </div>

                <br>

                <div class="col-6">

                    <form name="orderform" id="orderform" action="step3.php" method="post" >
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAI_FwAAAAAAAAAAAASaFRrAAA"></iframe>

                        <!--<input type="hidden" value="0" id="product_selection" name="product_selection">
                        <input type="hidden" value="On" id="sameshipping" name="sameshipping">
                        <label for="billing_fname">Nome</label>
                        <input id="billing_fname" type="text" name="billing_fname" placeholder="Nome">

                        <label for="billing_lname">Cognome</label>
                        <input id="billing_lname" type="text" name="billing_lname" placeholder="Cognome" value="">


                        <label for="billing_street">Via e numero</label>
                        <input id="billing_street" type="text" name="billing_street" placeholder="Via e numero" value="">
                        <input id="billing_number" type="text" name="billing_number" value="">

                        <label for="shipping_zvanec">Citofono, n.scala, palazzo,ecc.</label>
                        <input id="shipping_zvanec" type="text" placeholder="Citofono, n.scala, palazzo,ecc." name="shipping_zvanec" value="">

                        <label for="billing_state">Provincia</label>
                        <select id="billing_state" name="billing_state" value="">
                            <option value="">Scegliere</option>
                            <option value="AG">Agrigento</option>
                            <option value="AL">Alessandria</option>
                            <option value="AN">Ancona</option>
                            <option value="AO">Aosta</option>
                            <option value="AR">Arezzo</option>
                            <option value="AP">Ascoli Piceno</option>
                            <option value="AT">Asti</option>
                            <option value="AV">Avellino</option>
                            <option value="BA">Bari</option>
                            <option value="BT">Barletta-Andria-Trani</option>
                            <option value="BL">Belluno</option>
                            <option value="BN">Benevento</option>
                            <option value="BG">Bergamo</option>
                            <option value="BI">Biella</option>
                            <option value="BO">Bologna</option>
                            <option value="BZ">Bolzano</option>
                            <option value="BS">Brescia</option>
                            <option value="BR">Brindisi</option>
                            <option value="CA">Cagliari</option>
                            <option value="CL">Caltanissetta</option>
                            <option value="CB">Campobasso</option>
                            <option value="CI">Carbonia-Iglesias</option>
                            <option value="CE">Caserta</option>
                            <option value="CT">Catania</option>
                            <option value="CZ">Catanzaro</option>
                            <option value="CH">Chieti</option>
                            <option value="CO">Como</option>
                            <option value="CS">Cosenza</option>
                            <option value="CR">Cremona</option>
                            <option value="KR">Crotone</option>
                            <option value="CN">Cuneo</option>
                            <option value="EN">Enna</option>
                            <option value="FM">Fermo</option>
                            <option value="FE">Ferrara</option>
                            <option value="FI">Firenze</option>
                            <option value="FG">Foggia</option>
                            <option value="FC">Forli-Cesena</option>
                            <option value="FR">Frosinone</option>
                            <option value="GE">Genova</option>
                            <option value="GO">Gorizia</option>
                            <option value="GR">Grosseto</option>
                            <option value="IM">Imperia</option>
                            <option value="IS">Isernia</option>
                            <option value="SP">La Spezia</option>
                            <option value="AQ">L'Aquila</option>
                            <option value="LT">Latina</option>
                            <option value="LE">Lecce</option>
                            <option value="LC">Lecco</option>
                            <option value="LI">Livorno</option>
                            <option value="LO">Lodi</option>
                            <option value="LU">Lucca</option>
                            <option value="MC">Macerata</option>
                            <option value="MN">Mantova</option>
                            <option value="MS">Massa-Carrara</option>
                            <option value="MT">Matera</option>
                            <option value="ME">Messina</option>
                            <option value="MI">Milano</option>
                            <option value="MO">Modena</option>
                            <option value="MB">Monza e della Brianza</option>
                            <option value="NA">Napoli</option>
                            <option value="NO">Novara</option>
                            <option value="NU">Nuoro</option>
                            <option value="OT">Olbia-Tempio</option>
                            <option value="OR">Oristano</option>
                            <option value="PD">Padova</option>
                            <option value="PA">Palermo</option>
                            <option value="PR">Parma</option>
                            <option value="PV">Pavia</option>
                            <option value="PG">Perugia</option>
                            <option value="PU">Pesaro e Urbino</option>
                            <option value="PE">Pescara</option>
                            <option value="PC">Piacenza</option>
                            <option value="PI">Pisa</option>
                            <option value="PT">Pistoia</option>
                            <option value="PN">Pordenone</option>
                            <option value="PZ">Potenza</option>
                            <option value="PO">Prato</option>
                            <option value="RG">Ragusa</option>
                            <option value="RA">Ravenna</option>
                            <option value="RC">Reggio Calabria</option>
                            <option value="RE">Reggio Emilia</option>
                            <option value="RI">Rieti</option>
                            <option value="RN">Rimini</option>
                            <option value="RM">Roma</option>
                            <option value="RO">Rovigo</option>
                            <option value="SA">Salerno</option>
                            <option value="VS">Medio Campidano</option>
                            <option value="SS">Sassari</option>
                            <option value="SV">Savona</option>
                            <option value="SI">Siena</option>
                            <option value="SR">Siracusa</option>
                            <option value="SO">Sondrio</option>
                            <option value="TA">Taranto</option>
                            <option value="TE">Teramo</option>
                            <option value="TR">Terni</option>
                            <option value="TO">Torino</option>
                            <option value="OG">Ogliastra</option>
                            <option value="TP">Trapani</option>
                            <option value="TN">Trento</option>
                            <option value="TV">Treviso</option>
                            <option value="TS">Trieste</option>
                            <option value="UD">Udine</option>
                            <option value="VA">Varese</option>
                            <option value="VE">Venezia</option>
                            <option value="VB">Verbano-Cusio-Ossola</option>
                            <option value="VC">Vercelli</option>
                            <option value="VR">Verona</option>
                            <option value="VV">Vibo Valentia</option>
                            <option value="VI">Vicenza</option>
                            <option value="VT">Viterbo</option>
                        </select>

                        <label for="billing_city">Città</label>
                        <input id="billing_city" type="text" name="billing_city" placeholder="Città" value="">

                        <label for="billing_zip">CAP</label>
                        <input id="billing_zip" type="text" name="billing_zip" placeholder="CAP" value="">

                        <label for="billing_email">E-mail</label>
                        <input id="billing_email" placeholder="E-mail" type="text" name="billing_email">

                        <label for="billing_phone">Telefono</label>
                        <input id="billing_phone" type="text" name="billing_phone" placeholder="Telefono" value="">

                        <label for="billing_country">Stato</label>
                        <select name="billing_country" id="billing_country">
                            <option value="IT" selected="selected">Italia</option>
                        </select>

                        <button class="rushOrder" type="submit">ordina adesso!</button>-->

                        <p class="some_info">La spesa dell'acquisto di Magnufuel si recupera in 1-2 mesi!</p>

                    </form>

                </div>
            </div>

            <div class="step2_ib3">
                <div class="subblock-step2">
                    <img src="images/Success_Guarantee.png" alt="img" class="guaranteeImg">
                    <h4 class="step2_ib2">100% GARANTITO</h4>
                    <p>MagnuFuel garantisce la qualità del prodotto e del lavoro che viene fatto per renderlo efficace, affinché tu possa risparmiare denaro. Se riscontri un problema nell'utilizzo del prodotto che hai acquistato, puoi usufruire della garanzia soddisfatti o rimborsati.</p>
                </div>
            </div>

            <div class="wrapper clearfix">



                <div class="subblock-step2">
                    <img src="images/free_delivery_orange.png" alt="img" class="deliveryImg">
                    <h4 class="step2_ib2"> GRATIS</h4>
                    <p>La spedizione è gratis per minimizzare le spese! Dopo aver effettuato l'ordine, il prodotto sarà spedito in 1-3 giorni lavorativi, in base al luogo di spedizione. La spedizione viene effettuata durante le normali ore lavorative, quindi assicurarsi la disponibilità all'indirizzo fornito.</p>
                    <br>
                </div>

                <div class="subblock-step2">
                    <img src="images/payment-icon.png" alt="img" class="paymentImg">
                    <h4 class="step2_ib2">METODO DI PAGAMENTO</h4>
                    <p>Il pagamento è in contanti alla consegna. Senza rischiose transazioni finanziarie o pagamenti anticipati.</p>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!-- end of #wrapper -->



    <div class="border clearfix"></div>

    <div id="footer" class="clearfix">
        <div class="wrapper clearfix">
        </div>
    </div>

    <script type="text/javascript">
        jQuery(function($) {
            $('.priceSelect').on('change', function() {
                $('#product_selection').val($(this).val());
                $('#full_price').html(full_prices[$(this).val()]);
                $('#discount').html(discount[$(this).val()]);
                $('#promoTotal').html(prices[$(this).val()]);
            });
        });

    </script>

    <script type="text/javascript">
        var prices = [
            '&euro;&nbsp;45.00', '&euro;&nbsp;79.00', '&euro;&nbsp;109.00', '&euro;&nbsp;0.00'
        ];
        var quants = [
            '1', '2', '3', ''
        ];
        var full_prices = [
            '&euro;&nbsp;55.00', '&euro;&nbsp;110.00', '&euro;&nbsp;165.00', '&euro;&nbsp;0.00'
        ];
        var discount = [
            '&euro;&nbsp;10.00', '&euro;&nbsp;31.00', '&euro;&nbsp;56.00', '&euro;&nbsp;0.00'
        ];

    </script>

    <script type="text/javascript">
        $("#billing_address").on('focus', function() {
            $("body").append("<div class=\"tip\"><div class=\"toolTip\"><div class=\"toolTipArrow\"></div>Inserisci l'indirizzo</div></div>");
            $(".tip").css({
                top: $(this).offset().top + $(".tip").height() - 3,
                left: $(this).offset().left
            });
        });
        $("#billing_address").on('keydown', function() {
            $(".tip").remove();
        });

        $("#billing_address").on('blur', function() {
            $(".tip").remove();
        });

    </script>

















</div>
