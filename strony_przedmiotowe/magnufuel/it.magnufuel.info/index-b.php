<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect();
?>

<?php if ($detect->isMobile()) { ?>


    <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="carburante risparmio economico gas carburante benzina risparmio carburante dispositivo di risparmio dispositivo per risparmiare diesel efficienza del carburante efficienza del motore tubo del carburante economia carburante risparmio carburante dispositivo di risparmio carburante magnete neodimio magnete riduzione emissioni di molecole di idrocarburi molecole di combustibile minor consumo basso consumo combustibile riduzione di CO riduzione di CH capacità del motore combustione completa">

        <meta name="description" content="Magnufuel dispositivo per risparmiare carburante">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../../favicon.ico">

        <link rel="stylesheet" href="!common_files/styles_m.css" type="text/css">

        <script src="!common_files/jquery.js" type="text/javascript"></script>
        <script src="!common_files/navigation.js" type="text/javascript"></script>
        <title>Sito Ufficiale | Magnufuel</title>

        <script src="!common_files/popup_m.js.php"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('.glyphicon-shopping-cart').on('click',function(){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
            /**/
        </script>


    </head>

    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;">

<div id="visible_first">




    <header class="header">

        <div class="container">
            <div class="col">

                <h2 class="style_cib1">Inizia a risparmiare</h2>
                <h3 class="style_cib2"> <strong> più del <strong class="bg-orange">20%</strong> di carburante oggi.</strong></h3>
            </div>

        </div>
    </header>



    <div class="container">
        <nav>

            <img src="images/logo.png" alt="logo">
            <div class="pull-right shoppingCard">
                <a class="glyphicon glyphicon-shopping-cart" ></a>
            </div>

            <div class="clear"></div>
        </nav>
        <section class="col-12">
            <article>
                <div class="figure">
                    <img src="images/ezgif.com-crop.gif" class="product-img" alt="" id="style_cib33">

                    <img src="images/strelka_2.png" class="strelka2" alt="">
                </div>
                <div class="style_cib4">
                    <img src="images/1_sticker.png" alt="IMG" class="style_cib5">
                    <img src="images/2_sticker.png" alt="IMG" class="style_cib5"></div>
                <p class="style_cib6">* Consigliato dalla European Automobile Manufacturers Association</p>
                <p class="text-center">
                    Quando il carburante passa attraverso la forza del campo magnetico, le catene dei carboidrati si risolvono in componenti più semplici che vengono attivati di conseguenza.
                </p>

                <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post" onsubmit="return template_validator({})">

                    <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                    <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                </form>

                <button class="form-index rushOrder" > Ordina </button>
                <div class="phoneMobile">
                    <i class="glyphicon glyphicon-earphone"></i>
                    <p class="ib_pho_text"> 390694804413</p>
                </div>
                <div class="text-center mblock2">
                    <div class="row bg-inverse">
                        <div class="container212">
                            <ul class="list-ico">
                                <li>
                                    <img src="images/horse.png" class="ico-horse" alt="">
                                    <p class="style_cib7">Aumenta la capacità del motore di 5 Hp </p>
                                </li>
                                <li>

                                    <img src="images/benz.png" class="ico-benz" alt="">
                                    <p class="style_cib8">  <br> Riduzione del consumo di carburante fino al 20%</p>
                                </li>
                                <li>

                                    <img src="images/list.png" class="ico-lists" alt="">
                                    <p class="style_cib9">Riduzione delle emissioni di CO e CH fino al 40% - 50% </p>
                                </li>
                                <li>
                                    <img src="images/list_special.png" class="ico-bis" alt="">
                                    <p class="style_cib10">Il dispositivo è certificato</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="#sert" class="JUMP"><p>Fare clic per visualizzare il certificato</p></a>

                <h3 class="text-center" id="style_cib11">
                    Come funziona Magnufuel?
                </h3>
                <div class="text-center">
                    <img src="images/new_logo2.jpg" class="style_cib2016" alt="img">
                </div>
                <p class="text-center" id="style_cib13">
                    Tutti i tipi di carburante, indipendentemente da dove sono venduti, cambiano in modo costante le sostanze, a causa dell'influenza che hanno temperature e umidità. Tale influenza estende e comprime anche il carburante. Le molecole di idrocarburi, base dei carburanti, iniziano a gravitare una vicina all'altra, formando gruppi molecolari, che non possono bruciare interamente perché parte di essi è fuori dalla zona di azione dell'ossigeno. Quando il carburante passa attraverso l'area in cui è installato Magnufuel le frequenze della risonanza magnetica generate dal dispositivo disperdono i gruppi in molecole singole, caricandole positivamente. In tal modo, l'ossigeno penetra in ogni molecola del carburante, permettendo una combustione totale della miscela aria-carburante.
                    <br> <br> Il risultato è un minor consumo di combustibile e una riduzione dell'emissioni pericolose. La parte principale del dispositivo Magnufuel è il magnete neodimio (NdFeB) composto di due parti. Fino ad oggi, questo effetto si può ottenere solo con i magneti di neodimio grazie ad una speciale tecnologia (Brevetto USA № 4.802.931, 4.496.395, 7.458.412, rilasciato dalla corporazione della "General Motors").
                    <br> <br> I magneti di altri produttori (che sono mere imitazioni economiche) non hanno un tale effetto.
                    <br>
                </p><div class="style_cib14">
                    <br><h2>Installazione</h2><br>L'installazione non richiede particolari conoscenze. La parte più importante consiste nel non sbagliare a scegliere il tubo. Il dispositivo deve essere installato sul tubo del carburante. <br>• Aprire il cofano della macchina, individuare il tubo del carburante e scegliere dove installare il dispositivo;<br>• Attaccare Magnufuel sul tubo del carburante. <br>• Per una maggiore stabilità si consiglia di utilizzare la fascetta di plastica inclusa nella confezione. La fascetta di plastica deve passare in un piccolo foro nel canale centrale del dispositivo. Stringerla saldamente e se necessario tagliare la fascetta avanzata;
                </div>
                <img class="style_cib26" src="images/ezgif.com-crop.gif" alt="">

                <a href="#sert" class="JUMP"><p>Fare clic per visualizzare il certificato</p></a>

                <div class="style_cib24">
                    <button class="form-index rushOrder" > Ordina </button>
                </div>
                <div class="phoneMobile">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <p class="ib_pho_text"> 390694804413</p>
                </div>
                <h2 class="style_cib15">Principali vantaggi</h2>
                <div class="magnit col-4">

                </div>
                <br>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_1.jpg" alt="">
                    <p class="magnit-text">Facile da installare. L'installazione del dispositivo richiedere 5 minuti.</p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_2.jpg" alt="">
                    <p class="magnit-text">Non produce gas di scarico, prolungando considerevolmente la vita delle fasce elastiche. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_3.jpg" alt="">
                    <p class="magnit-text">Prolunga in modo significativo la vita del <br> catalizzatore. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_4.jpg" alt="">
                    <p class="magnit-text">Efficacia confermata. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_5.jpg" alt="">
                    <p class="magnit-text">Con l'installazione di Magnufuel avverrà una più <br> completa combustione.</p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_6.jpg" alt="">
                    <p class="magnit-text">Niente più spie del motore accese, principalmente dovute a una minore e incompleta combustione del combustibile. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_7.jpg" alt="">
                    <p class="magnit-text">La spesa dell'acquisto di Magnufuel si recupera in 1-2 mesi!</p>
                </div>

                <div class="clear"></div>

                <div class="text-center">
                    <a href="#sert" class="JUMP"><p>Fare clic per visualizzare il certificato</p></a>

                    <button class="form-index rushOrder" > Ordina </button>
                    <div class="phoneMobile">
                        <span class="glyphicon glyphicon-earphone"></span>
                        <p class="ib_pho_text"> 390694804413</p>
                    </div>

                </div>
                <div class=" sub-title">
                    <p class="style_cib16">RECENSIONI DEI NOSTRI CLIENTI</p>
                </div>

                <div class="doctors-2 col-6">
                    <img class="pull-left" src="images/driver3.jpg" alt="">
                    <p class="style_cib17"> Andrea Giannini 32 anni
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>era </small>  10L<i class="slash"></i></span> /
                        <span class="text-orange"><small>è</small> 7L</span>
                    </div>
                    <hr>
                    <strong>Quattro giorni fa ho installato Magnufuel su una Hyundai Accent (2012). Di solito in città consumavo 10 litri, ora 7-8. Sull'autostrada ora solo 5,8 litri per 100 km. Ieri ho percorso 350 km con soli 20 litri, grazie a questo piccolo miracolo.</strong>
                </div>




                <div class="doctors-2 col-6">
                    <img class="pull-right" src="images/driver2.jpg" alt="">
                    <p class="style_cib19">Michele Lessano 46 anni
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>era</small> 9L<i class="slash"></i></span> /
                        <span class="text-orange"><small>è</small>  7,5L</span>
                    </div>
                    <hr>
                    <strong>Dacia. I risultati si vedono dopo il secondo rifornimento. Il motore diventa più silenzioso, senza vibrazioni. Ho notato un aumento delle prestazioni. Su tratti brevi risparmio da 1 a 1,5 litri. Sono soddisfatto.</strong>
                    <hr>
                </div>



                <div class="clear"></div>

                <div class="sub-title-2">
                    <p class="style_cib20">
                        Magnufuel può essere utilizzato su diversi tipi di mezzi<br>
                        <br>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib1">

                    <img class="circle-img" src="images/img_thumb.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Macchine</strong>
                    </p>
                </div>
                <div class="circle col-6 text-center sub-title-2" id="style_cib2">

                    <img class="circle-img" src="images/img_thumb2.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Camion</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib34">

                    <img class="circle-img" src="images/img_thumb3.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Bus e furgoni</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib4">

                    <img class="circle-img" src="images/img_thumb4.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Motoscafi e altri veicoli d'acqua</strong>
                    </p>
                </div>


                <div class="circle col-6 text-center sub-title-2" id="style_cib5">

                    <img class="circle-img" src="images/img_thumb5.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Motocicli</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib6">

                    <img class="circle-img" src="images/img_thumb6.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Macchinari agricoli</strong>
                    </p>
                </div>

                <div class="clear"></div>

                <div class="sub-title-2 text-center">
                    <span id="sert" class="anchor"></span>
                    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=auto, height=auto, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">

                        <img alt="" class="cert" src="images/CE_BIG_M.jpg">
                    </a>
                    <p class="style_cib23"><br> Ordina Magnufuel</p>
                </div>



                <button class="form-index rushOrder" > Ordina </button>
                <div class="phoneMobile">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <p class="ib_pho_text"> 390694804413</p>
                </div>

                <div class="style_cib25">
                    <img src="images/1_sticker.png" alt="IMG">
                    <img src="images/2_sticker.png" alt="IMG">
                </div>
                <p style="text-align:center">* Consigliato dalla European Automobile Manufacturers Association</p>


            </article>

            <div class="clear"></div>

        </section>
        <hr> <footer class="col-12 links">

            </footer>
    </div>






    <div id="popUp"></div>
    </div>
<?php include 'page2m.php';?>

    </body></html>


<?php } else { ?>


    <html><head>
        <meta charset="UTF-8">
        <title>Página web oficial | Magnufuel</title>
        <meta name="keywords" content="carburante risparmio economico gas carburante benzina risparmio carburante dispositivo di risparmio dispositivo per risparmiare diesel efficienza del carburante efficienza del motore tubo del carburante economia carburante risparmio carburante dispositivo di risparmio carburante magnete neodimio magnete riduzione emissioni di molecole di idrocarburi molecole di combustibile minor consumo basso consumo combustibile riduzione di CO riduzione di CH capacità del motore combustione completa">

        <meta name="description" content="Magnufuel dispositivo per risparmiare carburante">
        <link rel="stylesheet" href="!common_files/all.css" id="style_page1">
        <meta name="format-detection" content="telephone=no">

        <script src="!common_files/popup.js.php"></script>
         <script type="text/javascript" src="!common_files/jquery.min.js"></script>
        <script src="!common_files/timer.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {

                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '!common_files/styles.css') );

                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
    /**/
    </script>
    </head>
    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;" >
<div id="visible_first">

    <header class="header">
        <div class="container">
            <div class="col" id="style_ib1">
                <img src="images/logo.png" id="style_ib2" alt="logo">
                <img src="images/head_ico1.png" style="width:50px;    top: 14px;" id="style_ib3" alt="logo">

                <h2 class="style_ib1">Inizia a risparmiare <br>più del <strong class="bg-orange">20%</strong> di carburante oggi.</h2>
            </div>
            <img src="images/ezgif.com-crop.gif" class="pull-right" id="style_ib4" alt="">
            <img src="images/1_sticker.png" class="style_ib4" alt="">
            <img src="images/2_sticker.png" class="style_ib5" alt="">
            <p class="pull-right">Quando il carburante passa attraverso la forza del campo magnetico, le catene dei carboidrati si risolvono in componenti più semplici che vengono attivati di conseguenza.</p>
            <p class="style_ib6">* Consigliato dalla European Automobile Manufacturers Association</p>
        </div>
        <button type="button" class="btn order" id="head_button_ord"> Ordina </button>
    </header>
    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=500, height=707, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">
        <img alt="" id="sertificate" src="images/CE.jpg">
        <p>Fare clic per ingrandire</p>
    </a>
    <div id="popWindow" style="opacity: 0;">Rimangono soltanto <i>5</i> pacchetti a prezzo promozionale.</div>
    <!--popWindow-->
    <section class="main">
        <div class="row bg-inverse">
            <div class="container">
                <ul class="list-ico">
                    <li>
                        <i class="ico-horse"></i>
                        <p>Aumenta la capacità del motore di 5 Hp</p>
                    </li>
                    <li class="style_ib7">
                        <i class="ico-benz"></i>
                        <p class="style_ib8"> <br> Riduzione del consumo di carburante fino al 20%</p>
                    </li>
                    <li>
                        <i class="ico-lists"></i>
                        <p>Riduzione delle emissioni di CO e CH fino al 40% - 50%</p>
                    </li>
                    <li>
                        <i class="ico-bis"></i>
                        <p>Il dispositivo è certificato</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row bg1">
            <div class="container">
                <table>
                    <tbody>
                    <tr>

                        <td class="tovar">
                            <h4>Compra Magnufuel ora <strong class="text-red"> con uno sconto sul </strong> prezzo</h4>


                            <div class="eTimer">
                                <div class="countdown_box">
                                    <span class="order-countdown-hours" id="hours">00</span>
                                    <span>Ora</span>
                                    <span class="order-countdown-minutes" id="minutes">14</span>
                                    <span>Minuti</span>
                                    <span class="order-countdown-seconds" id="seconds">20</span>
                                    <span>Secondi</span>
                                </div>
                            </div>
                            <div class="row">
                                <button type="button"  class="btn order"> Ordina </button>

                                <br>
                                <br>
                                <div class="phoneMobile">
                                    <img src="images/phone.png" alt=""> 390694804413
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Come funziona Magnufuel?</h3>
                <article>
                    <img src="images/img_article.png" class="pull-left" alt="">
                    <p>Tutti i tipi di carburante, indipendentemente da dove sono venduti, cambiano in modo costante le sostanze, a causa dell'influenza che hanno temperature e umidità. Tale influenza estende e comprime anche il carburante. Le molecole di idrocarburi, base dei carburanti, iniziano a gravitare una vicina all'altra, formando gruppi molecolari, che non possono bruciare interamente perché parte di essi è fuori dalla zona di azione dell'ossigeno. Quando il carburante passa attraverso l'area in cui è installato Magnufuel le frequenze della risonanza magnetica generate dal dispositivo disperdono i gruppi in molecole singole, caricandole positivamente. In tal modo, l'ossigeno penetra in ogni molecola del carburante, permettendo una combustione totale della miscela aria-carburante.</p>
                    <p><br> Il risultato è un minor consumo di combustibile e una riduzione dell'emissioni pericolose. La parte principale del dispositivo Magnufuel è il magnete neodimio (NdFeB) composto di due parti. Fino ad oggi, questo effetto si può ottenere solo con i magneti di neodimio grazie ad una speciale tecnologia (Brevetto USA № 4.802.931, 4.496.395, 7.458.412, rilasciato dalla corporazione della "General Motors").</p>
                </article>
                <article>
                    <img src="images/product_magnu.jpg" class="style_ib10" alt="">
                    <p><br> I magneti di altri produttori (che sono mere imitazioni economiche) non hanno un tale effetto.</p>
                    <br><h2>Installazione</h2><br>L'installazione non richiede particolari conoscenze. La parte più importante consiste nel non sbagliare a scegliere il tubo. Il dispositivo deve essere installato sul tubo del carburante. <br>• Aprire il cofano della macchina, individuare il tubo del carburante e scegliere dove installare il dispositivo;<br>• Attaccare Magnufuel sul tubo del carburante. <br>• Per una maggiore stabilità si consiglia di utilizzare la fascetta di plastica inclusa nella confezione. La fascetta di plastica deve passare in un piccolo foro nel canale centrale del dispositivo. Stringerla saldamente e se necessario tagliare la fascetta avanzata;
                    <div class="text-center">
                        <button type="button"  class="btn order"> Ordina </button>


                        <div class="phoneMobile">
                            <img src="images/phone.png" alt=""> 390694804413
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <div class="row row-grey">
            <div class="container">
                <h3>Principali vantaggi</h3>
                <ul class="list">
                    <li>
						<span class="icon-circle"><i class="ico-plane"></i>
</span>Facile da installare. L'installazione del dispositivo richiedere 5 minuti.</li>
                    <li>
						<span class="icon-circle"><i class="ico-service"></i>
</span>Non produce gas di scarico, prolungando considerevolmente la vita delle fasce elastiche.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-watch"></i>
</span>Prolunga in modo significativo la vita del <br> catalizzatore.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-bisines_d"></i>
</span>Efficacia confermata.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-clock"></i>
</span>Con l'installazione di Magnufuel avverrà una più <br> completa combustione.</li>
                    <li>
						<span class="icon-circle"><i class="ico-sun"></i>
</span>Niente più spie del motore accese, principalmente dovute a una minore e incompleta combustione del combustibile.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-payment"></i>
</span>La spesa dell'acquisto di Magnufuel si recupera in 1-2 mesi!</li>
                </ul>
                <div class="row bg-img">


                    <button type="button"  class="btn order"> Ordine <br> </button>

                    <br>
                    <br>
                    <br>
                    <div class="phoneMobile">
                        <img src="images/phone.png" alt=""> 390694804413
                    </div>
                </div>
            </div>
        </div>
        <div class="row bg2">
            <div class="container">
                <h3>RECENSIONI DEI NOSTRI CLIENTI</h3>
                <div class="row-thumb">
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Andrea Giannini 32 anni</span>
                                <img src="images/driver3.jpg" class="img" alt="driver">
                                <span>Consumi del carburante prima e dopo l'installazione di Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">10L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">7L
<small>è</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Quattro giorni fa ho installato Magnufuel su una Hyundai Accent (2012). Di solito in città consumavo 10 litri, ora 7-8. Sull'autostrada ora solo 5,8 litri per 100 km. Ieri ho percorso 350 km con soli 20 litri, grazie a questo piccolo miracolo.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Michele Lessano 46 anni</span>
                                <img src="images/driver2.jpg" class="img" alt="driver">
                                <span>Consumi del carburante prima e dopo l'installazione di Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">9L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">7,5L
<small>è</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Dacia. I risultati si vedono dopo il secondo rifornimento. Il motore diventa più silenzioso, senza vibrazioni. Ho notato un aumento delle prestazioni. Su tratti brevi risparmio da 1 a 1,5 litri. Sono soddisfatto.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Patrizio Sani 29 anni</span>
                                <img src="images/driver.jpg" height="187" width="249" alt="driver" class="img">
                                <span>Consumi del carburante prima e dopo l'installazione di Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">17L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">12L
<small>è</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Lexus RX 350. Ho ordinato immediatamente due pezzi di Magnufuel e da come si può vedere e specialmente sentire, il motore funziona meglio. Un paio di volte mi è capitato che dopo aver fatto il pieno in due stazioni di benzina a caso ho avuto problemi (la spia del motore si è accesa). Ora non ha problemi. Non solo i combustibili di basso grado non causano problemi, ma il serbatoio dura più a lungo dopo il rifornimento.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Magnufuel può essere utilizzato su diversi tipi di mezzi<br></h3>
                <p class="text-center">Magnufuel il dispositivo per risparmiare carburante è molto noto in America e in Europa e ora è disponibile anche in <span class="country_name">Italia!</span></p>
                <ul class="thumb">
                    <li>
                        <img src="images/img_thumb.jpg" class="img" alt="automobil">
                        <h5>Macchine</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb2.jpg" class="img" alt="automobil">
                        <h5>Camion</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb3.jpg" class="img" alt="automobil">
                        <h5>Bus e furgoni</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb4.jpg" class="img" alt="automobil">
                        <h5>Motoscafi e altri veicoli d'acqua</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb5.jpg" class="img" alt="automobil">
                        <h5>Motocicli</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb6.jpg" class="img" alt="automobil">
                        <h5>Macchinari agricoli</h5>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row bg4">
            <div class="container">
                <div class="box-record">
                    <h3>Solo per oggi lo puoi comprare
                        <strong class="text-yellow"> Magnufuel</strong>
                        al <em class="text-yellow"> record </em>  di prezzo più basso!</h3>
                    <div class="bg-white">
                        <div class="eTimer">
                            <div class="countdown_box">
                                <span class="order-countdown-hours" id="hours_bottom">00</span>
                                <span>Ora</span>
                                <span class="order-countdown-minutes" id="minutes_bottom">14</span>
                                <span>Minuti</span>
                                <span class="order-countdown-seconds" id="seconds_bottom">20</span>
                                <span>Secondi</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="form-inverse country">

                    <h3><br> Ordina Magnufuel</h3> e comincia a risparmiare carburante oggi.
                    <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post">
                        <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                        <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                        <button class="form-index rushOrder" type="submit" id="orderib">controlla i prezzi</button>
                    </form>
                    <img src="images/1_sticker.png" class="style_ib11" alt="Guarante">

                    <img src="images/2_sticker.png" class="style_ib11" alt="Guarante">
                    <p class="style_ib12">* Consigliato dalla European Automobile Manufacturers Association</p>

                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">





        </div>
    </footer>







</div>

    <?php include 'page2.php';?>
    </body>


    </html>
<?php } ?>