
<html><head>
    <meta charset="UTF-8">
    <title>Sito Ufficiale | Magnufuel</title>
    <meta name="keywords" content="carburante risparmio economico gas carburante benzina risparmio carburante dispositivo di risparmio dispositivo per risparmiare diesel efficienza del carburante efficienza del motore tubo del carburante economia carburante risparmio carburante dispositivo di risparmio carburante magnete neodimio magnete riduzione emissioni di molecole di idrocarburi molecole di combustibile minor consumo basso consumo combustibile riduzione di CO riduzione di CH capacità del motore combustione completa">

    <meta name="description" content="Magnufuel dispositivo per risparmiare carburante">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="!common_files/all.css" id="style_page1">

    <link rel="stylesheet" type="text/css" href="!common_files/styles.css" id="style_page2">
    <script src="!common_files//time.js"></script>

    <script type="text/javascript" src="!common_files/jquery.min.js"></script>

    <script>

        var package_prices = {"1":{"price":80,"old_price":160,"shipment_price":0},"3":{"price":160,"old_price":320,"shipment_price":0},"5":{"price":240,"old_price":480,"shipment_price":0}};
        var shipment_price = 0;
        var name_hint = 'Миндов Стефан';
        var phone_hint = '+359897974512';

        $(document).ready(function() {

            $('body').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                ' | <a href="http://ac-feedback.com/report_form/">Report</a><br><a class="download" href="#">Download our Tips!</a></div>');


            $('.download').click(function(e) {
                e.preventDefault();  //stop the browser from following
                window.location.href = 'out_tips.pdf';
            });
        });
    </script>

</head>
<body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;" >
<div id="visible_first">

    <header class="header">
        <div class="container">
            <div class="col" id="style_ib1">
                 <h2 class="style_ib1">Fuel Savers
                 </h2>
                  </div>
            <p class="pull-right">Want Better Gas Mileage?
            </p>
            <p class="style_ib6">Want to Reduced Emissions?
            </p>
        </div>
        </header>
    <!--popWindow-->
    <section class="main">


        <div class="row">
            <div class="container">
                  <article>
                   <p>There is almost NOTHING that has a greater impact on your lifestyle than the cost of the fuel for our cars. With the price of fuel today it is costing the average driver more and more to drive their vehicle everyday. You are paying more at the pump and getting far less mileage for your money. The skyrocketing price of gasoline is making a huge impact on everyone, from the teenager who is just starting out, to the most elderly person who remembers the days when gas cost as little as 25 cents a gallon. You came here to learn, now’s the time to continue your fuel economy education.</p>

                       </article>
                <article>
                    <p>Save Cash On Fuel In 10 Simple Steps
                        You see them advertised on TV and in magazines (and now online). Dozens of products that promise to save you money and gas costs. They come in all forms; Gas additives, carburetor attachments, enhancers, cleaners and just about any other angle you can imagine. In this ebook you’ll get the real truth from someone who is not trying to sell you any of these products. Learn what works and why it does, and more importantly what type of products are a scam and why you should run away from them as fast as you can.

                        The Idle Effect - Saving 10% - 40% On Fuel Everyday
                        Did You Know There Is An Easy Way To Slash Your Gas Or Diesel Bills Without Modifying Or Changing Anything On Your Vehicle? The Idle Effect is a straight forward eBook with solutions to cutting gas and diesel costs within minutes of reading.
                        ENGINE WARM-UP
                        1. Avoid prolonged warming up of engine, even on cold mornings – 30 to 45 seconds is plenty of time.
                        2. Be sure the automatic choke is disengaged after engine warm up… chokes often get stuck, resulting in bad gas/air mixture.
                        3. Don’t start and stop engine needlessly. Idling your engine for one minute consumes the gas amount equivalent to when you start the engine.
                        4. Avoid “revving” the engine, especially just before you switch the engine off; this wastes fuel needlessly and washes oil down from the inside cylinder walls, owing to loss of oil pressure.
                        5. Eliminate jack-rabbit starts. Accelerate slowly when starting from dead stop. Don’t push pedal down more than 1/4 of the total foot travel. This allows carburetor to function at peak efficiency.

                        HOW TO DRIVE ECONOMICALLY
                        6.Exceeding 40 mph forces your auto to overcome tremendous wind resistance.
                        7. Never exceed legal speed limit. Primarily they are set for your traveling safety, however better gas efficiency also occurs. Traveling at 55 mph gives you up to 21% better mileage when compared to former legal speed limits of 65 mph and 70 mph.
                        8. Traveling at fast rates in low gears can consume up to 45% more fuel than is needed.
                        9. Manual shift driven cars allow you to change to highest gear as soon as possible, thereby letting you save gas if you “nurse it along”. However, if you cause the engine to “bog down”, premature wearing of engine parts occurs.
                        10. Keep windows closed when traveling at highway speeds. Open windows cause air drag, reducing your mileage by 10%
                        11. Drive steadily. Slowing down or speeding up wastes fuel. Also avoid tailgating – the driver in front of you is unpredictable. Not only is it unsafe, but if affects your economy, if he slows down unexpectedly.
                        12. Think ahead when approaching hills. If you accelerate, do it before you reach the hill, not while you’re on it.

                        GENERAL ADVICE
                        13. Do not rest left foot on floor board pedals while driving. The slightest pressure puts “mechanical drag” on components, wearing them down prematurely. This “dragging” also demands additional fuel usage.
                        14. Avoid rough roads whenever possible, because dirt or gravel rob you of up to 30% of your gas mileage.
                        15. Use alternate roads when safer, shorter, straighter. Compare traveling distance differences – remember that corners, curves and lane jumping requires extra gas. The shortest distance between two points is always straight.
                        16. Stoplights are usually timed for your motoring advantage. By traveling steadily at the legal speed limit you boost your chances of having the “green light” all the way.
                        17. Automatic transmissions should be allowed to cool down when your car is idling at a standstill, e.g. railroad crossings, long traffic lights, etc. Place gear into neutral position. This reduces transmission strain and allows transmission to cool.
                        18. Park car so that you can later begin to travel in forward gear; avoid reverse gear maneuvers to save gas.
                        19. Regular tune-ups ensure best economy; check owner’s manual for recommended maintenance intervals. Special attention should be given to maintaining clean air filters… diminished air flow increases gas waste.
                        20. Inspect suspension and chassis parts for occasional misalignment. Bent wheels, axles, bad shocks, broken springs, etc. create engine drag and are unsafe at high traveling speeds.
                        21. Remove snow tires during good weather seasons; traveling on deep tire tread really robs fuel!
                        22. Inflate all tires to maximum limit. Each tire should be periodically spun, balanced and checked for out-of-round. When shopping for new tires, get large diameter tires for rear wheels. Radial designs are the recognized fuel-savers; check manufacturer’s specifications for maximum tire pressures.
                        23. Remove vinyl tops – they cause air drag. Rough surfaces disturb otherwise smooth air flow around a car’s body.
                        Bear in mind when buying new cars that a fancy sun roof helps disturb smooth air flow (and mileage).
                        24. Auto air conditioners can reduce fuel economy by 10% to 20%. Heater fan, power windows and seats increase engine load; the more load on your engine, the less miles per gallon.
                        25. Remove excess weight from trunk or inside of car – extra tires, back seats, unnecessary heavy parts. Extra weight reduces mileage, especially when driving up inclines.
                        26. Car pools reduce travel monotony and gas expense – all riders chip in to help you buy. Conversation helps to keep the driver alert. Pooling also reduces traffic congestion, gives the driver easier maneuverability and greater “steady speed” economy. For best results, distribute passenger weight evenly throughout car.
                        27. During cold weather watch for icicles frozen to car frame. Up to 100 lbs. can be quickly accumulated!
                        Unremoved snow and ice cause tremendous wind resistance. Warm water thrown on (or hosed on) will eliminate it fast.

                        EXTRA TIPS
                        Install pressure regulator valve (sold in auto parts stores)… Use graphite motor oil… Beware of oil additives, regardless of advertising claims… Add Marvel Mystery Oil into gas fill-ups… Investigate fuel/water injection methods and products… combine short errands into one trip… Use special gas additives to prevent winter freezing of gas lines!
                    </p>
                </article>
            </div>
        </div>





    </section>

    <footer class="footer">

    </footer>

</div>


</body>


</html>