<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect();
?>

<?php if ($detect->isMobile()) { ?>


    <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Offizielle Webseite | Magnufuel</title>
        <meta name="keywords" content="Kraftstoff sparen Wirtschaftlichkeit Autogas Benzin Benzinsparer Spritsparer Gerät spritsparendes Gerät Diesel Effizienz Kraftstoffeffizienz Motor Kraftstoffschlauch Kraftstoffersparnis Kraftstoff sparendes Gerät Neodym-Magnet Magnet reduziert Emissionen Kohlenwasserstoffmoleküle Brennstoffmoleküle weniger Verbrauch niedriger Kraftstoffverbrauch Reduzierung von CO Reduzierung von CH Hubraum vollständige Verbrennung">

        <meta name="description" content="Magnufuel Kraftstoffsparendes Gerät">
        <l<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../../favicon.ico">

        <link rel="stylesheet" href="!common_files/styles_m.css" type="text/css">

        <script src="!common_files/jquery.js" type="text/javascript"></script>
        <script src="!common_files/navigation.js" type="text/javascript"></script>

        <script src="!common_files/popup_m.js.php"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('.glyphicon-shopping-cart').on('click',function(){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
            /**/
        </script>


    </head>

    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;">

<div id="visible_first">


    <header class="header">

        <div class="container">
            <div class="col">

                <h2 class="style_cib1">Comece a poupar</h2>
                <h3 class="style_cib2"> <strong> mais do que <strong class="bg-orange">20%</strong> combustível hoje.</strong></h3>
            </div>

        </div>
    </header>



    <div class="container">
        <nav>

            <img src="images/logo.png" alt="logo">
            <div class="pull-right shoppingCard">
                <a class="glyphicon glyphicon-shopping-cart" ></a>
            </div>

            <div class="clear"></div>
        </nav>
        <section class="col-12">
            <article>
                <div class="figure">
                    <img src="images/ezgif.com-crop.gif" class="product-img" alt="" id="style_cib33">

                    <img src="images/strelka_2.png" class="strelka2" alt="">
                </div>
                <div class="style_cib4">
                    <img src="images/1_sticker.png" alt="IMG" class="style_cib5">
                    <img src="images/2_sticker.png" alt="IMG" class="style_cib5"></div>
                <p class="style_cib6">* Recomendado por European Automobile Manufacturers Association</p>
                <p class="text-center">
                    Quando o combustível passa por um forte campo magnético, as correntes carbohidratadas passam a componentes mais simples, que são subsequentemente ativadas.
                </p>

                <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post" onsubmit="return template_validator({})">

                    <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                    <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                </form>

                <button class="form-index rushOrder"> Encomende </button>
                <div class="phoneMobile">
                    <i class="glyphicon glyphicon-earphone"></i>
                    <p class="ib_pho_text">351308808948</p>
                </div>
                <div class="text-center mblock2">
                    <div class="row bg-inverse">
                        <div class="container212">
                            <ul class="list-ico">
                                <li>
                                    <img src="images/horse.png" class="ico-horse" alt="">
                                    <p class="style_cib7">Aumente a capacidade do motor até 5 Hp </p>
                                </li>
                                <li>

                                    <img src="images/benz.png" class="ico-benz" alt="">
                                    <p class="style_cib8">  <br> Redução no consumo do combustível até 20%</p>
                                </li>
                                <li>

                                    <img src="images/list.png" class="ico-lists" alt="">
                                    <p class="style_cib9">Redução de emissões de CO e CH entre 40% a 50% </p>
                                </li>
                                <li>
                                    <img src="images/list_special.png" class="ico-bis" alt="">
                                    <p class="style_cib10">O dispositivo foi certificado</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="#sert" class="JUMP"><p>Clique para ver o certificado</p></a>

                <h3 class="text-center" id="style_cib11">
                    Como Magnufuel funciona?
                </h3>
                <div class="text-center">
                    <img src="images/new_logo2.jpg" class="style_cib2016" alt="img">
                </div>
                <p class="text-center" id="style_cib13">
                    Qualquer tipo de combustível, independentemente do local onde é armazenado, muda constantemente a sua substância devido à influência da temperatura e da humidade. Esta influência estende-se e comprime o combustível. As moléculas de hidrocarbonetos (a base de qualquer combustível) começam a gravitar ao lado de um ao outro de forma а que os grupos moleculares "Coágulos ou moléculas". Os "coágulos" não pode queimar completamente, porque parte deles estão fora do alcance da zona de oxigênio. Quando o combustível passa através da zona onde Magnufuel é instalado, a frequência de ressonância magnética gerada pelo dispositivo dissipa os "coágulos" formados pelas moléculas individuais de carregamento positivas. Deste modo, as moléculas de oxigénio penetram em todas as moléculas do combustível, o que ajuda a queimar completamente o combustível e ar ou a mistura.
                    <br> <br> O resultado é um consumo menor de combustível e redução das emissões prejudiciais. A parte principal do dispositivo Magnufuel é o íman neodimio (NdFeB) consistente em duas partes. Up to date, this effect can have only group of neodymium magnets made by special technology (EUA Patente № 4.802.931, 4.496.395, 7.458.412, entregue pela corporação de "General Motors“).
                    <br> <br> Ímanes de outros fabricantes (para não falar de que se tratam de uma imitação rasca) não conseguem dar tal efeito.
                    <br>
                </p><div class="style_cib14">
                    <br><h1> Instalação</h1> <br> A instalação não requer nenhuma habilidade especial. A parte mais importante é não cometer nenhum erro na escolha da mangueira. O dispositivo deve ser conectado na mangueira de combustível. <br>• Abra o capô de seu carro, encontre a mangueira de combustível e escolha um local onde deseja instalar o dispositivo;<br>• Anexe Magnufuel no local escolhido na mangueira de combustível. A mangueira situar-se-á entre as duas partes do dispositivo. <br>• Para obter mais estabilidade use o que está incluído no pacote. Note-se que a gola de plástico passa através de um pequeno orifício no canal central do dispositivo. Aperte-o e, se for necessário corte fora a parte extra do colar de plástico;
                </div>
                <img class="style_cib26" src="images/ezgif.com-crop.gif" alt="">

                <a href="#sert" class="JUMP"><p>Clique para ver o certificado</p></a>

                <div class="style_cib24">
                    <button class="form-index rushOrder" > Encomende </button>
                </div>
                <div class="phoneMobile">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <p class="ib_pho_text">351308808948</p>
                </div>
                <h2 class="style_cib15">Vantagens Principais</h2>
                <div class="magnit col-4">

                </div>
                <br>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_1.jpg" alt="">
                    <p class="magnit-text">Fácil de instalar. Instalar o aparelho dura apenas cinco minutos.</p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_2.jpg" alt="">
                    <p class="magnit-text">Evita o desperdício de combústivel. Isto significa que  estende consideravelmente a vida dos pistões. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_3.jpg" alt="">
                    <p class="magnit-text">Estende significativamente a vida do  conversor catalizador. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_4.jpg" alt="">
                    <p class="magnit-text">Eficácia confirmada. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_5.jpg" alt="">
                    <p class="magnit-text">Rapidamente após a instalação Magnufuel existe uma maior e mais completa combustão.</p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_6.jpg" alt="">
                    <p class="magnit-text">Não haverá mais o sinal «Check Engine», que responde principalmente  ao baixo valor de combustível e à incompleta combustão. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_7.jpg" alt="">
                    <p class="magnit-text">A quantia gasta na compra de Magnufuel é recuperada em 1 a 2 meses!</p>
                </div>

                <div class="clear"></div>

                <div class="text-center">
                    <a href="#sert" class="JUMP"><p>Clique para ver o certificado</p></a>
                    <button class="form-index rushOrder" > Encomende </button>
                    <div class="phoneMobile">
                        <span class="glyphicon glyphicon-earphone"></span>
                        <p class="ib_pho_text">351308808948</p>
                    </div>

                </div>
                <div class=" sub-title">
                    <p class="style_cib16">COMENTÁRIOS DOS NOSSOS CLIENTES</p>
                </div>

                <div class="doctors-2 col-6">
                    <img class="pull-left" src="images/driver3.jpg" alt="">
                    <p class="style_cib17"> André Antunes 32 anos
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>era </small>  10L<i class="slash"></i></span> /
                        <span class="text-orange"><small>é</small> 7L</span>
                    </div>
                    <hr>
                    <strong>Há quatro dias atrás instalei Magnufuel num Hyundai Accent (2012). Ema cidade, tinha um consumo de 10 litros, agora 7 a 8. Na autoestrada é de 5.8 l aos 100 km agora. Ontem fiz a viagem de Porto a Portimão por 20 litros. Graças a este pequeno milagre.</strong>
                </div>




                <div class="doctors-2 col-6">
                    <img class="pull-right" src="images/driver2.jpg" alt="">
                    <p class="style_cib19">Miguel Lemos 46 anos
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>era</small> 9L<i class="slash"></i></span> /
                        <span class="text-orange"><small>é</small>  7.5L</span>
                    </div>
                    <hr>
                    <strong>Dacia. O resultado veio depois do segundo depósito. O motor começou a funcionar silenciosamente, livre de vibrações e com uma maior performance. Quando faço trajetos pequenos poupo entre 1 a 1.5 litros. Estou satisfeito.</strong>
                    <hr>
                </div>



                <div class="clear"></div>

                <div class="sub-title-2">
                    <p class="style_cib20">
                        Magnufuel pode ser usado em diferentes tipos de transportes <br>
                        <br>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib1">

                    <img class="circle-img" src="images/img_thumb.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Automóveis</strong>
                    </p>
                </div>
                <div class="circle col-6 text-center sub-title-2" id="style_cib2">

                    <img class="circle-img" src="images/img_thumb2.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Camiões</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib34">

                    <img class="circle-img" src="images/img_thumb3.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Autocarros e Carrinhas</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib4">

                    <img class="circle-img" src="images/img_thumb4.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Barcos a motor e veículos aquáticos</strong>
                    </p>
                </div>


                <div class="circle col-6 text-center sub-title-2" id="style_cib5">

                    <img class="circle-img" src="images/img_thumb5.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Motas</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib6">

                    <img class="circle-img" src="images/img_thumb6.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Maquinaria agrícola</strong>
                    </p>
                </div>

                <div class="clear"></div>

                <div class="sub-title-2 text-center">
                    <span id="sert" class="anchor"></span>
                    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=auto, height=auto, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">

                        <img alt="" class="cert" src="images/CE_BIG_M.jpg">
                    </a>
                    <p class="style_cib23"><br> Encomende Magnufuel</p>
                </div>



                <button class="form-index rushOrder" > Encomende </button>
                <div class="phoneMobile">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <p class="ib_pho_text">351308808948</p>
                </div>

                <div class="style_cib25">
                    <img src="images/1_sticker.png" alt="IMG">
                    <img src="images/2_sticker.png" alt="IMG">
                </div>
                <p style="text-align:center">* Recomendado por European Automobile Manufacturers Association</p>


            </article>

            <div class="clear"></div>

        </section>
        <hr> <footer class="col-12 links">

        </footer>
    </div>






    <div id="popUp" style="display: block; right: 13.408296658701477px;">No momento estão 235 visitantes neste site.</div>
    </div>
<?php include 'page2m.php';?>

    </body></html>


<?php } else { ?>


    <html><head>
        <meta charset="UTF-8">
        <title>Offizielle Webseite | Magnufuel</title>
        <meta name="keywords" content="Kraftstoff sparen Wirtschaftlichkeit Autogas Benzin Benzinsparer Spritsparer Gerät spritsparendes Gerät Diesel Effizienz Kraftstoffeffizienz Motor Kraftstoffschlauch Kraftstoffersparnis Kraftstoff sparendes Gerät Neodym-Magnet Magnet reduziert Emissionen Kohlenwasserstoffmoleküle Brennstoffmoleküle weniger Verbrauch niedriger Kraftstoffverbrauch Reduzierung von CO Reduzierung von CH Hubraum vollständige Verbrennung">

        <meta name="description" content="Magnufuel Kraftstoffsparendes Gerät">
        <link rel="stylesheet" href="!common_files/all.css" id="style_page1">

        <meta name="format-detection" content="telephone=no">

        <script src="!common_files/popup.js.php"></script>
         <script type="text/javascript" src="!common_files/jquery.min.js"></script>
        <script src="!common_files/timer.js"></script>
        <script src="!common_files/timer.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {

                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '!common_files/styles.css') );

                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
    /**/
    </script>
    </head>
    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;" >
<div id="visible_first">



    <header class="header">
        <div class="container">
            <div class="col" id="style_ib1">
                <img src="images/logo.png" id="style_ib2" alt="logo">
                <img src="images/PT.png" id="style_ib3" alt="logo" style="width:60px;top:20px;">
                <div>
                <h2 class="style_ib2">Comece a poupar <br> mais do que <strong class="bg-orange">20%</strong> combustível hoje.</h2>
                    </div>
            </div>
            <img src="images/ezgif.com-crop.gif" class="pull-right" id="style_ib4" alt="">
            <img src="images/1_sticker.png" class="style_ib4" alt="">
            <img src="images/2_sticker.png" class="style_ib5" alt="">
            <p class="pull-right">Quando o combustível passa por um forte campo magnético, as correntes carbohidratadas passam a componentes mais simples, que são subsequentemente ativadas.</p>
            <p class="style_ib6">* Recomendado por European Automobile Manufacturers Association</p>
        </div>
        <button type="button"  class="btn order" id="head_button_ord"> Encomende </button>
    </header>
    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=500, height=707, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">
        <img alt="" id="sertificate" src="images/CE.jpg">
        <p>Clique para aumentar</p>
    </a>
    <div id="popWindow" style="opacity: 0.3300000000000001;">Existem apenas <i>3</i> produtos a preço de desconto.</div>
    <!--popWindow-->
    <section class="main">
        <div class="row bg-inverse">
            <div class="container">
                <ul class="list-ico">
                    <li>
                        <i class="ico-horse"></i>
                        <p>Aumente a capacidade do motor até 5 Hp</p>
                    </li>
                    <li class="style_ib7">
                        <i class="ico-benz"></i>
                        <p class="style_ib8"> <br> Redução no consumo do combustível até 20%</p>
                    </li>
                    <li>
                        <i class="ico-lists"></i>
                        <p>Redução de emissões de CO e CH entre 40% a 50%</p>
                    </li>
                    <li>
                        <i class="ico-bis"></i>
                        <p>O dispositivo foi certificado</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row bg1">
            <div class="container">
                <table>
                    <tbody>
                    <tr>

                        <td class="tovar">
                            <h4>Compre Magnufuel agora <strong class="text-red"> a um preço </strong> reduzido</h4>


                            <div class="eTimer">
                                <div class="countdown_box">
                                    <span class="order-countdown-hours" id="hours"></span>
                                    <span>Horas</span>
                                    <span class="order-countdown-minutes" id="minutes"></span>
                                    <span>Minutos</span>
                                    <span class="order-countdown-seconds" id="seconds"></span>
                                    <span>Segundos</span>
                                </div>
                            </div>
                            <div class="row">
                                <button type="button"  class="btn order"> Encomende </button>

                                <br>
                                <br>
                                <div class="phoneMobile">
                                    <img src="images/phone.png" alt="">351308808948
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Como Magnufuel funciona?</h3>
                <article>
                    <img src="images/img_article.png" class="pull-left" alt="">
                    <p>Qualquer tipo de combustível, independentemente do local onde é armazenado, muda constantemente a sua substância devido à influência da temperatura e da humidade. Esta influência estende-se e comprime o combustível. As moléculas de hidrocarbonetos (a base de qualquer combustível) começam a gravitar ao lado de um ao outro de forma а que os grupos moleculares "Coágulos ou moléculas". Os "coágulos" não pode queimar completamente, porque parte deles estão fora do alcance da zona de oxigênio. Quando o combustível passa através da zona onde Magnufuel é instalado, a frequência de ressonância magnética gerada pelo dispositivo dissipa os "coágulos" formados pelas moléculas individuais de carregamento positivas. Deste modo, as moléculas de oxigénio penetram em todas as moléculas do combustível, o que ajuda a queimar completamente o combustível e ar ou a mistura.</p>
                    <p><br> O resultado é um consumo menor de combustível e redução das emissões prejudiciais. A parte principal do dispositivo Magnufuel é o íman neodimio (NdFeB) consistente em duas partes. Up to date, this effect can have only group of neodymium magnets made by special technology (EUA Patente № 4.802.931, 4.496.395, 7.458.412, entregue pela corporação de "General Motors“).</p>
                </article>
                <article>
                    <img src="images/product_magnu.jpg" class="style_ib10" alt="">
                    <p><br> Ímanes de outros fabricantes (para não falar de que se tratam de uma imitação rasca) não conseguem dar tal efeito.</p>
                    <br><h1> Instalação</h1> <br> A instalação não requer nenhuma habilidade especial. A parte mais importante é não cometer nenhum erro na escolha da mangueira. O dispositivo deve ser conectado na mangueira de combustível. <br>• Abra o capô de seu carro, encontre a mangueira de combustível e escolha um local onde deseja instalar o dispositivo;<br>• Anexe Magnufuel no local escolhido na mangueira de combustível. A mangueira situar-se-á entre as duas partes do dispositivo. <br>• Para obter mais estabilidade use o que está incluído no pacote. Note-se que a gola de plástico passa através de um pequeno orifício no canal central do dispositivo. Aperte-o e, se for necessário corte fora a parte extra do colar de plástico;
                    <div class="text-center">
                        <button type="button" class="btn order"> Encomende </button>


                        <div class="phoneMobile">
                            <img src="images/phone.png" alt="">351308808948
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <div class="row row-grey">
            <div class="container">
                <h3>Vantagens Principais</h3>
                <ul class="list">
                    <li>
						<span class="icon-circle"><i class="ico-plane"></i>
</span>Fácil de instalar. Instalar o aparelho dura apenas cinco minutos.</li>
                    <li>
						<span class="icon-circle"><i class="ico-service"></i>
</span>Evita o desperdício de combústivel. Isto significa que  estende consideravelmente a vida dos pistões.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-watch"></i>
</span>Estende significativamente a vida do  conversor catalizador.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-bisines_d"></i>
</span>Eficácia confirmada.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-clock"></i>
</span>Rapidamente após a instalação Magnufuel existe uma maior e mais completa combustão.</li>
                    <li>
						<span class="icon-circle"><i class="ico-sun"></i>
</span>Não haverá mais o sinal «Check Engine», que responde principalmente  ao baixo valor de combustível e à incompleta combustão.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-payment"></i>
</span>A quantia gasta na compra de Magnufuel é recuperada em 1 a 2 meses!</li>
                </ul>
                <div class="row bg-img">


                    <button type="button" class="btn order"> Encomende <br> </button>

                    <br>
                    <br>
                    <br>
                    <div class="phoneMobile">
                        <img src="images/phone.png" alt="">351308808948
                    </div>
                </div>
            </div>
        </div>
        <div class="row bg2">
            <div class="container">
                <h3>COMENTÁRIOS DOS NOSSOS CLIENTES</h3>
                <div class="row-thumb">
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">André Antunes 32 anos</span>
                                <img src="images/driver3.jpg" class="img" alt="driver">
                                <span>Consumo de combustível antes e depois da instalação de Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">10L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">7L
<small>é</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Há quatro dias atrás instalei Magnufuel num Hyundai Accent (2012). Ema cidade, tinha um consumo de 10 litros, agora 7 a 8. Na autoestrada é de 5.8 l aos 100 km agora. Ontem fiz a viagem de Porto a Portimão por 20 litros. Graças a este pequeno milagre.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Miguel Lemos 46 anos</span>
                                <img src="images/driver2.jpg" class="img" alt="driver">
                                <span>Consumo de combustível antes e depois da instalação de Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">9L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">7.5L
<small>é</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Dacia. O resultado veio depois do segundo depósito. O motor começou a funcionar silenciosamente, livre de vibrações e com uma maior performance. Quando faço trajetos pequenos poupo entre 1 a 1.5 litros. Estou satisfeito.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Pedro Silva 29 anos</span>
                                <img src="images/driver.jpg" height="187" width="249" alt="driver" class="img">
                                <span>Consumo de combustível antes e depois da instalação de Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">17L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">12L
<small>é</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Lexus RX 350. Encomendei imediatamente 2 dispositivos Magnufuel e posso ver mas principalmente ouvir o motor a funcionar melhor. Por duas vezes aconteceu-me encher com combustível low cost e tive problemas (a luz do motor começou a acender).E agora está tudo bem. Não só os combustíveis de baixo custo não causam problemas, mas também o meu depósito dura muito mais tempo depois de abastecer.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Magnufuel pode ser usado em diferentes tipos de transportes <br></h3>
                <p class="text-center">Magnufuel Poupança de combustível é muito popular nos EUA e Europa e, agora pode compra-los aqui em <span class="country_name">Portugal!</span></p>
                <ul class="thumb">
                    <li>
                        <img src="images/img_thumb.jpg" class="img" alt="automobil">
                        <h5>Automóveis</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb2.jpg" class="img" alt="automobil">
                        <h5>Camiões</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb3.jpg" class="img" alt="automobil">
                        <h5>Autocarros e Carrinhas</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb4.jpg" class="img" alt="automobil">
                        <h5>Barcos a motor e veículos aquáticos</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb5.jpg" class="img" alt="automobil">
                        <h5>Motas</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb6.jpg" class="img" alt="automobil">
                        <h5>Maquinaria agrícola</h5>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row bg4">
            <div class="container">
                <div class="box-record">
                    <h3>Apenas hoje pode comprar
                        <strong class="text-yellow"> Magnufuel</strong>
                        por <em class="text-yellow"> um surprendente </em>  preço baixo!</h3>
                    <div class="bg-white">
                        <div class="eTimer">
                            <div class="countdown_box">
                                <span class="order-countdown-hours" id="hours_bottom"></span>
                                <span>Horas</span>
                                <span class="order-countdown-minutes" id="minutes_bottom"></span>
                                <span>Minutos</span>
                                <span class="order-countdown-seconds" id="seconds_bottom"></span>
                                <span>Segundos</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="form-inverse country">

                    <h3><br> Encomende Magnufuel</h3> e comece a poupar combustível hoje.
                    <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post">
                        <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                        <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                        <button class="form-index rushOrder" type="submit" id="orderib">Confirme o preço</button>
                    </form>
                    <img src="images/1_sticker.png" class="style_ib11" alt="Guarante">

                    <img src="images/2_sticker.png" class="style_ib11" alt="Guarante">
                    <p class="style_ib12">* Recomendado por European Automobile Manufacturers Association</p>

                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">




        </div>
    </footer>









</div>

    <?php include 'page2.php';?>
    </body>


    </html>
<?php } ?>