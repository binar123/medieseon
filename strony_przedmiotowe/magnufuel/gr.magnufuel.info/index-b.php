<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect();
?>

<?php if ($detect->isMobile()) { ?>


    <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="συσκευή προφύλαξης εξοικονόμησης καυσίμων οικονομία βενζίνης αέριο βενζίνη προφύλαξη των καυσίμων συσκευή εξοικονόμησης καυσίμων diesel απόδοση καύσιμο κινητήρων σωλήνας καυσίμων οικονομία εξοικονόμηση καυσίμων συσκευή νεοδυμίου μαγνήτης μείωσης των εκπομπών υδρογονανθράκων μόρια καυσίμου χαμηλότερη κατανάλωση καυσίμων χαμηλή περιεκτικότητα μείωση της κατανάλωσης του CO του κινητήρα μείωσης της CH μηχανής πλήρη καύση του καυσίμου">
        <meta name="description" content="Magnufuel συσκευή εξοικονόμησης καυσίμων">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../../favicon.ico">

        <link rel="stylesheet" href="!common_files/styles_m.css" type="text/css">

        <script src="!common_files/jquery.js" type="text/javascript"></script>
        <script src="!common_files/navigation.js" type="text/javascript"></script>
        <title> Magnufuel</title>
        <script src="!common_files/popup_m.js.php"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('.glyphicon-shopping-cart').on('click',function(){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
            /**/
        </script>


    </head>

    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;">

<div id="visible_first">

   
    <header class="header">

        <div class="container">
            <div class="col">

                <h2 class="style_cib1">Ξεκινήστε να εξοικονομείτε</h2>
                <h3 class="style_cib2"> <strong> περισσότερο από <strong class="bg-orange">20%</strong> καύσιμα ΣΉΜΕΡΑ.</strong></h3>
            </div>

        </div>
    </header>



    <div class="container">
        <nav>

            <img src="images/logo.png" alt="logo">
            <div class="pull-right shoppingCard">
                <a class="glyphicon glyphicon-shopping-cart" ></a>
            </div>

            <div class="clear"></div>
        </nav>
        <section class="col-12">
            <article>
                <div class="figure">
                    <img src="images/ezgif.com-crop.gif" class="product-img" alt="" id="style_cib33">

                    <img src="images/strelka_2.png" class="strelka2" alt="">
                </div>
                <div class="style_cib4">
                    <img src="images/1_sticker.png" alt="IMG" class="style_cib5">
                    <img src="images/2_sticker.png" alt="IMG" class="style_cib5"></div>
                <p class="style_cib6">* Συνίσταται από την European Automobile Manufacturers Ασσοσιατιών</p>
                <p class="text-center">
                    Όταν το καύσιμο περνάει μέσα από το ισχυρό μαγνητικό πεδίο οι αλυσίδες υδατανθράκων του διαλύονται σε απλούστερα συστατικά τα οποία στη συνέχεια ενεργοποιούνται.
                </p>

                <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post" onsubmit="return template_validator({})">

                    <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                    <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                </form>

                <button class="form-index rushOrder" > Παραγγείλτε </button>
                <div class="phoneMobile">
                    <i class="glyphicon glyphicon-earphone"></i>
                    <p class="ib_pho_text">302111981372</p>
                </div>
                <div class="text-center mblock2">
                    <div class="row bg-inverse">
                        <div class="container212">
                            <ul class="list-ico">
                                <li>
                                    <img src="images/horse.png" class="ico-horse" alt="">
                                    <p class="style_cib7">Αύξηση της χωρητικότητας του κινητήρα έως 5 Hp </p>
                                </li>
                                <li>

                                    <img src="images/benz.png" class="ico-benz" alt="">
                                    <p class="style_cib8">  <br> Μείωση της κατανάλωσης καυσίμου έως και 20%</p>
                                </li>
                                <li>

                                    <img src="images/list.png" class="ico-lists" alt="">
                                    <p class="style_cib9">Μείωση των εκπομπών CO και CH μέχρι 40% - 50% </p>
                                </li>
                                <li>
                                    <img src="images/list_special.png" class="ico-bis" alt="">
                                    <p class="style_cib10">Η συσκευή έχει πιστοποιηθεί</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="#sert" class="JUMP"><p>Κάντε κλικ για να δείτε το Πιστοποιητικό</p></a>

                <h3 class="text-center" id="style_cib11">
                    Πως δουλεύει το Magnufuel;
                </h3>
                <div class="text-center">
                    <img src="images/new_logo2.jpg" class="style_cib2016" alt="img">
                </div>
                <p class="text-center" id="style_cib13">
                    Οποιοδήποτε είδος καυσίμου, ανεξάρτητα από το πού είναι αποθηκευμένο, αλλάζει συνεχώς την ουσία του, λόγω της επίδρασης της θερμοκρασίας και της υγρασίας. Η επιρροή αυτή επεκτείνεται και συμπιέζει τα καύσιμα. Τα μόρια υδρογονανθράκων (η βάση κάθε καυσίμου) αρχίζουν να κλίνουν το ένα δίπλα στο άλλο σε ένα τρόπο που σχηματίζει μοριακές ομάδες - "θρόμβοι των μορίων". Οι "θρόμβοι" δεν μπορούν να καούν εντελώς, γιατί μέρος από αυτούς είναι μακριά από τη ζώνη του οξυγόνου. Όταν το καύσιμο περνάει μέσα από τη ζώνη όπου έχει εγκατασταθεί το Magnufuel, η συχνότητα μαγνητικού συντονισμού που δημιουργείται από τη συσκευή διαλύει τους σχηματιζόμενους "θρόμβους" σε μεμονωμένα μόρια φορτίζοντάς τα θετικά. Με αυτό τον τρόπο τα μόρια του οξυγόνου διεισδύουν σε κάθε μόριο του καυσίμου, το οποίο βοηθά στην πλήρη καύση του μίγματος καυσίμου-αέρα.
                    <br> <br> Το αποτέλεσμα είναι χαμηλότερη κατανάλωση καυσίμων και μείωση των επιβλαβών εκπομπών. Το κύριο μέρος της συσκευής Magnufuel είναι ο μαγνήτης νεοδυμίου (NdFeB), που αποτελείται από δύο μέρη. Μέχρι σήμερα, αυτή την επίδραση μπορούν να την έχουν μόνο μία ομάδα μαγνητών κατασκευασμένοι μέσω ειδικής τεχνολογίας (Δίπλωμα Ευρεσιτεχνίας ΗΠΑ № 4.802.931, 4.496.395, 7.458.412, που εκδόθηκε από την εταιρεία "General Motors").
                    <br> <br> Μαγνήτες από άλλους κατασκευαστές (για να μην αναφέρουμε ότι είναι μια φτηνή απομίμηση), δεν μπορούν να δώσουν τέτοιο αποτέλεσμα.
                    <br>
                </p><div class="style_cib14">
                    <br><h1> Εγκατάσταση</h1> <br> Η εγκατάσταση δεν απαιτεί ιδιαίτερες δεξιότητες. Το πιο σημαντικό κομμάτι είναι να μην κάνετε κάποιο λάθος στην επιλογή του σωλήνα. Η συσκευή πρέπει να συνδέεται στο σωλήνα καυσίμου. <br> • Ανοίξτε το καπό του αυτοκινήτου σας, βρείτε το σωλήνα καυσίμου και επιλέξετε μια θέση όπου θέλετε να εγκαταστήσετε τη συσκευή. <br> • Συνδέστε το Magnafuel στην επιλεγμένη θέση στο σωλήνα καυσίμου. Ο εύκαμπτος σωλήνας πρέπει να είναι μεταξύ των δύο τμημάτων της συσκευής. <br> • Για μεγαλύτερη σταθερότητα χρησιμοποιήσετε το πλαστικό κολάρο που περιλαμβάνεται στη συσκευασία. Σημειώστε ότι το πλαστικό κολάρο περνά μέσα από μια μικρή οπή στο κεντρικό κανάλι της συσκευής. Σφίξτε το και αν είναι απαραίτητο κόψτε τα επιπλέον κομμάτια του πλαστικού κολλάρου.
                </div>
                <img class="style_cib26" src="images/ezgif.com-crop.gif" alt="">

                <a href="#sert" class="JUMP"><p>Κάντε κλικ για να δείτε το Πιστοποιητικό</p></a>

                <div class="style_cib24">
                    <button class="form-index rushOrder" > Παραγγείλτε </button>
                </div>
                <div class="phoneMobile">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <p class="ib_pho_text">302111981372</p>
                </div>
                <h2 class="style_cib15">Κύρια Πλεονεκτήματα</h2>
                <div class="magnit col-4">

                </div>
                <br>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_1.jpg" alt="">
                    <p class="magnit-text">Εύκολο στην εγκατάσταση. Η τοποθέτηση της συσκευής διαρκεί μόλις πέντε λεπτά.</p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_2.jpg" alt="">
                    <p class="magnit-text">Δεν δημιουργεί κανένα απαέριο. Αυτό σημαίνει ότι παρατείνει σημαντικά τη διάρκεια ζωής των δακτυλίων εμβόλου. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_3.jpg" alt="">
                    <p class="magnit-text">Επίσης, επεκτείνει σημαντικά τη διάρκεια ζωής του καταλυτικού μετατροπέα. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_4.jpg" alt="">
                    <p class="magnit-text">Επιβεβαιωμένη απόδοση. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_5.jpg" alt="">
                    <p class="magnit-text">Σύντομα μετά την εγκατάσταση του Magnafuel υπάρχει πολύ περισσότερη  πλήρης καύση.</p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_6.jpg" alt="">
                    <p class="magnit-text">Δεν υπάρχει πλέον σηματοδότηση "Έλεγχος Μηχανής", το οποίο ανταποκρίνεται κυρίως στα χαμηλά καύσιμα και την ατελή καύση. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_7.jpg" alt="">
                    <p class="magnit-text">Το ποσό που δαπανήθηκε για την αγορά του Magnafuel επιστρέφει για 1-2 μήνες!</p>
                </div>

                <div class="clear"></div>

                <div class="text-center">
                    <a href="#sert" class="JUMP"><p>Κάντε κλικ για να δείτε το Πιστοποιητικό</p></a>

                    <button class="form-index rushOrder" > Παραγγείλτε </button>
                    <div class="phoneMobile">
                        <span class="glyphicon glyphicon-earphone"></span>
                        <p class="ib_pho_text">302111981372</p>
                    </div>

                </div>
                <div class=" sub-title">
                    <p class="style_cib16">ΚΡΙΤΙΚΕΣ ΑΠΟ ΤΟΥΣ ΠΕΛΑΤΕΣ ΜΑΣ</p>
                </div>

                <div class="doctors-2 col-6">
                    <img class="pull-left" src="images/driver3.jpg" alt="">
                    <p class="style_cib17"> Βασίλης Καρανικόλας 32 χρονών
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>ήταν </small>  10L<i class="slash"></i></span> /
                        <span class="text-orange"><small>είναι</small> 7L</span>
                    </div>
                    <hr>
                    <strong>Πριν τέσσερις μέρες εγκατέστησα το Magnufuel σε ένα Hyundai Accent (2012). Μετά την πόλη είχα μια κατανάλωση 10 λίτρα, τώρα 7-8. Στον αυτοκινητόδρομο είναι 5,8 l ανά 100 km πλέον. Χθες έκανα τη διαδρομή Θεσσαλονίκη Λευκάδα με 20 λίτρα. Χάρη σε αυτό το μικρό θαύμα.</strong>
                </div>




                <div class="doctors-2 col-6">
                    <img class="pull-right" src="images/driver2.jpg" alt="">
                    <p class="style_cib19">Μιχάλης Λάππας 46 χρονών
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>ήταν</small> 9L<i class="slash"></i></span> /
                        <span class="text-orange"><small>είναι</small>  7.5L</span>
                    </div>
                    <hr>
                    <strong>Dacia. Το αποτέλεσμα ήρθε μετά τον δεύτερο ανεφοδιασμό. Ο κινητήρας άρχισε να δουλεύει πιο ήσυχα, χωρίς δονήσεις και με αυξημένη απόδοση. Όταν πηγαίνω για καμια μικρή βόλτα εξοικονομώ 1 έως 1,5 λίτρα. Είμαι ικανοποιημένος.</strong>
                    <hr>
                </div>



                <div class="clear"></div>

                <div class="sub-title-2">
                    <p class="style_cib20">
                        Το Magnufuel μπορεί να χρησιμοποιηθεί σε διαφορετικούς τύπους μεταφορών <br>
                        <br>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib1">

                    <img class="circle-img" src="images/img_thumb.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Αυτοκίνητα</strong>
                    </p>
                </div>
                <div class="circle col-6 text-center sub-title-2" id="style_cib2">

                    <img class="circle-img" src="images/img_thumb2.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Φορτηγά</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib34">

                    <img class="circle-img" src="images/img_thumb3.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Λεωφορεία και βαν</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib4">

                    <img class="circle-img" src="images/img_thumb4.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Μηχανοκίνητα οχήματα και άλλα οχήματα νερού</strong>
                    </p>
                </div>


                <div class="circle col-6 text-center sub-title-2" id="style_cib5">

                    <img class="circle-img" src="images/img_thumb5.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Μοτοσικλέτες</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib6">

                    <img class="circle-img" src="images/img_thumb6.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Γεωργικά μηχανήματα</strong>
                    </p>
                </div>

                <div class="clear"></div>

                <div class="sub-title-2 text-center">
                    <span id="sert" class="anchor"></span>
                    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=auto, height=auto, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">

                        <img alt="" class="cert" src="images/CE_BIG_M.jpg">
                    </a>
                    <p class="style_cib23"><br> Παραγγείλτε το Magnufuel</p>
                </div>



                <button class="form-index rushOrder" > Παραγγείλτε </button>
                <div class="phoneMobile">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <p class="ib_pho_text">302111981372</p>
                </div>

                <div class="style_cib25">
                    <img src="images/1_sticker.png" alt="IMG">
                    <img src="images/2_sticker.png" alt="IMG">
                </div>
                <p style="text-align:center">* Συνίσταται από την European Automobile Manufacturers Ασσοσιατιών</p>


            </article>

            <div class="clear"></div>

        </section>
        <hr> <footer class="col-12 links">

         </footer>
    </div>






    <div id="popUp" style="display: block; right: -0.0000000000000028729427326909663px;">Αυτή τη στιγμή υπάρχουν 217 επισκέπτες στη σελίδα.</div></body>
    </div>
<?php include 'page2m.php';?>

    </body></html>


<?php } else { ?>


    <html><head>
        <meta charset="UTF-8">
        <title>Επίσημη Ιστοσελίδα | Magnufuel</title>
        <meta name="keywords" content="συσκευή προφύλαξης εξοικονόμησης καυσίμων οικονομία βενζίνης αέριο βενζίνη προφύλαξη των καυσίμων συσκευή εξοικονόμησης καυσίμων diesel απόδοση καύσιμο κινητήρων σωλήνας καυσίμων οικονομία εξοικονόμηση καυσίμων συσκευή νεοδυμίου μαγνήτης μείωσης των εκπομπών υδρογονανθράκων μόρια καυσίμου χαμηλότερη κατανάλωση καυσίμων χαμηλή περιεκτικότητα μείωση της κατανάλωσης του CO του κινητήρα μείωσης της CH μηχανής πλήρη καύση του καυσίμου">
        <meta name="description" content="Magnufuel συσκευή εξοικονόμησης καυσίμων">
      <link rel="stylesheet" href="!common_files/all.css" id="style_page1">
        <meta name="format-detection" content="telephone=no">
        <script src="!common_files//time.js"></script>
        <script src="!common_files/popup.js.php"></script>
         <script type="text/javascript" src="!common_files/jquery.min.js"></script>

        <script src="!common_files/timer.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');


                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '!common_files/styles.css') );

                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
    /**/
    </script>
    </head>
    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;" >
<div id="visible_first">

    <header class="header">
        <div class="container">
            <div class="col" id="style_ib1">
                <img src="images/logo.png" id="style_ib2" alt="logo">
                <img src="images/GR.png" style="width:60px;" id="style_ib3" alt="logo">

                <h2 class="style_ib1">Ξεκινήστε να εξοικονομείτε <br> <div class="imagessss"></div>
               περισσότερο από <strong class="bg-orange">20%</strong> καύσιμα ΣΉΜΕΡΑ.</h2>

            </div>
            <img src="images/ezgif.com-crop.gif" class="pull-right" id="style_ib4" alt="">
            <img src="images/1_sticker.png" class="style_ib4" alt="">
            <img src="images/2_sticker.png" class="style_ib5" alt="">
            <p class="pull-right">Όταν το καύσιμο περνάει μέσα από το ισχυρό μαγνητικό πεδίο οι αλυσίδες υδατανθράκων του διαλύονται σε απλούστερα συστατικά τα οποία στη συνέχεια ενεργοποιούνται.</p>
            <p class="style_ib6">* Συνίσταται από την European Automobile Manufacturers Ασσοσιατιών</p>
        </div>
        <button type="button" class="btn order" id="head_button_ord"> Παραγγείλτε </button>
    </header>
    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=500, height=707, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">
        <img alt="" id="sertificate" src="images/CE.jpg">
        <p>Κάντε κλικ για μεγέθυνση</p>
    </a>
    <!--<img src="images/GR.png" class="new_logo_INDEX" alt="">-->
    <div id="popWindow" style="opacity: 0.6099999999999997;">Υπάρχουν μόνο <i>4</i> πακέτα σε τιμή έκπτωσης που έχουν μείνει.</div>
    <!--popWindow-->
    <section class="main">
        <div class="row bg-inverse">
            <div class="container">
                <ul class="list-ico">
                    <li>
                        <i class="ico-horse"></i>
                        <p>Αύξηση της χωρητικότητας του κινητήρα έως 5 Hp</p>
                    </li>
                    <li class="style_ib7">
                        <i class="ico-benz"></i>
                        <p class="style_ib8"> <br> Μείωση της κατανάλωσης καυσίμου έως και 20%</p>
                    </li>
                    <li>
                        <i class="ico-lists"></i>
                        <p>Μείωση των εκπομπών CO και CH μέχρι 40% - 50%</p>
                    </li>
                    <li>
                        <i class="ico-bis"></i>
                        <p>Η συσκευή έχει πιστοποιηθεί</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row bg1">
            <div class="container">
                <table>
                    <tbody>
                    <tr>

                        <td class="tovar">
                            <h4>Αγοράστε το Magnufuel τώρα <strong class="text-red"> σε μειωμένη </strong> τιμή</h4>


                            <div class="eTimer">
                                <div class="countdown_box">
                                    <span class="order-countdown-hours" id="hours"></span>
                                    <span>ώρες</span>
                                    <span class="order-countdown-minutes" id="minutes"></span>
                                    <span>Λεπτά</span>
                                    <span class="order-countdown-seconds" id="seconds"></span>
                                    <span>δευτερόλεπτα</span>
                                </div>
                            </div>
                            <div class="row">
                                <button type="button"  class="btn order"> Παραγγείλτε </button>

                                <br>
                                <br>
                                <div class="phoneMobile">
                                    <img src="images/phone.png" alt="">302111981372
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Πως δουλεύει το Magnufuel;</h3>
                <article>
                    <img src="images/img_article.png" class="pull-left" alt="">
                    <p>Οποιοδήποτε είδος καυσίμου, ανεξάρτητα από το πού είναι αποθηκευμένο, αλλάζει συνεχώς την ουσία του, λόγω της επίδρασης της θερμοκρασίας και της υγρασίας. Η επιρροή αυτή επεκτείνεται και συμπιέζει τα καύσιμα. Τα μόρια υδρογονανθράκων (η βάση κάθε καυσίμου) αρχίζουν να κλίνουν το ένα δίπλα στο άλλο σε ένα τρόπο που σχηματίζει μοριακές ομάδες - "θρόμβοι των μορίων". Οι "θρόμβοι" δεν μπορούν να καούν εντελώς, γιατί μέρος από αυτούς είναι μακριά από τη ζώνη του οξυγόνου. Όταν το καύσιμο περνάει μέσα από τη ζώνη όπου έχει εγκατασταθεί το Magnufuel, η συχνότητα μαγνητικού συντονισμού που δημιουργείται από τη συσκευή διαλύει τους σχηματιζόμενους "θρόμβους" σε μεμονωμένα μόρια φορτίζοντάς τα θετικά. Με αυτό τον τρόπο τα μόρια του οξυγόνου διεισδύουν σε κάθε μόριο του καυσίμου, το οποίο βοηθά στην πλήρη καύση του μίγματος καυσίμου-αέρα.</p>
                    <p><br> Το αποτέλεσμα είναι χαμηλότερη κατανάλωση καυσίμων και μείωση των επιβλαβών εκπομπών. Το κύριο μέρος της συσκευής Magnufuel είναι ο μαγνήτης νεοδυμίου (NdFeB), που αποτελείται από δύο μέρη. Μέχρι σήμερα, αυτή την επίδραση μπορούν να την έχουν μόνο μία ομάδα μαγνητών κατασκευασμένοι μέσω ειδικής τεχνολογίας (Δίπλωμα Ευρεσιτεχνίας ΗΠΑ № 4.802.931, 4.496.395, 7.458.412, που εκδόθηκε από την εταιρεία "General Motors").</p>
                </article>
                <article>
                    <img src="images/product_magnu.jpg" class="style_ib10" alt="">
                    <p><br> Μαγνήτες από άλλους κατασκευαστές (για να μην αναφέρουμε ότι είναι μια φτηνή απομίμηση), δεν μπορούν να δώσουν τέτοιο αποτέλεσμα.</p>
                    <br><h1> Εγκατάσταση</h1> <br> Η εγκατάσταση δεν απαιτεί ιδιαίτερες δεξιότητες. Το πιο σημαντικό κομμάτι είναι να μην κάνετε κάποιο λάθος στην επιλογή του σωλήνα. Η συσκευή πρέπει να συνδέεται στο σωλήνα καυσίμου. <br> • Ανοίξτε το καπό του αυτοκινήτου σας, βρείτε το σωλήνα καυσίμου και επιλέξετε μια θέση όπου θέλετε να εγκαταστήσετε τη συσκευή. <br> • Συνδέστε το Magnafuel στην επιλεγμένη θέση στο σωλήνα καυσίμου. Ο εύκαμπτος σωλήνας πρέπει να είναι μεταξύ των δύο τμημάτων της συσκευής. <br> • Για μεγαλύτερη σταθερότητα χρησιμοποιήσετε το πλαστικό κολάρο που περιλαμβάνεται στη συσκευασία. Σημειώστε ότι το πλαστικό κολάρο περνά μέσα από μια μικρή οπή στο κεντρικό κανάλι της συσκευής. Σφίξτε το και αν είναι απαραίτητο κόψτε τα επιπλέον κομμάτια του πλαστικού κολλάρου.
                    <div class="text-center">
                        <button type="button"  class="btn order"> Παραγγείλτε </button>


                        <div class="phoneMobile">
                            <img src="images/phone.png" alt="">302111981372
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <div class="row row-grey">
            <div class="container">
                <h3>Κύρια Πλεονεκτήματα</h3>
                <ul class="list">
                    <li>
						<span class="icon-circle"><i class="ico-plane"></i>
</span>Εύκολο στην εγκατάσταση. Η τοποθέτηση της συσκευής διαρκεί μόλις πέντε λεπτά.</li>
                    <li>
						<span class="icon-circle"><i class="ico-service"></i>
</span>Δεν δημιουργεί κανένα απαέριο. Αυτό σημαίνει ότι παρατείνει σημαντικά τη διάρκεια ζωής των δακτυλίων εμβόλου.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-watch"></i>
</span>Επίσης, επεκτείνει σημαντικά τη διάρκεια ζωής του καταλυτικού μετατροπέα.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-bisines_d"></i>
</span>Επιβεβαιωμένη απόδοση.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-clock"></i>
</span>Σύντομα μετά την εγκατάσταση του Magnafuel υπάρχει πολύ περισσότερη  πλήρης καύση.</li>
                    <li>
						<span class="icon-circle"><i class="ico-sun"></i>
</span>Δεν υπάρχει πλέον σηματοδότηση "Έλεγχος Μηχανής", το οποίο ανταποκρίνεται κυρίως στα χαμηλά καύσιμα και την ατελή καύση.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-payment"></i>
</span>Το ποσό που δαπανήθηκε για την αγορά του Magnafuel επιστρέφει για 1-2 μήνες!</li>
                </ul>
                <div class="row bg-img">


                    <button type="button" class="btn order"> Παραγγείλτε <br> </button>

                    <br>
                    <br>
                    <br>
                    <div class="phoneMobile">
                        <img src="images/phone.png" alt="">302111981372
                    </div>
                </div>
            </div>
        </div>
        <div class="row bg2">
            <div class="container">
                <h3>ΚΡΙΤΙΚΕΣ ΑΠΟ ΤΟΥΣ ΠΕΛΑΤΕΣ ΜΑΣ</h3>
                <div class="row-thumb">
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Βασίλης Καρανικόλας 32 χρονών</span>
                                <img src="images/driver3.jpg" class="img" alt="driver">
                                <span>Η κατανάλωση καυσίμου πριν και μετά την εγκατάσταση του Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">10L<i class="slash">/</i>
<small>ήταν</small>
</span>
                                    <span class="text-orange">7L
<small>είναι</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Πριν τέσσερις μέρες εγκατέστησα το Magnufuel σε ένα Hyundai Accent (2012). Μετά την πόλη είχα μια κατανάλωση 10 λίτρα, τώρα 7-8. Στον αυτοκινητόδρομο είναι 5,8 l ανά 100 km πλέον. Χθες έκανα τη διαδρομή Θεσσαλονίκη Λευκάδα με 20 λίτρα. Χάρη σε αυτό το μικρό θαύμα.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Μιχάλης Λάππας 46 χρονών</span>
                                <img src="images/driver2.jpg" class="img" alt="driver">
                                <span>Η κατανάλωση καυσίμου πριν και μετά την εγκατάσταση του Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">9L<i class="slash">/</i>
<small>ήταν</small>
</span>
                                    <span class="text-orange">7.5L
<small>είναι</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Dacia. Το αποτέλεσμα ήρθε μετά τον δεύτερο ανεφοδιασμό. Ο κινητήρας άρχισε να δουλεύει πιο ήσυχα, χωρίς δονήσεις και με αυξημένη απόδοση. Όταν πηγαίνω για καμια μικρή βόλτα εξοικονομώ 1 έως 1,5 λίτρα. Είμαι ικανοποιημένος.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Πέτρος Χρηστόπουλος</span>
                                <img src="images/driver.jpg" height="187" width="249" alt="driver" class="img">
                                <span>Η κατανάλωση καυσίμου πριν και μετά την εγκατάσταση του Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">17L<i class="slash">/</i>
<small>ήταν</small>
</span>
                                    <span class="text-orange">12L
<small>είναι</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Lexus RX 350. Παρήγγειλα αμέσως δύο κομμάτια Magnufuel και μπορώ να δω και να ακούσω κυρίως τον κινητήρα να λειτουργεί καλύτερα. Μια-δυο φορές μου συνέβη να βάλω βενζίνη σε ένα τυχαίο βενζινάδικο και να μου βγάλει πρόβλημα (το φωτάκι "Ελέγξτε τον κινητήρα" άρχισε να αναβοσβήνει). Και τώρα είναι εντάξει. Όχι μόνο τα χαμηλής ποιότητας καύσιμα δεν προκαλούν προβλήματα, αλλά και η βενζίνη διαρκεί πολύ περισσότερο μετά την επαναπλήρωση.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Το Magnufuel μπορεί να χρησιμοποιηθεί σε διαφορετικούς τύπους μεταφορών <br></h3>
                <p class="text-center">Η συσκευή εξοικόνομησης Magnufuel είναι πολύ δημοφιλής στις ΗΠΑ και τη Δυτική Ευρώπη και τώρα μπορείτε να την αγοράσετε και στην <span class="country_name">Ελλάδα!</span></p>
                <ul class="thumb">
                    <li>
                        <img src="images/img_thumb.jpg" class="img" alt="automobil">
                        <h5>Αυτοκίνητα</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb2.jpg" class="img" alt="automobil">
                        <h5>Φορτηγά</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb3.jpg" class="img" alt="automobil">
                        <h5>Λεωφορεία και βαν</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb4.jpg" class="img" alt="automobil">
                        <h5>Μηχανοκίνητα οχήματα και άλλα οχήματα νερού</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb5.jpg" class="img" alt="automobil">
                        <h5>Μοτοσικλέτες</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb6.jpg" class="img" alt="automobil">
                        <h5>Γεωργικά μηχανήματα</h5>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row bg4">
            <div class="container">
                <div class="box-record">
                    <h3>Μόνο σήμερα μπορείτε να αγοράσετε
                        <strong class="text-yellow"> Magnufuel</strong>
                        σε <em class="text-yellow"> ρεκόρ </em>  χαμηλής τιμής!</h3>
                    <div class="bg-white">
                        <div class="eTimer">
                            <div class="countdown_box">
                                <span class="order-countdown-hours" id="hours_bottom"></span>
                                <span>ώρες</span>
                                <span class="order-countdown-minutes" id="minutes_bottom"></span>
                                <span>Λεπτά</span>
                                <span class="order-countdown-seconds" id="seconds_bottom"></span>
                                <span>δευτερόλεπτα</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="form-inverse country">

                    <h3><br> Παραγγείλτε το Magnufuel</h3> και ξεκινήστε να εξοικονομείτε καύσιμα.
                    <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post">
                        <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                        <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                        <button class="form-index rushOrder" type="submit" id="orderib">ΕΛΕΓΞΤΕ ΤΙΜΕΣ</button>
                    </form>
                    <img src="images/1_sticker.png" class="style_ib11" alt="Guarante">

                    <img src="images/2_sticker.png" class="style_ib11" alt="Guarante">
                    <p class="style_ib12">* Συνίσταται από την European Automobile Manufacturers Ασσοσιατιών</p>

                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">





        </div>
    </footer>





</div>

    <?php include 'page2.php';?>
    </body>


    </html>
<?php } ?>