<div  id="step2"  style="display:none" >


    <div class="header" id="step2_ib">

        <div class="wrapper clearfix">
            <div class="step2 clearfix">

                <div class="holder-form">
                    <h1 class="center">ΞΕΚΙΝΉΣΤΕ ΝΑ ΕΞΟΙΚΟΝΟΜΕΊΤΕ ΠΕΡΙΣΣΌΤΕΡΟ ΑΠΌ <strong class="step2_ib1">20%!</strong>ΚΑΎΣΙΜΑ ΣΉΜΕΡΑ.
                        <img src="images/GR.png" class="new_logo4" alt="">

                    </h1>
                </div>


                <div class="col-6">
                    <img src="images/prod_s2.png" class="prod_s2" alt="">
                    <br>


                    <div class="priceBoxContainer">
                        <img src="images/2_sticker.png" class="new_logo" alt="">
                        <img src="images/1_sticker.png" class="new_logo2" alt="">
                        <table class="priceBox">
                            <tbody>
                            <tr>
                                <td class="text-right">Αρχική Τιμή:</td>
                                <td class="rPrice text-left lTh">
                                        <span class="bold" id="full_price" style="color:red">
                                        €&nbsp;48.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Τιμή:</td>
                                <td class="price text-left" style="color:green;font-size:24px">
                                        <span class="bold" id="promoTotal">
                                        €&nbsp;38.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Αποστολή:</td>
                                <td class="fShipping text-left">ΔΩΡΕΑΝ</td>
                            </tr>
                            <tr>
                                <td class="text-right">Εξοικονομείτε:</td>
                                <td class="uSave text-left" id="discount">€&nbsp;10.00</td>
                            </tr>
                            </tbody>
                        </table>
                        <select name="priceSelect" class="priceSelect">
                            <option value="0">Ποσότητα: 1</option>
                            <option value="1">Ποσότητα: 2</option>
                            <option value="2">Ποσότητα: 3</option>
                        </select>
                        <p class="more_text_privacy">* Συνίσταται από την European Automobile Manufacturers Ασσοσιατιών</p>
                    </div>
                </div>

                <br>

                <div class="col-6">

                    <form name="orderform" id="orderform" action="" method="post" >
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAI_FwAAAAAAAAAAAASaFRrAAA"></iframe>

                        <!--<input type="hidden" value="0" id="product_selection" name="product_selection">
                        <input type="hidden" value="On" id="sameshipping" name="sameshipping">
                        <label for="billing_fname">Όνομα</label>
                        <input id="billing_fname" type="text" name="billing_fname" placeholder="Όνομα">
                        <label for="billing_fname">Επίθετο</label>
                        <input id="billing_lname" type="text" name="billing_lname" placeholder="Επίθετο" value="">

                        <label for="billing_street">Διεύθυνση</label>
                        <input id="billing_street" type="text" name="billing_street" placeholder="Διεύθυνση" value="">

                        <label for="billing_number">Αριθμός Οδού</label>
                        <input id="billing_number" type="text" name="billing_number" placeholder="Αριθμός Οδού" value="">

                        <label for="billing_city">Πόλη</label>
                        <input id="billing_city" type="text" name="billing_city" placeholder="Πόλη" value="">


                        <label for="billing_zip">Ταχυδρομικός κώδικας</label>
                        <input id="billing_zip" type="text" name="billing_zip" placeholder="Ταχυδρομικός κώδικας" value="" autocomplete="off">

                        <label for="billing_email">Ηλεκτρονική διεύθυνση</label>
                        <input id="billing_email" placeholder="Ηλεκτρονική διεύθυνση" type="text" name="billing_email">

                        <label for="billing_phone">Τηλέφωνο</label>
                        <input id="billing_phone" type="text" name="billing_phone" placeholder="Τηλέφωνο" value="" autocomplete="off">

                        <label for="billing_country">Χώρα</label>
                        <select name="billing_country" id="billing_country">
                            <option value="GR" selected="selected">Ελλάδα</option>
                        </select>

                        <button class="rushOrder" type="submit">ΚΑΝΤΕ ΠΑΡΑΓΓΕΛΙΑ ΤΩΡΑ!</button>-->
                        <p class="some_info">Το ποσό που δαπανήθηκε για την αγορά του Magnafuel επιστρέφει για 1-2 μήνες!</p>
                    </form>

                </div>
            </div>

            <div class="step2_ib3">
                <div class="subblock-step2">
                    <img src="images/Success_Guarantee.png" alt="img" class="guaranteeImg">
                    <h4 class="step2_ib2">100% ΕΓΓΥΗΣΗ</h4>
                    <p>Στην MagnuFuel είμαστε σίγουροι για την ποιότητα των προϊόντων μας και το έργο που παράγουμε ώστε να τα ετοιμάσουμε για εσάς προκειμένου να εξοικονομήσετε χρήματα. Αν αντιμετωπίσετε κάποιο πρόβλημα κατά τη χρήση του προϊόντος που έχετε αγοράσει, μπορείτε να επωφεληθείτε από την εγγύηση επιστροφής χρημάτων.</p>
                </div>
            </div>

            <div class="wrapper clearfix">



                <div class="subblock-step2">
                    <img src="images/free_delivery_orange.png" alt="img" class="deliveryImg">
                    <h4 class="step2_ib2"> ΔΩΡΕΑΝ</h4>
                    <p>Προσφέρουμε δωρεάν παράδοση για να ελαχιστοποιήσουμε τα έξοδα σας! Μετά την παραλαβή της παραγγελίας σας το προϊόν θα σας παραδοθεί σε 1-3 εργάσιμες ημέρες, ανάλογα με τον τόπο παράδοσης. H παράδοση πραγματοποιείται κατά τη διάρκεια των κανονικών ωρών εργασίας, οπότε παρακαλούμε βεβαιωθείτε ότι εσείς ή ένας εκπρόσωπός σας είναι διαθέσιμος στη διεύθυνση που έχετε δώσει.</p>
                    <br>
                </div>

                <div class="subblock-step2">
                    <img src="images/payment-icon.png" alt="img" class="paymentImg">
                    <h4 class="step2_ib2">ΜΕΘΟΔΟΣ ΠΛΗΡΩΜΗΣ</h4>
                    <p>Η πληρωμή γίνεται με αντικαταβολή. Χωρίς ριψοκίνδυνες οικονομικές συναλλαγές ή πληρωμή εκ των προτέρων.</p>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!-- end of #wrapper -->



    <div class="border clearfix"></div>

    <div id="footer" class="clearfix">
        <div class="wrapper clearfix">
        </div>
    </div>

    <script type="text/javascript">
        jQuery(function($) {
            $('.priceSelect').on('change', function() {
                $('#product_selection').val($(this).val());
                $('#full_price').html(full_prices[$(this).val()]);
                $('#discount').html(discount[$(this).val()]);
                $('#promoTotal').html(prices[$(this).val()]);
            });
        });

    </script>

    <script type="text/javascript">
        var prices = [
            '&euro;&nbsp;38.00', '&euro;&nbsp;66.00', '&euro;&nbsp;89.00', '&euro;&nbsp;0.00'
        ];
        var quants = [
            '1', '2', '3', ''
        ];
        var full_prices = [
            '&euro;&nbsp;48.00', '&euro;&nbsp;96.00', '&euro;&nbsp;144.00', '&euro;&nbsp;0.00'
        ];
        var discount = [
            '&euro;&nbsp;10.00', '&euro;&nbsp;30.00', '&euro;&nbsp;55.00', '&euro;&nbsp;0.00'
        ];

    </script>

    <script type="text/javascript">
        $("#billing_address").on('focus', function() {
            $("body").append("<div class=\"tip\"><div class=\"toolTip\"><div class=\"toolTipArrow\"></div>Εισαγάγετε διεύθυνση!</div></div>");
            $(".tip").css({
                top: $(this).offset().top + $(".tip").height() - 3,
                left: $(this).offset().left
            });
        });
        $("#billing_address").on('keydown', function() {
            $(".tip").remove();
        });

        $("#billing_address").on('blur', function() {
            $(".tip").remove();
        });

    </script>












</div>
