<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect();
error_reporting(E_ALL);
ini_set('display_errors', 1);?>

<?php if ($detect->isMobile()) { ?>


    <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="economisire combustibil,  economie, benzina, petrol, gazolina, economisire, dispozitiv de economisire al combustibilului, dispozitiv economisire, eficienta diesel, eficienta combustibil, motor, furtun pentru combustibil, economisire combustibil, dispozitiv economisire benzina, magnet neodim, magnet,emisii reduse, molecule hidrocarbon, molecule combustibil,consum mai scazut, consum mai scazut de combustibil, reducere a CO, reducere a CH, capacitatea motorului, ardere completa">
        <meta name="description" content="Magnufuel dispozitiv de economisire al combustibilului">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../../favicon.ico">

        <link rel="stylesheet" href="!common_files/styles_m.css" type="text/css">

        <script src="!common_files/jquery.js" type="text/javascript"></script>
        <script src="!common_files/navigation.js" type="text/javascript"></script>
        <title> Magnufuel</title>
        <script src="!common_files/popup_m.js.php"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
            /**/
        </script>


    </head>

    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;">

<div id="visible_first">
    <header class="header">

        <div class="container">
            <div class="col">

                <h2 class="style_cib1">Incepe sa economisesti</h2>
                <h3 class="style_cib2"> <strong>mai mult decat <strong class="bg-orange">20%</strong> combustibil astazi.</strong></h3>
            </div>

        </div>
    </header>



    <div class="container">
        <nav>

            <img src="images/logo.png" alt="logo">
            <div class="pull-right shoppingCard">
                <a class="glyphicon glyphicon-shopping-cart go_to_cart" ></a>
            </div>

            <div class="clear"></div>
        </nav>
        <section class="col-12">
            <article>
                <div class="figure">
                    <img src="images_m/ezgif.com-crop.gif" class="product-img" alt="" id="style_cib33">

                    <img src="images_m/strelka_2.png" class="strelka2" alt="">
                </div>
                <div class="style_cib4">
                    <img src="images_m/1_sticker.png" alt="IMG" class="style_cib5">
                    <img src="images_m/2_sticker.png" alt="IMG" class="style_cib5"></div>
                <p class="style_cib6">* Recomandat de European Automobile Manufacturers Association</p>
                <p class="text-center">
                    Cand combustibilul trece prin puternicul camd magnetic lanturile sale de carbohidrati sunt descompuse in componente simple care sunt activate subsecvent.
                </p>

                <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post" onsubmit="return template_validator({})">

                    <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                    <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                </form>

                <button class="form-index rushOrder" > Comanda </button>

                <div class="text-center mblock2">
                    <div class="row bg-inverse">
                        <div class="container212">
                            <ul class="list-ico">
                                <li>
                                    <img src="images_m/horse.png" class="ico-horse" alt="">
                                    <p class="style_cib7">Creste capacitatea motorului pana la 5 Hp </p>
                                </li>
                                <li>

                                    <img src="images_m/benz.png" class="ico-benz" alt="">
                                    <p class="style_cib8">  <br> Diminuare a consumului de combustibil pana la 20%</p>
                                </li>
                                <li>

                                    <img src="images_m/list.png" class="ico-lists" alt="">
                                    <p class="style_cib9">Diminuare a emisiilor de CO si CH pana la 40% - 50% </p>
                                </li>
                                <li>
                                    <img src="images_m/list_special.png" class="ico-bis" alt="">
                                    <p class="style_cib10">Dispozitivul a fost certificat</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h3 class="text-center" id="style_cib11">
                    Cum functioneaza Magnufuel?
                </h3>
                <div class="text-center">
                    <img src="images_m/new_logo2.jpg" class="style_cib2016" alt="img">
                </div>
                <p class="text-center" id="style_cib13">
                    Orice tip de combustibil, indiferent unde este depozitat, isi schimba constant continutul datorita temperaturii si umiditatii. O astfel de influenta se extinde si comprima combustibilul. Moleculele de hidrocarbon (baza oricarui combustibil) incep sa graviteze una langa cealalta intr-un mod care formeaza grupuri moleculare- ‘’blocuri de molecule’’.”Blocurile” nu pot arde complet deoarece o parte din ele este in afara zonei de oxigen. Cand combustibilul trece prin zona unde Magnufuel  este instalat, frecventa cu rezonanta magnetica generata de dispozitiv risipeste “blocurile” formate in molecule individuale incarcandu-le pozitiv. Astfel moleculele de oxigen penetreaza in fiecare molecula de combustibil, lucru ce ajuta la arderea completa a combinatiei combustibil-aer.
                    <br> <br> Rezultatul este un consum mai scazut al combustibilului si reducerea emisiilor nocive. Partea principala a dispozitivului Magnufuel  este magnetul neodim (NdFeB) care are doua parti. Actualizat, acest efect poate avea doar grupuri de magneti neodim realizati cu o tehnologie speciala. (Certificat USA  № 4.802.931, 4.496.395, 7.458.412, eliberat de corporatia  "General Motors“).
                    <br> <br> Magnetii de la alti furnizori (sa nu mai mentionam ca reprezinta o imitatie ieftina) nu pot oferi un astfel de efect.
                    <br>
                </p><div class="style_cib14">
                    <br><h1> Instalare</h1> <br> Instalarea nu necesita abilitati speciale. Partea cea mai importanta este sa nu gresiti cand selectati furtunul. Dispozitivul trebuie atasat la furtunul pentru combustibil <br>• Deschideti capota masinii, gasiti furtunul pentru combustibil si alegeti un loc unde doriti sa instalati dispozitivul;<br>• Atasati Magnufuel in locul ales pe furtunul pentru combustibil. Furtunul trebuie sa fie intre cele doua parti ale dispozitivului. <br>• Pentru mai multa stabilitate folositi bratara din plastic care este inclusa in pachet. Observati ca bratara din plastic trece printr-o mica gaura in canalul central al dispozitivului. Strangeti mai tare iar daca e necesar taiati bucata suplimentara a bratarii de plastic;
                </div>
                <img class="style_cib26" src="images_m/ezgif.com-crop.gif" alt="">

                <br>
                <br>
                <div class="style_cib24">
                    <button class="form-index rushOrder"> Comanda </button>
                </div>

                <h2 class="style_cib15">Avantaje principale</h2>
                <div class="magnit col-4">

                </div>
                <br>
                <div class="magnit col-4">
                    <img class="pull-left" src="images_m/ico_1.jpg" alt="">
                    <p class="magnit-text">Usor de instalat. Instalarea dispozitivului dureaza doar cinci minute.</p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images_m/ico_2.jpg" alt="">
                    <p class="magnit-text">Nu genereaza gaze de ardere. Asta inseamna ca extinde considerabil durata inelelor pentru pistoane. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images_m/ico_3.jpg" alt="">
                    <p class="magnit-text">De asemenea extinde semnificativ durata de viata a convertorului catalitic. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images_m/ico_4.jpg" alt="">
                    <p class="magnit-text">Eficienta confirmata. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images_m/ico_5.jpg" alt="">
                    <p class="magnit-text">Imediat dupa instalarea Magnufuel  apare o combustie completa mult mai mare.</p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images_m/ico_6.jpg" alt="">
                    <p class="magnit-text">Nu vor mai exista semnalari «Verifica Motorul», care raspund in mare parte la combustibil putin si ardere incompleta </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images_m/ico_7.jpg" alt="">
                    <p class="magnit-text">Suma cheltuita pe achizitionarea Magnufuel reprezinta beneficii pe 1-2 luni</p>
                </div>

                <div class="clear"></div>

                <div class="text-center">
                    <button class="form-index rushOrder" > Comanda </button>


                </div>
                <div class=" sub-title">
                    <p class="style_cib16">PARERI DE LA CLIENTII NOSTRI</p>
                </div>

                <div class="doctors-2 col-6">
                    <img class="pull-left" src="images_m/driver3.jpg" alt="">
                    <p class="style_cib17"> Andrei Neagoe  32 ani
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>a fost </small>  10L<i class="slash"></i></span> /
                        <span class="text-orange"><small>este</small> 7L</span>
                    </div>
                    <hr>
                    <strong>Cu patru zile in urma am instalat Magnufuel   pe Hyundai Accent (2012). Dupa ce am mers prin oras aveam un consum de 10 litri, acum am intre 7 si 8. Acum pe autostrada consumul e de 5.8 l la 100 km. Ieri am parcurs ruta Iasi-Brasov cu 20 litri. Multumita acestui mic miracol.</strong>
                </div>




                <div class="doctors-2 col-6">
                    <img class="pull-right" src="images_m/driver2.jpg" alt="">
                    <p class="style_cib19">Daniel Pascal 46 years
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>a fost</small> 9L<i class="slash"></i></span> /
                        <span class="text-orange"><small>este</small>  7.5L</span>
                    </div>
                    <hr>
                    <strong>Dacia. Rezultatul a venit dupa realimentare. Motorul a inceput sa mearga mai silentios, fara vibratii si cu o performanta sporita. Cand merg la un drum scurt economisesc de la 1 la 1.5l. Sunt multumit.</strong>
                    <hr>
                </div>



                <div class="clear"></div>

                <div class="sub-title-2">
                    <p class="style_cib20">
                        Magnufuel poate fi folosit pe diverse tipuri de transport<br>
                        <br>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib1">

                    <img class="circle-img" src="images_m/img_thumb.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Masini</strong>
                    </p>
                </div>
                <div class="circle col-6 text-center sub-title-2" id="style_cib2">

                    <img class="circle-img" src="images_m/img_thumb2.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Camioane</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib34">

                    <img class="circle-img" src="images_m/img_thumb3.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Autobuze si rulote</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib4">

                    <img class="circle-img" src="images_m/img_thumb4.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Barci cu motor si alte vehicole acvatice</strong>
                    </p>
                </div>


                <div class="circle col-6 text-center sub-title-2" id="style_cib5">

                    <img class="circle-img" src="images_m/img_thumb5.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Motociclete</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib6">

                    <img class="circle-img" src="images_m/img_thumb6.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Masinarii agricole</strong>
                    </p>
                </div>

                <div class="clear"></div>

                <div class="sub-title-2 text-center">
                    <p class="style_cib23"><br> Comanda Magnufuel</p>
                </div>



                <button class="form-index rushOrder" > Comanda </button>


                <div class="style_cib25">
                    <img src="images_m/1_sticker.png" alt="IMG">
                    <img src="images_m/2_sticker.png" alt="IMG">
                </div>
                <p style="text-align:center">* Recomandat de European Automobile Manufacturers Association</p>


            </article>

            <div class="clear"></div>

        </section>
        <hr> <footer class="col-12 links">

             </footer>
    </div>






    <div id="popUp" style="display: block; right: -2.87294e-15px;">Momentan sunt 237 de vizitatori pe acest site.</div>
    </div>
<?php include 'page2m.php';?>

    </body></html>


<?php } else { ?>


    <html><head>
        <meta charset="UTF-8">
        <title>Website oficial | Magnufuel</title>
        <meta name="keywords" content="economisire combustibil,  economie, benzina, petrol, gazolina, economisire, dispozitiv de economisire al combustibilului, dispozitiv economisire, eficienta diesel, eficienta combustibil, motor, furtun pentru combustibil, economisire combustibil, dispozitiv economisire benzina, magnet neodim, magnet,emisii reduse, molecule hidrocarbon, molecule combustibil,consum mai scazut, consum mai scazut de combustibil, reducere a CO, reducere a CH, capacitatea motorului, ardere completa">
        <meta name="description" content="Magnufuel dispozitiv de economisire al combustibilului">
        <meta name="format-detection" content="telephone=no">
      <link rel="stylesheet" href="!common_files/all.css" id="style_page1">
        <script src="!common_files//time.js"></script>
        <script src="!common_files/popup.js.php"></script>
         <script type="text/javascript" src="!common_files/jquery.min.js"></script>
        <script src="!common_files/timer.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('.glyphicon-shopping-cart').on('click',function(){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '!common_files/styles.css') );

                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
    /**/
    </script>
    </head>
    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;" >
<div id="visible_first">

    <header class="header">
        <div class="container">
            <div class="col" id="style_ib1">
                <img src="images/logo.png" id="style_ib2" alt="logo">
                <img src="images/RO.png" style="width:60px;top:20px;" id="style_ib3" alt="logo">
                <h2 class="style_ib1">Incepe sa economisesti <br >mai mult decat <strong class="bg-orange">20%</strong> combustibil astazi.</h2>
            </div>
            <img src="images/ezgif.com-crop.gif" class="pull-right" id="style_ib4" alt="">
            <img src="images/1_sticker.png" class="style_ib4" alt="">
            <img src="images/2_sticker.png" class="style_ib5" alt="">
            <p class="pull-right">Cand combustibilul trece prin puternicul camd magnetic lanturile sale de carbohidrati sunt descompuse in componente simple care sunt activate subsecvent.</p>
            <p class="style_ib6">* Recomandat de European Automobile Manufacturers Association</p>
        </div>
        <button type="button"  class="btn order" id="head_button_ord"> Comanda </button>
    </header>
    <div id="popWindow" style="opacity: 1;"><i>5</i> pachete la preturi promotionale ramase!</div>
    <!--popWindow-->
    <section class="main">
        <div class="row bg-inverse">
            <div class="container">
                <ul class="list-ico">
                    <li>
                        <i class="ico-horse"></i>
                        <p>Creste capacitatea motorului pana la 5 Hp</p>
                    </li>
                    <li class="style_ib7">
                        <i class="ico-benz"></i>
                        <p class="style_ib8"> <br> Diminuare a consumului de combustibil pana la 20%</p>
                    </li>
                    <li>
                        <i class="ico-lists"></i>
                        <p>Diminuare a emisiilor de CO si CH pana la 40% - 50%</p>
                    </li>
                    <li>
                        <i class="ico-bis"></i>
                        <p>Dispozitivul a fost certificat</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row bg1">
            <div class="container">
                <table>
                    <tbody>
                    <tr>

                        <td class="tovar">
                            <h4>Cumpara Magnufuel acum <strong class="text-red"> pentru un pret </strong> redus</h4>


                            <div class="eTimer">
                                <div class="countdown_box">
                                    <span class="order-countdown-hours" id="hours"></span>
                                    <span>Ore</span>
                                    <span class="order-countdown-minutes" id="minutes"></span>
                                    <span>Minute</span>
                                    <span class="order-countdown-seconds" id="seconds"></span>
                                    <span>Secunde</span>
                                </div>
                            </div>
                            <div class="row">
                                <button type="button"    class="btn order"> Comanda </button>


                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Cum functioneaza Magnufuel?</h3>
                <article>
                    <img src="images/img_article.png" class="pull-left" alt="">
                    <p>Orice tip de combustibil, indiferent unde este depozitat, isi schimba constant continutul datorita temperaturii si umiditatii. O astfel de influenta se extinde si comprima combustibilul. Moleculele de hidrocarbon (baza oricarui combustibil) incep sa graviteze una langa cealalta intr-un mod care formeaza grupuri moleculare- ‘’blocuri de molecule’’.”Blocurile” nu pot arde complet deoarece o parte din ele este in afara zonei de oxigen. Cand combustibilul trece prin zona unde Magnufuel  este instalat, frecventa cu rezonanta magnetica generata de dispozitiv risipeste “blocurile” formate in molecule individuale incarcandu-le pozitiv. Astfel moleculele de oxigen penetreaza in fiecare molecula de combustibil, lucru ce ajuta la arderea completa a combinatiei combustibil-aer.</p>
                    <p><br> Rezultatul este un consum mai scazut al combustibilului si reducerea emisiilor nocive. Partea principala a dispozitivului Magnufuel  este magnetul neodim (NdFeB) care are doua parti. Actualizat, acest efect poate avea doar grupuri de magneti neodim realizati cu o tehnologie speciala. (Certificat USA  № 4.802.931, 4.496.395, 7.458.412, eliberat de corporatia  "General Motors“).</p>
                </article>
                <article>
                    <img src="images/product_magnu.jpg" class="style_ib10" alt="">
                    <p><br> Magnetii de la alti furnizori (sa nu mai mentionam ca reprezinta o imitatie ieftina) nu pot oferi un astfel de efect.</p>
                    <br><h1> Instalare</h1> <br> Instalarea nu necesita abilitati speciale. Partea cea mai importanta este sa nu gresiti cand selectati furtunul. Dispozitivul trebuie atasat la furtunul pentru combustibil <br>• Deschideti capota masinii, gasiti furtunul pentru combustibil si alegeti un loc unde doriti sa instalati dispozitivul;<br>• Atasati Magnufuel in locul ales pe furtunul pentru combustibil. Furtunul trebuie sa fie intre cele doua parti ale dispozitivului. <br>• Pentru mai multa stabilitate folositi bratara din plastic care este inclusa in pachet. Observati ca bratara din plastic trece printr-o mica gaura in canalul central al dispozitivului. Strangeti mai tare iar daca e necesar taiati bucata suplimentara a bratarii de plastic;
                    <div class="text-center">
                        <button type="button" class="btn order"> Comanda </button>


                    </div>
                </article>
            </div>
        </div>
        <div class="row row-grey">
            <div class="container">
                <h3>Avantaje principale</h3>
                <ul class="list">
                    <li>
						<span class="icon-circle"><i class="ico-plane"></i>
</span>Usor de instalat. Instalarea dispozitivului dureaza doar cinci minute.</li>
                    <li>
						<span class="icon-circle"><i class="ico-service"></i>
</span>Nu genereaza gaze de ardere. Asta inseamna ca extinde considerabil durata inelelor pentru pistoane.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-watch"></i>
</span>De asemenea extinde semnificativ durata de viata a convertorului catalitic.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-bisines_d"></i>
</span>Eficienta confirmata.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-clock"></i>
</span>Imediat dupa instalarea Magnufuel  apare o combustie completa mult mai mare.</li>
                    <li>
						<span class="icon-circle"><i class="ico-sun"></i>
</span>Nu vor mai exista semnalari «Verifica Motorul», care raspund in mare parte la combustibil putin si ardere incompleta

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-payment"></i>
</span>Suma cheltuita pe achizitionarea Magnufuel reprezinta beneficii pe 1-2 luni</li>
                </ul>
                <div class="row bg-img">


                    <button type="button"  class="btn order"> Comanda <br> </button>

                </div>
            </div>
        </div>
        <div class="row bg2">
            <div class="container">
                <h3>PARERI DE LA CLIENTII NOSTRI</h3>
                <div class="row-thumb">
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Andrei Neagoe  32 ani</span>
                                <img src="images/driver3.jpg" class="img" alt="driver">
                                <span>Consumul de combustibil inainte si dupa instalarea Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">10L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">7L
<small>este</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Cu patru zile in urma am instalat Magnufuel   pe Hyundai Accent (2012). Dupa ce am mers prin oras aveam un consum de 10 litri, acum am intre 7 si 8. Acum pe autostrada consumul e de 5.8 l la 100 km. Ieri am parcurs ruta Iasi-Brasov cu 20 litri. Multumita acestui mic miracol.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Daniel Pascal 46 years</span>
                                <img src="images/driver2.jpg" class="img" alt="driver">
                                <span>Consumul de combustibil inainte si dupa instalarea Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">9L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">7.5L
<small>este</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Dacia. Rezultatul a venit dupa realimentare. Motorul a inceput sa mearga mai silentios, fara vibratii si cu o performanta sporita. Cand merg la un drum scurt economisesc de la 1 la 1.5l. Sunt multumit.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Matei Munteanu 29 years</span>
                                <img src="images/driver.jpg" height="187" width="249" alt="driver" class="img">
                                <span>Consumul de combustibil inainte si dupa instalarea Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">17L<i class="slash">/</i>
<small>era</small>
</span>
                                    <span class="text-orange">12L
<small>este</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Lexus RX 350. Am comandat imediat doua bucati de Magnufuel  si se poate vedea si in special auzi ca motorul merge mai bine. De cateva ori mi s-a intamplat sa alimentez de la o benzinarie la intamplare si sa am probleme. ("Verifica motorul” a inceput sa se aprinda becul). Iar acum este in regula. Nu numai ca nivelul scazut de combustibil nu cauzeaza probleme dar de asemenea rezervorul meu dureaza mult mai mult dupa ce il umplu din nou.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Magnufuel poate fi folosit pe diverse tipuri de transport<br></h3>
                <p class="text-center">Magnufuel economizorul de combustibil este foarte cunoscut in US si Europa de Vest iar acum il puteti cumpara in <span class="country_name">ROMANIA!</span></p>
                <ul class="thumb">
                    <li>
                        <img src="images/img_thumb.jpg" class="img" alt="automobil">
                        <h5>Masini</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb2.jpg" class="img" alt="automobil">
                        <h5>Camioane</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb3.jpg" class="img" alt="automobil">
                        <h5> Autobuze si rulote</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb4.jpg" class="img" alt="automobil">
                        <h5>Barci cu motor si alte vehicole acvatice</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb5.jpg" class="img" alt="automobil">
                        <h5>Motociclete</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb6.jpg" class="img" alt="automobil">
                        <h5>Masinarii agricole</h5>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row bg4">
            <div class="container">
                <div class="box-record">
                    <h3>Doar astazi poti cumpara
                        <strong class="text-yellow"> Magnufuel</strong>
                        pentru <em class="text-yellow"> un pret mic </em>  incredibil!</h3>
                    <div class="bg-white">
                        <div class="eTimer">
                            <div class="countdown_box">
                                <span class="order-countdown-hours" id="hours_bottom"></span>
                                <span>Ore</span>
                                <span class="order-countdown-minutes" id="minutes_bottom"></span>
                                <span>Minute</span>
                                <span class="order-countdown-seconds" id="seconds_bottom"></span>
                                <span>Secunde</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="form-inverse country">

                    <h3><br> Comanda Magnufuel</h3> si incepe astazi sa economisesti combustibil.
                    <form name="form-index" class="form-inverse country" id="form-index" action="" method="post">
                        <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                        <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                        <button class="form-index rushOrder" type="submit" id="orderib">Verifica preturile</button>
                    </form>
                    <img src="images/1_sticker.png" class="style_ib11" alt="Guarante">

                    <img src="images/2_sticker.png" class="style_ib11" alt="Guarante">
                    <p class="style_ib12">* Recomandat de European Automobile Manufacturers Association</p>

                </div>
            </div>
        </div>
    </section>

    <footer class="footer">

    </footer>

</div>

    <?php include 'page2.php';?>
    </body>


    </html>
<?php } ?>