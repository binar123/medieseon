<div  id="step2"  style="display:none" >
    <link rel="stylesheet" href="!common_files/styles2_m.css" type="text/css">

    <div class="container">
    <nav>

        <img src="images/logo.png" alt="">

        <div class="clear"></div>
    </nav>
    <section class="col-12">

        <h1 class="center" style=" line-height: 1;overflow: hidden;font-weight: bold;      margin-bottom: -40px;  font-size: 28px;margin-top: 45px; text-align: center;">INCEPE SA ECONOMISESTI MAI MULT DECAT <strong style="display: inline-block;
    background: #cc1d00;
    color: #fff;
        padding: 0px 21px;"> 20%! </strong></h1>
        <div class="col-6" style="margin-top:140px">

            <div class="figure">
                <div class="figure">
                    <img src="images/new_logo.jpg" class="product-img" alt="">
                </div>
            </div>
            <div class="priceBoxContainer">
                <table class="priceBox">
                    <tbody><tr>
                        <td class="text-right">Pret Standard:</td>
                        <td class="price text-left lTh" id="full_price">220,00&nbsp;RON</td>
                    </tr>
                    <tr>
                        <td class="text-right"> Pret:</td>
                        <td class="rPrice text-left" id="promoTotal" style="font-size:21px;color:green;">172,00&nbsp;RON</td>
                    </tr>
                    <tr>
                        <td class="text-right">Livrare:</td>
                        <td class="fShipping text-left" style="color:black">GRATUIT</td>
                    </tr>
                    <tr>
                        <td class="text-right">Economisesti:</td>
                        <td class="uSave text-left" id="discount">48,00&nbsp;RON</td>
                    </tr>
                    </tbody></table>

                <div style="margin: 0px auto;     max-width: 280px; width: 77%; border: none;display: block;margin-right: auto;left: 36px;position: relative;">
                    <img src="images/1_sticker.png" alt="IMG" style="width:40%; right:22px;position:relative">

                    <img src="images/2_sticker.png" alt="IMG" style="width:40%; right:22px;position:relative">

                    <img src="images/RO.png" alt="IMG" style="width:40%; right:22px;position:relative">

                </div>
                <p style="text-align:center">* Recomandat de European Automobile Manufacturers Association</p>

                <select class="priceSelect">
                    <option value="0">Cantitate: 1</option>
                    <option value="1">Cantitate: 2</option>
                    <option value="2">Cantitate: 3</option>
                </select>
            </div>
            <hr>
        </div>
        <div class="col-6">
            <div class="formContainer">


                <form name="orderform" id="orderform" action="" method="post" >
                    <!--<input type="hidden" value="0" id="product_selection" name="product_selection">
                    <input type="hidden" value="On" id="sameshipping" name="sameshipping">
                    <label for="billing_fname">Prenume</label>
                    <input id="billing_fname" type="text" name="billing_fname" placeholder="Prenume">

                    <label for="billing_lname">Nume</label>
                    <input id="billing_lname" type="text" name="billing_lname" placeholder="Nume" value="">

                    <div class="backGrey">
                        <label for="billing_address">Adresa</label>
                        <input id="billing_address" type="text" name="billing_address" placeholder="Adresa" value="">
                        <p class="alert_message displayNone">Va rugam sa introduceti adresa completa unde veti putea primi comanda intre orele (09.00-18.00).</p>
                    </div>

                    <label for="billing_city">Oras</label>
                    <input id="billing_city" type="text" name="billing_city" placeholder="Oras" value="">

                    <label for="billing_state">Judet</label>
                    <select name="billing_state" id="billing_state" value="">
                        <option value="" selected="selected">Alege Judetul!</option>
                        <option value="Alba">Alba</option>
                        <option value="Arad">Arad</option>
                        <option value="Argeș">Argeș</option>
                        <option value="Bacău">Bacău</option>
                        <option value="Bihor">Bihor</option>
                        <option value="Bistrița-Năsăud">Bistrița-Năsăud</option>
                        <option value="Botoșani">Botoșani</option>
                        <option value="Brăila">Brăila</option>
                        <option value="Brașov">Brașov</option>
                        <option value="Bucharest">Bucharest</option>
                        <option value="Buzău">Buzău</option>
                        <option value="Călărași">Călărași</option>
                        <option value="Caraș-Severin">Caraș-Severin</option>
                        <option value="Cluj">Cluj</option>
                        <option value="Constanța">Constanța</option>
                        <option value="Covasna">Covasna</option>
                        <option value="Dâmbovița">Dâmbovița</option>
                        <option value="Dolj">Dolj</option>
                        <option value="Galați">Galați</option>
                        <option value="Giurgiu">Giurgiu</option>
                        <option value="Gorj">Gorj</option>
                        <option value="Harghita">Harghita</option>
                        <option value="Hunedoara">Hunedoara</option>
                        <option value="Ialomița">Ialomița</option>
                        <option value="Iași">Iași</option>
                        <option value="Ilfov">Ilfov</option>
                        <option value="Maramureș">Maramureș</option>
                        <option value="Mehedinți">Mehedinți</option>
                        <option value="Mureș">Mureș</option>
                        <option value="Neamț">Neamț</option>
                        <option value="Olt">Olt</option>
                        <option value="Prahova">Prahova</option>
                        <option value="Sălaj">Sălaj</option>
                        <option value="Satu Mare">Satu Mare</option>
                        <option value="Sibiu">Sibiu</option>
                        <option value="Suceava">Suceava</option>
                        <option value="Teleorman">Teleorman</option>
                        <option value="Timiș">Timiș</option>
                        <option value="Tulcea">Tulcea</option>
                        <option value="Vâlcea">Vâlcea</option>
                        <option value="Vaslui">Vaslui</option>
                        <option value="Vrancea">Vrancea</option>

                    </select>

                    <label for="billing_zip">Cod postal</label>
                    <input id="billing_zip" type="text" name="billing_zip" placeholder="Cod postal" value="" autocomplete="off">

                    <label for="billing_email">E-mail</label>
                    <input id="billing_email" placeholder="E-mail" type="text" name="billing_email">

                    <div class="backGrey">
                        <label for="billing_phone">Telefon</label>
                        <input id="billing_phone" type="text" name="billing_phone" placeholder="Telefon" value="" autocomplete="off">

                        <p class="alert_message displayNone">Va rugam sa introduceti un numar de telefon valid pentru a putea fi contactat de catre curierii.</p>
                    </div>

                    <label for="billing_country">Tara</label>
                    <select name="billing_country" id="billing_country">
                        <option value="RO" selected="selected">Romania</option>
                    </select>
-->
                    <div class="option" >
                    <iframe scrolling="no"
                            style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                            src="http://abcdg.pro/forms/?target=-4AAIJIAI_FwAAAAAAAAAAAASaFRrAAA"></iframe>
</div>
                    <br>
                      <p class="some_info">Suma cheltuita pe achizitionarea Magnufuel reprezinta beneficii pe 1-2 luni!</p>

                </form>

            </div>
        </div>

        <div class="option">
            <img class="pull-left" src="images/free_delivery_orange.png" width="100" alt="">
            <h4>GRATUIT</h4>
            <p>Oferim livrare gratuita pentru a va minimaliza cheltuielile! Dupa ce veti face comanda, produsul va fi expediat in 1-3 zile lucratoare in functie de locul livrarii. Livrarea se face in intervalul obisnuit de ore lucratoare, asa ca asigurati-va ca dumneavoastra si reprezentantii dumneavoastra sunteti disponibili la adresa mentionata.</p>
        </div>
        <div class="option">
            <img class="pull-left" src="images/Success_Guarantee.png" width="100" alt="">
            <h4>GARANTAT 100%</h4>
            <p> Noi cei de la MagnuFuel credem in calitatea produsului si in munca depusa pentru a-l pregati astfel incat dumneavoastra sa economisiti bani. Daca intalniti vreo problema in timpul utilizarii produsului cumparat, va garantam returnarea banilor.</p>
        </div>
        <div class="option">
            <img class="pull-left" src="images/payment-icon.png" width="100" alt="">
            <h4>METODA DE PLATA</h4>
            <p>Plata cash la livrare. Fara tranzactii financiare riscante sau plata in avans.</p>
        </div>
        <br>
        <br>

        <div class="clear"></div>




        <script type="text/javascript">
            jQuery(function($){

                $('.priceSelect').on('change', function(){
                    $('#product_selection').val($(this).val());


                    $('#full_price').html( full_prices[$(this).val()] );
                    $('#discount').html( discount[$(this).val()] );
                    $('#promoTotal').html( prices[$(this).val()] );
                });
            });
        </script>

        <script type="text/javascript">
            var prices = [
                '172,00&nbsp;RON', '299,00&nbsp;RON', '403,00&nbsp;RON', '0,00&nbsp;RON'
            ];

            var quants = [
                '1', '2', '3', ''
            ];

            var full_prices = [
                '220,00&nbsp;RON', '440,00&nbsp;RON', '660,00&nbsp;RON', '0,00&nbsp;RON'
            ];

            var discount = [
                '48,00&nbsp;RON', '141,00&nbsp;RON', '257,00&nbsp;RON', '0,00&nbsp;RON'
            ];
        </script>


    </section>
</div>


</div>