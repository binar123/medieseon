<div  id="step2"  style="display:none" >



    <div class="header" id="step2_ib">

        <div class="wrapper clearfix">
            <div class="step2 clearfix">

                <div class="holder-form">
                    <h1 class="center">ZAČNITE ŠETRIŤ VIAC NEŽ <strong class="step2_ib1">20%!</strong>PALIVA UŽ DNES.
                        <img src="images/SK.png" class="new_logo4" alt="">

                    </h1>
                </div>


                <div class="col-6">
                    <img src="images/prod_s2.png" class="prod_s2" alt="">

                    <br>


                    <div class="priceBoxContainer">
                        <img src="images/2_sticker.png" class="new_logo" alt="">
                        <img src="images/1_sticker.png" class="new_logo2" alt="">
                        <table class="priceBox">
                            <tbody>
                            <tr>
                                <td class="text-right">Bežná cena:</td>
                                <td class="rPrice text-left lTh">
                                        <span class="bold" id="full_price" style="color:red">
                                        €48.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right"> Cena:</td>
                                <td class="price text-left" style="color:green;font-size:24px">
                                        <span class="bold" id="promoTotal">
                                        €38.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Doručenie:</td>
                                <td class="fShipping text-left">ZADARMO</td>
                            </tr>
                            <tr>
                                <td class="text-right">Ušetríte:</td>
                                <td class="uSave text-left" id="discount">€10.00</td>
                            </tr>
                            </tbody>
                        </table>
                        <select name="priceSelect" class="priceSelect">
                            <option value="0">Množstvo: 1</option>
                            <option value="1">Množstvo: 2</option>
                            <option value="2">Množstvo: 3</option>
                        </select>
                        <p class="more_text_privacy">* Odporúčané asociáciou European Automobile Manufacturers Association</p>
                    </div>
                </div>

                <br>

                <div class="col-6">

                    <form name="orderform" id="orderform" action="" method="post" >
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAJEGwAAAAAAAAAAAARB-sHlAA"></iframe>
                        <!--<input type="hidden" value="0" id="product_selection" name="product_selection">
                        <input type="hidden" value="On" id="sameshipping" name="sameshipping">
                        <label for="billing_fname">Meno</label>
                        <input id="billing_fname" type="text" name="billing_fname" placeholder="Meno">
                        <label for="billing_fname">Priezvisko</label>
                        <input id="billing_lname" type="text" name="billing_lname" placeholder="Priezvisko" value="">

                        <label for="billing_address">Ulica číslo</label>
                        <input id="billing_address" type="text" name="billing_address" placeholder="Ulica číslo" value="">


                        <label for="billing_city">Mesto</label>
                        <input id="billing_city" type="text" name="billing_city" placeholder="Mesto" value="">


                        <label for="billing_zip">PSČ</label>
                        <input id="billing_zip" type="text" name="billing_zip" placeholder="PSČ" value="">

                        <label for="billing_email">E-mail</label>
                        <input id="billing_email" placeholder="E-mail" type="text" name="billing_email">

                        <label for="billing_phone">Telefón</label>
                        <input id="shipping_areacode" type="text" disabled="disabled" value="+421" name="areacode"><input id="billing_phone" type="text" name="billing_phone" placeholder="Telefón" value="" autocomplete="off">

                        <label for="billing_country">Štát</label>
                        <select name="billing_country" id="billing_country">
                            <option value="SK" selected="selected">Slovensko</option>
                        </select>-->

                               <p class="some_info">Suma, ktorú zaplatíte za Magnufuel , sa vám vráti v priebehu 1 – 2 mesiacov!</p>
                    </form>

                </div>
            </div>

            <div class="step2_ib3">
                <div class="subblock-step2">
                    <img src="images/Success_Guarantee.png" alt="img" class="guaranteeImg">
                    <h4 class="step2_ib2">100% ZÁRUKA</h4>
                    <p>V spoločnosti MagnuFuel veríme v&nbsp;kvalitu nášho výrobku a&nbsp;prácu, ktorú vynakladáme na to, aby sme sa ubezpečili, že ušetríte. Ak narazíte pri používaní zakúpeného výrobku na akýkoľvek problém, môžete využiť záruku vrátenia peňazí.</p>
                </div>
            </div>

            <div class="wrapper clearfix">



                <div class="subblock-step2">
                    <img src="images/free_delivery_orange.png" alt="img" class="deliveryImg">
                    <h4 class="step2_ib2"> ZADARMO</h4>
                    <p>Ponúkame bezplatné doručenie, aby sme minimalizovali vaše výdavky! Váš tovar doručíme do 1-3 pracovných dní od vašej objednávky (v závislosti od miesta doručenia). Doručenie prebieha počas bežných pracovných hodín, preto vás žiadame, aby ste na uvedenom mieste boli k&nbsp;dispozícii vy alebo váš zástupca.</p>
                    <br>
                </div>

                <div class="subblock-step2">
                    <img src="images/payment-icon.png" alt="img" class="paymentImg">
                    <h4 class="step2_ib2">SPÔSOB PLATBY</h4>
                    <p>Platba hotovosťou pri doručení. Bez riskantných finančných transakcií či platby vopred.</p>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!-- end of #wrapper -->



    <div class="border clearfix"></div>

    <div id="footer" class="clearfix">
        <div class="wrapper clearfix">
        </div>
    </div>

    <script type="text/javascript">
        jQuery(function($) {
            $('.priceSelect').on('change', function() {
                $('#product_selection').val($(this).val());
                $('#full_price').html(full_prices[$(this).val()]);
                $('#discount').html(discount[$(this).val()]);
                $('#promoTotal').html(prices[$(this).val()]);
            });
        });

    </script>

    <script type="text/javascript">
        var prices = [
            '&euro;38.00', '&euro;66.00', '&euro;89.00', '&euro;0.00'
        ];
        var quants = [
            '1', '2', '3', ''
        ];
        var full_prices = [
            '&euro;48.00', '&euro;96.00', '&euro;144.00', '&euro;0.00'
        ];
        var discount = [
            '&euro;10.00', '&euro;30.00', '&euro;55.00', '&euro;0.00'
        ];

    </script>

    <script type="text/javascript">
        $("#billing_address").on('focus', function() {
            $("body").append("<div class=\"tip\"><div class=\"toolTip\"><div class=\"toolTipArrow\"></div>Vložte adresu!</div></div>");
            $(".tip").css({
                top: $(this).offset().top + $(".tip").height() - 3,
                left: $(this).offset().left
            });
        });
        $("#billing_address").on('keydown', function() {
            $(".tip").remove();
        });

        $("#billing_address").on('blur', function() {
            $(".tip").remove();
        });

    </script>










</div>
