<div  id="step2"  style="display:none" >
    <link rel="stylesheet" href="!common_files/styles2_m.css" type="text/css">


    <div class="container">
        <nav>

            <img src="images/logo.png" alt="">

            <div class="clear"></div>
        </nav>
        <section class="col-12">

            <h1 class="center" style=" line-height: 1;overflow: hidden;font-weight: bold;      margin-bottom: -40px;  font-size: 28px;margin-top: 45px; text-align: center;">ZAČNITE ŠETRIŤ VIAC NEŽ <strong style="display: inline-block;
    background: #cc1d00;
    color: #fff;
        padding: 0px 21px;"> 20%! </strong></h1>
            <div class="col-6" style="margin-top:140px">

                <div class="figure">
                    <div class="figure">
                        <img src="images/new_logo.jpg" class="product-img" alt="">
                    </div>
                </div>
                <div class="priceBoxContainer">
                    <table class="priceBox">
                        <tbody><tr>
                            <td class="text-right">Bežná cena:</td>
                            <td class="price text-left lTh" id="full_price">€48.00</td>
                        </tr>
                        <tr>
                            <td class="text-right"> Cena:</td>
                            <td class="rPrice text-left" id="promoTotal" style="font-size:21px;color:green;">€38.00</td>
                        </tr>
                        <tr>
                            <td class="text-right">Doručenie:</td>
                            <td class="fShipping text-left" style="color:black">ZADARMO</td>
                        </tr>
                        <tr>
                            <td class="text-right">Ušetríte:</td>
                            <td class="uSave text-left" id="discount">€10.00</td>
                        </tr>
                        </tbody></table>

                    <div style="margin: 0px auto;     max-width: 280px; width: 77%; border: none;display: block;margin-right: auto;left: 36px;position: relative;">
                        <img src="images/1_sticker.png" alt="IMG" style="width:40%; right:22px;position:relative">

                        <img src="images/2_sticker.png" alt="IMG" style="width:40%; right:22px;position:relative">

                        <img src="images/SK.png" alt="IMG" style="width:40%; right:22px;position:relative">

                    </div>
                    <p style="text-align:center">* Odporúčané asociáciou European Automobile Manufacturers Association</p>

                    <select class="priceSelect">
                        <option value="0">Množstvo: 1</option>
                        <option value="1">Množstvo: 2</option>
                        <option value="2">Množstvo: 3</option>
                    </select>
                </div>
                <hr>
            </div>
            <div class="col-6">
                <div class="formContainer">


                    <form name="orderform" id="orderform" action="" method="post" >
                        <!--
                        <input type="hidden" value="0" id="product_selection" name="product_selection">
                        <input type="hidden" value="On" id="sameshipping" name="sameshipping">
                        <label for="billing_fname">Meno</label>
                        <input id="billing_fname" type="text" name="billing_fname" placeholder="Meno">
                        <label for="billing_fname">Priezvisko</label>
                        <input id="billing_lname" type="text" name="billing_lname" placeholder="Priezvisko" value="">

                        <label for="billing_address">Ulica číslo</label>
                        <input id="billing_address" type="text" name="billing_address" placeholder="Ulica číslo" value="">


                        <label for="billing_city">Mesto</label>
                        <input id="billing_city" type="text" name="billing_city" placeholder="Mesto" value="">


                        <label for="billing_zip">PSČ</label>
                        <input id="billing_zip" type="text" name="billing_zip" placeholder="PSČ" value="">

                        <label for="billing_email">E-mail</label>
                        <input id="billing_email" placeholder="E-mail" type="text" name="billing_email">

                        <label for="billing_phone">Telefón</label>
                        <input id="shipping_areacode" type="text" disabled="disabled" value="+421" name="areacode"><input id="billing_phone" type="text" name="billing_phone" placeholder="Telefón" value="" autocomplete="off">

                        <label for="billing_country">Štát</label>
                        <select name="billing_country" id="billing_country">
                            <option value="SK" selected="selected">Slovensko</option>
                        </select>

                        <button class="rushOrder" type="submit">OBJEDNAŤ TERAZ</button>-->
                        <div class="option" >
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAJEGwAAAAAAAAAAAARB-sHlAA"></iframe>
                            </div>

                        <p class="some_info">Suma, ktorú zaplatíte za Magnufuel , sa vám vráti v priebehu 1 – 2 mesiacov!</p>
                    </form>

                </div>
            </div>

            <div class="option">
                <img class="pull-left" src="images/free_delivery_orange.png" width="100" alt="">
                <h4>ZADARMO</h4>
                <p>Ponúkame bezplatné doručenie, aby sme minimalizovali vaše výdavky! Váš tovar doručíme do 1-3 pracovných dní od vašej objednávky (v závislosti od miesta doručenia). Doručenie prebieha počas bežných pracovných hodín, preto vás žiadame, aby ste na uvedenom mieste boli k&nbsp;dispozícii vy alebo váš zástupca.</p>
            </div>
            <div class="option">
                <img class="pull-left" src="images/Success_Guarantee.png" width="100" alt="">
                <h4>100% ZÁRUKA</h4>
                <p>V spoločnosti MagnuFuel veríme v&nbsp;kvalitu nášho výrobku a&nbsp;prácu, ktorú vynakladáme na to, aby sme sa ubezpečili, že ušetríte. Ak narazíte pri používaní zakúpeného výrobku na akýkoľvek problém, môžete využiť záruku vrátenia peňazí.</p>
            </div>
            <div class="option">
                <img class="pull-left" src="images/payment-icon.png" width="100" alt="">
                <h4>SPÔSOB PLATBY</h4>
                <p>Platba hotovosťou pri doručení. Bez riskantných finančných transakcií či platby vopred.</p>
            </div>
            <br>
            <br>

            <div class="clear"></div>




            <script type="text/javascript">
                jQuery(function($){

                    $('.priceSelect').on('change', function(){
                        $('#product_selection').val($(this).val());


                        $('#full_price').html( full_prices[$(this).val()] );
                        $('#discount').html( discount[$(this).val()] );
                        $('#promoTotal').html( prices[$(this).val()] );
                    });
                });
            </script>

            <script type="text/javascript">
                var prices = [
                    '&euro;38.00', '&euro;66.00', '&euro;89.00', '&euro;0.00'
                ];

                var quants = [
                    '1', '2', '3', ''
                ];

                var full_prices = [
                    '&euro;48.00', '&euro;96.00', '&euro;144.00', '&euro;0.00'
                ];

                var discount = [
                    '&euro;10.00', '&euro;30.00', '&euro;55.00', '&euro;0.00'
                ];
            </script>



        </section>
    </div>




</div>