<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect();
?>

<?php if ($detect->isMobile()) { ?>


    <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="úspora paliva ekonomické palivo benzín úspora benzínu zariadenie na úsporu paliva úsporné zariadenie efektívne palivo efektívny motor palivová hadica palivová ekonomika úspora paliva zariadenie na úsporu paliva neodýmový magnet magnet znižujúci emisie uhľovodíkové molekuly nižšia spotreba nízka spotreba paliva obmedzenie CO obmedzenie CH kapacity motora úplné spaľovanie"> <meta name="description" content="Magnufuel dispozitiv de economisire al combustibilului">
        <meta name="description" content="Magnufuel – zariadenie na úsporu paliva">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../../favicon.ico">

        <link rel="stylesheet" href="!common_files/styles_m.css" type="text/css">

        <script src="!common_files/jquery.js" type="text/javascript"></script>
        <script src="!common_files/navigation.js" type="text/javascript"></script>
        <title> Magnufuel</title>
        <script src="!common_files/popup_m.js.php"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('.glyphicon-shopping-cart').on('click',function(){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
            /**/
        </script>


    </head>

    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;">

<div id="visible_first">
   

    <header class="header">

        <div class="container">
            <div class="col">

                <h2 class="style_cib1">Začnite šetriť</h2>
                <h3 class="style_cib2"> <strong>viac než <strong class="bg-orange">20%</strong> paliva už dnes.</strong></h3>
            </div>

        </div>
    </header>



    <div class="container">
        <nav>

            <img src="images/logo.png" alt="logo">
            <div class="pull-right shoppingCard">
                <a class="glyphicon glyphicon-shopping-cart" ></a>
            </div>

            <div class="clear"></div>
        </nav>
        <section class="col-12">
            <article>
                <div class="figure">
                    <img src="images/ezgif.com-crop.gif" class="product-img" alt="" id="style_cib33">

                    <img src="images/strelka_2.png" class="strelka2" alt="">
                </div>
                <div class="style_cib4">
                    <img src="images/1_sticker.png" alt="IMG" class="style_cib5">
                    <img src="images/2_sticker.png" alt="IMG" class="style_cib5"></div>
                <p class="style_cib6">* Odporúčané asociáciou European Automobile Manufacturers Association</p>
                <p class="text-center">
                    Keď palivo prejde silným magnetickým poľom, jeho uhľovodíkové reťazce sa začnú rozkladať na jednoduchšie komponenty, ktoré sa následne aktivujú.
                </p>

                <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post" onsubmit="return template_validator({})">

                    <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                    <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                </form>

                <button class="form-index rushOrder" > Objednať </button>
                <div class="phoneMobile">
                    <i class="glyphicon glyphicon-earphone"></i>
                    <p class="ib_pho_text">421233456334</p>
                </div>
                <div class="text-center mblock2">
                    <div class="row bg-inverse">
                        <div class="container212">
                            <ul class="list-ico">
                                <li>
                                    <img src="images/horse.png" class="ico-horse" alt="">
                                    <p class="style_cib7">Zvýši výkon motora až o 5 konských síl </p>
                                </li>
                                <li>

                                    <img src="images/benz.png" class="ico-benz" alt="">
                                    <p class="style_cib8">  <br> Zníži spotrebu paliva až o 20%</p>
                                </li>
                                <li>

                                    <img src="images/list.png" class="ico-lists" alt="">
                                    <p class="style_cib9">Zníženie emisií CO a CH o 40% až 50% </p>
                                </li>
                                <li>
                                    <img src="images/list_special.png" class="ico-bis" alt="">
                                    <p class="style_cib10">Zariadenie bolo certifikované</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h3 class="text-center" id="style_cib11">
                    Ako Magnufuel funguje?
                </h3>
                <div class="text-center">
                    <img src="images/new_logo2.jpg" class="style_cib2016" alt="img">
                </div>
                <p class="text-center" id="style_cib13">
                    Vo všetkých druhoch paliva (bez ohľadu na miesto ich skladovania) sa neustále menia látky, ktoré sa v&nbsp;nich nachádzajú, pretože na ne vplýva okolitá teplota a vlhkosť. Takéto vplyvy spôsobujú expanziu a&nbsp;kompresiu paliva. Molekuly uhľovodíka (základu každého paliva) sa začínajú pri sebe pohybovať tak, že tvoria skupiny molekúl – „molekulové zhluky“. „Zhluky“ nemôžu celkom vyhorieť, pretože časť z&nbsp;nich sa nachádza mimo dosahu kyslíka. Keď palivo prejde oblasťou, kde je namontované zariadenie Magnufuel , frekvencia magnetickej rezonancie, ktorú zariadenie vytvára, rozloží tieto „zhluky“ na jednotlivé molekuly a&nbsp;kladne ich nabije. Vďaka tomu sa dostanú molekuly kyslíka ku každej molekule paliva, čo napomáha úplnému spaľovaniu zmesi kyslíka a paliva.
                    <br> <br> Výsledkom je nižšia spotreba paliva a&nbsp;zníženie množstva škodlivých emisií. Hlavnou súčasťou zariadenia Magnufuel  je neodýmový magnet (NdFeB), ktorý pozostáva z&nbsp;dvoch častí. V&nbsp;súčasnosti dosahuje tento efekt iba skupina neodýmových magentov, ktoré sú chránené patentom (USA patent Č. 4.802.931, 4.496.395, 7.458.412 – vlastnené korporáciou General Motors).
                    <br> <br> Magnety od ostatných výrobcov (ktoré sú lacnými imitáciami) takýto účinok neposkytujú.
                    <br>
                </p><div class="style_cib14">
                    <br><h1> Montáž</h1> <br>Montáž si nevyžaduje žiadne mimoriadne zručnosti. Najdôležitejšie je nepomýliť sa pri výbere hadice. Zariadenie musí byť upevnené na palivovú hadicu. <br>• Otvorte kapotu auta, nájdite palivovú hadicu a&nbsp;vyberte miesto, kde chcete zariadenie namontovať;<br>• Upevnite Magnufuel na vybraté miesto na palivovej hadici. Hadica by sa mala nachádzať medzi dvomi časťami zariadenia. <br>• Pre zabezpečenie stability použite plastovú svorku, ktorá je súčasťou balenia. Plastová svorka prechádza malým otvorom, ktorý sa nachádza v&nbsp;stredovom kanáli zariadenia. Pevne ju utiahnite a&nbsp;v&nbsp;prípade potreby odstrihnite nepotrebný zostávajúci kus plastu.
                </div>
                <img class="style_cib26" src="images/ezgif.com-crop.gif" alt="">

                <br>
                <br>
                <div class="style_cib24">
                    <button class="form-index rushOrder" > Objednať </button>
                    <div class="phoneMobile">
                        <i class="glyphicon glyphicon-earphone"></i>
                        <p class="ib_pho_text">421233456334</p>
                    </div>
                </div>

                <h2 class="style_cib15">Hlavné výhody</h2>
                <div class="magnit col-4">

                </div>
                <br>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_1.jpg" alt="">
                    <p class="magnit-text">Ľahká montáž. Montáž zariadenia zaberie iba päť minút.</p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_2.jpg" alt="">
                    <p class="magnit-text">Nevytvára žiadne odpadové plyny. Znamená to, že výrazne predlžuje životnosť piestnych krúžkov. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_3.jpg" alt="">
                    <p class="magnit-text">Taktiež výrazne predlžuje životnosť  katalyzátora. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_4.jpg" alt="">
                    <p class="magnit-text">Potvrdená účinnosť. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_5.jpg" alt="">
                    <p class="magnit-text">Krátko po namontovaní Magnufuel  zaznamenáte podstatne úplnejšie spaľovanie.</p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_6.jpg" alt="">
                    <p class="magnit-text">Už koniec kontrolky «Check Engine» („Skontrolujte motor“), ktorá sa rozsvieti najmä  pri nízkom množstve zostávajúceho paliva a&nbsp;neúplnom spaľovaní. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_7.jpg" alt="">
                    <p class="magnit-text">Suma, ktorú zaplatíte za Magnufuel , sa vám vráti v priebehu 1 – 2 mesiacov!</p>
                </div>

                <div class="clear"></div>

                <div class="text-center">
                    <button class="form-index rushOrder" > Objednať </button>
                    <div class="phoneMobile">
                        <i class="glyphicon glyphicon-earphone"></i>
                        <p class="ib_pho_text">421233456334</p>
                    </div>

                </div>
                <div class=" sub-title">
                    <p class="style_cib16">NÁZORY NAŠICH ZÁKAZNÍKOV</p>
                </div>

                <div class="doctors-2 col-6">
                    <img class="pull-left" src="images/driver3.jpg" alt="">
                    <p class="style_cib17"> Andrej Šuster, 32 rokov
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>predtým </small>  10L<i class="slash"></i></span> /
                        <span class="text-orange"><small>teraz</small> 7L</span>
                    </div>
                    <hr>
                    <strong>Pred štyrmi dňami som si namontoval Magnufuel  do Hyundai Accent (2012). V&nbsp;meste som mal spotrebu 10 litrov – teraz mám 7-8. Na diaľnici mám teraz 5,8 l na 100 km. Včera som spravil trasu Bratislava - Ružomberok za 18 litrov. Vďaka tomuto malému zázraku.</strong>
                </div>




                <div class="doctors-2 col-6">
                    <img class="pull-right" src="images/driver2.jpg" alt="">
                    <p class="style_cib19">Michal Pavlík, 46 rokov
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>predtým</small> 9L<i class="slash"></i></span> /
                        <span class="text-orange"><small>teraz</small>  7,5L</span>
                    </div>
                    <hr>
                    <strong>Dacia. Výsledok sa prejavil po druhom tankovaní. Motor začal fungovať tichšie, bez vibrácií a&nbsp;s&nbsp;vyšším výkonom. Keď idem na krátku trasu, tak ušetrím 1 až 1,5 litra. Som spokojný.</strong>
                    <hr>
                </div>



                <div class="clear"></div>

                <div class="sub-title-2">
                    <p class="style_cib20">
                        Magnufuel sa dá použiť na rôzne druhy vozidiel <br>
                        <br>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib1">

                    <img class="circle-img" src="images/img_thumb.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Autá</strong>
                    </p>
                </div>
                <div class="circle col-6 text-center sub-title-2" id="style_cib2">

                    <img class="circle-img" src="images/img_thumb2.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Dopravné vozidlá</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib34">

                    <img class="circle-img" src="images/img_thumb3.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Autobusy a dodávky</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib4">

                    <img class="circle-img" src="images/img_thumb4.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Motorové člny a&nbsp;iné motorové plavidlá</strong>
                    </p>
                </div>


                <div class="circle col-6 text-center sub-title-2" id="style_cib5">

                    <img class="circle-img" src="images/img_thumb5.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Motorky</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib6">

                    <img class="circle-img" src="images/img_thumb6.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Poľnohospodárske stroje</strong>
                    </p>
                </div>

                <div class="clear"></div>

                <div class="sub-title-2 text-center">
                    <p class="style_cib23"><br>  Objednajte si Magnufuel</p>
                </div>



                <button class="form-index rushOrder" > Objednať </button>

                <div class="phoneMobile">
                    <i class="glyphicon glyphicon-earphone"></i>
                    <p class="ib_pho_text">421233456334</p>
                </div>
                <div class="style_cib25">
                    <img src="images/1_sticker.png" alt="IMG">
                    <img src="images/2_sticker.png" alt="IMG">
                </div>
                <p style="text-align:center">* Odporúčané asociáciou European Automobile Manufacturers Association</p>


            </article>

            <div class="clear"></div>

        </section>
        <hr> <footer class="col-12 links">

              </footer>
    </div>






    <div id="popUp" style="display: block; right: -0.0000000000000028729427326909663px;">Zostáva 8 propagačných balíčkov!</div>
    </div>
<?php include 'page2m.php';?>

    </body></html>


<?php } else { ?>


    <html><head>
        <meta charset="UTF-8">
        <title>Oficiálna webová stránka | Magnufuel</title>
        <meta name="keywords" content="úspora paliva ekonomické palivo benzín úspora benzínu zariadenie na úsporu paliva úsporné zariadenie efektívne palivo efektívny motor palivová hadica palivová ekonomika úspora paliva zariadenie na úsporu paliva neodýmový magnet magnet znižujúci emisie uhľovodíkové molekuly nižšia spotreba nízka spotreba paliva obmedzenie CO obmedzenie CH kapacity motora úplné spaľovanie"> <meta name="description" content="Magnufuel dispozitiv de economisire al combustibilului">
        <meta name="description" content="Magnufuel – zariadenie na úsporu paliva">
      <link rel="stylesheet" href="!common_files/all.css" id="style_page1">
        <meta name="format-detection" content="telephone=no">
        <script src="!common_files//time.js"></script>
        <script src="!common_files/popup.js.php"></script>
         <script type="text/javascript" src="!common_files/jquery.min.js"></script>

        <script src="!common_files/timer.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '!common_files/styles.css') );

                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
    /**/
    </script>
    </head>
    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;" >
<div id="visible_first">

    <header class="header">
        <div class="container">
            <div class="col" id="style_ib1">
                <img src="images/logo.png" id="style_ib2" alt="logo">
                <img src="images/SK.png" style="width:60px;top:20px;" id="style_ib3" alt="logo">
                <h2 class="style_ib1">Začnite šetriť</h2>
                <h3 class="style_ib2">viac než <strong class="bg-orange">20%</strong> paliva už dnes.</h3>
            </div>
            <img src="images/ezgif.com-crop.gif" class="pull-right" id="style_ib4" alt="">
            <img src="images/1_sticker.png" class="style_ib4" alt="">
            <img src="images/2_sticker.png" class="style_ib5" alt="">
            <p class="pull-right">Keď palivo prejde silným magnetickým poľom, jeho uhľovodíkové reťazce sa začnú rozkladať na jednoduchšie komponenty, ktoré sa následne aktivujú.</p>
            <p class="style_ib6">* Odporúčané asociáciou European Automobile Manufacturers Association</p>
        </div>
        <button type="button " class="btn order" id="head_button_ord"> Objednať </button>
    </header>
    <div id="popWindow" style="opacity: 0;">K dispozícii je iba <i>3</i> zostávajúce propagačné balíčky.</div>
    <!--popWindow-->
    <section class="main">
        <div class="row bg-inverse">
            <div class="container">
                <ul class="list-ico">
                    <li>
                        <i class="ico-horse"></i>
                        <p>Zvýši výkon motora až o 5 konských síl</p>
                    </li>
                    <li class="style_ib7">
                        <i class="ico-benz"></i>
                        <p class="style_ib8"> <br> Zníži spotrebu paliva až o 20%</p>
                    </li>
                    <li>
                        <i class="ico-lists"></i>
                        <p>Zníženie emisií CO a CH o 40% až 50%</p>
                    </li>
                    <li>
                        <i class="ico-bis"></i>
                        <p>Zariadenie bolo certifikované</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row bg1">
            <div class="container">
                <table>
                    <tbody>
                    <tr>

                        <td class="tovar">
                            <h4>Kúpte si Magnufuel teraz <strong class="text-red"> za zníženú </strong> cenu</h4>


                            <div class="eTimer">
                                <div class="countdown_box">
                                    <span class="order-countdown-hours" id="hours"></span>
                                    <span>Hod</span>
                                    <span class="order-countdown-minutes" id="minutes"></span>
                                    <span>Min</span>
                                    <span class="order-countdown-seconds" id="seconds"></span>
                                    <span>Sek</span>
                                </div>
                            </div>
                            <div class="row">
                                <button type="button"  class="btn order"> Objednať </button>
                                <br>
                                <br>
                                <div class="phoneMobile">
                                    <img src="images/phone.png" alt="">421233456334
                                </div>

                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Ako Magnufuel funguje?</h3>
                <article>
                    <img src="images/img_article.png" class="pull-left" alt="">
                    <p>Vo všetkých druhoch paliva (bez ohľadu na miesto ich skladovania) sa neustále menia látky, ktoré sa v&nbsp;nich nachádzajú, pretože na ne vplýva okolitá teplota a vlhkosť. Takéto vplyvy spôsobujú expanziu a&nbsp;kompresiu paliva. Molekuly uhľovodíka (základu každého paliva) sa začínajú pri sebe pohybovať tak, že tvoria skupiny molekúl – „molekulové zhluky“. „Zhluky“ nemôžu celkom vyhorieť, pretože časť z&nbsp;nich sa nachádza mimo dosahu kyslíka. Keď palivo prejde oblasťou, kde je namontované zariadenie Magnufuel , frekvencia magnetickej rezonancie, ktorú zariadenie vytvára, rozloží tieto „zhluky“ na jednotlivé molekuly a&nbsp;kladne ich nabije. Vďaka tomu sa dostanú molekuly kyslíka ku každej molekule paliva, čo napomáha úplnému spaľovaniu zmesi kyslíka a paliva.</p>
                    <p><br> Výsledkom je nižšia spotreba paliva a&nbsp;zníženie množstva škodlivých emisií. Hlavnou súčasťou zariadenia Magnufuel  je neodýmový magnet (NdFeB), ktorý pozostáva z&nbsp;dvoch častí. V&nbsp;súčasnosti dosahuje tento efekt iba skupina neodýmových magentov, ktoré sú chránené patentom (USA patent Č. 4.802.931, 4.496.395, 7.458.412 – vlastnené korporáciou General Motors).</p>
                </article>
                <article>
                    <img src="images/product_magnu.jpg" class="style_ib10" alt="">
                    <p><br> Magnety od ostatných výrobcov (ktoré sú lacnými imitáciami) takýto účinok neposkytujú.</p>
                    <br><h1> Montáž</h1> <br>Montáž si nevyžaduje žiadne mimoriadne zručnosti. Najdôležitejšie je nepomýliť sa pri výbere hadice. Zariadenie musí byť upevnené na palivovú hadicu. <br>• Otvorte kapotu auta, nájdite palivovú hadicu a&nbsp;vyberte miesto, kde chcete zariadenie namontovať;<br>• Upevnite Magnufuel na vybraté miesto na palivovej hadici. Hadica by sa mala nachádzať medzi dvomi časťami zariadenia. <br>• Pre zabezpečenie stability použite plastovú svorku, ktorá je súčasťou balenia. Plastová svorka prechádza malým otvorom, ktorý sa nachádza v&nbsp;stredovom kanáli zariadenia. Pevne ju utiahnite a&nbsp;v&nbsp;prípade potreby odstrihnite nepotrebný zostávajúci kus plastu.
                    <div class="text-center">
                        <button type="button" class="btn order"> Objednať </button>

                        <div class="phoneMobile">
                            <img src="images/phone.png" alt="">421233456334
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <div class="row row-grey">
            <div class="container">
                <h3>Hlavné výhody</h3>
                <ul class="list">
                    <li>
						<span class="icon-circle"><i class="ico-plane"></i>
</span>Ľahká montáž. Montáž zariadenia zaberie iba päť minút.</li>
                    <li>
						<span class="icon-circle"><i class="ico-service"></i>
</span>Nevytvára žiadne odpadové plyny. Znamená to, že výrazne predlžuje životnosť piestnych krúžkov.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-watch"></i>
</span>Taktiež výrazne predlžuje životnosť  katalyzátora.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-bisines_d"></i>
</span>Potvrdená účinnosť.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-clock"></i>
</span>Krátko po namontovaní Magnufuel  zaznamenáte podstatne úplnejšie spaľovanie.</li>
                    <li>
						<span class="icon-circle"><i class="ico-sun"></i>
</span>Už koniec kontrolky «Check Engine» („Skontrolujte motor“), ktorá sa rozsvieti najmä  pri nízkom množstve zostávajúceho paliva a&nbsp;neúplnom spaľovaní.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-payment"></i>
</span>Suma, ktorú zaplatíte za Magnufuel , sa vám vráti v priebehu 1 – 2 mesiacov!</li>
                </ul>
                <div class="row bg-img">


                    <button type="button"  class="btn order"> Objednať <br> </button>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="phoneMobile">
                        <img src="images/phone.png" alt="">421233456334
                    </div>

                </div>
            </div>
        </div>
        <div class="row bg2">
            <div class="container">
                <h3>NÁZORY NAŠICH ZÁKAZNÍKOV</h3>
                <div class="row-thumb">
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Andrej Šuster, 32 rokov</span>
                                <img src="images/driver3.jpg" class="img" alt="driver">
                                <span>Spotreba paliva po namontovaní Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">10L<i class="slash">/</i>
<small>predtým</small>
</span>
                                    <span class="text-orange">7L
<small>teraz</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Pred štyrmi dňami som si namontoval Magnufuel  do Hyundai Accent (2012). V&nbsp;meste som mal spotrebu 10 litrov – teraz mám 7-8. Na diaľnici mám teraz 5,8 l na 100 km. Včera som spravil trasu Bratislava - Ružomberok za 18 litrov. Vďaka tomuto malému zázraku.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Michal Pavlík, 46 rokov</span>
                                <img src="images/driver2.jpg" class="img" alt="driver">
                                <span>Spotreba paliva po namontovaní Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">9L<i class="slash">/</i>
<small>predtým</small>
</span>
                                    <span class="text-orange">7,5L
<small>teraz</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Dacia. Výsledok sa prejavil po druhom tankovaní. Motor začal fungovať tichšie, bez vibrácií a&nbsp;s&nbsp;vyšším výkonom. Keď idem na krátku trasu, tak ušetrím 1 až 1,5 litra. Som spokojný.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Patrik Rajter, 29 rokov</span>
                                <img src="images/driver.jpg" height="187" width="249" alt="driver" class="img">
                                <span>Spotreba paliva po namontovaní Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">17L<i class="slash">/</i>
<small>predtým</small>
</span>
                                    <span class="text-orange">12L
<small>teraz</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Lexus RX 350. Objednal som si hneď 2 kusy Magnufuel  a&nbsp;je to vidieť a&nbsp;obzvlášť počuť na výkone a&nbsp;hlasitosti motora. Párkrát sa mi stalo, že som natankoval na neznámej čerpačke a&nbsp;potom mi blikala kontrola („Check the engine“ – skontrolujte motor). Teraz s&nbsp;tým nemam problém. Nielenže mi nerobí problémy nekvalitné palivo, ale na jednu nádrž tiež zájdem oveľa ďalej.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Magnufuel sa dá použiť na rôzne druhy vozidiel <br></h3>
                <p class="text-center">Magnufuel – nástroj na úsporu paliva je veľmi obľúbený v USA a&nbsp;krajinách západnej Európy a&nbsp;teraz si ho môžete kúpiť aj na <span class="country_name">Slovensku!</span></p>
                <ul class="thumb">
                    <li>
                        <img src="images/img_thumb.jpg" class="img" alt="automobil">
                        <h5>Autá</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb2.jpg" class="img" alt="automobil">
                        <h5>Dopravné vozidlá</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb3.jpg" class="img" alt="automobil">
                        <h5>Autobusy a dodávky</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb4.jpg" class="img" alt="automobil">
                        <h5>Motorové člny a&nbsp;iné motorové plavidlá</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb5.jpg" class="img" alt="automobil">
                        <h5>Motorky</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb6.jpg" class="img" alt="automobil">
                        <h5>Poľnohospodárske stroje</h5>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row bg4">
            <div class="container">
                <div class="box-record">
                    <h3>Iba dnes si môžete kúpiť
                        <strong class="text-yellow"> Magnufuel</strong>
                        za <em class="text-yellow"> rekordne </em>  nízku cenu!</h3>
                    <div class="bg-white">
                        <div class="eTimer">
                            <div class="countdown_box">
                                <span class="order-countdown-hours" id="hours_bottom"></span>
                                <span>Hod</span>
                                <span class="order-countdown-minutes" id="minutes_bottom"></span>
                                <span>Min</span>
                                <span class="order-countdown-seconds" id="seconds_bottom"></span>
                                <span>Sek</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="form-inverse country">

                    <h3><br>  Objednajte si Magnufuel</h3>  a&nbsp;začnite šetriť palivo už dnes.
                    <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post">
                        <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                        <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                        <button class="form-index rushOrder" type="submit" id="orderib">skontrolujte ceny</button>
                    </form>
                    <img src="images/1_sticker.png" class="style_ib11" alt="Guarante">

                    <img src="images/2_sticker.png" class="style_ib11" alt="Guarante">
                    <p class="style_ib12">* Odporúčané asociáciou European Automobile Manufacturers Association</p>

                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">




        </div>
    </footer>





</div>

    <?php include 'page2.php';?>
    </body>


    </html>
<?php } ?>