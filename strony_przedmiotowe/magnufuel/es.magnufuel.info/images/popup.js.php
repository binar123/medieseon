var City = new Array();
City[0] = "Iasi";
City[1] = "Piatra Neamt";
City[2] = "Braila";
City[3] = "Bucuresti";
City[4] = "Pitesti";
City[5] = "Sibiu";
City[6] = "Constanta";
City[7] = "Sighisoara";
City[8] = "Alba Iulia";
City[9] = "Cluj-Napoca";
City[10] = "Galati";
City[11] = "Ploiesti";
City[12] = "Oradea";
City[13] = "Suceava";
City[14] = "Arad";
City[15] = "Bacau";
City[16] = "Targu Mures";
City[17] = "Botosani";
City[18] = "Focsani";
City[19] = "Bistrita";

var catchText3;

var promoCount;

function showHydeWin(mode, vell){
	
	var showVell = vell;
	
	var theWindow = document.getElementById("popWindow");
	
	var opStart;
	var opEnd;
	
	if(mode === "+"){
		opStart = 0;
		opEnd = 1;
	}else if(mode === "-"){
		opStart = 1;
		opEnd = 0;
		showVell = -vell;
	}else{
		return;
	}
	
	var count = opStart;
	
	var showLoop = setInterval(function(){
		
		theWindow.style.opacity = count;
		count += showVell;
		
		if(mode === "+"){
			if(count > opEnd){
				theWindow.style.opacity = opEnd;
				clearInterval(showLoop);
			}
		}else if(mode === "-"){
			if(count < opEnd){
				theWindow.style.opacity = opEnd;
				clearInterval(showLoop);
				return;
			}
		}
	
	},17);
	
};

function genRandNum(min, max){
	var count = Math.floor((Math.random() * (max-min)) + min);
	return count;
};

function showBoxTime(sec, vell){
	showHydeWin("+", vell);
	var n = 0;
	var showLoop = setInterval(function(){
		if(n > sec){
			showHydeWin("-", vell);
			clearInterval(showLoop);
			return;
		}
		n ++;
	},1000);
};

function setBoxText(num){
	
	switch(num){
		case 1:
			var text = "Momentan sunt 100 de vizitatori pe acest site.";
			var count = genRandNum(150, 250);
			document.getElementById("popWindow").innerHTML = text.replace("100", "<span>"+count+"</span>");
			break;
		case 2:
			var text = "Tocmai a fost plasata o comanda din CITY.";
			var city = City[Math.floor(Math.random()*City.length)];
			document.getElementById("popWindow").innerHTML = text.replace("CITY", "<b>"+city+"</b>");
			break;
		case 3:
			var text = "15 pachete la preturi promotionale ramase!";
			var count = genRandNum(4, 8);
			catchText3 = count;
			document.getElementById("popWindow").innerHTML = text.replace("15", "<i>"+count+"</i>");
			break;
		default:
			return;
	}
	return;
	
};

function welcomeUser(){
	
	var time = 0;
	
	var custTime = genRandNum(9, 12);
	
	var userLoop = setInterval(function(){
		
		if(time === 0){
			setBoxText(1);
			showBoxTime(4, 0.05);
		}
		
		if(time === custTime){
			setBoxText(2);
			showBoxTime(4, 1);
		}
		
		if(time === custTime+7){
			setBoxText(3);
			promoCount = catchText3;
			showBoxTime(5, 0.01);
			clearInterval(userLoop);
			customLead(genRandNum(7, 20));
		}
		
		time ++;
	},1000);
	
};

function customLead(sec){
	
	setTimeout(function(){
		
		var time = 0;
		var leadLoop = setInterval(function(){
			
			if(time === 0){
				setBoxText(2);
				showBoxTime(4, 1);
			}
			
			if(time === 7){
				promoCount --;
				if(promoCount > 0){
					document.getElementById("popWindow").innerHTML = "Mai sunt doar <i>"+promoCount+"</i> pachete la preturi promotionale ramase.";
				}else{
					document.getElementById("popWindow").innerHTML = "<span>Daca comanzi acum, poti cumpara produsul la pret promotional!!!</span>";
				}
				showBoxTime(5, 0.01);
			}
			
			if(time === 14){
				clearInterval(leadLoop);
				if(promoCount > 0){
					customLead(genRandNum(2, 20));
				}
			}
			
			time ++;
		},1000);
		
	},sec*1000);
	
};

window.onload = welcomeUser;