<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect();
?>

<?php if ($detect->isMobile()) { ?>


    <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="ahorrar combustible, consumir menos gasolina, gasolina barata, gas barato, dispositivo para ahorrar gasolina, ahorrar gasolina, diésel eficiente, dispositivo para ahorrar combustible, motor eficiente, manguera de gasolina, gasolina eficiente, gasolina más eficiente, diésel más eficiente, imanes de neodimio, menos emisiones, moléculas de hidrocarburos, moléculas de combustible, moléculas de hidrocarburos, menos consumo de gasolina, menor consumo de combustible, menos emisiones de CO2, aumenta la capacidad del motor, combustión completa">


        <meta name="description" content="Magnufuel Dispositivo para ahorrar gasolina">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../../favicon.ico">

        <link rel="stylesheet" href="!common_files/styles_m.css" type="text/css">

        <script src="!common_files/jquery.js" type="text/javascript"></script>
        <script src="!common_files/navigation.js" type="text/javascript"></script>
        <title> Magnufuel</title>
        <script src="!common_files/popup_m.js.php"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('.glyphicon-shopping-cart').on('click',function(){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
            /**/
        </script>


    </head>

    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;">

<div id="visible_first">



    <header class="header">

        <div class="container">
            <div class="col">

                <h2 class="style_cib1">Empieza a ahorrar</h2>
                <h3 class="style_cib2"> <strong>más de un <strong class="bg-orange">20%</strong> en gasolina desde hoy.</strong></h3>
            </div>

        </div>
    </header>



    <div class="container">
        <nav>

            <img src="images/logo.png" alt="logo">
            <div class="pull-right shoppingCard">
                <a class="glyphicon glyphicon-shopping-cart" ></a>
            </div>

            <div class="clear"></div>
        </nav>
        <section class="col-12">
            <article>
                <div class="figure">
                    <img src="images/ezgif.com-crop.gif" class="product-img" alt="" id="style_cib33">

                    <img src="images/strelka_2.png" class="strelka2" alt="">
                </div>
                <div class="style_cib4">
                    <img src="images/1_sticker.png" alt="IMG" class="style_cib5">
                    <img src="images/2_sticker.png" alt="IMG" class="style_cib5"></div>
                <p class="style_cib6">* Recomendado por la European Automobile Manufacturers Association</p>
                <p class="text-center">
                    Cuando el combustible pasa a través de Magnufuel las cadenas de hidrocarburos que lo componen se transforman en componentes más sencillos que entran fácilmente en combustión.
                </p>

                <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post" onsubmit="return template_validator({})">

                    <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                    <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                </form>

                <button class="form-index rushOrder"> Hacer pedido </button>
                <div class="phoneMobile">
                    <i class="glyphicon glyphicon-earphone"></i>
                    <p class="ib_pho_text">34932204302</p>
                </div>
                <div class="text-center mblock2">
                    <div class="row bg-inverse">
                        <div class="container212">
                            <ul class="list-ico">
                                <li>
                                    <img src="images/horse.png" class="ico-horse" alt="">
                                    <p class="style_cib7">Aumento en la capacidad del motor de hasta 5 caballos. </p>
                                </li>
                                <li>

                                    <img src="images/benz.png" class="ico-benz" alt="">
                                    <p class="style_cib8">  <br> Reducción en el consumo de combustible de hasta un 20 %.</p>
                                </li>
                                <li>

                                    <img src="images/list.png" class="ico-lists" alt="">
                                    <p class="style_cib9">Reducción en la emisión de CO2 y otras partículas contaminantes de hasta un 50 %. </p>
                                </li>
                                <li>
                                    <img src="images/list_special.png" class="ico-bis" alt="">
                                    <p class="style_cib10">Dispositivo certificado</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="#sert" class="JUMP"><p>Hacer clic para ver el certificado</p></a>

                <h3 class="text-center" id="style_cib11">
                    Cómo funciona Magnufuel
                </h3>
                <div class="text-center">
                    <img src="images/new_logo2.jpg" class="style_cib2016" alt="img">
                </div>
                <p class="text-center" id="style_cib13">
                    Todos los combustibles se ven afectados por la temperatura y la humedad, factores que hacen que el combustible se expanda o se contraiga. Además, el combustible está formado por moléculas de hidrocarburos que se agrupan en conglomerados de moléculas, y esto causa que algunas moléculas no reciban el oxígeno que necesitan para entrar en combustión. Lo que hace Magnufuel es crear un campo magnético que disuelve estos conglomerados, lo cual consigue a su vez que cada molécula entre en combustión de forma individualizada al recibir siempre el oxígeno necesario para ello.
                    <br> <br> Gracias a Magnufuel es posible extraer el máximo rendimiento de los combustibles y esto a su vez nos ayuda a ahorrar dinero. Además, con Magnufuel disminuyen las emisiones. Magnufuel lleva dos imanes de neodimio (NdFeB). La tecnología basada en imanes de neodimio de Magnufuel está patentada a nombre de General Motors, y el número de la patente es 4.802.931, 4.496.395, 7.458.412
                    <br> <br> Otro tipo de imanes, aparte de ser imitaciones, no consiguen el mismo efecto.
                    <br>
                </p><div class="style_cib14">
                    <br><h1> Instalación</h1> <br>La instalación de Magnufuel es muy sencilla. Lo más importante es no cometer un error al elegir la manguera, para lo cual deberemos elegir siempre la manguera de combustible. <br>• Abre el capó de tu coche, busca la manguera de combustible y añádele el dispositivo<br>•Deberás asegurarte de que la manguera pase a través de las dos partes que forman el dispositivo.<br>•Para asegurar bien el dispositivo, utiliza la cuerda de plástico que viene incluida con el producto: deberás introducirla por el orificio que está en la parte central del dispositivo. Ajústala al máximo y corta el sobrante.
                </div>
                <img class="style_cib26" src="images/ezgif.com-crop.gif" alt="">

                <a href="#sert" class="JUMP"><p>Hacer clic para ver el certificado</p></a>

                <div class="style_cib24">
                    <button class="form-index rushOrder" > Hacer pedido </button>
                </div>
                <div class="phoneMobile">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <p class="ib_pho_text">34932204302</p>
                </div>
                <h2 class="style_cib15">Principales ventajas</h2>
                <div class="magnit col-4">

                </div>
                <br>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_1.jpg" alt="">
                    <p class="magnit-text">Fácil instalación: solo 5 minutos.</p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_2.jpg" alt="">
                    <p class="magnit-text">No genera gas residual, lo cual significa que la vida de los pistones aumenta considerablemente. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_3.jpg" alt="">
                    <p class="magnit-text">También aumenta la vida útil del catalizador. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_4.jpg" alt="">
                    <p class="magnit-text">Eficiencia garantizada. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_5.jpg" alt="">
                    <p class="magnit-text">Tras instalar Magnufuel la  combustión completa aumenta.</p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_6.jpg" alt="">
                    <p class="magnit-text">Recibirás menos alertas para revisar el motor producidas por  bajos niveles de combustible o por combustiones incompletas. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_7.jpg" alt="">
                    <p class="magnit-text">¡Solo tardarás entre 1 y 2 meses en recuperar el dinero invertido en Magnufuel!</p>
                </div>

                <div class="clear"></div>

                <div class="text-center">
                    <a href="#sert" class="JUMP"><p>Hacer clic para ver el certificado</p></a>

                    <button class="form-index rushOrder" > Hacer pedido </button>
                    <div class="phoneMobile">
                        <span class="glyphicon glyphicon-earphone"></span>
                        <p class="ib_pho_text">34932204302</p>
                    </div>

                </div>
                <div class=" sub-title">
                    <p class="style_cib16">OPINIONES DE NUESTROS CLIENTES</p>
                </div>

                <div class="doctors-2 col-6">
                    <img class="pull-left" src="images/driver3.jpg" alt="">
                    <p class="style_cib17"> Andrés Gómez (32 años)
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>antes </small>  10L<i class="slash"></i></span> /
                        <span class="text-orange"><small>después</small> 7L</span>
                    </div>
                    <hr>
                    <strong>Hace cuatro días instalé Magnufuel en un Hyundai Accent del 2012. Mi consumo era de 10 litros en ciudad y 7 en autovía. Ahora ha bajado a 7 y 5,8 respectivamente. Ayer me hice 500 km con unos 25 litros de gasolina. Es un milagro.</strong>
                </div>




                <div class="doctors-2 col-6">
                    <img class="pull-right" src="images/driver2.jpg" alt="">
                    <p class="style_cib19">Miguel Gómez (46 años)
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>antes</small> 9L<i class="slash"></i></span> /
                        <span class="text-orange"><small>después</small>  7.5L</span>
                    </div>
                    <hr>
                    <strong>Tengo un Dacia. En mi caso, los resultados aparecieron la segunda vez que llené el depósito. El motor se volvió más silencioso y vibraba menos. Ahora ahorro entre 1 y 1,5 litros cada 100 km.</strong>
                    <hr>
                </div>



                <div class="clear"></div>

                <div class="sub-title-2">
                    <p class="style_cib20">
                        Magnufuel puede utilizarse con todo tipo de vehículos.
                        <br>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib1">

                    <img class="circle-img" src="images/img_thumb.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Coches</strong>
                    </p>
                </div>
                <div class="circle col-6 text-center sub-title-2" id="style_cib2">

                    <img class="circle-img" src="images/img_thumb2.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Camiones</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib34">

                    <img class="circle-img" src="images/img_thumb3.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Autobuses y furgonetas</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib4">

                    <img class="circle-img" src="images/img_thumb4.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Lanchas y otras embarcaciones</strong>
                    </p>
                </div>


                <div class="circle col-6 text-center sub-title-2" id="style_cib5">

                    <img class="circle-img" src="images/img_thumb5.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Motocicletas</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib6">

                    <img class="circle-img" src="images/img_thumb6.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Maquinaria agrícola</strong>
                    </p>
                </div>

                <div class="clear"></div>

                <div class="sub-title-2 text-center">
                    <span id="sert" class="anchor"></span>
                    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=auto, height=auto, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">

                        <img alt="" class="cert" src="images/CE_BIG_M.jpg">
                    </a>
                    <p class="style_cib23"><br> Haz tu pedido de Magnufuel</p>
                </div>



                <button class="form-index rushOrder" > Hacer pedido </button>
                <div class="phoneMobile">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <p class="ib_pho_text">34932204302</p>
                </div>

                <div class="style_cib25">
                    <img src="images/1_sticker.png" alt="IMG">
                    <img src="images/2_sticker.png" alt="IMG">
                </div>
                <p style="text-align:center">* Recomendado por la European Automobile Manufacturers Association</p>


            </article>

            <div class="clear"></div>

        </section>
        <hr> <footer class="col-12 links">
   </footer>
    </div>






    <div id="popUp"></div>
    </div>
<?php include 'page2m.php';?>

    </body></html>


<?php } else { ?>


    <html><head>
        <meta charset="UTF-8">
        <title>Página web oficial | Magnufuel</title>
        <meta name="keywords" content="ahorrar combustible, consumir menos gasolina, gasolina barata, gas barato, dispositivo para ahorrar gasolina, ahorrar gasolina, diésel eficiente, dispositivo para ahorrar combustible, motor eficiente, manguera de gasolina, gasolina eficiente, gasolina más eficiente, diésel más eficiente, imanes de neodimio, menos emisiones, moléculas de hidrocarburos, moléculas de combustible, moléculas de hidrocarburos, menos consumo de gasolina, menor consumo de combustible, menos emisiones de CO2, aumenta la capacidad del motor, combustión completa"> 
      
      
        <meta name="description" content="Magnufuel Dispositivo para ahorrar gasolina">

        <link rel="stylesheet" href="!common_files/all.css" id="style_page1">
        <meta name="format-detection" content="telephone=no">
        <script src="!common_files//time.js"></script>
        <script src="!common_files/popup.js.php"></script>
         <script type="text/javascript" src="!common_files/jquery.min.js"></script>

        <script src="!common_files/timer.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '!common_files/styles.css') );

                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
    /**/
    </script>
    </head>
    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;" >
<div id="visible_first">

    <header class="header">
        <div class="container">
            <div class="col" id="style_ib1">
                <img src="images/logo.png" id="style_ib2" alt="logo">
                <img src="images/ES.png" style="width: 50px; top: 20px; right: 51px;" id="style_ib" alt="logo">
                <h2 class="style_ib1">Empieza a ahorrar <br>más de un <strong class="bg-orange">20%</strong> en gasolina desde hoy.</h2>
            </div>
            <img src="images/ezgif.com-crop.gif" class="pull-right" id="style_ib4" alt="">
            <img src="images/1_sticker.png" class="style_ib4" alt="">
            <img src="images/2_sticker.png" class="style_ib5" alt="">
            <p class="pull-right">Cuando el combustible pasa a través de Magnufuel las cadenas de hidrocarburos que lo componen se transforman en componentes más sencillos que entran fácilmente en combustión.</p>
            <p class="style_ib6">* Recomendado por la European Automobile Manufacturers Association</p>
        </div>
        <button type="button"  class="btn order" id="head_button_ord"> Hacer pedido </button>
    </header>
    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=500, height=707, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">
        <img alt="" id="sertificate" src="images/CE.jpg">
        <p>Hacer clic para ampliar</p>
    </a>
    <div id="popWindow" style="opacity: 1;">Quedan sólo <i>3</i> paquetes a precio promocional.</div>
    <!--popWindow-->
    <section class="main">
        <div class="row bg-inverse">
            <div class="container">
                <ul class="list-ico">
                    <li>
                        <i class="ico-horse"></i>
                        <p>Aumento en la capacidad del motor de hasta 5 caballos.</p>
                    </li>
                    <li class="style_ib7">
                        <i class="ico-benz"></i>
                        <p class="style_ib8"> <br> Reducción en el consumo de combustible de hasta un 20 %.</p>
                    </li>
                    <li>
                        <i class="ico-lists"></i>
                        <p>Reducción en la emisión de CO2 y otras partículas contaminantes de hasta un 50 %.</p>
                    </li>
                    <li>
                        <i class="ico-bis"></i>
                        <p>Dispositivo certificado</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row bg1">
            <div class="container">
                <table>
                    <tbody>
                    <tr>

                        <td class="tovar">
                            <h4>Compra ahora Magnufuel <strong class="text-red"> por un precio </strong> reducido</h4>


                            <div class="eTimer">
                                <div class="countdown_box">
                                    <span class="order-countdown-hours" id="hours"></span>
                                    <span>Horas</span>
                                    <span class="order-countdown-minutes" id="minutes"></span>
                                    <span>Minutos</span>
                                    <span class="order-countdown-seconds" id="seconds"></span>
                                    <span>Segundos</span>
                                </div>
                            </div>
                            <div class="row">
                                <button type="button"  class="btn order"> Hacer pedido </button>

                                <br>
                                <br>
                                <div class="phoneMobile">
                                    <img src="images/phone.png" alt="">34932204302
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Cómo funciona Magnufuel</h3>
                <article>
                    <img src="images/img_article.png" class="pull-left" alt="">
                    <p>Todos los combustibles se ven afectados por la temperatura y la humedad, factores que hacen que el combustible se expanda o se contraiga. Además, el combustible está formado por moléculas de hidrocarburos que se agrupan en conglomerados de moléculas, y esto causa que algunas moléculas no reciban el oxígeno que necesitan para entrar en combustión. Lo que hace Magnufuel es crear un campo magnético que disuelve estos conglomerados, lo cual consigue a su vez que cada molécula entre en combustión de forma individualizada al recibir siempre el oxígeno necesario para ello.</p>
                    <p><br> Gracias a Magnufuel es posible extraer el máximo rendimiento de los combustibles y esto a su vez nos ayuda a ahorrar dinero. Además, con Magnufuel disminuyen las emisiones. Magnufuel lleva dos imanes de neodimio (NdFeB). La tecnología basada en imanes de neodimio de Magnufuel está patentada a nombre de General Motors, y el número de la patente es 4.802.931, 4.496.395, 7.458.412</p>
                </article>
                <article>
                    <img src="images/product_magnu.jpg" class="style_ib10" alt="">
                    <p><br> Otro tipo de imanes, aparte de ser imitaciones, no consiguen el mismo efecto.</p>
                    <br><h1> Instalación</h1> <br>La instalación de Magnufuel es muy sencilla. Lo más importante es no cometer un error al elegir la manguera, para lo cual deberemos elegir siempre la manguera de combustible. <br>• Abre el capó de tu coche, busca la manguera de combustible y añádele el dispositivo<br>•Deberás asegurarte de que la manguera pase a través de las dos partes que forman el dispositivo.<br>•Para asegurar bien el dispositivo, utiliza la cuerda de plástico que viene incluida con el producto: deberás introducirla por el orificio que está en la parte central del dispositivo. Ajústala al máximo y corta el sobrante.
                    <div class="text-center">
                        <button type="button"  class="btn order"> Hacer pedido </button>


                        <div class="phoneMobile">
                            <img src="images/phone.png" alt="">34932204302
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <div class="row row-grey">
            <div class="container">
                <h3>Principales ventajas</h3>
                <ul class="list">
                    <li>
						<span class="icon-circle"><i class="ico-plane"></i>
</span>Fácil instalación: solo 5 minutos.</li>
                    <li>
						<span class="icon-circle"><i class="ico-service"></i>
</span>No genera gas residual, lo cual significa que la vida de los pistones aumenta considerablemente.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-watch"></i>
</span>También aumenta la vida útil del catalizador.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-bisines_d"></i>
</span>Eficiencia garantizada.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-clock"></i>
</span>Tras instalar Magnufuel la  combustión completa aumenta.</li>
                    <li>
						<span class="icon-circle"><i class="ico-sun"></i>
</span>Recibirás menos alertas para revisar el motor producidas por  bajos niveles de combustible o por combustiones incompletas.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-payment"></i>
</span>¡Solo tardarás entre 1 y 2 meses en recuperar el dinero invertido en Magnufuel!</li>
                </ul>
                <div class="row bg-img">


                    <button type="button"  class="btn order"> Hacer un pedido <br> </button>

                    <br>
                    <br>
                    <br>
                    <div class="phoneMobile">
                        <img src="images/phone.png" alt="">34932204302
                    </div>
                </div>
            </div>
        </div>
        <div class="row bg2">
            <div class="container">
                <h3>OPINIONES DE NUESTROS CLIENTES</h3>
                <div class="row-thumb">
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Andrés Gómez (32 años)</span>
                                <img src="images/driver3.jpg" class="img" alt="driver">
                                <span>Consumo de combustible antes y después de la instalación de Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">10L<i class="slash">/</i>
<small>antes</small>
</span>
                                    <span class="text-orange">7L
<small>después</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Hace cuatro días instalé Magnufuel en un Hyundai Accent del 2012. Mi consumo era de 10 litros en ciudad y 7 en autovía. Ahora ha bajado a 7 y 5,8 respectivamente. Ayer me hice 500 km con unos 25 litros de gasolina. Es un milagro.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Miguel Gómez (46 años)</span>
                                <img src="images/driver2.jpg" class="img" alt="driver">
                                <span>Consumo de combustible antes y después de la instalación de Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">9L<i class="slash">/</i>
<small>antes</small>
</span>
                                    <span class="text-orange">7.5L
<small>después</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Tengo un Dacia. En mi caso, los resultados aparecieron la segunda vez que llené el depósito. El motor se volvió más silencioso y vibraba menos. Ahora ahorro entre 1 y 1,5 litros cada 100 km.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Pedro Del Hierro López (28 años)</span>
                                <img src="images/driver.jpg" height="187" width="249" alt="driver" class="img">
                                <span>Consumo de combustible antes y después de la instalación de Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">17L<i class="slash">/</i>
<small>antes</small>
</span>
                                    <span class="text-orange">12L
<small>después</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Tengo un C5. Hice un pedido de Magnufuel e inmediatamente pude apreciar como el motor sonaba y vibraba menos. Además, no han vuelto a aparecerme los avisos de “comprobar motor” pese a seguir echándole a veces combustible de mala calidad. Por último, mi consumo ha disminuido considerablemente, cada tanque me dura mucho más.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Magnufuel puede utilizarse con todo tipo de vehículos.</h3>
                <p class="text-center">Magnufuel es un dispositivo para ahorrar gasolina muy popular en EE. UU., y ahora se puede comprar también en <span class="country_name">¡España!</span></p>
                <ul class="thumb">
                    <li>
                        <img src="images/img_thumb.jpg" class="img" alt="automobil">
                        <h5>Coches</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb2.jpg" class="img" alt="automobil">
                        <h5>Camiones</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb3.jpg" class="img" alt="automobil">
                        <h5>Autobuses y furgonetas</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb4.jpg" class="img" alt="automobil">
                        <h5>Lanchas y otras embarcaciones</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb5.jpg" class="img" alt="automobil">
                        <h5>Motocicletas</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb6.jpg" class="img" alt="automobil">
                        <h5>Maquinaria agrícola</h5>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row bg4">
            <div class="container">
                <div class="box-record">
                    <h3>Solo hoy a un
                        <strong class="text-yellow"> Magnufuel</strong>
                        precio <em class="text-yellow"> especialmente </em>  reducido</h3>
                    <div class="bg-white">
                        <div class="eTimer">
                            <div class="countdown_box">
                                <span class="order-countdown-hours" id="hours_bottom"></span>
                                <span>Horas</span>
                                <span class="order-countdown-minutes" id="minutes_bottom"></span>
                                <span>Minutos</span>
                                <span class="order-countdown-seconds" id="seconds_bottom"></span>
                                <span>Segundos</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="form-inverse country">

                    <h3><br> Haz tu pedido de Magnufuel</h3> y empieza a ahorrar en combustible hoy mismo
                    <form name="form-index" class="form-inverse country" id="form-index" action="step2.php" method="post">
                        <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                        <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                        <button class="form-index rushOrder" type="submit" id="orderib">consultar precios</button>
                    </form>
                    <img src="images/1_sticker.png" class="style_ib11" alt="Guarante">

                    <img src="images/2_sticker.png" class="style_ib11" alt="Guarante">
                    <p class="style_ib12">* Recomendado por la European Automobile Manufacturers Association</p>

                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">


        </div>
    </footer>





</div>

    <?php include 'page2.php';?>
    </body>


    </html>
<?php } ?>