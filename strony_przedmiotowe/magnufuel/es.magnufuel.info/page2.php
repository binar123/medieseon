<div  id="step2"  style="display:none" >


    <div class="header" id="step2_ib">

        <div class="wrapper clearfix">
            <div class="step2 clearfix">

                <div class="holder-form">
                    <h1 class="center">EMPIEZA A AHORRAR MÁS DE UN <strong class="step2_ib1">20%!</strong>EN GASOLINA DESDE HOY.
                        <img src="images/ES.png" class="new_logo4" alt="">

                    </h1>
                </div>


                <div class="col-6">
                    <img src="images/prod_s2.png" class="prod_s2" alt="">

                    <br>


                    <div class="priceBoxContainer">
                        <img src="images/2_sticker.png" class="new_logo" alt="">
                        <img src="images/1_sticker.png" class="new_logo2" alt="">

                        <table class="priceBox">
                            <tbody>
                            <tr>
                                <td class="text-right">Precio habitual:</td>
                                <td class="rPrice text-left lTh">
                                        <span class="bold" id="full_price" style="color:red">
                                        €&nbsp;55.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Precio:</td>
                                <td class="price text-left" style="color:green;font-size:24px">
                                        <span class="bold" id="promoTotal">
                                        €&nbsp;45.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Entrega:</td>
                                <td class="fShipping text-left">GRATIS</td>
                            </tr>
                            <tr>
                                <td class="text-right">Ahorras:</td>
                                <td class="uSave text-left" id="discount">€&nbsp;10.00</td>
                            </tr>
                            </tbody>
                        </table>
                        <select name="priceSelect" class="priceSelect">
                            <option value="0">Cantidad: 1</option>
                            <option value="1">Cantidad: 2</option>
                            <option value="2">Cantidad: 3</option>
                        </select>
                        <p class="more_text_privacy">* Recomendado por la European Automobile Manufacturers Association</p>
                    </div>
                </div>

                <br>

                <div class="col-6">

                    <form name="orderform" id="orderform" action="" method="post">
                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAI_FwAAAAAAAAAAAASaFRrAAA"></iframe>

                        <!--<input type="hidden" value="0" id="product_selection" name="product_selection">
                        <input type="hidden" value="On" id="sameshipping" name="sameshipping">
                        <label for="billing_fname">Nombre</label>
                        <input id="billing_fname" type="text" name="billing_fname" placeholder="Nombre">

                        <label for="billing_lname">Apellidos</label>
                        <input id="billing_lname" type="text" name="billing_lname" placeholder="Apellidos" value="">

                        <label for="billing_street">Dirección</label>
                        <input id="billing_street" type="text" name="billing_street" placeholder="Dirección" value="">

                        <label for="billing_number">N.º</label>
                        <input id="billing_number" type="text" name="billing_number" placeholder="N.º" value="">

                        <label for="billing_apt">piso/puerta/bloque</label>
                        <input id="billing_apt" type="text" name="billing_apt" placeholder="piso/puerta/bloque" value="">


                        <label for="billing_state">Provincia</label>
                        <select id="billing_state" name="billing_state" value="">
                            <option value="">Provincia</option>
                            <option value="ALBACETE">ALBACETE</option>
                            <option value="ALICANTE">ALICANTE</option>
                            <option value="ALMERIA">ALMERIA</option>
                            <option value="ARABA/ALAVA">ARABA/ALAVA</option>
                            <option value="ASTURIAS">ASTURIAS</option>
                            <option value="AVILA">AVILA</option>
                            <option value="BADAJOZ">BADAJOZ</option>
                            <option value="BARCELONA">BARCELONA</option>
                            <option value="BIZKAIA">BIZKAIA</option>
                            <option value="BURGOS">BURGOS</option>
                            <option value="CACERES">CACERES</option>
                            <option value="CADIZ">CADIZ</option>
                            <option value="CANTABRIA">CANTABRIA</option>
                            <option value="CASTELLON">CASTELLON</option>
                            <option value="CIUDAD REAL">CIUDAD REAL</option>
                            <option value="CORDOBA">CORDOBA</option>
                            <option value="CORUÑA, A">CORUÑA, A</option>
                            <option value="CUENCA">CUENCA</option>
                            <option value="GIPUZKOA">GIPUZKOA</option>
                            <option value="GIRONA">GIRONA</option>
                            <option value="GRANADA">GRANADA</option>
                            <option value="GUADALAJARA">GUADALAJARA</option>
                            <option value="HUELVA">HUELVA</option>
                            <option value="HUESCA">HUESCA</option>
                            <option value="ILLES BALEARS">ILLES BALEARS</option>
                            <option value="JAEN">JAEN</option>
                            <option value="LEON">LEON</option>
                            <option value="LLEIDA">LLEIDA</option>
                            <option value="LUGO">LUGO</option>
                            <option value="MADRID">MADRID</option>
                            <option value="MALAGA">MALAGA</option>
                            <option value="MURCIA">MURCIA</option>
                            <option value="NAVARRA">NAVARRA</option>
                            <option value="OURENSE">OURENSE</option>
                            <option value="PALENCIA">PALENCIA</option>
                            <option value="PONTEVEDRA">PONTEVEDRA</option>
                            <option value="RIOJA, LA">RIOJA, LA</option>
                            <option value="SALAMANCA">SALAMANCA</option>
                            <option value="SEGOVIA">SEGOVIA</option>
                            <option value="SEVILLA">SEVILLA</option>
                            <option value="SORIA">SORIA</option>
                            <option value="TARRAGONA">TARRAGONA</option>
                            <option value="TERUEL">TERUEL</option>
                            <option value="TOLEDO">TOLEDO</option>
                            <option value="VALENCIA">VALENCIA</option>
                            <option value="VALLADOLID">VALLADOLID</option>
                            <option value="ZAMORA">ZAMORA</option>
                            <option value="ZARAGOZA">ZARAGOZA</option>
                        </select>




                        <label for="billing_city">Ciudad</label>
                        <input id="billing_city" type="text" name="billing_city" placeholder="Ciudad" value="">

                        <label for="billing_zip">Código postal</label>
                        <input id="billing_zip" type="text" name="billing_zip" placeholder="Código postal" value="">

                        <label for="billing_email">Correo electrónico</label>
                        <input id="billing_email" placeholder="Correo electrónico" type="text" name="billing_email">

                        <label for="billing_phone">Teléfono</label>
                        <input id="billing_phone" type="text" name="billing_phone" placeholder="Teléfono" value="">

                        <label for="billing_country">País</label>
                        <select name="billing_country" id="billing_country">
                            <option value="ES-Peninsula">Peninsula</option>
                            <option value="ES-Islas Baleares">Islas Baleares</option>
                        </select>
-->

                        <div class="attention">Atención: Entregamos dentro del territorio de la Península, excepto Islas Canarias, Ceuta y Melilla.</div>


                        <p class="some_info">¡Solo tardarás entre 1 y 2 meses en recuperar el dinero invertido en Magnufuel!</p>
                    </form>

                </div>
            </div>

            <div class="step2_ib3">
                <div class="subblock-step2">
                    <img src="images/Success_Guarantee.png" alt="img" class="guaranteeImg">
                    <h4 class="step2_ib2">GARANTÍA DE DEVOLUCIÓN DEL 100 % DE TU DINERO</h4>
                    <p>En MagnuFuel confiamos en la calidad de nuestro producto, y sabemos bien cuánto nos hemos esforzado para conseguir que ahorres dinero. Ahora bien, si pese a esto no quedas satisfecho con el artículo, podrás utilizar nuestra garantía de devolución del 100 % del importe pagado.</p>
                </div>
            </div>

            <div class="wrapper clearfix">



                <div class="subblock-step2">
                    <img src="images/free_delivery_orange.png" alt="img" class="deliveryImg">
                    <h4 class="step2_ib2"> GRATIS</h4>
                    <p>¡Envío gratuito! El producto te será entregado en un plazo de entre 1 y 3 días hábiles a contar desde la fecha de realización del pedido. La entrega se realizará en horario comercial.</p>
                    <br>
                </div>

                <div class="subblock-step2">
                    <img src="images/payment-icon.png" alt="img" class="paymentImg">
                    <h4 class="step2_ib2">MÉTODO DE PAGO</h4>
                    <p>Contra reembolso. Ahórrate la incertidumbre de los pagos por adelantado.</p>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!-- end of #wrapper -->



    <div class="border clearfix"></div>

    <div id="footer" class="clearfix">
        <div class="wrapper clearfix">
        </div>
    </div>

    <script type="text/javascript">
        jQuery(function($) {
            $('.priceSelect').on('change', function() {
                $('#product_selection').val($(this).val());
                $('#full_price').html(full_prices[$(this).val()]);
                $('#discount').html(discount[$(this).val()]);
                $('#promoTotal').html(prices[$(this).val()]);
            });
        });

    </script>

    <script type="text/javascript">
        var prices = [
            '&euro;&nbsp;45.00', '&euro;&nbsp;79.00', '&euro;&nbsp;109.00', '&euro;&nbsp;0.00'
        ];
        var quants = [
            '1', '2', '3', ''
        ];
        var full_prices = [
            '&euro;&nbsp;55.00', '&euro;&nbsp;110.00', '&euro;&nbsp;165.00', '&euro;&nbsp;0.00'
        ];
        var discount = [
            '&euro;&nbsp;10.00', '&euro;&nbsp;31.00', '&euro;&nbsp;56.00', '&euro;&nbsp;0.00'
        ];

    </script>

    <script type="text/javascript">
        $("#billing_address").on('focus', function() {
            $("body").append("<div class=\"tip\"><div class=\"toolTip\"><div class=\"toolTipArrow\"></div>Introduzca su dirección.</div></div>");
            $(".tip").css({
                top: $(this).offset().top + $(".tip").height() - 3,
                left: $(this).offset().left
            });
        });
        $("#billing_address").on('keydown', function() {
            $(".tip").remove();
        });

        $("#billing_address").on('blur', function() {
            $(".tip").remove();
        });

    </script>















</div>
