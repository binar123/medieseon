<div  id="step2"  style="display:none" >
    <link rel="stylesheet" href="!common_files/styles2_m.css" type="text/css">


    <div class="container">
        <nav>

            <img src="images/logo.png" alt="">

            <div class="clear"></div>
        </nav>
        <section class="col-12">

            <h1 class="center" style=" line-height: 1;overflow: hidden;font-weight: bold;      margin-bottom: -40px;  font-size: 28px;margin-top: 45px; text-align: center;">EMPIEZA A AHORRAR MÁS DE UN <strong style="display: inline-block;
    background: #cc1d00;
    color: #fff;
        padding: 0px 21px;"> 20%! </strong></h1>
            <div class="col-6" style="margin-top:140px">

                <div class="figure">
                    <div class="figure">
                        <img src="images/new_logo.jpg" class="product-img" alt="">
                    </div>
                </div>
                <div class="priceBoxContainer">
                    <table class="priceBox">
                        <tbody><tr>
                            <td class="text-right">Precio habitual:</td>
                            <td class="price text-left lTh" id="full_price">€&nbsp;55.00</td>
                        </tr>
                        <tr>
                            <td class="text-right">Precio:</td>
                            <td class="rPrice text-left" id="promoTotal" style="font-size:21px;color:green">€&nbsp;45.00</td>
                        </tr>
                        <tr>
                            <td class="text-right">Entrega:</td>
                            <td class="fShipping text-left" style="color:black">GRATIS</td>
                        </tr>
                        <tr>
                            <td class="text-right">Ahorras:</td>
                            <td class="uSave text-left" id="discount">€&nbsp;10.00</td>
                        </tr>
                        </tbody></table>

                    <div style="margin: 0px auto;     max-width: 280px; width: 77%; border: none;display: block;margin-right: auto;left: 36px;position: relative;">
                        <img src="images/1_sticker.png" alt="IMG" style="width:40%; right:22px;position:relative">

                        <img src="images/2_sticker.png" alt="IMG" style="width:40%; right:22px;position:relative">
                        <img src="images/ES.png" alt="IMG" style="width:40%; right:22px;position:relative">


                    </div>
                    <p style="text-align:center">* Recomendado por la European Automobile Manufacturers Association</p>

                    <select class="priceSelect">
                        <option value="0">Cantidad: 1</option>
                        <option value="1">Cantidad: 2</option>
                        <option value="2">Cantidad: 3</option>
                    </select>
                </div>
                <hr>
            </div>
            <div class="col-6">
                <div class="formContainer">


                    <form name="orderform" id="orderform" action="" method="post">

                        <div class="option" >
                            <iframe scrolling="no"
                                    style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                    src="http://abcdg.pro/forms/?target=-4AAIJIAI_FwAAAAAAAAAAAASaFRrAAA"></iframe>
                        </div>
                        <!--<input type="hidden" value="0" id="product_selection" name="product_selection">
                        <input type="hidden" value="On" id="sameshipping" name="sameshipping">
                        <label for="billing_fname">Nombre</label>
                        <input id="billing_fname" type="text" name="billing_fname" placeholder="Nombre">

                        <label for="billing_lname">Apellidos</label>
                        <input id="billing_lname" type="text" name="billing_lname" placeholder="Apellidos" value="">

                        <label for="billing_street">Dirección</label>
                        <input id="billing_street" type="text" name="billing_street" placeholder="Dirección" value="">

                        <label for="billing_number">N.º</label>
                        <input id="billing_number" type="text" name="billing_number" placeholder="N.º" value="">

                        <label for="billing_apt">piso/puerta/bloque</label>
                        <input id="billing_apt" type="text" name="billing_apt" placeholder="piso/puerta/bloque" value="">


                        <label for="billing_state">Provincia</label>
                        <select id="billing_state" name="billing_state" value="">
                            <option value="">Provincia</option>
                            <option value="ALBACETE">ALBACETE</option>
                            <option value="ALICANTE">ALICANTE</option>
                            <option value="ALMERIA">ALMERIA</option>
                            <option value="ARABA/ALAVA">ARABA/ALAVA</option>
                            <option value="ASTURIAS">ASTURIAS</option>
                            <option value="AVILA">AVILA</option>
                            <option value="BADAJOZ">BADAJOZ</option>
                            <option value="BARCELONA">BARCELONA</option>
                            <option value="BIZKAIA">BIZKAIA</option>
                            <option value="BURGOS">BURGOS</option>
                            <option value="CACERES">CACERES</option>
                            <option value="CADIZ">CADIZ</option>
                            <option value="CANTABRIA">CANTABRIA</option>
                            <option value="CASTELLON">CASTELLON</option>
                            <option value="CIUDAD REAL">CIUDAD REAL</option>
                            <option value="CORDOBA">CORDOBA</option>
                            <option value="CORUÑA, A">CORUÑA, A</option>
                            <option value="CUENCA">CUENCA</option>
                            <option value="GIPUZKOA">GIPUZKOA</option>
                            <option value="GIRONA">GIRONA</option>
                            <option value="GRANADA">GRANADA</option>
                            <option value="GUADALAJARA">GUADALAJARA</option>
                            <option value="HUELVA">HUELVA</option>
                            <option value="HUESCA">HUESCA</option>
                            <option value="ILLES BALEARS">ILLES BALEARS</option>
                            <option value="JAEN">JAEN</option>
                            <option value="LEON">LEON</option>
                            <option value="LLEIDA">LLEIDA</option>
                            <option value="LUGO">LUGO</option>
                            <option value="MADRID">MADRID</option>
                            <option value="MALAGA">MALAGA</option>
                            <option value="MURCIA">MURCIA</option>
                            <option value="NAVARRA">NAVARRA</option>
                            <option value="OURENSE">OURENSE</option>
                            <option value="PALENCIA">PALENCIA</option>
                            <option value="PONTEVEDRA">PONTEVEDRA</option>
                            <option value="RIOJA, LA">RIOJA, LA</option>
                            <option value="SALAMANCA">SALAMANCA</option>
                            <option value="SEGOVIA">SEGOVIA</option>
                            <option value="SEVILLA">SEVILLA</option>
                            <option value="SORIA">SORIA</option>
                            <option value="TARRAGONA">TARRAGONA</option>
                            <option value="TERUEL">TERUEL</option>
                            <option value="TOLEDO">TOLEDO</option>
                            <option value="VALENCIA">VALENCIA</option>
                            <option value="VALLADOLID">VALLADOLID</option>
                            <option value="ZAMORA">ZAMORA</option>
                            <option value="ZARAGOZA">ZARAGOZA</option>
                        </select>




                        <label for="billing_city">Ciudad</label>
                        <input id="billing_city" type="text" name="billing_city" placeholder="Ciudad" value="">

                        <label for="billing_zip">Código postal</label>
                        <input id="billing_zip" type="text" name="billing_zip" placeholder="Código postal" value="">

                        <label for="billing_email">Correo electrónico</label>
                        <input id="billing_email" placeholder="Correo electrónico" type="text" name="billing_email">

                        <label for="billing_phone">Teléfono</label>
                        <input id="billing_phone" type="text" name="billing_phone" placeholder="Teléfono" value="">

                        <label for="billing_country">País</label>
                        <select name="billing_country" id="billing_country">
                            <option value="ES-Peninsula">Peninsula</option>
                            <option value="ES-Islas Baleares">Islas Baleares</option>
                        </select>-->


                        <div class="attention">Atención: Entregamos dentro del territorio de la Península, excepto Islas Canarias, Ceuta y Melilla.</div>


                        <p class="some_info">¡Solo tardarás entre 1 y 2 meses en recuperar el dinero invertido en Magnufuel!</p>
                    </form>

                </div>
            </div>

            <div class="option">
                <img class="pull-left" src="images/free_delivery_orange.png" width="100" alt="">
                <h4>GRATIS</h4>
                <p>¡Envío gratuito! El producto te será entregado en un plazo de entre 1 y 3 días hábiles a contar desde la fecha de realización del pedido. La entrega se realizará en horario comercial.</p>
            </div>
            <div class="option">
                <img class="pull-left" src="images/Success_Guarantee.png" width="100" alt="">
                <h4>GARANTÍA DE DEVOLUCIÓN DEL 100 % DE TU DINERO</h4>
                <p>En MagnuFuel confiamos en la calidad de nuestro producto, y sabemos bien cuánto nos hemos esforzado para conseguir que ahorres dinero. Ahora bien, si pese a esto no quedas satisfecho con el artículo, podrás utilizar nuestra garantía de devolución del 100 % del importe pagado.</p>
            </div>
            <div class="option">
                <img class="pull-left" src="images/payment-icon.png" width="100" alt="">
                <h4>MÉTODO DE PAGO</h4>
                <p>Contra reembolso. Ahórrate la incertidumbre de los pagos por adelantado.</p>
            </div>
            <br>
            <br>

            <div class="clear"></div>




            <script type="text/javascript">
                jQuery(function($){

                    $('.priceSelect').on('change', function(){
                        $('#product_selection').val($(this).val());


                        $('#full_price').html( full_prices[$(this).val()] );
                        $('#discount').html( discount[$(this).val()] );
                        $('#promoTotal').html( prices[$(this).val()] );
                    });
                });
            </script>

            <script type="text/javascript">
                var prices = [
                    '&euro;&nbsp;45.00', '&euro;&nbsp;79.00', '&euro;&nbsp;109.00', '&euro;&nbsp;0.00'
                ];

                var quants = [
                    '1', '2', '3', ''
                ];

                var full_prices = [
                    '&euro;&nbsp;55.00', '&euro;&nbsp;110.00', '&euro;&nbsp;165.00', '&euro;&nbsp;0.00'
                ];

                var discount = [
                    '&euro;&nbsp;10.00', '&euro;&nbsp;31.00', '&euro;&nbsp;56.00', '&euro;&nbsp;0.00'
                ];
            </script>

            <script type="text/javascript">
                $("#billing_address").on('focus', function() {
                    $("body").append("<div class=\"tip\"><div class=\"toolTip\"><div class=\"toolTipArrow\"></div>Introduzca su dirección.</div></div>");
                    $(".tip").css({
                        top: $(this).offset().top + $(".tip").height() - 3,
                        left: $(this).offset().left
                    });
                });
                $("#billing_address").on('keydown', function() {
                    $(".tip").remove();
                });

                $("#billing_address").on('blur', function() {
                    $(".tip").remove();
                });
            </script>
        </section>
    </div>



</div>