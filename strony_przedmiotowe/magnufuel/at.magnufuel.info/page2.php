<div  id="step2"  style="display:none" >




    <div class="header" id="step2_ib">

        <div class="wrapper clearfix">
            <div class="step2 clearfix">

                <div class="holder-form">
                    <h1 class="center title_up">STARTEN SIE ZU SPAREN MEHR ALS <strong class="step2_ib1">20%!</strong> SPRIT AB HEUTE.
                        <img src="images/AT.png" class="new_logo4" alt="">
                    </h1>
                </div>


                <div class="col-6">
                    <img src="images/prod_s2.png" class="prod_s2" alt="">
                    <br>


                    <div class="priceBoxContainer">
                        <img src="images/2_sticker.png" class="new_logo" alt="">
                        <img src="images/1_sticker.png" class="new_logo2" alt="">

                        <table class="priceBox">
                            <tbody>
                            <tr>
                                <td class="text-right">Standard preis:</td>
                                <td class="rPrice text-left lTh">
                                        <span class="bold" id="full_price" style="color:red">
                                        €&nbsp;55.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Preis:</td>
                                <td class="price text-left" style="color:green;font-size:24px">
                                        <span class="bold" id="promoTotal">
                                        €&nbsp;45.00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">Versand:</td>
                                <td class="fShipping text-left">KOSTENLOS</td>
                            </tr>
                            <tr>
                                <td class="text-right">Sie sparen:</td>
                                <td class="uSave text-left" id="discount">€&nbsp;10.00</td>
                            </tr>
                            </tbody>
                        </table>
                        <select name="priceSelect" class="priceSelect">
                            <option value="0">Anzahl: 1</option>
                            <option value="1">Anzahl: 2</option>
                            <option value="2">Anzahl: 3</option>
                        </select>
                        <p class="more_text_privacy">* Empfohlen von der European Automobile Manufacturers Association</p>
                    </div>
                </div>

                <br>

                <div class="col-6">


                    <form name="orderform" id="orderform" class="order-form header-form" action="" method="post" >

                        <iframe scrolling="no"
                                style="border:0; left:0; right:0; bottom:0; width:100%; height:220px;"
                                src="http://abcdg.pro/forms/?target=-4AAIJIAI_FwAAAAAAAAAAAASaFRrAAA"></iframe>


                        <p class="eli-text">Die Rechnung wird Ihrem Paket beigefügt. Bitte beachten Sie, dass Sie beim Kauf auf Rechnung nach Erhalt der Ware 14 Tage bis zur Zahlung haben.</p>
                    </form>


                    <p class="some_info">Die ausgegebene Summe für den Kauf von Magnufuel zahlt sich in 1-2 Monaten aus!</p>


                </div>
            </div>
        </div>



        <div class="wrapper clearfix">

            <div class="step2_ib3">
                <div class="subblock-step2">
                    <img src="images/Success_Guarantee.png" alt="img" class="guaranteeImg">
                    <h4 class="step2_ib2">100% GARANTIE</h4>
                    <p>Wir sind fest von der MagnuFuel Qualität überzeugt, da wir viel Mühe und Sorgfalt investiert haben, damit es zu einem  kostensparenden Gerät wird. Sollten Probleme bei der Nutzung von Ihrem MagnuFuel Gerät auftreten, können Sie von Ihrer Geld-Zurück-Garantie Gebrauch machen.</p>
                </div>
            </div>

            <div class="subblock-step2">
                <img src="images/free_delivery_orange.png" alt="img" class="deliveryImg">
                <h4 class="step2_ib2"> KOSTENLOS</h4>
                <p>Wir bieten Ihnen einen freien Versand, um Ihre Kosten zu minimieren! Nachdem Sie Ihre Bestellung aufgegeben haben, werden wir Ihnen das Produkt innerhalb von 1-5 Werktagen zustellen, abhängig vom Zustellort. Die Auslieferung erfolgt während der üblichen Geschäftszeiten. Bitte stellen Sie daher sicher, dass Sie oder eine beauftragte Person an der angegebenen Adresse anzutreffen ist, um die Ware entgegenzunehmen.</p>
                <br>
            </div>

            <div class="subblock-step2">
                <img src="images/payment-icon.png" alt="img" class="paymentImg">
                <h4 class="step2_ib2">ZAHLUNGSMETHODE</h4>
                <p> Zahlung per Rechnung(Kauf auf Rechnung und PayPal).</p>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- end of #wrapper -->



<div class="border clearfix"></div>

<div id="footer" class="clearfix">
    <div class="wrapper clearfix">
    </div>
</div>

<script type="text/javascript">
    jQuery(function($) {
        $('.priceSelect').on('change', function() {
            $('#product_selection').val($(this).val());
            $('#full_price').html(full_prices[$(this).val()]);
            $('#discount').html(discount[$(this).val()]);
            $('#promoTotal').html(prices[$(this).val()]);
            selected_quantity_id = $(this).val();
        });
    });

</script>

<script type="text/javascript">
    var prices = [
        '&euro;&nbsp;45.00', '&euro;&nbsp;79.00', '&euro;&nbsp;109.00', '&euro;&nbsp;0.00'
    ];
    var quants = [
        '1', '2', '3', ''
    ];
    var full_prices = [
        '&euro;&nbsp;55.00', '&euro;&nbsp;110.00', '&euro;&nbsp;165.00', '&euro;&nbsp;0.00'
    ];
    var discount = [
        '&euro;&nbsp;10.00', '&euro;&nbsp;31.00', '&euro;&nbsp;56.00', '&euro;&nbsp;0.00'
    ];

</script>















</div>
