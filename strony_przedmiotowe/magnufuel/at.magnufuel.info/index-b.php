<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect();
?>

<?php if ($detect->isMobile()) { ?>


    <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Offizielle Webseite | Magnufuel</title>
        <meta name="keywords" content="Kraftstoff sparen Wirtschaftlichkeit Autogas Benzin Benzinsparer Spritsparer Gerät spritsparendes Gerät Diesel Effizienz Kraftstoffeffizienz Motor Kraftstoffschlauch Kraftstoffersparnis Kraftstoff sparendes Gerät Neodym-Magnet Magnet reduziert Emissionen Kohlenwasserstoffmoleküle Brennstoffmoleküle weniger Verbrauch niedriger Kraftstoffverbrauch Reduzierung von CO Reduzierung von CH Hubraum vollständige Verbrennung">
        <meta name="description" content="Magnufuel Kraftstoffsparendes Gerät">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="no">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../../favicon.ico">

        <link rel="stylesheet" href="!common_files/styles_m.css" type="text/css">

        <script src="!common_files/jquery.js" type="text/javascript"></script>
        <script src="!common_files/navigation.js" type="text/javascript"></script>

        <script src="!common_files/popup_m.js.php"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('.glyphicon-shopping-cart').on('click',function(){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
            /**/
        </script>


    </head>

    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;">

<div id="visible_first">

   
    <header class="header">

        <div class="container">
            <div class="col">

                <h2 class="style_cib1">Starten Sie zu sparen</h2>
                <h3 class="style_cib2"> <strong> mehr als <strong class="bg-orange">20%</strong> Sprit  ab heute.</strong></h3>
            </div>

        </div>
    </header>



    <div class="container">
        <nav>

            <img src="images/logo.png" alt="logo">
            <div class="pull-right shoppingCard">
                <a class="glyphicon glyphicon-shopping-cart"></a>
            </div>

            <div class="clear"></div>
        </nav>
        <section class="col-12">
            <article>
                <div class="figure">
                    <img src="images/ezgif.com-crop.gif" class="product-img" alt="" id="style_cib33">

                    <img src="images/strelka_2.png" class="strelka2" alt="">
                </div>
                <div class="style_cib4">
                    <img src="images/1_sticker.png" alt="IMG" class="style_cib5">
                    <img src="images/2_sticker.png" alt="IMG" class="style_cib5"></div>
                <p class="style_cib6">* Empfohlen von der European Automobile Manufacturers Association</p>
                <p class="text-center">
                    Wenn der Kraftstoff das starke Magnetfeld durchquert, werden seine Kohlenwasserstoffketten in einfachere Komponente aufgelöst, die nachfolgend aktiviert werden.
                </p>

                <form name="form-index" class="form-inverse country" id="form-index" action="https://www.magnufuel.com/DE_official_T11/m/step2.php" method="post" onsubmit="return template_validator({})">

                    <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                    <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                </form>

                <button class="form-index rushOrder"> Bestellen </button>

                <div class="text-center mblock2">
                    <div class="row bg-inverse">
                        <div class="container212">
                            <ul class="list-ico">
                                <li>
                                    <img src="images/horse.png" class="ico-horse" alt="">
                                    <p class="style_cib7">Erhöhen Sie die Motorleistung um bis zu 5 PS </p>
                                </li>
                                <li>

                                    <img src="images/benz.png" class="ico-benz" alt="">
                                    <p class="style_cib8">  <br> Reduzierung des Kraftstoffverbrauchs um bis zu 20%</p>
                                </li>
                                <li>

                                    <img src="images/list.png" class="ico-lists" alt="">
                                    <p class="style_cib9">Reduzierung von CO und CH Emissionen um bis zu 40% - 50% </p>
                                </li>
                                <li>
                                    <img src="images/list_special.png" class="ico-bis" alt="">
                                    <p class="style_cib10">Das Gerät ist zertifiziert</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="#sert" class="JUMP"><p>Klicken Sie auf das Zertifikat zu sehen</p></a>

                <h3 class="text-center" id="style_cib11">
                    Wie funktioniert Magnufuel?
                </h3>
                <div class="text-center">
                    <img src="images/new_logo2.jpg" class="style_cib2016" alt="img">
                </div>
                <p class="text-center" id="style_cib13">
                    Jeder Kraftstoff, ungeachtet davon, wo es gelagert wird, ändert seine Substanz durch den Enfluss von Temperatur und Feuchtigkeit. Dieser Einfluss äußert sich entweder in einer Ausdehnung oder einer Verdichtung des Kraftstoffs. Die Kohlenwasserstoffmoleküle (die Basis von jedem Kraftstoff) beginnen dabei nebeneinander so zu gravitieren, dass sie Mokelülguppen bilden – „Anhäufungen von Molekülen“. Die Anhäufungen können nicht vollständig verbrennen, weil ein Teil der Moleküle außerhalb der Sauerstoffreichweite bleibt. Wenn aber der Kraftstoff den Bereich durchquert, wo Magnufuel  installiert ist, dann zerteilt die Frequenz der Magentresonanz, die durch das Gerät erzeugt wird, die gebildeten Anhäufungen in einzelne Moleküle und lädt sie positiv auf. Dadruch gelangen die Sauerstoffmoleküle zu jeder Treibstoffmoleküle, was den vollständigen Verbrennungsprozess vom Kraftstoff-Sauerstoff Gemisch begünstigt.
                    <br> <br> Das Ergebnis ist ein niedrigerer Kraftstoffverbrauch und die Reduzierung der schädlichen Emissionen. Den Kern vom Gerät Magnufuel  bildet das Neodym Magnet (NdFeB), das aus zwei Teilen besteht. Nach heutigem Stand können diesen Effekt allerdings nur wenige Neodym Magnete erzeugen, die durch eine spezielle, patentierte Technologie hergestellt werden (USA Patent № 4.802.931, 4.496.395, 7.458.412, ausgestellt vom Unternehmen "General Motors“).
                    <br> <br> Magnete anderer Hersteller (meistens billige Nachahmungen) können dieses Ergebnis nicht erreichen.
                    <br>
                </p><div class="style_cib14">
                    <br><h1> Einbau</h1> <br>Die Montage erfordert keine speziellen Fähigkeiten und Kenntnisse. Das Wichtigste ist, den richtigen Schlauch zu erkennen. Das Gerät muss an den Kraftstoffschlauch angebracht werden. <br>• Öffnen Sie die Motorhaube Ihres Autos, finden Sie den Kraftstoffschlauch und wählen Sie eine Stelle, wo Sie das Gerät installieren möchten; <br>• Befestigen Sie Magnufuel an der gewünschten Stelle am Kraftstoffschlauch. Der Schlauch sollte zwischen den beiden Teilen des Geräts liegen. <br>• Für mehr Stabilität verwenden Sie den Kunststoffkragen, der im Paket enthalten ist. Beachten Sie dabei, dass der Kunststoffkragen in einer kleinen Öffnung im Zentralkanal des Geräts passt. Ziehen Sie den Kragen fest und wenn notwendig, schneiden Sie den überflüssigen Teil weg; <br>
                </div>
                <img class="style_cib26" src="images/ezgif.com-crop.gif" alt="">

                <br>
                <br>
                <a href="#sert" class="JUMP"><p>Klicken Sie auf das Zertifikat zu sehen</p></a>

                <div class="style_cib24">
                    <button class="form-index rushOrder" > Bestellen </button>
                </div>

                <h2 class="style_cib15">Hauptvorteile</h2>
                <div class="magnit col-4">

                </div>
                <br>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_1.jpg" alt="">
                    <p class="magnit-text">Einfache Montage. Die Installation nimmt nur 5 Minuten in Anspruch.</p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_2.jpg" alt="">
                    <p class="magnit-text">Es verursacht keine Abgase. Das bedeutet, dass es das Leben der Kolbenringe  signifikant verlängert. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_3.jpg" alt="">
                    <p class="magnit-text">Es verlängert deutlich das Leben auch vom  Katalysator. </p>
                </div>
                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_4.jpg" alt="">
                    <p class="magnit-text">Bewiesene Effizienz </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_5.jpg" alt="">
                    <p class="magnit-text">Bald nach Installation vom Magnufuel  kommt es zu einer annährend vollständigen Verbrennung.</p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_6.jpg" alt="">
                    <p class="magnit-text">Sie können Warnmeldungen wie „Motor prüfen” vergessen, die hauptsächlich damit verbunden sind, dass  der Kraftstoffstand niedrig ist oder die Verbrennung unvollständig ist. </p>
                </div>

                <div class="magnit col-4">
                    <img class="pull-left" src="images/ico_7.jpg" alt="">
                    <p class="magnit-text">Die ausgegebene Summe für den Kauf von  Magnufuel  zahlt sich in 1-2 Monaten aus!</p>
                </div>

                <div class="clear"></div>

                <div class="text-center">
                    <a href="#sert" class="JUMP"><p>Klicken Sie auf das Zertifikat zu sehen</p></a>

                    <button class="form-index rushOrder" > Bestellen </button>


                </div>
                <div class=" sub-title">
                    <p class="style_cib16">FEEDBACK VON UNSEREN KUNDEN</p>
                </div>

                <div class="doctors-2 col-6">
                    <img class="pull-left" src="images/driver3.jpg" alt="">
                    <p class="style_cib17"> Andreas Hoffmann 32 Jahre alt
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>war </small>  10 l<i class="slash"></i></span> /
                        <span class="text-orange"><small>ist</small> 7 l</span>
                    </div>
                    <hr>
                    <strong>Vor 4 Tagen habe ich Magnufuel  auf Hyundai Accent (2012) montiert. In der Stadt hatte ich früher einen Verbrauch von 10 Litern, jetzt 7-8. Auf der Autobahn erreiche ich jetzt 5,8 l per 100 km. Gestern bin ich von München nach Fulda gefahren und hab 20 Liter verbraucht. Dank diesem kleinen Wunder.</strong>
                </div>




                <div class="doctors-2 col-6">
                    <img class="pull-right" src="images/driver2.jpg" alt="">
                    <p class="style_cib19">Michael Lang 46 Jahre alt
                        <br>
                    </p><div class="percents">
                        <span class="style_cib18"><small>war</small> 9 l<i class="slash"></i></span> /
                        <span class="text-orange"><small>ist</small>  7,5 l</span>
                    </div>
                    <hr>
                    <strong>Dacia. Das Ergebnis kam nach dem zweiten Auftanken. Der Motor begann leiser zu arbeiten, es gab kein Vibrieren mehr und die Leistung verbesserte sich. Wenn ich eine kurze Fahrt unternehme, spare ich zwischen 1 und 1,5 l. Ich bin zufrieden.</strong>
                    <hr>
                </div>



                <div class="clear"></div>

                <div class="sub-title-2">
                    <p class="style_cib20">
                        Magnufuel kann an verschiedenen Fahrzeugen eingebaut werden <br>
                        <br>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib1">

                    <img class="circle-img" src="images/img_thumb.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Autos</strong>
                    </p>
                </div>
                <div class="circle col-6 text-center sub-title-2" id="style_cib2">

                    <img class="circle-img" src="images/img_thumb2.jpg" alt="">
                    <p>
                        <strong class="style_cib22">LKWs</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib34">

                    <img class="circle-img" src="images/img_thumb3.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Busse und Transporter</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib4">

                    <img class="circle-img" src="images/img_thumb4.jpg" alt="">
                    <p>
                        <strong class="style_cib22">Motorboote und andere Wasserfahrzeuge</strong>
                    </p>
                </div>


                <div class="circle col-6 text-center sub-title-2" id="style_cib5">

                    <img class="circle-img" src="images/img_thumb5.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Motorräder</strong>
                    </p>
                </div>

                <div class="circle col-6 text-center sub-title-2" id="style_cib6">

                    <img class="circle-img" src="images/img_thumb6.jpg" alt="">
                    <p>
                        <strong class="style_cib22"> Landwirtschaftsmaschinen</strong>
                    </p>
                </div>

                <div class="clear"></div>

                <div class="sub-title-2 text-center">
                    <span id="sert" class="anchor"></span>
                    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=auto, height=auto, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">

                        <img alt="" class="cert" src="images/CE_BIG_M.jpg">
                    </a>
                    <p class="style_cib23"><br>  Magnufuel bestellen</p>
                </div>



                <button class="form-index rushOrder" > Bestellen </button>


                <div class="style_cib25">
                    <img src="images/1_sticker.png" alt="IMG">
                    <img src="images/2_sticker.png" alt="IMG">
                </div>
                <p style="text-align:center">* Empfohlen von der European Automobile Manufacturers Association</p>


            </article>

            <div class="clear"></div>

        </section>
        <hr> <footer class="col-12 links">

         </footer>
    </div>






    <div id="popUp" style="display: block; right: -0.0000000000000028729427326909663px;">In diesem Moment hat diese Seite 223 Besucher.</div>
    </div>
<?php include 'page2m.php';?>

    </body></html>


<?php } else { ?>


    <html><head>
        <meta charset="UTF-8">
        <title>Offizielle Webseite | Magnufuel</title>
        <meta name="keywords" content="Kraftstoff sparen Wirtschaftlichkeit Autogas Benzin Benzinsparer Spritsparer Gerät spritsparendes Gerät Diesel Effizienz Kraftstoffeffizienz Motor Kraftstoffschlauch Kraftstoffersparnis Kraftstoff sparendes Gerät Neodym-Magnet Magnet reduziert Emissionen Kohlenwasserstoffmoleküle Brennstoffmoleküle weniger Verbrauch niedriger Kraftstoffverbrauch Reduzierung von CO Reduzierung von CH Hubraum vollständige Verbrennung">
        <meta name="description" content="Magnufuel Kraftstoffsparendes Gerät">
        <link rel="stylesheet" href="!common_files/all.css" id="style_page1">
        <meta name="format-detection" content="telephone=no">

        <script src="!common_files/popup.js.php"></script>
         <script type="text/javascript" src="!common_files/jquery.min.js"></script>
        <script src="!common_files/timer.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.footer').append('<div class="footer" style="background-color:black "><span>&copy; 2016 Copyright. All rights reserved.' +
                    '</span><br><a href="/!common_files/policy_en.html" target="_blank">Privacy policy</a>' +
                    ' | <a href="http://ac-feedback.com/report_form/">Report</a>');

                $('button').on('click',function(e){
                    e.preventDefault();
                    $("#style_page1").attr("disabled", "disabled");
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '!common_files/styles.css') );

                    $('#step2').show();
                    $('#visible_first').hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                })
            });
    /**/
    </script>
    </head>
    <body oncontextmenu="return false" ondragstart="return false;" ondrop="return false;" >
<div id="visible_first">


    <header class="header">
        <div class="container">
            <div class="col" id="style_ib1">
                <img src="images/logo.png" id="style_ib2" alt="logo">
                <img src="images/AT.png" style="width:60px;top:20px;" id="style_ib3" alt="logo">
                <h2 class="style_ib1">Starten Sie zu sparen</h2>
                <h3 class="style_ib2"> mehr als <strong class="bg-orange">20%</strong> Sprit  ab heute.</h3>
            </div>
            <img src="images/ezgif.com-crop.gif" class="pull-right" id="style_ib4" alt="">
            <img src="images/1_sticker.png" class="style_ib4" alt="">
            <img src="images/2_sticker.png" class="style_ib5" alt="">
            <p class="pull-right">Wenn der Kraftstoff das starke Magnetfeld durchquert, werden seine Kohlenwasserstoffketten in einfachere Komponente aufgelöst, die nachfolgend aktiviert werden.</p>
            <p class="style_ib6">* Empfohlen von der European Automobile Manufacturers Association</p>
        </div>
        <button type="button" class="btn order" id="head_button_ord"> Bestellen </button>
    </header>
    <a onclick="window.open('images/Certificate_of_Conformity.jpg', 'windownameImg', 'width=500, height=707, scrollbars=0'); return false;" id="sert_desk" href="javascript: void(0)">
        <img alt="" id="sertificate" src="images/CE.jpg">
        <p>Klicken Sie um zu vergrößern</p>
    </a>
    <div id="popWindow" style="opacity: 1;">Soeben wurde eine Bestellung aus <b>Hannover</b> aufgegeben.</div>
    <!--popWindow-->
    <section class="main">
        <div class="row bg-inverse">
            <div class="container">
                <ul class="list-ico">
                    <li>
                        <i class="ico-horse"></i>
                        <p>Erhöhen Sie die Motorleistung um bis zu 5 PS</p>
                    </li>
                    <li class="style_ib7">
                        <i class="ico-benz"></i>
                        <p class="style_ib8"> <br> Reduzierung des Kraftstoffverbrauchs um bis zu 20%</p>
                    </li>
                    <li>
                        <i class="ico-lists"></i>
                        <p>Reduzierung von CO und CH Emissionen um bis zu 40% - 50%</p>
                    </li>
                    <li>
                        <i class="ico-bis"></i>
                        <p>Das Gerät ist zertifiziert</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row bg1">
            <div class="container">
                <table>
                    <tbody>
                    <tr>

                        <td class="tovar">
                            <h4>Kaufen Sie Magnufuel jetzt <strong class="text-red"> zum reduzierten  </strong> Preis</h4>


                            <div class="eTimer">
                                <div class="countdown_box">
                                    <span class="order-countdown-hours" id="hours"></span>
                                    <span>Stunden</span>
                                    <span class="order-countdown-minutes" id="minutes"></span>
                                    <span>Minuten</span>
                                    <span class="order-countdown-seconds" id="seconds"></span>
                                    <span>Sekunden</span>
                                </div>
                            </div>
                            <div class="row">
                                <button type="button"  class="btn order"> Bestellen </button>

                                <br>
                                <br>

                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Wie funktioniert Magnufuel?</h3>
                <article>
                    <img src="images/img_article.png" class="pull-left" alt="">
                    <p> Jeder Kraftstoff, ungeachtet davon, wo es gelagert wird, ändert seine Substanz durch den Enfluss von Temperatur und Feuchtigkeit. Dieser Einfluss äußert sich entweder in einer Ausdehnung oder einer Verdichtung des Kraftstoffs. Die Kohlenwasserstoffmoleküle (die Basis von jedem Kraftstoff) beginnen dabei nebeneinander so zu gravitieren, dass sie Mokelülguppen bilden – „Anhäufungen von Molekülen“. Die Anhäufungen können nicht vollständig verbrennen, weil ein Teil der Moleküle außerhalb der Sauerstoffreichweite bleibt. Wenn aber der Kraftstoff den Bereich durchquert, wo Magnufuel  installiert ist, dann zerteilt die Frequenz der Magentresonanz, die durch das Gerät erzeugt wird, die gebildeten Anhäufungen in einzelne Moleküle und lädt sie positiv auf. Dadruch gelangen die Sauerstoffmoleküle zu jeder Treibstoffmoleküle, was den vollständigen Verbrennungsprozess vom Kraftstoff-Sauerstoff Gemisch begünstigt.</p>
                    <p><br> Das Ergebnis ist ein niedrigerer Kraftstoffverbrauch und die Reduzierung der schädlichen Emissionen. Den Kern vom Gerät Magnufuel  bildet das Neodym Magnet (NdFeB), das aus zwei Teilen besteht. Nach heutigem Stand können diesen Effekt allerdings nur wenige Neodym Magnete erzeugen, die durch eine spezielle, patentierte Technologie hergestellt werden (USA Patent № 4.802.931, 4.496.395, 7.458.412, ausgestellt vom Unternehmen "General Motors“).</p>
                </article>
                <article>
                    <img src="images/product_magnu.jpg" class="style_ib10" alt="">
                    <p><br> Magnete anderer Hersteller (meistens billige Nachahmungen) können dieses Ergebnis nicht erreichen.</p>
                    <br><h1> Einbau</h1> <br>Die Montage erfordert keine speziellen Fähigkeiten und Kenntnisse. Das Wichtigste ist, den richtigen Schlauch zu erkennen. Das Gerät muss an den Kraftstoffschlauch angebracht werden. <br>• Öffnen Sie die Motorhaube Ihres Autos, finden Sie den Kraftstoffschlauch und wählen Sie eine Stelle, wo Sie das Gerät installieren möchten; <br>• Befestigen Sie Magnufuel an der gewünschten Stelle am Kraftstoffschlauch. Der Schlauch sollte zwischen den beiden Teilen des Geräts liegen. <br>• Für mehr Stabilität verwenden Sie den Kunststoffkragen, der im Paket enthalten ist. Beachten Sie dabei, dass der Kunststoffkragen in einer kleinen Öffnung im Zentralkanal des Geräts passt. Ziehen Sie den Kragen fest und wenn notwendig, schneiden Sie den überflüssigen Teil weg; <br>
                    <div class="text-center">
                        <button type="button"  class="btn order"> Bestellen </button>



                    </div>
                </article>
            </div>
        </div>
        <div class="row row-grey">
            <div class="container">
                <h3>Hauptvorteile</h3>
                <ul class="list">
                    <li>
						<span class="icon-circle"><i class="ico-plane"></i>
</span>Einfache Montage. Die Installation nimmt nur 5 Minuten in Anspruch.</li>
                    <li>
						<span class="icon-circle"><i class="ico-service"></i>
</span>Es verursacht keine Abgase. Das bedeutet, dass es das Leben der Kolbenringe  signifikant verlängert.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-watch"></i>
</span>Es verlängert deutlich das Leben auch vom  Katalysator.
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-bisines_d"></i>
</span>Bewiesene Effizienz
                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-clock"></i>
</span>Bald nach Installation vom Magnufuel  kommt es zu einer annährend vollständigen Verbrennung.</li>
                    <li>
						<span class="icon-circle"><i class="ico-sun"></i>
</span>Sie können Warnmeldungen wie „Motor prüfen” vergessen, die hauptsächlich damit verbunden sind, dass  der Kraftstoffstand niedrig ist oder die Verbrennung unvollständig ist.

                    </li>
                    <li>
						<span class="icon-circle"><i class="ico-payment"></i>
</span>Die ausgegebene Summe für den Kauf von  Magnufuel  zahlt sich in 1-2 Monaten aus!</li>
                </ul>
                <div class="row bg-img">


                    <button type="button"  class="btn order"> Bestellen <br> </button>

                    <br>
                    <br>
                    <br>

                </div>
            </div>
        </div>
        <div class="row bg2">
            <div class="container">
                <h3>FEEDBACK VON UNSEREN KUNDEN</h3>
                <div class="row-thumb">
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Andreas Hoffmann 32 Jahre alt</span>
                                <img src="images/driver3.jpg" class="img" alt="driver">
                                <span>Kraftstoffverbrauch vor und nach der Installation von Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">10 l<i class="slash">/</i>
<small>war</small>
</span>
                                    <span class="text-orange">7 l
<small>ist</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Vor 4 Tagen habe ich Magnufuel  auf Hyundai Accent (2012) montiert. In der Stadt hatte ich früher einen Verbrauch von 10 Litern, jetzt 7-8. Auf der Autobahn erreiche ich jetzt 5,8 l per 100 km. Gestern bin ich von München nach Fulda gefahren und hab 20 Liter verbraucht. Dank diesem kleinen Wunder.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Michael Lang 46 Jahre alt</span>
                                <img src="images/driver2.jpg" class="img" alt="driver">
                                <span>Kraftstoffverbrauch vor und nach der Installation von Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">9 l<i class="slash">/</i>
<small>war</small>
</span>
                                    <span class="text-orange">7,5 l
<small>ist</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Dacia. Das Ergebnis kam nach dem zweiten Auftanken. Der Motor begann leiser zu arbeiten, es gab kein Vibrieren mehr und die Leistung verbesserte sich. Wenn ich eine kurze Fahrt unternehme, spare ich zwischen 1 und 1,5 l. Ich bin zufrieden.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="thumbnail">
                            <div class="row">
                                <span class="name">Patrick Meier 29 Jahre alt</span>
                                <img src="images/driver.jpg" height="187" width="249" alt="driver" class="img">
                                <span>Kraftstoffverbrauch vor und nach der Installation von Magnufuel</span>
                                <div class="percents">
									<span class="style_ib9">17 l<i class="slash">/</i>
<small>war</small>
</span>
                                    <span class="text-orange">12 l
<small>ist</small>
</span>
                                </div>
                            </div>
                            <div class="text-comment">
                                <p>Lexus RX 350. Ich habe sofort zwei Stück Magnufuel  bestellt. Das Ergebnis wird gleich sichtbar und man hört, dass der Motor besser läuft. Ich hatte einige Mal das Problem gehabt, dass ich an unbekannten Tankstellen getankt habe und darauf kam diese  leuchtende Signalmeldung “Check the engine”. Und jetzt ist es ok. Nicht nur, dass Sprit von niedrigerer Qualität keine Probleme mehr verursacht, sondern auch dass der Kraftstoff im Tank länger reicht.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <h3>Magnufuel kann an verschiedenen Fahrzeugen eingebaut werden <br></h3>
                <p class="text-center">Magnufuel Spritsparer ist sehr populär in den USA und Westeuropa und jetzt können Sie ihn auch in Deutschland kaufen <span class="country_name">!</span></p>
                <ul class="thumb">
                    <li>
                        <img src="images/img_thumb.jpg" class="img" alt="automobil">
                        <h5>Autos</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb2.jpg" class="img" alt="automobil">
                        <h5>LKWs</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb3.jpg" class="img" alt="automobil">
                        <h5>Busse und Transporter</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb4.jpg" class="img" alt="automobil">
                        <h5>Motorboote und andere Wasserfahrzeuge</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb5.jpg" class="img" alt="automobil">
                        <h5>Motorräder</h5>
                    </li>
                    <li>
                        <img src="images/img_thumb6.jpg" class="img" alt="automobil">
                        <h5> Landwirtschaftsmaschinen</h5>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row bg4">
            <div class="container">
                <div class="box-record">
                    <h3>Nur heute können Sie kaufen
                        <strong class="text-yellow"> Magnufuel</strong>
                        für <em class="text-yellow"> unglaublich </em>  niedrigen Preis!</h3>
                    <div class="bg-white">
                        <div class="eTimer">
                            <div class="countdown_box">
                                <span class="order-countdown-hours" id="hours_bottom"></span>
                                <span>Stunden</span>
                                <span class="order-countdown-minutes" id="minutes_bottom"></span>
                                <span>Minuten</span>
                                <span class="order-countdown-seconds" id="seconds_bottom"></span>
                                <span>Sekunden</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="form-inverse country">

                    <h3><br>  Magnufuel bestellen</h3>  und beginnen Sie noch heute, Sprit zu sparen.
                    <form name="form-index" class="form-inverse country" id="form-index" action="" method="post">
                        <input id="shipping_fname" type="hidden" name="shipping_fname" value="NULL">
                        <input id="shipping_email" type="hidden" name="shipping_email" value="NULL">
                        <button class="form-index rushOrder" type="submit" id="orderib">prüfen Sie die Preise</button>
                    </form>
                    <img src="images/1_sticker.png" class="style_ib11" alt="Guarante">

                    <img src="images/2_sticker.png" class="style_ib11" alt="Guarante">
                    <p class="style_ib12">* Empfohlen von der European Automobile Manufacturers Association</p>

                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">





        </div>
    </footer>










</div>

    <?php include 'page2.php';?>
    </body>


    </html>
<?php } ?>