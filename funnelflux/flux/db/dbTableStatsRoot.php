<?php
require_once dirname(__FILE__) . '/dbTableBackgroundJobs.php'; require_once dirname(__FILE__) . '/dbTableMeta.php'; require_once dirname(__FILE__) . '/dbTableHistory.php'; require_once dirname(__FILE__) . '/dbTableIncomingTraffic.php'; require_once dirname(__FILE__) . '/dbTableCampaignFunnels.php'; require_once dirname(__FILE__) . '/dbTableHitRecords.php'; require_once dirname(__FILE__) . '/dbTableVisitorsInfoAdvanced.php'; require_once dirname(__FILE__) . '/dbTableStatsAggregate.php'; require_once dirname(__FILE__) . '/dbTableTrafficSources.php'; require_once dirname(__FILE__) . '/dbTableApplication.php'; require_once dirname(__FILE__) . '/../includes/funnelNodeTypes.php'; require_once dirname(__FILE__) . '/../includes/strings.php'; require_once dirname(__FILE__) . '/../tracking/hitConversion.php'; require_once dirname(__FILE__) . '/../tracking/hitRecord.php'; require_once dirname(__FILE__) . '/../tracking/visitorInfoAdvanced.php'; require_once dirname(__FILE__) . '/../tracking/incomingTraffic.php'; require_once dirname(__FILE__) . '/../content/campaignFunnelNode.php'; require_once dirname(__FILE__) . '/../content/campaignFunnel.php'; require_once dirname(__FILE__) . '/../content/fluxPage.php'; require_once dirname(__FILE__) . '/../libs/carbon/Carbon.php'; use Carbon\Carbon; class DBTableStatsRoot { const STATS_GENERATION_BATCH_SIZE = 1000; const FLAG_FILTERED_TRAFFIC = 1; const TABLE_NAME = 'stats_root'; const COLUMN_ID = 'id'; const COLUMN_TIMESTAMP = 'timestamp'; const COLUMN_ID_HIT = 'idHit'; const COLUMN_ID_VISITOR = 'idVisitor'; const COLUMN_FUNNEL_ENTRANCE = 'bFunnelEntrance'; const COLUMN_NEW_VISITOR = 'bNewVisitor'; const COLUMN_ID_VISITED_NODE = 'idVisitedNode'; const COLUMN_NODE_TYPE = 'nodeType'; const COLUMN_ID_FLUX_LANDER = 'idFluxLander'; const COLUMN_ID_FLUX_OFFER = 'idFluxOffer'; const COLUMN_ID_CONVERSION = 'idConversion'; const COLUMN_HIT_PAYOUT = 'hitPayout'; const COLUMN_HIT_COST = 'hitCost'; const COLUMN_ID_INCOMING_TRAFFIC = 'idIncomingTraffic'; const COLUMN_ID_TRAFFIC_SOURCE = 'idTrafficSource'; const COLUMN_ID_CAMPAIGN = 'idCampaign'; const COLUMN_ID_FUNNEL = 'idFunnel'; const COLUMN_URL_PARAM_FIELD_1 = 'urlParamField1'; const COLUMN_URL_PARAM_FIELD_2 = 'urlParamField2'; const COLUMN_URL_PARAM_FIELD_3 = 'urlParamField3'; const COLUMN_URL_PARAM_FIELD_4 = 'urlParamField4'; const COLUMN_URL_PARAM_FIELD_5 = 'urlParamField5'; const COLUMN_URL_PARAM_FIELD_6 = 'urlParamField6'; const COLUMN_URL_PARAM_FIELD_7 = 'urlParamField7'; const COLUMN_URL_PARAM_FIELD_8 = 'urlParamField8'; const COLUMN_URL_PARAM_FIELD_9 = 'urlParamField9'; const COLUMN_URL_PARAM_FIELD_10 = 'urlParamField10'; const COLUMN_URL_PARAM_VALUE_1 = 'urlParamValue1'; const COLUMN_URL_PARAM_VALUE_2 = 'urlParamValue2'; const COLUMN_URL_PARAM_VALUE_3 = 'urlParamValue3'; const COLUMN_URL_PARAM_VALUE_4 = 'urlParamValue4'; const COLUMN_URL_PARAM_VALUE_5 = 'urlParamValue5'; const COLUMN_URL_PARAM_VALUE_6 = 'urlParamValue6'; const COLUMN_URL_PARAM_VALUE_7 = 'urlParamValue7'; const COLUMN_URL_PARAM_VALUE_8 = 'urlParamValue8'; const COLUMN_URL_PARAM_VALUE_9 = 'urlParamValue9'; const COLUMN_URL_PARAM_VALUE_10 = 'urlParamValue10'; const COLUMN_LOCATION_CONTINENT = 'locationContinent'; const COLUMN_LOCATION_COUNTRY_CODE = 'locationCountryCode'; const COLUMN_LOCATION_COUNTRY_NAME = 'locationCountryName'; const COLUMN_LOCATION_CITY = 'locationCity'; const COLUMN_LOCATION_REGION = 'locationRegion'; const COLUMN_LOCATION_LATITUDE = 'locationLatitude'; const COLUMN_LOCATION_LONGITUDE = 'locationLongitude'; const COLUMN_LOCATION_TIMEZONE = 'locationTimezone'; const COLUMN_CONNECTION_IP = 'connectionIpv4'; const COLUMN_CONNECTION_REFERRER = 'connectionReferrer'; const COLUMN_CONNECTION_ISP = 'connectionIsp'; const COLUMN_DEVICE_TYPE = 'deviceType'; const COLUMN_DEVICE_BRAND = 'deviceBrand'; const COLUMN_DEVICE_NAME = 'deviceName'; const COLUMN_DEVICE_OS = 'deviceOs'; const COLUMN_DEVICE_OS_VERSION = 'deviceOsVersion'; const COLUMN_DEVICE_BROWSER = 'deviceBrowser'; const COLUMN_DEVICE_BROWSER_VERSION = 'deviceBrowserVersion'; const COLUMN_DEVICE_DISPLAY_WIDTH = 'deviceDisplayWidth'; const COLUMN_DEVICE_DISPLAY_HEIGHT = 'deviceDisplayHeight'; const COLUMN_ID_OFFER_SOURCE = 'idOfferSource'; const COLUMN_FLAGS = 'statFlags'; private static $aAllColumnsExceptId = array(self::COLUMN_TIMESTAMP, self::COLUMN_ID_HIT, self::COLUMN_ID_VISITOR, self::COLUMN_FUNNEL_ENTRANCE, self::COLUMN_NEW_VISITOR, self::COLUMN_ID_VISITED_NODE, self::COLUMN_NODE_TYPE, self::COLUMN_ID_FLUX_LANDER, self::COLUMN_ID_FLUX_OFFER, self::COLUMN_ID_CONVERSION, self::COLUMN_HIT_PAYOUT, self::COLUMN_HIT_COST, self::COLUMN_ID_INCOMING_TRAFFIC, self::COLUMN_ID_TRAFFIC_SOURCE, self::COLUMN_ID_CAMPAIGN, self::COLUMN_ID_FUNNEL, self::COLUMN_URL_PARAM_FIELD_1, self::COLUMN_URL_PARAM_FIELD_2, self::COLUMN_URL_PARAM_FIELD_3, self::COLUMN_URL_PARAM_FIELD_4, self::COLUMN_URL_PARAM_FIELD_5, self::COLUMN_URL_PARAM_FIELD_6, self::COLUMN_URL_PARAM_FIELD_7, self::COLUMN_URL_PARAM_FIELD_8, self::COLUMN_URL_PARAM_FIELD_9, self::COLUMN_URL_PARAM_FIELD_10, self::COLUMN_URL_PARAM_VALUE_1, self::COLUMN_URL_PARAM_VALUE_2, self::COLUMN_URL_PARAM_VALUE_3, self::COLUMN_URL_PARAM_VALUE_4, self::COLUMN_URL_PARAM_VALUE_5, self::COLUMN_URL_PARAM_VALUE_6, self::COLUMN_URL_PARAM_VALUE_7, self::COLUMN_URL_PARAM_VALUE_8, self::COLUMN_URL_PARAM_VALUE_9, self::COLUMN_URL_PARAM_VALUE_10, self::COLUMN_LOCATION_CONTINENT, self::COLUMN_LOCATION_COUNTRY_CODE, self::COLUMN_LOCATION_COUNTRY_NAME, self::COLUMN_LOCATION_CITY, self::COLUMN_LOCATION_REGION, self::COLUMN_LOCATION_LATITUDE, self::COLUMN_LOCATION_LONGITUDE, self::COLUMN_LOCATION_TIMEZONE, self::COLUMN_CONNECTION_IP, self::COLUMN_CONNECTION_REFERRER, self::COLUMN_CONNECTION_ISP, self::COLUMN_DEVICE_TYPE, self::COLUMN_DEVICE_BRAND, self::COLUMN_DEVICE_NAME, self::COLUMN_DEVICE_OS, self::COLUMN_DEVICE_OS_VERSION, self::COLUMN_DEVICE_BROWSER, self::COLUMN_DEVICE_BROWSER_VERSION, self::COLUMN_DEVICE_DISPLAY_WIDTH, self::COLUMN_DEVICE_DISPLAY_HEIGHT, self::COLUMN_ID_OFFER_SOURCE, self::COLUMN_FLAGS); public static function createTable() { DB::getDB()->rawQuery('CREATE TABLE `stats_root` (
                                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                `timestamp` int(10) unsigned NOT NULL,
                                `idHit` int(10) unsigned NOT NULL,
                                `idVisitor` int(10) unsigned NOT NULL,
                                `bFunnelEntrance` tinyint(4) NOT NULL DEFAULT \'0\',
                                `bNewVisitor` tinyint(4) NOT NULL,
                                `idVisitedNode` bigint(20) unsigned NOT NULL,
                                `nodeType` int(11) NOT NULL,
                                `idFluxLander` int(10) unsigned DEFAULT NULL,
                                `idFluxOffer` int(10) unsigned DEFAULT NULL,
                                `idConversion` int(10) unsigned DEFAULT NULL,
                                `hitPayout` double NOT NULL DEFAULT \'0\',
                                `hitCost` double NOT NULL DEFAULT \'0\',
                                `idIncomingTraffic` int(10) unsigned DEFAULT NULL,
                                `idTrafficSource` int(10) unsigned DEFAULT NULL,
                                `idCampaign` int(10) unsigned DEFAULT NULL,
                                `idFunnel` int(10) unsigned DEFAULT NULL,
                                `urlParamField1` varchar(45) DEFAULT NULL,
                                `urlParamField2` varchar(45) DEFAULT NULL,
                                `urlParamField3` varchar(45) DEFAULT NULL,
                                `urlParamField4` varchar(45) DEFAULT NULL,
                                `urlParamField5` varchar(45) DEFAULT NULL,
                                `urlParamField6` varchar(45) DEFAULT NULL,
                                `urlParamField7` varchar(45) DEFAULT NULL,
                                `urlParamField8` varchar(45) DEFAULT NULL,
                                `urlParamField9` varchar(45) DEFAULT NULL,
                                `urlParamField10` varchar(45) DEFAULT NULL,
                                `urlParamValue1` varchar(255) DEFAULT NULL,
                                `urlParamValue2` varchar(255) DEFAULT NULL,
                                `urlParamValue3` varchar(255) DEFAULT NULL,
                                `urlParamValue4` varchar(255) DEFAULT NULL,
                                `urlParamValue5` varchar(255) DEFAULT NULL,
                                `urlParamValue6` varchar(255) DEFAULT NULL,
                                `urlParamValue7` varchar(255) DEFAULT NULL,
                                `urlParamValue8` varchar(255) DEFAULT NULL,
                                `urlParamValue9` varchar(255) DEFAULT NULL,
                                `urlParamValue10` varchar(255) DEFAULT NULL,
                                `locationContinent` varchar(40) DEFAULT NULL,
                                `locationCountryCode` varchar(2) DEFAULT NULL,
                                `locationCountryName` varchar(40) DEFAULT NULL,
                                `locationCity` varchar(40) DEFAULT NULL,
                                `locationRegion` varchar(42) DEFAULT NULL,
                                `locationLatitude` float DEFAULT NULL,
                                `locationLongitude` float DEFAULT NULL,
                                `locationTimezone` varchar(40) DEFAULT NULL,
                                `connectionIpv4` int(10) unsigned NOT NULL DEFAULT \'0\',
                                `connectionReferrer` varchar(255) DEFAULT NULL,
                                `connectionIsp` varchar(60) DEFAULT NULL,
                                `deviceType` varchar(13) DEFAULT NULL,
                                `deviceBrand` varchar(45) DEFAULT NULL,
                                `deviceName` varchar(45) DEFAULT NULL,
                                `deviceOs` varchar(45) DEFAULT NULL,
                                `deviceOsVersion` varchar(45) DEFAULT NULL,
                                `deviceBrowser` varchar(45) DEFAULT NULL,
                                `deviceBrowserVersion` varchar(45) DEFAULT NULL,
                                `deviceDisplayWidth` varchar(5) DEFAULT NULL,
                                `deviceDisplayHeight` varchar(5) DEFAULT NULL,
                                `idOfferSource` varchar(20) DEFAULT NULL,
                                `statFlags` int(10) unsigned NOT NULL DEFAULT \'0\',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `id_UNIQUE` (`id`),
                                KEY `idHit` (`idHit`),
                                KEY `idIncomingTraffic` (`idIncomingTraffic`),
                                KEY `idConversion` (`idConversion`),
                                KEY `idVisitor` (`idVisitor`),
                                KEY `idCampaign` (`idCampaign`),
                                KEY `idFunnel` (`idFunnel`),
                                KEY `idFluxLander` (`idFluxLander`),
                                KEY `idFluxOffer` (`idFluxOffer`),
                                KEY `idVisitedNode` (`idVisitedNode`),
                                KEY `idTrafficSource` (`idTrafficSource`),
                                KEY `idOfferSource` (`idOfferSource`),
                                KEY `timestamp_IDX` (`timestamp`),
                                KEY `bFunnelEntrance_IDX` (`bFunnelEntrance`),
                                KEY `bNewVisitor_IDX` (`bNewVisitor`)
                              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                        '); self::setupMetaTimestampMin(); self::setupBackgroundJobs(); } public static function upgradeTable($sp874611, $spe4858e) { if ($sp874611 < 0.019) { DB::getDB()->rawQuery('ALTER TABLE `stats_root`
                ADD COLUMN `urlParamField1` VARCHAR(255) NULL DEFAULT NULL AFTER `idFunnel`,
                ADD COLUMN `urlParamField2` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField1`,
                ADD COLUMN `urlParamField3` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField2`,
                ADD COLUMN `urlParamField4` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField3`,
                ADD COLUMN `urlParamField5` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField4`,
                ADD COLUMN `urlParamField6` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField5`,
                ADD COLUMN `urlParamField7` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField6`,
                ADD COLUMN `urlParamField8` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField7`,
                ADD COLUMN `urlParamField9` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField8`,
                ADD COLUMN `urlParamField10` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField9`,
                ADD COLUMN `urlParamValue1` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamField10`,
                ADD COLUMN `urlParamValue2` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue1`,
                ADD COLUMN `urlParamValue3` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue2`,
                ADD COLUMN `urlParamValue4` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue3`,
                ADD COLUMN `urlParamValue5` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue4`,
                ADD COLUMN `urlParamValue6` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue5`,
                ADD COLUMN `urlParamValue7` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue6`,
                ADD COLUMN `urlParamValue8` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue7`,
                ADD COLUMN `urlParamValue9` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue8`,
                ADD COLUMN `urlParamValue10` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue9`,
                ADD COLUMN `locationContinent` VARCHAR(255) NULL DEFAULT NULL AFTER `urlParamValue10`,
                ADD COLUMN `locationCountryCode` VARCHAR(255) NULL DEFAULT NULL AFTER `locationContinent`,
                ADD COLUMN `locationCountryName` VARCHAR(255) NULL DEFAULT NULL AFTER `locationCountryCode`,
                ADD COLUMN `locationCity` VARCHAR(255) NULL DEFAULT NULL AFTER `locationCountryName`,
                ADD COLUMN `locationLatitude` VARCHAR(255) NULL DEFAULT NULL AFTER `locationCity`,
                ADD COLUMN `locationLongitude` VARCHAR(255) NULL DEFAULT NULL AFTER `locationLatitude`,
                ADD COLUMN `locationTimezone` VARCHAR(255) NULL DEFAULT NULL AFTER `locationLongitude`,
                ADD COLUMN `connectionIp` VARCHAR(255) NULL DEFAULT NULL AFTER `locationTimezone`,
                ADD COLUMN `connectionReferrer` VARCHAR(255) NULL DEFAULT NULL AFTER `connectionIp`,
                ADD COLUMN `connectionIsp` VARCHAR(255) NULL DEFAULT NULL AFTER `connectionReferrer`,
                ADD COLUMN `deviceType` VARCHAR(255) NULL DEFAULT NULL AFTER `connectionIsp`,
                ADD COLUMN `deviceBrand` VARCHAR(255) NULL DEFAULT NULL AFTER `deviceType`,
                ADD COLUMN `deviceName` VARCHAR(255) NULL DEFAULT NULL AFTER `deviceBrand`,
                ADD COLUMN `deviceOs` VARCHAR(255) NULL DEFAULT NULL AFTER `deviceName`,
                ADD COLUMN `deviceOsVersion` VARCHAR(255) NULL DEFAULT NULL AFTER `deviceOs`,
                ADD COLUMN `deviceBrowser` VARCHAR(255) NULL DEFAULT NULL AFTER `deviceOsVersion`,
                ADD COLUMN `deviceBrowserVersion` VARCHAR(255) NULL DEFAULT NULL AFTER `deviceBrowser`,
                ADD COLUMN `deviceDisplayWidth` VARCHAR(255) NULL DEFAULT NULL AFTER `deviceBrowserVersion`,
                ADD COLUMN `deviceDisplayHeight` VARCHAR(255) NULL DEFAULT NULL AFTER `deviceDisplayWidth`,
                ADD COLUMN `idOfferSource` int(10) unsigned DEFAULT NULL AFTER `deviceDisplayHeight`;
                '); self::upgradeStatsTo0019(); } if ($sp874611 < 0.02) { DB::getDB()->rawQuery('ALTER TABLE `stats_root`
                    CHANGE COLUMN `urlParamField1` `urlParamField1` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField2` `urlParamField2` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField3` `urlParamField3` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField4` `urlParamField4` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField5` `urlParamField5` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField6` `urlParamField6` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField7` `urlParamField7` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField8` `urlParamField8` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField9` `urlParamField9` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField10` `urlParamField10` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationContinent` `locationContinent` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationCountryCode` `locationCountryCode` VARCHAR(2) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationCountryName` `locationCountryName` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationCity` `locationCity` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationLatitude` `locationLatitude` VARCHAR(10) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationLongitude` `locationLongitude` VARCHAR(10) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationTimezone` `locationTimezone` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `connectionIp` `connectionIp` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `connectionIsp` `connectionIsp` VARCHAR(60) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceType` `deviceType` VARCHAR(13) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceBrand` `deviceBrand` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceName` `deviceName` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceOs` `deviceOs` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceOsVersion` `deviceOsVersion` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceBrowser` `deviceBrowser` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceBrowserVersion` `deviceBrowserVersion` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceDisplayWidth` `deviceDisplayWidth` VARCHAR(5) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceDisplayHeight` `deviceDisplayHeight` VARCHAR(5) NULL DEFAULT NULL ,
                    CHANGE COLUMN `idOfferSource` `idOfferSource` VARCHAR(20) NULL DEFAULT NULL ;
                '); } if ($sp874611 < 0.037) { DB::getDB()->rawQuery('ALTER TABLE `stats_root` ADD COLUMN `bFunnelEntrance` TINYINT(4) NOT NULL DEFAULT 0 AFTER `idVisitor`;'); } if ($sp874611 < 0.038) { DB::getDB()->rawQuery('UPDATE stats_root
                                    SET bFunnelEntrance = 1
                                    WHERE nodeType = \'' . FunnelNodeTypes::ROOT . '\''); } if ($sp874611 < 0.9) { set_time_limit(300); DB::getDB()->rawQuery('ALTER TABLE `stats_root` ADD COLUMN `statFlags` int(10) unsigned NOT NULL DEFAULT 0 AFTER `idOfferSource`;'); } if ($sp874611 < 0.905) { set_time_limit(300); DB::getDB()->rawQuery('UPDATE stats_root SET connectionIp = INET_ATON(connectionIp)'); } if ($sp874611 < 0.911) { set_time_limit(300); DB::getDB()->rawQuery('UPDATE stats_root SET connectionIp = 0 WHERE connectionIp IS NULL'); DB::getDB()->rawQuery('UPDATE stats_root SET locationContinent = SUBSTRING(locationContinent, 1, 40)'); DB::getDB()->rawQuery('UPDATE stats_root SET locationCountryName = SUBSTRING(locationCountryName, 1, 40)'); DB::getDB()->rawQuery('UPDATE stats_root SET locationCity = SUBSTRING(locationCity, 1, 40)'); DB::getDB()->rawQuery('UPDATE stats_root SET locationTimezone = SUBSTRING(locationTimezone, 1, 40)'); DB::getDB()->rawQuery('UPDATE stats_root SET connectionIsp = SUBSTRING(connectionIsp, 1, 60)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceType = SUBSTRING(deviceType, 1, 13)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceBrand = SUBSTRING(deviceBrand, 1, 45)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceName = SUBSTRING(deviceName, 1, 45)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceOs = SUBSTRING(deviceOs, 1, 45)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceOsVersion = SUBSTRING(deviceOsVersion, 1, 45)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceBrowser = SUBSTRING(deviceBrowser, 1, 45)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceBrowserVersion = SUBSTRING(deviceBrowserVersion, 1, 45)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceDisplayWidth = SUBSTRING(deviceDisplayWidth, 1, 5)'); DB::getDB()->rawQuery('UPDATE stats_root SET deviceDisplayHeight = SUBSTRING(deviceDisplayHeight, 1, 5)'); set_time_limit(300); DB::getDB()->rawQuery('ALTER TABLE `stats_root`
                    CHANGE COLUMN `urlParamField1` `urlParamField1` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField2` `urlParamField2` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField3` `urlParamField3` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField4` `urlParamField4` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField5` `urlParamField5` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField6` `urlParamField6` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField7` `urlParamField7` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField8` `urlParamField8` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField9` `urlParamField9` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `urlParamField10` `urlParamField10` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationContinent` `locationContinent` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationCountryCode` `locationCountryCode` VARCHAR(2) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationCountryName` `locationCountryName` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationCity` `locationCity` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationLatitude` `locationLatitude` FLOAT NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationLongitude` `locationLongitude` FLOAT NULL DEFAULT NULL ,
                    CHANGE COLUMN `locationTimezone` `locationTimezone` VARCHAR(40) NULL DEFAULT NULL ,
                    CHANGE COLUMN `connectionIp` `connectionIpv4` INT(10) UNSIGNED NOT NULL DEFAULT 0 ,
                    CHANGE COLUMN `connectionIsp` `connectionIsp` VARCHAR(60) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceType` `deviceType` VARCHAR(13) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceBrand` `deviceBrand` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceName` `deviceName` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceOs` `deviceOs` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceOsVersion` `deviceOsVersion` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceBrowser` `deviceBrowser` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceBrowserVersion` `deviceBrowserVersion` VARCHAR(45) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceDisplayWidth` `deviceDisplayWidth` VARCHAR(5) NULL DEFAULT NULL ,
                    CHANGE COLUMN `deviceDisplayHeight` `deviceDisplayHeight` VARCHAR(5) NULL DEFAULT NULL ,
                    CHANGE COLUMN `idOfferSource` `idOfferSource` VARCHAR(20) NULL DEFAULT NULL ;
                '); } if ($sp874611 < 0.921) { set_time_limit(3000); self::generateUnrecordedStats(PHP_INT_MAX); } if ($sp874611 < 0.923) { DB::getDB()->rawQuery('ALTER TABLE `stats_root` ADD INDEX `idConversion` (`idConversion` ASC);'); } if ($sp874611 < 1.0) { set_time_limit(3000); self::addUnrecordedConversions(); } if ($sp874611 < 1.042) { DB::getDB()->rawQuery('ALTER TABLE `stats_root`
                                    ADD INDEX `idVisitor` (`idVisitor` ASC),
                                    ADD INDEX `idCampaign` (`idCampaign` ASC),
                                    ADD INDEX `idFunnel` (`idFunnel` ASC),
                                    ADD INDEX `idFluxLander` (`idFluxLander` ASC),
                                    ADD INDEX `idFluxOffer` (`idFluxOffer` ASC),
                                    ADD INDEX `idVisitedNode` (`idVisitedNode` ASC),
                                    ADD INDEX `idTrafficSource` (`idTrafficSource` ASC),
                                    ADD INDEX `idOfferSource` (`idOfferSource` ASC);'); } if ($sp874611 < 1.058) { DB::getDB()->rawQuery('ALTER TABLE `stats_root`
                                    ADD INDEX `idHit` (`idHit` ASC),
                                    ADD INDEX `idIncomingTraffic` (`idIncomingTraffic` ASC);'); } if ($sp874611 < 1.089) { DB::getDB()->rawQuery('ALTER TABLE `stats_root`
                                    ADD INDEX `timestamp_IDX` (`timestamp` ASC),
                                    ADD INDEX `bFunnelEntrance_IDX` (`bFunnelEntrance` ASC),
                                    ADD INDEX `bNewVisitor_IDX` (`bNewVisitor` ASC);'); } if ($sp874611 < 1.12) { DB::getDB()->rawQuery('ALTER TABLE `stats_root` ADD COLUMN `locationRegion` VARCHAR(42) NULL DEFAULT NULL AFTER `locationCity`;'); } if ($sp874611 < 1.123) { $spf361ad = new BackgroundJob(); $spf361ad->setTitle('Remove Duplicate Conversion Rows'); $spf361ad->setFunctionName('DBTableStatsRoot::bjDeleteDuplicateConvRows'); DBTableBackgroundJobs::save($spf361ad); } if ($sp874611 < 1.126) { self::setupMetaTimestampMin(); } if ($sp874611 < 1.1281) { $sp133cfb = self::TABLE_NAME; $sp93b713 = self::COLUMN_ID_HIT; $sp6867b5 = DB::getDB()->rawQuery("SELECT {$sp93b713} FROM {$sp133cfb} ORDER BY {$sp93b713} DESC LIMIT 1"); if (!empty($sp6867b5)) { $spe65cd0 = $sp6867b5[0][$sp93b713]; DBTableMeta::set(DBTableMeta::KEY_STATS_ROOT_LATEST_PROCESSED_ID_HIT, $spe65cd0); } } if ($sp874611 < 1.1291) { self::setupBackgroundJobs(); } if ($sp874611 < 1.1305) { self::setupMetaTimestampMin(); } if ($sp874611 < 1.13432) { self::resetProcedureInvalidIndirectConversions(); } if ($sp874611 < 1.1352) { $spd56727 = DBTableHitRecords::COLUMN_TIMESTAMP; $sp133cfb = DBTableHitRecords::TABLE_NAME; $sp34c508 = DB::getDB()->rawQuery("SELECT {$spd56727} FROM {$sp133cfb} ORDER BY {$spd56727} ASC LIMIT 1"); if (!empty($sp34c508)) { $sp99b7ee = $sp34c508[0][$spd56727]; DBTableMeta::set(DBTableMeta::KEY_STATS_ROOT_LATEST_PROCESSED_TIMESTAMP, $sp99b7ee); } } } public static function getAdditionalTablesToLockDuringUpgrade($sp874611, $spe4858e) { $spf0a7cc = array(); if ($sp874611 < 0.921) { $spf0a7cc = array_merge($spf0a7cc, array('hit_conversions conversion READ', 'stats_root root READ', 'stats_root root2 READ', 'hit_records hit2 READ', 'hit_conversions conv READ', 'campaign_funnel_nodes node READ', 'campaign_funnels funnel READ', 'campaigns campaign READ', 'incoming_traffic incoming READ', 'pages offerPage READ', 'affiliate_networks offerSource READ', 'visitors_info visitor READ', 'visitors_info_advanced visitorAdvanced READ', 'visitor_locations_info location READ', 'visitor_networks_info network READ', 'visitor_devices_info device READ', 'incoming_traffic_url_params urlParams1 READ', 'incoming_traffic_url_params urlParams2 READ', 'incoming_traffic_url_params urlParams3 READ', 'incoming_traffic_url_params urlParams4 READ', 'incoming_traffic_url_params urlParams5 READ', 'incoming_traffic_url_params urlParams6 READ', 'incoming_traffic_url_params urlParams7 READ', 'incoming_traffic_url_params urlParams8 READ', 'incoming_traffic_url_params urlParams9 READ', 'incoming_traffic_url_params urlParams10 READ')); } if ($sp874611 < 1.0) { $spf0a7cc = array_merge($spf0a7cc, array('stats_root root READ', 'stats_root root2 READ', 'hit_conversions conv READ')); } if ($sp874611 < 1.0) { $spf0a7cc = array_merge($spf0a7cc, array('stats_root root READ', 'stats_root root2 READ', 'hit_conversions conv READ')); } return array_unique($spf0a7cc); } public static function upgradeStatsTo0019() { DB::getDB()->rawQuery('UPDATE stats_root root
            INNER JOIN
            (
                SELECT STRAIGHT_JOIN
                        base.id,
                        urlParams1.urlParamField as urlParamField1,
                        urlParams2.urlParamField as urlParamField2,
                        urlParams3.urlParamField as urlParamField3,
                        urlParams4.urlParamField as urlParamField4,
                        urlParams5.urlParamField as urlParamField5,
                        urlParams6.urlParamField as urlParamField6,
                        urlParams7.urlParamField as urlParamField7,
                        urlParams8.urlParamField as urlParamField8,
                        urlParams9.urlParamField as urlParamField9,
                        urlParams10.urlParamField as urlParamField10,
                        urlParams1.urlParamValue as urlParamValue1,
                        urlParams2.urlParamValue as urlParamValue2,
                        urlParams3.urlParamValue as urlParamValue3,
                        urlParams4.urlParamValue as urlParamValue4,
                        urlParams5.urlParamValue as urlParamValue5,
                        urlParams6.urlParamValue as urlParamValue6,
                        urlParams7.urlParamValue as urlParamValue7,
                        urlParams8.urlParamValue as urlParamValue8,
                        urlParams9.urlParamValue as urlParamValue9,
                        urlParams10.urlParamValue as urlParamValue10,
                        location.continent as locationContinent,
                        location.countryCode as locationCountryCode,
                        location.countryName as locationCountryName,
                        location.city as locationCity,
                        location.latitude as locationLatitude,
                        location.longitude as locationLongitude,
                        location.timezone as locationTimezone,
                        visitor.ip as connectionIp,
                        incoming.referrer as connectionReferrer,
                        network.isp as connectionIsp,
                        device.type as deviceType,
                        device.brand as deviceBrand,
                        device.name as deviceName,
                        device.os as deviceOs,
                        device.osVersion as deviceOsVersion,
                        device.browser as deviceBrowser,
                        device.browserVersion as deviceBrowserVersion,
                        device.displayWidth as deviceDisplayWidth,
                        device.displayHeight as deviceDisplayHeight,
                        offerSource.idAffiliateNetwork as idOfferSource
                    FROM
                        stats_root AS base
                    LEFT JOIN pages offerPage on offerPage.idPage = base.idFluxOffer
                    LEFT JOIN affiliate_networks offerSource on offerSource.idAffiliateNetwork = SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(offerPage.aTypeParams, \'"idAffiliateNetwork";s\' , -1 ),\'";\', 1),\'"\',-1)
                    LEFT JOIN visitors_info visitor on visitor.idVisitor = base.idVisitor
                    LEFT JOIN visitors_info_advanced visitorAdvanced on visitorAdvanced.idVisitor = base.idVisitor
                    LEFT JOIN visitor_locations_info location on location.hash = visitorAdvanced.locationHash
                    LEFT JOIN visitor_networks_info network on network.hash = visitorAdvanced.networkHash
                    LEFT JOIN visitor_devices_info device on device.hash = visitorAdvanced.deviceHash
                    LEFT JOIN incoming_traffic incoming on incoming.idIncomingTraffic = base.idIncomingTraffic
                    LEFT JOIN incoming_traffic_url_params AS urlParams1 ON urlParams1.idIncomingTraffic = base.idIncomingTraffic
                    LEFT JOIN incoming_traffic_url_params AS urlParams2 ON urlParams2.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams2.idUrlParam > urlParams1.idUrlParam
                    LEFT JOIN incoming_traffic_url_params AS urlParams3 ON urlParams3.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams3.idUrlParam > urlParams2.idUrlParam
                    LEFT JOIN incoming_traffic_url_params AS urlParams4 ON urlParams4.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams4.idUrlParam > urlParams3.idUrlParam
                    LEFT JOIN incoming_traffic_url_params AS urlParams5 ON urlParams5.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams5.idUrlParam > urlParams4.idUrlParam
                    LEFT JOIN incoming_traffic_url_params AS urlParams6 ON urlParams6.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams6.idUrlParam > urlParams5.idUrlParam
                    LEFT JOIN incoming_traffic_url_params AS urlParams7 ON urlParams7.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams7.idUrlParam > urlParams6.idUrlParam
                    LEFT JOIN incoming_traffic_url_params AS urlParams8 ON urlParams8.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams8.idUrlParam > urlParams7.idUrlParam
                    LEFT JOIN incoming_traffic_url_params AS urlParams9 ON urlParams9.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams9.idUrlParam > urlParams8.idUrlParam
                    LEFT JOIN incoming_traffic_url_params AS urlParams10 ON urlParams10.idIncomingTraffic = base.idIncomingTraffic
                                AND urlParams10.idUrlParam > urlParams9.idUrlParam
                GROUP BY base.id
            ) base ON base.id = root.id
            SET
                root.urlParamField1 = base.urlParamField1,
                root.urlParamField2 = base.urlParamField2,
                root.urlParamField3 = base.urlParamField3,
                root.urlParamField4 = base.urlParamField4,
                root.urlParamField5 = base.urlParamField5,
                root.urlParamField6 = base.urlParamField6,
                root.urlParamField7 = base.urlParamField7,
                root.urlParamField8 = base.urlParamField8,
                root.urlParamField9 = base.urlParamField9,
                root.urlParamField10 = base.urlParamField10,
                root.urlParamValue1 = base.urlParamValue1,
                root.urlParamValue2 = base.urlParamValue2,
                root.urlParamValue3 = base.urlParamValue3,
                root.urlParamValue4 = base.urlParamValue4,
                root.urlParamValue5 = base.urlParamValue5,
                root.urlParamValue6 = base.urlParamValue6,
                root.urlParamValue7 = base.urlParamValue7,
                root.urlParamValue8 = base.urlParamValue8,
                root.urlParamValue9 = base.urlParamValue9,
                root.urlParamValue10 = base.urlParamValue10,
                root.locationContinent = base.locationContinent,
                root.locationCountryCode = base.locationCountryCode,
                root.locationCountryName = base.locationCountryName,
                root.locationCity = base.locationCity,
                root.locationLatitude = base.locationLatitude,
                root.locationLongitude = base.locationLongitude,
                root.locationTimezone = base.locationTimezone,
                root.connectionIp = base.connectionIp,
                root.connectionReferrer = base.connectionReferrer,
                root.connectionIsp = base.connectionIsp,
                root.deviceType = base.deviceType,
                root.deviceBrand = base.deviceBrand,
                root.deviceName = base.deviceName,
                root.deviceOs = base.deviceOs,
                root.deviceOsVersion = base.deviceOsVersion,
                root.deviceBrowser = base.deviceBrowser,
                root.deviceBrowserVersion = base.deviceBrowserVersion,
                root.deviceDisplayWidth = base.deviceDisplayWidth,
                root.deviceDisplayHeight = base.deviceDisplayHeight,
                root.idOfferSource = base.idOfferSource
            ', null, false); } private static function createProcedureForInvalidIndirectConversions() { DB::getDB()->getMysqli()->query('DROP PROCEDURE IF EXISTS `removeInvalidIndirectConversions_hits_to_conversions`;'); $sp8ba294 = DB::getDB()->getMysqli()->error; $spce19c0 = 'CREATE PROCEDURE `removeInvalidIndirectConversions_hits_to_conversions`()
                BEGIN
                        DROP TEMPORARY TABLE IF EXISTS _hits_to_conversions2;
                        CREATE TEMPORARY TABLE _hits_to_conversions2
                            (
                                INDEX `idHitConversion_INDEX` (`idHitConversion`),
                                INDEX `idConversionDirect_INDEX` (`idConversionDirect`),
                                INDEX `idConversionIndirect_INDEX` (`idConversionIndirect`),
                                INDEX `idNodeToConversion_INDEX` (`idNodeToConversion`)
                            )
                            ENGINE=MyISAM
                            SELECT * FROM _hits_to_conversions;

                        DELETE ndh1 FROM _hits_to_conversions ndh1
                        JOIN _hits_to_conversions2 ndh2 ON ndh2.idConversionDirect = ndh1.idConversionIndirect AND ndh2.idNodeToConversion = ndh1.idNodeToConversion;

                        DROP TEMPORARY TABLE IF EXISTS _hits_to_conversions2;
                END'; DB::getDB()->getMysqli()->query($spce19c0); $sp13d7fa = DB::getDB()->getMysqli()->error; $spc6952f = ($sp8ba294 ? $sp8ba294 : '') . ($sp13d7fa ? $sp13d7fa : ''); return $spc6952f; } public static function setupMetaTimestampMin() { if (DBTableMeta::get(DBTableMeta::KEY_STATS_ROOT_TIMESTAMP_MIN) === null) { $sp133cfb = self::TABLE_NAME; $sp5dcd64 = self::COLUMN_TIMESTAMP; $sp6867b5 = DB::getDB()->rawQuery("SELECT {$sp5dcd64} FROM {$sp133cfb} ORDER BY {$sp5dcd64} ASC LIMIT 1"); if (empty($sp6867b5)) { $sp05ab77 = time(); } else { $sp05ab77 = $sp6867b5[0][$sp5dcd64]; } DBTableMeta::set(DBTableMeta::KEY_STATS_ROOT_TIMESTAMP_MIN, $sp05ab77); } } private static function setupBackgroundJobs() { $spd79e77 = array(self::BJ_CREATE_PROCEDURE_INVALID_INDIRECT_CONVERSIONS_TITLE => 'DBTableStatsRoot::bjCreateProcedureForInvalidIndirectConversions', self::BJ_PROCESS_UNRECORDED_VISITORS_TITLE => array(DBTableBackgroundJobs::SETUP_FUNCTION_DESCRIPTION => null, DBTableBackgroundJobs::SETUP_FUNCTION_NAME => 'DBTableStatsRoot::bjGenerateUnrecordedStats', DBTableBackgroundJobs::SETUP_FUNCTION_IS_SYSTEM => true)); DBTableBackgroundJobs::setupBackgroundJobs($spd79e77); } private static function resetProcedureInvalidIndirectConversions() { $sp873a24 = self::BJ_CREATE_PROCEDURE_INVALID_INDIRECT_CONVERSIONS_TITLE; DBTableBackgroundJobs::deleteByTitle($sp873a24); DBTableBackgroundJobs::setupBackgroundJobs(array($sp873a24 => 'DBTableStatsRoot::bjCreateProcedureForInvalidIndirectConversions')); } public static function bjResetStats($sp386ba2, $sp0be594) { $spb4be30 = false; $spfe2d64 = BackgroundJob::FUNCTION_MUST_RESTART; for ($sp5e99c4 = 0; $sp5e99c4 < 10; $sp5e99c4++) { $sp9356fe = 50000; $spce19c0 = $sp0be594['sql'] . " LIMIT {$sp9356fe} "; if (!DB::temporaryTableCreate('_bj_reset_stats_idhits', $spce19c0, 'idHit')) { $spb4be30 = true; break; } else { $sp34c508 = DB::getDB()->rawQuery('select * from _bj_reset_stats_idhits limit 1'); if (empty($sp34c508)) { DBTableStatsAggregate::uncacheByPeriod($sp0be594['start'], $sp0be594['end'], true); $spe25191 = DBTableBackgroundJobs::loadById($sp386ba2); $sp3b9dac = new Message(); $sp3b9dac->setSender(Message::SENDER_SYSTEM_NOTIFICATION); $sp3b9dac->setTitle('Reset Stats Complete'); $sp3b9dac->setBody('The job for the reset described below completed succesfully (but historical re-caching still under process): ' . PHP_EOL . PHP_EOL . $spe25191->getDescription()); DBTableMessages::saveMessage($sp3b9dac); $spfe2d64 = BackgroundJob::FUNCTION_COMPLETED; break; } else { DB::getDB()->startTransaction(); try { $sp8a7325 = 'DELETE stats_root 
                                        FROM stats_root 
                                        JOIN _bj_reset_stats_idhits on _bj_reset_stats_idhits.idHit = stats_root.idHit'; DB::getDB()->rawQuery($sp8a7325, null, false); $sp400487 = DB::getDB()->getLastError(); if (!isEmpty($sp400487)) { throw new Exception("Cannot delete from stats_root: {$sp400487}"); } $sp8a7325 = 'DELETE hit_conversions
                                        FROM hit_conversions 
                                        JOIN _bj_reset_stats_idhits on _bj_reset_stats_idhits.idHit = hit_conversions.idHit'; DB::getDB()->rawQuery($sp8a7325, null, false); $sp400487 = DB::getDB()->getLastError(); if (!isEmpty($sp400487)) { throw new Exception("Cannot delete from hit_conversions: {$sp400487}"); } $sp8a7325 = 'DELETE action_clicks
                                        FROM action_clicks 
                                        JOIN _bj_reset_stats_idhits on _bj_reset_stats_idhits.idHit = action_clicks.sourceHitId'; DB::getDB()->rawQuery($sp8a7325, null, false); $sp400487 = DB::getDB()->getLastError(); if (!isEmpty($sp400487)) { throw new Exception("Cannot delete from action_clicks: {$sp400487}"); } DB::getDB()->commit(); } catch (Exception $spe00369) { DB::getDB()->rollback(); DBTableHistory::addEventSystemMessage("DBTableStatsRoot::bjResetStats() ERROR: {$spe00369->getMessage()}", DBTableHistory::EVENT_LEVEL_ERROR); $spfd602d = 10; if (!isset($sp0be594['attempt'])) { $sp0be594['attempt'] = 0; } $sp0be594['attempt']++; if ($sp0be594['attempt'] > $spfd602d) { $spb4be30 = true; break; } else { $spe25191 = DBTableBackgroundJobs::loadById($sp386ba2); $spe25191->setFunctionParams($sp0be594); DBTableBackgroundJobs::save($spe25191); } } } } } if ($spb4be30) { $spfe2d64 = BackgroundJob::FUNCTION_COMPLETED; $spe25191 = DBTableBackgroundJobs::loadById($sp386ba2); $sp3b9dac = new Message(); $sp3b9dac->setSender(Message::SENDER_SYSTEM_NOTIFICATION); $sp3b9dac->setTitle('Reset Stats Failed'); $sp3b9dac->setBody('The job for the reset described below failed to complete: ' . PHP_EOL . PHP_EOL . $spe25191->getDescription()); DBTableMessages::saveMessage($sp3b9dac); } return $spfe2d64; } public static function bjUpdateTrafficCostViaAPI($sp386ba2, $sp0be594) { $spfe2d64 = null; require_once dirname(__FILE__) . '/../admin/api/traffic/cost.php'; require_once dirname(__FILE__) . '/../admin/includes/periods.php'; require_once dirname(__FILE__) . '/dbTableIncomingTrafficURLParams.php'; $sp984022 = 'whichTF'; $sp0035b2 = 'computedCpe'; $spf2ab40 = self::TABLE_NAME; $sp99bfbf = DBTableIncomingTrafficURLParams::TABLE_NAME; $sp6b8396 = "{$spf2ab40}." . self::COLUMN_ID_FUNNEL; $speceadd = "{$spf2ab40}." . self::COLUMN_ID_TRAFFIC_SOURCE; $sp73480d = "{$spf2ab40}." . self::COLUMN_TIMESTAMP; $spbe8e72 = "{$spf2ab40}." . self::COLUMN_FLAGS; $spd56b8e = "{$spf2ab40}." . self::COLUMN_LOCATION_COUNTRY_CODE; $spdbd559 = "{$spf2ab40}." . self::COLUMN_ID_INCOMING_TRAFFIC; $sp0e0fe4 = "{$spf2ab40}." . self::COLUMN_HIT_COST; $spc244f6 = "{$spf2ab40}." . self::COLUMN_FUNNEL_ENTRANCE; $spad6743 = $sp0be594[CostREST::TIME_RANGE]; $sp414c8d = $sp0be594[CostREST::TIME_ZONE]; $spc3a115 = $sp0be594[CostREST::ID_FUNNEL]; $spda3b9a = $sp0be594[CostREST::ID_TRAFFIC_SOURCE]; $sp40bec6 = $sp0be594[CostREST::OPTIONAL_APPLY_TO_FILTERED_TOO]; $sp70db2e = $sp0be594[CostREST::OPTIONAL_ONLY_COUNTRY]; $sp832f15 = $sp0be594[CostREST::OPTIONAL_UPDATE_METHOD]; $spf2c872 = isset($sp0be594[$sp984022]) ? $sp0be594[$sp984022] : 0; $sp982c72 = isset($sp0be594[CostREST::OPTIONAL_TRACKING_FIELDS]) ? $sp0be594[CostREST::OPTIONAL_TRACKING_FIELDS] : null; Periods::stringToUTCTimestamps($spad6743, $sp0476cc, $spba2964, $sp414c8d); $sp92660f = array(); $spaa1e50 = array(); $spaa1e50[] = "{$spc244f6} = 1"; $spaa1e50[] = "{$sp6b8396} = {$spc3a115}"; $spaa1e50[] = "{$speceadd} = {$spda3b9a}"; $spaa1e50[] = "{$sp73480d} >= {$sp0476cc}"; $spaa1e50[] = "{$sp73480d} <= {$spba2964}"; if (!$sp40bec6) { $sp5917e8 = self::FLAG_FILTERED_TRAFFIC; $spaa1e50[] = "({$spbe8e72} & {$sp5917e8}) = 0"; } if ($sp70db2e !== CostREST::VALUE_ALL_COUNTRIES) { $spaa1e50[] = "{$spd56b8e} = '{$sp70db2e}'"; } if ($sp982c72) { $spd62c89 = DBTableIncomingTrafficURLParams::COLUMN_ID_INCOMING_TRAFFIC; $sp80f30e = DBTableIncomingTrafficURLParams::COLUMN_URL_PARAM_FIELD; $sp094d1d = DBTableIncomingTrafficURLParams::COLUMN_URL_PARAM_VALUE; foreach ($sp982c72[$spf2c872][CostREST::KEY_TRACKING_FIELDS_FIELDS] as $sp3b1219 => $sp385a4b) { $sp3b1219 = urldecode($sp3b1219); $sp385a4b = urldecode($sp385a4b); $spe15c01 = '_' . md5($sp3b1219); $sp9d1bbe = "JOIN {$sp99bfbf} AS {$spe15c01}\n                                ON {$spe15c01}.{$spd62c89} = {$spdbd559}\n                                AND {$spe15c01}.{$sp80f30e} = '{$sp3b1219}'"; if (strpos($sp385a4b, '*') !== false) { $sp385a4b = str_replace('*', '%', $sp385a4b); $sp9d1bbe .= " AND {$spe15c01}.{$sp094d1d} LIKE '{$sp385a4b}' "; } else { $sp9d1bbe .= " AND {$spe15c01}.{$sp094d1d} = '{$sp385a4b}' "; } $sp92660f[] = $sp9d1bbe; } } if (!isset($sp0be594[$sp0035b2])) { $sp19560d = $sp982c72 ? $sp982c72[$spf2c872][CostREST::KEY_TRACKING_FIELDS_NEW_COST] : $sp0be594[CostREST::NEW_COST]; if ($sp832f15 === 'cpe') { $sp0be594[$sp0035b2] = $sp19560d; } else { $spce19c0 = "SELECT SUM({$spf2ab40}.bFunnelEntrance) as `entrances`\n                        FROM {$spf2ab40} " . implode(' ', $sp92660f) . ' WHERE ' . implode(' AND ', $spaa1e50); $sp34c508 = DB::getDB()->rawQuery($spce19c0); $sp400487 = DB::getDB()->getLastError(); if (!empty($sp400487) || empty($sp34c508)) { DBTableHistory::addEventSystemMessage("DBTableStatsRoot::bjUpdateTrafficCostViaAPI() - Can't get number of entrances with query {$spce19c0} - ERROR: {$sp400487}"); $spfe2d64 = BackgroundJob::FUNCTION_MUST_RESTART; } else { $spfea63b = $sp34c508[0]['entrances']; $sp1d3bac = $sp19560d; $sp0be594[$sp0035b2] = $spfea63b ? $sp1d3bac / $spfea63b : 0; $spe25191 = DBTableBackgroundJobs::loadById($sp386ba2); if ($spe25191) { $spe25191->setFunctionParams($sp0be594); DBTableBackgroundJobs::save($spe25191); $spfe2d64 = BackgroundJob::FUNCTION_MUST_RESTART; } } } } if (!$spfe2d64) { $spce19c0 = "UPDATE {$spf2ab40} " . implode(' ', $sp92660f) . " SET {$sp0e0fe4} = {$sp0be594[$sp0035b2]}" . ' WHERE ' . implode(' AND ', $spaa1e50); DB::getDB()->rawQuery($spce19c0); $sp400487 = DB::getDB()->getLastError(); if (!empty($sp400487)) { DBTableHistory::addEventSystemMessage("DBTableStatsRoot::bjUpdateTrafficCostViaAPI() - Can't update hitCost with query {$spce19c0} - ERROR: {$sp400487}"); $spfe2d64 = BackgroundJob::FUNCTION_MUST_RESTART; } else { $spf2c872++; if (!$sp982c72 || $spf2c872 >= count($sp982c72)) { $spfe2d64 = BackgroundJob::FUNCTION_COMPLETED; DBTableStatsAggregate::uncacheByPeriod($sp0476cc, $spba2964); if (empty($sp0be594[CostREST::OPTIONAL_DISABLE_NOTIFICATION])) { $spe25191 = DBTableBackgroundJobs::loadById($sp386ba2); if ($spe25191) { $sp3b9dac = new Message(); $sp3b9dac->setSender(Message::SENDER_SYSTEM_NOTIFICATION); $sp3b9dac->setTitle('Traffic Cost Update Complete'); $sp3b9dac->setBody('The job for the cost update described below completed succesfully (but historical re-caching still under process): ' . PHP_EOL . PHP_EOL . $spe25191->getDescription()); DBTableMessages::saveMessage($sp3b9dac); } } } else { $sp0be594[$sp984022] = $spf2c872; unset($sp0be594[$sp0035b2]); $spe25191 = DBTableBackgroundJobs::loadById($sp386ba2); if ($spe25191) { $spe25191->setFunctionParams($sp0be594); DBTableBackgroundJobs::save($spe25191); } $spfe2d64 = BackgroundJob::FUNCTION_MUST_RESTART; } } } return $spfe2d64; } public static function bjGenerateUnrecordedStats($sp386ba2, $sp0be594) { if (!DB::isGeneratingStats()) { if (defined('AppConfig::STATS_PUSH_BATCH_SIZE')) { $sp9356fe = AppConfig::STATS_PUSH_BATCH_SIZE; } else { $sp9356fe = self::STATS_GENERATION_BATCH_SIZE; } self::generateUnrecordedStats($sp9356fe); } return BackgroundJob::FUNCTION_MUST_RESTART; } public static function bjDeleteDuplicateConvRows($sp386ba2, $sp0be594) { $sp3a7795 = self::deleteDuplicateConvRows(0, time() + 1, 1000); return $sp3a7795 ? BackgroundJob::FUNCTION_COMPLETED : BackgroundJob::FUNCTION_MUST_RESTART; } public static function bjCreateProcedureForInvalidIndirectConversions($sp386ba2, $sp0be594) { $spc6952f = self::createProcedureForInvalidIndirectConversions(); if (empty($spc6952f)) { DB::getDB()->getMysqli()->query('DROP PROCEDURE IF EXISTS `levelNodePayouts_nodevalue_distinct_hits`;'); DB::getDB()->getMysqli()->query('DROP PROCEDURE IF EXISTS `levelNodePayouts_nodevalue_hits`;'); } return empty($spc6952f) ? BackgroundJob::FUNCTION_COMPLETED : BackgroundJob::FUNCTION_MUST_RESTART; } public static function bjCreateProcedureLevelNodePayouts($sp386ba2, $sp0be594) { return BackgroundJob::FUNCTION_COMPLETED; } public static function bjCreateProcedureLevelNodePayoutsDistinctHits($sp386ba2, $sp0be594) { return BackgroundJob::FUNCTION_COMPLETED; } public static function deleteDuplicateConvRows($spd62e13, $sp1de23d, $spaaab64 = null) { $sp3a7795 = false; $spce19c0 = "SELECT DISTINCT duplicates.id\n                        FROM stats_root as base\n                        RIGHT JOIN stats_root as duplicates ON (duplicates.idHit = base.idHit AND duplicates.id > base.id)\n                        WHERE\n                            base.`timestamp` >= {$spd62e13} AND base.`timestamp` <= {$sp1de23d}\n                            AND base.idConversion <=> duplicates.idConversion\n                            AND duplicates.id > base.id"; if ($spaaab64 !== null) { $spce19c0 .= " LIMIT {$spaaab64}"; } $sp34c508 = DB::getDB()->rawQuery($spce19c0, null, false); if (count($sp34c508)) { $sp849ce8 = implode(',', twoDimArrayFlattenedValues($sp34c508)); $spce19c0 = "DELETE FROM stats_root WHERE id IN ({$sp849ce8})"; DB::getDB()->rawQuery($spce19c0, null, false); } if (isEmpty(DB::getDB()->getLastError())) { $sp3a7795 = $spaaab64 === null || $spaaab64 > count($sp34c508); } return $sp3a7795; } public static function nullifyConversion($sp139aca) { $sp0be594 = array(self::COLUMN_ID_CONVERSION => null, self::COLUMN_HIT_PAYOUT => 0); DB::getDB()->where(self::COLUMN_ID_CONVERSION, $sp139aca)->update(self::TABLE_NAME, $sp0be594); } public static function getFluxOfferIdByHitId($sp5a71fa) { $spf5ffbf = DB::getDB()->where(self::COLUMN_ID_HIT, $sp5a71fa)->getOne(self::TABLE_NAME, self::COLUMN_ID_FLUX_OFFER); if ($spf5ffbf) { $sp9b46e6 = $spf5ffbf[self::COLUMN_ID_FLUX_OFFER]; } else { $sp9b46e6 = null; } return $sp9b46e6; } public static function getVisitorIdByHitId($sp5a71fa) { $spf5ffbf = DB::getDB()->where(self::COLUMN_ID_HIT, $sp5a71fa)->getOne(self::TABLE_NAME, self::COLUMN_ID_VISITOR); if ($spf5ffbf) { $sp96815b = $spf5ffbf[self::COLUMN_ID_VISITOR]; } else { $sp96815b = null; } return $sp96815b; } public static function getIncomingTrafficIdByHitId($sp5a71fa) { $spf5ffbf = DB::getDB()->where(self::COLUMN_ID_HIT, $sp5a71fa)->getOne(self::TABLE_NAME, self::COLUMN_ID_INCOMING_TRAFFIC); if ($spf5ffbf && isset($spf5ffbf[self::COLUMN_ID_INCOMING_TRAFFIC])) { $spa59931 = $spf5ffbf[self::COLUMN_ID_INCOMING_TRAFFIC]; } else { $spa59931 = null; } return $spa59931; } public static function saveHit($sp263669, $sp7730be) { if (!$sp263669 || !$sp7730be) { $sp2f8927 = $sp263669 == null ? '- hitRecord is null -' : ''; $sp2f8927 .= $sp7730be == null ? '- node is null - ' : ''; DBTableHistory::addEventSystemMessage('saveHit() ERROR: ' . $sp2f8927, DBTableHistory::EVENT_LEVEL_ERROR); return; } $sp3d9301 = $sp7730be->getType(); $spc62a11 = $sp7730be->getParams(); $spe96eed = Session::loadParam(Session::KEY_INCOMING_TRAFFIC); if (!$spe96eed) { DBTableHistory::addEventSystemMessage('saveHit() WARNING: No incomingTraffic could be loaded from the session. Trying to find visitor\'s incoming traffic id from cookie...'); $spb02cf8 = Session::loadParam(Session::KEY_INCOMING_TRAFFIC_ID); if ($spb02cf8) { $spe96eed = DBTableIncomingTraffic::load($spb02cf8); } if ($spe96eed) { DBTableHistory::addEventSystemMessage('saveHit() UNDER CONTROL: incomingTraffic reverted through cookie'); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); } else { DBTableHistory::addEventSystemMessage('saveHit() WARNING 2: incomingTrafficId could not be loaded from cookie... let\'s try to get it from DB'); $spf5ffbf = DB::getDB()->where(self::COLUMN_CONNECTION_IP, ip2long($sp263669->getVisitorInfo()->getIp()))->orderBy(self::COLUMN_ID, 'DESC')->getOne(self::TABLE_NAME, self::COLUMN_ID_INCOMING_TRAFFIC); if ($spf5ffbf && !empty($spf5ffbf[self::COLUMN_ID_INCOMING_TRAFFIC])) { $spe96eed = DBTableIncomingTraffic::load($spf5ffbf[self::COLUMN_ID_INCOMING_TRAFFIC]); if ($spe96eed) { DBTableHistory::addEventSystemMessage('saveHit() UNDER CONTROL: incomingTraffic reverted through DB'); } } } if (!$spe96eed) { DBTableHistory::addEventSystemMessage('saveHit() ERROR: There\'s no incomingTraffic: ' . serialize($_SERVER), DBTableHistory::EVENT_LEVEL_ERROR); return; } } $sp97cc8b = $spe96eed->getURLParamsArray(); $sp350981 = array_keys($sp97cc8b); $spa2457d = array_values($sp97cc8b); $sp6c8593 = $sp263669->getVisitorInfo(); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $spda41ab = $sp4e3b33->getLocationInfo(); $sp5959db = $sp4e3b33->getDeviceInfo(); $sp005624 = $sp4e3b33->getNetworkInfo(); $sp9c0a35 = null; if ($sp3d9301 == FunnelNodeTypes::FLUX_OFFER) { $sp8c3766 = FluxPage::get($spc62a11['pageId']); if ($sp8c3766) { $sp9c0a35 = $sp8c3766->getTypeParams()['idAffiliateNetwork']; } } $sp0be594 = array(self::COLUMN_TIMESTAMP => $sp263669->getTimeStamp(), self::COLUMN_ID_HIT => $sp263669->getIdHit(), self::COLUMN_ID_VISITOR => $sp6c8593->getIdVisitor(), self::COLUMN_FUNNEL_ENTRANCE => $sp263669->isFunnelEntrance(), self::COLUMN_NEW_VISITOR => $sp263669->isNewVisitor(), self::COLUMN_ID_VISITED_NODE => $sp263669->getVisitedNodeId(), self::COLUMN_NODE_TYPE => $sp3d9301, self::COLUMN_ID_FLUX_LANDER => $sp3d9301 == FunnelNodeTypes::FLUX_LANDER ? $spc62a11['pageId'] : null, self::COLUMN_ID_FLUX_OFFER => $sp3d9301 == FunnelNodeTypes::FLUX_OFFER ? $spc62a11['pageId'] : null, self::COLUMN_ID_CONVERSION => null, self::COLUMN_HIT_PAYOUT => 0, self::COLUMN_HIT_COST => $sp263669->isFunnelEntrance() && $spe96eed ? $spe96eed->getCost() : 0, self::COLUMN_ID_INCOMING_TRAFFIC => $spe96eed->getIdIncomingTraffic(), self::COLUMN_ID_TRAFFIC_SOURCE => $spe96eed->getIdTrafficSource(), self::COLUMN_ID_CAMPAIGN => DBTableCampaignFunnels::getCampaignIdByFunnelId($spe96eed->getIdFunnel()), self::COLUMN_ID_FUNNEL => $spe96eed->getIdFunnel(), self::COLUMN_URL_PARAM_FIELD_1 => isset($sp350981[0]) ? substr($sp350981[0], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_2 => isset($sp350981[1]) ? substr($sp350981[1], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_3 => isset($sp350981[2]) ? substr($sp350981[2], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_4 => isset($sp350981[3]) ? substr($sp350981[3], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_5 => isset($sp350981[4]) ? substr($sp350981[4], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_6 => isset($sp350981[5]) ? substr($sp350981[5], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_7 => isset($sp350981[6]) ? substr($sp350981[6], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_8 => isset($sp350981[7]) ? substr($sp350981[7], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_9 => isset($sp350981[8]) ? substr($sp350981[8], 0, 45) : null, self::COLUMN_URL_PARAM_FIELD_10 => isset($sp350981[9]) ? substr($sp350981[9], 0, 45) : null, self::COLUMN_URL_PARAM_VALUE_1 => isset($spa2457d[0]) ? substr($spa2457d[0], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_2 => isset($spa2457d[1]) ? substr($spa2457d[1], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_3 => isset($spa2457d[2]) ? substr($spa2457d[2], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_4 => isset($spa2457d[3]) ? substr($spa2457d[3], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_5 => isset($spa2457d[4]) ? substr($spa2457d[4], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_6 => isset($spa2457d[5]) ? substr($spa2457d[5], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_7 => isset($spa2457d[6]) ? substr($spa2457d[6], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_8 => isset($spa2457d[7]) ? substr($spa2457d[7], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_9 => isset($spa2457d[8]) ? substr($spa2457d[8], 0, 255) : null, self::COLUMN_URL_PARAM_VALUE_10 => isset($spa2457d[9]) ? substr($spa2457d[9], 0, 255) : null, self::COLUMN_LOCATION_CONTINENT => substrNull($spda41ab->getContinent(), 0, 40), self::COLUMN_LOCATION_COUNTRY_CODE => substrNull($spda41ab->getCountryCode(), 0, 2), self::COLUMN_LOCATION_COUNTRY_NAME => substrNull($spda41ab->getCountryName(), 0, 40), self::COLUMN_LOCATION_REGION => substrNull($spda41ab->getRegion(), 0, 42), self::COLUMN_LOCATION_CITY => substrNull($spda41ab->getCity(), 0, 40), self::COLUMN_LOCATION_LATITUDE => $spda41ab->getLatitude(), self::COLUMN_LOCATION_LONGITUDE => $spda41ab->getLongitude(), self::COLUMN_LOCATION_TIMEZONE => substrNull($spda41ab->getTimezone(), 0, 40), self::COLUMN_CONNECTION_IP => ip2long($sp6c8593->getIp()), self::COLUMN_CONNECTION_REFERRER => $spe96eed ? substrNull($spe96eed->getReferrer(), 0, 255) : null, self::COLUMN_CONNECTION_ISP => substrNull($sp005624->getIsp(), 0, 60), self::COLUMN_DEVICE_TYPE => substrNull($sp5959db->getType(), 0, 13), self::COLUMN_DEVICE_BRAND => substrNull($sp5959db->getBrand(), 0, 45), self::COLUMN_DEVICE_NAME => substrNull($sp5959db->getName(), 0, 45), self::COLUMN_DEVICE_OS => substrNull($sp5959db->getOs(), 0, 45), self::COLUMN_DEVICE_OS_VERSION => substrNull($sp5959db->getOsVersion(), 0, 45), self::COLUMN_DEVICE_BROWSER => substrNull($sp5959db->getBrowser(), 0, 45), self::COLUMN_DEVICE_BROWSER_VERSION => substrNull($sp5959db->getBrowserVersion(), 0, 45), self::COLUMN_DEVICE_DISPLAY_WIDTH => substrNull($sp5959db->getDisplayWidth(), 0, 5), self::COLUMN_DEVICE_DISPLAY_HEIGHT => substrNull($sp5959db->getDisplayHeight(), 0, 5), self::COLUMN_ID_OFFER_SOURCE => substrNull($sp9c0a35, 0, 20), self::COLUMN_FLAGS => $sp263669->isFilteredTraffic() ? self::FLAG_FILTERED_TRAFFIC : 0); DB::getDB()->insert(self::TABLE_NAME, $sp0be594); } public static function generateUnrecordedStats($sp9356fe = self::STATS_GENERATION_BATCH_SIZE) { $sp27d0d1 = DBTableMeta::get(DBTableMeta::KEY_STATS_ROOT_LATEST_PROCESSED_ID_HIT, 0); $spf1e87f = $sp9356fe; $spec0ab5 = DBTableHitRecords::getRecordsCount(); $sp2ae5e6 = min($spec0ab5, $spf1e87f); if (!$sp2ae5e6) { return; } $sp4318f3 = DBTableHitRecords::COLUMN_ID_HIT; $sp133cfb = DBTableHitRecords::TABLE_NAME; $sp34c508 = DB::getDB()->rawQuery("SELECT {$sp4318f3} FROM {$sp133cfb}\n                                                ORDER BY {$sp4318f3} ASC\n                                                LIMIT " . ($sp2ae5e6 - 1) . ', 1'); if (!$sp34c508 || !count($sp34c508)) { return; } $spcdaca2 = $sp34c508[0][$sp4318f3]; DB::getDB()->startTransaction(); try { $sp49fa98 = DBTableHitRecords::loadRange($sp27d0d1 + 1, $spcdaca2); $sp0110cf = array(); foreach ($sp49fa98 as $sp263669) { if (!isset($sp0110cf[$sp263669->getVisitorInfoId()])) { $sp0110cf[$sp263669->getVisitorInfoId()] = 1; $sp6c8593 = $sp263669->getVisitorInfo(); if ($sp6c8593) { $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); if ($sp4e3b33) { DBTableVisitorsInfoAdvanced::insert($sp4e3b33, true); } } } } if (!empty($sp49fa98)) { $spd62e13 = $sp49fa98[0]->getTimeStamp(); $sp1de23d = $sp49fa98[count($sp49fa98) - 1]->getTimeStamp(); $spc759a3 = self::createNonRecordedConvsTable($spd62e13, $sp1de23d); } DB::getDB()->rawQuery('SET SQL_BIG_SELECTS=1;'); $spce19c0 = 'INSERT INTO stats_root
                    (
                        ' . implode(',', self::$aAllColumnsExceptId) . '
                    )
                    SELECT
                        hit_records.`timeStamp`,
                        hit_records.idHit,
                        hit_records.idVisitor,
                        hit_records.bFunnelEntrance,
                        hit_records.bNewVisitor,
                        hit_records.visitedNodeId,
                        node.nodeType,
                        IF(node.nodeType=' . FunnelNodeTypes::FLUX_LANDER . ', SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( node.nodeParams, \'"pageId";i:\' , -1 ),\';\', 1),\'}\',-1),null) as idFluxLander,
                        IF(node.nodeType=' . FunnelNodeTypes::FLUX_OFFER . ', SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( node.nodeParams, \'"pageId";i:\' , -1 ),\';\', 1),\'}\',-1),null) as idFluxOffer,
                        conversion.idConversion,
                        IF(conversion.idConversion IS NOT NULL, conversion.payout, 0) as hitPayout,
                        IF( (traffic_sources.costType=' . TrafficSource::COST_TYPE_CPE . ' AND hit_records.bFunnelEntrance=1)
                            OR
                            (traffic_sources.costType=' . TrafficSource::COST_TYPE_CPA . ' AND hit_records.bFunnelEntrance=1 AND conversion.idConversion IS NOT NULL)
                            ,incoming.cost, 0
                        ) as hitCost,
                        incoming.idIncomingTraffic,
                        incoming.idTrafficSource,
                        campaign.idCampaign,
                        funnel.idFunnel,
                        urlParams1.urlParamField as urlParamField1,
                        urlParams2.urlParamField as urlParamField2,
                        urlParams3.urlParamField as urlParamField3,
                        urlParams4.urlParamField as urlParamField4,
                        urlParams5.urlParamField as urlParamField5,
                        urlParams6.urlParamField as urlParamField6,
                        urlParams7.urlParamField as urlParamField7,
                        urlParams8.urlParamField as urlParamField8,
                        urlParams9.urlParamField as urlParamField9,
                        urlParams10.urlParamField as urlParamField10,
                        urlParams1.urlParamValue as urlParamValue1,
                        urlParams2.urlParamValue as urlParamValue2,
                        urlParams3.urlParamValue as urlParamValue3,
                        urlParams4.urlParamValue as urlParamValue4,
                        urlParams5.urlParamValue as urlParamValue5,
                        urlParams6.urlParamValue as urlParamValue6,
                        urlParams7.urlParamValue as urlParamValue7,
                        urlParams8.urlParamValue as urlParamValue8,
                        urlParams9.urlParamValue as urlParamValue9,
                        urlParams10.urlParamValue as urlParamValue10,
                        location.continent as locationContinent,
                        location.countryCode as locationCountryCode,
                        location.countryName as locationCountryName,
                        location.city as locationCity,
                        location.region as locationRegion,
                        location.latitude as locationLatitude,
                        location.longitude as locationLongitude,
                        location.timezone as locationTimezone,
                        visitor.ipv4 as connectionIpv4,
                        incoming.referrer as connectionReferrer,
                        network.isp as connectionIsp,
                        device.type as deviceType,
                        device.brand as deviceBrand,
                        device.name as deviceName,
                        device.os as deviceOs,
                        device.osVersion as deviceOsVersion,
                        device.browser as deviceBrowser,
                        device.browserVersion as deviceBrowserVersion,
                        device.displayWidth as deviceDisplayWidth,
                        device.displayHeight as deviceDisplayHeight,
                        offerSource.idAffiliateNetwork as idOfferSource,
                        IF(hit_records.bFilteredTraffic, ' . self::FLAG_FILTERED_TRAFFIC . ', 0) as statFlags
                    FROM hit_records
                    LEFT JOIN hit_conversions conversion ON conversion.idHit = hit_records.idHit
                    LEFT JOIN campaign_funnel_nodes node ON node.idNode = hit_records.visitedNodeId
                    LEFT JOIN campaign_funnels funnel ON funnel.idFunnel = node.idFunnel
                    LEFT JOIN campaigns campaign ON campaign.idCampaign = funnel.idCampaign
                    LEFT JOIN incoming_traffic incoming ON incoming.idIncomingTraffic = hit_records.idIncomingTraffic
                    LEFT JOIN pages offerPage on offerPage.idPage = IF(node.nodeType=' . FunnelNodeTypes::FLUX_OFFER . ', SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( node.nodeParams, \'"pageId";i:\' , -1 ),\';\', 1),\'}\',-1),null)
                    LEFT JOIN affiliate_networks offerSource on offerSource.idAffiliateNetwork = SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(offerPage.aTypeParams, \'"idAffiliateNetwork";s\' , -1 ),\'";\', 1),\'"\',-1)
                    LEFT JOIN visitors_info visitor on visitor.idVisitor = hit_records.idVisitor
                    LEFT JOIN visitors_info_advanced visitorAdvanced on visitorAdvanced.idVisitor = hit_records.idVisitor
                    LEFT JOIN visitor_locations_info location on location.hash = visitorAdvanced.locationHash
                    LEFT JOIN visitor_networks_info network on network.hash = visitorAdvanced.networkHash
                    LEFT JOIN visitor_devices_info device on device.hash = visitorAdvanced.deviceHash
                    LEFT JOIN traffic_source_url_params params1 ON params1.idTrafficSource = incoming.idTrafficSource AND params1.iParam = 1
                    LEFT JOIN traffic_source_url_params params2 ON params2.idTrafficSource = incoming.idTrafficSource AND params2.iParam = 2
                    LEFT JOIN traffic_source_url_params params3 ON params3.idTrafficSource = incoming.idTrafficSource AND params3.iParam = 3
                    LEFT JOIN traffic_source_url_params params4 ON params4.idTrafficSource = incoming.idTrafficSource AND params4.iParam = 4
                    LEFT JOIN traffic_source_url_params params5 ON params5.idTrafficSource = incoming.idTrafficSource AND params5.iParam = 5
                    LEFT JOIN traffic_source_url_params params6 ON params6.idTrafficSource = incoming.idTrafficSource AND params6.iParam = 6
                    LEFT JOIN traffic_source_url_params params7 ON params7.idTrafficSource = incoming.idTrafficSource AND params7.iParam = 7
                    LEFT JOIN traffic_source_url_params params8 ON params8.idTrafficSource = incoming.idTrafficSource AND params8.iParam = 8
                    LEFT JOIN traffic_source_url_params params9 ON params9.idTrafficSource = incoming.idTrafficSource AND params9.iParam = 9
                    LEFT JOIN traffic_source_url_params params10 ON params10.idTrafficSource = incoming.idTrafficSource AND params10.iParam = 10
                    LEFT JOIN incoming_traffic_url_params AS urlParams1 ON urlParams1.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams1.urlParamField = params1.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams2 ON urlParams2.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams2.urlParamField = params2.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams3 ON urlParams3.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams3.urlParamField = params3.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams4 ON urlParams4.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams4.urlParamField = params4.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams5 ON urlParams5.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams5.urlParamField = params5.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams6 ON urlParams6.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams6.urlParamField = params6.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams7 ON urlParams7.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams7.urlParamField = params7.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams8 ON urlParams8.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams8.urlParamField = params8.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams9 ON urlParams9.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams9.urlParamField = params9.`field`
                    LEFT JOIN incoming_traffic_url_params AS urlParams10 ON urlParams10.idIncomingTraffic = incoming.idIncomingTraffic AND urlParams10.urlParamField = params10.`field`
                    LEFT JOIN traffic_sources ON traffic_sources.idTrafficSource = incoming.idTrafficSource
                    WHERE hit_records.`idHit` > ' . $sp27d0d1 . ' AND hit_records.`idHit` <= ' . $spcdaca2 . '
                    GROUP BY hit_records.idHit'; $spa73024 = false; DB::getDB()->rawQuery($spce19c0, null, false); $sp400487 = DB::getDB()->getLastError(); if (empty($sp400487)) { $speeabf7 = DBTableHitRecords::TABLE_NAME; $spe52311 = DBTableHitRecords::COLUMN_ID_HIT; $spce19c0 = "DELETE FROM {$speeabf7} WHERE {$spe52311} < {$spcdaca2}"; DB::getDB()->rawQuery($spce19c0, null, false); $sp400487 = DB::getDB()->getLastError(); if (empty($sp400487)) { $spa73024 = true; } } if ($spa73024) { DBTableMeta::set(DBTableMeta::KEY_STATS_ROOT_LATEST_PROCESSED_ID_HIT, $spcdaca2); if (isset($sp1de23d)) { DBTableMeta::set(DBTableMeta::KEY_STATS_ROOT_LATEST_PROCESSED_TIMESTAMP, $sp1de23d); } if (isset($spc759a3)) { self::updateCPACostOfNonRecordedConvs($spc759a3); self::dropNonRecordedConvsTable($spc759a3); } DB::getDB()->commit(); } else { file_put_contents(DBTableApplication::getCacheFolderPath() . 'errors.txt', date('Y-m-d H:i:s') . ": (D) - {$sp400487}" . PHP_EOL . PHP_EOL, FILE_APPEND | LOCK_EX); DB::getDB()->rollback(); } } catch (Exception $sp7c4ea2) { file_put_contents(DBTableApplication::getCacheFolderPath() . 'errors.txt', date('Y-m-d H:i:s') . ': (E) - ' . $sp7c4ea2 . PHP_EOL . PHP_EOL, FILE_APPEND | LOCK_EX); DB::getDB()->rollback(); } } private static function createNonRecordedConvsTable($spd62e13 = 0, $sp1de23d = PHP_INT_MAX) { $spc759a3 = '_nonrecordedconvs_' . md5(microtime() . rand()); $speaab23 = "SELECT DISTINCT\n                                        conv.idHit,\n                                        conv.idConversion,\n                                        conv.payout,\n                                        conv.timestamp\n                                    FROM hit_conversions conv\n                                    LEFT JOIN stats_root root2 ON conv.idConversion=root2.idConversion\n                                    WHERE conv.hitTimestamp >= {$spd62e13} AND conv.hitTimestamp <= {$sp1de23d}\n                                        AND root2.id is null\n                                    ORDER BY conv.timestamp ASC, conv.idHit ASC"; DB::temporaryTableCreate($spc759a3, $speaab23, null, array('idHit')); return $spc759a3; } private static function dropNonRecordedConvsTable($spc759a3) { DB::getDB()->rawQuery(DB::temporaryTableGetDropQuery($spc759a3)); } private static function updateCPACostOfNonRecordedConvs($spc759a3) { $sp7ed8de = '_root_ids_' . md5(microtime() . rand()); $spd9a797 = "SELECT DISTINCT\n                            root2.id\n                            FROM {$spc759a3} nonReportedConv\n                            JOIN stats_root root1 ON root1.idHit = nonReportedConv.idHit\n                            JOIN stats_root root2 ON root2.idIncomingTraffic = root1.idIncomingTraffic\n                            WHERE root2.bFunnelEntrance = 1"; DB::temporaryTableCreate($sp7ed8de, $spd9a797, 'id'); $spce19c0 = "UPDATE stats_root\n                JOIN {$sp7ed8de} AS _root_ids ON _root_ids.id = stats_root.id\n                JOIN traffic_sources ON traffic_sources.idTrafficSource = stats_root.idTrafficSource\n                JOIN incoming_traffic ON incoming_traffic.idIncomingTraffic = stats_root.idIncomingTraffic\n                SET stats_root.hitCost = incoming_traffic.cost\n                WHERE traffic_sources.costType = " . TrafficSource::COST_TYPE_CPA; DB::getDB()->rawQuery($spce19c0, null, false); $sp400487 = DB::getDB()->getLastError(); DB::getDB()->rawQuery(DB::temporaryTableGetDropQuery($sp7ed8de)); return $sp400487; } public static function addUnrecordedConversions($spd62e13 = 0, $sp1de23d = PHP_INT_MAX, $sp1987f6 = false) { for ($sp0a3703 = 0; $sp0a3703 < 3; $sp0a3703++) { $spa73024 = true; $sp2f8927 = null; DB::getDB()->startTransaction(); if ($sp1987f6) { DB::getDB()->getMysqli()->query('LOCK TABLES
                                                    stats_root WRITE,
                                                    stats_root root READ,
                                                    stats_root root2 READ,
                                                    hit_conversions conv READ
                                                '); } try { $spc759a3 = self::createNonRecordedConvsTable($spd62e13, $sp1de23d); $spce19c0 = 'INSERT INTO stats_root
                            (
                                ' . implode(',', self::$aAllColumnsExceptId) . "\n                            )\n                        SELECT DISTINCT\n                        root.timestamp,\n                        root.idHit,\n                        root.idVisitor,\n                        0 as bFunnelEntrance,\n                        0 as bNewVisitor,\n                        root.idVisitedNode,\n                        root.nodeType,\n                        root.idFluxLander,\n                        root.idFluxOffer,\n                        nonReportedConv.idConversion,\n                        nonReportedConv.payout as hitPayout,\n                        0 as hitCost,\n                        root.idIncomingTraffic,\n                        root.idTrafficSource,\n                        root.idCampaign,\n                        root.idFunnel,\n                        root.urlParamField1,\n                        root.urlParamField2,\n                        root.urlParamField3,\n                        root.urlParamField4,\n                        root.urlParamField5,\n                        root.urlParamField6,\n                        root.urlParamField7,\n                        root.urlParamField8,\n                        root.urlParamField9,\n                        root.urlParamField10,\n                        root.urlParamValue1,\n                        root.urlParamValue2,\n                        root.urlParamValue3,\n                        root.urlParamValue4,\n                        root.urlParamValue5,\n                        root.urlParamValue6,\n                        root.urlParamValue7,\n                        root.urlParamValue8,\n                        root.urlParamValue9,\n                        root.urlParamValue10,\n                        root.locationContinent,\n                        root.locationCountryCode,\n                        root.locationCountryName,\n                        root.locationCity,\n                        root.locationRegion,\n                        root.locationLatitude,\n                        root.locationLongitude,\n                        root.locationTimezone,\n                        root.connectionIpv4,\n                        root.connectionReferrer,\n                        root.connectionIsp,\n                        root.deviceType,\n                        root.deviceBrand,\n                        root.deviceName,\n                        root.deviceOs,\n                        root.deviceOsVersion,\n                        root.deviceBrowser,\n                        root.deviceBrowserVersion,\n                        root.deviceDisplayWidth,\n                        root.deviceDisplayHeight,\n                        root.idOfferSource,\n                        root.statFlags\n                        FROM stats_root root\n                        RIGHT JOIN {$spc759a3} nonReportedConv ON nonReportedConv.idHit = root.idHit\n                        LEFT JOIN incoming_traffic ON incoming_traffic.idIncomingTraffic = root.idIncomingTraffic\n                        LEFT JOIN traffic_sources ON traffic_sources.idTrafficSource = root.idTrafficSource\n                        WHERE root.idHit IS NOT NULL"; DB::getDB()->rawQuery($spce19c0, null, false); $sp2f8927 = DB::getDB()->getLastError(); if (!empty($sp2f8927)) { $spa73024 = false; } else { $sp2f8927 = self::updateCPACostOfNonRecordedConvs($spc759a3); if (!empty($sp2f8927)) { $spa73024 = false; } } self::dropNonRecordedConvsTable($spc759a3); if ($spa73024) { DB::getDB()->commit(); } else { DB::getDB()->rollback(); } } catch (Exception $sp7c4ea2) { $sp2f8927 = $sp7c4ea2->getMessage(); $spa73024 = false; DB::getDB()->rollback(); } if ($sp1987f6) { DB::getDB()->getMysqli()->query('UNLOCK TABLES'); } if (!$sp2f8927) { if ($sp0a3703) { DBTableHistory::addEventSystemMessage("DBTableStatsRoot::addUnrecordedConversions() SUCCESS at attempt {$sp0a3703}"); } break; } else { DBTableHistory::addEventSystemMessage("DBTableStatsRoot::addUnrecordedConversions() ERROR at attempt {$sp0a3703} -> {$sp2f8927}", DBTableHistory::EVENT_LEVEL_ERROR); sleep(1); } } if ($sp2f8927) { DBTableHistory::addEventSystemMessage('DBTableStatsRoot::addUnrecordedConversions() FATAL ERROR - All attempts failed', DBTableHistory::EVENT_LEVEL_ERROR); } return $spa73024; } public static function criteriasToWhereClause($spcfd51b) { return DB::buildCriterias('root', $spcfd51b); } public static function getCount($spcfd51b = null) { $spc372f1 = 0; $spce19c0 = 'SELECT COUNT(id) as nbRows FROM ' . self::TABLE_NAME; $spffdd32 = self::criteriasToWhereClause($spcfd51b); if (!empty($spffdd32)) { $spce19c0 .= " WHERE {$spffdd32}"; } $sp34c508 = DB::getDB()->rawQuery($spce19c0); if ($sp34c508 && count($sp34c508)) { $spc372f1 = $sp34c508[0]['nbRows']; } return $spc372f1; } public static function deleteWhere($spe68345, $spe85583) { DB::getDB()->where($spe68345, $spe85583)->delete(self::TABLE_NAME); DBTableStatsAggregate::deleteWhere($spe68345, $spe85583); } public static function getVisitorIds($sp8c204d) { $sp8c204d = encapsulateEntries($sp8c204d, '\'', '\''); $spa74095 = array(); $sp9bde90 = implode(',', $sp8c204d); $sp5e5976 = DBTableStatsRoot::TABLE_NAME; $spe49994 = DBTableStatsRoot::COLUMN_ID_VISITOR; $spfb9134 = DBTableStatsRoot::COLUMN_ID_HIT; $spce19c0 = "SELECT DISTINCT {$spe49994}\n                FROM `{$sp5e5976}`\n                WHERE `{$spfb9134}` IN ({$sp9bde90})"; $sp34c508 = DB::getDB()->rawQuery($spce19c0); if (!empty($sp34c508)) { foreach ($sp34c508 as $sp737dec) { $spa74095[] = $sp737dec[$spe49994]; } } return $spa74095; } public static function getTimerange($spe68345, $spef2c15, &$sp74e939, &$spb457eb) { $spef2c15 = encapsulateEntries($spef2c15, '\'', '\'', true); $sp5df54c = implode(',', $spef2c15); $sp5e5976 = DBTableStatsRoot::TABLE_NAME; $sp1f5656 = DBTableStatsRoot::COLUMN_TIMESTAMP; $spce19c0 = "SELECT\n                MIN(`{$sp1f5656}`) as `timeStart`,\n                MAX(`{$sp1f5656}`) as `timeEnd`\n                FROM `{$sp5e5976}`\n                WHERE `{$spe68345}` IN ({$sp5df54c})"; $sp34c508 = DB::getDB()->rawQuery($spce19c0); if (!empty($sp34c508)) { $sp74e939 = $sp34c508[0]['timeStart']; $spb457eb = $sp34c508[0]['timeEnd']; } else { $sp74e939 = null; $spb457eb = null; } } public static function getVisitsTimerange($spfe9bdc, &$sp74e939, &$spb457eb) { self::getTimerange(DBTableStatsRoot::COLUMN_ID_VISITOR, $spfe9bdc, $sp74e939, $spb457eb); } public static function getIncomingTrafficTimeRange($spa59931) { $sp387f57 = array(); $sp34c508 = DB::getDB()->rawQuery("SELECT min(timestamp) as start, max(timestamp) as end\n                                            FROM stats_root\n                                            WHERE idIncomingTraffic = '{$spa59931}'"); if (!empty($sp34c508) && !empty($sp34c508[0]['start']) && !empty($sp34c508[0]['end'])) { $sp387f57[] = $sp34c508[0]['start']; $sp387f57[] = $sp34c508[0]['end']; } return $sp387f57; } public static function getHitTimestampsAndOfferPayouts($spf59b81) { $sp98892a = array(); $sp44d864 = implode(',', encapsulateEntries($spf59b81, '\'', '\'')); $spce19c0 = "SELECT \n                stats_root.idHit,\n                MIN(stats_root.`timestamp`) AS `hitTimestamp`,\n                CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(pages.aTypeParams, '\"payout\";s:' , -1 ), '\";s:18:\"idAffiliateNetwork\"', 1), ':\"', -1) AS DECIMAL(10,4)) AS `payout`\n                FROM stats_root\n                JOIN pages ON pages.idPage = stats_root.idFluxOffer\n                WHERE stats_root.idFluxOffer IS NOT NULL \n                AND stats_root.idHit IN ({$sp44d864})\n                GROUP BY stats_root.idHit"; $sp34c508 = DB::getDB()->rawQuery($spce19c0); if (!empty($sp34c508)) { foreach ($sp34c508 as $sp64ccca) { $sp5a71fa = $sp64ccca['idHit']; $sp98892a[$sp5a71fa] = $sp64ccca; } } return $sp98892a; } const BJ_CREATE_PROCEDURE_INVALID_INDIRECT_CONVERSIONS_TITLE = 'Create Procedure for Hits to Conversions'; const BJ_RESET_STATS_TITLE_PREFIX = 'Reset Stats'; const BJ_PROCESS_UNRECORDED_VISITORS_TITLE = 'Process Unrecorded Visitors'; }