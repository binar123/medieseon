<?php
require_once dirname(__FILE__) . '/db.php'; require_once dirname(__FILE__) . '/../tracking/visitorLocationInfo.php'; class DBTableVisitorLocationsInfo { const TABLE_NAME = 'visitor_locations_info'; const COLUMN_HASH = 'hash'; const COLUMN_CONTINENT = 'continent'; const COLUMN_COUNTRY_CODE = 'countryCode'; const COLUMN_COUNTRY_NAME = 'countryName'; const COLUMN_CITY = 'city'; const COLUMN_REGION = 'region'; const COLUMN_LATITUDE = 'latitude'; const COLUMN_LONGITUDE = 'longitude'; const COLUMN_TIMEZONE = 'timezone'; public static function createTable() { DB::getDB()->rawQuery('CREATE TABLE `visitor_locations_info` (
                                        `hash` VARCHAR(32) NOT NULL,
                                        `continent` VARCHAR(40) NULL,
                                        `countryCode` VARCHAR(2) NULL,
                                        `countryName` VARCHAR(40) NULL,
                                        `city` VARCHAR(40) NULL,
                                        `region` VARCHAR(42) NULL,
                                        `latitude` FLOAT NULL,
                                        `longitude` FLOAT NULL,
                                        `timezone` VARCHAR(40) NULL,
                                        PRIMARY KEY (`hash`),
                                        UNIQUE INDEX `hash_UNIQUE` (`hash` ASC)
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8; '); self::setupBackgroundJobDownloadMaxmindDB(); } public static function upgradeTable($sp874611, $spe4858e) { if ($sp874611 < 0.019) { DB::getDB()->rawQuery('UPDATE visitor_locations_info
                        SET continent = CASE
                                WHEN continent=\'*unknown*\' THEN NULL
                                ELSE continent
                        END,
                        countryCode = CASE
                                WHEN countryCode=\'*unknown*\' THEN NULL
                                ELSE countryCode
                        END,
                        countryName = CASE
                                WHEN countryName=\'*unknown*\' THEN NULL
                                ELSE countryName
                        END,
                        city = CASE
                                WHEN city=\'*unknown*\' THEN NULL
                                ELSE city
                        END,
                        latitude = CASE
                                WHEN latitude=0 THEN NULL
                                ELSE latitude
                        END,
                        longitude = CASE
                                WHEN longitude=0 THEN NULL
                                ELSE longitude
                        END,
                        timezone = CASE
                                WHEN timezone=\'*unknown*\' THEN NULL
                                ELSE timezone
                        END;'); } if ($sp874611 < 1.119) { DB::getDB()->rawQuery('ALTER TABLE `visitor_locations_info` ADD COLUMN `region` VARCHAR(42) NULL DEFAULT NULL AFTER `city`;'); } if ($sp874611 < 1.5085) { self::setupBackgroundJobDownloadMaxmindDB(); } } private static function setupBackgroundJobDownloadMaxmindDB() { $sp873a24 = 'Download Latest Maxmind Location DB'; DBTableBackgroundJobs::deleteByTitle($sp873a24); DBTableBackgroundJobs::setupBackgroundJobs(array($sp873a24 => 'DBTableVisitorLocationsInfo::bjDownloadMaxmindDB')); } public static function bjDownloadMaxmindDB($sp386ba2, $sp0be594) { $sped92cb = 60 * 60 * 24 * 8; $sp3a6fcb = DBTableMeta::get(DBTableMeta::KEY_LATEST_MAXMIND_LOCATION_DOWNLOAD, 0); if (time() > $sp3a6fcb + $sped92cb) { require_once dirname(__FILE__) . '/../admin/tools/remote.php'; $spc9ebd7 = 'GeoLite2-City.mmdb.gz'; $spc936d1 = 'GeoLite2-City.mmdb'; $sp38bf99 = 'GeoLite2-City.md5'; $sp979c22 = "http://geolite.maxmind.com/download/geoip/database/{$spc9ebd7}"; $sp8cd987 = "http://geolite.maxmind.com/download/geoip/database/{$sp38bf99}"; $spe6654d = DBTableApplication::getCacheFolderPath() . $spc9ebd7; $sp64ccca = Remote::download($spe6654d, $sp979c22, true); if ($sp64ccca->res3) { DBTableHistory::addEventSystemMessage("ERROR: Can't download maxmind location db -> {$sp64ccca->res3}", DBTableHistory::EVENT_LEVEL_ERROR); } else { $spc5ae04 = DBTableApplication::getCacheFolderPath() . $sp38bf99; $sp97543d = Remote::download($spc5ae04, $sp8cd987); if ($sp97543d->res2) { $sp86d15a = DBTableApplication::getCacheFolderPath() . $spc936d1; $sp83bec1 = md5_file($sp86d15a); $spd3b4ea = fgets(fopen($spc5ae04, 'r')); if ($sp83bec1 == $spd3b4ea) { $spb3b14a = DBTableApplication::getFilesRoot() . 'libs/maxmind/databases/' . $spc936d1; rename($sp86d15a, $spb3b14a); DBTableMeta::set(DBTableMeta::KEY_LATEST_MAXMIND_LOCATION_DOWNLOAD, time()); } } } } return BackgroundJob::FUNCTION_MUST_RESTART; } public static function getAllEntriesOfColumn($spe28503) { DB::getDB()->groupBy($spe28503); DB::getDB()->orderBy($spe28503, 'ASC'); return DB::getDB()->get(self::TABLE_NAME, null, $spe28503); } public static function getAllCoordinates() { return DB::getDB()->rawQuery('SELECT * FROM (SELECT CONCAT_WS(\',\', ' . self::COLUMN_LATITUDE . ', ' . self::COLUMN_LONGITUDE . ') as coordinates
                                                        FROM ' . self::TABLE_NAME . ') as res
                                        GROUP BY coordinates ORDER BY coordinates ASC'); } public static function load($sp936c7b) { $spda41ab = null; if ($sp936c7b) { $spf5ffbf = DB::getDB()->where(self::COLUMN_HASH, $sp936c7b)->getOne(self::TABLE_NAME); if (isset($spf5ffbf)) { $spda41ab = VisitorLocationInfo::getEmpty(); $spda41ab->setContinent($spf5ffbf[self::COLUMN_CONTINENT] ? $spf5ffbf[self::COLUMN_CONTINENT] : null); $spda41ab->setCountryCode($spf5ffbf[self::COLUMN_COUNTRY_CODE] ? $spf5ffbf[self::COLUMN_COUNTRY_CODE] : null); $spda41ab->setCountryName($spf5ffbf[self::COLUMN_COUNTRY_NAME] ? $spf5ffbf[self::COLUMN_COUNTRY_NAME] : null); $spda41ab->setCity($spf5ffbf[self::COLUMN_CITY] ? $spf5ffbf[self::COLUMN_CITY] : null); $spda41ab->setRegion($spf5ffbf[self::COLUMN_REGION] ? $spf5ffbf[self::COLUMN_REGION] : null); $spda41ab->setLatitude($spf5ffbf[self::COLUMN_LATITUDE] ? $spf5ffbf[self::COLUMN_LATITUDE] : null); $spda41ab->setLongitude($spf5ffbf[self::COLUMN_LONGITUDE] ? $spf5ffbf[self::COLUMN_LONGITUDE] : null); $spda41ab->setTimezone($spf5ffbf[self::COLUMN_TIMEZONE] ? $spf5ffbf[self::COLUMN_TIMEZONE] : null); } } return $spda41ab; } public static function insert($spda41ab) { $sp0be594 = array(self::COLUMN_CONTINENT => substrNull($spda41ab->getContinent(), 0, 40), self::COLUMN_COUNTRY_CODE => substrNull($spda41ab->getCountryCode(), 0, 2), self::COLUMN_COUNTRY_NAME => substrNull($spda41ab->getCountryName(), 0, 40), self::COLUMN_CITY => substrNull($spda41ab->getCity(), 0, 40), self::COLUMN_REGION => substrNull($spda41ab->getRegion(), 0, 42), self::COLUMN_LATITUDE => $spda41ab->getLatitude(), self::COLUMN_LONGITUDE => $spda41ab->getLongitude(), self::COLUMN_TIMEZONE => substrNull($spda41ab->getTimezone(), 0, 40)); $sp5046cb = $sp0be594; $sp5046cb[self::COLUMN_HASH] = $spda41ab->getLocationHash(); DB::buildQueryInsertFieldsAndValues($sp5046cb, $sp1cc8f9, $sp1c3366); $spce19c0 = 'INSERT INTO ' . self::TABLE_NAME . $sp1cc8f9 . $sp1c3366; $spce19c0 .= ' ON DUPLICATE KEY UPDATE '; $spce19c0 .= implode(',', array_map(function ($spc5c0dc, $spc09d00) { return "`{$spc09d00}` = VALUES(`{$spc09d00}`)"; }, $sp0be594, array_keys($sp0be594))); DB::getDB()->rawQuery($spce19c0); } }