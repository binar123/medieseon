<?php
require_once dirname(__FILE__) . '/db.php'; class DBTableHitIds { const TABLE_NAME = 'hit_ids'; const COLUMN_ID = 'id'; public static function createTable() { DB::getDB()->rawQuery('CREATE TABLE `hit_ids` (
                                `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                                PRIMARY KEY (`id`),
                                UNIQUE INDEX `id_UNIQUE` (`id` ASC)
                              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'); } public static function upgradeTable($sp874611, $spe4858e) { } public static function getNewId() { return DB::getDB()->insert(self::TABLE_NAME, array(self::COLUMN_ID => null)); } }