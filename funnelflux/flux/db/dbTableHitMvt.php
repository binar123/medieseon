<?php
require_once dirname(__FILE__) . '/db.php'; require_once dirname(__FILE__) . '/../tracking/hitMvt.php'; class DBTableHitMvt { const TABLE_NAME = 'hit_mvt'; const COLUMN_ID_HIT = 'idHit'; const COLUMN_KEY = 'mvtKey'; const COLUMN_KEY_VALUE = 'mvtKeyValue'; public static function createTable() { DB::getDB()->rawQuery('CREATE TABLE `hit_mvt` (
                                `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                                `idHit` INT UNSIGNED NOT NULL,
                                `mvtKey` VARCHAR(255),
                                `mvtKeyValue` VARCHAR(20000),
                                PRIMARY KEY (`id`),
                                UNIQUE INDEX `id_UNIQUE` (`idHit`,`mvtKey` ASC),
                                KEY `idHit_Key` (`idHit`),
                                KEY `mvtKey_Key` (`mvtKey`)
                              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'); } public static function upgradeTable($sp874611, $spe4858e) { } public static function insert($sp50009c) { $sp5a71fa = $sp50009c->getIdHit(); $sp68c7a0 = $sp50009c->getMvt(); $sp410b05 = array(self::COLUMN_ID_HIT, self::COLUMN_KEY, self::COLUMN_KEY_VALUE); $sp2f1fe5 = array(); foreach ($sp68c7a0 as $sp237ed2 => $spa76def) { $sp0be594 = array($sp5a71fa, $sp237ed2, $spa76def); $sp2f1fe5[] = $sp0be594; } DB::buildQueryMultipleInsertFieldsAndValues($sp410b05, $sp2f1fe5, $sp1cc8f9, $sp1c3366); DB::getDB()->rawQuery('INSERT IGNORE INTO ' . self::TABLE_NAME . $sp1cc8f9 . $sp1c3366 . ' ON DUPLICATE KEY UPDATE ' . self::COLUMN_KEY_VALUE . '=VALUES(' . self::COLUMN_KEY_VALUE . ')'); return DB::getDB()->getMysqli()->insert_id; } }