<?php
require_once dirname(__FILE__) . '/db.php'; require_once dirname(__FILE__) . '/../content/condition.php'; class DBTableConditions { const TABLE_NAME = 'conditions'; const COLUMN_ID = 'idCondition'; const COLUMN_NAME = 'conditionName'; const COLUMN_BLOCKS = 'conditionBlocks'; const COLUMN_SCOPE = 'conditionScope'; public static function createTable() { DB::getDB()->rawQuery('CREATE TABLE `conditions` (
                                `idCondition` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                                `conditionName` VARCHAR(255) NOT NULL,
                                `conditionBlocks` TEXT NOT NULL,
                                `conditionScope` INT(11) DEFAULT NULL,
                                PRIMARY KEY (`idCondition`),
                                UNIQUE INDEX `idCondition_UNIQUE` (`idCondition` ASC),
                                UNIQUE INDEX `name_UNIQUE` (`conditionName` ASC)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                              '); } public static function upgradeTable($sp874611, $spe4858e) { if ($sp874611 < 1.045) { $sp07de04 = self::loadAll(); foreach ($sp07de04 as $sp1f69f7) { $spee8c5f = $sp1f69f7->getConditionBlocks(); $sp5ea884 = false; foreach ($spee8c5f as $spc8e2a2 => &$sp1e6e7d) { foreach ($sp1e6e7d as $sp289f57 => &$spce03a7) { if ($spce03a7[Condition::OR_TEST] == 'Device: Name') { $spce03a7[Condition::OR_TEST] = Condition::TEST_DEVICE_NAME; $sp5ea884 = true; } } } if ($sp5ea884) { $sp1f69f7->setConditionBlocks($spee8c5f); self::save($sp1f69f7); } } } if ($sp874611 < 1.5077) { if (!DB::doesColumnExist('conditions', 'conditionScope')) { DB::getDB()->rawQuery('ALTER TABLE `conditions` ADD COLUMN `conditionScope` int(11) DEFAULT NULL AFTER `conditionBlocks`;'); } $sp873a24 = 'Convert singly used conditions to local'; DBTableBackgroundJobs::deleteByTitle($sp873a24); DBTableBackgroundJobs::setupBackgroundJobs(array($sp873a24 => 'DBTableConditions::bjConvertSinglyUseConditionsToLocal')); } } public static function bjConvertSinglyUseConditionsToLocal() { $sp86465c = 'UPDATE conditions c1
            INNER JOIN(
                SELECT
                    conditions.idCondition,
                    conditions.conditionName,
                    conditions.conditionScope,
                    campaign_funnel_nodes.idFunnel,
                    count(distinct campaign_funnel_nodes.idFunnel) as parentFunnelsCount
                FROM
                    conditions,
                    campaign_funnel_nodes
                WHERE
                    campaign_funnel_nodes.nodeParams LIKE CONCAT(\'%"idCondition";i:\', conditions.idCondition, \';}%\')
                    AND
                    campaign_funnel_nodes.bArchived = 0
                    AND
                    conditions.conditionScope IS NULL
                GROUP BY
                    conditions.idCondition
            ) as c2
        ON
           c1.idCondition = c2.idCondition
        SET
           c1.conditionScope = c2.idFunnel
        WHERE
            c2.parentFunnelsCount=1;'; DB::getDB()->rawQuery($sp86465c); return BackgroundJob::FUNCTION_COMPLETED; } public static function doesExistById($spaade87) { $spf5ffbf = DB::getDB()->where(self::COLUMN_ID, $spaade87)->getOne(self::TABLE_NAME); return isset($spf5ffbf); } public static function doesExistByName($spadff4a) { $spf5ffbf = DB::getDB()->where(self::COLUMN_NAME, $spadff4a)->getOne(self::TABLE_NAME); return isset($spf5ffbf); } public static function loadById($spaade87) { $sp1f69f7 = null; if ($spaade87) { $spf5ffbf = DB::getDB()->where(self::COLUMN_ID, $spaade87)->getOne(self::TABLE_NAME); if (isset($spf5ffbf)) { $sp1f69f7 = self::dbResultToConditions($spf5ffbf); } } return $sp1f69f7; } public static function loadByName($spadff4a) { $sp1f69f7 = null; if ($spadff4a) { $spf5ffbf = DB::getDB()->where(self::COLUMN_NAME, $spadff4a)->getOne(self::TABLE_NAME); if (isset($spf5ffbf)) { $sp1f69f7 = self::dbResultToConditions($spf5ffbf); } } return $sp1f69f7; } public static function loadAll() { $sp07de04 = array(); $sp6867b5 = DB::getDB()->orderBy(self::COLUMN_NAME, 'ASC')->get(self::TABLE_NAME); if ($sp6867b5) { foreach ($sp6867b5 as $spf5ffbf) { $sp07de04[] = self::dbResultToConditions($spf5ffbf); } } return $sp07de04; } public static function loadGlocal($sp7f34f7) { $sp07de04 = array(); $sp6867b5 = DB::getDB()->where(self::COLUMN_SCOPE, $sp7f34f7)->orWhere(self::COLUMN_SCOPE, NULL, 'IS')->orderBy(self::COLUMN_NAME, 'ASC')->get(self::TABLE_NAME); if ($sp6867b5) { foreach ($sp6867b5 as $spf5ffbf) { $sp07de04[] = self::dbResultToConditions($spf5ffbf); } } return $sp07de04; } private static function dbResultToConditions($spf5ffbf) { $sp1f69f7 = new Condition(); $sp1f69f7->setIdCondition($spf5ffbf[self::COLUMN_ID]); $sp1f69f7->setConditionName($spf5ffbf[self::COLUMN_NAME]); $sp1f69f7->setConditionBlocks(unserialize($spf5ffbf[self::COLUMN_BLOCKS])); $sp1f69f7->setConditionScope($spf5ffbf[self::COLUMN_SCOPE]); return $sp1f69f7; } public static function save($sp1f69f7) { $sp560920 = -1; $sp0be594 = array(self::COLUMN_NAME => $sp1f69f7->getConditionName(), self::COLUMN_BLOCKS => serialize($sp1f69f7->getConditionBlocks()), self::COLUMN_SCOPE => $sp1f69f7->getConditionScope()); if (self::loadById($sp1f69f7->getIdCondition()) != null) { $sp7e9f7a = DB::getDB()->where(self::COLUMN_ID, $sp1f69f7->getIdCondition())->update(self::TABLE_NAME, $sp0be594) !== false; if ($sp7e9f7a) { $sp560920 = $sp1f69f7->getIdCondition(); } } else { $sp560920 = DB::getDB()->insert(self::TABLE_NAME, $sp0be594); if (!$sp560920) { $sp560920 = -1; } } return $sp560920; } }