<?php

require_once dirname(__FILE__) . '/../../includes/settings.php';

$currentDomain = FluxAdminSettings::getScheme().$_SERVER['HTTP_HOST'];
header('Access-Control-Allow-Origin: '.$currentDomain);
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Expose-Headers: Access-Control-Allow-Credentials, Access-Control-Allow-Origin, Access-Control-Allow-Methods");

header('X-Robots-Tag: noindex, noarchive, nofollow', true);
header('Cache-Control: no-cache, must-revalidate', true);
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT', true);
header('Content-type: text/html; charset=utf-8', true);


require_once dirname(__FILE__) . '/../../includes/admin.php';
require_once dirname(__FILE__) . '/../../includes/urlparams.php';
require_once dirname(__FILE__) . '/../../../db/db.php';
require_once dirname(__FILE__) . '/../../tools/backgroundJobs/bjrunner.php';
require_once dirname(__FILE__) . '/../../tools/backgroundJobs/backgroundJobsManager.php';