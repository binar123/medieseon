/* 
 * (c) FunnelFlux.com
 * 
 * Only key codes compatible between all major browsers are listed here
 * Refer to http://unixpapa.com/js/key.html
 * 
 */
/* exported FFKeyboard */
function FFKeyboard() {

  var keyIds = [0,0,0,0];
  var callbacks = {};
  var keyMap = {
    SPACE : new keyCode(32, 'Space'),
    TAB: new keyCode(9, 'Tab'),
    ESC: new keyCode(27, 'Esc'),
    BACKSPACE: new keyCode(8, 'Backspace'),
    SHIFT: new keyCode(16, 'Shift'),
    CONTROL: new keyCode(17, 'Ctrl'),
    ALT: new keyCode(18, 'Alt'),
    CAPSLOCK: new keyCode(20, 'Caps Lock'),
    NUMLOCK: new keyCode(144, 'Num Lock'),
    LEFT: new keyCode(37, 'Left'),
    UP: new keyCode(38, 'Up'),
    RIGHT: new keyCode(39, 'Right'),
    DOWN: new keyCode(40, 'Down'),
    INSERT: new keyCode(45, 'Insert'),
    DELETE: new keyCode(46, 'Delete'),
    HOME: new keyCode(36, 'Home'),
    END: new keyCode(35, 'End'),
    PAGEUP: new keyCode(33, 'Page Up'),
    PAGEDOWN: new keyCode(34, 'Page Down'),
    F1: new keyCode(112, 'F1'),
    F2: new keyCode(113, 'F2'),
    F3: new keyCode(114, 'F3'),
    F4: new keyCode(115, 'F4'),
    F5: new keyCode(116, 'F5'),
    F6: new keyCode(117, 'F6'),
    F7: new keyCode(118, 'F7'),
    F8: new keyCode(119, 'F8'),
    F9: new keyCode(120, 'F9'),
    F10: new keyCode(121, 'F10'),
    F11: new keyCode(122, 'F11'),
    F12: new keyCode(123, 'F12'),
  };
  
  function keyCode(code, name){
    var toString = function(){
      return this.name;
    };
    var obj = {
      code: code,
      name: name,
      toString: toString
    };
    return obj;
  }
      
  /**
   * 
   * @param {type} callback - a function to call when keys are being pressed.
   *                          This function will receive 2 parameters:
   *                              1/ The key code that has just been pressed
   *                              2/ An object containing true for all the currently pressed key codes
   *                          This function must return true if no other handler should manage this shortcut
   * @returns 
   */
  function addShortcutHandler()
  {
    var handler = new function(){
      var shortcuts = [];

      var addShortcut = function(keyCodes, description, callback, hideOnPalette){
        var newShortcut = [];
        var keyId = [0,0,0,0];
        newShortcut['keys'] = keyCodes;
        newShortcut['description'] = description;
        newShortcut['callback'] = callback;
        newShortcut['hide'] = hideOnPalette;
        for(var i in keyCodes){
          switch(keyCodes[i]['code']){
          case 17:  // Ctrl
            keyId[0] = 17;
            break;
          case 16:  // Alt
            keyId[1] = 16;
            break;
          case 18:
            keyId[2] = 18;
            break;
          default:
            if(keyCodes[i]['code']){
              keyId[3] = keyCodes[i]['code'];                  
            } else {
              keyId[3] = keyCodes[i].toUpperCase().charCodeAt(0);
            }
            break;
          }
        }
        keyId = keyId.join('-');
        callbacks[keyId] = callback;
        shortcuts.push(newShortcut);
      };

      var getShortcuts = function(){
        // Just return shortcuts without hide=1
        return shortcuts.filter(function(obj){
          if(!obj.hide){
            return obj;
          }
        });
      };

      return {
        addShortcut: addShortcut,
        getShortcuts: getShortcuts,
      };
    };
    return handler;
  }
  
  /**
   * 
   * @param {type} key
   * @returns {unresolved}
   */
  function getCode(key)
  {
    return keyMap[key] ? keyMap[key].code : key;
  }
  
  /**
   * 
   * @param {type} key
   * @returns {unresolved}
   */
  function getName(key)
  {
    return keyMap[key] ? keyMap[key].name : key;
  }
  /**
   * 
   * @returns {undefined} 
   */
  function overrideShortcuts()
  {
    document.onkeydown = overrideKeyboardEvent;
    document.onkeyup = overrideKeyboardEvent;
  }

  /**
   * 
   * @param {type} e
   * @returns {Boolean} 
   */
  function overrideKeyboardEvent(e)
  {
    var bStopProcessing = false;
    
    switch (e.type) 
    {
    case 'keydown':
      if(e.ctrlKey){
        keyIds[0] = 17;
      } else {
        keyIds[0] = 0;
      }
      if(e.shiftKey){
        keyIds[1] = 16;
      } else {
        keyIds[1] = 0;
      }
      if(e.altKey){
        keyIds[2] = 18;
      } else {
        keyIds[2] = 0;
      }
      switch(e.which){
        case 17:  // Ctrl
          break;
        case 16:  // Alt
          break;
        case 18:
          break;
        default:
          keyIds[3] = e.which;
          break;
      }

      var keyId = keyIds.join('-');
      if(callbacks[keyId]){
        callbacks[keyId]();
        bStopProcessing = true;
      }
      break;
        
    case 'keyup':
      switch(e.which){
      case 17:
        keyIds[0] = 0;
        break;
      case 16:
        keyIds[1] = 0;
        break;
      case 18:
        keyIds[2] = 0;
        break;
      default:
        keyIds[3] = 0;
        break;
      }
      break;
    }

    if( bStopProcessing )
    {
      disabledEventPropagation(e);
      e.preventDefault();
    }
      
    return !bStopProcessing;
  }

  function disabledEventPropagation(e) {
    if (e) {
      if (e.stopPropagation) {
        e.stopPropagation();
      } else if (window.event) {
        window.event.cancelBubble = true;
      }
    }
  }

  function init()
  {
    overrideShortcuts();
  }

  init();

  return {
    keyMap: keyMap,
    getCode: getCode,
    getName: getName,
    addShortcutHandler: addShortcutHandler,
  };
}
