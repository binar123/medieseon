var updateColumnFlat = false;

function getFormatConfidenceRateCell(value,bTree,row,columnDef,flatValue)
{
    if (!bDisplayConfidentRate(columnDef,bTree))
    {
        if (bTree)
        {
            return value;
        }
        else
        {
            return flatValue;
        }

    }
    else
    {
        var valueConfidentRate = getConfidentRateValue(columnDef,row,bTree);
        if (!valueConfidentRate)
        {
            if (bTree)
            {
                return value;
            }
            else
            {
                return flatValue;
            }
        }
    }

    var confidenceRate = valueConfidentRate.split(BayesConfidence.SEPARATOR_HTML)[0];
    var clicks = value;

    setLengthColumnContent(columnDef,confidenceRate,clicks,bTree);
    var color;
    color = "#95A9B7";
    if (confidenceRate > thresholdConfidenceRate)
    {
        color = "#2FF893";
    }

    if (bTree)
    {
        return getHtmlConfidenceRateformatForTree(confidenceRate,clicks,color);
    }
    else
    {
        return getFormatConfidenceRateCellFlat(confidenceRate,clicks,color);
    }
}

function getConfidentRateValue(columnDef, row,bTree)
{
    var confidentValue;
    var columnName = getColumnName(columnDef,bTree);

    switch (columnName)
    {
        case StatsSummaryColumns.CLICKS:
        case StatsSummaryColumns.CTR:
            if (!row['totalrow'] && row[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO])
            {
                confidentValue = row[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][URLParams.STAT_OPTION_WINNERS_CLICK];
            }
            break;

        case StatsSummaryColumns.CONVERSIONS:
        case StatsSummaryColumns.CRUV:
        case StatsSummaryColumns.CRV:
            if (!row['totalrow'] && row[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO])
            {
                confidentValue = row[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][URLParams.STAT_OPTION_WINNERS_CONV];
            }
            break;

        case StatsSummaryColumns.EPUV:
        case StatsSummaryColumns.EPV:
        case StatsSummaryColumns.REVENUE:
            if (!row['totalrow'] && row[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO])
            {
                confidentValue = row[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][URLParams.STAT_OPTION_WINNERS_EPC];
            }
            break;
    }

    return confidentValue;
}

function getHtmlConfidenceRateformatForTree(confidenceRate,clicks,color)
{
    var html = '';
    html += '<div style="text-align: left;">';
    html += '<span style="font-size: 10px;color:' + color + ';">' + confidenceRate + '%</span>';
    html += '<div class="percent-bar-position">';
    html += '<span class="confidence-success-position">' + clicks + '</span>';
    html += '<span class="percent-complete-bar" style="background:' + color + ';width:' + confidenceRate + '%"></span>';
    html += '</div></div>';

    return html;
}

function getFormatConfidenceRateCellFlat(confidenceRate,clicks,color)
{
    var html = '';
    html += '<div style="text-align: left;">';
    html += '<th><td><span class="confidentRateLabel" style="color:' + color + ';">' + confidenceRate + '%</span>';
    html += '</td>';
    html += '<td><span class="clicksLabel" >' + clicks + '</span><span class="percent-complete-bar" style="background:' + color + ';width:' + confidenceRate + '%"></span></td></th>';
    html += '</div>';

    return html;
}

function bConfidenceRateValue(value)
{
    var bResult = true;

    if (typeof value != 'string')
    {
        value = value.toString();
    }

    if (value.indexOf(BayesConfidence.SEPARATOR_HTML) == -1)
    {
        bResult = false;
    }

    return bResult;
}

function bDisplayConfidentRate(columnDef, bTree)
{
    var confidentOptions = getColumnConfidenceRate();
    var columnsVisible = getColumnsVisibilityArray(bTree);
    var result = false;
    var columnName = getColumnName(columnDef,bTree);

    switch (columnName)
    {
        case StatsSummaryColumns.CTR:
            if (inArray(URLParams.STAT_OPTION_WINNERS_CLICK, confidentOptions))
            {
                if (columnsVisible[StatsProcessorColumnsInfo.COLUMN_CLICKS_CTR])
                {
                    result = true;
                }
            }
            break;
        case StatsSummaryColumns.CLICKS:
            if (inArray(URLParams.STAT_OPTION_WINNERS_CLICK, confidentOptions))
            {
                if (!columnsVisible[StatsProcessorColumnsInfo.COLUMN_CLICKS_CTR] && columnsVisible[StatsProcessorColumnsInfo.COLUMN_CLICKS_COUNT])
                {
                    result = true;
                }
            }
            break;

        case StatsSummaryColumns.CRUV:
            if (inArray(URLParams.STAT_OPTION_WINNERS_CONV, confidentOptions))
            {
                if (columnsVisible[StatsProcessorColumnsInfo.COLUMN_CRUV])
                {
                    result = true;
                }
            }
            break;
        case StatsSummaryColumns.CRV:
            if (inArray(URLParams.STAT_OPTION_WINNERS_CONV, confidentOptions))
            {
                if (columnsVisible[StatsProcessorColumnsInfo.COLUMN_CRV] && !columnsVisible[StatsProcessorColumnsInfo.COLUMN_CRUV])
                {
                    result = true;
                }
            }
            break;
        case StatsSummaryColumns.CONVERSIONS:
            if (inArray(URLParams.STAT_OPTION_WINNERS_CONV, confidentOptions))
            {
                if (columnsVisible[StatsProcessorColumnsInfo.COLUMN_CONVERSIONS] && !columnsVisible[StatsProcessorColumnsInfo.COLUMN_CRUV] && !columnsVisible[StatsProcessorColumnsInfo.COLUMN_CRV])
                {
                    result = true;
                }
            }
            break;
        case StatsSummaryColumns.EPUV:
            if (inArray(URLParams.STAT_OPTION_WINNERS_EPC, confidentOptions))
            {
                if (columnsVisible[StatsProcessorColumnsInfo.COLUMN_EPUV])
                {
                    result = true;
                }
            }
            break;
        case StatsSummaryColumns.EPV:
            if (inArray(URLParams.STAT_OPTION_WINNERS_EPC, confidentOptions))
            {
                if (columnsVisible[StatsProcessorColumnsInfo.COLUMN_EPV] && !columnsVisible[StatsProcessorColumnsInfo.COLUMN_EPUV])
                {
                    result = true;
                }
            }
            break;
        case StatsSummaryColumns.REVENUE:
            if (inArray(URLParams.STAT_OPTION_WINNERS_EPC, confidentOptions))
            {
                if (columnsVisible[StatsProcessorColumnsInfo.COLUMN_REVENUE] && !columnsVisible[StatsProcessorColumnsInfo.COLUMN_EPUV] && !columnsVisible[StatsProcessorColumnsInfo.COLUMN_EPV])
                {
                    result = true;
                }
            }
            break;
    }

    return result;
}

function getColumnConfidenceRate()
{
    var columns = (bShowWinners == 'undefined' || bShowWinners)
                    ? $("#select-confidence-options").getSelectionOrder()
                    : [];

    return columns;
}

function getColumnName(columnDef,bTree)
{
    return bTree ? columnDef.name : columnDef.header().innerText.trim();
}

function setLengthColumnContent(columnDef,confidenceRate,clicks,bTree)
{
    var content = confidenceRate + clicks;
    var nbCaract = content.length + 1;
    if (bTree)
    {
        if (nbCaract > 6 && columnDef.width < 100)
        {
            setLengthColumnContentForTree(columnDef);
        }
    }
    else
    {
        if (nbCaract > 6 && columnDef.header().scrollWidth < 100)
        {
            updateColumnFlat = true;
        }
    }

}

function setLengthColumnContentForTree(columnDef)
{
    var columnName = getColumnName(columnDef, true);
    var columnWinner = getGroupColumnWinners(columnName);
    switch (columnWinner)
    {
        case 0:
            if (!updateClickColumn)
            {
                updateClickColumn = true;
            }
            else
            {
                return;
            }
            break;
        case 1:
            if (!updateConversionColumn)
            {
                updateConversionColumn = true;
            }
            else
            {
                return;
            }
            break;
        case 2:
            if (!updateRevenueColumn)
            {
                updateRevenueColumn = true;
            }
            else
            {
                return;
            }
            break;
    }
        columnDef.width += 20;
}

/**
 *
 * @param columnName
 * @returns {number}0 : winner click, 1: winner conversion, 2: winner revenue
 */
function getGroupColumnWinners(columnName)
{
    switch (columnName)
    {
        case StatsSummaryColumns.CTR:
        case StatsSummaryColumns.CLICKS:
            return 0;
        case StatsSummaryColumns.CRUV:
        case StatsSummaryColumns.CRV:
        case StatsSummaryColumns.CONVERSIONS:
            return 1;
        case StatsSummaryColumns.EPUV:
        case StatsSummaryColumns.EPV:
        case StatsSummaryColumns.REVENUE:
            return 2;
    }
}

function updateHeaderSize(columns)
{
    for(var i = 0;i < columns.length ; i++)
    {
        var headerClass = '.' + columns[i].headerCssClass;
        var headerWidth =  $(headerClass).width();
        if (columns[i].width !== headerWidth)
        {
            $(headerClass).width(columns[i].width);
        }
    }


}
