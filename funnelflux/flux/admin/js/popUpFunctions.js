/**
 * Created by yao on 29/11/2015.
 */

/**
 * refresh parent popup when new or udpate ID
 * @type {boolean}
 */
var hasToRefresh = false;

var tabIdSelectOption = [];

var popupDirtyCloseConfirmMessage = "Your settings have been modified but NOT saved. Do you want to close this page and lose all your changes?";

/**
 * treatment after saving new element
 * @param params
 */
function refreshParentPage(params)
{
    var idSelectBox = params.selectElement;
    var idTableSection = params.idTable;
    var tabOptions = params.tabIdSelection;
    var sectionURL = params.sectionURL;

    if (tabOptions && tabOptions.length > 0 && $(idSelectBox).length)
    {
        for (var i = 0; i < tabOptions.length; i++)
        {
            $(idSelectBox).append($("<option></option>")
                .attr("value", tabOptions[i]['id'])
                .text(tabOptions[i]['name']));

            $(idSelectBox).val(tabOptions[i]['id']);
            $(idSelectBox).trigger('chosen:updated');
        }
        tabIdSelectOption = [];

    }
    else if (idTableSection)
    {
        if( document.phpDatatablesTableIdToCheckboxesSetup )
        {
            document.phpDatatablesTableIdToCheckboxesSetup[window.statsSummaryTableId] = 0;
        }

        $('#'+idTableSection).dataTable().api().ajax.reload();

    }
    else if (sectionURL)
    {
        location.href = sectionURL;
    }
}

/**
 * test is id already exist on the idselection tab
 * @param id
 * @returns {boolean}
 */
function bExist(id) {

    if (tabIdSelectOption.length > 0)
    {
        for (var i= 0; i < tabIdSelectOption.length; i++)
        {
            if (tabIdSelectOption[i]['id'] == id)
            {
                return true;
            }
        }
    }

    return false;
}

/**
 * add a new traffic sources
 *
 */
function getPopupTrafficSources(idTrafficSource)
{
    var section;
    var bManualCloseOnOk = true;


    section = Sections.TRAFFIC_SOURCE_EDIT;

    var urlParams = {};
    if (idTrafficSource)
    {
        urlParams[URLParams.TRAFFIC_SOURCE_ID] = idTrafficSource;
        urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;
    }
    else
    {
        urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;
    }
    var settings = {};
    var name;
    var id;
    var hasToRefresh = false;

    if (typeof mySlickGrid == 'undefined') {
        mySlickGrid = null;
    }


    modalShowSection(section, {
        button1Text: "SAVE TRAFFIC SOURCE",
        button2Text: "SAVE & CREATE NEW",
        modalTitleText: (idTrafficSource) ? "EDIT TRAFFIC SOURCE" : "ADD NEW TRAFFIC SOURCE",
        bManualCloseOnOk: bManualCloseOnOk,
        urlParams: urlParams,

        onOk: function (contentWindow) {
            contentWindow.saveTrafficSource(contentWindow,
            false,
                function(idTrafficSource) {
                    settings = contentWindow.getIdAndNameTrafficSource();
                    if (!bExist(idTrafficSource))
                    {
                        var temp = [];
                        temp['name'] = settings.name;
                        temp['id'] = idTrafficSource;
                        tabIdSelectOption.push(temp);


                        hasToRefresh = true;
                    }

                },mySlickGrid);
        },
        onOk2: function(contentWindow)
        {
            contentWindow.saveTrafficSource(contentWindow,
                true,
                function(idTrafficSource) {
                    settings = contentWindow.getIdAndNameTrafficSource();
                    if (!bExist(idTrafficSource))
                    {
                        var temp = [];
                        temp['name'] = settings.name;
                        temp['id'] = idTrafficSource;
                        tabIdSelectOption.push(temp);

                        hasToRefresh = true;

                    }
                    contentWindow.createNewIDtraffic();

                },mySlickGrid);
        },
        onClose: function() {
            var params = {};
            params.selectElement = '#select-traffic-source';
            params.tabIdSelection = tabIdSelectOption;
            if( window.statsSummaryTableId )
            {
                params.idTable = window.statsSummaryTableId;
            }
            if (hasToRefresh)
            {
                refreshParentPage(params);
                if ($('#select-campaign option:selected').length)
                {
                    refreshFunnelLinkInputs();
                }
            }



        },
        beforeClose : function (contentWindow) {

            var bModified = contentWindow.bCheckingIsContentModified();
            if (bModified)
            {
                return confirm(popupDirtyCloseConfirmMessage);
            }
            return true;
        }
    });
}

/**
 * add a new offer sources
 *
 */
function getPopupOfferSources(idOfferSources, options)
{
    var defaults = {
        'canCreateNew': true,
        'autoClose': false,
        'onSaved': null
    };
    
    options = $.extend({}, defaults, options ? options : {});

    var section;
    var bManualCloseOnOk = true;

    section = Sections.OFFER_SOURCE_EDIT;

    var urlParams = {};
    if (idOfferSources)
    {
        urlParams[URLParams.AFFILIATE_NETWORK_ID] = idOfferSources;
        urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;
    }
    else
    {
        urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;
    }

    if (typeof mySlickGrid == 'undefined') {
        mySlickGrid = null;
    }


    modalShowSection(section, {
        button1Text: "SAVE OFFER SOURCE",
        button2Text: options.canCreateNew ? "SAVE & CREATE NEW" : null,
        modalTitleText: (idOfferSources) ? "EDIT OFFER SOURCE" : "ADD NEW OFFER SOURCE",
        bManualCloseOnOk: bManualCloseOnOk,
        urlParams: urlParams,

        onOk: function (contentWindow) {
            contentWindow.saveOfferSource(contentWindow,
                false,
                function(idOfferSource) {
                    settings = contentWindow.getIdAndNameOfferSource();
                    if (!bExist(idOfferSource))
                    {
                        var temp = [];
                        temp['name'] = settings.name;
                        temp['id'] = idOfferSource;
                        tabIdSelectOption.push(temp);

                        hasToRefresh = true;
                        
                        if( options.autoClose )
                        {
                            modalShowSectionClose('B');
                        }
                    }

                }, mySlickGrid);
        },
        onOk2: function(contentWindow)
        {
            if( options.canCreateNew )
            {
                contentWindow.saveOfferSource(contentWindow,
                    true,function () {
                        contentWindow.createIdOfferSources();
                        hasToRefresh = true;
                    },mySlickGrid);
            }
            else
            {
                tabIdSelectOption = [];
                modalShowSectionClose('B');
            }
        },
        onClose: function()
        {
            if( options.onSaved && tabIdSelectOption.length )
            {
                options.onSaved( tabIdSelectOption[0]['id'], tabIdSelectOption[0]['name'] );
            }

            if( window.currentSection == Sections.OFFER_SOURCES_LIST && hasToRefresh )
            {
                var params = {
                    'sectionURL': getSectionURL(Sections.OFFER_SOURCES_LIST)
                };

                refreshParentPage(params);
            }
        },
        beforeClose : function (contentWindow) {

            var bModified = contentWindow.bCheckingIsContentModified();
            if (bModified)
            {
                return confirm(popupDirtyCloseConfirmMessage);
            }
            return true;
        }
    });
}

/**
 * add a new flux offer
 *
 */
function getPopupPage(pageType,idPage)
{
    var section;
    var button1Text;
    var button2Text;
    var bManualCloseOnOk = true;


    if (pageType == Sections.OFFER_EDIT)
    {
        section = Sections.OFFER_EDIT;
        button1Text = "SAVE FLUX OFFER";
        button2Text = "SAVE & CREATE NEW";
        modalTitleText = (idPage) ? "EDIT FLUX OFFER" : "ADD FLUX OFFER";
    }

    if (pageType == Sections.LANDER_EDIT)
    {
        button1Text = "SAVE FLUX LANDER";
        button2Text = "SAVE & CREATE NEW";
        modalTitleText = (idPage) ? "EDIT FLUX LANDER" : "ADD FLUX LANDER";
        section = Sections.LANDER_EDIT;
    }


    var urlParams = {};
    if (idPage)
    {
        urlParams[URLParams.PAGE_ID] = idPage;
        urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;
    }
    else
    {
        urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;
    }

    var hasToRefresh = false;

    if (typeof mySlickGrid == 'undefined') {
        mySlickGrid = null;
    }

    modalShowSection(section, {
        button1Text: button1Text,
        button2Text: button2Text,
        modalTitleText: modalTitleText,
        bManualCloseOnOk: bManualCloseOnOk,
        urlParams: urlParams,

        onOk: function (contentWindow) {
            contentWindow.savePage(contentWindow,
                false,function ()
                {
                    hasToRefresh = true;
                },mySlickGrid !== undefined ? mySlickGrid : null);
            hasToRefresh = true;
        },
        onOk2: function(contentWindow)
        {
            contentWindow.savePage(contentWindow,
                true,function ()
                {
                    hasToRefresh = true;
                    if (pageType == Sections.OFFER_EDIT)
                    {
                        contentWindow.createNewIdFluxOffers();
                    }
                    else if (pageType == Sections.LANDER_EDIT)
                    {
                        contentWindow.createNewIdFluxLanders();
                    }
                },mySlickGrid);


        },
        onClose: function() {

            if (hasToRefresh)
            {
                var params = {};
                if (pageType == Sections.OFFER_EDIT)
                {
                    if( window.statsSummaryTableId )
                    {
                        params.idTable = window.statsSummaryTableId;
                    }
                }

                if (pageType == Sections.LANDER_EDIT)
                {
                    if( window.statsSummaryTableId )
                    {
                        params.idTable = window.statsSummaryTableId;
                    }
                }
                refreshParentPage(params);
            }
        },
        beforeClose : function (contentWindow)
        {
            var bModified = contentWindow.bCheckingIsContentModified();
            if (bModified)
            {
                return confirm(popupDirtyCloseConfirmMessage);
            }
            return true;
        }
    });
}

/**
 * open campaign view
 *
 */
function getPopupCampaign(idCampaign)
{
    var section;
    var bManualCloseOnOk = true;

    section = Sections.CAMPAIGNS_EDIT;

    var urlParams = {};
    urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;
    if (idCampaign)
    {
        urlParams[URLParams.CAMPAIGN_ID] = idCampaign;
    }

    var settings = {};
    var name;
    var id;
    var hasToRefresh = false;

    if (typeof mySlickGrid == 'undefined')
    {
        mySlickGrid = null;
    }

    modalShowSection(section, {
        button1Text: "SAVE CAMPAIGN",
        button2Text: "SAVE & CREATE NEW",
        modalTitleText: (idCampaign) ? "EDIT CAMPAIGN" : "ADD NEW CAMPAIGN",
        bManualCloseOnOk: bManualCloseOnOk,
        urlParams: urlParams,

        onOk: function (contentWindow)
        {
            contentWindow.saveCampaign(contentWindow,
                false,mySlickGrid);
        },
        onOk2: function(contentWindow)
        {
            contentWindow.saveCampaign(contentWindow,
                true,mySlickGrid);
        },
        onClose: function()
        {
            var params = {};
            params.selectElement = '#select-traffic-source';
            params.tabIdSelection = tabIdSelectOption;
            if( window.statsSummaryTableId )
            {
                params.idTable = window.statsSummaryTableId;
            }
            if (hasToRefresh)
            {
                refreshParentPage(params);
                if ($('#select-campaign option:selected').length)
                {
                    refreshFunnelLinkInputs();
                }
            }
        },
        beforeClose : function (contentWindow)
        {
            var bModified = contentWindow.bCheckingIsContentModified();
            if (bModified)
            {
                return confirm(popupDirtyCloseConfirmMessage);
            }
            return true;
        }
    });
}
function getPopupStoredLink(idStoredLink)
{
    var section = Sections.STORED_LINKS_EDIT;
    var bManualCloseOnOk = true;

    var urlParams = {};
    if (idStoredLink)
    {
        urlParams[URLParams.STORED_LINK_ID] = idStoredLink;
        urlParams[URLParams.PAGE_HIDE_BUTTONS] = 0;
    }
    else
    {
        urlParams[URLParams.PAGE_HIDE_BUTTONS] = 0;
    }

    if (typeof  mySlickGrid == 'undefined')
    {
        mySlickGrid = null;
    }

    modalShowSection(section, {
        button1Text: "SAVE STORED LINK",
        modalTitleText: (idStoredLink) ? "EDIT STORED LINK" : "Stored Link <span style='font-size: 11px;'> Input a custom URL or follow the steps below to get the tracking url of one of your funnels</span>",
        bManualCloseOnOk: bManualCloseOnOk,
        urlParams: urlParams,

        onOk: function (contentWindow) {
            contentWindow.saveLink(idStoredLink, function(){
                storedLinkRefreshList(idStoredLink, contentWindow);
                storedLinkClosePopup();
            });
        },
        onOk2: function(contentWindow) {
            storedLinkRefreshList(idStoredLink, contentWindow);
            modalShowSectionClose('B');
        },
        onClose: function()
        {
        },
        beforeClose : function (contentWindow) {
            storedLinkRefreshList(idStoredLink, contentWindow);
            return true;
        }
    })
}

function storedLinkClosePopup()
{
    setTimeout(function(){
        modalShowSectionClose('B');
    }, 10);
}

function storedLinkRefreshList(idStoredLink, contentWindow)
{
    var bModified = contentWindow.bCheckingIsContentModified();
    if (bModified)
    {
        if (idStoredLink)
        {
            updateOneRow(idStoredLink, contentWindow.newData);
        }
        else
        {
            addRow(contentWindow.storedLink, contentWindow.storedLinkID);
        }

        //return confirm(popupDirtyCloseConfirmMessage);
    }
}