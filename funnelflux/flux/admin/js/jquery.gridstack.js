/**
 * Created by hp on 03/12/2015.
 */


var GRID_WIDTH = 12;
var GRID_CELL_RATIO = 60 / 111;

var section;
var optionsGrid;
var myGrid;
var tableNameDom;
var Controller;
var content;
var items;

var idDimension;
var objectCookiesWidgets;
var tabContainerWidget = [];

var arrayRowCampaigns = [];
var arrayRowLanders = [];
var arrayRowOffers = [];
var arrayRowTraficSources = [];





(function ($) {

    var gridStack = function (optionsOrMethod) {

        return initGrid(optionsOrMethod, function (options,idDimension)
        {
            var mapColumnNameToIndex = getMapColumnsNamesByIdDimension(idDimension);
            tabOrderElement[idDimension] = {
                columnOrders: mapColumnNameToIndex["Visits"],
                way: -1
            };
        });
    };

    $.fn.gridStack = function (optionsOrMethod) {
        $.fn.gridStack.defaults = {
            "ajaxSource": "",
            "tableNameDom": "#dashboard-gridstack",
            "savePosition": false,
            "urlAdmin":"",
            callback: null,
            "bAddSource":false,
            "bUpdate": false
        };

        return new gridStack(optionsOrMethod);
    };

/////////////////////// GRIDSTACK /////////////////////////////

    function initGrid(options, onSuccessCallback) {

        optionsGrid = $.extend({}, $.fn.gridStack.defaults, options);

        var options = {
            float: false,
            animate: true
        };
        $('.grid-stack').gridstack(options);

        myGrid = $('.grid-stack').data('gridstack');


        this.add_new_widget = function () {
            addNewWidget();
        }.bind(this);

        this.save_grid = function () {
            saveOrderWidgets();
        }.bind(this);

        this.load_grid = function () {
            loadOrderWidgets();
        }.bind(this);

        $('#add-new-widget').click(this.add_new_widget);

        $('.grid-stack').on('dragstop', function (event, ui) {
            //var idDimension = parseInt($(event.target).attr('data-gs-iddimension'));
            saveOrderWidgets();

        });
        $('.grid-stack').on('dragstart', function (event, ui) {
        });
        $('.grid-stack').on('resizestop', function (event, ui) {
            saveOrderWidgets();
        });



        //if (optionsGrid.bAddSource)
        //{
        //    addWidgetsOnDashboard(optionsGrid,onSuccessCallback);
        //}


        //if( onSuccessCallback )
        //{
        //    onSuccessCallback(optionsGrid);
        //}

        return {
            myGrid: myGrid,
            addNewWidget: addNewWidget,
            resizableWidget: resizableWidget,
            removeWidget: removeWidget,
            updateWidget: updateWidget,
            resize: resize,
            getWidgetInfo: getWidgetInfo,
            loadOrderWidgets : loadOrderWidgets,
            buildURlStatsForAjax : buildURlStatsForAjax,
            getWidgetInternalHTML : getWidgetInternalHTML,
            saveOrderWidgets : saveOrderWidgets,
            getDimensionWidgetByIdDimension : getDimensionWidgetByIdDimension,
            checkIfIDDimensionExistonCookiesObjectList : checkIfIDDimensionExistonCookiesObjectList
        };
    }

    function resize()
    {
        var w = myGrid.cell_width();
        
        if( !isNaN(w) )
        {
            myGrid.cell_height( w * GRID_CELL_RATIO );
        }
    }

    function saveOrderWidgets()
    {
        var nodes = _.map($('.grid-stack > .grid-stack-item:visible'), function (el)
        {
            el = $(el);
            var node = el.data('_gridstack_node');
            if( node )
            {
                return {
                    x: node.x,
                    y: node.y,
                    width: node.width,
                    height: node.height,
                    idDimension: node.idDimension
                };
            }

        }, this);
        createCookie( PersistentParams.KEY_ADMIN_GRIDSTACK_WIDGET_ORDER, JSON.stringify(nodes), null );
    }

    function loadOrderWidgets()
    {
        if (getCookie(PersistentParams.KEY_ADMIN_GRIDSTACK_WIDGET_ORDER))
        {
             return JSON.parse(getCookie(PersistentParams.KEY_ADMIN_GRIDSTACK_WIDGET_ORDER));
        }
    }
    
    function getWidgetInfo(dimensionId)
    {
        var orderWidget = null;
        var orderWidgets = loadOrderWidgets();
        
        if (orderWidgets)
        {
            for (var b = 0; b<orderWidgets.length ; b++)
            {
                if (orderWidgets[b]['idDimension'])
                {
                    if (orderWidgets[b]['idDimension'] == dimensionId)
                    {
                        orderWidget = orderWidgets[b];
                        break;
                    }
                }

            }
        }

        return orderWidget;
    }


    
    function getWidgetInternalHTML(content, idDimension, style)
    {
        if (style)
        {
            style = 'style="border-radius: 4px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 12px 0 rgba(0, 0, 0, 0.01);' + style + '"';
        }
        else
        {
            style = 'style="border-radius: 4px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 12px 0 rgba(0, 0, 0, 0.01);"';
        }

        return '<div class="grid-stack-item-content ui-draggable-handle dim-' + idDimension +'" ' + style + '  ><span class="widget-menu-item-container">' + getMenuWidget(idDimension) + '<span class="widget-menu-item"><img id="img-movable" src="images/icons/movable.png"></span></span>' + content + '</div>';
    }
    
    function getWidgetHTML(content, style, idDimension)
    {
        var internalHtml = getWidgetInternalHTML(content, idDimension, style);
        return '<div id="' + idDimension + '" class="widget-content">'+internalHtml+'</div>';
    }

    function getMenuWidget(idDimension)
    {
        var html = '<span id="menuIdimension' + idDimension + '" class="widget-menu-item menuWidget-' + idDimension + ' iw-mTrigger"><img id="burger-img-' + idDimension + '" src="images/icons/burger.png" ></span>';
        return html;
    }

    function addNewWidget(content,idDimension,options)
    {
        var orderWidgets = loadOrderWidgets();
        var node;
        var results = false;

        if (options === undefined)
        {
            var options = {
                style:'',
            autoPosition:'',
            bDoublon:true};
        }
        else
        {
            if(options.style === undefined)
            {
                options.style = '';
            }

            if (options.autoPosition === undefined)
            {
                options.autoPosition = true;
            }

            if (options.bDoublon === undefined)
            {
                options.bDoublon = true;
            }
        }



        if (orderWidgets)
        {
            for (var b = 0; b<orderWidgets.length ; b++)
            {
                if (orderWidgets[b])
                {
                    if (orderWidgets[b]['idDimension'] == idDimension)
                    {
                        node = orderWidgets[b];
                        results = true;
                        options.autoPosition = false;
                        break;
                    }
                }
            }
        }


        if (!results)
        {
            var positionXY = getDefaultXAndY(idDimension);
            if (!options.x)
            {
                options.x = positionXY.x;
            }
            if (!options.y)
            {
                options.y = positionXY.y;
            }
            if (!options.w)
            {
                options.w = positionXY.width;
            }
            if (!options.h)
            {
                options.h = positionXY.height;
            }
            node =  {
                x: options.x,
                y: options.y,
                width:options.w,
                height: options.h,
                idDimension:idDimension
            };
            options.autoPosition = false;
        }

        content = getWidgetHTML(content, options.style,idDimension);
        if (options.bDoublon)
        {
            window.tabContainerWidget[idDimension] = myGrid.add_widget($(content),
                node.x, node.y, node.width, node.height,options.autoPosition,node.idDimension);

            initializeMenu(idDimension);

            setEffectWidget();
        }
        else
        {
            if ( window.tabContainerWidget[idDimension] === undefined)
            {
                window.tabContainerWidget[idDimension] = myGrid.add_widget($(content),
                    node.x, node.y, node.width, node.height,options.autoPosition,node.idDimension);

                initializeMenu(idDimension);

                setEffectWidget();
            }
        }

        return window.tabContainerWidget[idDimension];
    }

    function getDimensionWidgetByIdDimension(idDimension)
    {
        var orderWidgets = loadOrderWidgets();
        var node;
        var results = false;

        if (orderWidgets)
        {
            for (var i = 0; i<orderWidgets.length ; i++)
            {
                if (orderWidgets[i])
                {
                    if (orderWidgets[i]['idDimension'] == idDimension)
                    {
                        node = orderWidgets[i];
                        results = true;
                        break;
                    }
                }
            }
        }


        if (!results)
        {
            node = getDefaultXAndY(idDimension);
        }

        return node;
    }

    function checkIfIDDimensionExistonCookiesObjectList(idDimension)
    {
        var orderWidgets = loadOrderWidgets();
        var node;
        var results = false;

        if (orderWidgets)
        {
            for (var i = 0; i<orderWidgets.length ; i++)
            {
                if (orderWidgets[i])
                {
                    if (orderWidgets[i]['idDimension'] == idDimension)
                    {
                        node = orderWidgets[i];
                        results = true;
                        break;
                    }
                }
            }
        }

        return results;
    }

    function buildURlStatsForAjax(url, idDimension, periodName, dateRangeStart, dateRangeEnd, timezone)
    {
        var resultsUrl = url;

        resultsUrl += "?"+ URLParams.STATS_DIMENSION_ID +"=" + idDimension;

        if (periodName)
        {
            resultsUrl += "&" +  URLParams.STATS_DATERANGE_NAME + "=" + periodName;
        }
        else
        {
            if (dateRangeStart)
            {
                resultsUrl += "&" +  URLParams.STATS_DATERANGE_START + "=" + dateRangeStart;
            }
            if (dateRangeEnd)
            {
                resultsUrl += "&" +  URLParams.STATS_DATERANGE_END + "=" + dateRangeEnd;
            }
        }
        if (timezone)
        {
            resultsUrl += "&" +  URLParams.STATS_TIMEZONE + "=" + timezone;
        }
        
        return resultsUrl;
    }


    function resizableWidget(widget,val)
    {
        myGrid.resizable(widget,val);
    }



    function updateWidget(widget,content,idDimension,style)
    {
        removeWidget(widget,idDimension);
        var monWidgets;
        if (style)
        {
            monWidgets = addNewWidget(content,idDimension,{style:style});
        }
        else
        {
            monWidgets = addNewWidget(content,idDimension);
        }

        return monWidgets;
    }

    function initializeMenu(idDimension) {

        var action = [{
            name: "<img src='images/icons/bin.png' style='padding-right:9px;padding-bottom: 3px;'>Remove widget",
            classNames: 'optionDelete-widget',
            onClick: function () {
                window.bBuildMEnuAddWidget = true;
                removeWidget(tabContainerWidget[idDimension],idDimension);

            }
        }];

        addSpecialMenu(idDimension,action);

        var menu = new BootstrapMenu('.menuWidget-' + idDimension, {
            menuEvent: 'click', // default value, can be omitted
            menuSource: 'element',
            menuPosition: 'belowLeft', // default value, can be omitted
            actions: action
        });



        $( ".iw-contextMenu" ).addClass( "customize-menu-context" );
        $( ".iw-mIcon" ).addClass( "customize-image-context" );


    }

    function addSpecialMenu(idDimension,action)
    {
        switch (idDimension)
        {
            case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
                addMenuForCampaignDashBoard(action);
                break;
        }
    }

    function addMenuForCampaignDashBoard(action)
    {
        var expandAllRowCampaign = {
            name: "<img src='images/icons/expand-all.png?v=4' style='padding-right:9px;padding-bottom: 3px;'>Expand All",
            classNames: 'optionDelete-widget',
            onClick: function() {
                window.bBuildMEnuAddWidget = true;

                var gridCampaign = listGrids[StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS];
                if (!expandAllRow)
                {
                    mySlickGrid.getGridForMultipleInstance('',gridCampaign);
                    //put loading image and deactivate menu
                    $('#burger-img-' + StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS).attr('src','images/ajax-loader-small-rect-blue.gif');
                    $('#menuIdimension' + StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS ).removeClass('menuWidget-' + StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS);
                    autoExpandCampaign = true;
                    getDataFromAjaxAndBuildWidgetSlick( {bNewAddViaMenu : false, bUpdateRow:true}, StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS,
                    function()
                    {
                        $('#burger-img-' + StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS).attr('src','images/icons/burger.png');
                        $('#menuIdimension' + StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS).addClass('menuWidget-' + StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS);
                    });
                }
                else
                {
                    mySlickGrid.getGridForMultipleInstance('',gridCampaign);
                    dataView.beginUpdate();
                    dataView.setItems([]);
                    dataView.endUpdate();
                    for(var i = 0; i < data.length ; i++)
                    {
                        data[i][COLUMNS_COLLAPSE] = false;
                    }
                    dataView.beginUpdate();
                    dataView.setItems(data);
                    dataView.endUpdate();
                }
            }
        };

        action.push(expandAllRowCampaign);

        var collapseAllRow = {
            name: "<img src='images/icons/collapse-all.png?v=4' style='padding-right:9px;padding-bottom: 3px;'>Collapse All",
            classNames: 'optionDelete-widget',
            onClick: function() {
                if (mySlickGrid)
                {
                    myGrid = listGrids[StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS];
                    mySlickGrid.getGridForMultipleInstance('',myGrid);
                    dataView.beginUpdate();
                    dataView.setItems([]);
                    dataView.endUpdate();
                    for(var i = 0; i < data.length ; i++)
                    {
                        data[i][COLUMNS_COLLAPSE] = true;
                    }
                    dataView.beginUpdate();
                    dataView.setItems(data);
                    dataView.endUpdate();
                }
                else
                {
                    console.log("mySlcikgrid undefined");
                }
            }
        };

        action.push(collapseAllRow);
    }

    function removeWidget(widget,idDimension)
    {
        delete tabContainerWidget[idDimension];
        myGrid.remove_widget(widget);
        saveOrderWidgets();


    }

    function setEffectWidget()
    {
        $( ".widget-menu-item" ).css("visibility", "hidden");
        
        $( ".grid-stack-item-content" ).hover(
            function()
            {
                $( this ).find( ".widget-menu-item" ).css("visibility", "visible");
            }, 
            function() 
            {
                $( this ).find( ".widget-menu-item" ).css("visibility", "hidden");
            });
    }

})(jQuery);


