var LOADABLE_REPORTS_DIV_ID = 'reports-loader';
var LOADABLE_REPORTS_TABLE_ANCHOR_NAME = 'section-loadable-reports';

/**
 * Check if new loadable reports have been computed and display them in a div with id = "reports-loader"
 * 
 * @returns {undefined}     
 */
function setupBackgroundReportsMonitor()
{
//    var aResponse = unserialize('a:2:{s:7:"success";i:1;s:8:"aReports";s:7451:"[{"report":{"id":"f47e87ecda79191ba2ca403e839eb500","title":"Drilldown Report 2016-01-14 09:27:37","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59nTimezone: Asia/DubainFiltered Traffic: HiddennGroupings: Connection: ISPn","aApiParams":{"dtRangeName":"2015-01-01+00%3A00+to+2015-12-31+23%3A59","timezone":"Asia%2FDubai","showEmpty":"1","showFiltered":"0","looseMode":"0","sgb1":"Connection%3A+ISP","sgid1":""},"timestampStarted":1452763700,"timestampCompleted":1452763772},"info":{"apiQuery":"dtRangeName=2015-01-01+00%3A00+to+2015-12-31+23%3A59&timezone=Asia%2FDubai&showEmpty=1&showFiltered=0&looseMode=0&sgb1=Connection%3A+ISP&sgid1=","groupingNames":"["Connection: ISP"]","groupingIds":"[""]","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59<br />Timezone: Asia/Dubai<br />Filtered Traffic: Hidden<br />Groupings: Connection: ISP<br />"}},{"report":{"id":"ddd59428fafdf2247dc1803e165b0c5b","title":"Drilldown Report 2016-01-14 07:58:26","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59nTimezone: Asia/DubainFiltered Traffic: DisplayednGroupings: Element: Campaign / Device: Brand (Google) / Element: Flux Landern","aApiParams":{"dtRangeName":"2015-01-01+00%3A00+to+2015-12-31+23%3A59","timezone":"Asia%2FDubai","showEmpty":"1","showFiltered":"1","looseMode":"1","sgb1":"Element%3A+Campaign","sgid1":"","sgb2":"Device%3A+Brand","sgid2":"Google","sgb3":"Element%3A+Flux+Lander","sgid3":""},"timestampStarted":1452758320,"timestampCompleted":1452758352},"info":{"apiQuery":"dtRangeName=2015-01-01+00%3A00+to+2015-12-31+23%3A59&timezone=Asia%2FDubai&showEmpty=1&showFiltered=1&looseMode=1&sgb1=Element%3A+Campaign&sgid1=&sgb2=Device%3A+Brand&sgid2=Google&sgb3=Element%3A+Flux+Lander&sgid3=","groupingNames":"["Element: Campaign","Device: Brand","Element: Flux Lander"]","groupingIds":"["","Google",""]","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59<br />Timezone: Asia/Dubai<br />Filtered Traffic: Displayed<br />Groupings: Element: Campaign / Device: Brand (Google) / Element: Flux Lander<br />"}},{"report":{"id":"c7763e1f57b4889f5b0e4cf3cf9c5b9c","title":"Drilldown Report 2016-01-14 08:28:46","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59nTimezone: Asia/DubainFiltered Traffic: DisplayednGroupings: Element: Campaign / Connection: ISPn","aApiParams":{"dtRangeName":"2015-01-01+00%3A00+to+2015-12-31+23%3A59","timezone":"Asia%2FDubai","showEmpty":"1","showFiltered":"1","looseMode":"0","sgb1":"Element%3A+Campaign","sgid1":"","sgb2":"Connection%3A+ISP","sgid2":""},"timestampStarted":1452760133,"timestampCompleted":1452760259},"info":{"apiQuery":"dtRangeName=2015-01-01+00%3A00+to+2015-12-31+23%3A59&timezone=Asia%2FDubai&showEmpty=1&showFiltered=1&looseMode=0&sgb1=Element%3A+Campaign&sgid1=&sgb2=Connection%3A+ISP&sgid2=","groupingNames":"["Element: Campaign","Connection: ISP"]","groupingIds":"["",""]","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59<br />Timezone: Asia/Dubai<br />Filtered Traffic: Displayed<br />Groupings: Element: Campaign / Connection: ISP<br />"}},{"report":{"id":"b0f81a6ea3619ebdefc5d94fb2737b7c","title":"Drilldown    Report 2016-01-14 08:14:19","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59nTimezone: Asia/DubainFiltered Traffic: DisplayednGroupings: Element: Campaign / Device: Brand / Element: Flux Landern","aApiParams":{"dtRangeName":"2015-01-01+00%3A00+to+2015-12-31+23%3A59","timezone":"Asia%2FDubai","showEmpty":"1","showFiltered":"1","looseMode":"1","sgb1":"Element%3A+Campaign","sgid1":"","sgb2":"Device%3A+Brand","sgid2":"","sgb3":"Element%3A+Flux+Lander","sgid3":""},"timestampStarted":1452759963,"timestampCompleted":1452760004},"info":{"apiQuery":"dtRangeName=2015-01-01+00%3A00+to+2015-12-31+23%3A59&timezone=Asia%2FDubai&showEmpty=1&showFiltered=1&looseMode=1&sgb1=Element%3A+Campaign&sgid1=&sgb2=Device%3A+Brand&sgid2=&sgb3=Element%3A+Flux+Lander&sgid3=","groupingNames":"["Element: Campaign","Device: Brand","Element: Flux Lander"]","groupingIds":"["","",""]","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59<br />Timezone: Asia/Dubai<br />Filtered Traffic: Displayed<br />Groupings: Element: Campaign / Device: Brand / Element: Flux Lander<br />"}},{"report":{"id":"768dab256d31c8292db41a5fedec7147","title":"Drilldown Report 2016-01-12 02:40:31","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59nTimezone: Asia/DubainFiltered Traffic: HiddennGroupings: Element: Campaign / Connection: ISPn","aApiParams":{"dtRangeName":"2015-01-01+00%3A00+to+2015-12-31+23%3A59","timezone":"Asia%2FDubai","showEmpty":"1","showFiltered":"0","looseMode":"0","orderBy":"23","orderDir":"desc","sgb1":"Element%3A+Campaign","sgid1":"","sgb2":"Connection%3A+ISP","sgid2":""},"timestampStarted":1452611282,"timestampCompleted":1452611377},"info":{"apiQuery":"dtRangeName=2015-01-01+00%3A00+to+2015-12-31+23%3A59&timezone=Asia%2FDubai&showEmpty=1&showFiltered=0&looseMode=0&orderBy=23&orderDir=desc&sgb1=Element%3A+Campaign&sgid1=&sgb2=Connection%3A+ISP&sgid2=","groupingNames":"["Element: Campaign","Connection: ISP"]","groupingIds":"["",""]","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59<br />Timezone: Asia/Dubai<br />Filtered Traffic: Hidden<br />Groupings: Element: Campaign / Connection: ISP<br />"}},{"report":{"id":"6f31057104503b99d285574a56f20032","title":"Drilldown Report 2016-01-14 08:28:56","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59nTimezone: Asia/DubainFiltered Traffic: HiddennGroupings: Element: Campaign / Connection: ISPn","aApiParams":{"dtRangeName":"2015-01-01+00%3A00+to+2015-12-31+23%3A59","timezone":"Asia%2FDubai","showEmpty":"1","showFiltered":"0","looseMode":"0","sgb1":"Element%3A+Campaign","sgid1":"","sgb2":"Connection%3A+ISP","sgid2":""},"timestampStarted":1452760144,"timestampCompleted":1452760269},"info":{"apiQuery":"dtRangeName=2015-01-01+00%3A00+to+2015-12-31+23%3A59&timezone=Asia%2FDubai&showEmpty=1&showFiltered=0&looseMode=0&sgb1=Element%3A+Campaign&sgid1=&sgb2=Connection%3A+ISP&sgid2=","groupingNames":"["Element: Campaign","Connection: ISP"]","groupingIds":"["",""]","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59<br />Timezone: Asia/Dubai<br />Filtered Traffic: Hidden<br />Groupings: Element: Campaign / Connection: ISP<br />"}},{"report":{"id":"5d730f3b234bc7852127fed4cecb7f4f","title":"Drilldown Report 2016-01-03 17:56:09","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59nTimezone: Asia/DubainFiltered Traffic: HiddennGroupings: Element: Campaign / Device: Brand / Element: Flux Landern","aApiParams":{"dtRangeName":"2015-01-01+00%3A00+to+2015-12-31+23%3A59","timezone":"Asia%2FDubai","showEmpty":"1","showFiltered":"1","looseMode":"1","orderBy":"8","orderDir":"asc","sgb1":"Element%3A+Campaign","sgid1":"","sgb2":"Device%3A+Brand","sgid2":"","sgb3":"Element%3A+Flux+Lander","sgid3":""},"timestampStarted":1452611278,"timestampCompleted":1452611337},"info":{"apiQuery":"dtRangeName=2015-01-01+00%3A00+to+2015-12-31+23%3A59&timezone=Asia%2FDubai&showEmpty=1&showFiltered=1&looseMode=1&orderBy=8&orderDir=asc&sgb1=Element%3A+Campaign&sgid1=&sgb2=Device%3A+Brand&sgid2=&sgb3=Element%3A+Flux+Lander&sgid3=","groupingNames":"["Element: Campaign","Device: Brand","Element: Flux Lander"]","groupingIds":"["","",""]","description":"Period: 2015-01-01 00:00 to 2015-12-31 23:59<br />Timezone: Asia/Dubai<br />Filtered Traffic: Hidden<br />Groupings: Element: Campaign / Device: Brand / Element: Flux Lander<br />"}}]";}');
//    window.aLoadableReports = aResponse['aReports'];
//    refreshLoadableReports(window.aLoadableReports);
//    return;

    ajaxRequest("getStatsReports", {}, function(response)
    {
        var aResponse = unserialize(response);
        if( aResponse['success'] )
        {
            window.aLoadableReports = aResponse['aReports'];
            refreshLoadableReports(window.aLoadableReports);

            var INTERVAL = 10000;
            setInterval(function()
                {
                    ajaxRequest("getStatsReports", {}, function(response)
                    {
                        var aResponse = unserialize(response);
                        if( aResponse['success'] )
                        {
                            if( aResponse['aReports'].length === window.aLoadableReports.length )
                            {
                                // Same number of reports.
                                // Is there at least one that just completed processing?
                                
                                for(var i=0; i<aResponse['aReports'].length; i++)
                                {
                                    var newReport = aResponse['aReports'][i];
                                    var oldReport = window.aLoadableReports[i];

                                    if( oldReport['info']['creationDate'] !== newReport['info']['creationDate'] )
                                    {
                                        window.loadableReportsForceRefresh = true;
                                        
                                        if( newReport['info']['creationDate'] !== null )
                                        {
                                            notifyOfNewLoadableReports();
                                        }
                                        
                                        break;
                                    }
                                }
                            }

                            if( window.loadableReportsForceRefresh || aResponse['aReports'].length !== window.aLoadableReports.length )
                            {
                                // New report(s) available...
                                refreshLoadableReports(aResponse['aReports'], window.aLoadableReports);
                                window.aLoadableReports = aResponse['aReports'];

                                window.loadableReportsForceRefresh = false;
                            }
                        }
                    });

                }, INTERVAL);
        }
    });
}

/**
 * 
 * @param {type} reportId
 * @returns {undefined}
 */
function setLoadableReportAsViewed(reportId)
{
    window.loadableReportsForceRefresh = true;
    $('#view-selector').val('');
}

function loadableReportGetEntryById(reportId)
{
    var entry = null;
    
    if( window.aLoadableReports )
    {
        for(var i=0; i<window.aLoadableReports.length; i++)
        {
            if( window.aLoadableReports[i]['report']['id'] === reportId )
            {
                entry = window.aLoadableReports[i];
                break;
            }
        }
    }
    
    return entry;
}

/**
 * 
 * @returns {undefined}
 */
function highlightLoadableReports()
{
    $('#'+LOADABLE_REPORTS_DIV_ID).find('tr').effect('highlight', 2000);
}

/**
 * 
 * @returns {undefined}
 */
function notifyOfNewLoadableReports()
{
//    var reportsLink = '#' + LOADABLE_REPORTS_TABLE_ANCHOR_NAME;
//
//    if( window.currentSection !== Sections.STATS_DETAILED && window.currentSection !== Sections.STATS_DETAILED_SLICK )
//    {
//        reportsLink = getSectionURL(Sections.STATS_DETAILED_SLICK) + reportsLink;
//    }
//
//    modalShowMessage("New report(s) available", "You have new report(s) waiting for you.", "<a onclick='modalCloseMessage(); highlightLoadableReports();' href='"+reportsLink+"'>Click here to see them now</a>");
}

/**
 * 
 * @param {type} aReports
 * @param {type} aOldReports
 * @returns {undefined}     
 */
function refreshLoadableReports(aReports, aOldReports)
{    
    // Are there new reports?

    var aNewReportIds = [];

    if( aOldReports && aReports.length !== aOldReports.length )
    {
        for(var i=0; i<aReports.length; i++)
        {
            if( aReports[i]['info']['creationDate'] !== null )
            {
                var reportId = aReports[i]['report']['id'];
                var bNew = true;

                for(var j=0; j<aOldReports.length; j++)
                {
                    var oldReportId = aOldReports[j]['report']['id'];

                    if( reportId == oldReportId )
                    {
                        bNew = false;
                        break;
                    }
                }

                if( bNew )
                {
                    aNewReportIds.push(reportId);
                }
            }
        }
    }
    
    if( aNewReportIds.length > 0 )
    {
        notifyOfNewLoadableReports();
    }

    $reportsDiv = $('#'+LOADABLE_REPORTS_DIV_ID);
    if( $reportsDiv.length )
    {
        $reportsDiv.empty();

        var html = '';
        html += '<a name="'+LOADABLE_REPORTS_TABLE_ANCHOR_NAME+'"/>';
        html += '<table class="loadable-reports">';
        html += '<thead><tr>';
        html += '<td>Most Recent Reports</td>';
        html += '<td>Date Range</td>';
        html += '<td>Groupings</td>';
        html += '<td>Options</td>';
        html += '</tr></thead>';

        for(var i=0; i<aReports.length; i++)
        {
            var report = aReports[i]['report'];

            var reportId = report['id'];
            var bViewed = report['bViewed'];
            var groups = '';

            var loadableReportInfo = aReports[i]['info'];
            var creationDate = loadableReportInfo['creationDate'];

            var aGroupNames = [];

            for(var iGroup=0; iGroup < loadableReportInfo['groupingNames'].length; iGroup++)
            {
                var name = loadableReportInfo['groupingNames'][iGroup];
                var id = iGroup < loadableReportInfo['groupingIds'][iGroup].length
                            ? ' (' + loadableReportInfo['groupingIds'][iGroup] + ')'
                            : '';

                aGroupNames.push( name + id );
            }

            if( aGroupNames.length )
            {
                groups = implode(' / ', aGroupNames);
            }

            var period = cleanupPeriodString(loadableReportInfo['period']);
            var options = loadableReportInfo['options'];
            var linkHtml = '';
            
            if( creationDate === null )
            {
                linkHtml = '<img src="images/ajax-loader-small-rect-blue.gif"/><span style="margin-left: 10px;">Processing...</span>';
            }
            else
            {
                var reportTitle = creationDate;
                linkHtml = buildLoadableReportLink(reportId, reportTitle, loadableReportInfo['period'], loadableReportInfo['timezone'], bViewed);
            }

            var aClasses = [];
            if( !bViewed )
            {
                aClasses.push('not-viewed');
            }
            var classes = "class='" + implode(' ', aClasses) + "'";

            html += '<tr>';
            html += '<td '+classes+'>'+linkHtml+'</td>';
            html += '<td '+classes+'>'+period+'</td>';
            html += '<td '+classes+'>'+groups+'</td>';
            html += '<td '+classes+'>'+options+'</td>';
            html += '</tr>';
        }

        html += '</table>';

        var wrapper = "";
    //    wrapper += "<ul class='accordion' data-accordion>";
    //    wrapper += "    <li class='accordion-navigation'>";
    //    wrapper += "        <a href='#panel-advanced-settings'><span id='title-advanced-settings'>Existing Reports</a></a>";
    //    wrapper += "        <div id='panel-advanced-settings' class='content'>";
    //    wrapper += "            <div class='row'>";
    //    wrapper += "                <div class='large-12 columns'>";
        wrapper +=                        html;
    //    wrapper += "                </div>";
    //    wrapper += "            </div>";
    //    wrapper += "        </div>";
    //    wrapper += "    </li>";
    //    wrapper += "</ul>";

        $reportsDiv.append(wrapper);

        $(document).foundation('accordion', 'reflow');
    }
}

function clickedLoadableReport(reportId, reportTitle, period, timezone, bViewed)
{
    drilldownLoadReport(reportId); 
    setLoadableReportAsViewed(reportId);
    datePickerRange_setRange(period);
    datePickerRange_setTimezone(timezone);
}

/**
 * 
 * @param {type} report
 * @returns html for link
 */
function buildLoadableReportLink(reportId, reportTitle, period, timezone, bViewed)
{
    var classes = '';
    var allParams = '"'+reportId+'", "'+reportTitle+'", "'+period+'", "'+timezone+'", "'+bViewed+'"';

    var html = "<a title='Delete Report' onclick='deleteLoadableReport(\""+reportId+"\");'><i class='icon-trash'></i></a>";
    html += "&nbsp;&nbsp;";
    html += "<a title='Load Report' onclick='clickedLoadableReport("+allParams+");'><span "+classes+">"+reportTitle+"</span></a>";
    return html;
}

/**
 * 
 * @param {type} reportId
 * @returns {undefined}
 */
function deleteLoadableReport(reportId)
{
    window.reportIdToDelete = reportId;
    
    modalShowConfirmMessage({
        message: 'Are you sure you want to delete this report?',
        submessage: 'This action cannot be undone',
        okButtonText: 'Delete it!',
        noButtonText: "Wait, I've Changed My Mind!",
        callbackOnOK: function()
        {
            var options = {};
            options[URLParams.STATS_REPORT_ID] = window.reportIdToDelete;

            modalLongActionOpen("Deleting Report", "Please wait...");

            ajaxRequest('deleteStatsReport', options,
                function (msg)
                {
                    var response = unserialize(msg);

                    if( response['success'] )
                    {
                        for(var i=window.aLoadableReports.length-1; i >= 0 ; i-- )
                        {
                            if( window.aLoadableReports[i]['report']['id'] === window.reportIdToDelete )
                            {
                                // Remove element
                                window.aLoadableReports.splice(i, 1);
                                break;
                            }
                        }

                        refreshLoadableReports(window.aLoadableReports);
                        modalLongActionClose();
                    }
                    else if( response['errorMsg'] )
                    {
                        modalLongActionClose();
                        modalShowMessage("ERROR", "Cannot delete report", response['errorMsg']);
                    }
                }, 
                function()
                {
                    modalLongActionClose();
                });   
        }
    });
}