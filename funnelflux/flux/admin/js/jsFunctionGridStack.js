/**
 * Created by hp on 04/02/2016.
 */

var mapColumnNameToIndexCampaigns = [];
var mapColumnNameToIndexTrafficOffer = [];
var mapColumnNameToIndexLanders = [];
var mapColumnNameToIndexOffers = [];

var mapIndexToColumnNameCampaigns = [];
var mapIndexToColumnNameTrafficOffer = [];
var mapIndexToColumnNameLanders = [];
var mapIndexToColumnNameOffers = [];

var tabOrderElement = [];
var tabAllIdDimensionOnGrid = [];
var tabAllIdDimensionMetrics = [];


function buildHeaderTable(idDimension, content, columnName, bProcessUpdate)
{
    var viewsName;

    switch(idDimension)
    {
        case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
        case StatsAggregateSummary.DIMENSION_TYPE_FUNNELS:
        case StatsAggregateSummary.DIMENSION_TYPE_COUNTRIES:
        case StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES:
            viewsName = "Visits";
            break;

        default:
            viewsName = "Views";
            break;
    }

    //put the sort icon
    var iconsViews = '';
    var iconsConv = '';
    var iconsRevenue = '';

    var way = 0;
    if (columnName)
    {
        var mapColumnNameToIndex = getMapColumnsNamesByIdDimension(idDimension);
        switch (columnName)
        {
            case "Visits":
            case "Views":
                way = getTheWay(idDimension,mapColumnNameToIndex[columnName],bProcessUpdate);
                if (way == -1)
                {
                    iconsViews = '<span style="margin-left: 7px;" class="ion-ios-arrow-thin-up">';
                }
                else
                {
                    iconsViews = '<span style="margin-left: 7px;" class="ion-ios-arrow-thin-down">';
                }

                break;
            case "Conv.":
                way = getTheWay(idDimension,mapColumnNameToIndex[columnName],bProcessUpdate);
                if (way == -1)
                {
                    iconsConv = '<span style="margin-left: 7px;" class="ion-ios-arrow-thin-up">';
                }
                else
                {
                    iconsConv = '<span style="margin-left: 7px;" class="ion-ios-arrow-thin-down">';
                }

                break;
            case "Revenue":
                way = getTheWay(idDimension,mapColumnNameToIndex[columnName],bProcessUpdate);
                if (way == -1)
                {
                    iconsRevenue = '<span style="margin-left: 7px;" class="ion-ios-arrow-thin-up">';
                }
                else
                {
                    iconsRevenue = '<span style="margin-left: 7px;" class="ion-ios-arrow-thin-down">';
                }
                break;

        }
    }
    else
    {
        iconsViews = '<span style="margin-left: 7px;" class="ion-ios-arrow-thin-up">';
    }

    content += '<tr>';
    content += '<td class="widget-table-title" >' + getNameByIdDimension(idDimension) + '</td>';
    content += '<td class="widget-table-header" id="dim' + idDimension + viewsName + '" style="cursor: pointer">'+viewsName +iconsViews+'</span></td>';
    content += '<td class="widget-table-header" id="dim' + idDimension + 'Conv" style="cursor: pointer">Conv.'+iconsConv+'</td>';
    content += '<td class="widget-table-header" id="dim' + idDimension + 'Revenue" style="cursor: pointer">Revenue'+iconsRevenue+'</td>';
    content += '</tr>';




    return content;

}

function getNameByIdDimension(idDimension)
{
    switch (idDimension)
    {
        case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
            return 'CAMPAIGNS';
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_FUNNELS:
            return 'FUNNELS';
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_COUNTRIES:
            return 'COUNTRIES';
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_OFFERS:
            return 'OFFERS';
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_LANDERS:
            return 'LANDERS';
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES:
            return 'TRAFFIC SOURCES';
            break;
        case StatsAggregateSummary.DIMENSION_UPDATE_SYSTEM:
            return 'SYSTEM UPDATES';
            break;
        case StatsAggregateSummary.DIMENSION_LATEST_POSTS:
            return 'FORUM POSTS';
            break;
        case StatsAggregateSummary.DIMENSION_ACCESS_LOGS:
            return 'ACCESS LOG';
            break;
        case StatsAggregateSummary.DIMENSION_VIDEO_1:
            return 'VIDEO 1';
            break;
        case StatsAggregateSummary.DIMENSION_VIDEO_2:
            return 'VIDEO 2';
            break;
        case StatsAggregateSummary.DIMENSION_VIDEO_3:
            return 'VIDEO 3';
            break;
    }
}

function buildBodyTable(content, rows, idDimension)
{
    var MAX_ROWS = 10;
    var nbRows = Math.min(MAX_ROWS, rows.length);

    var mapColumnNameToIndex= getMapColumnsNamesByIdDimension(idDimension);

    for( var i = 0; i < nbRows ; i++ )
    {
        var row = rows[i];

        if( row[mapColumnNameToIndex["Status"]] === Strings.STR_ACTIVE )
        {
            content += '<tr class="widget-table-row">';
            content += '<td>' + row[mapColumnNameToIndex["Actions"]] + " " + row[mapColumnNameToIndex["Name"]] + '</td>';
            content += '<td>' + row[mapColumnNameToIndex["Visits"]] + '</td>';
            content += '<td>' + row[mapColumnNameToIndex["Conv."]] + '</td>';

            var revenue = row[mapColumnNameToIndex["Revenue"]];
            content += '<td class="widget-table-row">'+ revenue + '</span></td>';
            content += '</tr>';
        }
    }

    content += '</table>';
    return content;
}


function getNewContentSortArray(idDimension,columnName,bProcessUpdate)
{

    var way;
    switch (idDimension)
    {
        case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
            way = getTheWay(idDimension,columnName,bProcessUpdate);
            window.arrayRowCampaigns = window.arrayRowCampaigns.sort(dynamicSort(columnName,way));
            updatetabOrderElement(idDimension,columnName,way);
            return window.arrayRowCampaigns;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_OFFERS:
            way = getTheWay(idDimension,columnName,bProcessUpdate);
            window.arrayRowOffers = window.arrayRowOffers.sort(dynamicSort(columnName,way));
            updatetabOrderElement(idDimension,columnName,way);
            return window.arrayRowOffers;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_LANDERS:
            way = getTheWay(idDimension,columnName,bProcessUpdate);
            window.arrayRowLanders = window.arrayRowLanders.sort(dynamicSort(columnName,way));
            updatetabOrderElement(idDimension,columnName,way);
            return window.arrayRowLanders;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES:
            way = getTheWay(idDimension,columnName,bProcessUpdate);
            window.arrayRowTraficSources.sort(dynamicSort(columnName,way));
            updatetabOrderElement(idDimension,columnName,way);
            return window.arrayRowTraficSources;
            break;
    }
}

function dynamicSort(property,way)
{
    var sortOrder = way;
    return function (a,b) {

        var result = (formatInt(parseInt(a[property])) < formatInt(parseInt(b[property]))) ? -1 : (formatInt(parseInt(a[property])) > formatInt(parseInt(b[property]))) ? 1 : 0;
        return result * sortOrder;
    }
}

function updateWidgetWithNewTable(idDimension,columnName,bProcessUpdate)
{
    if (!bProcessUpdate)
    {
        bProcessUpdate = false;
    }
    var mapColumnNameToIndex = getMapColumnsNamesByIdDimension(idDimension);
    $('.dashboard-table-dim-' + idDimension).remove();
    var content = '<table class="dashboard-table dashboard-table-dim-' + idDimension + '">';
    content = buildHeaderTable(idDimension,content,columnName,bProcessUpdate);
    content += buildScriptEventForSort(idDimension);
    content = buildBodyTable(content, getNewContentSortArray(idDimension,mapColumnNameToIndex[columnName],bProcessUpdate),idDimension);


    $('.dim-' + idDimension).append(content);

}

function buildScriptEventForSort(idDimension)
{

    var content = '';
    content += '<script>';
    content += '$("#dim'+idDimension +'Conv").on("click", function (event,ui) {';
    content += '    updateWidgetWithNewTable('+idDimension+',"Conv.")';
    content += '});';
    content += '$("#dim'+idDimension +'Revenue").on("click", function (event,ui) {';
    content += '    updateWidgetWithNewTable('+idDimension+',"Revenue")';
    content += '});';
    content += '$("#dim'+idDimension +'Visits").on("click", function (event,ui) {';
    content += '    updateWidgetWithNewTable('+idDimension+',"Visits")';
    content += '});';
    content += '$("#dim'+idDimension +'Views").on("click", function (event,ui) {';
    content += '    updateWidgetWithNewTable('+idDimension+',"Visits")';
    content += '});';
    content += '</script>';

    return content;
}

function createMapColumnNameToIndexByIdDimension(columnsNames,idDimension)
{
    switch (idDimension)
    {
        case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
            for (var i = 0; i < columnsNames.length ; i++)
            {
                mapColumnNameToIndexCampaigns[columnsNames[i]] = i;
                mapIndexToColumnNameCampaigns[i] = columnsNames[i];
            }
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_OFFERS:
            for (var i = 0; i < columnsNames.length ; i++)
            {
                mapColumnNameToIndexOffers[columnsNames[i]] = i;
                mapIndexToColumnNameOffers[i] = columnsNames[i];
            }
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_LANDERS:
            for (var i = 0; i < columnsNames.length ; i++)
            {
                mapColumnNameToIndexLanders[columnsNames[i]] = i;
                mapIndexToColumnNameLanders[i] = columnsNames[i];
            }
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES:
            for (var i = 0; i < columnsNames.length ; i++)
            {
                mapColumnNameToIndexTrafficOffer[columnsNames[i]] = i;
                mapIndexToColumnNameTrafficOffer[i] = columnsNames[i];
            }
            break;
    }


}

function getMapColumnsNamesByIdDimension(idDimension)
{
    switch (idDimension)
    {
        case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
            return mapColumnNameToIndexCampaigns;
            break;

        case StatsAggregateSummary.DIMENSION_TYPE_OFFERS:
            return mapColumnNameToIndexOffers;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_LANDERS:
            return mapColumnNameToIndexLanders;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES:
            return mapColumnNameToIndexTrafficOffer;
            break;
    }
}

function getMapIndexColumnNamesByIdDimension(idDimension)
{
    switch (idDimension)
    {
        case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
            return mapIndexToColumnNameCampaigns;
            break;

        case StatsAggregateSummary.DIMENSION_TYPE_OFFERS:
            return mapIndexToColumnNameOffers;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_LANDERS:
            return mapIndexToColumnNameLanders;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES:
            return mapIndexToColumnNameTrafficOffer;
            break;
    }
}


function setArrayForSort(idDimension,arrayRows)
{
    switch (idDimension)
    {
        case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
            window.arrayRowCampaigns = arrayRows;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_OFFERS:
            window.arrayRowOffers = arrayRows;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_LANDERS:
            window.arrayRowLanders = arrayRows;
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES:
            window.arrayRowTraficSources = arrayRows;
            break;
    }
}

function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10));
}

function formatInt (value) {
    if (isNaN(value))
    {
        return -100000;
    }
    return value;
}

function getTheWay(idDimension,columnName,bProcessUpdate)
{
    var way;
    var wayRow;

    if (bProcessUpdate)
    {
        way = tabOrderElement[idDimension]["way"];
    }
    else
    {
        wayRow = tabOrderElement[idDimension];
        if(wayRow['columnOrders'] == columnName)
        {
            way = -(wayRow['way']);
        }
        else
        {
            way = -1;
        }
    }


    return way;
}

function updatetabOrderElement(idDimension,columnName,way)
{

    tabOrderElement[idDimension] = {
        columnOrders : columnName,
        way : way
    };
}

function getDefaultXAndY(idDimension)
{
    var positionXY = {};
    switch (idDimension)
    {
        case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
            positionXY =
            {
                x:0,
                y:0,
                width:6,
                height:6
            };
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_OFFERS:
            positionXY =
            {
                x:6,
                y:0,
                width:6,
                height:6
            };
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES:
            positionXY =
            {
                x:0,
                y:6,
                width:6,
                height:6
            };
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_LANDERS:
            positionXY =
            {
                x:6,
                y:6,
                width:6,
                height:6
            };
            break;
        case StatsAggregateSummary.DIMENSION_TYPE_COUNTRIES:
            positionXY =
            {
                x:0,
                y:12,
                width:8,
                height:8
            };
            break;
        case StatsAggregateSummary.DIMENSION_ACCESS_LOGS:
            positionXY =
            {
                x:8,
                y:12,
                width:4,
                height:4
            };
            break;            
        case StatsAggregateSummary.DIMENSION_UPDATE_SYSTEM:
            positionXY =
            {
                x:8,
                y:16,
                width:4,
                height:4
            };
            break;
        case StatsAggregateSummary.DIMENSION_LATEST_POSTS:
            positionXY =
            {
                x:0,
                y:0,
                width:4,
                height:4
            };
            break;
        case StatsAggregateSummary.DIMENSION_VIDEO_1:
            positionXY =
            {
                x:0,
                y:17,
                width:4,
                height:4
            };
            break;
        case StatsAggregateSummary.DIMENSION_VIDEO_2:
            positionXY =
            {
                x:4,
                y:17,
                width:4,
                height:4
            };
            break;
        case StatsAggregateSummary.DIMENSION_VIDEO_3:
            positionXY =
            {
                x:8,
                y:17,
                width:4,
                height:4
            };
            break;
    }

    return positionXY;
}
