

function deleteStatsonNodes()
{
    var aNodes = getFunnelNodes();

    for(var i=0; i<aNodes.length; i++)
    {
        var node = aNodes[i];
        var statsSelector = '#' + node.funnelNode.getDivId() + " .node-inline-stats";
        $(statsSelector).hide();
    }
}

function formatSubTitle(nodeType, name)
{
    if( !name )
    {
        name = '';
    }

    switch(nodeType)
    {
        case NODE_TYPE.ROOT:
        case NODE_TYPE.ROTATOR:
            name = '';
            break;

        default:
            name = strip_tags(name).substr(0, 30);
            break;
    }

    return name;
}

function getHeatmapColorFromPalette(aPalette, value, maxValue)
{
    maxValue = Math.max(value, maxValue);

    var nbIntensities = aPalette.length;
    var colorIndex = maxValue > 0
        ? parseInt((nbIntensities-1) * value / maxValue)
        : 0;

    return aPalette[colorIndex];
}

function getHeatmapTextColor(value, maxValue)
{
    var aPalette = [
        '#000000',
        '#000000',
        '#000000',
        '#000000',
        '#000000',
        '#FFFFFF',
        '#FFFFFF',
        '#FFFFFF'
    ];

    return getHeatmapColorFromPalette(aPalette, value, maxValue);
}

function getHeatmapColorFromRange(minHexColor, maxHexColor, value, maxValue)
{
    var gradientsIntensities = 128;
    value = maxValue > 0 ? Math.max(0, parseInt((value * gradientsIntensities) / maxValue)-1) : 0;
    maxValue = gradientsIntensities - 1;

    var minCol = hexToRgb(minHexColor);
    var maxCol = hexToRgb(maxHexColor);

    var step = maxValue > 0 ? (value / maxValue) : 0;

    var r = parseInt(minCol.r + ((maxCol.r - minCol.r) * step));
    var g = parseInt(minCol.g + ((maxCol.g - minCol.g) * step));
    var b = parseInt(minCol.b + ((maxCol.b - minCol.b) * step));

    return rgbToHex(r, g, b);
}

function getHeatmapBGColor(value, maxValue)
{
    var aPalette = [
        '#ffffd9',
        '#c7e9b4',
        '#41b6c4',
        '#225ea8',
        '#081d58'
    ];
    //return getHeatmapColorFromPalette(aPalette, value, maxValue);

    return getHeatmapColorFromRange("#e7f9d4", "#081d58", value, maxValue);
}

function createClickableId(nodeId, cssTextColor)
{
    var style = cssTextColor ? "color:"+cssTextColor+";" : "";
    return '<a onclick="heatmapGoToAnchor(\''+nodeId+'\');" style="'+style+'">ID ' + nodeId + '</a>';
}

function refreshNodesWithStats(heatmapType)
{
    if( window.aNodesStats )
    {
        window.nodeIdToStats = {};

        var aNodes = getFunnelNodes();
        var columnsOffset = window.aNodesStats['columnsOffset'];

        for(var i=0; i<aNodes.length; i++)
        {
            var node = aNodes[i];
            var nodeId = node.funnelNode.getNodeId();
            var nodeType = node.funnelNode.getType();
            var nodeStats = window.aNodesStats[nodeId];


            // Metrics for traffic flow
            var nbVisits = (nodeStats ? nodeStats[columnsOffset+StatsProcessorColumnsInfo.COLUMN_VIEWS] : 0);
            var nbUniques = (nodeStats ? nodeStats[columnsOffset+StatsProcessorColumnsInfo.COLUMN_UNIQUE_VIEWS] : 0);
            var maxVisits = window.aNodesStats['maxVisits'];
            var maxUniques = window.aNodesStats['maxUniques'];
            var flow = 100 * nbVisits / (maxVisits ? maxVisits : 1);


            // Metrics for direct value
            var iNodeValueDirectConversionsColumn = window.aNodesStats["iNodeValueDirectConversionsColumn"];
            var valueDirectConversions = (nodeStats ? nodeStats[iNodeValueDirectConversionsColumn] : 0);
            var maxValueDirectConversions = window.aNodesStats["maxNodeValueDirectConversions"];
            if( valueDirectConversions === undefined )
            {
                valueDirectConversions = 0;
                maxValueDirectConversions = 0;
            }

            var iNodeValueDirectRevenueColumn = window.aNodesStats["iNodeValueDirectRevenueColumn"];
            var valueDirectRevenue = (nodeStats ? nodeStats[iNodeValueDirectRevenueColumn] : 0);
            var maxValueDirectRevenue = window.aNodesStats["maxNodeValueDirectRevenue"];
            if( valueDirectRevenue === undefined )
            {
                valueDirectRevenue = 0;
                maxValueDirectRevenue = 0;
            }

            var iNodeValueDirectRevenueColumn = window.aNodesStats["iNodeValueDirectRevenueColumn"];
            var valueDirectRevenue = (nodeStats ? nodeStats[iNodeValueDirectRevenueColumn] : 0);
            var valueDirectEPV = nbVisits ? valueDirectRevenue / nbVisits : 0;
            var maxValueDirectEPV = window.aNodesStats["maxValueDirectEPV"];


            // Metrics for lifetime value
            var iNodeValueLifetimeConversionsColumn = window.aNodesStats["iNodeValueLifetimeConversionsColumn"];
            var valueLifetimeConversions = (nodeStats ? nodeStats[iNodeValueLifetimeConversionsColumn] : 0);
            var maxValueLifetimeConversions = window.aNodesStats["maxNodeValueLifetimeConversions"];
            if( valueLifetimeConversions === undefined )
            {
                valueLifetimeConversions = 0;
                maxValueLifetimeConversions = 0;
            }

            var iNodeValueLifetimeRevenueColumn = window.aNodesStats["iNodeValueLifetimeRevenueColumn"];
            var valueLifetimeRevenue = (nodeStats ? nodeStats[iNodeValueLifetimeRevenueColumn] : 0);
            var maxValueLifetimeRevenue = window.aNodesStats["maxNodeValueLifetimeRevenue"];
            if( valueLifetimeRevenue === undefined )
            {
                valueLifetimeRevenue = 0;
                maxValueLifetimeRevenue = 0;
            }

            var iNodeValueLifetimeRevenueColumn = window.aNodesStats["iNodeValueLifetimeRevenueColumn"];
            var valueLifetimeRevenue = (nodeStats ? nodeStats[iNodeValueLifetimeRevenueColumn] : 0);
            var valueLifetimeEPV = nbVisits ? valueLifetimeRevenue / nbVisits : 0;
            var maxValueLifetimeEPV = window.aNodesStats["maxValueLifetimeEPV"];


            var metrics1Name = null, metrics1Value = 0, metrics1MaxValue = 0;
            var metrics2Name = null, metrics2Value = 0, metrics2MaxValue = 0;
            var metrics3Name = null, metrics3Value = 0, metrics3MaxValue = 0;
            var sortingValue = 0;
            var sortingMaxValue = 0;

            window.nodeIdToStats[nodeId] = {};
            window.nodeIdToStats[nodeId][KEY_STATS_NODE_TYPE] = nodeType;
            window.nodeIdToStats[nodeId][KEY_STATS_SUBTITLE] = formatSubTitle(nodeType, node.funnelNode.getName());
            window.nodeIdToStats[nodeId][KEY_STATS_VIEWS] = nbVisits;
            window.nodeIdToStats[nodeId][KEY_STATS_UNIQUE_VIEWS] = nbUniques;
            window.nodeIdToStats[nodeId][KEY_STATS_FLOW] = accounting.formatMoney(flow, {symbol: "%", format: "%v%s", precision: 1});
            window.nodeIdToStats[nodeId][KEY_STATS_DIRECT_REVENUE] = accounting.formatMoney(valueDirectRevenue, "", 2);
            window.nodeIdToStats[nodeId][KEY_STATS_DIRECT_CONV] = valueDirectConversions;
            window.nodeIdToStats[nodeId][KEY_STATS_DIRECT_EPV] = accounting.formatMoney(valueDirectEPV, "", 4);
            window.nodeIdToStats[nodeId][KEY_STATS_LIFETIME_REVENUE] = accounting.formatMoney(valueLifetimeRevenue, "", 2);
            window.nodeIdToStats[nodeId][KEY_STATS_LIFETIME_CONV] = valueLifetimeConversions;
            window.nodeIdToStats[nodeId][KEY_STATS_LIFETIME_EPV] = accounting.formatMoney(valueLifetimeEPV, "", 4);


            // Get metric values

            switch( heatmapType )
            {
                case HeatmapTypes.TRAFFIC_FLOW:
                {
                    metrics1Name = "Views";
                    metrics1Value = window.nodeIdToStats[nodeId][KEY_STATS_VIEWS];
                    metrics1MaxValue = maxVisits;

                    metrics2Name = "Unique Views";
                    metrics2Value = window.nodeIdToStats[nodeId][KEY_STATS_UNIQUE_VIEWS];
                    metrics2MaxValue = maxUniques;

                    metrics3Name = "Flow";
                    metrics3Value = window.nodeIdToStats[nodeId][KEY_STATS_FLOW];
                    metrics3MaxValue = 100;
                }
                    break;

                case HeatmapTypes.DIRECT_VALUE_BY_REVENUE:
                case HeatmapTypes.DIRECT_VALUE_BY_CONVERSIONS:
                case HeatmapTypes.DIRECT_VALUE_BY_EPV:
                {
                    metrics1Name = "Direct Revenue";
                    metrics1Value = window.nodeIdToStats[nodeId][KEY_STATS_DIRECT_REVENUE];
                    metrics1MaxValue = accounting.formatMoney(maxValueDirectRevenue, "", 2);

                    metrics2Name = "Conversions";
                    metrics2Value = window.nodeIdToStats[nodeId][KEY_STATS_DIRECT_CONV];
                    metrics2MaxValue = maxValueDirectConversions;

                    metrics3Name = "EPV";
                    metrics3Value = window.nodeIdToStats[nodeId][KEY_STATS_DIRECT_EPV];
                    metrics3MaxValue = accounting.formatMoney(maxValueDirectEPV, "", 4);
                }
                    break;

                case HeatmapTypes.LIFETIME_VALUE_BY_REVENUE:
                case HeatmapTypes.LIFETIME_VALUE_BY_CONVERSIONS:
                case HeatmapTypes.LIFETIME_VALUE_BY_EPV:
                {
                    metrics1Name = "Lifetime Revenue";
                    metrics1Value = window.nodeIdToStats[nodeId][KEY_STATS_LIFETIME_REVENUE];
                    metrics1MaxValue = accounting.formatMoney(maxValueLifetimeRevenue, "", 2);

                    metrics2Name = "Conversions";
                    metrics2Value = window.nodeIdToStats[nodeId][KEY_STATS_LIFETIME_CONV];
                    metrics2MaxValue = maxValueLifetimeConversions;

                    metrics3Name = "EPV";
                    metrics3Value = window.nodeIdToStats[nodeId][KEY_STATS_LIFETIME_EPV];
                    metrics3MaxValue = accounting.formatMoney(maxValueLifetimeEPV, "", 4);
                }
                    break;
            }


            // Get sorting values

            switch( heatmapType )
            {
                case HeatmapTypes.TRAFFIC_FLOW:
                case HeatmapTypes.DIRECT_VALUE_BY_REVENUE:
                case HeatmapTypes.LIFETIME_VALUE_BY_REVENUE:
                {
                    sortingValue = getNumeric(metrics1Value);
                    sortingMaxValue = getNumeric(metrics1MaxValue);
                }
                    break;

                case HeatmapTypes.DIRECT_VALUE_BY_CONVERSIONS:
                case HeatmapTypes.LIFETIME_VALUE_BY_CONVERSIONS:
                {
                    sortingValue = getNumeric(metrics2Value);
                    sortingMaxValue = getNumeric(metrics2MaxValue);
                }
                    break;

                case HeatmapTypes.DIRECT_VALUE_BY_EPV:
                case HeatmapTypes.LIFETIME_VALUE_BY_EPV:
                {
                    sortingValue = getNumeric(metrics3Value);
                    sortingMaxValue = getNumeric(metrics3MaxValue);
                }
                    break;
            }


            var nodeSelector = '#' + node.funnelNode.getDivId();
            var statsSelector = '#' + node.funnelNode.getDivId() + " .node-inline-stats";

            var cssTextColor = getHeatmapTextColor(sortingValue, sortingMaxValue);
            var cssTextColorR = hexToRgb(cssTextColor).r;
            var cssTextColorG = hexToRgb(cssTextColor).r;
            var cssTextColorB = hexToRgb(cssTextColor).r;
            var cssTextColorSquaredDist = cssTextColorR*cssTextColorR + cssTextColorG*cssTextColorG + cssTextColorB*cssTextColorB;
            var mediumColorSquaredDist = 128*128 + 128*128 + 128*128;

            // For the expand icon, if the intensity of the text color is >=128, then it will be white, else it will be black
            var expandIconFilter = cssTextColorSquaredDist >= mediumColorSquaredDist ? "#filter-white" : "#filter-black";

            var cssBgColor = getHeatmapBGColor(sortingValue, sortingMaxValue);
            var cssBgColorR = hexToRgb(cssBgColor).r;
            var cssBgColorG = hexToRgb(cssBgColor).g;
            var cssBgColorB = hexToRgb(cssBgColor).b;

            var clickableId = createClickableId(nodeId, cssTextColor);
            var content = "<span class='title'>"+mapNodeTypeToTitle[nodeType].short+": "+clickableId+"</a></span>";
            content += "<span class='subtitle'>"+window.nodeIdToStats[nodeId][KEY_STATS_SUBTITLE]+"</span>";
            content += "<span nodeId='"+nodeId+"' class='expand-icon'><img src='images/icons/funnel-diagram/16-maximize.png' /></span>";
            content += "<span class='metric1-name'>"+metrics1Name+"</span><span class='metric1-value'>"+metrics1Value+"</span>";
            content += "<span class='metric2-name'>"+metrics2Name+"</span><span class='metric2-value'>"+metrics2Value+"</span>";
            content += "<span class='metric3-name'>"+metrics3Name+"</span><span class='metric3-value'>"+metrics3Value+"</span>";
            $(statsSelector).html("<p>"+content+"</p>");

            var nodeWidth = $(nodeSelector).width() + parseInt($(nodeSelector).css('padding-left')) + parseInt($(nodeSelector).css('padding-right'));
            var nodeHeight = $(nodeSelector).height() + parseInt($(nodeSelector).css('padding-top')) + parseInt($(nodeSelector).css('padding-bottom'));
            $(nodeSelector).css('z-index', 1000);

            var statsWidth = 216;
            var statsHeight = 118;
            var statsLeft = (nodeWidth - statsWidth) / 2;
            var statsTop = (nodeHeight - statsHeight) / 2;

            $(statsSelector).css('left', statsLeft);
            $(statsSelector).css('top', statsTop);
            $(statsSelector).css('width', statsWidth);
            $(statsSelector).css('height', statsHeight);
            $(statsSelector).css('background-color', 'rgba('+cssBgColorR+','+cssBgColorG+','+cssBgColorB+',0.7)');
            $(statsSelector).find('p').css('color', cssTextColor);
            $(statsSelector).find('.expand-icon').css('filter', 'url("'+expandIconFilter+'")');
            $(statsSelector).find('.expand-icon').css('-webkit-filter', 'url("'+expandIconFilter+'")');

            // When displaying the stats over the node, we need to disable the filter of the node
            // (else the filter will also cascade on the stats div). when we disable the stats,
            // we then need to reapply the original filter to the node.
            $(nodeSelector).css('filter', 'none');
            $(nodeSelector).css('-webkit-filter', 'none');
            $(statsSelector).show();
        }

        refreshAllNodes();
    }
}


