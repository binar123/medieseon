/**
 * Created by hp on 24/02/2016.
 */

var HIDDEN_CLASS_COLUMN = "isHidden";
var HIDDEN_CLASS_COLUMN_HEADER = "isHiddenHeader";
var PREFIX_CLASS_COLUMN = "slick_";
var PREFIX_CLASS_COLUMN_HEADER = "slick_header_";
var GROUP_CLASS_GROUPING = "slick-column-group";
var GROUP_PREFIX = "group";
var ROW_LOADING_ID_SUFFIX = "loading-row";
var ROW_LOADER_CLASS = "a-rowloader";
var TOTAL_ROW = "totalrow";
var COLUMNS_ORDERS_COOKIES = "cord";
var COLUMNS_ORDERS_COOKIES_DEFAULT_SORTBY = "csrtb";
var COLUMNS_ORDERS_COOKIES_DEFAULT_SORTDIR = "csrtd";
var PAGE_SIZE_PAGER = "psp";
var COLUMN_REF_COOKIES = "id";
var GROUPING_NAME = Strings.STR_DRILLDOWN_COLUMN_GROUPING;
var MAX_GROUPS = 10;
var INDENT_WIDTH = 15;
var MAX_RESULTS_LOAD = 100;
var INDEXTITLECOLUMNTREE = 0;
var INDEXTITLECOLUMN = 1;
var IDDIMENSION = "idDimension";
//Fixed header Value
var SCROLL_LEFT_HEADER = 31;
var SCROLL_LEFT_HEADER_ROW = 31;
var LEFT_HEADER_POSITION = 0;
var GROUP_NAME_SEPARATOR = " : ";
var MARGINFORPAGER = 70;
var MARGIN_LEFT_TABLEGRID = 5000;

var COLUMNS_PARENT = "parent";
var COLUMNS_ISGROUPING = "isGrouping";
var COLUMNS_ID = "id";
var COLUMNS_INDENT = "indent";
var COLUMNS_COLLAPSE = "_collapsed";

var TabidRecup = [];
var dataView;
var data2,data3,data4,data5;
var myGridSlick;
var columns = [];
var columnFilters = {};

var data = [];
var uniqueID = 1;
var idTotalRow;
var parent = 0;
var indent = 0;
var collapse = true;
var isGroup = false;

var apiResponse;
var searchString = '';
var listDefaultFilter = [];
var hasgrouping = false;
var isHideColumnDisplay = false;
var listGrids = [];
var listColumnsObject = [];
var columnsDisplayed = [];
var defaultColumnFilter;
var defaultColumnFilterValue;
var bDefaultFilter = false;
var groupItemMetadataProvider;
var listOptionsSlick = [];
var hiddenColumns;
var hiddenFilters = [];
var slickOptions;
var bUnderStartProcess = true;
var defaultFilter;
var indexColumnFilter;
var indexColumnId;
var indexColumnStatus;
var sheets;
var sortedItems;
var columnIdToColumnName = {};
var customizeUrl = null;
var idTrafficSourceFilter = null;
var tabIdReferenceForceGoup = [];
var bLoadingNewRow = false;
var loadingNewRow = [];

//Confidence Rate Variable
var updateClickColumn = false;
var updateConversionColumn = false;
var updateRevenueColumn = false;

var aNoTooltipForGroupings = [
    StatsGroupBy.BY_MVT_COMBINATION,
    StatsGroupBy.BY_MVT_KEY
];

function slickFormatColumnTitle(title)
{
    var newName;
    
    if (title == StatsSummaryColumns.ELEMENT_CHECKBOX_HTML)
    {
        newName = StatsSummaryColumns.ELEMENT_CHECKBOX;
    }
    else
    {
        newName = title.replace(/\s+/g, '').replace('/', '').replace('.', '');
    }
    
    columnIdToColumnName[newName] = title;
    
    return newName;
}

function slickGetColumnIndex(columnName)
{
    return myGridSlick.getColumnIndex(slickFormatColumnTitle(columnName));
}

(function($) {

    var slickGrid = function (optionsOrMethod) {

        return initTable(optionsOrMethod, function (options) {

            if (slickOptions.bOnDashboard)
            {
                afterDisplayTreatmentDashboard(optionsOrMethod.tableNameDom);
                var idDim = aDimensions[aDimensions.length - 1];
                if (idDim && idDim == slickOptions.idDimension)
                {
                    window.bonStart = false;
                }
            }
            window.hasToRefresh = true;
            if (window.bmodalwindowopen)
            {
                modalLongActionClose();
            }

        });


    };

    $.fn.slickGrid = function (optionsOrMethod) {
        $.fn.slickGrid.defaults = {
            "ajaxSource": "",
            "tableLoader": "#table-loader-section",
            "bCookies": true,
            "bScrollHeader": false,
            "bLooseMode": false,
            "bFilterHeader": true,
            "bResizeSlickGrid": true,
            "bAutoSize": true,
            "columnRefLooseMode": StatsProcessorColumnsInfo.COLUMN_ROI,
            "bDrillDown": false,
            "autoColumnSize": true,
            "bTreeGrouping": false,
            "matchIdealWidthFromContainer": null,
            "groupingBy":'',
            "bRefreshLayout" : true,
            "bPager" : true,
            "bSelectFilter" : true,
            "enableColumnReorder" : true,
            "bOnDashboard" : false,
            "pagerName" : "pager",
            "bLastRow" : true,
            "bMultipleInstance" : false,
            "columnsMaxWidth" : {},
            "rowHeight" : 40,
            "bDeleteHiddenColumns": false,
            "onRendered": null,
            "columnForceGroup" : 1
        };


        tableNameDom = optionsOrMethod.tableNameDom;
        return new slickGrid(optionsOrMethod);

    };

    $.extend({
        getUrlVars: function () {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getUrlVar: function (name) {
            return $.getUrlVars()[name];
        }
    });

    var FormatHtmlCell = function(row, cell, value, columnDef, dataContext) {

        if (bSetColorFormatData(columnDef.id))
        {
            return setColorFormatData(value);
        }
        else
        {
            return value;
        }

    };

    var TaskNameFormatter = function (row, cell, value, columnDef, dataContext)
    {
        var id = dataContext.id !== undefined
            ? dataContext.id
            : (dataContext.id === undefined && dataContext[0].id !== undefined)
            ? dataContext[0].id
            : -1;

        if( id !== -1 )
        {
            var spacer = "<span style='display:inline-block;height:1px;width:" + (INDENT_WIDTH * dataContext[COLUMNS_INDENT]) + "px'></span>";
            var idx = dataView.getIdxById(id);

            if( idx !== undefined )
            {
                if (data[idx + 1] && data[idx + 1].indent > data[idx].indent)
                {
                    if (data[idx]._collapsed)
                    {
                        return spacer + " <span class='toggle expand'></span>&nbsp;" + value;
                    }
                    else
                    {
                        return spacer + " <span class='toggle collapse'></span>&nbsp;" + value;
                    }
                }
                else
                {
                    if (data[idx] && data[idx][COLUMNS_ISGROUPING])
                    {
                        if (data[idx]._collapsed)
                        {
                            return spacer + " <span class='toggle expand'></span>&nbsp;" + value;
                        }
                        else
                        {
                            return spacer + " <span class='toggle collapse'></span>&nbsp;" + value;
                        }

                    }
                    else
                    {
                        return spacer + " <span class='toggleSubMenu'></span>&nbsp;" + value;
                    }
                }
            }
        }

        return value;
    };

    function initTable(optionsSlick,onSuccessCallback)
    {
        bUnderStartProcess = true;

        optionsSlick = $.extend({}, $.fn.slickGrid.defaults, optionsSlick);

        if (optionsSlick.bMultipleInstance)
        {
            listOptionsSlick[optionsSlick.idDimension] = optionsSlick;
        }
        slickOptions = optionsSlick;

        var params = {};
        var url = optionsSlick.ajaxSource;


        $.ajax({
            url: url,
            data: params,
            type: "POST",
            cache: false,
            success: function (msg) {

                var aResult = JSON.parse(msg);
                apiResponse = aResult['response'];
                hiddenColumns = [StatsSummaryColumns.ELEMENT_ACTIONS];

                if( apiResponse['isSlowReport'] )
                {
                    modalLongActionClose();
                    showSlowReportModal();
                }
                else
                {
                    var columnDefs = apiResponse["columnNames"];
                    var columnTooltips = apiResponse["columnTooltips"];
                    var groupingNames = apiResponse['groupingNames'];
                    var groupingIds = apiResponse['groupingIds'];
                    data = apiResponse["rowsData"];

                    if (optionsSlick.bMultipleInstance)
                    {
                        listDefaultFilter[optionsSlick.idDimension] = apiResponse['defaultFilter'];
                    }

                    //Specific OFFERS
                    if (optionsSlick.idDimension && optionsSlick.idDimension == StatsAggregateSummary.DIMENSION_TYPE_OFFERS && optionsSlick.bOnDashboard)
                    {
                        columnDefs.push(StatsSummaryColumns.CRV);
                    }
                    defaultFilter = apiResponse['defaultFilter'];
                    var totalRowsCalcul = apiResponse['totals'];


                    columns = [];
                    if (groupingNames)
                    {
                        hasgrouping = groupingNames.length > 0;
                    }


                    if( window.drilldownTreeLoadedReportId && hasgrouping )
                    {
                        // Populate loaded report groupings

                        window.aStatsSelectedGroupsAndIds = [];
                        for(var i=0; i<groupingNames.length; i++)
                        {
                            var groupBy = groupingNames[i];
                            var groupId = i < groupingIds.length ? groupingIds[i] : '';
                            window.aStatsSelectedGroupsAndIds.push({by: groupBy, id: groupId});
                        }
                        statsCascadingGroupsPopulateGroups(window.aStatsSelectedGroupsAndIds);


                        // Set options

                        var statOptions = apiResponse["statOptions"];
                        statsTableBuilderSetOptions(
                            inArray(URLParams.STATS_HIDE_EMPTY_GROUPS, statOptions),
                            inArray(URLParams.STATS_STRICT_MODE, statOptions),
                            inArray(URLParams.STATS_SHOW_FILTERED_IPS, statOptions),
                            inArray(URLParams.STATS_SHOW_DUPLICATE_CLICKS, statOptions));
                    }

                    //Slick Column creation
                    if (optionsSlick.hiddenColumns)
                    {
                        hiddenColumns = arrayUnique(hiddenColumns.concat(optionsSlick.hiddenColumns));
                    }
                    for (var i = 0; i < columnDefs.length; i++)
                    {
                        managingColumnCreation(columnDefs[i],optionsSlick,i);

                        // Add tooltip
                        var latestAddedColumn = columns[columns.length-1];
                        if (optionsSlick.bDrillDown)
                        {
                            if ((i || !hasgrouping) && columnTooltips)
                            {
                                latestAddedColumn["toolTip"] = columnTooltips[i];
                            }
                        }
                        else
                        {
                            if (!optionsSlick.bOnDashboard && columnTooltips && i && columnTooltips[latestAddedColumn["field"]])
                            {
                                latestAddedColumn["toolTip"] = columnTooltips[latestAddedColumn["field"]];
                            }
                        }

                    }

                    if (optionsSlick.forceGroups)
                    {
                        columns[optionsSlick.columnForceGroup].formatter = TaskNameFormatter;
                    }

                    if (hasgrouping)
                    {
                        var formatters = optionsSlick.formatters;
                        var taskmNameFormatterSpecific = formatters[GROUPING_NAME] ? formatters[GROUPING_NAME] : TaskNameFormatter;
                        var sizeMaxColumn = 10000;
                        columns.unshift({
                            id: GROUPING_NAME,
                            name: GROUPING_NAME,
                            field: 0,
                            formatter: taskmNameFormatterSpecific,
                            rerenderOnResize: true,
                            sortable: true,
                            cssClass: PREFIX_CLASS_COLUMN + GROUPING_NAME,
                            headerCssClass: PREFIX_CLASS_COLUMN_HEADER + GROUPING_NAME,
                            maxWidth: sizeMaxColumn,    // Columns autosizer will allow this column to be as wide as this
                            canShrink: true     // Columns autosizer will allow this column to get smaller than its content
                        });
                    }

                    if (optionsSlick.bMultipleInstance)
                    {
                        listColumnsObject[optionsSlick.idDimension] = columns;
                    }

                    //Fixing Bug display last column
                    if (!optionsSlick.bOnDashboard)
                    {
                        fixSlickGridDisplayBugLastColumn(columns);
                    }



                    //create window column display
                    if (!isHideColumnDisplay)
                    {
                        createPopupColumn();
                        isHideColumnDisplay = true;
                    }

                    var options = {
                        matchIdealWidthFromContainer: optionsSlick.matchIdealWidthFromContainer,  // Table will always try to fit on the width of this component
                        editable: false,
                        enableAddRow: true,
                        enableCellNavigation: true,
                        forceFitColumns: optionsSlick.forceFitColumns !== undefined ? optionsSlick.forceFitColumns : true,
                        enableColumnReorder: optionsSlick.enableColumnReorder,
                        multiColumnSort: true,
                        defaultColumnWidth: 100,
                        rowHeight: optionsSlick.rowHeight,
                        autoHeight: true,
                        asyncEditorLoading: false,
                        enableTextSelectionOnCells : true,
                        showHeaderRow: optionsSlick.bFilterHeader,
                        headerRowHeight: 40,
                        explicitInitialization: true,
                        bResizeSlickGrid: optionsSlick.bResizeSlickGrid,
                        idDimension : optionsSlick.idDimension
                    };

                    //ROW
                    data = apiResponse["rowsData"];
                    var aNewGroups;
                    if (optionsSlick.bTreeGrouping)
                    {
                        if (optionsSlick.forceGroups)
                        {
                            aNewGroups = optionsSlick.forceGroups;
                        }
                        else
                        {
                            aNewGroups = getGroupings();
                        }
                    }
                    else
                    {
                        aNewGroups = [];
                    }

                    isGroup = aNewGroups.length > 1 ;

                    if (optionsSlick.forceGroups)
                    {
                        data = reClassifyDataByGroup(data,optionsSlick.forceGroups);
                    }

                    for (var i = 0; i < data.length; i++)
                    {
                        var d = data[i];
                        parent = 0;
                        indent = 0;
                        collapse = true;

                        if (aNewGroups.length > 0)
                        {
                            var key = inArray(aNewGroups[0]['by'], aStatsGroupByTrackingFields) ? "id" : "by";
                            var columnName = aNewGroups[0][key];

                            if( optionsSlick.bDrillDown )
                            {
                                d[0] = setupGroupingTooltip(d[0], aNewGroups[0]['by'], aNoTooltipForGroupings);
                            }

                            if( doesGroupingRequirePrefix(aNewGroups[0]['by']) )
                            {
                                // Add the grouping name in front of the data

                                var sep = columnName.indexOf(": ");
                                if( sep != -1 )
                                {
                                    d[0] = columnName.substring(sep+1) + GROUP_NAME_SEPARATOR + d[0];
                                }
                                else
                                {
                                    d[0] = columnName + GROUP_NAME_SEPARATOR + d[0];
                                }
                            }
                        }
                        //Specific OFFERS
                        if (optionsSlick.idDimension && optionsSlick.idDimension == StatsAggregateSummary.DIMENSION_TYPE_OFFERS && optionsSlick.bOnDashboard)
                        {
                            d[columnDefs.length-1] = d[StatsSummaryColumns.CRV];
                        }

                        //tree groups and expand all row
                        if (optionsSlick.forceGroups)
                        {
                            if (optionsSlick.bMultipleInstance)
                            {
                                slickOptions = listOptionsSlick[optionsSlick.idDimension]
                            }
                            managingDatasForGroupForce(d);
                        }

                        d[COLUMNS_ID] = uniqueID;
                        d[COLUMNS_INDENT] = indent;
                        d[COLUMNS_PARENT] = parent;
                        d[COLUMNS_COLLAPSE] = collapse;
                        d[COLUMNS_ISGROUPING] = isGroup;
                        uniqueID++;

                        //Get all ID of the grouping
                        if (aNewGroups.length > 1)
                        {
                            var idGroups = d[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS];
                            for (var y = 0; y < idGroups.length; y++)
                            {
                                var groupsId = GROUP_PREFIX + y;
                                d[groupsId] = idGroups[y];
                            }
                        }
                    }

                    // Setup the model
                    {
                        if (optionsSlick.groupingBy)
                        {
                            groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();
                            dataView = new Slick.Data.DataView({
                                groupItemMetadataProvider: groupItemMetadataProvider,
                                inlineFilters: true
                            });

                        }
                        else
                        {
                            dataView = new Slick.Data.DataView({inlineFilters: true});
                        }

                        dataView.beginUpdate();
                        
                        dataView.sort = sortWithGroups;
                        dataView.setFilter(contentFilter);
                        dataView.setFilterArgs({searchString: searchString});
                        dataView.getItemMetadata = metadata(dataView.getItemMetadata);

                        dataView.setItems(data);
                        manageAdditionalLoadInfo(dataView, null, apiResponse,true);

                        dataView.onPagingInfoChanged.subscribe(function (e, args)
                        {
//                            if( myGridSlick && !optionsSlick.bOnDashboard )
//                            {
//                                setTimeout(function(){
//                                    myGridSlick.resetAutosizer();
//                                    myGridSlick.refreshLayout();                                    
//                                }, 100);
//                            }
                        });

                        dataView.onRefreshing.subscribe(function (e, args)
                        {
                            externTreatment();
                        });                        
                        
                        dataView.onRefreshed.subscribe(function (e, args)
                        {
                            $(document).foundation('tooltip', 'reflow');
//                            console.log('start onRefreshed');
//                            externTreatment();
//                            console.log('end onRefreshed');
                        });                        
                    }

                    // Setup the grid
                    {
                        // If the grid was already created before, then free resources and disable existing callbacks
                        if( myGridSlick && !optionsSlick.bOnDashboard )
                        {
                            myGridSlick.destroy();
                        }
                        // initialize the grid
                        myGridSlick = new Slick.Grid(optionsSlick.tableNameDom, dataView, columns, options);
                        myGridSlick.freezeRender();

                        myGridSlick.onColumnResizeStart.subscribe(function (e, args)
                        {
                            updateConfidenceRateDisplay();
                        });

                        myGridSlick.onColumnResizeEnd.subscribe(function (e, args)
                        {
                            updateConfidenceRateDisplay();
                        });

                        myGridSlick.onRendered.subscribe(function (e, args)
                        {
                            cssTreatmentLooseMode();
                            externTreatment();

                            if( optionsSlick.onRendered ) 
                            {
                                optionsSlick.onRendered();
                            }                            
                        });                        

                        //$(optionsSlick.tableNameDom).css('left', MARGIN_LEFT_TABLEGRID);
                        if (optionsSlick.idDimension)
                        {
                            listGrids[optionsSlick.idDimension] = myGridSlick;
                        }

                        //Events Grid
                        initSlickGridEvents(optionsSlick);

                        //Total Rows
                        if (totalRowsCalcul )
                        {
                            data = insertUpdateLastRowTotal(data,totalRowsCalcul);
                        }

                        setIndexColumnFix(optionsSlick.idDimension);

                        //Pager
                        if (optionsSlick.bPager)
                        {
                            insertPagerSlick(optionsSlick.idDimension);
                        }

                        // Retrieve columns order if saved in cookie
                        if (optionsSlick.bCookies)
                        {
                            var columnsOrder = getCookie(createCookieKeyFromURL(location.href + optionsSlick.tableNameDom, COLUMNS_ORDERS_COOKIES));
                            if( columnsOrder )
                            {
                                myGridSlick.setColumns(setColumnsOrder(JSON.parse(columnsOrder)));

                            }

                            refreshColumnSettings();
                        }
                        myGridSlick.init();

                        // If a cookie is saved for the default sorting, then sort the data now
                        {
                            if (optionsSlick.bCookies)
                            {
                                sortDataWithCookiesManagement(optionsSlick.idDimension);

                            }
                        }

                        //cssTreatmentLooseMode();
                    }

                    //Set the default filter if it is
                    if (defaultFilter)
                    {
                        if (optionsSlick.defaultFilter !== undefined)
                        {
                            defaultColumnFilterValue = optionsSlick.defaultFilter
                        }
                        else
                        {
                            defaultColumnFilterValue = defaultFilter[1];
                        }
                        defaultColumnFilter = defaultFilter[0];

                        $(".status-select").val(defaultColumnFilterValue);

                        $(window).unbind('beforeunload');
                    }

                    if (optionsSlick.groupingBy)
                    {
                        data = myGridSlick.getColumns();
                        data[slickGetColumnIndex(optionsSlick.groupingBy)].cssClasses = "slick-column-group";
                        myGridSlick.setColumns(data);

                        groupByGroupingColumn(optionsSlick.groupingBy,totalRowsCalcul);
                    }

                    if (optionsSlick.bRefreshLayout)
                    {
                        myGridSlick.refreshLayout();
                    }

                    // Loading complete
                    $(optionsSlick.tableLoader).hide();
                    $(optionsSlick.tableNameDom).addClass("table-slickgrid");


                    if( window.drilldownTreeLoadedReportId )
                    {
                        // If this report was loaded from one of the "loadable reports" entry in the table
                        // below the drilldown, then after loading, we scroll to the top so we can see
                        // the loaded data

                        $('html,body').animate({scrollTop: 80}, 1000, "easeOutCubic");
                    }

                    bUnderStartProcess = false;

                    myGridSlick.unfreezeRender();
                    dataView.endUpdate();


                    //setTimeout(function ()
                    //{
                    //    $(optionsSlick.tableNameDom).css('left', 0);
                    //
                    //}, 50);
                }

                if( onSuccessCallback )
                {
                    onSuccessCallback(optionsSlick);
                }
            },
            error: function (msg) {
                modalShowMessage('Cannot display table', msg);
            }
        });

        return {
            resetAutosizer : resetAutosizer,
            refreshColumnSettings : refreshColumnSettings,
            applyChangesSlick : applyChangesSlick,
            myDataview : getDataView,
            myGrid : getMySlickGrid,
            loadMoreRowsClickEvent : loadMoreRowsClickEvent,
            externTreatment : externTreatment,
            exportTableToCSV : exportTableToCSV,
            update_grid_datas : update_grid_datas,
            update_delete_datas : update_delete_datas,
            update_insert_datas : update_insert_datas,
            updateAllRowsForGrouping : updateAllRowsForGrouping,
            updateAllRows : updateAllRows,
            getGridForMultipleInstance : getGridForMultipleInstance,
            formatColumnTitle : slickFormatColumnTitle,
            sortWithGroups : sortWithGroups,
            sortData : sortData,
            setCustomizeUrl : setCustomizeUrl,
            updateConfidenceRateDisplay : updateConfidenceRateDisplay,
            taskNameFormatter: TaskNameFormatter,
            setupGroupingTooltip: setupGroupingTooltip,
        };
    }

    function getDataView()
    {
        return dataView;
    }

    function getMySlickGrid()
    {
        return myGridSlick;
    }

    function getIdDimension()
    {
        return slickOptions.idDimension;
    }

    function insertPagerSlick(idDimension)
    {
        var pager;
        var options;
        var myGridPager;

        if (idDimension && slickOptions.bMultipleInstance)
        {
            options = listOptionsSlick[idDimension];
            myGridPager = listGrids[idDimension];

            getGridForMultipleInstance('',myGridPager);
            setIndexColumnFix(idDimension);
        }


        if (slickOptions.bMultipleInstance)
        {
            var height = getHeightWithMarginFromGridStackWidget();
            height = Math.floor(height / options.rowHeight);
            pager = new Slick.Controls.Pager(dataView, myGridSlick, $("#pager" + options.idDimension), height);
        }
        else
        {
            pager = new Slick.Controls.Pager(dataView, myGridSlick, $("#pager"));
            $(".slick-pager-settings-expanded").show();
        }
    }

    function sortDataWithCookiesManagement(idDimension)
    {
        var options;
        var myGridPager;

        if (idDimension && slickOptions.bMultipleInstance)
        {
            options = listOptionsSlick[idDimension];
            myGridPager = listGrids[idDimension];

            getGridForMultipleInstance('',myGridPager);
            setIndexColumnFix(idDimension);
        }
        else
        {
            options = slickOptions;
        }

        var sortColumn = getCookie(createCookieKeyFromURL(location.href + options.tableNameDom, COLUMNS_ORDERS_COOKIES_DEFAULT_SORTBY));
        var sortDir = getCookie(createCookieKeyFromURL(location.href + options.tableNameDom, COLUMNS_ORDERS_COOKIES_DEFAULT_SORTDIR));
        if (sortColumn !== null && sortDir !== null)
        {
            myGridSlick.setSortColumn(sortColumn, sortDir > 0);
            var columnsSlick;
            if (options.bMultipleInstance)
            {
                columnsSlick = listColumnsObject[idDimension]
            }
            else
            {
                columnsSlick = columns;
            }
            var cols = [];
            var FieldColumn = searchOnObjectArray(sortColumn, columnsSlick, 'id');
            if (!FieldColumn)
            {
                return;
            }
            cols.push({
                sortCol: {field: FieldColumn.field},
                sortAsc: sortDir > 0
            });

            if (options.bTreeGrouping)
            {
                if (options.forceGroups)
                {
                    sortData(cols, INDEXTITLECOLUMN);
                }
                else
                {
                    sortData(cols, INDEXTITLECOLUMNTREE);
                }
            }
            else
            {
                sortData(cols, INDEXTITLECOLUMN);
            }

        }
    }

    function initSlickGridEvents(options)
    {
        myGridSlick.onSort.subscribe(function (e, args)
        {
            var cols = args.sortCols;
            if (options.bMultipleInstance)
            {
                getGridForMultipleInstance(args);
            }

            createCookie(createCookieKeyFromURL(location.href + slickOptions.tableNameDom, COLUMNS_ORDERS_COOKIES_DEFAULT_SORTBY), cols[0].sortCol.id);
            createCookie(createCookieKeyFromURL(location.href + slickOptions.tableNameDom, COLUMNS_ORDERS_COOKIES_DEFAULT_SORTDIR), cols[0].sortAsc ? 1 : -1);

            if (options.bTreeGrouping)
            {
                if (options.forceGroups)
                {
                    sortData(cols, INDEXTITLECOLUMN);
                }
                else
                {
                    sortData(cols, INDEXTITLECOLUMNTREE);
                }

            }
            else
            {
                sortData(cols, INDEXTITLECOLUMN);
            }
            
            //externTreatment();
        });

        myGridSlick.onHeaderRowCellRendered.subscribe(function(e, args)
        {
            if (options.bMultipleInstance)
            {
                getGridForMultipleInstance(args);
            }
            
            $(args.node).empty();
            $("<input type='text'>")
                .data("columnId", args.column.id)
                .val(columnFilters[args.column.id])
                .appendTo(args.node);
        
            //externTreatment();
        });

        //create Cookies when get event columns reorder
        myGridSlick.onColumnsReordered.subscribe(function(e, args)
        {
            if (options.bMultipleInstance)
            {
                getGridForMultipleInstance(args);
            }

            saveColumnsOrder();
            //cssTreatmentLooseMode();
            //externTreatment();
        });

        myGridSlick.onColumnsResized.subscribe(function (e, args)
        {
            if (options.bMultipleInstance)
            {
                getGridForMultipleInstance(args);
            }
            // Remove potentially opened zurb foundation tooltips
            // that were open before the columns got resized
            // See this: http://foundation.zurb.com/forum/posts/2341-tooltips-dont-auto-hide-if-parent-element-is-removed-from-dom
            $('.tooltip').each(function () {
                var tooltip = $(this);
                if (tooltip.attr('style')) {
                    tooltip.removeAttr('style');
                }
            });

            //cssTreatmentLooseMode();
            //externTreatment();
        });

        myGridSlick.onCellChange.subscribe(function (e, args)
        {
            if (options.bMultipleInstance)
            {
                getGridForMultipleInstance(args);
            }
            dataView.updateItem(args.item.id, args.item);
            //externTreatment();
        });

        //Click event
        myGridSlick.onClick.subscribe(function (e, args)
        {
            if (options.bMultipleInstance)
            {
                getGridForMultipleInstance(args);
            }

            if ($(e.target).hasClass("toggle"))
            {
                var item = dataView.getItem(args.row);

                if( !inArray(item.id, TabidRecup) )
                {
                    if (slickOptions.idDimension == StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS && !slickOptions.bOnDashboard)
                    {
                        idTrafficSourceFilter = $('#traffic-source-select').val() ? $('#traffic-source-select').val() : null;
                        openRow(item);
                    }
                    else
                    {
                        openRow(item);
                    }
                }

                if (item)
                {
                    item._collapsed = !item._collapsed;
                    dataView.updateItem(item.id, item);
                }

                e.stopImmediatePropagation();

            }
            if ($(e.target).hasClass("slick-checkbox"))
            {
                var aCheckedRows = [];
                $('.slick-checkbox:checked').each(function() {
                    if (this.id !== '')
                    {
                        aCheckedRows.push(this.id);
                    }
                });

                if (aCheckedRows.length > 0)
                {
                    $("#bulk-slick-action-select").prop('disabled',false);
                }
                else
                {
                    $("#bulk-slick-action-select").prop('disabled',true);
                }

                window.aPagesListCheckedRows = aCheckedRows;
                //externTreatment();
            }
        });

        $(myGridSlick.getHeaderRow()).delegate(":input", "change keyup", function (e)
        {
            if (options.bMultipleInstance)
            {
                getGridForMultipleInstance(e,args);
            }
            var columnId = $(this).data("columnId");
            if (columnId != null)
            {
                columnFilters[columnId] = $.trim($(this).val());
                dataView.refresh();

                //externTreatment();

                $(this).focus();
            }
        });


        if (options.bScrollHeader)
        {
            setTimeout(function(){                
                var tableTop = $('.slick-header').offset().top;
                var scrollLeftHeader = $('.grid-canvas').offset().left;

                $(window).scroll(
                    function ()
                    {                    
                        if( $('.slick-header').length === 1 )
                        {
                            if (tableTop < $(this).scrollTop() && $(myGridSlick.getContainerNode()).height() > $(window).height() )
                            {
                                $('.slick-header').addClass("fixheader");
                                $('.fixheader').css('left',scrollLeftHeader-$(window).scrollLeft());

                                $('.slick-headerrow').addClass("fixheaderRow");
                                $('.fixheaderRow').css('left',scrollLeftHeader-$(window).scrollLeft());

                                var headerHeight = $('.slick-headerrow').height() + $('.slick-header').height();
                                $('.grid-canvas').css('margin-top', headerHeight);
                            }
                            else
                            {
                                $('.slick-header').removeClass("fixheader");
                                $('.slick-header').css('left',-LEFT_HEADER_POSITION);

                                $('.slick-headerrow').removeClass("fixheaderRow");
                                $('.slick-headerrow').css('left',0);

                                $('.grid-canvas').css('margin-top',0);
                            }
                        }
                    }
                );
            }, 100);
        }

        if (options.groupingBy) {
            // register the group item metadata provider to add expand/collapse group handlers
            myGridSlick.registerPlugin(groupItemMetadataProvider);
        }

        setDefaultFilterEvent();

    }

    function getGridForMultipleInstance(args,grid)
    {

        var id;
        if (grid)
        {
            myGridSlick = grid;
            dataView = myGridSlick.getData();
            data = dataView.getItems();
            id = myGridSlick.optionsGrid.idDimension;
        }
        else
        {
            myGridSlick = args.grid;
            dataView = args.grid.getData();
            data = dataView.getItems();
            id = myGridSlick.optionsGrid.idDimension;
        }


        slickOptions = listOptionsSlick[id];
        setIndexColumnFix(id);

        //if (slickOptions.bOnDashboard)
        //{
        //    id = $(e.currentTarget).closest(".widget-content").attr("id");
        //}
        //else
        //{
        //
        //}

        return id;
    }

    function externTreatment()
    {
        if (!bUnderStartProcess)
        {
            //console.log('externTreatment');
            
             if (slickOptions.bSelectFilter)
            {
                $('.slick-pager-status').insertBefore(".slick-pager");
                $('#top-tools-container').show();
                $('#top-tools-container').insertAfter(".slick-pager-nav");
            }

            if (slickOptions.defaultFilter || slickOptions.groupingBy || defaultFilter)
            {
                refreshDefaultFilter();
            }

            if (slickOptions.bCheckboxColumn)
            {
                managingCheckboxColumn();
            }

            if (slickOptions.groupingBy)
            {
                fixBugCategoryGroupingDisplay();

            }

        }


    }

    function setCustomizeUrl(aUrl)
    {
        customizeUrl = aUrl;
    }

    function managingCheckboxColumn()
    {

        var html = "<script>";
        html += '$( ".slick-all-checkbox" ).change(function() {';
        html += '   var aCheckedRows = [];';
        html += '   var checked = $(".slick-all-checkbox:checked").length;';
        html += '   if (checked)';
        html += '  {';
        html += '      $(".slick-checkbox").prop("checked", true);';
        html += '      $(".slick-checkbox").each(function() {';
        html += '          if (this.id != "") {';
        html += '           aCheckedRows.push(this.id);';
        html += '          }';
        html += '       });';
        html += '  }';
        html += '   else';
        html += '  {';
        html += '$(".slick-checkbox").prop("checked", false);';
        html += '  }';
        html += ' window.aPagesListCheckedRows = aCheckedRows;';
        html += 'if (aCheckedRows.length > 0)';
        html += '{';
        html += '    $("#bulk-slick-action-select").prop("disabled",false);';
        html += '}';
        html += 'else';
        html += '{';
        html += '    $("#bulk-slick-action-select").prop("disabled",true);';
        html += '}';
        html += '});';
        html += "</script>";

        $(html).insertAfter('.slick-all-checkbox');

        //var checked = $(".slick-all-checkbox:checked").length;
        //if ( checked )
        //{
        //    $(".slick-checkbox").prop("checked", true);
        //}

        var aCheckedRows = window.aPagesListCheckedRows ;

        if (aCheckedRows && aCheckedRows.length > 0)
        {
            $("#bulk-slick-action-select").prop("disabled",false);
            for (var i = 0; i < aCheckedRows.length ; i++)
            {
                $(".slick-checkbox#" + aCheckedRows[i]).prop("checked", true);
            }
        }
        else
        {
            $("#bulk-slick-action-select").prop("disabled",true);
        }
        $(".slick-all-checkbox:checked").prop("checked", false)



    }

    function managingColumnCreation(title,options,index)
    {
        //title = formatColumnTitle(title);
        var sizeMaxColumn = options.columnsMaxWidth[index] ? options.columnsMaxWidth[index] : null;
        var additionalClasses = [];
        var sizeMinColumn;

        if( !sizeMaxColumn )
        {
            if (title == StatsSummaryColumns.ELEMENT_ID)
            {
                sizeMaxColumn = 80;
                additionalClasses = "light";
            }
            else if (title == StatsSummaryColumns.FLUX_OFFER)
            {
                sizeMaxColumn = 250;
            }
            else
            {
                sizeMaxColumn = 10000;
                sizeMinColumn = 1;
            }
        }

        if (options.bOnDashboard && title == StatsSummaryColumns.ELEMENT_NAME)
        {
            sizeMinColumn = 189;
        }

        var sortable = true;

        if (title == StatsSummaryColumns.ELEMENT_CHECKBOX_HTML)
        {
            sortable = false;
        }

        if (!setSpecificFormatter(title, options,sortable,sizeMaxColumn,additionalClasses,index))
        {
            if (options.groupingBy && options.groupingBy == title)
            {
                columns.push(
                    {
                        id: slickFormatColumnTitle(title),
                        formatter: FormatHtmlCell,
                        name: title,
                        field: index,
                        rerenderOnResize: true,
                        sortable: sortable,
                        width: 1,
                        minWidth: sizeMinColumn,
                        maxWidth: 1,
                        cssClass: GROUP_CLASS_GROUPING,
                        headerCssClass: GROUP_CLASS_GROUPING
                    });
            }
            else if (inArray(title,hiddenColumns))
            {
                if( !options.bDeleteHiddenColumns )
                {
                    columns.push(
                        {
                            id: slickFormatColumnTitle(title),
                            name: title,
                            field: index,
                            rerenderOnResize: false,
                            sortable: false,
                            width: 1,
                            minWidth: sizeMinColumn,
                            resizable : false,
                            maxWidth: 1,
                            cssClass: HIDDEN_CLASS_COLUMN,
                            headerCssClass: HIDDEN_CLASS_COLUMN_HEADER
                        });
                }
            }
            else if (options.bOnDashboard)
            {
                if (title == StatsSummaryColumns.ELEMENT_NAME)
                {
                    columns.push(
                        {
                            id: slickFormatColumnTitle(title),
                            name: getNameByIdDimension(options.idDimension),
                            field: index,
                            formatter: FormatHtmlCell,
                            rerenderOnResize: true,
                            sortable: sortable,
                            cssClass: PREFIX_CLASS_COLUMN + title,
                            headerCssClass: PREFIX_CLASS_COLUMN_HEADER + title,
                            maxWidth: sizeMaxColumn,    // Columns autosizer will allow this column to be as wide as this
                            minWidth: sizeMinColumn,
                            canShrink: true     // Columns autosizer will allow this column to get smaller than its content
                        });
                }
                else
                {
                    columns.push(
                        {
                            id: slickFormatColumnTitle(title),
                            name: title,
                            field: index,
                            formatter: FormatHtmlCell,
                            rerenderOnResize: true,
                            sortable: sortable,
                            cssClass: PREFIX_CLASS_COLUMN + title,
                            headerCssClass: 'widget-table-header',
                            maxWidth: sizeMaxColumn,    // Columns autosizer will allow this column to be as wide as this
                            minWidth: 67,
                            canShrink: true     // Columns autosizer will allow this column to get smaller than its content
                        });
                }

            }
            else
            {
                columns.push(
                    {
                        id: slickFormatColumnTitle(title),
                        formatter: FormatHtmlCell,
                        name: title,
                        field: index,
                        rerenderOnResize: true,
                        sortable: sortable,
                        maxWidth: sizeMaxColumn,    // Columns autosizer will allow this column to be as wide as this
                        canShrink: true,
                        cssClass: PREFIX_CLASS_COLUMN + slickFormatColumnTitle(title) + " " + implode(" ", additionalClasses),
                        headerCssClass: PREFIX_CLASS_COLUMN_HEADER + slickFormatColumnTitle(title)
                    });
            }
        }
    }

    function setSpecificFormatter(columnTitle,options,sortable,sizeMaxColumn,additionalClasses,index)
    {
        var formatters = options.formatters;
        var bSetFormatter = false;
        if (formatters)
        {
            if (formatters[columnTitle])
            {
                columns.push(
                    {
                        id: slickFormatColumnTitle(columnTitle),
                        formatter: formatters[columnTitle],
                        name: columnTitle,
                        field: index,
                        rerenderOnResize: true,
                        sortable: sortable,
                        maxWidth: sizeMaxColumn,    // Columns autosizer will allow this column to be as wide as this
                        canShrink: true,
                        cssClass: PREFIX_CLASS_COLUMN + slickFormatColumnTitle(columnTitle) + " " + implode(" ", additionalClasses),
                        headerCssClass: PREFIX_CLASS_COLUMN_HEADER + slickFormatColumnTitle(columnTitle)
                    });
                bSetFormatter = true;
            }
        }

        return bSetFormatter;
    }

    function _applyChangesSlickSuccessCallback(successCallback)
    {
        if( myGridSlick && !slickOptions.bOnDashboard )
        {
            myGridSlick.resetAutosizer();
            myGridSlick.refreshLayout();
        }

        if( successCallback )
        {
            successCallback();
        }
    }

    function applyChangesSlick(bCustomizeUrl, successCallback,options)
    {
        setTimeout(function()
        {
            if( idDimension == undefined && options && options.idDimension != undefined )
            {
                idDimension = options.idDimension;
            }
            
            if (idDimension && idDimension !== undefined)
            {
                switch (idDimension)
                {
                    case StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS:
                        applyChangeForCampaigns(successCallback,options);
                        break;

                    case StatsAggregateSummary.DIMENSION_TYPE_FUNNELS:
                        applyChangeForFunnels(bCustomizeUrl,successCallback);
                        break;

                    default:
                        applyChangeDefault(bCustomizeUrl,successCallback,options);
                }
            }
            else if( isFunction(initSLickGridTable) )
            {
                initSLickGridTable();
            }
            customizeUrl = null;
            applyChangesVisual();

        }, 100);
    }

    function applyChangeForCampaigns(successCallback,options)
    {
        if (options !== undefined && options.bRefresh)
        {
            //update only display rows
            var tabIds = engineCalculateRowToUpdateForCampaignTree();
            update_grid_datas(tabIds,customizeUrl);
        }
        else
        {
            updateAllRowsForGrouping(customizeUrl, function(){
                _applyChangesSlickSuccessCallback(successCallback);
            },null,options.bCollapse,options.bResetRow);
        }
    }

    function applyChangeForFunnels(bCustomizeUrl,successCallback)
    {
        if (bCustomizeUrl && customizeUrl)
        {
            updateAllRows(customizeUrl, null, function(){
                _applyChangesSlickSuccessCallback(successCallback);
            });
        }
        else
        {
            var url =  buildURlStatsAjax(idDimension,window.statsPeriodName,window.statsDateStart,window.statsDateEnd,window.statsTimezone,idCampaign);

            updateAllRows(url, null, function(){
                _applyChangesSlickSuccessCallback(successCallback);
            });
        }
    }

    function applyChangeDefault(bCustomizeUrl,successCallback,options)
    {
        if (slickOptions.forceGroups)
        {
            if (bCustomizeUrl && customizeUrl)
            {
                updateAllRowsForGrouping(customizeUrl, function(){
                    _applyChangesSlickSuccessCallback(successCallback);
                },null,options.bCollapse,options.bResetRow);
            }
            else
            {
                var url =  buildURlStatsAjax(idDimension,window.statsPeriodName,window.statsDateStart,window.statsDateEnd,window.statsTimezone,null,slickOptions.forceGroups);

                updateAllRowsForGrouping(url, function(){
                    _applyChangesSlickSuccessCallback(successCallback);
                },null,options.bCollapse,options.bResetRow);
            }
        }
        else
        {
            if (bCustomizeUrl && customizeUrl)
            {
                updateAllRows(customizeUrl, null, function(){
                    _applyChangesSlickSuccessCallback(successCallback);
                });
            }
            else
            {
                var url =  buildURlStatsAjax(idDimension,window.statsPeriodName,window.statsDateStart,window.statsDateEnd,window.statsTimezone);

                updateAllRows(url, null, function(){
                    _applyChangesSlickSuccessCallback(successCallback);
                });
            }
        }
    }

    /**
     * get all Id row to update
     * @returns {Array}
     */
    function engineCalculateRowToUpdateForCampaignTree()
    {
        var tabIds = [];
        var idsSubLevel = [];
        var x = 0;
        for (var i =0 ; i < data.length ; i++)
        {
            tabIds[i] = data[i][COLUMNS_ID];
            if (data[i][COLUMNS_INDENT] == 1 && !inArray(data[i][StatsProcessorColumnsInfo.KEY_ROW_REFERENCE_ID],idsSubLevel))
            {
                idsSubLevel[x] = data[i][StatsProcessorColumnsInfo.KEY_ROW_REFERENCE_ID];
                x++;
            }
        }
        if (idsSubLevel.length > 0 )
        {
            customizeUrl = setExpandAllRowCampaign();
            customizeUrl += "&idsSubLevel=" + idsSubLevel.join();
        }

        return tabIds;
    }

    function doesGroupingRequirePrefix(groupBy)
    {
        var bRequired = true;

        switch(groupBy)
        {
            case StatsGroupBy.BY_CAMPAIGN:
            case StatsGroupBy.BY_FUNNEL:
            case StatsGroupBy.BY_FUNNEL_NODE_NAME:
            case StatsGroupBy.BY_LANDER:
            case StatsGroupBy.BY_OFFER:
            case StatsGroupBy.BY_LANDER_OFFER:
            case StatsGroupBy.BY_PAGE_CATEGORY:
            case StatsGroupBy.BY_TRAFFIC_SOURCE:
            case StatsGroupBy.BY_OFFER_SOURCE:
            case StatsGroupBy.BY_DATE:
            case StatsGroupBy.BY_HOUR_MINUTE:
            case StatsGroupBy.BY_DAYPARTING_WEEKDAY:
            case StatsGroupBy.BY_DAYPARTING_HOURS:
            case StatsGroupBy.BY_CONVERSION_PATH_FULL:
            case StatsGroupBy.BY_CONVERSION_PATH_LANDERS_OFFERS:
            {
                bRequired = false;
            }
            break;
        }

        return bRequired;
    }

    function cssTreatmentLooseMode()
    {
        for (var i = 0 ; i < dataView.getLength() ; i++)
        {
            var item = dataView.getItem(i);
            if (item)
            {
                setCssForLooseModeRow(i, item);
            }
        }
    }

    /**
     *
     * @param {type} dataView
     * @param {type} parentItem
     * @param {type} response
     * @returns {undefined}
     */
    function manageAdditionalLoadInfo(dataView, parentItem, response, init)
    {
        var loadedRows = response["rowsData"];
        var maxRows = response["recordsTotal"];
        var limitStart = response["limitStart"];
        var totalRowsCalcul = response['totals'];

        if( loadedRows && maxRows && (loadedRows.length + limitStart) < maxRows )
        {
            // We didn't load all rows. Some more can be loaded.

            var remainingRows = maxRows - loadedRows.length - limitStart;
            var maxRowsToLoad = remainingRows < MAX_RESULTS_LOAD ? remainingRows : MAX_RESULTS_LOAD;

            var infoRowUniqueID = randString(10)+"_"+randString(10);

            var infoTitle = "<a class='"+ROW_LOADER_CLASS+"' id='"+infoRowUniqueID+"' limitStart='"+limitStart+"'>Load "+maxRowsToLoad+" more rows... ("+remainingRows+" remaining)</a>";
            var infoRow = createRowItem(infoTitle, infoRowUniqueID, parentItem);

            var parentId = parentItem ? parentItem.id : 0;
            var parentIndex = parentItem ? dataView.getIdxById(parentId) : 0;
            var insertionIndex = parentIndex + limitStart + loadedRows.length + 1;

            // Are there any child opened between the parent index and the regular insertion index?

            var items = dataView.getItems();

            for(var i = parentIndex + 1; i < (insertionIndex-1) ; i++ )
            {
                var nextItem = items[i];

                if( nextItem && nextItem.parent != parentId )
                {
                    // Child opened...
                    insertionIndex++;
                }
            }
            //delete Total Row
            var totalRow = dataView.getItemById(idTotalRow);
            var bInsertTotalRow = false;
            if (totalRow)
            {
                dataView.deleteItem(idTotalRow);
                bInsertTotalRow = true;
            }
            dataView.insertItem(insertionIndex, infoRow);
            //Total Rows
            if (totalRowsCalcul && bInsertTotalRow && init !== true)
            {
                data = insertUpdateLastRowTotal(data,totalRowsCalcul);
            }

            dataView.setIdxById({});
            dataView.updateIdxById();
        }
    }

    /**
     *
     * @returns {undefined}
     */
    function loadMoreRowsClickEvent(event)
    {
        $this = $(event.target);

        if( $this && $this.hasClass(ROW_LOADER_CLASS) )
        {
            var limitStart = Number($this.attr("limitStart"));

            var loadRowItemId = $this.attr("id");
            var loadRowItemIdx = dataView.getIdxById(loadRowItemId);
            var loadRowItem = dataView.getItemByIdx(loadRowItemIdx);

            var parentItemId = loadRowItem.parent;
            var parentItem = parentItemId ? dataView.getItemById(parentItemId) : null;

            openRow(parentItem, loadRowItemIdx, limitStart+MAX_RESULTS_LOAD);

            dataView.deleteItem(loadRowItemId);
        }
    }

    function openRow(rowItem, forceOpenAtIndex, limitStart)
    {
        if( rowItem )
        {
            if (inArray(rowItem.id, loadingNewRow))
            {
                return;
            }
            loadingNewRow.push(rowItem.id);
        }

        if (limitStart === undefined)
        {
            limitStart = 0;
        }

        var tabRows = [];
        var idItem = rowItem ? rowItem.id : 0;
        var aGroups = [];
        if (slickOptions.forceGroups)
        {
            aGroups = slickOptions.forceGroups;
        }
        else
        {
            aGroups = getGroupings();
        }
        document.aGroupBys = [];

        //get the name of the grouping
        var namesGroup = [];
        for (var i = 0; i < aGroups.length; i++)
        {
            var key = inArray(aGroups[i]['by'], aStatsGroupByTrackingFields) ? "id" : "by";
            var columnName = aGroups[i][key];
            document.aGroupBys.push(columnName);

            var sep = columnName.indexOf(": ");
            if (sep != -1)
            {
                namesGroup.push(columnName.substring(sep + 1));
            }
            else
            {
                namesGroup.push(columnName);
            }
        }

        //insert a row indicating the loading...
        {
            var loadingRow = createRowItem("Loading <img src='images/ajax-loader-small-rect-blue.gif'/>", idItem + ROW_LOADING_ID_SUFFIX, rowItem);
            var indexItem = rowItem ? dataView.getIdxById(rowItem.id) : 0;
            dataView.insertItem(forceOpenAtIndex ? forceOpenAtIndex : indexItem + 1, loadingRow);
        }

        var nbId = rowItem ? rowItem.indent : 0;
        var tabIds = [];
        if (rowItem && aGroups.length > 1)
        {
            if (nbId == 0)
            {
                tabIds.push(rowItem[GROUP_PREFIX + 0]);
                aGroups[0]['id'] = rowItem[GROUP_PREFIX + 0];
            }
            else
            {
                for (var i = 0; i <= nbId; i++)
                {
                    var groupName = GROUP_PREFIX + i;
                    tabIds.push(rowItem[groupName]);
                    aGroups[i]['id'] = rowItem[GROUP_PREFIX + 0];
                }
            }
        }
        // determine if insert line is also grouping
        var bGroup = ((!rowItem && aGroups.length > 1) || ((nbId + 2) < (aGroups.length))) ? true : false;

        var params = {};
        var url;
        if (slickOptions.bDrillDown)
        {
            var params = getDrilldownAPIParams(tabIds,limitStart);
            url = buildUrlForAPI('report.drilldown.get',params);
        }
        else
        {
            var idDimension = getIdDimensionForForceGrouping(document.aGroupBys, rowItem.indent);
            url = buildURlStatsAjax(idDimension, window.statsPeriodName, window.statsDateStart, window.statsDateEnd, window.statsTimezone, tabIds[rowItem.indent], aGroups, idTrafficSourceFilter);
            if (slickOptions.bOnDashboard)
            {
                url += '&' + URLParams.STATS_ONLY_WITH_TRAFFIC + '=1';
                url += '&' + URLParams.STATS_NO_ARCHIVED + '=1';
            }
            resetForceGroups();
        }

        $.ajax({
            url: url,
            type: "POST",
            data: params,
            cache: false,
            success: function (msg)
            {
                var aResult = JSON.parse(msg);

                if (aResult['response']['isSlowReport'])
                {
                    showSlowReportModal();
                }
                else
                {
                    tabRows = aResult['response']['rowsData'];
                    var columnOffset = aResult['response']['columnsOffset'];
                    var y = 0;
                    var z = 0;
                    var x = 1;

                    dataView.beginUpdate();
                    {
                        for (var i = 0; i < tabRows.length; i++)
                        {
                            var newItem = tabRows[i];

                            // Add group name
                            if (slickOptions.bDrillDown)
                            {
                                for (var j = 0; j < columnOffset; j++)
                                {
                                    if (doesGroupingRequirePrefix(document.aGroupBys[j]))
                                    {
                                        newItem[j] = namesGroup[j] + GROUP_NAME_SEPARATOR + newItem[j];
                                    }

                                    newItem[j] = setupGroupingTooltip(newItem[j], document.aGroupBys[j], aNoTooltipForGroupings);
                                }
                            }

                            newItem[COLUMNS_ID] = uniqueID;
                            newItem[COLUMNS_INDENT] = rowItem ? nbId + 1 : 0;
                            newItem[COLUMNS_PARENT] = idItem;
                            newItem[COLUMNS_COLLAPSE] = true;
                            newItem[COLUMNS_ISGROUPING] = bGroup;

                            //Get ids of grouping
                            var idGroups = newItem[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS];
                            for (var y = 0; y < idGroups.length; y++)
                            {
                                var groupsId = GROUP_PREFIX + y;
                                newItem[groupsId] = idGroups[y];

                            }
                            //insert the row
                            var indexItem = rowItem ? dataView.getIdxById(rowItem.id) : 0;
                            dataView.insertItem(forceOpenAtIndex ? forceOpenAtIndex + x : indexItem + x, newItem);
                            setCssForLooseModeRow(forceOpenAtIndex ? forceOpenAtIndex + x : indexItem + x, newItem);

                            x++;
                            uniqueID++;
                        }

                        //Adapt column name display
                        if (slickOptions.bDrillDown)
                        {
                            for (var i = 0; i < tabRows.length; i++)
                            {
                                var newItem = tabRows[i];

                                for (var prop in newItem)
                                {
                                    y = parseInt(prop);
                                    z = y + (columnOffset - 1);
                                    newItem[y] = newItem[z];
                                }
                            }
                        }

                        //manage sorting for grouping view
                        if (slickOptions.forceGroups && slickOptions.bCookies)
                        {
                            sortDataWithCookiesManagement(slickOptions.idDimension);
                        }

                        // Delete the loading row animated icon
                        dataView.deleteItem(idItem + ROW_LOADING_ID_SUFFIX);

                        manageAdditionalLoadInfo(dataView, rowItem, aResult['response']);

                        dataView.getItemMetadata = metadata(dataView.getItemMetadata);
                    }
                    //$(slickOptions.tableNameDom).css('left',5000);
                    dataView.endUpdate();
                    //setTimeout(function () {
                    //    $(slickOptions.tableNameDom).css('left',0);
                    //},50);
                    TabidRecup.push(idItem);

                    $(document).foundation('tooltip', 'reflow');
                    //cssTreatmentLooseMode();
                    if (slickOptions.forceGroups)
                    {
                        externTreatment();
                    }

                    updateConfidenceRateDisplay();

                    if( rowItem )
                    {
                        var idRow = inArrayIndex(rowItem.id,loadingNewRow);
                        loadingNewRow.splice(idRow,1);
                    }
                }
            },
            error: function (msg)
            {
                modalShowMessage('Cannot display table', msg);
            }
        });
    }

    function putTooltipsForLooseMode(rowIndex,cellIndex)
    {
        var cellNode = myGridSlick.getCellNode(rowIndex, cellIndex);
        var $cellNode = $(cellNode);

        $cellNode.attr("title", Strings.STR_LOOSE_MODE_CELL_TOOLTIP);
        $cellNode.attr("style", "cursor:help");
        $cellNode.addClass( "stats-loose" );
        //$cellNode.addClass( "has-tip" );
    }

    function resetForceGroups()
    {
        for (var groups in slickOptions.forceGroups)
        {
            slickOptions.forceGroups[groups].id = "";
        }
    }

    function createRowItem(title, id, parentItem)
    {
        var row = [];
        if (!slickOptions.bDrillDown)
        {
            row.push(" ");
        }
        row.push(title);

        for(var i=1; i<columns.length; i++)
        {
            row.push(" ");
        }

        if (defaultFilter)
        {
            row[defaultFilter[2]] = $(".status-select").val();
        }
        row[COLUMNS_ID] = id;
        row[COLUMNS_INDENT] = parentItem ? parentItem.indent + 1 : 0;
        row[COLUMNS_PARENT] = parentItem ? parentItem.id : 0;
        row[COLUMNS_COLLAPSE] = true;
        row[COLUMNS_ISGROUPING] = false;
        row[ROW_LOADING_ID_SUFFIX] = true;

        for(var i=0; i<=row[COLUMNS_INDENT]; i++)
        {
            if( parentItem && parentItem[GROUP_PREFIX+i] )
            {
                row[GROUP_PREFIX+i] = parentItem[GROUP_PREFIX+i];
            }
            else
            {
                row[GROUP_PREFIX+i] = 0;
            }
        }

        return row;
    }

    /**
     *
     * @param {type} value
     * @returns {undefined}
     */
    function sortData_getCleanValue(value)
    {
        if( typeof value === "string" )
        {
            value = replaceAll(strip_tags(value), "%", "");
            value = replaceAll(value, "&nbsp;", "").trim();
        }

        if( !isNaN(Number(value)) )
        {
            value = Number(value);
        }

        return value;
    }

    /**
     *
     * @returns {undefined}
     */
    function sortData(cols,indexTitleColumn)
    {
        dataView.sort(function (dataRow1, dataRow2)
        {
            var bRow1LoadMore = dataRow1[indexTitleColumn].indexOf(ROW_LOADER_CLASS) !== -1 ? true : false;
            var bRow2LoadMore = dataRow2[indexTitleColumn].indexOf(ROW_LOADER_CLASS) !== -1 ? true : false;

            if (dataRow1[indexTitleColumn] == TOTAL_ROW || dataRow1[indexTitleColumn] == TOTAL_ROW )
            {
                return -1;
            }
            if( bRow1LoadMore || bRow2LoadMore )
            {
                if( bRow1LoadMore && !bRow2LoadMore )
                {
                    return 1;
                }
                else if( !bRow1LoadMore && bRow2LoadMore )
                {
                    return -1;
                }
            }
            else
            {
                for (var i = 0, l = cols.length; i < l; i++)
                {
                    var field = cols[i].sortCol.field;
                    var sign = cols[i].sortAsc ? 1 : -1;

                    var value1 = sortData_getCleanValue(dataRow1[field]);
                    var value2 = sortData_getCleanValue(dataRow2[field]);

                    if( value1 == value2 )
                    {
                        // When the 2 fields we compare are equal,
                        // we sort based on the previous columns until we
                        // find 2 different fields or we reach the 1st column

                        var prevField = field - 1;
                        while( prevField >= 0 )
                        {
                            var prevVal1 = sortData_getCleanValue(dataRow1[prevField]);
                            var prevVal2 = sortData_getCleanValue(dataRow2[prevField]);

                            if( prevVal1 != prevVal2 )
                            {
                                value1 = prevVal1;
                                value2 = prevVal2;
                                break;
                            }

                            prevField--;
                        }
                    }

                    if( typeof value1 === "string" )
                    {
                        value1 = replaceAll(strip_tags(value1), "%", "");
                        value1 = replaceAll(value1, "&nbsp;", "").trim();
                    }

                    if( typeof value2 === "string" )
                    {
                        value2 = replaceAll(strip_tags(value2), "%", "");
                        value2 = replaceAll(value2, "&nbsp;", "").trim();
                    }

                    if( !isNaN(Number(value1)) )
                    {
                        value1 = Number(value1);
                    }

                    if( !isNaN(Number(value2)) )
                    {
                        value2 = Number(value2);
                    }

                    var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1));

                    if (result != 0)
                    {
                        return result * sign;
                    }
                }
            }
            return 0;
        });
    }

    /**
     *
     * @param {type} comparer
     * @param {type} ascending
     * @returns {undefined}
     */
    function sortWithGroups(comparer, ascending)
    {
        dataView.setSortAsc(ascending);
        dataView.setSortComparer(comparer);

        var items = dataView.getItems();

        // Build tree of arrays to sort
        var itemsTree = {};
        for(var iGroup=0; iGroup<MAX_GROUPS; iGroup++)
        {
            itemsTree[iGroup] = {};
            var groupItems = itemsTree[iGroup];
            var thisGroupKey = GROUP_PREFIX + iGroup;
            var nextGroupKey = GROUP_PREFIX + (iGroup+1);

            for(var iItem=0; iItem<items.length; iItem++)
            {
                var item = items[iItem];

                if(   (!item.isGrouping && iGroup === 0 && item[nextGroupKey] === undefined)
                    || (item[thisGroupKey] !== undefined && item[nextGroupKey] === undefined) )
                {
                    var key = item.parent === null ? 0 : item.parent;

                    if( groupItems[key] === undefined )
                    {
                        groupItems[key] = [];
                    }

                    groupItems[key].push(item);
                }
            }
        }

        // Sort each array independently
        for(var iGroup in itemsTree)
        {
            var iGroupItems = itemsTree[iGroup];

            for(var iParent in iGroupItems)
            {
                var itemsToSort = iGroupItems[iParent];

                if (ascending === false)
                {
                    itemsToSort.reverse();
                }
                itemsToSort.sort(comparer);
                if (ascending === false)
                {
                    itemsToSort.reverse();
                }
            }
        }

         //And rebuild linear items list from sorted tree's arrays
        sortedItems = [];
        pushItems(itemsTree, 0, sortedItems);

        if( sortedItems.length == items.length )
        {
            for(var i=0; i<items.length; i++)
            {
                items[i] = sortedItems[i];
            }

            dataView.setIdxById({});
            dataView.updateIdxById();
            dataView.refresh();


        }
    }

    function pushItems(itemsTree, iGroup, sortedItems, onlyFromParentId)
    {
        var iGroupItems = itemsTree[iGroup];

        for(var iParent in iGroupItems)
        {
            var items = iGroupItems[iParent];

            for( var i=0; i<items.length; i++ )
            {
                var item = items[i];

                if( !onlyFromParentId || onlyFromParentId == item.parent )
                {
                    sortedItems.push(item);
                    //console.log("pushed "+strip_tags(item[0])+" from group "+iGroup);

                    if( iGroup < MAX_GROUPS )
                    {
                        // Has this item any child?

                        var id = item.id;
                        var iNextGroupItems = itemsTree[iGroup+1];

                        if( iNextGroupItems[id] !== undefined )
                        {
                            pushItems(itemsTree, iGroup+1, sortedItems, id);
                        }
                    }
                }
            }
        }
    }

    function setCssForLooseModeRow(row, rowData)
    {
        if( myGridSlick )
        {
            var idxColumn;

            if (rowData[StatsProcessorColumnsInfo.KEY_ROW_IS_LOOSE])
            {
                if (rowData[StatsProcessorColumnsInfo.KEY_ROW_IS_SINGLE_NODE_TYPE])
                {
                    if (rowData[StatsProcessorColumnsInfo.KEY_ROW_IS_SINGLE_NODE_TYPE] == FunnelNodeTypes.FLUX_OFFER )
                    {
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_FUNNEL_ENTRANCES]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }

                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_UNIQUE_ENTRANCES]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }

                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_SPEND]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                    }
                    else if ( rowData[StatsProcessorColumnsInfo.KEY_ROW_IS_SINGLE_NODE_TYPE] == FunnelNodeTypes.ROOT )
                    {
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_PROFITLOSS]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_CONVERSIONS]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_REVENUE]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                    }
                    else
                    {
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_FUNNEL_ENTRANCES]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_UNIQUE_ENTRANCES]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_PROFITLOSS]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row,idxColumn);
                        }
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_CONVERSIONS]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_SPEND]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                        idxColumn = slickGetColumnIndex(StatsProcessorColumnNames[StatsProcessorColumnsInfo.COLUMN_REVENUE]);
                        if (idxColumn)
                        {
                            putTooltipsForLooseMode(row, idxColumn);
                        }
                    }
                }
            }
        }


    }

    function metadata(old_metadata_provider)
    {
        return function (row) {
            var item = dataView.getItem(row);
            var ret = (old_metadata_provider(row) || {});

            if (item) {
                ret.cssClasses = (ret.cssClasses || '');
                if (item[TOTAL_ROW])
                {
                    ret.cssClasses = ' slick-last-row';
                }
                else if (item[slickOptions.columnRefLooseMode] > 0) {
                    ret.cssClasses = ' stats-positive';
                }
                else if (item[slickOptions.columnRefLooseMode] < 0)
                {
                    ret.cssClasses = ' stats-negative';
                }
                else if (item[indexColumnStatus] == Strings.STR_ARCHIVED )
                {
                    ret.cssClasses = ' slick_archive';
                }
                else
                {
                    ret.cssClasses = ' ';
                }
            }

            return ret;
        };
    }

    function getColumnsSlickOffset()
    {
        var results = 0;
        if (hasgrouping)
        {
            results = 1;
        }
        if(data.length > 0 && hasgrouping)
        {
            results = 2;
        }
        return results;
    }

    function createPopupColumn() {

        var aColVis = statsTableBuilderGetColumnVisibilitySlick();

        var htmlApplyButton = "<li style='padding: 0; margin: 0px 4px 0px 4px; border: 0;'><a style='margin: 4px 0px 4px 0px;' class='medium button postfix colvisApplyButton'>Apply</a></li>";
        var html = '';

        html += "<div id='Colvis_MasterPopup' style='display: none; float: right; margin-right: 10px; margin-top: 39px;'>";
        html += "<ul class='ColVis_collection' style='opacity: 1; position: absolute;min-width: 190px;'>";
        html += htmlApplyButton;

        var offset = getColumnsSlickOffset();

        for (var i = 0; i < columns.length; i++)
        {
            if (columns[i].cssClass != HIDDEN_CLASS_COLUMN && columns[i].id != StatsSummaryColumns.ELEMENT_CHECKBOX && i >= offset)
            {
                var title = columns[i].name;
                var checked = (!aColVis || aColVis[i-offset]) ? "checked" : "";
                title = slickFormatColumnTitle(title);

                var columnName = columns[i].name;

                if( columns[i].toolTip )
                {
                    columnName ="<span data-tooltip aria-haspopup='true' class='has-tip' title='"+columns[i].toolTip+"'>"+columns[i].name+"</span>";
                }
                else
                {
                    columnName = "<span>"+columns[i].name+"</span>";
                }

                if (title != '')
                {
                    html += "<li class='colVisColumnLI' iColumn='"+(i-offset)+"'><label><input type='checkbox' class='advanced-settings' "+checked+" id='id_" + title + "'>" + columnName + "</label></li>";
                }
            }
        }

        html += htmlApplyButton;
        html += "</ul></div>";

        $(html).insertAfter('.ColVis_MasterButton');

        $(document).foundation('tooltip', 'reflow');
    }

    function isColumnVisible(i)
    {
        var bVisible = (i == 0 && hasgrouping);
        var title = slickFormatColumnTitle(columns[i].name);

        if (slickOptions.bDrillDown && !bVisible)
        {
            bVisible = $("#id_" + title + "").is(':checked');
        }
        else if (!bVisible)
        {
            if (window.aColumnsVisibilitySlick)
            {
                if (window.aColumnsVisibilitySlick[i])
                {
                    bVisible = true;
                }
                else
                {
                    bVisible = false;
                }

            }
            else
            {
                bVisible = $("#id_" + title + "").is(':checked');
            }

        }
        return bVisible;
    }

    function areColumnPropertiesSame(aColumns1, aColumns2, propertyName)
    {
        var min = aColumns1.length <= aColumns2.length ? aColumns1.length : aColumns2.length;

        for(var i=0; i<min; i++)
        {
            if( aColumns1[i][propertyName] !== aColumns2[i][propertyName] )
            {
                //console.log(propertyName + ": "+aColumns1[i][propertyName]+" vs "+aColumns2[i][propertyName]);
                return false;
            }
        }

        return true;
    }

    function resetAutosizer()
    {
        myGridSlick.resetAutosizer();
    }

    function refreshColumnSettings()
    {
        columnsDisplayed = [];
        var columnsHeadDisplayed = [];

        for (var i = 0; i < columns.length; i++)
        {
            if( isColumnVisible(i) )
            {
                columnsDisplayed.push(columns[i]);
                columnsHeadDisplayed.push(columns[i][COLUMN_REF_COOKIES]);
            }
        }

        if( !areColumnPropertiesSame(myGridSlick.getColumns(), columnsDisplayed, COLUMN_REF_COOKIES) || slickOptions.bDrillDown )
        {
            //reorder the columns depending on their visibility status
            columnsDisplayed = updateColumnsOrder(columnsHeadDisplayed, myGridSlick.getColumns());

        }

        if (!slickOptions.bOnDashboard && slickOptions.bCookies)
        {
            myGridSlick.setColumns(columnsDisplayed);
            saveColumnsOrder();

            myGridSlick.refreshLayout();
        }
        
        updateConfidenceRateDisplay();
    }

    function fixSlickGridDisplayBugLastColumn(columns)
    {
        var idLastColumn = columns.length - 1;
        var column = columns[idLastColumn];
        column.headerCssClass = "displayTitleLastColumn";
    }

    /**
     * Create cookies for column order
     */
    function saveColumnsOrder()
    {
        var columnsHead = [];
        var columnsOrder = myGridSlick.getColumns();
        for (var column in columnsOrder )
        {
            columnsHead.push(columnsOrder[column][COLUMN_REF_COOKIES]);
        }
        createCookie(createCookieKeyFromURL(location.href + slickOptions.tableNameDom, COLUMNS_ORDERS_COOKIES), JSON.stringify(columnsHead));
    }

    /**
     * reorder columns when refreshing column hide/display
     * @param columnsHead (no ordered)
     * @param columnOrder (front display)
     * @return columns reorder
     */
    function updateColumnsOrder(columnsHead, columnOrder)
    {
        var isDiff = false;
        var doesExist = false;
        var columnResults = [];
        var columnMiss = [];

        //determine if length columnHead is less than columnOrder
        if (columnsHead.length < columnOrder.length)
        {
            isDiff = true;
        }

        for (var columnId in columnsHead)
        {
            doesExist = false;
            for (var columnOrderid in columnOrder )
            {
                if ( columnsHead[columnId] == columnOrder[columnOrderid][COLUMN_REF_COOKIES] && columnOrder[columnOrderid]['name'] !== "" )
                {
                    columnResults[columnOrderid] = columnOrder[columnOrderid];

                    doesExist = true;
                    break;
                }
            }
            if (!doesExist)
            {
                columnMiss.push(columnsHead[columnId]);
            }
        }

        //Add no existing column on the reorder columns
        if (columnMiss.length > 0)
        {
            var bAdded = false;
            for (var columnId in columnMiss )
            {
                //myGridSlick.setColumns(columns);
                var column = searchOnObjectArray(columnMiss[columnId],columns,COLUMN_REF_COOKIES);
                for (var id in columnResults)
                {
                    if (columnResults[id]['field'] > column['field'])
                    {
                        columnResults.splice(id,0,column);
                        bAdded = true;
                        break;
                    }

                }
                if (!bAdded)
                {
                    if (!searchOnObjectArray(column['field'],columnResults,"field"))
                    {
                        columnResults.push(column);
                    }

                }
            }
        }

        //reindex result when columnsHead is less than columnorder
        var columnReindex = [];
        var index = 0;
        for (var id in  columnResults)
        {
            columnReindex[index] = columnResults[id];
            index++;
        }
        return columnReindex;

    }

    /**
     * set new order for slickGrid
     * @param list of column name
     * @return list of grid column
     */
    function setColumnsOrder(newColumnsOrder, columnRef)
    {
        var columnTotal = [];

        if (columnRef)
        {
            columnTotal = columnRef;
        }
        else
        {
            columnTotal = columns;
        }

        var newColumnOrderSlick = [];
        var doesGroupingExist = false;

        for( var columnOrder in newColumnsOrder )
        {
            if( newColumnsOrder[columnOrder] == GROUPING_NAME )
            {
                doesGroupingExist = true;
            }
            for (var column in columnTotal)
            {
                if (columnTotal[column][COLUMN_REF_COOKIES] == newColumnsOrder[columnOrder])
                {
                    newColumnOrderSlick.push(columnTotal[column]);
                    break;
                }
            }
        }

        //Managing grouping column
        if (!doesGroupingExist)
        {
            for (var idColumn in columns )
            {
                if (columns[idColumn][COLUMN_REF_COOKIES] == GROUPING_NAME)
                {
                    newColumnOrderSlick.unshift(columns[idColumn]);
                }
            }

        }

        return newColumnOrderSlick;
    }

    function setIndexColumnFix(idDimension)
    {
        if (idDimension && slickOptions.bMultipleInstance)
        {
            defaultFilter =  listDefaultFilter[idDimension];
        }
        if (defaultFilter)
        {
            indexColumnFilter = defaultFilter[2];
        }
        indexColumnId = slickGetColumnIndex(StatsSummaryColumns.ELEMENT_ID);
        indexColumnStatus = slickGetColumnIndex(StatsSummaryColumns.ELEMENT_STATUS);
    }



    function contentFilter(item, args)
    {
        if( item.parent && item[ROW_LOADING_ID_SUFFIX] !== true)
        {
            var parent = data[dataView.getIdxById(item.parent)];
            while (parent)
            {
                if (parent._collapsed)
                {
                    return false;
                }
                if (parent[indexColumnFilter] && parent[indexColumnFilter].indexOf(args.searchString) == -1)
                {
                    return false;
                }
                parent = data[dataView.getIdxById(parent.parent)];
            }
        }
        //if (bDefaultFilter )
        {
            if (args.searchString != "" && item[indexColumnFilter].indexOf(args.searchString) == -1)
            {
                return false;
            }
        }
        //else
        //{
        //    if (args.searchString != "" && item[0].indexOf(args.searchString) == -1)
        //    {
        //        return false;
        //    }
        //}



        for (var columnId in columnFilters)
        {
            if(columnId !== undefined && columnFilters[columnId] !== "" )
            {
                var idx = slickGetColumnIndex(columnId);

                if( idx !== undefined )
                {
                    var c = myGridSlick.getColumns()[idx];
                    var compareString = columnFilters[columnId];

                    var columnField = item[c.field];

                    if (columnId == GROUPING_NAME || columnId == StatsSummaryColumns.ELEMENT_NAME || columnId == StatsSummaryColumns.FLUX_OFFER)
                    {
                        // String comparison with grouping column

                        columnField = replaceAll(strip_tags(columnField), "&nbsp;", "");

                        if (columnField.toLowerCase().indexOf(compareString.toLowerCase()) == -1)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        // Intelligent comparison with metrics columns.
                        // Accepts expressions like >100 to show all data from column where value greater than 100 etc...

                        // Remove all spaces for test
                        compareString = compareString.replace(/ /g,'');

                        if (compareString == '<' || compareString == '>' || compareString == '='
                            || compareString == '<=' || compareString == '>=' )
                        {
                            continue;
                        }

                        if (typeof columnField === 'string' )
                        {
                            columnField = parseFloat(columnField.replace('%',''));
                        }

                        var bCheckSingleCharFormula = false;
                        var secondFirstCharactere = compareString.substring(0,2);
                        switch (secondFirstCharactere)
                        {
                            case '>=':
                                compareString = compareString.replace('>=','');

                                if (+columnField < +compareString)
                                {
                                    return false;
                                }
                                break;

                            case '<=':
                                compareString = compareString.replace('<=','');
                                if (+columnField > +compareString)
                                {
                                    return false;
                                }
                                break;

                            default:
                                bCheckSingleCharFormula = true;
                                break;
                        }

                        if( bCheckSingleCharFormula )
                        {
                            var firstCharactere = compareString.substring(0,1);
                            switch (firstCharactere)
                            {
                                case '=':
                                    compareString = compareString.replace('=','');
                                    if (columnField != compareString)
                                    {
                                        return false;
                                    }
                                    break;

                                case '>':
                                    compareString = compareString.replace('>','');

                                    if (+columnField <= +compareString)
                                    {
                                        return false;
                                    }
                                    break;

                                case '<':
                                    compareString = compareString.replace('<','');
                                    if (+columnField >= +compareString)
                                    {
                                        return false;
                                    }
                                    break;
                                default:
                                    if (columnField != columnFilters[columnId])
                                    {
                                        return false;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    function bSetColorFormatData(columnID)
    {
        var formattedRoiCol = window.formattedRoiColumnName || (window.formattedRoiColumnName = slickFormatColumnTitle(StatsSummaryColumns.ROI));
        var formattedProfitLossCol = window.formattedProfitLossCol || (window.formattedProfitLossCol = slickFormatColumnTitle(StatsSummaryColumns.PROFIT_LOSS));

        return columnID === formattedRoiCol || columnID === formattedProfitLossCol;
    }

    function setColorFormatData(value)
    {
        if(parseFloat(value) < 0)
        {
            return '<span class="stats-negative">' + value +'</span>';
        }
        else if (parseFloat(value) > 0)
        {
            return '<span class="stats-positive">' + value +'</span>';
        }
        else
        {
            return value;
        }

    }
    function setDefaultFilterEvent()
    {
        $(".status-select").change(function (e) {
            defaultColumnFilterValue = this.value;

            externTreatment();
        });
    }

    function refreshDefaultFilter(value)
    {
        bDefaultFilter = true;
        if (value === undefined)
        {
            value = $(".status-select").val();
            //managing last row
            var rows = dataView.getItems();
            var lastTotalRow = searchOnObjectArray(true,rows,TOTAL_ROW);
            if (lastTotalRow)
            {
                if (value)
                {
                    lastTotalRow[defaultFilter[2]] = value;
                }
                else
                {
                    lastTotalRow[defaultFilter[2]] = defaultFilter[1];
                }

                dataView.updateItem(lastTotalRow.id,lastTotalRow);
            }

        }
        Slick.GlobalEditorLock.cancelCurrentEdit();
        // clear on Esc
        //if (e.which == 27) {
        //    this.value = "";
        //}
        searchString = value;
        updateFilter();
        bDefaultFilter = false;

        //grid.refreshLayout();
    }

    function updateFilter()
    {
        dataView.setFilterArgs({
            searchString: searchString
        });
        dataView.refresh();
    }

    function insertUpdateLastRowTotal(data,totalRowsCalcul,bUpdate)
    {
        if (slickOptions.bLastRow)
        {
            if (bUpdate )
            {
                var rows = dataView.getItems();
                var totalRow = totalRowsCalcul;
                var lastTotalRow = searchOnObjectArray(true,rows,TOTAL_ROW);

                if (slickOptions.groupingBy)
                {
                    totalRow[slickGetColumnIndex(slickOptions.groupingBy)] = TOTAL_ROW;
                }
                totalRow[COLUMNS_ID] = lastTotalRow.id;
                idTotalRow = lastTotalRow.id;
                totalRow[COLUMNS_COLLAPSE] = true;
                totalRow[TOTAL_ROW] = true;
                if (defaultFilter)
                {
                    totalRow[defaultFilter[2]] = defaultFilter[1];
                }
                dataView.updateItem(lastTotalRow.id,totalRow);
            }
            else
            {
                if (slickOptions.groupingBy)
                {
                    totalRowsCalcul[slickGetColumnIndex(slickOptions.groupingBy)] = TOTAL_ROW;
                }
                totalRowsCalcul[COLUMNS_ID] = uniqueID;
                idTotalRow = uniqueID;
                totalRowsCalcul[COLUMNS_COLLAPSE] = true;
                totalRowsCalcul[TOTAL_ROW] = true;
                if (defaultFilter)
                {
                    totalRowsCalcul[defaultFilter[2]] = defaultFilter[1];
                }
                addRow(totalRowsCalcul);
                uniqueID++;
            }
        }

        return data;

    }

    function addRow(item)
    {
        dataView.beginUpdate();
        item[COLUMNS_ID] = uniqueID;
        dataView.addItem(item);
        dataView.endUpdate();
        uniqueID++
    }

    function exportTableToCSV(filename)
    {
        var nbColumns = columns.length ;
        var adjustColumn = 0;

        if (hasgrouping)
        {
            adjustColumn = 1;
        }

        var processRow = function (row)
        {
            var finalVal = '';
            for (var y = 0; y < nbColumns - adjustColumn; y++)
            {
                if (row.__group)
                {
                    finalVal += row.groupingKey;
                    break;
                }
                var innerValue = row[y] === null ? '' : row[y].toString();
                var result = strip_tags(innerValue.replace(/"/g, '""'));
                result = result.replace(/&nbsp/g,"").replace(/;/g,"");

                if (result.search(/("|,|\n)/g) >= 0)
                {
                    result = '"' + result + '"';
                }

                if (y > 0)
                {
                    finalVal += ',';
                }
                else
                {
                    // Add indentation
                    for(var i=0; i<row.indent*4; i++)
                    {
                        finalVal = ' ' + finalVal;
                    }
                }
                finalVal += result;
            }

            return finalVal + '\n';
        };

        var csvFile = '';
        var nbRow = dataView.getLength();
        //Columns
        for (var x = adjustColumn; x < nbColumns ; x++)
        {
            csvFile += columns[x][StatsSummaryColumns.ELEMENT_ID.toLowerCase()];
            if (x != nbColumns)
            {
                csvFile += ',';
            }
        }
        csvFile += '\n';

        //Rows
        for (var i = 0; i < nbRow; i++)
        {
            var row = dataView.getItem(i);
            csvFile += processRow(row);
        }


        var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csvFile);

        $(this)
            .attr({
                'download': filename,
                'href': csvData,
                'target': '_blank'
            });
    }

    function groupByGroupingColumn(titleNameGroup,totalRow)
    {
        var id;
        dataView.setGrouping({
            getter: slickGetColumnIndex(titleNameGroup),
            formatter: function (g) {
                id = g.value;
                return '<span id="' + g.value + '">' + g.value + '</span>' ;
            }
        });

        insertUpdateLastRowTotal(data,totalRow);
        //var item = totalRow;
        //item[grid.getColumnIndex(titleNameGroup)] = TOTAL_ROW;
        //item[TOTAL_ROW] = true;
        //if (defaultFilter)
        //{
        //    item[defaultFilter[2]] = defaultFilter[1];
        //}
        //addRow(item);


        $( "#" + id ).parent("slick-cell").css( "background-color", "#eaf0f4" );

        var html = "<div class='DTTT_container' style='float: right;'>";
        html += '<a class="DTTT_button DTTT_button_expend DTTT_button_text"  tabindex="0"><span>Expand all</span></a>';
        html += '<a class="DTTT_button DTTT_button_collapse DTTT_button_text"  tabindex="0"><span>Collapse all</span></a>';
        html += "</div>";
        html += "<script>";
        html += '$( ".DTTT_button_expend" ).on( "click",function() {';
        html += '    dataView.expandAllGroups();';
        html += '    if (mySlickGrid)';
        html += '    { /*mySlickGrid.externTreatment();*/  }';
        html += '});';

        html += '$( ".DTTT_button_collapse" ).on( "click", function() {';
        html += '    dataView.collapseAllGroups();';
        html += '    if (mySlickGrid)';
        html += '    { /*mySlickGrid.externTreatment();*/  }';
        html += '})';
        html += "</script>";

        $( ".DTTT_container" ).remove();
        $(html).insertAfter('#daterangepicker-container');
        $('#daterangepicker-container').css('top','32px');

        //externTreatment();
    }

    function fixBugCategoryGroupingDisplay()
    {
        $(".r0").css("display","none");

        $('.slick-column-group').hover(function ()
        {
            $(this).css('background-color','#C0E1FF');
        },
        function ()
        {
            $(this).css('background-color','transparent');
        })
    }

    function update_grid_datas(idsListToUpdate,customUrl)
    {
        var params = {};
        var url;
        if (customUrl)
        {
            url = customUrl
        }
        else
        {
            url = slickOptions.ajaxSource;
        }

        var item;
        var rows = dataView.getItems();

        $.ajax({
            type: "POST",
            url: url,
            data: params,

            success: function(msg) 
            {
                var aResult = JSON.parse(msg);
                apiResponse = aResult['response'];
                var newData = [];
                newData = apiResponse["rowsData"];
                var updateItem;

                if (slickOptions.bMultipleInstance)
                {
                    var myGrid = listGrids[idDimension];
                    if( myGrid )
                    {
                        getGridForMultipleInstance('', myGrid);
                    }
                }
                //Update Data for Campaign view in dashboard
                if (slickOptions.bOnDashboard && slickOptions.idDimension == StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS)
                {
                    updateManagingForCampaignRowOnDashboard(newData);
                }
                else
                {
                    for (var i = 0; i < idsListToUpdate.length; i++)
                    {
                        if (slickOptions.forceGroups)
                        {
                            item = dataView.getItemById(idsListToUpdate[i]);
                            updateItem = searchRowOnNewData(item,newData,indexColumnId);
                            if (updateItem === undefined || updateItem === null)
                            {
                                if (!item[TOTAL_ROW]) console.log("item not found on new data: " + item.id);
                                continue;
                            }
                        }
                        else
                        {
                            item = searchOnObjectArray(idsListToUpdate[i],rows,indexColumnId);
                            updateItem = searchOnObjectArray(idsListToUpdate[i],newData,indexColumnId);
                            if (updateItem === undefined || updateItem === null)
                            {
                                if (!item[TOTAL_ROW]) console.log("item not found on new data: " + item.id);
                                continue;
                            }
                        }

                        updateItem[COLUMNS_ID] = item[COLUMNS_ID];
                        updateItem[COLUMNS_INDENT] = item[COLUMNS_INDENT];
                        updateItem[COLUMNS_PARENT] = item[COLUMNS_PARENT];
                        updateItem[COLUMNS_COLLAPSE] = item[COLUMNS_COLLAPSE];
                        updateItem[COLUMNS_ISGROUPING] = item[COLUMNS_ISGROUPING];

                        if (slickOptions.forceGroups)
                        {
                            updateItem[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO] = item[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO];

                            //Get ids of grouping
                            var idGroups = item[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS];
                            for (var y = 0; y < idGroups.length; y++) {
                                var groupsId = GROUP_PREFIX + y;
                                updateItem[groupsId] = idGroups[y];

                            }
                        }
                        dataView.updateItem(item.id,updateItem);
                    }
                }

                if (slickOptions.bCookies)
                {
                    sortDataWithCookiesManagement(slickOptions.idDimension);

                }
                modalLongActionClose();
            }
        });
    }

    function update_delete_datas(idFunnels,customURL)
    {
        var params = {};
        var url;
        if (customURL)
        {
            url = customURL;
        }
        else
        {
            url = slickOptions.ajaxSource;
        }

        var rows = dataView.getItems();
        for (var i = 0; i < idFunnels.length; i++)
        {
            var item = searchOnObjectArray(idFunnels[i],rows,indexColumnId);
            dataView.deleteItem(item.id);
        }

        $.ajax({
            type: "POST",
            url: url,
            data: params,

            success: function(msg) {

                var aResult = JSON.parse(msg);
                apiResponse = aResult['response'];
                var totalRowsCalcul = apiResponse['totals'];

                insertUpdateLastRowTotal(data,totalRowsCalcul,true);
                //externTreatment();
            }
        });

        //externTreatment();
    }

    function update_insert_datas(idFunnels,customUrl,itemCampaignRef,indent)
    {
        var params = {};
        var url;
        if (customUrl)
        {
            url = customUrl
        }
        else
        {
            url = slickOptions.ajaxSource;
        }

        $.ajax({
            type: "POST",
            url: url,
            data: params,

            success: function(msg) {

                var aResult = JSON.parse(msg);
                apiResponse = aResult['response'];
                var newData = apiResponse["rowsData"];
                var insertItem;
                var x = 1;

                for (var i = 0; i < idFunnels.length; i++)
                {
                    insertItem = searchOnObjectArray(idFunnels[i],newData,indexColumnId);
                    insertItem[COLUMNS_ID] = uniqueID;

                    if (slickOptions.forceGroups)
                    {
                        insertItem[COLUMNS_INDENT] = indent;
                        insertItem[COLUMNS_PARENT] = itemCampaignRef[COLUMNS_ID];
                        insertItem[COLUMNS_COLLAPSE] = true;
                        insertItem[COLUMNS_ISGROUPING] = indent < insertItem[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS].length - 1;
                        insertItem[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS][insertItem[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS].length] = insertItem[indexColumnId];

                        //Get ids of grouping
                        var idGroups = insertItem[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS];
                        for (var y = 0; y < idGroups.length; y++) {
                            var groupsId = GROUP_PREFIX + y;
                            insertItem[groupsId] = idGroups[y];

                        }

                        var idxCampaign = dataView.getIdxById(itemCampaignRef.id);
                        var binTabRecup = inArray(itemCampaignRef.id,TabidRecup);
                        if (binTabRecup)
                        {
                            dataView.insertItem(idxCampaign + x,insertItem);
                        }
                        else if (!itemCampaignRef[COLUMNS_COLLAPSE])
                        {
                            dataView.insertItem(idxCampaign + x,insertItem);
                        }

                    }
                    else
                    {
                        dataView.addItem(insertItem);
                    }
                    uniqueID++;
                }
            }
        });
    }

    function updateAllRows(url, idDimension, successCallback)
    {
        var params = {};
        if (url === undefined)
        {
            url = slickOptions.ajaxSource;
        }
        $.ajax({
            type: "POST",
            url: url,
            data: params,

            success: function(msg) {

                var aResult = JSON.parse(msg);
                apiResponse = aResult['response'];
                var data = apiResponse["rowsData"];
                var columnDefs = apiResponse["columnNames"];

                //Specific OFFERS
                if (idDimension && idDimension == StatsAggregateSummary.DIMENSION_TYPE_OFFERS && slickOptions.bMultipleInstance)
                {
                    columnDefs.push(StatsSummaryColumns.CRV);
                }

                for (var i = 0; i < data.length; i++)
                {
                    var d = data[i];
                    parent = 0;
                    indent = 0;
                    collapse = true;

                    //Specific OFFERS
                    if (idDimension && idDimension == StatsAggregateSummary.DIMENSION_TYPE_OFFERS && slickOptions.bMultipleInstance)
                    {
                        d[columnDefs.length-1] = d[StatsSummaryColumns.CRV];
                    }

                    d[COLUMNS_ID] = uniqueID;
                    d[COLUMNS_INDENT] = indent;
                    d[COLUMNS_PARENT] = parent;
                    d[COLUMNS_COLLAPSE] = collapse;
                    uniqueID++;
                }

                var myGrid;
                if (slickOptions.bMultipleInstance)
                {
                    myGrid = listGrids[idDimension];
                    var options = listOptionsSlick[idDimension];
                    slickOptions = options;

                    getGridForMultipleInstance('',myGrid);
                    setIndexColumnFix(idDimension);
                    if (slickOptions.bPager)
                    {
                        insertPagerSlick(idDimension);
                    }
                }
                else
                {
                    myGrid = myGridSlick;
                }

                dataView.beginUpdate();
                dataView.setItems(data);
                dataView.endUpdate();

                $(slickOptions.tableLoader).hide();
                $(slickOptions.tableNameDom).show();

                if (slickOptions.bOnDashboard)
                {
                    //resize the table-gridn otherwise data were hidden
                    var height = getHeightWithMarginFromGridStackWidget();
                    $(slickOptions.tableNameDom).height(height);
                }

                var totalRowsCalcul = apiResponse['totals'];
                if (totalRowsCalcul)
                {
                    insertUpdateLastRowTotal(data,totalRowsCalcul);
                }
                sortDataWithCookiesManagement(idDimension);
                modalLongActionClose();
                if (!slickOptions.bMultipleInstance)
                {
                    TabidRecup = [];
                }

                if (slickOptions.bMultipleInstance)
                {
                    myGrid.invalidate();
                }
                
                if( successCallback )
                {
                    successCallback();
                }
            }
        });
    }

    function updateAllRowsForGrouping(url, successCallback, idDimension, bcollapse, bResetRow)
    {
        var params = {};
        if (url === undefined)
        {
            url = slickOptions.ajaxSource;
        }
        $.ajax({
            type: "POST",
            url: url,
            data: params,

            success: function(msg) {

                var aResult = JSON.parse(msg);
                apiResponse = aResult['response'];
                TabidRecup = [];
                uniqueID = 1;
                indent = 0;

                if (idDimension)
                {
                    if (slickOptions.bMultipleInstance)
                    {
                        var myGrid;
                        myGrid = listGrids[idDimension];
                        var options = listOptionsSlick[idDimension];
                        slickOptions = options;

                        getGridForMultipleInstance('',myGrid);
                    }
                }

                data = apiResponse["rowsData"];
                if (slickOptions.forceGroups)
                {
                    data = reClassifyDataByGroup(data,slickOptions.forceGroups)
                }
                var aNewGroups;
                if (slickOptions.bTreeGrouping)
                {
                    if (slickOptions.forceGroups)
                    {
                        aNewGroups = slickOptions.forceGroups;
                        tabIdReferenceForceGoup = [];
                    }
                    else
                    {
                        aNewGroups = getGroupings();
                    }
                }
                else
                {
                    aNewGroups = [];
                }

                isGroup = aNewGroups.length > 1 ;

                for (var i = 0; i < data.length; i++)
                {
                    var d = data[i];
                    parent = 0;
                    indent = 0;
                    if (bcollapse !== undefined)
                    {
                        collapse = bcollapse
                    }
                    else
                    {
                        collapse = true;
                    }

                    if (aNewGroups.length > 0)
                    {
                        var key = inArray(aNewGroups[0]['by'], aStatsGroupByTrackingFields) ? "id" : "by";
                        var columnName = aNewGroups[0][key];
                        
                        if( doesGroupingRequirePrefix(aNewGroups[0]['by']) )
                        {
                            // Add the grouping name in front of the data

                            var sep = columnName.indexOf(": ");
                            if( sep != -1 )
                            {
                                d[0] = columnName.substring(sep+1) + GROUP_NAME_SEPARATOR + d[0];
                            }
                            else
                            {
                                d[0] = columnName + GROUP_NAME_SEPARATOR + d[0];
                            }
                        }
                        
                        d[0] = setupGroupingTooltip(d[0], aNewGroups[0]['by'], aNoTooltipForGroupings);
                    }
                    if (slickOptions.forceGroups)
                    {
                        if (slickOptions.bMultipleInstance)
                        {
                            slickOptions = listOptionsSlick[slickOptions.idDimension]
                        }
                        managingDatasForGroupForce(d);
                    }

                    d[COLUMNS_ID] = uniqueID;
                    d[COLUMNS_INDENT] = indent;
                    d[COLUMNS_PARENT] = parent;
                    d[COLUMNS_COLLAPSE] = collapse;
                    d[COLUMNS_ISGROUPING] = isGroup;
                    uniqueID++;

                    //Get all ID of the grouping
                    var idGroups = d[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS];
                    for (var y = 0; y < idGroups.length; y++)
                    {
                        var groupsId = GROUP_PREFIX + y;
                        d[groupsId] = idGroups[y];
                    }

                }
                if (bResetRow === true)
                {
                    resetRowsGrid();
                }
                dataView.beginUpdate();
                dataView.setItems(data);
                dataView.endUpdate();

                if (slickOptions.bCookies)
                {
                    sortDataWithCookiesManagement(slickOptions.idDimension);

                }
                modalLongActionClose();
                var totalRowsCalcul = apiResponse['totals'];
                insertUpdateLastRowTotal(data,totalRowsCalcul);
                
                if( successCallback )
                {
                    successCallback();
                }
            }
        });
    }

    /**
     * Update data Campaign view on dashboard
     * @param newData
     */
    function updateManagingForCampaignRowOnDashboard(newData)
    {
        var item,updateItem;
        if ( indexColumnId === undefined) indexColumnId = 0;
        expandAllRow = false;
        reClassifyDataByGroup(newData,slickOptions.forceGroups);
        var idDimensionParent = getIdDimensionForForceGrouping('','',slickOptions.forceGroups[0]['by']);
        var resultsItemParents = searchItemsOnObjectArray(idDimensionParent,data,URLParams.STATS_DIMENSION_ID);

        for (var i = 0; i < newData.length; i++)
        {
            item = searchRowOnNewData(newData[i],data,indexColumnId);
            if (item)
            {
                updateItem = newData[i];

                updateItem[COLUMNS_ID] = item[COLUMNS_ID];
                updateItem[COLUMNS_INDENT] = item[COLUMNS_INDENT];
                updateItem[COLUMNS_PARENT] = item[COLUMNS_PARENT];
                updateItem[COLUMNS_COLLAPSE] = item[COLUMNS_COLLAPSE];
                updateItem[COLUMNS_ISGROUPING] = item[COLUMNS_ISGROUPING];

                if (slickOptions.forceGroups)
                {
                    updateItem[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO] = item[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO];

                    //Get ids of grouping
                    var idGroups = item[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS];
                    for (var y = 0; y < idGroups.length; y++) {
                        var groupsId = GROUP_PREFIX + y;
                        updateItem[groupsId] = idGroups[y];

                    }
                }
                dataView.updateItem(item.id,updateItem);
            }
            else
            {
                parent = 0;
                indent = 0;
                collapse = true;
                isGroup = true;

                var d = newData[i];
                var idDimensionForceGroup = d[URLParams.STATS_DIMENSION_ID];

                //Insert new sub row
                if (idDimensionParent != idDimensionForceGroup)
                {
                    insertRowSubLevel(resultsItemParents,d);
                }
                else
                {
                    d[COLUMNS_ID] = uniqueID;
                    d[COLUMNS_INDENT] = indent;
                    d[COLUMNS_PARENT] = parent;
                    d[COLUMNS_COLLAPSE] = collapse;
                    d[COLUMNS_ISGROUPING] = isGroup;
                    uniqueID++;

                    //Get all ID of the grouping
                    var idGroups = d[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS];
                    for (var y = 0; y < idGroups.length; y++)
                    {
                        var groupsId = GROUP_PREFIX + y;
                        d[groupsId] = idGroups[y];
                    }
                    dataView.addItem(d);
                }
            }
        }

        //Delete Item No present on the new data
        var tabIdsToDelete = [];
        for (var i = 0 ; i < data.length ; i++)
        {
            if(!searchRowOnNewData(data[i],newData,indexColumnId))
            {
                tabIdsToDelete.push(data[i][COLUMNS_ID]);
            }
        }
        for (var x = 0 ; x < tabIdsToDelete.length; x++)
        {
            dataView.deleteItem(tabIdsToDelete[x]);
        }

        //Update IdParent list with loading children
        resultsItemParents = searchItemsOnObjectArray(idDimensionParent,data,URLParams.STATS_DIMENSION_ID);
        updateTabIdRecupForTreeDimension(resultsItemParents);

    }

    /**
     * Update list of the idParent already loading children
     * @param resultsItemParents
     */
    function updateTabIdRecupForTreeDimension(resultsItemParents)
    {
        TabidRecup = [];
        for (var i = 0 ; i < resultsItemParents.length ; i ++)
        {
            if (bParentItemHasChild(resultsItemParents[i]))
            {
                TabidRecup.push(resultsItemParents[i][COLUMNS_ID]);
            }
        }
    }

    /**
     * boolean check if item has children
     * @param item
     * @returns {boolean}
     */
    function bParentItemHasChild(item)
    {
        var itemsChild = searchItemsOnObjectArray(item[indexColumnId],data,StatsProcessorColumnsInfo.KEY_ROW_REFERENCE_ID);
        for (var itemFound in itemsChild)
        {
            if (itemsChild[itemFound][URLParams.STATS_DIMENSION_ID] != item[URLParams.STATS_DIMENSION_ID])
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Search Item on Data (with same ID dimension)
     * @param item
     * @param newData
     * @param indexColumnId
     * @returns {*}
     */
    function searchRowOnNewData(item,newData,indexColumnId)
    {
        var resultsTab = searchItemsOnObjectArray(item[indexColumnId],newData,indexColumnId);
        var newItem = searchOnObjectArray(item[URLParams.STATS_DIMENSION_ID],resultsTab,URLParams.STATS_DIMENSION_ID);

        return newItem ? newItem : null;

    }

    /**
     * insert new Sub Row on tree view
     * @param resultsItemParents
     * @param d
     */
    function insertRowSubLevel(resultsItemParents,d)
    {
        var item = searchOnObjectArray(d[StatsProcessorColumnsInfo.KEY_ROW_REFERENCE_ID],resultsItemParents,indexColumnId);
        if (item && bParentItemHasChild(item))
        {
            parent = item[COLUMNS_ID];
            isGroup = false;
            indent =  d[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS].length - 1;

            d[COLUMNS_ID] = uniqueID;
            d[COLUMNS_INDENT] = indent;
            d[COLUMNS_PARENT] = parent;
            d[COLUMNS_COLLAPSE] = collapse;
            d[COLUMNS_ISGROUPING] = isGroup;
            uniqueID++;

            //Get all ID of the grouping
            var idGroups = d[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS];
            for (var y = 0; y < idGroups.length; y++)
            {
                var groupsId = GROUP_PREFIX + y;
                d[groupsId] = idGroups[y];
            }
            var rowItemIdx = dataView.getIdxById(item[COLUMNS_ID]);
            dataView.insertItem(rowItemIdx + 1,d);
        }
    }

    function resetRowsGrid()
    {
        dataView.beginUpdate();
        dataView.setItems([]);
        dataView.endUpdate();
    }
    
    /**
     * 
     * @param {type} text
     * @returns {undefined}
     */
    function setupGroupingTooltip(text, groupBy, aNoTooltipForGroupings)
    {
        var newText = text;

        if( isString(text) && (!aNoTooltipForGroupings || !inArray(groupBy, aNoTooltipForGroupings)) )
        {
            var title = replaceAll((strip_tags(text)), "'", "&#39;");

            switch(groupBy)
            {
                case StatsGroupBy.BY_CONVERSION_PATH_FULL:
                case StatsGroupBy.BY_CONVERSION_PATH_LANDERS_OFFERS:
                {
                    if( text.indexOf('conversion-path-ellipsis') >= 0 )
                    {
                        title += ' (Hover the ellipsis to see more)';
                    }
                }
                break;
                
                case StatsGroupBy.BY_MVT_COMBINATION:
                {
                    var mText = replaceAll(text, '<strong>', '<strong>&nbsp;&nbsp;');
                    title = replaceAll((strip_tags(mText)), "'", "&#39;");
                }
                break;
            }

            //var spanMarkup = " data-tooltip aria-haspopup='true'  title='"+title+"'";
            var spanMarkup = "title='"+title+"'";
            newText = "<span "+spanMarkup+">"+text+"</span>";
        }

        return newText;
    }

    /**
     * 
     * @param {type} nameSlickTable
     * @returns {undefined}
     */
    function afterDisplayTreatmentDashboard(nameSlickTable)
    {

        //$(".new-row").css('display','none');
        var heightSlickGrid = $( nameSlickTable ).height();
        heightSlickGrid -= 40;
        $( nameSlickTable).css('height',heightSlickGrid);
        //$('.slick-pager-status').css('float','left');
        $('.slick_header_Name').addClass('widget-table-title');
        //$('.slick_header_Name').css('color','#667c8a !important');
        $('.slick_header_Name').each(function () {
            var header = $(this);
            //header[0].style.setProperty('color', '#667c8a', 'important');
        });
        //$('.grid-canvas').css('margin-left',-30);


    }

    function getHeightWithMarginFromGridStackWidget()
    {
        var height = $('.dim-' + slickOptions.idDimension).height();
        height -= MARGINFORPAGER;

        return height;
    }

    function getIdDimensionForForceGrouping(forceGroupsName,indent,groupBy)
    {
        if (!groupBy)
        {
            groupBy = forceGroupsName[indent+1];
        }

        switch (groupBy)
        {
            case StatsGroupBy.BY_CAMPAIGN:
                return StatsAggregateSummary.DIMENSION_TYPE_CAMPAIGNS;
            case StatsGroupBy.BY_FUNNEL:
                return StatsAggregateSummary.DIMENSION_TYPE_FUNNELS;
            case StatsGroupBy.BY_LANDER:
                return StatsAggregateSummary.DIMENSION_TYPE_LANDERS;
            case StatsGroupBy.BY_OFFER:
                return StatsAggregateSummary.DIMENSION_TYPE_OFFERS;
            case StatsGroupBy.BY_TRAFFIC_SOURCE:
                return StatsAggregateSummary.DIMENSION_TYPE_TRAFFIC_SOURCES;
        }
    }

    function updateConfidenceRateDisplay()
    {
        if (slickOptions.bDrillDown)        
        {
            updateClickColumn = false;
            updateConversionColumn = false;
            updateRevenueColumn = false;

            $("#select-confidence-options").trigger("chosen:updated");
            var myGrid = mySlickGrid.myGrid();
            myGrid.invalidateAllRows();
            myGrid.render();

            myGrid.updateCanvasWidth();
            updateHeaderSize(myGrid.getColumns());
        }
    }

    function reClassifyDataByGroup(data,forceGroups)
    {
        var idDimensionParent = getIdDimensionForForceGrouping('','',forceGroups[0]['by']);
        var idDimensionChild = getIdDimensionForForceGrouping('','',forceGroups[1]['by']);
        var dataResult = [];
        var datasChild = [];

        if (idDimensionChild)
        {
            var datasParent = searchItemsOnObjectArray(idDimensionParent,data,URLParams.STATS_DIMENSION_ID);
            for (var i=0 ; i < datasParent.length ; i++)
            {
                dataResult.push(datasParent[i]);
                datasChild = searchItemsOnObjectArray(idDimensionChild,data,URLParams.STATS_DIMENSION_ID);
                datasChild = searchItemsOnObjectArray(datasParent[i][StatsProcessorColumnsInfo.KEY_ROW_REFERENCE_ID],datasChild,StatsProcessorColumnsInfo.KEY_ROW_REFERENCE_ID);

                for(var b=0 ; b < datasChild.length ; b++)
                {
                    dataResult.push(datasChild[b]);
                }
            }

            return dataResult;
        }
        else
        {
            return data;
        }
    }

    function managingDatasForGroupForce(d)
    {
        var idDimensionForceGroup = d[URLParams.STATS_DIMENSION_ID];
        tabIdReferenceForceGoup.push(
            {
                idDimension : idDimensionForceGroup,
                idTable: d[StatsProcessorColumnsInfo.KEY_ROW_REFERENCE_ID],
                idSlick:uniqueID
            });
        var idDimensionParent = getIdDimensionForForceGrouping('','',slickOptions.forceGroups[0]['by']);
        if (idDimensionParent != idDimensionForceGroup)
        {
            indent =  d[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS].length - 1;
            var tabParents = searchItemsOnObjectArray(idDimensionParent, tabIdReferenceForceGoup, IDDIMENSION);

            var itemParent = searchOnObjectArray(d[StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO][StatsProcessorColumnsInfo.KEY_ROW_EXPANDABLE_INFO_GROUP_IDS][0], tabParents,"idTable");
            parent = itemParent.idSlick;
            isGroup = false;
            if (!inArray(parent,TabidRecup))
            {
                TabidRecup.push(parent);
            }
        }
        else
        {
            isGroup = true;
            if (slickOptions.expandAllParentRow)
            {
                if (!inArray(parent,TabidRecup))
                {
                    TabidRecup.push(uniqueID);
                }
                collapse = false;
            }
        }
    }

})(jQuery);
