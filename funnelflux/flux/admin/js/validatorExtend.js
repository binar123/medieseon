(function(){

    function isFilled(s) {
        return s.trim().length > 0;
    }

    validator.extend("isIPsInTextarea", function(s){
        var lines = s.split('\n');
        var is_valid = true;
        var was_checked = false;
        for (var i = 0; i < lines.length && is_valid; i++) {
            var line = lines[i].trim();
            if (!line) {
                continue;
            }
            is_valid = is_valid && validator.isIP(line);
            was_checked = true;
        }
        return is_valid && was_checked;
    });

    validator.extend("equalCountNotEmptyLines", function(source, dest){
        var sourceLines = source.split('\n').filter(isFilled) || [];
        var destLines = dest.split('\n').filter(isFilled) || [];
        return sourceLines.length == destLines.length;
    });
})();
