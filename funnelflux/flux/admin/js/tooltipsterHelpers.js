/* 
 * (c) FunnelFlux.com
 */

function applyConversionPathEllipsis()
{
    // Destroy all already-initialized tooltips for the elipsis
    var tooltipsters = $.tooltipster.instances('.conversion-path-ellipsis');
    $.each(tooltipsters, function(i, instance)
    {
        instance.destroy();
    });

    $('.conversion-path-ellipsis').tooltipster(
            {
                contentAsHtml:true,

                functionFormat: function(instance, helper, content)
                {
                    var INDENT_BLOCK = '&nbsp;&nbsp;';
                    var IMG_NEXT = '<img class="conversion-path-ellipsis-tooltip-arrow" src="images/next-down-right.png?v=2" alt=">&nbsp;" />';
                    var aParts = explode('\\\\n', content);
                    for(var i=1; i<aParts.length; i++)
                    {
                        var indent = '';
                        for(var j=0; j<i; j++)
                        {
                            indent += INDENT_BLOCK;
                        }
                        
                        aParts[i] = indent + IMG_NEXT + aParts[i];
                    }

                    return $('<span>'+implode('<br>', aParts)+'</span>');
                }
            });
}
