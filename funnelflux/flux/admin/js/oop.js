
// Based on http://stackoverflow.com/a/5134494/3135599
//
function inherits(child, parent)
{
    var f = new Function;
    f.prototype = parent.prototype;
    f.prototype.constructor = parent;
    child.prototype = new f;
    child.prototype.constructor = child;
}

