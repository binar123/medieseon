/* global $ */
/*jshint unused: true */
/*exported FFCommandPalette */
function FFCommandPalette()
{
  var isOpen = false;
  var CommandPalette;
  var DOMPalette;
  var DOMInner;
  var DOMList;
  var DOMForm;
  var DOMSearch;
  var DOMListMessage;
  var List = new CommandList();

  
  /**
   * Initialize command palette
   */
  var init = function(){
    DOMPalette = $('<div id="command-palette"></div>');
    DOMInner = $('<div class="inner">');
    DOMForm = $('<form></form>');
    DOMSearch = $('<input type="text" id="command-palette-search" autocomplete="off" />');
    DOMList = List.get();
    DOMForm.append(DOMSearch);
    DOMInner.append(DOMForm);
    DOMInner.append(DOMList);
    DOMPalette.append(DOMInner);
    if($('body #command-palette').length == 0){
      $('body').append(DOMPalette);
    }
    var searchKey;

    /* Bind keyboard shortcuts */
    DOMSearch.keydown(function(e){
      searchKey = DOMSearch.val().toLowerCase();
      switch(e.which){
      case 13:  // Enter
        // List.currentEl.trigger('click');
        List.items[List.activeIndexes[List.idx]]['el'].trigger('click');
        e.preventDefault();
        break;

      case 38: // Prev
        List.prev(); // Up
        e.preventDefault();
        break;

      case 40: // Next
        List.next(); // Down
        e.preventDefault();
        break;
      }
    });
    
    /* prevent event bubbling when clicking palette */
    $('#command-palette').click(function(e){
      e.preventDefault();
      e.stopPropagation();
    });
    
    /* Close palette when user clicks outside of it */
    $(window).click(function(e){
      if(!DOMPalette.is(e.target) && DOMPalette.has(e.target).length === 0)
      {
        close();
      }
    });
    
    DOMList.on('wheel', function(e){
      var e0 = e.originalEvent;
      DOMList.scrollTop(DOMList.scrollTop()-e0.wheelDeltaY);
      return false;
    });
    
    DOMSearch.keyup(function(e){
      var keyword = DOMSearch.val().toLowerCase();
      var lis = List.items;
      var matches = [];
      
      if(e.which==38 || e.which==40){
        return false;
      }
      // if(keyword!==''){
      //   lis.hide();
      //   lis.each(function(){
      //     var li = $(this);
      //     var description = li.children('.description');
      //     var descriptionText = li.data('text');
      //     if( descriptionText.search(keyword) > -1 ){
      //       description.parent('li').show();
      //       matches.push(li);
      //     }
      //   });
      //   if(matches.length>0){
      //     List.sel(matches[0]);
      //     DOMListMessage.hide();
      //   } else {
      //     DOMListMessage.html('<em>No matches found</em>');
      //     DOMListMessage.show();
      //   }
      // } else {
      //   lis.show();
      //   DOMListMessage.hide();
      // }
      
      if(keyword!==''){
        if(searchKey != keyword){ // Keyword changed
          List.idx = 0;
          List.activeIndexes = [];
          for(var i in lis){
            var li = lis[i]['el'];
            if( li.text.toLowerCase().search(keyword) > -1 ){
              li.show();
              li.children('.description').html( highlight(keyword, li.text) );
              List.activeIndexes.push(li.idx);
              matches.push(li);
            } else {
              li.hide();
            }
          }
          if(matches.length>0){
            /* Matches Found */
            List.sel(0);
            DOMListMessage.hide();
          } else {
            DOMListMessage.html('<em>No matches found</em>');
            DOMListMessage.show();
          }
        }
      } else {
        // if(searchKey != keyword){
          List.activeIndexes = [];
          for(var i in lis){
            List.activeIndexes.push(i);
            lis[i]['el'].children('.description').html( lis[i]['el'].text );
            lis[i]['el'].show();
          }
          List.sel(0);
          DOMListMessage.hide();
        // }
      }
    });
  };
  
  /**
   * Populate the command palette list
   */
  var populate = function(data, formatter){
    for(var i in data)
    {
      var item = $('<li class="list-item" >' + formatter(data[i]) + '</li>');
      item.idx = i;
      item.text = data[i]['description'];
      List.append(item, data[i]['callback']);
    }
    DOMListMessage = $('<li class="list-message"></li>');
    // List.append(DOMListMessage);
    DOMList.append(DOMListMessage);
  };
  
  /**
   * Close the command palette
   */
  var close = function(){
    isOpen = false;
    DOMPalette.addClass('animating');
    setTimeout(function(){
      DOMPalette.removeClass('on');
    },0);
    setTimeout(function(){
      DOMPalette.removeClass('animating');
    },300);
  };

  /**
   * Open the command palette
   */
  var open = function(){
    if(isOpen){
      close();
      return false;
    }
    // List.idx=0;
    List.sel(List.idx);
    DOMSearch.val('').trigger('keyup');
    DOMPalette.addClass('animating');
    setTimeout(function(){
      DOMPalette.addClass('on');
    },0);
    setTimeout(function(){
      DOMPalette.removeClass('animating');
      DOMSearch.focus();
    },300);
    
    isOpen = true;
  };
  
  function highlight(needle, haystack){
    var regex = new RegExp('(' + needle + ')', 'ig');
    match = regex.exec(haystack);
    if(match[0]){
      var highlighted = haystack.replace(match[0], '<strong>'+match[0]+'</strong>');
      return highlighted;
    } else {
      console.log('no match');
    }
  }
  
  /*exported List */
  function CommandList(){
    var DOMList = $('<ul></ul>');
    // var DOMListItems = [];
    var currentEl;
    var items = [];
    var size = 0;
    var currentIdx = 0;
    var idx = 0;
    
    var activeIndexes = [];
    
    var scrollList = function(index, direction){
      /**
      * Max list height: 500px
      * List item height: 50px
      */
      var height = DOMList.innerHeight();
      var top = parseInt(index)*50;
      var bottom = (parseInt(index)+1)*50;
      var scrollBottom = bottom-height;
      var scrollTop = (parseInt(index)-1)*50
      if(bottom>(height+DOMList.scrollTop())){
          DOMList.scrollTop(scrollBottom);
      }
      if(DOMList.scrollTop()>top){
          DOMList.scrollTop(scrollTop);
      }
    }
        
    var sel = function(idxOrObj, direction){
      if(!this.activeIndexes[idxOrObj]){
        return false;
      }
      var realIndex = this.activeIndexes[idxOrObj];
      var el = this.items[realIndex]['el'];
      var currentEl = this.items[this.currentIdx]['el'];
      currentEl.removeClass('active');
      el.addClass('active');
      // el[0].scrollIntoView(false);
      this.scrollList(idxOrObj, direction);
      this.idx = idxOrObj;
      this.currentIdx = realIndex;
    };
    
    var next = function(){
      if(this.activeIndexes.length===0){
        return false;
      }
      this.idx = parseInt(this.idx) + 1;
      if(this.idx>(this.activeIndexes.length-1)){
        this.idx = 0;
      }
      this.sel(this.idx, 1);
    };
    
    var prev = function(){
      if(this.activeIndexes.length===0){
        return false;
      }
      this.idx = parseInt(this.idx) - 1;
      if(this.idx<0){
        this.idx = this.activeIndexes.length-1;
      }
      this.sel(this.idx, -1);
    };
    
    var append = function(element, callback){
      var item = [];
      element.mouseover(function(){
        element.addClass('active');
      });
      
      element.mouseout(function(){
        element.removeClass('active');
      });
      
      element.click(function(){
        callback();
        CommandPalette.close();
      });
      
      item['el'] = element;
      item['callback'] = callback;
      // DOMListItems.push(item);
      DOMList.append(element);
      this.items[this.size] = item;
      this.activeIndexes[this.size] = this.size;
      this.size+=1;
    };
    
    var get = function(){
      return DOMList;
    };
    
    return {
      prev: prev,
      next: next,
      get: get,
      sel: sel,
      scrollList: scrollList,
      append: append,
      items: items,
      activeIndexes: activeIndexes,
      size: size,
      idx: idx,
      currentIdx: currentIdx,
      currentEl: currentEl,
    };
  }
  
  init();
  
  CommandPalette = {
    populate: populate,
    open: open,
    close: close,
    List: List
  }
  
  return CommandPalette;
}