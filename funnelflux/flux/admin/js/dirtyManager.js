
var DirtyManager = function()
{
    var m_bDirty = false;
    
    this.setDirty = function(bDirty)
    {
        if( bDirty !== m_bDirty )
        {
            //console.log('DirtyManager says dirty is ' + bDirty);
            m_bDirty = bDirty;
        }
    };
    
    this.isDirty = function()
    {
        return m_bDirty;
    };
};

