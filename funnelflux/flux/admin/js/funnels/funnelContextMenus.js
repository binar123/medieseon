
var aFunnelContextMenuRootNode = [

    {
        name: "Send Traffic Here",
        prepend: '<i class="size-18 iw-mIcon-2 ion-ios-paperplane"/>',
        //img: 'images/icons/link.png',
        fun: function ()
        {
            sendTrafficToNodeUnderContextMenu(false);
        }
    }
];

var aFunnelContextMenuNodes = [

    {
        name: "Send Traffic Here",
        prepend: '<i class="size-18 iw-mIcon-2 ion-ios-paperplane"/>',
        //img: 'images/icons/link.png',
        fun: function ()
        {
            sendTrafficToNodeUnderContextMenu(false);
        }
    },
    {
        name: 'Delete This Node',
        prepend: '<i class="size-18 iw-mIcon-2 ion-trash-a"/>',
        //img: 'images/icons/delete.png?v=3',
        fun: function ()
        {
            // For nodes, 'elementUnderContextMenu' is a jquery object representing the node's div
            var $elem = $('#funnelDiagramContainer').get(0).elementUnderContextMenu;
            
            if( $elem )
            {
                if( $elem.hasClass('w') )
                {
                    deletePlumbElement($elem);
                }
            }
        }
    }
];

var aFunnelContextMenuPageNodes = [

    {
        name: "Send Traffic Here",
        prepend: '<i class="size-18 iw-mIcon-2 ion-ios-paperplane"/>',
        //img: 'images/icons/link.png',
        fun: function ()
        {
            sendTrafficToNodeUnderContextMenu(true);
        }
    },
    {
        name: "Edit This Page",
        prepend: '<i class="size-18 iw-mIcon-2 ion-android-create"/>',
        //img: 'images/icons/edit.png',
        fun: function ()
        {
            editPageUnderContextMenu();
        }
    },
    {
        name: 'Delete This Node',
        prepend: '<i class="size-18 iw-mIcon-2 ion-trash-a"/>',
        //img: 'images/icons/delete.png?v=3',
        fun: function ()
        {
            // For nodes, 'elementUnderContextMenu' is a jquery object representing the node's div
            var $elem = $('#funnelDiagramContainer').get(0).elementUnderContextMenu;
            
            if( $elem )
            {
                if( $elem.hasClass('w') )
                {
                    deletePlumbElement($elem);
                }
            }
        }
    }
];

var aFunnelContextMenuConnectionLabelAction = [

    {
        name: "Get Action's URL",
        prepend: '<i class="size-18 iw-mIcon-2 ion-link"/>',
        //img: 'images/icons/link.png',
        fun: function ()
        {
            // For labels, 'elementUnderContextMenu' is a jsPlumb.Connection instance
            var connection = $('#funnelDiagramContainer').get(0).elementUnderContextMenu;
            
            if( connection )
            {
                var funnelElementData = connection.funnelElementData;
                var actionNumber = funnelElementData.getData()['selectedOption'].replace(CampaignFunnelConnection.ON_ACTION_PREFIX, '');

                var actionUrl = window.actionClickURLTemplate.replace('ACTION-NUMBER', actionNumber);
                var sourceNodeId = "";
                var fallbackUrl = "";

                var sourceNode = connection.source.funnelNode;
                if( sourceNode )
                {
                    sourceNodeId = sourceNode.getNodeId();
                }

                var targetNode = connection.target.funnelNode;
                if( targetNode )
                {
                    switch(targetNode.getType())
                    {
                        case NODE_TYPE.EXTERNAL_URL:
                        case NODE_TYPE.FLUX_LANDER:
                        case NODE_TYPE.FLUX_OFFER:
                        {
                            var params = targetNode.getParams();
                            if( params['pageUrl'] )
                            {
                                fallbackUrl = params['pageUrl'];
                            }
                        }
                        break;
                    }
                }
                
                modalCopyActionURL(actionUrl, window.plumber.idFunnel, sourceNodeId, fallbackUrl);
            }
        }
    },

    {
        name: 'Delete This Connection',
        prepend: '<i class="size-18 iw-mIcon-2 ion-trash-a"/>',
        //img: 'images/icons/delete.png?v=3',
        fun: function ()
        {
            // For labels, 'elementUnderContextMenu' is a jsPlumb.Connection instance
            var elem = $('#funnelDiagramContainer').get(0).elementUnderContextMenu;
            
            if( elem )
            {
                deletePlumbElement(elem);
            }
        }
    }
];

var aFunnelContextMenuConnections = [

    {
        name: 'Delete This Connection',
        prepend: '<i class="size-18 iw-mIcon-2 ion-trash-a"/>',
        //img: 'images/icons/delete.png?v=3',
        fun: function ()
        {
            // For connections, 'elementUnderContextMenu' is a jsPlumb.Connection instance
            var elem = $('#funnelDiagramContainer').get(0).elementUnderContextMenu;
            
            if( elem )
            {
                deletePlumbElement(elem);
            }
        }
    }
];

var aFunnelContextMenuAddElements = [

    {
        name: '&nbsp;&nbsp;Add Offers',
        prepend: '<i class="iw-mIcon-2 icon-offers"/>',
        //img: 'images/icons/offer.png?v=3',
        fun: function ()
        {
            addMultipleFluxOffers();
        }
    },
    {
        name: '&nbsp;&nbsp;Add Landers',
        prepend: '<i class="iw-mIcon-2 icon-landers"/>',
        //img: 'images/icons/page.png?v=3',
        fun: function ()
        {
            addMultipleFluxLanders();
        }
    },
    {
        name: '&nbsp;&nbsp;Add an External URL',
        prepend: '<i class="iw-mIcon-2 icon-externalurl"/>',
        //img: 'images/icons/link.png?v=4',
        fun: function ()
        {
            addExternalUrl();
        }
    },
    {
        name: '&nbsp;&nbsp;Add a Rotator',
        prepend: '<i class="iw-mIcon-2 icon-rotator"/>',
        //img: 'images/icons/rotator.png?v=3',
        fun: function ()
        {
            var xPos = window.mainContextMenuX;
            var yPos = window.mainContextMenuY;

            addNode({nodeType: NODE_TYPE.ROTATOR, nodeName: 'Rotator<br>(%)', x: xPos+'px', y: yPos+'px'}, null, true);
        }
    },
    {
        name: '&nbsp;&nbsp;Add a Condition',
        prepend: '<i class="iw-mIcon-2 icon-condition"/>',
        //img: 'images/icons/condition.png',
        fun: function ()
        {
            addCondition();

            // onClose is not called for submenus. So we need
            // to call the close code from here too...
            window.bMainContextMenuOpened = false;
        }
    },
    {
        name: '&nbsp;&nbsp;Advanced',
        prepend: '<i class="iw-mIcon-2 icon-arrow-right"/>',
        //img: 'images/icons/advanced.png?v=2',
        subMenu: [
            {
                name: '&nbsp;&nbsp;Add Javascript Code',
                prepend: '<i class="iw-mIcon-2 icon-js"/>',
                //img: 'images/icons/js.png?v=5',
                fun: function ()
                {
                    addJSCodeExecutor();

                    // onClose is not called for submenus. So we need
                    // to call the close code from here too...
                    window.bMainContextMenuOpened = false;
                }
            },
            {
                name: '&nbsp;&nbsp;Add PHP Code',
                prepend: '<i class="iw-mIcon-2 icon-php"/>',
                //img: 'images/icons/php.png?v=3',
                fun: function ()
                {
                    addPHPCodeExecutor();
                    
                    // onClose is not called for submenus. So we need
                    // to call the close code from here too...                    
                    window.bMainContextMenuOpened = false;
                }
            }   
        ]
    }
];
