
var FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT = 200;

function editPageUnderContextMenu()
{
    // For nodes, 'elementUnderContextMenu' is a jquery object representing the node's div
    var $nodeElem = $('#funnelDiagramContainer').get(0).elementUnderContextMenu;

    if( $nodeElem )
    {
        if( $nodeElem.hasClass('w') )
        {
            var funnelNode = ($nodeElem.get(0) && $nodeElem.get(0).funnelNode) ? $nodeElem.get(0).funnelNode : null;

            if( funnelNode )
            {
                var section;

                switch( funnelNode.getType() )
                {
                    case NODE_TYPE.FLUX_LANDER:
                    {
                        section = Sections.LANDER_EDIT;
                    }
                    break;

                    case NODE_TYPE.FLUX_OFFER:
                    {
                        section = Sections.OFFER_EDIT;
                    }
                    break;
                }

                if( section )
                {
                    var urlParams = {};
                    urlParams[URLParams.PAGE_ID] = funnelNode.getParams()['id'];
                    urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;
                    var bManualCloseOnOk = true;
                    //urlParams[URLParams.MODAL_HIDE_LOADING] = 1;

                    modalShowSection(section, {
                        urlParams: urlParams,
                        bManualCloseOnOk: bManualCloseOnOk,
                        onOk: function(contentWindow)
                                {
                                    contentWindow.submitPageSettings(function(namePage)
                                    {
                                        funnelNode.setName(namePage, true);
                                        var oParams = funnelNode.getParams();
                                        oParams['name'] = namePage;
                                        funnelNode.setParams(oParams);
                                        modalShowSectionClose('B');
                                    });
                                },
                        onOk2: function()
                                {
                                    modalShowSectionClose('B');
                                }
                    });
                }
            }
        }
    }
}

function sendTrafficToNodeUnderContextMenu(bFromPage)
{
    if( window.bBrandNewFunnel )
    {
        modalShowMessage("Notice", "You must save your funnel first", "Save your funnel then try again...");
    }
    else
    {
        if (!bFromPage)
        {
            bFromPage = '0';
        }
        else
        {
            bFromPage = '1';
        }
        // For nodes, 'elementUnderContextMenu' is a jquery object representing the node's div
        var $nodeElem = $('#funnelDiagramContainer').get(0).elementUnderContextMenu;

        if( $nodeElem )
        {
            if( $nodeElem.hasClass('w') )
            {
                var funnelNode = ($nodeElem.get(0) && $nodeElem.get(0).funnelNode) ? $nodeElem.get(0).funnelNode : null;

                if( funnelNode )
                {
                    var campaignId = window.plumber.idCampaign;
                    var funnelId = window.plumber.idFunnel;
                    var nodeId = funnelNode.getNodeId();

                    if( campaignId && funnelId && nodeId )
                    {
                        var urlParams = {};
                        urlParams[URLParams.YOUR_LINKS_LIMIT_TO_SECTION_BLOCK] = "block-funnel-url";
                        urlParams[URLParams.CAMPAIGN_FUNNEL_FROM_PAGE] = bFromPage;
                        urlParams[URLParams.CAMPAIGN_ID] = campaignId;
                        urlParams[URLParams.CAMPAIGN_FUNNEL_ID] = funnelId;
                        urlParams[URLParams.CAMPAIGN_FUNNEL_NODE_ID] = nodeId;

                        modalShowSection(Sections.GET_LINKS, {
                            urlParams: urlParams,
                            onOk: function(contentWindow)
                                    {
                                    }
                        });
                    }
                    else
                    {
                        console.log('campaignId = '+campaignId);
                        console.log('funnelId = '+funnelId);
                        console.log('nodeId = '+nodeId);

                        modalShowMessage("ERROR", "Could not retrieve node data", "Press F12 to check the debug console");
                    }
                }
            }
        }
    }
}

function plumbFunnelDoWhileSuspended(fn)
{
    if( window.plumber.isSuspendDrawing() )
    {
        try
        {
            fn();
        }
        catch(e)
        {
            console.log('Problem with fn: ' + e);
        }
    }
    else
    {
        window.plumber.doWhileSuspended(fn);
    }
}

/**
 *
 * @param (boolean} bCompletelyRemove - optional, if set to true, node is deleted instead of hidden
 * @returns {undefined}
 */
function deleteAllNodesAndConnections(bCompletelyRemove)
{
    if( window.plumber )
    {
//        window.plumber._katavorio_internal = null;
//        window.plumber._katavorio_main = null;

        plumbFunnelDoWhileSuspended(function()
        {
            window.plumber.detachEveryConnection();
            window.plumber.deleteEveryEndpoint();

            $('#funnelDiagramContainer > .w').each(function()
            {
                $div = $(this);
                deleteNode($div.get(0), bCompletelyRemove);
            });
        });
    }
}

/**
 *
 * @returns {undefined}
 */
function getFunnelNodes()
{
    var aNodes = [];

    $('#funnelDiagramContainer > .w').each(function()
    {
        var funnelNode = ($(this).get(0) && $(this).get(0).funnelNode) ? $(this).get(0).funnelNode : null;

        if(funnelNode)
        {
            var node = {};

            node.funnelNode = funnelNode;
            node.idNode = funnelNode.getNodeId();
            node.type = funnelNode.getType();
            node.name = funnelNode.getOriginalName();
            node.params = funnelNode.getParams();
            node.visible = $(this).is(':visible');

            funnelNode.recordCurrentPosition();
            node.percentPosX = node.visible ? funnelNode.getPercentPosX() : 0;
            node.percentPosY = node.visible ? funnelNode.getPercentPosY() : 0;

            aNodes.push(node);
        }
    });

    return aNodes;
}

/**
 *
 * @returns {undefined}
 */
function getFunnelConnections()
{
    var aConnections = [];

    var aPlumberConnections = window.plumber.getAllConnections();

    if( aPlumberConnections && aPlumberConnections instanceof Array )
    {
        var rotatorsPercentageStartEnd = {};

        for( var i=0; i<aPlumberConnections.length; i++ )
        {
            var plumberConnection = aPlumberConnections[i];

            var sourceNode = plumberConnection.source.funnelNode;
            var targetNode = plumberConnection.target.funnelNode;
            var funnelElementData = plumberConnection.funnelElementData;

            var labelElem = plumberConnection.getOverlay("label");
            var labelLocation = labelElem.getLocation();


//            console.log('---------------------------------------');
//            console.log('source = ' + sourceNode.getNodeId());
//            console.log('target = ' + targetNode.getNodeId());

            var connection = {};
            connection.idSourceNode = sourceNode.getNodeId();
            connection.idTargetNode = targetNode.getNodeId();

            connection.elementData = {};
            connection.elementData.dataType = funnelElementData.getDataType();
            connection.elementData.data = funnelElementData.getData();
            connection.elementData.data.labelLocation = labelLocation;
            connection.elementData.bDirty = funnelElementData.isDirty();


            switch( sourceNode.getType() )
            {
                case NODE_TYPE.ROOT:
                case NODE_TYPE.ROTATOR:
                {
                    var rotationPercentage = funnelElementData.getData()['percentageValue'];

                    if( !rotatorsPercentageStartEnd[sourceNode.getNodeId()] )
                    {
                        rotatorsPercentageStartEnd[sourceNode.getNodeId()] = new Object();
                        rotatorsPercentageStartEnd[sourceNode.getNodeId()].curStart = 0;
                    }

                    var rangeStart = rotatorsPercentageStartEnd[sourceNode.getNodeId()].curStart;
                    var rangeEnd = rangeStart + rotationPercentage;

                    connection.param1Int = rangeStart;
                    connection.param2Int = rangeEnd;

                    rotatorsPercentageStartEnd[sourceNode.getNodeId()].curStart = rangeEnd;
                }
                break;

                case NODE_TYPE.EXTERNAL_URL:
                case NODE_TYPE.FLUX_LANDER:
                case NODE_TYPE.FLUX_OFFER:
                {
                    var action = funnelElementData.getData()['selectedOption'].replace(CampaignFunnelConnection.ON_ACTION_PREFIX, '');
                    connection.param1Int = action;
                }
                break;

                case NODE_TYPE.CONDITION:
                {
                    connection.param1Int = funnelElementData.getData()['selectedOption'].indexOf('Yes') != -1 ? 1 : 0;
                }
                break;
            }

            aConnections.push(connection);
        }
    }

    return aConnections;
}


/**
 *
 * @param {type} campaignId
 * @param {type} aNodes
 * @param {type} onComplete
 * @param {type} bNotEditable - optional. If set to true, then funnel diagram will not be editable
 * @returns {undefined}
 */
function loadFunnelNodes(campaignId, aNodes, onComplete, bNotEditable)
{
    if( aNodes instanceof Array )
    {
        window.plumber.bNotEditable = bNotEditable ? true : false;

        var width  = $('#funnelDiagramContainer').width();
        var height = $('#funnelDiagramContainer').height();

        var nbNodes = aNodes.length;
        var nbLoadedNodes = 0;

        if( nbNodes == 0 )
        {
            if( onComplete )
                onComplete();
        }

        plumbFunnelDoWhileSuspended(function()
            {
                window.plumber.idCampaign = campaignId;

                for(var i=0; i<aNodes.length; i++)
                {
                    var node = aNodes[i];

                    window.plumber.idFunnel = node['idFunnel'];
                    var idNode = node['idNode'];
                    var nodeType = node['nodeType'];
                    var nodeName = node['nodeName'];
                    var percentPosX = node['percentPosX'];
                    var percentPosY = node['percentPosY'];
                    var archived = node['archived'];
                    var aNodeParams = node['nodeParams'] ? unserialize(node['nodeParams']) : null;

                    var cssX = parseInt( percentPosX * width, 10 ) + 'px';
                    var cssY = parseInt( percentPosY * height, 10 ) + 'px';

                    var params = {};
                    params.idNode = idNode;
                    params.nodeType = nodeType;
                    params.nodeName = nodeName;
                    params.archived = archived;
                    params.x = cssX;
                    params.y = cssY;

                    switch( nodeType )
                    {
                        case NODE_TYPE.EXTERNAL_URL:
                        {
                            params.url = aNodeParams ? aNodeParams['pageUrl'] : null;
                        }
                        break;

                        default:
                        {
                            if( aNodeParams )
                            {
                                for(key in aNodeParams)
                                {
                                    params[key] = aNodeParams[key];
                                }
                            }
                        }
                        break;
                    }

                    addNode(params, function()
                        {
                            nbLoadedNodes++;

                            if( nbLoadedNodes == nbNodes )
                            {
                                onInitialLoadedNode();

                                if( onComplete )
                                {
                                    onComplete();
                                }
                            }
                        }, false);
                }
            });
    }
    else
    {
        if( onComplete )
            onComplete();
    }
}

/**
 *
 * @returns {undefined}
 */
function loadFunnelConnections(aConnections)
{
    if( aConnections instanceof Array )
    {
        plumbFunnelDoWhileSuspended(function()
            {
                for(var i=0; i<aConnections.length; i++)
                {
                    var connection = aConnections[i];

                    $sourceNodeDiv = $('[idnode="'+connection['idSourceNode']+'"]');
                    $targetNodeDiv = $('[idnode="'+connection['idTargetNode']+'"]');

                    if( $sourceNodeDiv.length > 0 && $targetNodeDiv.length > 0 )
                    {
                        var plumbConnection = window.plumber.connect({
                            source: $sourceNodeDiv,
                            target: $targetNodeDiv
                        });

                        if( plumbConnection )
                        {
                            var labelElement = plumbConnection.getOverlay("label");
                            var $el = $(labelElement.canvas);
                            plumbConnection.funnelElementData = new FunnelElementData($el, connection.elementData.dataType, connection.elementData.data, connection.elementData.bDirty);

                            refreshLabels(plumbConnection.source);
                        }
                    }
                }
            });
    }
}

/**
 *
 * @param {type} fromJSPlumbNode
 * @returns {undefined}
 */
function refreshNode(fromJSPlumbNode)
{
    if( fromJSPlumbNode && fromJSPlumbNode.funnelNode )
    {
        var funnelNode = fromJSPlumbNode.funnelNode;
        funnelNode.refresh();
    }
}

/**
 *
 * @returns {undefined}
 */
function refreshAllNodes()
{
    var bOldDirty = window.dirtyManager.isDirty();
    
    $('#funnelDiagramContainer > .w').each(function()
    {
        var funnelNode = ($(this).get(0) && $(this).get(0).funnelNode) ? $(this).get(0).funnelNode : null;

        if(funnelNode)
        {
            funnelNode.refresh();
        }
    });

    if( !bOldDirty )
    {
        // If dirty was false before refreshing the nodes,
        // set it back to false as simply redrawing
        // should not trigger the dirty flag
        window.dirtyManager.setDirty(false);
    }

    window.plumber.repaintEverything();
}

/**
 *
 * @returns {undefined}
 */
function recordPercentPositionOfAllNodes()
{
    $('#funnelDiagramContainer > .w').each(function()
    {
        var funnelNode = ($(this).get(0) && $(this).get(0).funnelNode) ? $(this).get(0).funnelNode : null;

        if(funnelNode)
        {
            funnelNode.recordCurrentPosition();
        }
    });
}


/**
 *
 * @returns {undefined}
 */
function getContentHeight()
{
    var maxY = 0;

    var containerRect = document.getElementById('funnelDiagramContainer').getBoundingClientRect();

    $('#funnelDiagramContainer').children('.w').each(function(index)
    {
        var el = document.getElementById($(this).attr('id'));

        if( el )
        {
            var rect = el.getBoundingClientRect();

            if( rect.height !== 0 )
            {
                var rectBottom = rect.top - containerRect.top + rect.height;

                if( rectBottom > maxY )
                {
                    maxY = rectBottom;
                }
            }
        }
    });

    return maxY;
}

/**
 *
 * @param {type} selector
 * @param {type} bInstant
 * @param {type} onComplete
 * @param {type} additionalHeight
 * @returns {undefined}
 */
function autoSizeDiagramContainer(selector, bInstant, onComplete, additionalHeight)
{
    var height = getContentHeight() + (additionalHeight ? additionalHeight : 20);

    $container = $(selector);

    if( bInstant )
    {
        $container.height(height);

        recordPercentPositionOfAllNodes();

        if( onComplete )
            onComplete();
    }
    else
    {
        $container.stop().animate({'height':height}, 200,
            function()
            {
                recordPercentPositionOfAllNodes();

                if( onComplete )
                    onComplete();
            });
    }
}


/**
 * @param fromSource
 * @returns {undefined}
 */
function refreshLabels(fromSource)
{
    if( fromSource && fromSource.funnelNode )
    {
        var connections = window.plumber.getConnections({ source: fromSource });

        if( connections && connections.length > 0 )
        {
            var aLabelLocations = [0.35, 0.5, 0.65];

            for(var i = 0 ; i < connections.length ; i++ )
            {
                var connection = connections[i];

                if( connection.funnelElementData
                        && connection.funnelElementData.getData()['labelLocation'] !== undefined )
                {
                    var labelElem = connection.getOverlay("label");
                    labelElem.setLocation( connection.funnelElementData.getData()['labelLocation'] );
                }
            }

            switch( fromSource.funnelNode.getType() )
            {
                case NODE_TYPE.ROOT:
                case NODE_TYPE.ROTATOR:
                {
                    // Find assigned rotation percentages

                    var nonAssigned = 0;

                    if( !window.assignedPercentages )
                        window.assignedPercentages = {};

                    var id = $(fromSource).attr('id');
                    window.assignedPercentages[id] = 0;

                    for(var i = 0 ; i < connections.length ; i++ )
                    {
                        var connection = connections[i];

                        if( connection.funnelElementData && connection.funnelElementData.isDirty() )
                        {
                            window.assignedPercentages[id] += parseInt(connection.funnelElementData.getData()['percentageValue'], 10);
                        }
                        else
                        {
                            nonAssigned++;
                        }
                    }


                    // Split remaining percentages between non-assigned elements

                    if( nonAssigned > 0 )
                    {
                        var remainingAvailablePercentage = Math.max( 100 - window.assignedPercentages[id], 0 );
                        var percentSplit = remainingAvailablePercentage / nonAssigned;

                        for(var i = 0 ; i < connections.length ; i++ )
                        {
                            var connection = connections[i];

                            var data = {};
                            if( connection.funnelElementData && connection.funnelElementData.getData() )
                            {
                                // Deep copy of existing data
                                data = jQuery.extend(true, {}, connection.funnelElementData.getData());
                            }
                            data.percentageValue = percentSplit;

                            if( !connection.funnelElementData )
                            {
                                var $el = $(connection.getOverlay("label").canvas);
                                connection.funnelElementData = new FunnelElementData($el, ELEMENT_DATA_TYPE.PERCENTAGE_0_100, data);
                            }
                            else if( !connection.funnelElementData.isDirty() )
                            {
                                connection.funnelElementData.setData(data);
                            }
                        }
                    }


                    // Display percentages and if all have been modified by user, check that total is 100%

                    var bMissingAssignments = (nonAssigned == 0 && window.assignedPercentages[id] < 100);

                    for(var i = 0 ; i < connections.length ; i++ )
                    {
                        var connection = connections[i];
                        var $el = $(connection.getOverlay("label").canvas);

                        if( bMissingAssignments )
                        {
                            $el.removeClass('aLabel');
                            $el.addClass('aLabelError');
                        }
                        else
                        {
                            $el.addClass('aLabel');
                            $el.removeClass('aLabelError');
                        }

                        $el.addClass("connection-rotator-percent");

                        var percentage = parseFloat(connection.funnelElementData.getData()['percentageValue']).toFixed(1);
                        var minFontSize = 12; //11;
                        var maxFontSize = 12; //17;
                        var fontSize = parseInt((maxFontSize - minFontSize) * percentage / 100, 10) + minFontSize;

                        var prefix = connection.funnelElementData.isDirty() ? '<b>' : '';
                        var suffix = connection.funnelElementData.isDirty() ? '*</b>' : '';

                        var labelElem = connection.getOverlay("label");
                        $(labelElem.canvas).css('font-size', fontSize + 'px');
                        labelElem.setLabel(prefix + percentage + "%" + suffix);

                        if( connection.funnelElementData.getData()['labelLocation'] === undefined )
                        {
                            var location = aLabelLocations[i % aLabelLocations.length];
                            labelElem.setLocation( location );
                            connection.funnelElementData.getData()['labelLocation'] = location;
                        }
                    }
                }
                break;

                case NODE_TYPE.EXTERNAL_URL:
                case NODE_TYPE.FLUX_LANDER:
                case NODE_TYPE.FLUX_OFFER:
                {
                    var aAllChoices = [];
                    var aUsedChoices = [];
                    var aAvailableChoices = [];

                    for(var i = 0; i < fromSource.funnelNode.getMaxExitConnections(); i++ )
                    {
                        aAllChoices.push( CampaignFunnelConnection.ON_ACTION_PREFIX + (i+1) );
                    }

                    aAvailableChoices = aAllChoices.slice(0);   // Duplicate array

                    for(var i = 0 ; i < connections.length ; i++ )
                    {
                        var connection = connections[i];

                        if( connection.funnelElementData )
                        {
                            var usedOption = connection.funnelElementData.getData()['selectedOption'];
                            aUsedChoices.push( usedOption );

                            var j = aAvailableChoices.indexOf(usedOption);
                            if( j != -1 )
                                aAvailableChoices.splice(j, 1); // Remove from available actions
                        }
                    }

                    for(var i = 0 ; i < connections.length ; i++ )
                    {
                        var connection = connections[i];
                        var $el = $(connection.getOverlay("label").canvas);
                        $el.addClass("connection-action");

                        if( !connection.funnelElementData )
                        {
                            var data = {
                                aOptions: aAllChoices,
                                selectedOption: aAvailableChoices[0],
                                oCheckboxes: {}
                            };

                            if( fromSource.funnelNode.getType() == NODE_TYPE.FLUX_OFFER )
                            {
                                data.oCheckboxes[CampaignFunnelConnection.ELEMENT_DATA_CHECKBOXES_IS_CONVERSION] = {'title': 'This is a Conversion', 'checked': 0};
                            }

                            aAvailableChoices.splice(0, 1);
                            connection.funnelElementData = new FunnelElementData($el, ELEMENT_DATA_TYPE.COMBO_BOX, data);
                        }

                        var selectedOption = connection.funnelElementData.getData()['selectedOption'];

                        var labelElem = connection.getOverlay("label");
                        labelElem.setLabel( selectedOption );

                        if( connection.funnelElementData.getData()['labelLocation'] === undefined )
                        {
                            var location = aLabelLocations[i % aLabelLocations.length];
                            labelElem.setLocation( location );
                            connection.funnelElementData.getData()['labelLocation'] = location;
                        }

                        if( fromSource.funnelNode.getParams()["actionsSourceURLs"] )
                        {
                            var actionIndex = aAllChoices.indexOf(selectedOption);

                            if( actionIndex != -1 )
                            {
                                var actionSourceURL = fromSource.funnelNode.getParams()["actionsSourceURLs"][actionIndex];
                                $(labelElem.canvas).attr('title', actionSourceURL);
                            }
                        }

                        var oCheckboxes = connection.funnelElementData.getData()['oCheckboxes'];

                        if( oCheckboxes )
                        {
                            var oIsConversion = oCheckboxes[CampaignFunnelConnection.ELEMENT_DATA_CHECKBOXES_IS_CONVERSION];

                            if( oIsConversion && oIsConversion['checked'] )
                            {
                                labelElem.setLabel( '<div align="center">' + selectedOption + '<br/>Conversion</div>' );
                            }
                        }
                    }
                }
                break;

                case NODE_TYPE.JS_CODE_EXECUTOR:
                case NODE_TYPE.PHP_CODE_EXECUTOR:
                {
                    var aAllChoices = [];
                    var aUsedChoices = [];
                    var aAvailableChoices = [];

                    for(var i = 0; i < fromSource.funnelNode.getMaxExitConnections(); i++ )
                    {
                        aAllChoices.push( CampaignFunnelConnection.ON_DONE_PREFIX + (i+1) );
                    }

                    aAvailableChoices = aAllChoices.slice(0);   // Duplicate array

                    for(var i = 0 ; i < connections.length ; i++ )
                    {
                        var connection = connections[i];

                        if( connection.funnelElementData )
                        {
                            var usedOption = connection.funnelElementData.getData()['selectedOption'];
                            
                            // No selectedOption data, means old JS/PHP Node connection
                            if(!usedOption)
                            {
                                var data = {
                                    aOptions: aAllChoices,
                                    selectedOption: aAvailableChoices[0],
                                    oCheckboxes: {}
                                };

                                usedOption = aAvailableChoices[0];
                                connection.funnelElementData = new FunnelElementData($el, ELEMENT_DATA_TYPE.COMBO_BOX, data);
                            }

                            aUsedChoices.push( usedOption );

                            var j = aAvailableChoices.indexOf(usedOption);
                            if( j != -1 )
                                aAvailableChoices.splice(j, 1); // Remove from available actions
                        }
                    }

                    /*########################################################*/
                    /*########################################################*/
                    /*########################################################*/

                    for(var i = 0 ; i < connections.length ; i++ )
                    {
                        var connection = connections[i];
                        var $el = $(connection.getOverlay("label").canvas);
                        $el.addClass("connection-code-done");

                        if( !connection.funnelElementData )
                        {
                            var data = {
                                aOptions: aAllChoices,
                                selectedOption: aAvailableChoices[0],
                                oCheckboxes: {}
                            };

                            aAvailableChoices.splice(0, 1);
                            connection.funnelElementData = new FunnelElementData($el, ELEMENT_DATA_TYPE.COMBO_BOX, data);
                        }

                        var selectedOption = connection.funnelElementData.getData()['selectedOption'];

                        var labelElem = connection.getOverlay("label");
                        labelElem.setLabel( selectedOption );

                        if( connection.funnelElementData.getData()['labelLocation'] === undefined )
                        {
                            var location = aLabelLocations[i % aLabelLocations.length];
                            labelElem.setLocation( location );
                            connection.funnelElementData.getData()['labelLocation'] = location;
                        }

                        // var labelElem = connection.getOverlay("label");
                        // labelElem.setLabel( CodeSnippet.ON_DONE_PREFIX + (i+1) );
                    }
                }
                break;

                case NODE_TYPE.CONDITION:
                {
                    var aAllChoices = [];
                    var aUsedChoices = [];
                    var aAvailableChoices = [];

                    aAllChoices.push( Condition.CHOICE_YES );
                    aAllChoices.push( Condition.CHOICE_NO );

                    aAvailableChoices = aAllChoices.slice(0);   // Duplicate array

                    for(var i = 0 ; i < connections.length ; i++ )
                    {
                        var connection = connections[i];

                        if( connection.funnelElementData )
                        {
                            var usedOption = connection.funnelElementData.getData()['selectedOption'];
                            aUsedChoices.push( usedOption );

                            var j = aAvailableChoices.indexOf(usedOption);
                            if( j != -1 )
                                aAvailableChoices.splice(j, 1); // Remove from available actions
                        }
                    }

                    for(var i = 0 ; i < connections.length ; i++ )
                    {
                        var connection = connections[i];
                        var $el = $(connection.getOverlay("label").canvas);

                        if( !connection.funnelElementData )
                        {
                            var data = {
                                aOptions: aAllChoices,
                                selectedOption: aAvailableChoices[0],
                                oCheckboxes: {}
                            };

                            aAvailableChoices.splice(0, 1);
                            connection.funnelElementData = new FunnelElementData($el, ELEMENT_DATA_TYPE.COMBO_BOX, data);
                        }

                        var selectedOption = connection.funnelElementData.getData()['selectedOption'];

                        if( selectedOption.indexOf('Yes') >= 0 )
                        {
                            //$el.css('background-color', '#80D0A0');
                            //$el.css('color', '#b1ffb3');
                            $el.addClass('connection-condition-yes');
                        }
                        else
                        {
                            //$el.css('background-color', '#D07090');
                            //$el.css('color', '#D07090');
                            $el.addClass('connection-condition-no');
                        }

                        var labelElem = connection.getOverlay("label");
                        labelElem.setLabel( selectedOption );

                        if( connection.funnelElementData.getData()['labelLocation'] === undefined )
                        {
                            var location = aLabelLocations[i % aLabelLocations.length];
                            labelElem.setLocation( location );
                            connection.funnelElementData.getData()['labelLocation'] = location;
                        }
                    }
                }
                break;
            }
        }
    }
}

/**
 *
 * @param {type} element
 * @returns {undefined}
 */
function editElement(element)
{
    if( element instanceof FunnelNode )
    {
        element.edit();
    }
    else if( element instanceof jsPlumb.Overlays.Label )
    {
        var connection = element.component;

        if( connection.funnelElementData )
        {
            var id = $(connection.source).attr('id');
            var params = {};

            switch( connection.source.funnelNode.getType() )
            {
                case NODE_TYPE.ROOT:
                case NODE_TYPE.ROTATOR:
                {
                    if( window.assignedPercentages[id] )
                        params.maxAdditionalPercentage = Math.max( 100 - window.assignedPercentages[id], 0 );

                    connection.funnelElementData.editData(
                        params,
                        function()
                        {
                            refreshNode(connection.source);
                            refreshNode(connection.target);
                            refreshLabels(connection.source);
                        });
                }
                break;

                case NODE_TYPE.JS_CODE_EXECUTOR:
                case NODE_TYPE.PHP_CODE_EXECUTOR:
                case NODE_TYPE.EXTERNAL_URL:
                case NODE_TYPE.FLUX_LANDER:
                case NODE_TYPE.FLUX_OFFER:
                case NODE_TYPE.CONDITION:
                {
                    var oldAction = connection.funnelElementData.getData()['selectedOption'];

                    connection.funnelElementData.editData(
                        params,
                        function()
                        {
                            var newAction = connection.funnelElementData.getData()['selectedOption'];

                            // Find if there was another element using the new selected action

                            var connections = window.plumber.getConnections({ source: connection.source });

                            for(var i=0; i<connections.length; i++)
                            {
                                var otherConnection = connections[i];

                                if( otherConnection !== connection && otherConnection.funnelElementData )
                                {
                                    var otherData = otherConnection.funnelElementData.getData();

                                    if( otherData['selectedOption'] == newAction )
                                    {
                                        otherData['selectedOption'] = oldAction;
                                        otherConnection.funnelElementData.setData(otherData);
                                        break;
                                    }
                                }
                            }


                            refreshNode(connection.source);
                            refreshNode(connection.target);
                            refreshLabels(connection.source);
                        });
                }
                break;
            }
        }
    }
}

/**
 *
 * @param {type} aConnections
 * @returns {undefined} */
function deleteConnections(aConnections)
{
    if( aConnections instanceof Array )
    {
        for(var i=0; i<aConnections.length; i++)
        {
            deletePlumbElement(aConnections[i]);
        }
    }
}

/**
 *
 * @param {type} elem
 * @returns {undefined} */
function deletePlumbElement(elem)
{
    if( elem instanceof jsPlumb.Connection )
    {
        var connection = elem;

        var source = connection.source;
        window.plumber.detach(connection);

        refreshNode(source);
        refreshLabels(source);
    }
    else
    {
        deleteNode( elem.get(0) );
    }

    if( !window.plumber.bNotEditable )
        window.dirtyManager.setDirty(true);
}


/**
 * We assign contextMenus on the container and on the nodes.
 * The problem is that if we right click on a node, the contextMenu that will
 * open is the one from the container (because the mouse is indeed above the container,
 * eventhough it is also above a node). To fix this problem, we check the mouse position
 * against the nodes that are inside the container. If the mouse is above a node, we
 * deactivate the container's menu. We reactivate it when the mouse leaves the node.
 * @returns {undefined}
 */
function funnelMouseMove()
{
    if( window.plumber && !window.plumber.bNotEditable )
    {
//        var bInContainer = $('#funnelDiagramContainer').is(":hover");
//        var abInChildOfContainer = $('.w, ._jsPlumb_connector, ._jsPlumb_overlay').filter(function(){
//            return $(this).is(":hover");
//        });
//
//        var bInChildOfContainer = false;
//        for(var i=0; i<abInChildOfContainer.length; i++)
//        {
//            if( abInChildOfContainer[i] )
//            {
//                bInChildOfContainer = true;
//                break;
//            }
//        }
//
//        if( !bInChildOfContainer && $('.w.dragHover').length )
//        {
//            bInChildOfContainer = true;
//        }
//
//        if( window.bMainContextMenuOpened )
//        {
//            if( bInContainer && bInChildOfContainer && window.bMainContextMenuAdded )
//            {
//                console.log("destroy");
//                $('#funnelDiagramContainer').contextMenu('destroy');
//                window.bMainContextMenuAdded = false;
//                window.bMainContextMenuOpened = false;
//            }
//        }
//        else
//        {
//            if( bInContainer && !bInChildOfContainer )
//            {
//                if( !window.bMainContextMenuAdded )
//                {
//                    window.bMainContextMenuAdded = true;
//
//                    $('#funnelDiagramContainer').contextMenu('menu', aFunnelContextMenuAddElements, {
//                        triggerOn: 'click',
//                        mouseClick: 'right',
//                        onOpen: function(data, event)
//                        {
//                            console.log("opened");
//                            window.bMainContextMenuOpened = true;
//
//                            var posContainer = $('#funnelDiagramContainer').position();
//                            window.mainContextMenuX = window.mouseX - posContainer.left;
//                            window.mainContextMenuY = window.mouseY - posContainer.top;
//                        },
//                        onClose: function(data, event)
//                        {
//                            console.log("closed");
//                            window.bMainContextMenuOpened = false;
//                        }
//                    });
//                }
//            }
//            else if( window.bMainContextMenuAdded )
//            {
//                console.log("destroy");
//                $('#funnelDiagramContainer').contextMenu('destroy');
//                window.bMainContextMenuAdded = false;
//                window.bMainContextMenuOpened = false;
//            }
//        }

        //Manage autosize in relation drag element
        if( $('.w.jsPlumb_dragged').length )
        {
            var heightComponent = $('.w.jsPlumb_dragged').position();
            var heightContainer = getContentHeight();
            var diffHeight = heightContainer - heightComponent.top;
            if (diffHeight < FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT)
            {
                autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
            }
        }
    }
}

jsPlumb.ready(function()
{
    jsPlumbInit();
});

/**
 *
 */
function jsPlumbInit()
{
    var instance = jsPlumb.getInstance("funnelDiagramContainer");
    jsPlumb.makeLabelsDraggable(instance);

    window.plumber = instance;

    instance.importDefaults({
        Anchor : "Center",
        Anchors : [ "AutoDefault", "AutoDefault" ],  // Source, Target
        ConnectionsDetachable   : true,
        ConnectionOverlays  : [
                                [ "Arrow", {
                                    location:1,
                                    id:"arrow",
                                    length:7,
                                    foldback:0.6,
                                    paintStyle: { strokeStyle : "transparent", fillStyle: "#acbfc3" }
                                } ],
                                [ "Label", { location: 0.3, label:"", id:"label", cssClass:"aLabel" }]
                            ],
        Connector : ["Bezier", {curviness: 50}],
        //Connector:[ "Straight" ],
        DoNotThrowErrors  : false,
        DragOptions : { cursor: 'pointer', zIndex:2000 },
        DropOptions : { },
        Endpoint : "Dot",
        Endpoints : [ "Dot", "Blank" ],
        EndpointOverlays : [ ],
        EndpointStyle : { fillStyle : "#4c5355", radius: 2 },
        EndpointStyles : [ null, null ],
        EndpointHoverStyle : null,
        EndpointHoverStyles : [ null, null ],
        HoverPaintStyle : null,
        LabelStyle : { color : "black" },
        LogEnabled : true,
        Overlays : [],
        PaintStyle : { lineWidth : 2, strokeStyle : "#4c5355" },
        //connectorStyle:{ strokeStyle:"#00ff00", lineWidth:2, outlineColor:"transparent", outlineWidth:4 },
        ReattachConnections : false,
        RenderMode : "svg",
        Scope : "jsPlumb_DefaultScope"
    });

    var linkableSrcElements = jsPlumb.getSelector("#funnelDiagramContainer .w");
    var draggableElements = linkableSrcElements;

    instance.draggable( draggableElements, {containment:true} );


    /**
     * The "dblclick" event happens when a double click happens on a jsPlumb element.
     */
    instance.bind("dblclick", function(element)
        {
            if( !window.plumber.bNotEditable )
            {
                editElement(element);
            }
        });


    /**
     * The "connection" event happens when a new connection has been established between 2 nodes
     */
    instance.bind("connection", function(info)
        {
            if( !window.plumber.bNotEditable )
                window.dirtyManager.setDirty(true);

            refreshNode(info.connection.source);
            refreshNode(info.connection.target);
            refreshLabels(info.connection.source);

            if( !window.plumber.bNotEditable )
            {
                // Setup context menu on connection line

                $(info.connection.canvas).contextMenu('menu', aFunnelContextMenuConnections, {
                    triggerOn: 'click',
                    mouseClick: 'right',
                    onOpen: function(data, event)
                    {
                        $('#funnelDiagramContainer').get(0).elementUnderContextMenu = info.connection;
                    }
                });


                // Setup context menu on connection's label

                var sourceNode = info.connection.source.funnelNode;

                if( sourceNode )
                {
                    var aMenu = null;

                    switch( sourceNode.getType() )
                    {
                        case NODE_TYPE.FLUX_LANDER:
                        case NODE_TYPE.FLUX_OFFER:
                        case NODE_TYPE.EXTERNAL_URL:
                        {
                            aMenu = aFunnelContextMenuConnectionLabelAction;
                        }
                        break;

                        default:
                        {
                            aMenu = aFunnelContextMenuConnections;
                        }
                        break;
                    }

                    if( aMenu )
                    {
                        var $label = $(info.connection.getOverlay("label").canvas);

                        $label.contextMenu('menu', aMenu, {
                            triggerOn: 'click',
                            mouseClick: 'right',
                            onOpen: function(data, event)
                            {
                                $('#funnelDiagramContainer').get(0).elementUnderContextMenu = info.connection;
                            }
                        });
                    }
                }
            }
        });


    /**
     * The "beforeDrop" event happens just before a new connection gets established between 2 nodes
     * If false or undefined is returned, then the connection is cancelled
     */
    instance.bind("beforeDrop", function(info)
        {
            if( info.connection.target.funnelNode )
            {
                switch( info.connection.target.funnelNode.getType() )
                {
                    case NODE_TYPE.ROOT:
                    {
                        return false;
                    }

                    case NODE_TYPE.EXTERNAL_URL:
                    case NODE_TYPE.FLUX_LANDER:
                    case NODE_TYPE.FLUX_OFFER:
                    {
                        // EDIT: We now allow connecting a node to another one multiple times
                        // because we may want to have different "on actions" go to the same node.
//                        // Check that target is not already connected by that same source
//
//                        var sources = instance.getConnections({ target: info.connection.target });
//
//                        for( var i = 0 ; i < sources.length ; i++ )
//                        {
//                            var existingSource = sources[i].source;
//
//                            if( existingSource === info.connection.source )
//                            {
//                                // This source is already connected to this target.
//                                // Abort this duplicate connection attempt.
//                                return false;
//                            }
//                        }
                    }
                    break;
                }
            }

            // Check exit connections count

            var sourceNode = info.connection.source.funnelNode;
            var targets = instance.getConnections({ source: info.connection.source });

            if( targets.length >= sourceNode.getMaxExitConnections() )
            {
                // Max exit connections from this source has already been reached

                var msg = "";
                var subMsg = "";

                switch( sourceNode.getType() )
                {
                    case NODE_TYPE.FLUX_LANDER:
                    case NODE_TYPE.FLUX_OFFER:
                        msg = "The page '" + sourceNode.getOriginalName() + "' cannot have more than " + sourceNode.getMaxExitConnections() + " exit connections.";
                        subMsg = "Consider editing that page if you need more...";
                        break;

                    default:
                        subMsg = "The maximum number of exit connections from this node has been reached";
                        break;
                }

                modalShowMessage("Limit Reached", msg, subMsg);
                return false;
            }

            // Check entry connections count

            var targetNode = info.connection.target.funnelNode;
            var sources = instance.getConnections({ target: info.connection.target });

            if( sources.length >= targetNode.getMaxEntryConnections() )
            {
                // Max entry connections to this target has already been reached

                var msg = "";
                var subMsg = "The maximum number of entry connections to this node has been reached";

                modalShowMessage("Limit Reached", msg, subMsg);
                return false;
            }

            return true;
        });

    $(window).resize(function(){
        refreshAllNodes();
    });

    window.dirtyManager.setDirty(false);
}

function onInitialLoadedNode(nodeDivElement)
{
    window.dirtyManager.setDirty(false);
}
