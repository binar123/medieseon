
var ELEMENT_DATA_TYPE = Object.freeze(
                {
                    PERCENTAGE_0_100: 0,    // elementData: 'percentageValue'
                    COMBO_BOX: 1,           // elementData: * 'aOptions' - an array with the available options
                                            //              * 'selectedOption' - The selected option (the value MUST be available in aOptions[])
                                            //              * optional: 'oCheckboxes' - if set, then it has to be an object of objects
                                            //                as in the example below:
                                            //                      {
                                            //                          'isConversion': {'title': 'Is it a conversion?', 'checked': 0},
                                            //                          'bReceiveURLParams': {'title': 'Receive URL Params?', 'checked': 1},
                                            //                          etc...
                                            //                      }

                    NONE: 99999
                });

/**
 * 
 * @param {type} $el  - Jquery element on which this data is going to be attached
 * @param {type} type - One of the CONNECTION_DATA_TYPE values
 * @param {type} elementData  - A data object whose content depends on type
 * @param {type} dirty - OPTIONAL - Used during loading
 * 
 * @returns {undefined}
 */
var FunnelElementData = function($el, type, elementData, dirty)
{
    var $element = $el;
    var dataType = type;
    var data = elementData;
    var bDirty = dirty !== 'undefined' ? dirty : false;

    this.getDataType = function() { return dataType; };
    this.getData = function() { return data; };
    this.setData = function(newData) { data = newData; };
    this.isDirty = function() { return bDirty; };

    /**
     * Opens an editor to change the data
     * 
     * @param {type} params - [OPTIONAL] with:
     *                  params.maxAdditionalPercentage - Maximum percentage we can add to the edited value
     * @param {type} onEdited - [OPTIONAL] callback called when edit is completed
     * 
     * @returns 
     */
    this.editData = function(params, onEdited)
    {
        switch( dataType )
        {
            case ELEMENT_DATA_TYPE.PERCENTAGE_0_100:
                this._editDataPercentage0_100(params, onEdited);
                break;
                
            case ELEMENT_DATA_TYPE.COMBO_BOX:
                this._editDataCombobox(params, onEdited);
                break;
        }        
    };

    /**
     * 
     * @param {type} params
     * @param {type} onEdited
     * @returns {undefined}
     */
    this._editDataCombobox= function(params, onEdited)
    {
        window.editData = {instance: this, onEdited: onEdited};
        
        var htmlContent = '';
        htmlContent += '<div class="panel" style="width: 450px;"><div class="row"><div class="large-12 columns">';
        htmlContent += '  <select id="funnel-tools-combobox">';
        
        if( this.getData() && this.getData()['aOptions'] && this.getData()['aOptions'] instanceof Array )
        {
            for(var i = 0; i < this.getData()['aOptions'].length; i++ )
            {
                var value = this.getData()['aOptions'][i];
                var selected = (value == this.getData()['selectedOption']) ? 'selected' : '';
                
                htmlContent += '    <option '+selected+' value="'+(i+1)+'">'+value+'</option>';
            }
        }        
        htmlContent += '  </select>';
        htmlContent += '</div><div id="oCheckboxes" class="large-12 columns"><br/>';
        
        if( this.getData() && this.getData()['oCheckboxes'] )
        {
            for(key in this.getData()['oCheckboxes'])
            {
                var params = this.getData()['oCheckboxes'][key];
                var title = params['title'];
                var bChecked = params['checked'];
                
                if( title !== undefined && bChecked !== undefined )
                {
                    var checked = bChecked ? 'checked' : '';

                    htmlContent += '<div class="switch tiny">';
                    htmlContent +=     '<input id="'+key+'" type="checkbox" '+checked+'/>';
                    htmlContent +=     '<label for="'+key+'"></label>';
                    htmlContent +=     '<span style="font-size: 85%; position:relative; top:-8px; left: 10px;">'+title+'</span>';
                    htmlContent += '</div>';
                }
            }
        }

        htmlContent += '</div></div></div>';

        modalShowContent({
            html: htmlContent,
            //x: parseInt($element.offset().left, 10) - $(document).scrollLeft(),
            //y: parseInt($element.offset().top, 10) - $(document).scrollTop(),
            button1Text: 'OK',
            button2Text: 'Cancel',
            onOpen: function()
            {
                $("#funnel-tools-combobox").chosen({
                    no_results_text: "Oops, nothing found!", 
                    search_contains: true,
                    width: '100%'
                }); 
            },
            button1Callback: function()
            {
                window.editData['instance']._editDataCombobox_validate();
            }
        });
    };

    this._editDataCombobox_validate = function()
    {
        var newData = window.editData['instance'].getData();

        //if( newData['selectedOption'] != $('#funnel-tools-combobox option:selected').text() )
        {
            $('#oCheckboxes').find('input').each(function()
            {
                var id = $(this).attr('id');
                var checked = $(this).is(':checked');
                
                if( newData['oCheckboxes'] && newData['oCheckboxes'][id] )
                {
                    newData['oCheckboxes'][id].checked = checked;
                }
            });

            newData['selectedOption'] = $('#funnel-tools-combobox option:selected').text();
            window.editData['instance'].setData(newData);
            bDirty = true;

            if( window.editData['onEdited'] )
            {
                window.editData['onEdited']();
            }

            if( !window.plumber.bNotEditable )
            {
                window.dirtyManager.setDirty(true);
            }
        }
    };

    /**
     * 
     * @param {type} params
     * @param {type} window.editDataOnEdited
     * @returns {undefined}
     */
    this._editDataPercentage0_100 = function(params, onEdited)
    {
        window.editData = {instance: this, onEdited: onEdited};

        modalShowContent({
            html: '<div class="panel" style="width: 300px;"><div class="row"><div class="large-12 columns" id="funnel-tools-rotator-percent-slider"></div></div></div>'/* +
                    '<br/><div class="row">' +
                    '<div class="small-2 push-4 columns"><a class="button small">Save</a></div>' +
                    '</div>'*/,
            //x: parseInt($element.offset().left, 10) - $(document).scrollLeft(),
            //y: parseInt($element.offset().top, 10) - $(document).scrollTop(),
            button1Text: 'OK',
            button2Text: 'Cancel',
            onOpen: function()
            {
                var min = 0;
                var max = (params && params.maxAdditionalPercentage != undefined) ? params.maxAdditionalPercentage : 100;
                max = Math.min( bDirty ? window.editData['instance'].getData()['percentageValue'] + max : max, 100 );
                var step = max > min ? 1 : 0;

                $("#funnel-tools-rotator-percent-slider").noUiSlider({
                        start: window.editData['instance'].getData()['percentageValue'],
                        connect: "lower",
                        step: step,
                        range: {
                                'min': min,
                                'max': max
                        }
                });          
                $("#funnel-tools-rotator-percent-slider").Link('lower').to('-inline-<div class="noUIslider-tooltip"></div>', function ( value ) {

                        // The tooltip HTML is 'this', so additional
                        // markup can be inserted here.
                        $(this).html(
                                '<strong>Value: </strong>' +
                                '<span>' + value + '</span>'
                        );
                });
            },
            button1Callback: function()
            {
                window.editData['instance']._editDataPercentage0_100_validate();
            }
        });
    };
    
    this._editDataPercentage0_100_validate = function()
    {
        var newData = window.editData['instance'].getData();

        if( newData['percentageValue'] != $('#funnel-tools-rotator-percent-slider').val() )
        {                    
            newData['percentageValue'] = parseInt($('#funnel-tools-rotator-percent-slider').val(), 10);
            window.editData['instance'].setData(newData);
            bDirty = true;

            if( window.editData['onEdited'] )
            {
                window.editData['onEdited']();
                
                if( !window.plumber.bNotEditable )
                    window.dirtyManager.setDirty(true);
            }
        }        
    };    
};

