
var NODE_BASE_NAME = '###NAME###';


/**
 *
 * @param {type} nodeParams object with:
 *                  nodeParams.nodeType - one of the NODE_TYPE values
 *                  nodeParams.divId - ID of this node's div
 *                  nodeParams.idNode - OPTIONAL - id of the node in the database. Normally set for loaded nodes.
 *                  nodeParams.maxEntryConnections - OPTIONAL - Maximum number of connections that can come into this node
 *                  nodeParams.maxExitConnections - OPTIONAL - Maximum number of connections that can go out from this node
 *
 * @returns {FunnelNode}
 */
var FunnelNode = function(nodeParams)
{
    var idNode = nodeParams.idNode ? nodeParams.idNode : (new Date().getTime());
    var type = nodeParams.nodeType;
    var name = nodeParams.nodeName;
    var divId = nodeParams.divId;
    var maxEntryConnections = nodeParams.maxEntryConnections ? nodeParams.maxEntryConnections : CampaignFunnelNode.MAX_NUMBER_OF_NODE_CONNECTIONS;
    var maxExitConnections = nodeParams.maxExitConnections ? nodeParams.maxExitConnections : CampaignFunnelNode.MAX_NUMBER_OF_NODE_CONNECTIONS;
    var bNegativeMaxExitConnections = false;
    var percentPosX = null;
    var percentPosY = null;
    var oParams = null;


    /**
     * Basic getters and setters
     */
    this.getNodeId = function() { return idNode; };
    this.getType = function() { return type; };
    this.getDivId = function() { return divId; };
    this.getName = function() { return name; };
    this.getMaxEntryConnections = function() { return maxEntryConnections; };
    this.setMaxEntryConnections = function(max) { maxEntryConnections = max; window.dirtyManager.setDirty(true); };
    this.getMaxExitConnections = function() { return maxExitConnections; };
    this.getParams = function() { return oParams; };
    this.getPercentPosX = function() { return percentPosX; };
    this.getPercentPosY = function() { return percentPosY; };

    this.setMaxExitConnections = function(max)
    {
        if(max>=0)
        {
            maxExitConnections = max;
        }
        else
        {
            bNegativeMaxExitConnections = true;
        }

        window.dirtyManager.setDirty(true);
    };


    /**
     *
     * @param {type} p
     * @returns {undefined}
     */
    this.setParams = function(p)
    {
        oParams = p;

        if( !window.plumber.bNotEditable )
            window.dirtyManager.setDirty(true);
    };

    /**
     *
     * @returns {undefined}
     */
    this.recordCurrentPosition = function()
    {
        var width  = parseInt( $('#funnelDiagramContainer').width(), 10 );
        var height = parseInt( $('#funnelDiagramContainer').height(), 10 );

        var $div = $('#'+divId);
        var x = $div.position().left;
        var y = $div.position().top;

        percentPosX = x / width;
        percentPosY = y / height;
    };


    /**
     *
     * @returns {newName|FunnelNode.name}
     */
    this.getOriginalName = function()
    {
        var originalName;
        var $div = $('#'+divId);

        if( $div.get(0).hasAttribute('originalName') )
            originalName = ($div.attr('originalName'));
        else
            originalName = this.getName();

        return originalName;
    };

    /**
     *
     * @param {type} bOriginalName - OPTIONAL
     */
    this.setNameBasedOnType = function(bOriginalName)
    {
        switch(type)
        {
            case NODE_TYPE.EXTERNAL_URL:
            {
                this.setName(parseUri(oParams['pageUrl']).host + parseUri(oParams['pageUrl']).path, bOriginalName);
                $('#'+divId).attr('title', oParams['pageUrl']);
            }
            break;

            case NODE_TYPE.FLUX_LANDER:
            case NODE_TYPE.FLUX_OFFER:
            {
                this.setName(oParams['name'], bOriginalName);
            }
            break;
        }
    };

    /**
     *
     * @param {type} newName
     * @param {type} bOriginalName - OPTIONAL
     * @returns {undefined}
     */
    this.setName = function(newName, bOriginalName)
    {
        var $div = $('#'+divId);

        if( $div.get(0) )
        {
            var previousName;
            if( $div.get(0).hasAttribute('previousName') )
                previousName = ($div.attr('previousName'));
            else
                previousName = NODE_BASE_NAME;

            if( bOriginalName || !($div.get(0).hasAttribute('originalName')) )
                $div.attr('originalName', (newName));

            $div.html( $div.html().replace(name, newName) );

            $div.attr('previousName', (name));
            name = newName;

            window.plumber.recalculateOffsets($div);
            window.plumber.repaint($div);
            window.plumber.repaintEverything();

            if( !window.plumber.bNotEditable )
                window.dirtyManager.setDirty(true);
        }
    };

    /**
     *
     * @param {type} newName
     * @returns {undefined}
     */
    this.setNameRaw = function(newName)
    {
        var $div = $('#'+divId);
        $div.html( newName );

        window.plumber.recalculateOffsets($div);
        window.plumber.repaint($div);
        window.plumber.repaintEverything();

        if( !window.plumber.bNotEditable )
            window.dirtyManager.setDirty(true);
    };

    /**
     *
     * @returns {undefined}
     */
    this.dumpParams = function()
    {
        if( oParams )
            for(key in oParams)
                console.log(key + ' = ' + oParams[key]);
    };

    /**
     *
     * @returns {undefined}
     */
    this.edit = function()
    {
        var instance = this;

        switch( type )
        {
            case NODE_TYPE.FLUX_LANDER:
            case NODE_TYPE.FLUX_OFFER:
            {
                var oParams = this.getParams();

                dlgEditPageOptions(function(newParams)
                {
                    instance.setParams(newParams);
                    instance.refresh();
                }, oParams);
            }
            break;

            case NODE_TYPE.CONDITION:
            {
                var oParams = this.getParams();

                dlgEditCondition(function(idCondition, conditionName)
                {
                    oParams['idCondition'] = idCondition;
                    oParams['conditionName'] = conditionName;
                    oParams['conditionBlocks'] = window.conditions[idCondition]['conditionBlocks'];
                    oParams['conditionScope'] = window.conditions[idCondition]['conditionScope'];

                    instance.setParams(oParams);
                    instance.refresh();

                }, oParams['idCondition']);
            }
            break;

            case NODE_TYPE.EXTERNAL_URL:
            {
                var oParams = this.getParams();

                dlgEditExternalUrlValue (
                        function(newUrl)
                        {
                            oParams['pageUrl'] = newUrl;

                            instance.setParams(oParams);
                            instance.setNameBasedOnType();
                            instance.refresh();

                        }, oParams['pageUrl']
                    );
            }
            break;

            case NODE_TYPE.JS_CODE_EXECUTOR:
            {
                var oParams = this.getParams();

                dlgEditJSCode(function(id, name, code, delay)
                    {
                        oParams['idCode'] = id;
                        oParams['codeName'] = name;
                        oParams['codeContent'] = code;
                        oParams['codeDelay'] = delay;

                        codeSnippetUpdated(id, name, code, delay);

                        instance.setParams(oParams);
                        instance.refresh();

                    }, oParams['idCode'], oParams['codeName'], oParams['codeContent'], oParams['codeDelay']);
            }
            break;

            case NODE_TYPE.PHP_CODE_EXECUTOR:
            {
                var oParams = this.getParams();

                dlgEditPHPCode(function(id, name, code)
                    {
                        oParams['idCode'] = id;
                        oParams['codeName'] = name;
                        oParams['codeContent'] = code;

                        codeSnippetUpdated(id, name, code);

                        instance.setParams(oParams);
                        instance.refresh();

                    }, oParams['idCode'], oParams['codeName'], oParams['codeContent']);
            }
            break;
        }
    };

    /**
     *
     * @returns {undefined}
     */
    this.refresh = function()
    {
        var $div = $('#'+divId);

        if( percentPosX !== null )
        {
            var width  = $('#funnelDiagramContainer').width();
            var x = parseInt( percentPosX * width, 10 );
            $div.css('left', x+'px');
        }

        if( percentPosY !== null )
        {
            var height = $('#funnelDiagramContainer').height();
            var y = parseInt( percentPosY * height, 10 );
            $div.css('top', y+'px');
        }

        switch( type )
        {
            case NODE_TYPE.EXTERNAL_URL:
            {

            }
            break;

            case NODE_TYPE.JS_CODE_EXECUTOR:
            case NODE_TYPE.PHP_CODE_EXECUTOR:
            {
                var oParams = this.getParams();

                if( oParams['codeName'] && oParams['codeName'].length )
                {
//                    var name = this.getOriginalName();
//
//                    //if( !window.plumber.bNotEditable )
//                    {
//                        name += "<br>" + "(" + oParams['codeName'] + ")";
//                    }

                    var name = oParams['codeName'];
                    this.setName( name );
                }
            }
            break;

            case NODE_TYPE.CONDITION:
            {
                var oParams = this.getParams();

                if( oParams['conditionName'] && oParams['conditionName'].length )
                {
                    var name = "IF"; //this.getOriginalName();

                    //if( !window.plumber.bNotEditable )
                    {
                        name += " (" + oParams['conditionName'] + ")";
                    }

                    this.setName( name );
                }
            }
            break;

            case NODE_TYPE.FLUX_LANDER:
            case NODE_TYPE.FLUX_OFFER:
            {
                if( !bNegativeMaxExitConnections )
                {
                    var requiredActionsCount = maxExitConnections;
                    var currentActionsCount  = window.plumber.getConnections({ source: $div }).length;

                    var sActionsCount = "";

                    if( requiredActionsCount === 0 )
                    {
                        sActionsCount = "No exit action";
                    }
                    else
                    {
                        sActionsCount = "Exit actions: " + currentActionsCount + "/" + requiredActionsCount;

                        var color;
                        if( currentActionsCount != requiredActionsCount )
                            color = '#cc0000';
                        else
                            color = '#009000';

                        sActionsCount = '<span style="color: '+color+';">' + sActionsCount + '</span>';
                    }

                    var name = this.getOriginalName();

                    if( !window.plumber.bNotEditable )
                    {
                        name += "<br>" + "(" + sActionsCount + ")";
                    }

                    this.setName( name );
                }
            }
            break;
        }
    };
};


/**
 *
 * @param {type} divElem
 * @param (boolean} bCompletelyRemove - optional, if set to true, node is deleted instead of hidden
 * @returns {undefined}
 */
function deleteNode(divElem, bCompletelyRemove)
{
    $div = $(divElem);

    var divId = $div.attr('id');

    var aConnectionsFrom = window.plumber.getConnections({source: divId});
    var aConnectionsTo = window.plumber.getConnections({target: divId});

    if( aConnectionsFrom )
        deleteConnections(aConnectionsFrom);

    if( aConnectionsTo )
        deleteConnections(aConnectionsTo);

    if( bCompletelyRemove )
    {
        $div.remove();
    }
    else
    {
        $div.hide();
    }
}

/**
 * Add a listerner to be notified of any node move
 *
 * @param {type} callback - takes 1 parameter:
 *                   1/ the FunnelNode "object" that has been moved
 *
 * @returns {undefined}
 */
function addNodeMovedListener(callback)
{
    if( !window.aFunnelNodeMovedListeners )
    {
        window.aFunnelNodeMovedListeners = [];
    }

    if( !Array.prototype.indexOf || window.aFunnelNodeMovedListeners.indexOf(callback) == -1 )
    {
        window.aFunnelNodeMovedListeners.push(callback);
    }
}

/**
 * Add a listerner to be notified of any node starting to be moved
 *
 * @param {type} callback - takes 1 parameter:
 *                   1/ the FunnelNode "object" that has started to be moved
 *
 * @returns {undefined}
 */
function addNodeStartMoveListener(callback)
{
    if( !window.aFunnelNodeStartMoveListeners )
    {
        window.aFunnelNodeStartMoveListeners = [];
    }

    if( !Array.prototype.indexOf || window.aFunnelNodeStartMoveListeners.indexOf(callback) == -1 )
    {
        window.aFunnelNodeStartMoveListeners.push(callback);
    }
}

/**
 *
 * @param {type} params ->
 *                      params.nodeType - MANDATORY
 *                      params.nodeName - MANDATORY
 *                      params.idNode - OPTIONAL - id of the node in the database. Normally set for loaded nodes.
 *                      params.id - OPTIONAL - id assigned to the created div
 *                      params.x - OPTIONAL - X position of element (CSS style, accepts %, px or no specifier)
 *                      params.y - OPTIONAL - Y position of element (CSS style, accepts %, px or no specifier)
 *                      params.archived - OPTIONAL - if set to true/1 then the div will be hidden
 *
 *                      Plus, if params.nodeType == NODE_TYPE.FLUX_LANDER or NODE_TYPE.FLUX_OFFER:
 *                      params.pageId
 *
 *                      If params.nodeType == NODE_TYPE.EXTERNAL_URL:
 *                      params.url
 *
 * @param {callback} onComplete (divElement) - called when the node is added and completely initialized.
 *
 *
 * @returns {undefined} created div element containing property "funnelNode"
 */
function addNode(params, onComplete, bResizeContainer)
{
    if( !window.plumber.bNotEditable )
        window.dirtyManager.setDirty(true);

    var filterName = null;
    var nodeType = params.nodeType;
    var nodeName = params.nodeName;

    switch( params.nodeType )
    {
        case NODE_TYPE.ROOT:
            filterName = "#filter-traffic";
            break;

        case NODE_TYPE.ROTATOR:
            filterName = "#filter-rotators";
            nodeName = "";
            break;

        case NODE_TYPE.FLUX_LANDER:
            filterName = "#filter-landers";
            break;

        case NODE_TYPE.FLUX_OFFER:
            filterName = "#filter-offers";
            break;

        case NODE_TYPE.CONDITION:
            filterName = "#filter-conditions";
            break;
    }

    var style = 'style="';
    if( filterName )
    {
        style += 'filter:url('+filterName+'); -webkit-filter:url('+filterName+');';
    }
    style += '"';
    style = '';     // Filters are not working well with firefox. We remove them for now
                    // and use colored images instead.

    var divHtml = ' <div class="w active" '+style+'>\
                        <div class="node-inline-stats" style="display: none;"></div>\
                        <div class="ep"></div>\
                        <div class="nodeBackground"></div>\
                        <span>'+nodeName+'</span>\
                    </div>';
    var $div = $(divHtml).appendTo('#funnelDiagramContainer');

    var divId;

    if( params.id )
        divId = params.id;
    else
        divId = UUID.generate();

    $div.attr('id', divId);


    if( params.x )
        $div.css('left', params.x);

    if( params.y )
        $div.css('top', params.y);

    if( params.idNode )
        $div.attr('idnode', params.idNode);

    if( params.archived )
        $div.hide();

    var divElem = $div.get(0);

    divElem.funnelNode = new FunnelNode({
        idNode: params.idNode,
        nodeType: nodeType,
        nodeName: nodeName,
        divId: divId
    });

    divElem.funnelNode.recordCurrentPosition();

    $div.attrchange({
        trackValues: true,
        callback: function(event)
        {
	    //event    	          - event object
	    //event.attributeName - Name of the attribute modified
	    //event.oldValue      - Previous value of the modified attribute
	    //event.newValue      - New value of the modified attribute

            if( event.attributeName == 'class' )
            {
                if( event.oldValue.indexOf('jsPlumb_dragged') != -1 && event.newValue.indexOf('jsPlumb_dragged') == -1 )
                {
                    // jsPlumb doesn't have any event notifying node moves but we know that a this node has just moved!
                    // The 'jsPlumb_dragged' class is added to nodes when they start getting dragged
                    // and is removed when they have been dropped.
                    // By monitoring the class changes, we can know when the move has ended.

                    $(this).get(0).funnelNode.recordCurrentPosition();

                    if( !window.plumber.bNotEditable )
                        window.dirtyManager.setDirty(true);

                    if( window.aFunnelNodeMovedListeners )
                    {
                        for(var i=0; i<window.aFunnelNodeMovedListeners.length; i++)
                        {
                            window.aFunnelNodeMovedListeners[i]( $(this).get(0).funnelNode );
                        }
                    }
                }
                else if( event.oldValue.indexOf('jsPlumb_dragged') == -1 && event.newValue.indexOf('jsPlumb_dragged') != -1 )
                {
                    // A node is starting to be moved

                    if( window.aFunnelNodeStartMoveListeners )
                    {
                        for(var i=0; i<window.aFunnelNodeStartMoveListeners.length; i++)
                        {
                            window.aFunnelNodeStartMoveListeners[i]( $(this).get(0).funnelNode );
                        }
                    }
                }
            }
        }
    });

    $div.dblclick(function()
    {
        if( !window.plumber.bNotEditable )
        {
            editElement(divElem.funnelNode);
        }
    });

    if( !window.plumber.bNotEditable )
    {
        switch( nodeType )
        {
            case NODE_TYPE.ROOT:
            {
                $div.contextMenu('menu', aFunnelContextMenuRootNode, {
                    triggerOn: 'click',
                    mouseClick: 'right',
                    onOpen: function(data, event)
                    {
                        $('#funnelDiagramContainer').get(0).elementUnderContextMenu = $div;
                    }
                });
            }
            break;

            case NODE_TYPE.FLUX_LANDER:
            case NODE_TYPE.FLUX_OFFER:
            {
                $div.contextMenu('menu', aFunnelContextMenuPageNodes, {
                    triggerOn: 'click',
                    mouseClick: 'right',
                    onOpen: function(data, event)
                    {
                        $('#funnelDiagramContainer').get(0).elementUnderContextMenu = $div;
                    }
                });
            }
            break;

            default:
            {
                $div.contextMenu('menu', aFunnelContextMenuNodes, {
                    triggerOn: 'click',
                    mouseClick: 'right',
                    onOpen: function(data, event)
                    {
                        $('#funnelDiagramContainer').get(0).elementUnderContextMenu = $div;
                    }
                });
            }
            break;
        }
    }

    switch(nodeType)
    {
        case NODE_TYPE.CONDITION:
        {
            $div.addClass('funnelCondition funnelDiagramNode');

            divElem.funnelNode.setParams({
                idCondition: params.idCondition,
                conditionName: params.conditionName,
                conditionBlocks: params.conditionBlocks,
                conditionScope: params.conditionScope
            });
            divElem.funnelNode.setName(nodeName, true);
            divElem.funnelNode.setMaxExitConnections(2);
            divElem.funnelNode.refresh();

            makeDraggableSourceTargetNode(divElem);

            if( onComplete )
                onComplete(divElem);

            if( bResizeContainer )
            {
                autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
            }
        }
        break;

        case NODE_TYPE.JS_CODE_EXECUTOR:
        case NODE_TYPE.PHP_CODE_EXECUTOR:
        {
            $div.addClass('funnelCodeExecutor funnelDiagramNode');

            if( nodeType === NODE_TYPE.PHP_CODE_EXECUTOR )
            {
                $div.addClass('funnelCodeExecutorPHP');

                divElem.funnelNode.setParams({
                    idCode: params.idCode,
                    codeType: params.codeType,
                    codeName: params.codeName,
                    codeContent: params.codeContent
                });
            }
            else
            {
                $div.addClass('funnelCodeExecutorJS');
                divElem.funnelNode.setParams({
                    idCode: params.idCode,
                    codeType: params.codeType,
                    codeName: params.codeName,
                    codeContent: params.codeContent,
                    codeDelay: params.codeDelay
                });
            }

            divElem.funnelNode.setName(nodeName, true);
            // divElem.funnelNode.setMaxExitConnections(1);
            divElem.funnelNode.refresh();

            makeDraggableSourceTargetNode(divElem);

            if( onComplete )
                onComplete(divElem);

            if( bResizeContainer )
            {
                autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
            }
        }
        break;

        case NODE_TYPE.ROOT:
        {
            $div.addClass('funnelRoot funnelRotator funnelDiagramNode');

            divElem.funnelNode.setName(nodeName, true);
            divElem.funnelNode.refresh();

            makeDraggableNode(divElem);
            makeSourceNode(divElem);

            if( onComplete )
                onComplete(divElem);

            if( bResizeContainer )
            {
                autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
            }
        }
        break;

        case NODE_TYPE.ROTATOR:
        {
            $div.addClass('funnelRotator funnelDiagramNode');

            divElem.funnelNode.setName(nodeName, true);
            divElem.funnelNode.refresh();

            makeDraggableSourceTargetNode(divElem);

            if( onComplete )
                onComplete(divElem);

            if( bResizeContainer )
            {
                autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
            }
        }
        break;

        case NODE_TYPE.EXTERNAL_URL:
        {
            $div.addClass('funnelExternalUrl funnelDiagramNode');

            divElem.funnelNode.setParams({pageUrl: params.url});
            //divElem.funnelNode.setMaxExitConnections(0);
            divElem.funnelNode.setNameBasedOnType(true);
            divElem.funnelNode.refresh();

            makeDraggableSourceTargetNode(divElem);

            if( onComplete )
                onComplete(divElem);

            if( bResizeContainer )
            {
                autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
            }
        }
        break;

        case NODE_TYPE.FLUX_LANDER:
        case NODE_TYPE.FLUX_OFFER:
        {
            $div.addClass('funnelFluxPage funnelDiagramNode');

            if( nodeType === NODE_TYPE.FLUX_LANDER )
            {
                $div.addClass('funnelFluxPageLander');
            }
            else
            {
                $div.addClass('funnelFluxPageOffer');

                if(window.aPagesParams[params.pageId] && window.aPagesParams[params.pageId].typeParams)
                {
                    if(window.aPagesParams[params.pageId].typeParams.note)
                    {
                        $div.attr('title', window.aPagesParams[params.pageId].typeParams.note.replace(/(\r\n|\n\r|\r|\n)/g,'<br />'));
                    }
                }
            }

            if( !window.funnelNodesPageOptions )
            {
                window.funnelNodesPageOptions = {};
            }

            window.funnelNodesPageOptions[params.pageId] = params.pageOptions;
            window.bFunnelNodeResizeContainerOnComplete = bResizeContainer;

            if( window.aPagesParams[params.pageId] )
            {
                addNodeInitPage(params, divElem, $div, function(divElem)
                                                        {
                                                            if( onComplete )
                                                                onComplete(divElem);

                                                            if( window.bFunnelNodeResizeContainerOnComplete )
                                                            {
                                                                autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
                                                            }
                                                        });
            }
            else
            {
                var aParams = {};
                aParams[ URLParams.PAGE_ID ] = params.pageId;

                ajaxRequest('getPage',
                            aParams,
                            function (msg)
                            {
                                if (msg)
                                {
                                    window.aPagesParams[params.pageId] = unserialize(msg);

                                    addNodeInitPage(params, divElem, $div, function(divElem)
                                                        {
                                                            if( onComplete )
                                                                onComplete(divElem);

                                                            if( window.bFunnelNodeResizeContainerOnComplete )
                                                            {
                                                                autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
                                                            }
                                                        });
                                }
                            });
            }
        }
        break;
    }

    return divElem;
}

function addNodeInitPage(params, divElem, $div, onComplete)
{
    var aPageParams = window.aPagesParams[params.pageId];

    if( params.pageName )
    {
        aPageParams["name"] = params.pageName;
    }

    var id = aPageParams["id"];
    var name = aPageParams["name"];
    var url = aPageParams["pageUrl"];
    var aURLMapper = aPageParams["aURLMapper"];
    var bArchived = aPageParams["archived"];
    var actionsCount = aPageParams["pageFunnelActionsCount"];
    var actionsSourceURLs = [];

    if( actionsCount > 0 )
    {
        // Get source urls for each action

        for(var i=0; i<aURLMapper.length; i++)
        {
            if( aURLMapper[i].to.startsWith(PageTokens["PAGE_FUNNEL_ACTION_PREFIX"]) )
            {
                actionsSourceURLs.push( aURLMapper[i].from );
            }
        }

        aPageParams["actionsSourceURLs"] = actionsSourceURLs;
    }

    if( window.funnelNodesPageOptions[params.pageId] )
    {
        aPageParams["pageOptions"] = window.funnelNodesPageOptions[params.pageId];
    }

    divElem.funnelNode.setParams(aPageParams);
    divElem.funnelNode.setNameBasedOnType(true);
    divElem.funnelNode.setMaxExitConnections(actionsCount);
    divElem.funnelNode.refresh();

    if( bArchived )
    {
        $div.removeClass('active').addClass('archived');
        $div.attr('title', 'This page has been archived');
    }

    makeDraggableSourceTargetNode(divElem);

    if( onComplete )
        onComplete(divElem);
}

function makeDraggableSourceTargetNode(divElem)
{
    makeDraggableNode(divElem);
    makeSourceNode(divElem);
    makeTargetNode(divElem);
}

function makeDraggableNode(divElem)
{
    //if( !window.plumber.bNotEditable )
    {
        window.plumber.draggable(divElem, {containment:true});
    }
}

function makeSourceNode(divElem)
{
    if( divElem.funnelNode.getMaxExitConnections() > 0 )
    {
        window.plumber.makeSource(divElem,
        {
            filter:".ep",
            anchor: getFunnelNodeAnchor(divElem.funnelNode),
            //connectorStyle:{ strokeStyle:"#5c96bc", lineWidth:2, outlineColor:"transparent", outlineWidth:4 }
        });

        $(divElem).find('.ep').css('display', window.plumber.bNotEditable ? 'none' : 'block');
    }
    else
    {
        $(divElem).find('.ep').css('display', 'none');
    }
}

function makeTargetNode(divElem)
{
    if( divElem.funnelNode.getMaxEntryConnections() > 0 )
    {
        window.plumber.makeTarget(divElem,
        {
            dropOptions:{ hoverClass:"dragHover" },
            anchor: getFunnelNodeAnchor(divElem.funnelNode),
            allowLoopback:false
        });
    }
}

function getFunnelNodeAnchor(funnelNode)
{
    var anchor = "";

    switch(funnelNode.getType())
    {
//        case NODE_TYPE.CONDITION:
//        {
//            anchor = ["Perimeter", {shape: "Diamond"}];
//        }
//        break;

        default:
        {
            anchor = "Continuous";
        }
        break;
    }

    return anchor;
}

/**
 *
 * @param {callback} onValidatedUrl - called when url is validated. Takes 1 parameter which is the url.
 * @param {string} initialUrl - optional - initial url to pre-populate the field
 *
 * @returns {undefined}
 */
function dlgEditExternalUrlValue(onValidatedUrl, initialUrl)
{
    window.dlgEditExternalUrlValueOnValidatedUrl = onValidatedUrl;

    modalEnterText({
        onValidatedText: function(text)
        {
            if( window.dlgEditExternalUrlValueOnValidatedUrl )
            {
                window.dlgEditExternalUrlValueOnValidatedUrl(text);
            }
        },
        label: 'Enter URL:',
        initialText: initialUrl,
        placeHolder: 'http://',
        onTextInput: function(text)
        {
            if( validator.isURL(text) )
            {
                return null;
            }

            return "Please enter a valid URL";
        }
    });
}

/**
 *
 * @param {type} onValidated - takes 1 parameter, the new params
 * @param {type} oldParams
 * @returns {undefined}
 */
function dlgEditPageOptions(onValidated, oldParams)
{
    var htmlContent = '';
    htmlContent += '<div id="dlgEditPageOptionsPanel" class="panel" style="width: 450px;"><div class="row"><div class="large-12 columns">';

    var oCheckboxes = {
        "bIncludeAccumulatedURLParams": {title: "This page needs all the accumulated URL Params", checked: false}
    };

    window.editPageOptionsOnValidated = onValidated;
    window.editPageOptionsOldParams = oldParams;
    window.editPageOptions = oldParams["pageOptions"];
    if( !window.editPageOptions )
    {
        window.editPageOptions = {};
    }

    if( oCheckboxes )
    {
        for(key in oCheckboxes)
        {
            var params = oCheckboxes[key];
            var title = params['title'];
            var bChecked = window.editPageOptions[key] ? window.editPageOptions[key] : params['checked'];

            if( title !== undefined && bChecked !== undefined )
            {
                var checked = bChecked ? 'checked' : '';

                htmlContent += '<div class="switch tiny">';
                htmlContent +=     '<input id="'+key+'" type="checkbox" '+checked+'/>';
                htmlContent +=     '<label for="'+key+'"></label>';
                htmlContent +=     '<span style="font-size: 85%; position:relative; top:-8px; left: 10px;">'+title+'</span>';
                htmlContent += '</div>';
            }
        }
    }

    htmlContent += '</div></div></div>';

    modalShowContent({
        html: htmlContent,
        //x: parseInt($element.offset().left, 10) - $(document).scrollLeft(),
        //y: parseInt($element.offset().top, 10) - $(document).scrollTop(),
        button1Text: 'OK',
        button2Text: 'Cancel',
        onOpen: function()
        {
        },
        button1Callback: function()
        {
            var newParams = $.extend(true, {}, window.editPageOptionsOldParams);
            newParams["pageOptions"] = window.editPageOptions;

            $('#dlgEditPageOptionsPanel').find('input:checkbox').each(function()
            {
                var key = $(this).attr('id');
                newParams["pageOptions"][key] = $(this).is(":checked") ? 1 : 0;

                if( window.editPageOptionsOnValidated )
                {
                    window.editPageOptionsOnValidated(newParams);
                }
            });
        }
    });
}

/**
 *
 * @param {callback} onValidate - called when number is validated. Takes 1 parameter which is the entered value.
 * @param {string} initialValue - optional - initial url to pre-populate the field
 *
 * @returns {undefined}
 */
function dlgEditMaxExitConnections(onValidate, initialValue)
{
    modalEnterText({
        onValidatedText: onValidate,
        label: 'Number of Actions on this Page:',
        initialText: initialValue,
        placeHolder: '',
        bPopupSettings: {
            modalClose: false,
            escClose: false
        }
    });
}

/**
 *
 * @param {type} onValidate
 * @param {type} idCode
 * @param {type} initialName
 * @param {type} initialCode
 * @param {type} initialDelay
 * @returns {undefined}
 */
function dlgEditJSCode(onValidate, idCode, initialName, initialCode, initialDelay)
{
    modalCodeEditor("HTML", {
        label: "Enter your Javascript code below:",
        onValidatedCode: onValidate,
        id: idCode,
        name: initialName,
        code: initialCode,
        delay: initialDelay
    });
}

/**
 *
 * @param {type} onValidate
 * @param {type} idCode
 * @param {type} initialName
 * @param {type} initialCode
 * @returns {undefined}
 */
function dlgEditPHPCode(onValidate, idCode, initialName, initialCode)
{
    modalCodeEditor("PHP", {
        label: "Enter your PHP code below:",
        onValidatedCode: onValidate,
        id: idCode,
        name: initialName,
        code: initialCode
    });
}

/**
 *
 * @returns {undefined}
 */
function addExternalUrl()
{
    dlgEditExternalUrlValue(function(url)
        {
            var xPos = window.mainContextMenuX + 'px';
            var yPos = window.mainContextMenuY + 'px';

            addNode({nodeType: NODE_TYPE.EXTERNAL_URL, x: xPos, y: yPos, url: url}, null, true);
        });
}

/**
 *
 * @returns {undefined}
 */
function addCondition()
{
    var xPos = window.mainContextMenuX + 'px';
    var yPos = window.mainContextMenuY + 'px';

    dlgEditCondition(function(idCondition, conditionName)
    {
        addNode({
            idCondition: idCondition,
            conditionName: conditionName,
            conditionBlocks: window.conditions[idCondition]['conditionBlocks'],
            conditionScope: window.conditions[idCondition]['conditionScope'],
            nodeName: 'Condition', 
            nodeType: NODE_TYPE.CONDITION, 
            x: xPos, y: yPos}, null, true);
    });
}

/**
 *
 * @returns {undefined}
 */
function addJSCodeExecutor()
{

    dlgEditJSCode(function(id, name, code, delay)
                    {
                        var xPos = window.mainContextMenuX + 'px';
                        var yPos = window.mainContextMenuY + 'px';

                        addNode({
                            idCode: id,
                            codeType: CodeSnippet.TYPE_JAVASCRIPT,
                            codeName: name,
                            codeContent: code,
                            codeDelay: delay,
                            nodeName: 'Javascript',
                            nodeType: NODE_TYPE.JS_CODE_EXECUTOR,
                            x: xPos, y: yPos}, null, true);
                    }
                );
}

/**
 *
 * @returns {undefined}
 */
function addPHPCodeExecutor()
{
    dlgEditPHPCode(function(id, name, code)
                    {
                        var xPos = window.mainContextMenuX + 'px';
                        var yPos = window.mainContextMenuY + 'px';

                        addNode({
                            idCode: id,
                            codeType: CodeSnippet.TYPE_PHP,
                            codeName: name,
                            codeContent: code,
                            nodeName: 'PHP Code',
                            nodeType: NODE_TYPE.PHP_CODE_EXECUTOR,
                            x: xPos, y: yPos}, null, true);
                    }
                );
}

/**
 *
 * @param {callback} onValidatedPageId - called when pageId is validated. Takes 2 parameter which are the selected pageId and pageName
 * @param {string} initialPageId - optional
 *
 */
function dlgEditFluxPageId(onValidatedPageId, initialPageId, nodeType)
{
    var label = 'Select page:';

    if( nodeType === NODE_TYPE.FLUX_LANDER )
        label = 'Select lander:';
    else if( nodeType === NODE_TYPE.FLUX_OFFER )
        label = 'Select offer:';

    ajaxRequest('getPageIdsAndNames', {nodeType: nodeType, archived: 0},
                function (msg)
                {
                    var aPageIdsAndNames = unserialize(msg);

                    if( aPageIdsAndNames instanceof Array )
                    {
                        modalComboboxSelectOption({
                            aOptions: aPageIdsAndNames,
                            initialSelectedOptionId: initialPageId,
                            label: label,
                            onSelectedOption: onValidatedPageId
                        });
                    }
                }
        );
}

/**
 *
 * @param {callback} onValidatedPagesId - called when pageIds are validated. Takes 2 parameters which are the selected pageIds and pageNames (2 arrays)
 *
 */
function dlgEditMultipleFluxPageIds(onValidatedPagesId, nodeType)
{
    var label = 'Select page:';

    if( nodeType === NODE_TYPE.FLUX_LANDER )
        label = 'Select Landers...';
    else if( nodeType === NODE_TYPE.FLUX_OFFER )
        label = 'Select Offers...';

    ajaxRequest('getPageIdsAndNames', {nodeType: nodeType, archived: 0},
        function (msg)
        {
            var aPageIdsAndNames = unserialize(msg);

            if( aPageIdsAndNames instanceof Array )
            {
                modalComboboxSelectMultipleOptions({
                    aOptions: aPageIdsAndNames,
                    label: label,
                    nodeType: nodeType,
                    onSelectedOptions: onValidatedPagesId,
                    onCreateNewOne: function()
                    {

                    }
                });
            }
        }
    );
}

/**
 *
 * @param {callback} onEditedCondition - called when pageId is validated. Takes 2 parameter which are the selected conditionId and conditionName
 * @param {string} initialConditionId - optional
 *
 */
function dlgEditCondition(onEditedCondition, initialConditionId)
{
    modalComboboxSelectOption({
        aOptions: window.aConditionsIdsAndNames,
        initialSelectedOptionId: initialConditionId,
        label: "Select a condition",
        onSelectedOption: onEditedCondition,
        onEditSelected: function(idSelected)
        {
            var urlParams = {};
            urlParams[URLParams.CONDITION_ID] = idSelected;
            urlParams[URLParams.CAMPAIGN_FUNNEL_ID] = window.plumber.idFunnel;
//            urlParams[URLParams.CONDITION_NAME] = encodeDataForUrl(window.conditionsIdToName[idSelected]);
//            urlParams[URLParams.CONDITION_BLOCKS] = encodeDataForUrl(window.conditions[idSelected]['conditionBlocks']);

            modalShowSection(Sections.NODE_CONDITIONS_EDIT, {
                urlParams: urlParams,
                onOk: function(contentWindow)
                        {
                            conditionUpdated( idSelected, 
                                              contentWindow.getConditionName(), 
                                              contentWindow.getConditionBlocks(),
                                              contentWindow.getConditionScope() );

                            modalComboboxSelectOptionEditSelectedName({
                                id: idSelected,
                                name: contentWindow.getConditionName(),
                                scope: contentWindow.getConditionScope()
                            });
                        }
            });

        },
        onCreateNewOne: function()
        {
            var urlParams = {};
            urlParams[URLParams.CONDITION_ID] = null;

            modalShowSection(Sections.NODE_CONDITIONS_EDIT, {
                urlParams: urlParams,
                initialScope: true,
                onOk: function(contentWindow)
                        {
                            var name = contentWindow.getConditionName();
                            var scope = contentWindow.getConditionScope();
                            var blocks = contentWindow.getConditionBlocks();
                            var idCondition = conditionRecordNewOne(name, blocks, scope);
                            
                            modalComboboxSelectOptionNewEntry({
                                id: idCondition,
                                name: name,
                                scope: scope
                            });
                        }
            });
        }
    });
}

/**
 * @returns {undefined}
 */
function addMultipleFluxLanders()
{
    addMultipleFluxPages(NODE_TYPE.FLUX_LANDER);
}

/**
 * @returns {undefined}
 */
function addMultipleFluxOffers()
{
    addMultipleFluxPages(NODE_TYPE.FLUX_OFFER);
}

/**
 * @returns {undefined}
 */
function addMultipleFluxPages(pageType)
{
    document.addMultipleFluxPagesType = pageType;

    dlgEditMultipleFluxPageIds(function(pageIds, pageNames)
    {
        var xPos;
        var yPos;
        var xStart = window.mainContextMenuX;
        var yStart = window.mainContextMenuY;

        window.multipleFluxPagesLength = pageIds.length;

        for(var i=0; i<pageIds.length; i++)
        {
            var pageId = pageIds[i];
            var pageName = pageNames[i];

            if( pageId )
            {
                xPos = xStart + 'px';
                yPos = yStart + 'px';

                addNode({nodeName: 'Loading...', nodeType: document.addMultipleFluxPagesType, x: xPos, y: yPos, pageId: pageId, pageName: pageName},
                    function(divElem)
                    {
                        window.multipleFluxPagesLength--;

                        if( !window.multipleFluxPagesLength )
                        {
                            autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
                        }
                    });

                xStart += 50;
                yStart += 50;
            }
        }

//        if( pageIds.length )
//        {
//            autoSizeDiagramContainer('#funnelDiagramContainer', true, null, FUNNEL_DIAGRAM_DEFAULT_ADDITIONAL_HEIGHT);
//        }

    }, document.addMultipleFluxPagesType);
}
/**
 * add a new page (landers or offers ) from the funnel diagram
 * @param nodeType
 */
function addPages(nodeType)
{

    if( nodeType )
    {
        var section;

        switch( nodeType )
        {
            case NODE_TYPE.FLUX_LANDER:
            {
                section = Sections.LANDER_EDIT;
            }
                break;

            case NODE_TYPE.FLUX_OFFER:
            {
                section = Sections.OFFER_EDIT;
            }
                break;
        }

        if( section )
        {
            var urlParams = {};
            urlParams[URLParams.PAGE_HIDE_BUTTONS] = 1;

            modalShowSection(section, {
                urlParams: urlParams,
                onOk: function(contentWindow)
                {
                    var pageName = contentWindow.getPageName();
                    var settings = contentWindow.getSetting();

                    ajaxRequest('savePage',
                        {
                            'pageSettings' : serialize(settings)
                        },
                        function (msg)
                        {
                            modalLongActionClose();

                            var aResult = unserialize(msg);

                            if( !aResult['success'] )
                            {

                            }
                            else
                            {

                                var entryName = [];
                                entryName['name'] = pageName;
                                entryName['id'] = aResult['idPage'];
                                modalComboboxSelectOptionNewEntry(entryName);

                            }
                        }
                    );



                }
            });
        }
    }

}



