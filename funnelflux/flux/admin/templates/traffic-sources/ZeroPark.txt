[identification]
typecost = 0
defaultcost = 

[tracking-fields]
cid = {cid}
source = {source}
target = {target}
keyword = {keyword}
match = {match}
traffic_type = {traffic_type}
visitor_type = {visitor_type}
campaign_id = {campaign_id}
ad_copy_name = {ad_copy_name}

[postback]
type=1
code="http://postback.zeroredirect1.com/zppostback/##REPLACE##?cid={cid}&payout={flux_payout}"
