#!/bin/bash

# cd in current script's directory
cd "${0%/*}"
rm -f ./*.zip
rm -f ./*.md5
sh ./wurfl-updater.sh https://data.scientiamobile.com/uzyqb/wurfl.zip .

cd ../../../libs/wurfl-php
rm -rf _ff_data
### php composer.phar install --optimize-autoloader --no-dev
php wurfl-updater -c ./config/config.php

# cleanup
rm -f _ff_data/storage/wurfl_updater/*.zip
rm -f ../../admin/tools/wurflUpdater/wurfl.zip

#mv _ff_data/storage/Repository.php _ff_data/storage/Repository2.php
#zip -r -9 ../../admin/tools/wurflUpdater/temp.zip _ff_data/
#mv _ff_data/storage/Repository2.php _ff_data/storage/Repository.php
#mv ../../admin/tools/wurflUpdater/temp.zip ../../admin/tools/wurflUpdater/_ff_data.zip

#cd ../../admin/tools/wurflUpdater/
#md5=`md5sum -b ./_ff_data.zip | awk '{ print $1 }'`
#echo $md5 > _ff_data.md5

#chown -R nginx:nginx *
#mv ./_ff_data.* ../../../libs/wurfl-php/_ff_data/