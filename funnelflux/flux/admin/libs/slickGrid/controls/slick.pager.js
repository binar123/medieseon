(function ($) {
  function SlickGridPager(dataView, grid, $container, nbByPage) {
    var $status;

    function init() {
      dataView.onPagingInfoChanged.subscribe(function (e, pagingInfo) {
        updatePager(pagingInfo);
      });

      constructPagerUI();
      if (!slickOptions.bOnDashboard)
      {
        var pageSize = getCookie(createCookieKeyFromURL(location.href + slickOptions.tableNameDom, PAGE_SIZE_PAGER));

        if (parseInt(pageSize))
        {
          dataView.setPagingOptions({pageSize: pageSize});
        }
        else
        {
          dataView.setPagingOptions({pageSize: 50});
          createCookie(createCookieKeyFromURL(location.href + slickOptions.tableNameDom, PAGE_SIZE_PAGER), 50);
        }
      }

      updatePager(dataView.getPagingInfo());
    }

    function getNavState() {
      var cannotLeaveEditMode = !Slick.GlobalEditorLock.commitCurrentEdit();
      var pagingInfo = dataView.getPagingInfo();
      var lastPage = pagingInfo.totalPages - 1;

      return {
        canGotoFirst: !cannotLeaveEditMode && pagingInfo.pageSize != 0 && pagingInfo.pageNum > 0,
        canGotoLast: !cannotLeaveEditMode && pagingInfo.pageSize != 0 && pagingInfo.pageNum != lastPage,
        canGotoPrev: !cannotLeaveEditMode && pagingInfo.pageSize != 0 && pagingInfo.pageNum > 0,
        canGotoNext: !cannotLeaveEditMode && pagingInfo.pageSize != 0 && pagingInfo.pageNum < lastPage,
        pagingInfo: pagingInfo
      };
    }

    function setPageSize(n) {
      dataView.setRefreshHints({
        isFilterUnchanged: true
      });
      dataView.setPagingOptions({pageSize: n});
    }

    function gotoFirst() {
      if (getNavState().canGotoFirst) {
        grid.setNbNumberPage(0);
        dataView.setPagingOptions({pageNum: 0});
      }
    }

    function gotoLast() {
      var state = getNavState();
      if (state.canGotoLast) {
        grid.setNbNumberPage(state.pagingInfo.totalPages - 1);
        dataView.setPagingOptions({pageNum: state.pagingInfo.totalPages - 1});
      }
    }

    function gotoPrev() {
      var state = getNavState();
      if (state.canGotoPrev) {
        grid.setNbNumberPage(state.pagingInfo.pageNum - 1);
        dataView.setPagingOptions({pageNum: state.pagingInfo.pageNum - 1});
      }
    }

    function gotoNext() {
      var state = getNavState();
      if (state.canGotoNext) {
        grid.setNbNumberPage(state.pagingInfo.pageNum + 1);
        dataView.setPagingOptions({pageNum: state.pagingInfo.pageNum + 1});
      }
    }

    function constructPagerUI() {
      $container.empty();

      //var $nav = $("<span class='slick-pager-nav' />").appendTo($container);
      //var $settings = $("<span class='slick-pager-settings' />").appendTo($container);
      $status = $("<span class='slick-pager-status' />").appendTo($container);
      var $nav = $("<span class='slick-pager-settings' />").appendTo($container);
      var $settings = $("<span class='slick-pager-nav' />").appendTo($container);


      $settings
          .append("<span class='slick-pager-settings-expanded' style='display:none'>Show: <a class='slick-pager-filter-row' data=0>All</a><a class='slick-pager-filter-row' data=25>25</a><a class='slick-pager-filter-row' data=50>50</a><a class='slick-pager-filter-row' data=100>100</a></span>");

      $settings.find("a[data]").click(function (e) {
        var pagesize = $(e.target).attr("data");
        if (pagesize != undefined) {
          if (pagesize == -1) {
            var vp = grid.getViewport();
            setPageSize(vp.bottom - vp.top);
          } else {
            setPageSize(parseInt(pagesize));
            if (slickOptions.tableNameDom && !slickOptions.bOnDashboard)
            {
              createCookie(createCookieKeyFromURL(location.href + slickOptions.tableNameDom, PAGE_SIZE_PAGER), parseInt(pagesize));
            }
          }
        }
      });

      var icon_prefix = "<span class='ui-state-default ui-corner-all ui-icon-container'><span class='ui-icon ";
      var icon_suffix = "' /></span>";

      //$(icon_prefix + "ui-icon-lightbulb" + icon_suffix)
      //    .click(function () {
      //      $(".slick-pager-settings-expanded").toggle()
      //    })
      //    .appendTo($settings);

      $(icon_prefix + "ui-icon-seek-first" + icon_suffix)
          .click(function (event) {
            if (slickOptions.bMultipleInstance && mySlickGrid) {
              var id = $(event.currentTarget).closest(".widget-content").attr("id");
              var myGrid = listGrids[id];
              mySlickGrid.getGridForMultipleInstance('', myGrid);
            }
            gotoFirst();
          })
          .appendTo($nav);

      $(icon_prefix + "ui-icon-seek-prev" + icon_suffix)
          .click(function (event) {
            if (slickOptions.bMultipleInstance && mySlickGrid) {
              var id = $(event.currentTarget).closest(".widget-content").attr("id");
              var myGrid = listGrids[id];
              mySlickGrid.getGridForMultipleInstance('', myGrid);
            }
            gotoPrev();
          })
          .appendTo($nav);

      $(icon_prefix + "ui-icon-seek-next" + icon_suffix)
          .click(function (event) {
            if (slickOptions.bMultipleInstance && mySlickGrid) {
              var id = $(event.currentTarget).closest(".widget-content").attr("id");
              var myGrid = listGrids[id];
              mySlickGrid.getGridForMultipleInstance('', myGrid);
            }
            gotoNext();
          })
          .appendTo($nav);

      $(icon_prefix + "ui-icon-seek-end" + icon_suffix)
          .click(function(event) {
            if (slickOptions.bMultipleInstance && mySlickGrid)
            {
              var id = $(event.currentTarget).closest(".widget-content").attr("id");
              var myGrid = listGrids[id];
              mySlickGrid.getGridForMultipleInstance('',myGrid);
            }
            gotoLast();
          })
          .appendTo($nav);

      $container.find(".ui-icon-container")
          .hover(function () {
            $(this).toggleClass("ui-state-hover");
          });

      $container.children().wrapAll("<div class='slick-pager' />");

      if (slickOptions.bOnDashboard && nbByPage)
      {
        var pagesize = nbByPage-1;
        if (pagesize != undefined) {
          if (pagesize == -1) {
            var vp = grid.getViewport();
            setPageSize(vp.bottom - vp.top);
          } else {
            setPageSize(parseInt(pagesize));
          }
        }
      }

    }


    function updatePager(pagingInfo) {
      var state = getNavState();

      $container.find(".slick-pager-nav span").removeClass("ui-state-disabled");
      if (!state.canGotoFirst) {
        $container.find(".ui-icon-seek-first").addClass("ui-state-disabled");
      }
      if (!state.canGotoLast) {
        $container.find(".ui-icon-seek-end").addClass("ui-state-disabled");
      }
      if (!state.canGotoNext) {
        $container.find(".ui-icon-seek-next").addClass("ui-state-disabled");
      }
      if (!state.canGotoPrev) {
        $container.find(".ui-icon-seek-prev").addClass("ui-state-disabled");
      }

      if (pagingInfo.pageSize == 0) {
        var totalRowsCount = dataView.getItems().length;
        var visibleRowsCount = pagingInfo.totalRows;

        if (visibleRowsCount < totalRowsCount) {
          $status.text("Showing " + visibleRowsCount + " of " + totalRowsCount + " rows");
        } else {
          $status.text("Showing all " + totalRowsCount + " rows");
        }
        $status.text("Showing all " + pagingInfo.totalRows + " rows");
      } else {
        if (slickOptions.bOnDashboard)
        {
          $status.text("Page " + (pagingInfo.pageNum + 1) + " of " + pagingInfo.totalPages);
          
          if( pagingInfo.totalPages <= 1 )
          {
              $status.closest('.slick-pager').hide();
          }
          else
          {
              $status.closest('.slick-pager').show();
          }
        }
        else
        {
          $status.text("Showing page " + (pagingInfo.pageNum + 1) + " of " + pagingInfo.totalPages);
        }

      }
    }

    init();
  }

  // Slick.Controls.Pager
  $.extend(true, window, { Slick:{ Controls:{ Pager:SlickGridPager }}});
})(jQuery);
