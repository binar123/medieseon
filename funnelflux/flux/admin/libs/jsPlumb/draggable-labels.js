;
(function () {

    "use strict";

    // is c between a and b?
    var within = function (a, b, c) {
        return c >= Math.min(a, b) && c <= Math.max(a, b);
    };
    // find which of a and b is closest to c
    var closest = function (a, b, c) {
        return Math.abs(c - a) < Math.abs(c - b) ? a : b;
    };

    function simpleBezierFindClosestPointOnPath(x, y, segment)
    {
        var x1 = segment.params.x2;
        var x2 = segment.params.x1;
        var y1 = segment.params.y2;
        var y2 = segment.params.y1;
        var m = Biltong.gradient({x: x1, y: y1}, {x: x2, y: y2});
        var m2 = -1 / m;
        var length = segment.getLength();

        var out = {
            d: Infinity,
            x: null,
            y: null,
            l: null,
            x1: x1,
            x2: x2,
            y1: y1,
            y2: y2
        };

        if (m === 0) {
            out.y = y1;
            out.x = within(x1, x2, x) ? x : closest(x1, x2, x);
        }
        else if (m == Infinity || m == -Infinity) {
            out.x = x1;
            out.y = within(y1, y2, y) ? y : closest(y1, y2, y);
        }
        else {
            // closest point lies on normal from given point to this line.  
            var b = y1 - (m * x1),
                    b2 = y - (m2 * x),
                    // y1 = m.x1 + b and y1 = m2.x1 + b2
                    // so m.x1 + b = m2.x1 + b2
                    // x1(m - m2) = b2 - b
                    // x1 = (b2 - b) / (m - m2)
                    _x1 = (b2 - b) / (m - m2),
                    _y1 = (m * _x1) + b;

            out.x = within(x1, x2, _x1) ? _x1 : closest(x1, x2, _x1);//_x1;
            out.y = within(y1, y2, _y1) ? _y1 : closest(y1, y2, _y1);//_y1;                    
        }

        var fractionInSegment = Biltong.lineLength([out.x, out.y], [x1, y1]);
        out.d = Biltong.lineLength([x, y], [out.x, out.y]);
        out.l = fractionInSegment / length;
        return out;
    }

    function getLabelPosition(connection, x, y) {
        var segments = connection.connector.getSegments(),
                closest,
                projectionWay,
                totalWay = 0;
        for (var i = 0; i < segments.length; i++) {
            var segment = segments[i];
            //var projection = segment.findClosestPointOnPath(x, y, i, connection.connector.bounds);
            var projection = simpleBezierFindClosestPointOnPath(x, y, segment);
            var segmentWay = segment.getLength();
            // Calculate projectionWay, as the distance that the label travels
            // within the full available length of the totalWay.
            if (closest === undefined || projection.d < closest.d) {
                closest = projection;
                projectionWay = totalWay + segmentWay * projection.l;
            }
            totalWay += segmentWay;
        }
        // calculate total percentage of the label's new position
        closest.totalPercent = projectionWay / totalWay;
        return closest;
    }

    function setupConnection(instance, params) {
        var connection = params.connection;
        var label = connection.getOverlay('label');
        var elLabel = label.getElement();
        instance.draggable(elLabel, {
            stop: function () 
            {
                if (connection.source.funnelNode)
                {
                    $('#' + connection.source.funnelNode.getDivId()).removeClass(jsPlumb.hoverClass + " _jsPlumb_source_hover _jsPlumb_target_hover");
                }

                if (connection.target.funnelNode)
                {
                    $('#' + connection.target.funnelNode.getDivId()).removeClass(jsPlumb.hoverClass + " _jsPlumb_source_hover _jsPlumb_target_hover");
                }

                $(label.canvas).removeClass(jsPlumb.hoverClass);
                
                if( connection.funnelElementData )
                {
                    var data = connection.funnelElementData.getData();
                    
                    if( data )
                    {
                        data['labelLocation'] = label.getLocation();
                    }
                }
            },
            drag: function () {
                var pos = jsPlumb.getUIPosition(arguments, jsPlumb.getZoom());
                // Instead of the canvas of the connection, the top left bound of the
                // two endpoint coordinates must be considered.
                var oe1 = jsPlumb.getElementObject(connection.endpoints[0].canvas);
                var o1 = jsPlumbAdapter.getOffset(oe1, instance);
                var oe2 = jsPlumb.getElementObject(connection.endpoints[1].canvas);
                var o2 = jsPlumbAdapter.getOffset(oe2, instance);
                // In addition, offset if with the label's midpoint, because we want
                // to grab the middle of the label instead of the top left corner.
                var labelWidth = label.canvas.offsetWidth;
                var labelHeight = label.canvas.offsetHeight;
                var o = {
                    left: Math.min(o1.left, o2.left) + labelWidth / 2,
                    top: Math.min(o1.top, o2.top) + labelHeight / 2
                };
                // Find the closest point on the segment.
                var closest = getLabelPosition(connection, pos.left - o.left, pos.top - o.top);
                // Store the label's position, and repaint it.
                label.loc = closest.totalPercent;
                if (!instance.isSuspendDrawing()) {
                    label.component.repaint();
                }
            }
        });
    }

    function makeLabelsDraggable(instance) {
        // suspend drawing and initialise.
        instance.doWhileSuspended(function () {
            // listen for new connections; initialise them the same way we initialise the connections at startup.
            instance.bind('connection', function (connInfo, originalEvent) {
                setupConnection(instance, connInfo);
            });
        });
    }
    jsPlumb.makeLabelsDraggable = makeLabelsDraggable;

})();