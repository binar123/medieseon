<?php
require_once dirname(__FILE__) . '/../../content/condition.php'; require_once dirname(__FILE__) . '/../../tracking/visitorInfo.php'; require_once dirname(__FILE__) . '/../../includes/session.php'; class ConditionDeviceInfoTest extends PHPUnit_Framework_TestCase { public function setUp() { @session_start(); parent::setUp(); } public function testDeviceType_MatchIs_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); $spf83d76 = array('hash' => $sp5959db->getDeviceHash(), $sp5959db->getBrand(), $sp5959db->getType(), $sp5959db->getName(), $sp5959db->getOs(), $sp5959db->getOsVersion(), 'browser' => $sp5959db->getBrowser(), 'browser_language' => '', 'browser_version' => $sp5959db->getBrowserVersion(), 'display_width' => $sp5959db->getDisplayWidth(), 'display_height' => $sp5959db->getDisplayHeight(), 'marketing_name' => $sp5959db->getMarketingName(), 'manufacturer' => $sp5959db->getManufacturer(), 'pointing_method' => $sp5959db->getPointingMethod(), 'max_speed_rate' => $sp5959db->getMaxDataRate(), 'support_wifi' => $sp5959db->getBWifi(), 'support nfc' => $sp5959db->getBNfc(), 'dual orientation' => $sp5959db->getBDualOrientation(), 'support cookies' => $sp5959db->getBCookies(), 'third party cookies' => $sp5959db->getBThirdPartyCookies(), 'iframes' => $sp5959db->getBIframes(), 'video embed' => $sp5959db->getBIframes(), 'html5' => $sp5959db->getBHTML5(), 'javascript' => $sp5959db->getBJavascript(), 'click2call' => $sp5959db->getBClickToCall(), 'click2sms' => $sp5959db->getBClickToSMS(), 'click2mms' => $sp5959db->getBClickToMMS(), 'streaming mp4' => $sp5959db->getBStreamingMP4(), 'streaming mov' => $sp5959db->getBStreamingMOV(), 'streaming flv' => $sp5959db->getBStreamingFLV(), 'streaming wmv' => $sp5959db->getBStreamingWMV(), 'flash' => $sp5959db->getBFlash(), 'pdf' => $sp5959db->getBPDF(), 'isp' => $sp4e3b33->getNetworkInfo()->getIsp()); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Type",
          "operator":"IS",
          "value":"Computer"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); } public function testDeviceType_MatchIsComputer_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Type",
          "operator":"IS",
          "value":"Computer"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceType_MismatchIsComputer_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Type",
          "operator":"IS",
          "value":"Computer"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceType_MatchIsNotComputer_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Type",
          "operator":"IS NOT",
          "value":"Computer"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceType_MismatchIsNotComputer_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Type",
          "operator":"IS NOT",
          "value":"Computer"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceOS_MatchIsMac_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: OS",
          "operator":"IS",
          "value":"Mac OS X"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceOS_MismatchIsMac_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: OS",
          "operator":"IS",
          "value":"Mac OS X"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceOS_MatchIsNotMac_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: OS",
          "operator":"IS NOT",
          "value":"Mac OS X"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceOS_MismatchIsNotMac_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: OS",
          "operator":"IS NOT",
          "value":"Mac OS X"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceOSVersion_MatchIs10_11_4_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: OS Version",
          "operator":"IS",
          "value":"10.11.4"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceOSVersion_MismatchIs10_11_4_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: OS Version",
          "operator":"IS",
          "value":"10.11.4"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceOSVersion_MatchIsNot10_11_4_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: OS Version",
          "operator":"IS NOT",
          "value":"10.11.4"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceOSVersion_MismatchIsNot10_11_4_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: OS Version",
          "operator":"IS NOT",
          "value":"10.11.4"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceBrowser_MatchIsChrome_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Browser",
          "operator":"IS",
          "value":"Chrome"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceBrowser_MismatchIsChrome_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:41.0) Gecko/20100101 Firefox/41.0', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Browser",
          "operator":"IS",
          "value":"Chrome"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceBrowser_MatchIsNotChrome_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Browser",
          "operator":"IS NOT",
          "value":"Chrome"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceBrowser_MismatchIsNotChrome_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Browser",
          "operator":"IS NOT",
          "value":"Chrome"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceBrowserVersion_MatchIs49_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Browser Version",
          "operator":"IS",
          "value":"49.0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceBrowserVersion_MismatchIs49_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:41.0) Gecko/20100101 Firefox/41.0', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Browser Version",
          "operator":"IS",
          "value":"49.0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceBrowserVersion_MatchIsNot49_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Browser Version",
          "operator":"IS NOT",
          "value":"49.0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceBrowserVersion_MismatchIsNot49_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Browser Version",
          "operator":"IS NOT",
          "value":"49.0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDisplayWidth_MatchIs720_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Display Width",
          "operator":"IS",
          "value":"720"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDisplayWidth_MismatchIs720_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Display Width",
          "operator":"IS",
          "value":"720"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDisplayWidth_MatchIsNot720_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Display Width",
          "operator":"IS NOT",
          "value":"720"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDisplayWidth_MismatchIsNot720_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Display Width",
          "operator":"IS NOT",
          "value":"720"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDisplayHeight_MatchIs1280_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Display Height",
          "operator":"IS",
          "value":"1280"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDisplayHeight_MismatchIs1280_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Display Height",
          "operator":"IS",
          "value":"1280"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDisplayHeight_MatchIsNot1280_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Display Height",
          "operator":"IS NOT",
          "value":"1280"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDisplayHeight_MismatchIsNot1280_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Display Height",
          "operator":"IS NOT",
          "value":"1280"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceMarketingName_MatchIsZenfone5_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Name",
          "operator":"IS",
          "value":"Zenfone 5"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceMarketingName_MismatchIsZenfone5_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Name",
          "operator":"IS",
          "value":"Zenfone 5"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceMarketingName_MatchIsNotZenfone5_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Name",
          "operator":"IS NOT",
          "value":"Zenfone 5"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceMarketingName_MismatchIsNotZenfone5_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Name",
          "operator":"IS NOT",
          "value":"Zenfone 5"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceManufacturer_MatchIsAsus_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Manufacturer",
          "operator":"IS",
          "value":"Asus"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceManufacturer_MismatchIsAsus_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Manufacturer",
          "operator":"IS",
          "value":"Asus"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceManufacturer_MatchIsNotAsus_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Manufacturer",
          "operator":"IS NOT",
          "value":"Asus"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceManufacturer_MismatchIsNotAsus_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Manufacturer",
          "operator":"IS NOT",
          "value":"Asus"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDevicePointingMethod_MatchIsTouchscreen_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Pointing Method",
          "operator":"IS",
          "value":"Touchscreen"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDevicePointingMethod_MismatchIsTouchscreen_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Pointing Method",
          "operator":"IS",
          "value":"Touchscreen"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDevicePointingMethod_MatchIsNotTouchscreen_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Pointing Method",
          "operator":"IS NOT",
          "value":"Touchscreen"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDevicePointingMethod_MismatchIsNotTouchscreen_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Pointing Method",
          "operator":"IS NOT",
          "value":"Touchscreen"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceMaxSpeedRate_MatchIs3600_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Connection Speed",
          "operator":"IS",
          "value":"3600"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceMaxSpeedRate_MismatchIs3600_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Connection Speed",
          "operator":"IS",
          "value":"3600"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceMaxSpeedRate_MatchIsNot3600_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Connection Speed",
          "operator":"IS NOT",
          "value":"3600"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceMaxSpeedRate_MismatchIsNot3600_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Connection Speed",
          "operator":"IS NOT",
          "value":"3600"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceWifiSupport_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Wifi Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceWifiSupport_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Wifi Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceWifiSupport_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Wifi Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceWifiSupport_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Wifi Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceNFCSupport_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: NFC Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceNFCSupport_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: NFC Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceNFCSupport_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: NFC Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceNFCSupport_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: NFC Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDualOrientation_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Dual Orientation Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDualOrientation_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Dual Orientation Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDualOrientation_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Dual Orientation Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceDualOrientation_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Dual Orientation Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceCookiesSupport_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Cookies Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceCookiesSupport_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Cookies Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDevice3rdPartyCookies_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: 3rd-Party Cookies Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDevice3rdPartyCookies_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; nb-no) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: 3rd-Party Cookies Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDevice3rdPartyCookies_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; nb-no) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: 3rd-Party Cookies Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDevice3rdPartyCookies_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: 3rd-Party Cookies Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceIframes_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: iFrames Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceIframes_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: iFrames Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceIframes_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: iFrames Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceIframes_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS ZenFone 2 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: iFrames Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceVideoEmbed_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Embedded Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceVideoEmbed_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Embedded Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceVideoEmbed_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Embedded Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceVideoEmbed_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Embedded Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceHTML5_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: HTML 5 Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceHTML5_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: HTML 5 Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceHTML5_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: HTML 5 Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceHTML5_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: HTML 5 Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceJavascript_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Javascript Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceJavascript_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Javascript Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceJavascript_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Javascript Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceJavascript_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Javascript Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2Call_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-Call Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2Call_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-Call Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2Call_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-Call Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2Call_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-Call Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2SMS_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-SMS Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2SMS_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-SMS Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2SMS_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-SMS Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2SMS_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-SMS Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2MMS_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-MMS Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2MMS_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-MMS Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2MMS_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-MMS Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceClick2MMS_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Click-To-MMS Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamMP4_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: MP4 Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamMP4_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: MP4 Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamMP4_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: MP4 Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamMP4_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: MP4 Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamMOV_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: MOV Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamMOV_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: MOV Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamMOV_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: MOV Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamMOV_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: MOV Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamFLV_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: FLV Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamFLV_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: FLV Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamFLV_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: FLV Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamFLV_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: FLV Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamWMV_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: WMV Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamWMV_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: WMV Videos Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamWMV_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: WMV Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceStreamWMV_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: WMV Videos Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceSupportFlash_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Flash Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceSupportFlash_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Flash Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceSupportFlash_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Flash Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceSupportFlash_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: Flash Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceSupportPDF_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: PDF Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceSupportPDF_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: PDF Support",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceSupportPDF_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Nokia5250/10.0.011 (SymbianOS/9.4; U; Series60/5.0 Mozilla/5.0; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Safari/525 3gpp-gba', true); $sp5959db->setBWifi(0); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: PDF Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceSupportPDF_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $sp5959db = VisitorDeviceInfo::getFromUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', true); $sp4e3b33->setDeviceInfo($sp5959db); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Device: PDF Support",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } private function sp79287d($spd76052) { $sp522465 = explode('_', $spd76052); $sp522465 = strtolower($sp522465[sizeof($sp522465) - 1]); return $sp522465 === 'true' ? true : false; } }