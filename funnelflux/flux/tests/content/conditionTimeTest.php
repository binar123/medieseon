<?php
require_once dirname(__FILE__) . '/../../content/condition.php'; require_once dirname(__FILE__) . '/../../tracking/visitorInfo.php'; require_once dirname(__FILE__) . '/../../includes/session.php'; class ConditionTimeTest extends PHPUnit_Framework_TestCase { private $day_of_week; private $day_of_month; private $month_of_year; private $time_of_day; private $cur_year; private $cur_month; private $cur_day; private $cur_weekday; private $cur_hour; private $cur_minute; public function setUp() { @session_start(); parent::setUp(); $sp6c8593 = VisitorInfo::getNew(); $sp4e3b33 = $sp6c8593->getVisitorInfoAdvanced(); $spda41ab = $sp4e3b33 ? $sp4e3b33->getLocationInfo() : null; $sp414c8d = $spda41ab ? $spda41ab->getTimezone() : null; $sp642d4b = time(); if ($sp414c8d) { $sp642d4b = toTimeZone($sp642d4b, $sp414c8d); } $this->day_of_week = ''; $this->day_of_month = ''; $this->month_of_year = ''; $this->time_of_day = ''; $this->cur_year = date('Y', $sp642d4b); $this->cur_month = date('n', $sp642d4b); $this->cur_day = date('j', $sp642d4b); $this->cur_weekday = date('w', $sp642d4b); $this->cur_hour = date('G', $sp642d4b); $this->cur_minute = date('i', $sp642d4b); } public function testTimeDate_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Date",
          "operator":"IS",
          "value":"' . $this->cur_year . '",
          "value2":"' . $this->cur_month . '",
          "value3":"' . $this->cur_day . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDate_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Date",
          "operator":"IS",
          "value":"2015",
          "value2":"' . $this->cur_month . '",
          "value3":"' . $this->cur_day . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDate_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Date",
          "operator":"IS NOT",
          "value":"2015",
          "value2":"' . $this->cur_month . '",
          "value3":"' . $this->cur_day . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDate_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Date",
          "operator":"IS NOT",
          "value":"' . $this->cur_year . '",
          "value2":"' . $this->cur_month . '",
          "value3":"' . $this->cur_day . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDayOfWeek_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Day of Week",
          "operator":"IS",
          "value":"' . $this->cur_weekday . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDayOfWeek_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Day of Week",
          "operator":"IS",
          "value":"5"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDayOfWeek_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Day of Week",
          "operator":"IS NOT",
          "value":"5"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDayOfWeek_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Day of Week",
          "operator":"IS NOT",
          "value":"' . $this->cur_weekday . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDayOfMonth_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Day of Month",
          "operator":"IS",
          "value":"' . $this->cur_day . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDayOfMonth_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Day of Month",
          "operator":"IS",
          "value":"0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDayOfMonth_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Day of Month",
          "operator":"IS NOT",
          "value":"0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeDayOfMonth_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Day of Month",
          "operator":"IS NOT",
          "value":"' . $this->cur_day . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeMonthOfYear_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Month of Year",
          "operator":"IS",
          "value":"' . $this->cur_month . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeMonthOfYear_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Month of Year",
          "operator":"IS",
          "value":"0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeMonthOfYear_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Month of Year",
          "operator":"IS NOT",
          "value":"0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeMonthOfYear_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Month of Year",
          "operator":"IS NOT",
          "value":"' . $this->cur_month . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeTimeOfDay_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Time of Day",
          "operator":"IS",
          "value":"' . $this->cur_hour . '",
          "value2":"' . $this->cur_minute . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeTimeOfDay_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Time of Day",
          "operator":"IS",
          "value":"0",
          "value2":"0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeTimeOfDay_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Time of Day",
          "operator":"IS NOT",
          "value":"0",
          "value2":"0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTimeTimeOfDay_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Time: Time of Day",
          "operator":"IS NOT",
          "value":"' . $this->cur_hour . '",
          "value2":"' . $this->cur_minute . '"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } private function sp79287d($spd76052) { $sp522465 = explode('_', $spd76052); $sp522465 = strtolower($sp522465[sizeof($sp522465) - 1]); return $sp522465 === 'true' ? true : false; } }