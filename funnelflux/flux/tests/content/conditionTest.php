<?php
require_once dirname(__FILE__) . '/../../content/condition.php'; require_once dirname(__FILE__) . '/../../tracking/visitorInfo.php'; require_once dirname(__FILE__) . '/../../includes/session.php'; class ConditionTest extends PHPUnit_Framework_TestCase { public function setUp() { @session_start(); parent::setUp(); } public function testLocationContinent_MatchIs_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Continent",
          "operator":"IS",
          "value":"Asia"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(TRUE, $sp64ccca); } public function testLocationContinent_MismatchIs_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('41.57.128.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Continent",
          "operator":"IS",
          "value":"Asia"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(FALSE, $sp64ccca); } public function testLocationContinent_MatchIsNot_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('70.0.0.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Continent",
          "operator":"IS NOT",
          "value":"Asia"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(TRUE, $sp64ccca); } public function testLocationContinent_MismatchIsNot_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Continent",
          "operator":"IS NOT",
          "value":"Asia"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(FALSE, $sp64ccca); } public function testLocationContinent_MatchIsMultiple_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('41.57.128.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Continent",
          "operator":"IS",
          "value":"Asia,,Africa"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(TRUE, $sp64ccca); } public function testLocationContinent_MismatchIsMultiple_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('41.57.128.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Continent",
          "operator":"IS",
          "value":"Asia,,North America"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(FALSE, $sp64ccca); } public function testLocationContinent_MatchOr_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('41.57.128.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Continent",
          "operator":"IS",
          "value":"North America"
        },
        {
          "test":"Location: Continent",
          "operator":"IS",
          "value":"Asia,,Africa"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(TRUE, $sp64ccca); } }