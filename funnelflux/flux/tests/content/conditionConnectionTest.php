<?php
require_once dirname(__FILE__) . '/../../content/condition.php'; require_once dirname(__FILE__) . '/../../tracking/visitorInfo.php'; require_once dirname(__FILE__) . '/../../includes/session.php'; class ConditionConnectionTest extends PHPUnit_Framework_TestCase { public function setUp() { @session_start(); parent::setUp(); } public function testDeviceConnectionIP_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: IP",
          "operator":"IS",
          "value":"210.1.82*"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionIP_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: IP",
          "operator":"IS",
          "value":"24.158.0.0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionIP_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: IP",
          "operator":"IS NOT",
          "value":"24.158.0.0"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionIP_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: IP",
          "operator":"IS NOT",
          "value":"210.1.82*"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionISP_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: ISP",
          "operator":"IS",
          "value":"Philippine Long Distance Telephone Company"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionISP_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('24.39.0.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: ISP",
          "operator":"IS",
          "value":"Globe"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionISP_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('24.39.0.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: ISP",
          "operator":"IS NOT",
          "value":"Philippine Long Distance Telephone Company"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionISP_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: ISP",
          "operator":"IS NOT",
          "value":"Philippine Long Distance Telephone Company"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionUA_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp6c8593->setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: User Agent",
          "operator":"IS",
          "value":"Mozilla/5.0 (Macintosh;*"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionUA_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('24.39.0.0'); $sp6c8593->setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: User Agent",
          "operator":"IS",
          "value":"Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionUA_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('24.39.0.0'); $sp6c8593->setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: User Agent",
          "operator":"IS NOT",
          "value":"Mozilla/5.0 (Linux; Android 5.0; ASUS_T00J Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.95 Mobile Safari/537.36"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionUA_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $sp6c8593->setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: User Agent",
          "operator":"IS NOT",
          "value":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionReferer_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $_SERVER['HTTP_REFERER'] = 'http://madbeagle.com/'; Session::saveParam(Session::KEY_INITIAL_REFERRER, filter_input_2(INPUT_SERVER, 'HTTP_REFERER')); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: Referrer",
          "operator":"IS",
          "value":"http://madbeagle.com/"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionReferer_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('24.39.0.0'); $_SERVER['HTTP_REFERER'] = 'http://madbeagle.com/'; Session::saveParam(Session::KEY_INITIAL_REFERRER, filter_input_2(INPUT_SERVER, 'HTTP_REFERER')); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: Referrer",
          "operator":"IS",
          "value":"http://somedomain.com/"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionReferer_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('24.39.0.0'); $_SERVER['HTTP_REFERER'] = 'http://madbeagle.com/'; Session::saveParam(Session::KEY_INITIAL_REFERRER, filter_input_2(INPUT_SERVER, 'HTTP_REFERER')); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: Referrer",
          "operator":"IS NOT",
          "value":"http://somedomain.com/"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionReferer_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $_SERVER['HTTP_REFERER'] = 'http://madbeagle.com/'; Session::saveParam(Session::KEY_INITIAL_REFERRER, filter_input_2(INPUT_SERVER, 'HTTP_REFERER')); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: Referrer",
          "operator":"IS NOT",
          "value":"http://madbeagle.com/"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionCurrentURL_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $_SERVER['HTTPS'] = 'off'; $_SERVER['HTTP_HOST'] = 'madbeagle.com'; $_SERVER['REQUEST_URI'] = '/'; Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: Current URL",
          "operator":"IS",
          "value":"http://madbeagle.com/"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionCurrentURL_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('24.39.0.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: Current URL",
          "operator":"IS",
          "value":"http://somedomain.com/"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionCurrentURL_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('24.39.0.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: Current URL",
          "operator":"IS NOT",
          "value":"http://somedomain.com/"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testDeviceConnectionCurrentURL_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); $_SERVER['HTTP'] = 'off'; $_SERVER['HTTP_HOST'] = 'madbeagle.com'; $_SERVER['REQUEST_URI'] = '/'; Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Connection: Current URL",
          "operator":"IS NOT",
          "value":"http://madbeagle.com/"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } private function sp79287d($spd76052) { $sp522465 = explode('_', $spd76052); $sp522465 = strtolower($sp522465[sizeof($sp522465) - 1]); return $sp522465 === 'true' ? true : false; } }