<?php
require_once dirname(__FILE__) . '/../../content/condition.php'; require_once dirname(__FILE__) . '/../../tracking/visitorInfo.php'; require_once dirname(__FILE__) . '/../../includes/session.php'; class ConditionCityTest extends PHPUnit_Framework_TestCase { public function setUp() { @session_start(); parent::setUp(); } public function testLocationCountryCode_MatchIs_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('210.1.82.165'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Country",
          "operator":"IS",
          "value":"PH"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(TRUE, $sp64ccca); } public function testLocationCountryCode_MismatchIs_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('41.57.128.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Country",
          "operator":"IS",
          "value":"PH"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(FALSE, $sp64ccca); } public function testLocationCountryCode_MatchIsNot_True() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('41.57.128.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Country",
          "operator":"IS NOT",
          "value":"US"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(TRUE, $sp64ccca); } public function testLocationCountryCode_MismatchIsNot_False() { $sp6c8593 = VisitorInfo::getNew(); $sp6c8593->setIp('70.0.0.0'); Session::saveParam(Session::KEY_VISITOR_INFO, $sp6c8593); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Location: Country",
          "operator":"IS NOT",
          "value":"US"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals(FALSE, $sp64ccca); } }