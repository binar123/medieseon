<?php
require_once dirname(__FILE__) . '/../../content/condition.php'; require_once dirname(__FILE__) . '/../../tracking/visitorInfo.php'; require_once dirname(__FILE__) . '/../../tracking/incomingTraffic.php'; require_once dirname(__FILE__) . '/../../tracking/incomingTrafficURLParam.php'; require_once dirname(__FILE__) . '/../../includes/session.php'; class ConditionTrafficTest extends PHPUnit_Framework_TestCase { public function setUp() { @session_start(); parent::setUp(); } public function testTrafficSource_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $spe96eed = IncomingTraffic::getNew(1, 13); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Traffic Source",
          "operator":"IS",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTrafficSource_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $spe96eed = IncomingTraffic::getNew(1, 1); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Traffic Source",
          "operator":"IS",
          "value":"2"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTrafficSource_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $spe96eed = IncomingTraffic::getNew(1, 1); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Traffic Source",
          "operator":"IS NOT",
          "value":"2"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTrafficSource_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $spe96eed = IncomingTraffic::getNew(1, 1); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Traffic Source",
          "operator":"IS NOT",
          "value":"1"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTrackingField_MatchIsTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $spe96eed = IncomingTraffic::getNew(1, 13, null); $spa50438 = array(); $spb969d3 = IncomingTrafficURLParam::getEmpty(); $spb969d3->setURLParamField('page'); $spb969d3->setURLParamValue('test'); $spa50438[] = $spb969d3; $spe96eed->setIncomingTrafficURLParams($spa50438); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Tracking Field",
          "operator":"IS",
          "value":"test",
          "value2":"page"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTrackingField_MismatchIsTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $spe96eed = IncomingTraffic::getNew(1, 13, null); $spa50438 = array(); $spb969d3 = IncomingTrafficURLParam::getEmpty(); $spb969d3->setURLParamField('page'); $spb969d3->setURLParamValue('test123'); $spa50438[] = $spb969d3; $spe96eed->setIncomingTrafficURLParams($spa50438); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Tracking Field",
          "operator":"IS",
          "value":"test",
          "value2":"page"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTrackingField_MatchIsNotTrue_True() { $sp6c8593 = VisitorInfo::getNew(); $spe96eed = IncomingTraffic::getNew(1, 13, null); $spa50438 = array(); $spb969d3 = IncomingTrafficURLParam::getEmpty(); $spb969d3->setURLParamField('page'); $spb969d3->setURLParamValue('test123'); $spa50438[] = $spb969d3; $spe96eed->setIncomingTrafficURLParams($spa50438); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Tracking Field",
          "operator":"IS NOT",
          "value":"test",
          "value2":"page"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } public function testTrackingField_MismatchIsNotTrue_False() { $sp6c8593 = VisitorInfo::getNew(); $spe96eed = IncomingTraffic::getNew(1, 13, null); $spa50438 = array(); $spb969d3 = IncomingTrafficURLParam::getEmpty(); $spb969d3->setURLParamField('page'); $spb969d3->setURLParamValue('test'); $spa50438[] = $spb969d3; $spe96eed->setIncomingTrafficURLParams($spa50438); Session::saveParam(Session::KEY_INCOMING_TRAFFIC, $spe96eed); $sp63bcb0 = json_decode('[
      [
        {
          "test":"Tracking Field",
          "operator":"IS NOT",
          "value":"test",
          "value2":"page"
        }
      ]
    ]', TRUE); $sp1f69f7 = new Condition(); $sp1f69f7->setConditionBlocks($sp63bcb0); $sp64ccca = $sp1f69f7->isConditionFulfilled(null); $this->assertEquals($this->sp79287d(__FUNCTION__), $sp64ccca); } private function sp79287d($spd76052) { $sp522465 = explode('_', $spd76052); $sp522465 = strtolower($sp522465[sizeof($sp522465) - 1]); return $sp522465 === 'true' ? true : false; } }