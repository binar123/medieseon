<?php
require_once realpath(dirname(__FILE__) . '/../TeraWurfl.php'); try { $sp168445 = new TeraWurfl(); } catch (Exception $sp7c4ea2) { } $sp638bd5 = false; $spb8e733 = $sp168445->rootdir . TeraWurflConfig::$DATADIR . 'custom_web_patch.xml'; $sp15bc9f = $sp168445->rootdir . TeraWurflConfig::$DATADIR . 'custom_web_patch_uas.txt'; if (isset($_POST['action']) && $_POST['action'] == 'generate_patch') { $sp9daf9b = '<?xml version="1.0" encoding="utf-8"?>
<wurfl_patch>
	<devices>'; $sp5e99c4 = 0; $spc8a5cc = $_POST['data']; if (get_magic_quotes_gpc()) { $spc8a5cc = stripslashes($spc8a5cc); } $spc8a5cc = preg_replace('/[\\r\\n]+/', '
', $spc8a5cc); $sp2c2ab4 = explode('
', $spc8a5cc); foreach ($sp2c2ab4 as $spa29e51) { $spa29e51 = trim($spa29e51); if ($spa29e51 == '') { continue; } $sp9daf9b .= '
		' . '<device user_agent="' . htmlspecialchars($spa29e51) . '" fall_back="generic_web_browser" id="terawurfl_generic_web_browser' . $sp5e99c4++ . '"/>'; } $sp9daf9b .= '
	</devices>
</wurfl_patch>'; file_put_contents($sp15bc9f, $spc8a5cc); file_put_contents($spb8e733, $sp9daf9b); $sp638bd5 = true; } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tera-WURFL Custom Patch Generator</title>
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%">
	<tr><td>
	<div align="center" class="titlediv">
	<p>Tera-WURFL Custom Patch Generator<br/>
		<span class="version">Version <?php  echo $sp168445->release_branch . ' ' . $sp168445->release_version; ?>
</span></p>
	</div>
	<?php  if ($sp638bd5) { ?>
<div align="center" class="noticediv" style="width: 100%">Custom patch file saved.  <a href="#patch">View patch file</a></div><?php  } ?>
	</td></tr>
	<tr><td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><th>Enter your non-mobile user agents below</th></tr>
	</table>
	</td></tr>
	<tr><td class="lightrow">Enter your non-mobile user agents below, one per line, and press <strong>Generate Patch File</strong>.  These user agents will be compiled into the Tera-WURFL custom patch file <strong><?php  echo $spb8e733; ?>
</strong>. After you submit the changes, go to the <a href="index.php">Tera-WURFL Administration Page</a> and update your WURFL database to load the new patch file.</td></tr>
	<tr>
		<td><form action="generatePatch.php" method="post">
		<input type="hidden" name="action" value="generate_patch" />
		<textarea name="data" rows="25" cols="97" style="width: 100%;"><?php  echo file_get_contents($sp15bc9f); ?>
</textarea>
		<br/><center><input type="submit" value="Generate Patch File" name="submit" /></center>
		</form></td>
	</tr>
</table>
<pre><a name="patch"></a><?php  if ($sp638bd5) { echo htmlspecialchars(file_get_contents($spb8e733)); } ?>
</pre>
</body>
</html><?php 