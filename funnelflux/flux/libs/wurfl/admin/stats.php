<?php
require_once realpath(dirname(__FILE__) . '/../TeraWurfl.php'); $sp168445 = new TeraWurfl(); $sp033004 = $sp168445->db; $sp1f428d = false; if ($sp033004->connected === true) { $spaf78c1 = array(TeraWurflConfig::$TABLE_PREFIX . 'Cache', TeraWurflConfig::$TABLE_PREFIX . 'Index', TeraWurflConfig::$TABLE_PREFIX . 'Merge'); $sp0c058d = $sp033004->getTableList(); foreach ($spaf78c1 as $sp18d028) { if (!in_array($sp18d028, $sp0c058d)) { $sp1f428d = true; } } } $spb36005 = $sp033004->getTableStats(TeraWurflConfig::$TABLE_PREFIX . 'Merge'); $sp248ebf = $sp033004->getTableStats(TeraWurflConfig::$TABLE_PREFIX . 'Index'); $sp32e0dc = $sp033004->getTableStats(TeraWurflConfig::$TABLE_PREFIX . 'Cache'); $spe98e1a = $sp033004->getMatcherTableList(); $sp8fb301 = array(); foreach ($spe98e1a as $spadff4a) { $sp8fb301[] = array('name' => $spadff4a, 'stats' => $sp033004->getTableStats($spadff4a)); } $spe348c6 = $sp168445->rootdir . TeraWurflConfig::$DATADIR . TeraWurflConfig::$LOG_FILE; if (!is_readable($spe348c6) || filesize($spe348c6) < 5) { $spaa96b1 = 'Empty'; } else { $sp61d76b = file($spe348c6); $sp87f4ac = 30; if (count($sp61d76b) < $sp87f4ac) { $sp87f4ac = count($sp61d76b); } $spba2964 = count($sp61d76b) - 1; $spaa96b1 = ''; for ($sp5e99c4 = $spba2964; $sp5e99c4 >= $spba2964 - $sp87f4ac; $sp5e99c4--) { $spaa96b1 .= @htmlspecialchars($sp61d76b[$sp5e99c4]) . '<br />'; } } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Tera-WURFL Administration</title>
<link href="style.css" rel="stylesheet" type="text/css" /></head>

<body>
<table width="800">
	<tr><td>
<div align="center" class="titlediv">
	<p>		Tera-WURFL <?php  echo $sp168445->release_version; ?>
 Administration<br />
		<span class="version">Loaded WURFL: <?php  echo $sp168445->getSetting(TeraWurfl::$SETTING_WURFL_VERSION); ?>
</span></p>
</div>
</td></tr><tr><td>
		<h3><br />
			<a href="index.php">&lt;&lt; Back	to main page </a></h3>
		<table width="800" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<th scope="col">Database Table </th>
			<th scope="col">Statistics</th>
		</tr>
		<tr>
			<td width="145" class="darkrow">MERGE<br />
					<span class="setting"><?php  echo TeraWurflConfig::$TABLE_PREFIX . 'Merge'; ?>
</span></td>
			<td width="655" class="darkrow">Rows: <span class="setting"><?php  echo $spb36005['rows']; ?>
</span><br />
				Actual Devices: <span class="setting"><?php  echo $spb36005['actual_devices']; ?>
</span> <br />
				Table Size: <span class="setting"><?php  echo WurflSupport::formatBytes($spb36005['bytesize']); ?>
</span><br />
				Purpose:<br />
				<span class="setting">The MERGE table holds all the data from the WURFL file.  When a new WURFL is loaded, it is loaded into this table first, then it is filtered through all the UserAgentMatchers and split into many different tables specific to each matching technique. This MERGE table is retained for a last chance lookup if the UserAgentMatchers and INDEX table are unable to provide a conclusive match.</span></td>
		</tr>
<?php  if (!empty($sp248ebf)) { ?>
		<tr>
			<td class="lightrow">INDEX		<br />
				<span class="setting"><?php  echo TeraWurflConfig::$TABLE_PREFIX . 'Index'; ?>
</span></td>
		  <td class="lightrow">Rows: <span class="setting"><?php  echo $sp248ebf['rows']; ?>
</span><br />
				Table Size: <span class="setting"><?php  echo WurflSupport::formatBytes($sp248ebf['bytesize']); ?>
</span><br />
				Purpose:<br />
				<span class="setting">The INDEX table acts as a lookup table for WURFL IDs and their respective UserAgentMatchers. </span></td>
		</tr>
<?php  } ?>
		<tr>
			<td class="darkrow">CACHE		<br />
				<span class="setting"><?php  echo TeraWurflConfig::$TABLE_PREFIX . 'Cache'; ?>
</span></td>
<td class="darkrow">Rows: <span class="setting"><?php  echo $sp32e0dc['rows']; ?>
</span><br />
				Table Size: <span class="setting"><?php  echo WurflSupport::formatBytes($sp32e0dc['bytesize']); ?>
</span><br />
				Purpose:<br />
				<span class="setting">The CACHE table stores unique user agents and the complete capabilities and device root that were determined when the device was first identified. <strong>Unlike version 1.x</strong>, the CACHE table stores every device that is detected <strong>permanently</strong>. When the device database is updated, the cached devices are also redetected and recached. This behavior is configurable.</span></td>
		</tr>
<?php  if (!empty($sp8fb301)) { ?>
		<tr>
			<td class="lightrow" style="vertical-align:top;">User Agent Matchers<br/>
				Purpose:<br />
				<span class="setting">The User Agent Matchers store similar user agents.  Tera-WURFL sorts all the devices into the most appropriate UserAgentMatcher table to make lookups faster and perform different matching hueristics on certain groups of devices.</span></td><td>
				<table>
<?php  $sp5e99c4 = 0; foreach ($sp8fb301 as $spe3811e) { $spdaa5a2 = $sp5e99c4 % 2 == 0 ? 'lightrow' : 'darkrow'; ?>
<tr><td class="<?php  echo $spdaa5a2; ?>
">UserAgentMatcher: <span class="setting"><?php  echo $spe3811e['name']; ?>
</span><br />
Rows: <span class="setting"><?php  echo $spe3811e['stats']['rows']; ?>
</span><br />
Table Size: <span class="setting"><?php  echo WurflSupport::formatBytes($spe3811e['stats']['bytesize']); ?>
</span></td></tr>
<?php  $sp5e99c4++; } ?>
</table></td>
		</tr>
<?php  } ?>
	</table>
<p><br/>
			<br/>
	</p>
	<table width="800" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th scope="col">Tera-WURFL Settings</th>
		</tr>
		<tr><td>Installation Directory: <span class="setting"><?php  echo dirname(dirname(__FILE__)); ?>
</span></td></tr>
		<tr>
			<td class="lightrow"><p>-- Database options --<br/>
				DB_HOST <span class="setting">
	<?php  echo TeraWurflConfig::$DB_HOST; ?>
	</span>,	database server hostname or IP<br />
				DB_USER <span class="setting">
	<?php  echo TeraWurflConfig::$DB_USER; ?>
	</span>,	database username (needs SELECT,INSERT,DELETE,DROP,CREATE)<br />
				DB_PASS <span class="setting">********</span>, database password<br />
				DB_SCHEMA <span class="setting">
	<?php  echo TeraWurflConfig::$DB_SCHEMA; ?>
	</span>, database schema (database name)<br />
				DB_CONNECTOR <span class="setting">
	<?php  echo TeraWurflConfig::$DB_CONNECTOR; ?>
	</span>, database type (MySQL4, MySQL5, MSSQL2005, etc...);<br />
				TABLE_PREFIX <span class="setting">
	<?php  echo TeraWurflConfig::$TABLE_PREFIX; ?>
	</span>, prefix to be used for all table names<br />
							<br />
					-- General options --<br />
					WURFL_DL_URL <span class="setting">
						<?php  echo TeraWurflConfig::$WURFL_DL_URL; ?>
							</span>, full URL to the current WURFL<br />
					DATADIR <span class="setting">
						<?php  echo TeraWurflConfig::$DATADIR; ?>
			  </span>,	where all data is stored (wurfl.xml, temp files, logs)<br />
					  CACHE_ENABLE <span class="setting"><?php  echo WurflSupport::showBool(TeraWurflConfig::$CACHE_ENABLE); ?>
</span>, enables or disables the cache <br />
					PATCH_ENABLE <span class="setting">
						<?php  echo WurflSupport::showBool(TeraWurflConfig::$PATCH_ENABLE); ?>
				  </span>, enables or disables the patch<br />
					PATCH_FILE <span class="setting">
						<?php  echo TeraWurflConfig::$PATCH_FILE; ?>
</span>, optional patch file for WURFL. To use more than one, separate them with semicolons<br />
					WURFL_FILE <span class="setting">
						<?php  echo TeraWurflConfig::$WURFL_FILE; ?>
						</span>, path and filename of wurfl.xml<br />
					WURFL_LOG_FILE <span class="setting">
						<?php  echo TeraWurflConfig::$LOG_FILE; ?>
						</span>, defines full path and filename for logging<br />
					LOG_LEVEL <span class="setting">
						<?php  echo WurflSupport::showLogLevel(TeraWurflConfig::$LOG_LEVEL); ?>
						</span>, desired logging level. Use the same constants as for PHP logging<br />
					OVERRIDE_MEMORY_LIMIT <span class="setting">
						<?php  echo WurflSupport::showBool(TeraWurflConfig::$OVERRIDE_MEMORY_LIMIT); ?>
						</span>, override PHP's default memory limit<br />
					MEMORY_LIMIT <span class="setting">
						<?php  echo TeraWurflConfig::$MEMORY_LIMIT; ?>
						</span>, the amount of memory to allocate to PHP if OVERRIDE_MEMORY_LIMIT is enabled<br />
					SIMPLE_DESKTOP_ENGINE_ENABLE <span class="setting">
						<?php  echo WurflSupport::showBool(TeraWurflConfig::$SIMPLE_DESKTOP_ENGINE_ENABLE); ?>
						</span>, enable the SimpleDesktop Detection Engine to increase performance<br />
					CAPABILITY_FILTER:
						<?php  echo '<pre class="setting">' . var_export(TeraWurflConfig::$CAPABILITY_FILTER, true) . '</pre>'; ?>
						the capability filter that is used to determine which capabilities are available<br />
			</p>
				</td>
		</tr>
	</table>
	<p>&nbsp;</p>
	<table width="800" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th scope="col">Log File (last 30 lines) </th>
		</tr>
		<tr>
			<td class="lightrow"><div class="logfile"><?php  echo $spaa96b1; ?>
</div>
				<br/></td>
		</tr>
	</table>	<p>&nbsp; </p></td>
</tr></table>
</body>
</html>
<?php 