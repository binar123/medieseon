-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__WindowsRT`
--

DROP TABLE IF EXISTS `ztw__WindowsRT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__WindowsRT` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__WindowsRT`
--

LOCK TABLES `ztw__WindowsRT` WRITE;
/*!40000 ALTER TABLE `ztw__WindowsRT` DISABLE KEYS */;
INSERT INTO `ztw__WindowsRT` VALUES ('windows_8_rt_ver1','Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; ARM; Trident/6.0; Touch)','generic_windows_8_rt',1,1,'a:7:{s:2:\"id\";s:17:\"windows_8_rt_ver1\";s:10:\"user_agent\";s:76:\"Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; ARM; Trident/6.0; Touch)\";s:9:\"fall_back\";s:20:\"generic_windows_8_rt\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:1:{s:10:\"brand_name\";s:9:\"Microsoft\";}s:9:\"streaming\";a:2:{s:13:\"streaming_wmv\";d:9;s:13:\"streaming_mp4\";b:1;}s:6:\"bearer\";a:2:{s:4:\"wifi\";b:1;s:13:\"max_data_rate\";d:7200;}}'),('windows_8_rt_ver1_subos81','Mozilla/5.0 (Windows NT 6.3; ARM; Trident/7.0; Touch; rv:11.0) like Gecko','windows_8_rt_ver1',NULL,1,'a:4:{s:2:\"id\";s:25:\"windows_8_rt_ver1_subos81\";s:10:\"user_agent\";s:73:\"Mozilla/5.0 (Windows NT 6.3; ARM; Trident/7.0; Touch; rv:11.0) like Gecko\";s:9:\"fall_back\";s:17:\"windows_8_rt_ver1\";s:12:\"product_info\";a:2:{s:22:\"mobile_browser_version\";s:4:\"11.0\";s:17:\"device_os_version\";d:8.0999999999999996;}}'),('windows_8_rt_ver1_subos81app','Mozilla/5.0 (Windows NT 6.3; ARM; Trident/7.0; Touch; WebView/2.0; rv:11.0) like Gecko','windows_8_rt_ver1',NULL,1,'a:5:{s:2:\"id\";s:28:\"windows_8_rt_ver1_subos81app\";s:10:\"user_agent\";s:86:\"Mozilla/5.0 (Windows NT 6.3; ARM; Trident/7.0; Touch; WebView/2.0; rv:11.0) like Gecko\";s:9:\"fall_back\";s:17:\"windows_8_rt_ver1\";s:12:\"product_info\";a:2:{s:22:\"mobile_browser_version\";s:4:\"11.0\";s:17:\"device_os_version\";d:8.0999999999999996;}s:7:\"virtual\";a:1:{s:17:\"controlcap_is_app\";s:10:\"force_true\";}}');
/*!40000 ALTER TABLE `ztw__WindowsRT` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
