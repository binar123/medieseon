-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Toshiba`
--

DROP TABLE IF EXISTS `ztw__Toshiba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Toshiba` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Toshiba`
--

LOCK TABLES `ztw__Toshiba` WRITE;
/*!40000 ALTER TABLE `ztw__Toshiba` DISABLE KEYS */;
INSERT INTO `ztw__Toshiba` VALUES ('toshiba_ts605_ver1','Toshiba_TS605/v1.0 UP.Browser/6.2.3.9.d.3.103 (GUI) MMP/2.0','opwv_v62_generic',1,1,'a:8:{s:2:\"id\";s:18:\"toshiba_ts605_ver1\";s:10:\"user_agent\";s:59:\"Toshiba_TS605/v1.0 UP.Browser/6.2.3.9.d.3.103 (GUI) MMP/2.0\";s:9:\"fall_back\";s:16:\"opwv_v62_generic\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:3:{s:10:\"brand_name\";s:7:\"Toshiba\";s:10:\"model_name\";s:6:\"TS 605\";s:6:\"uaprof\";s:71:\"http://gphone.toshiba.co.jp/tech/profiles/UAPROF/GPRS/Toshiba_TS605.xml\";}s:6:\"markup\";a:2:{s:19:\"xhtml_support_level\";d:3;s:16:\"preferred_markup\";s:23:\"html_wi_oma_xhtmlmp_1_0\";}s:7:\"display\";a:8:{s:7:\"columns\";d:15;s:16:\"max_image_height\";d:220;s:15:\"max_image_width\";d:169;s:17:\"resolution_height\";d:220;s:16:\"resolution_width\";d:176;s:4:\"rows\";d:5;s:21:\"physical_screen_width\";d:31;s:22:\"physical_screen_height\";d:38;}s:6:\"bearer\";a:1:{s:13:\"max_data_rate\";d:40;}}'),('toshiba_ts608_ver1','ToshibaTS608','opwv_v62_generic',1,1,'a:6:{s:2:\"id\";s:18:\"toshiba_ts608_ver1\";s:10:\"user_agent\";s:12:\"ToshibaTS608\";s:9:\"fall_back\";s:16:\"opwv_v62_generic\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:7:\"Toshiba\";s:10:\"model_name\";s:6:\"TS 608\";}s:7:\"display\";a:6:{s:16:\"resolution_width\";d:176;s:17:\"resolution_height\";d:220;s:15:\"max_image_width\";d:169;s:16:\"max_image_height\";d:220;s:21:\"physical_screen_width\";d:31;s:22:\"physical_screen_height\";d:39;}}'),('toshiba_ts608_ver1_subv106239d1','Toshiba TS608_TS30/v1.0 UP.Browser/6.2.3.9.d.1 (GUI) MMP/2.0','toshiba_ts608_ver1',NULL,1,'a:3:{s:2:\"id\";s:31:\"toshiba_ts608_ver1_subv106239d1\";s:10:\"user_agent\";s:60:\"Toshiba TS608_TS30/v1.0 UP.Browser/6.2.3.9.d.1 (GUI) MMP/2.0\";s:9:\"fall_back\";s:18:\"toshiba_ts608_ver1\";}'),('toshiba_ts608_ver1_simi','Toshiba TS608;TS30/v1.0','toshiba_ts608_ver1',1,1,'a:7:{s:2:\"id\";s:23:\"toshiba_ts608_ver1_simi\";s:10:\"user_agent\";s:23:\"Toshiba TS608;TS30/v1.0\";s:9:\"fall_back\";s:18:\"toshiba_ts608_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:3:{s:10:\"brand_name\";s:7:\"Toshiba\";s:10:\"model_name\";s:10:\"TS608_TS30\";s:6:\"uaprof\";s:70:\"http://gphone.toshiba.co.jp/tech/profiles/UAPROF/GPRS/Toshiba_GS20.xml\";}s:7:\"display\";a:4:{s:15:\"max_image_width\";d:166;s:16:\"max_image_height\";d:215;s:7:\"columns\";d:15;s:4:\"rows\";d:5;}s:6:\"bearer\";a:1:{s:13:\"max_data_rate\";d:40;}}'),('toshiba_ts10_ver1','ToshibaTS10','generic_mobile',1,1,'a:8:{s:2:\"id\";s:17:\"toshiba_ts10_ver1\";s:10:\"user_agent\";s:11:\"ToshibaTS10\";s:9:\"fall_back\";s:14:\"generic_mobile\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:4:{s:10:\"brand_name\";s:7:\"Toshiba\";s:10:\"model_name\";s:4:\"TS10\";s:14:\"mobile_browser\";s:23:\"Openwave Mobile Browser\";s:22:\"mobile_browser_version\";s:7:\"6.2.3.9\";}s:6:\"markup\";a:1:{s:16:\"preferred_markup\";s:7:\"wml_1_3\";}s:7:\"display\";a:4:{s:16:\"resolution_width\";d:128;s:17:\"resolution_height\";d:160;s:15:\"max_image_width\";d:121;s:16:\"max_image_height\";d:160;}s:6:\"bearer\";a:1:{s:13:\"max_data_rate\";d:40;}}'),('toshiba_ts10_ver1_c6239d1','Toshiba-TS10/1.0 UP.Browser/6.2.3.9.d.1 (GUI) MMP/2.0','toshiba_ts10_ver1',NULL,1,'a:5:{s:2:\"id\";s:25:\"toshiba_ts10_ver1_c6239d1\";s:10:\"user_agent\";s:53:\"Toshiba-TS10/1.0 UP.Browser/6.2.3.9.d.1 (GUI) MMP/2.0\";s:9:\"fall_back\";s:17:\"toshiba_ts10_ver1\";s:7:\"display\";a:4:{s:7:\"columns\";d:16;s:17:\"resolution_height\";d:160;s:16:\"resolution_width\";d:128;s:4:\"rows\";d:8;}s:12:\"product_info\";a:1:{s:6:\"uaprof\";s:70:\"http://gphone.toshiba.co.jp/tech/profiles/UAPROF/GPRS/Toshiba_TS10.xml\";}}');
/*!40000 ALTER TABLE `ztw__Toshiba` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
