-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__SmartTV`
--

DROP TABLE IF EXISTS `ztw__SmartTV`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__SmartTV` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__SmartTV`
--

LOCK TABLES `ztw__SmartTV` WRITE;
/*!40000 ALTER TABLE `ztw__SmartTV` DISABLE KEYS */;
INSERT INTO `ztw__SmartTV` VALUES ('generic_smarttv_browser','DO_NOT_MATCH_GENERIC_SMARTTV','generic_web_browser',NULL,0,'a:7:{s:2:\"id\";s:23:\"generic_smarttv_browser\";s:10:\"user_agent\";s:28:\"DO_NOT_MATCH_GENERIC_SMARTTV\";s:9:\"fall_back\";s:19:\"generic_web_browser\";s:12:\"product_info\";a:4:{s:15:\"pointing_method\";s:0:\"\";s:10:\"model_name\";s:7:\"SmartTV\";s:10:\"brand_name\";s:7:\"Generic\";s:12:\"release_date\";s:12:\"2011_january\";}s:7:\"display\";a:2:{s:15:\"max_image_width\";d:650;s:16:\"resolution_width\";d:685;}s:7:\"smarttv\";a:1:{s:10:\"is_smarttv\";b:1;}s:15:\"user_experience\";a:1:{s:15:\"ux_full_desktop\";b:0;}}'),('generic_smarttv_googletv_browser','DO_NOT_MATCH_GENERIC_GOOGLETV','generic_smarttv_browser',NULL,0,'a:4:{s:2:\"id\";s:32:\"generic_smarttv_googletv_browser\";s:10:\"user_agent\";s:29:\"DO_NOT_MATCH_GENERIC_GOOGLETV\";s:9:\"fall_back\";s:23:\"generic_smarttv_browser\";s:12:\"product_info\";a:1:{s:10:\"model_name\";s:8:\"GoogleTV\";}}'),('generic_smarttv_boxeebox_browser','DO_NOT_MATCH_GENERIC_BOXEEBOX','generic_smarttv_browser',NULL,0,'a:4:{s:2:\"id\";s:32:\"generic_smarttv_boxeebox_browser\";s:10:\"user_agent\";s:29:\"DO_NOT_MATCH_GENERIC_BOXEEBOX\";s:9:\"fall_back\";s:23:\"generic_smarttv_browser\";s:12:\"product_info\";a:1:{s:10:\"model_name\";s:9:\"Boxee Box\";}}'),('generic_smarttv_appletv_browser','DO_NOT_MATCH_GENERIC_APPLETV','generic_smarttv_browser',NULL,0,'a:5:{s:2:\"id\";s:31:\"generic_smarttv_appletv_browser\";s:10:\"user_agent\";s:28:\"DO_NOT_MATCH_GENERIC_APPLETV\";s:9:\"fall_back\";s:23:\"generic_smarttv_browser\";s:12:\"product_info\";a:2:{s:10:\"model_name\";s:2:\"TV\";s:10:\"brand_name\";s:5:\"Apple\";}s:10:\"flash_lite\";a:1:{s:18:\"full_flash_support\";b:0;}}'),('generic_smarttv_chromecast','Mozilla/5.0 (CrKey armv7l 1.4.15250) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.0 Safari/537.36','generic_smarttv_browser',NULL,1,'a:5:{s:2:\"id\";s:26:\"generic_smarttv_chromecast\";s:10:\"user_agent\";s:108:\"Mozilla/5.0 (CrKey armv7l 1.4.15250) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.0 Safari/537.36\";s:9:\"fall_back\";s:23:\"generic_smarttv_browser\";s:12:\"product_info\";a:3:{s:10:\"model_name\";s:10:\"Chromecast\";s:10:\"brand_name\";s:6:\"Google\";s:12:\"release_date\";s:9:\"2013_july\";}s:7:\"virtual\";a:1:{s:31:\"controlcap_advertised_device_os\";s:10:\"Chromecast\";}}'),('apple_tv_ver1','AppleTV','generic_smarttv_appletv_browser',1,1,'a:5:{s:2:\"id\";s:13:\"apple_tv_ver1\";s:10:\"user_agent\";s:7:\"AppleTV\";s:9:\"fall_back\";s:31:\"generic_smarttv_appletv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:5:\"Apple\";s:10:\"model_name\";s:2:\"TV\";}}'),('lg_netcast_ver1','Linux/2.6.31-1.0 UPnP/1.0 DLNADOC/1.50 INTEL_NMPR/2.0 LGE_DLNA_SDK/1.5.0','generic_smarttv_browser',1,1,'a:5:{s:2:\"id\";s:15:\"lg_netcast_ver1\";s:10:\"user_agent\";s:72:\"Linux/2.6.31-1.0 UPnP/1.0 DLNADOC/1.50 INTEL_NMPR/2.0 LGE_DLNA_SDK/1.5.0\";s:9:\"fall_back\";s:23:\"generic_smarttv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:2:\"LG\";s:10:\"model_name\";s:7:\"NetCast\";}}'),('samsung_smart_tv_ver1','Mozilla/5.0 (SmartHub; SMART-TV; U; Linux/SmartTV) AppleWebKit/531.2+ (KHTML, Like Gecko) WebBrowser/1.0 SmartTV Safari/531.2+','generic_smarttv_browser',1,1,'a:5:{s:2:\"id\";s:21:\"samsung_smart_tv_ver1\";s:10:\"user_agent\";s:126:\"Mozilla/5.0 (SmartHub; SMART-TV; U; Linux/SmartTV) AppleWebKit/531.2+ (KHTML, Like Gecko) WebBrowser/1.0 SmartTV Safari/531.2+\";s:9:\"fall_back\";s:23:\"generic_smarttv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:7:\"Samsung\";s:10:\"model_name\";s:8:\"Smart TV\";}}'),('panasonic_viera_cast_ver1','Panasonic MIL DLNA CP UPnP/1.0 DLNADOC/1.50','generic_smarttv_browser',1,1,'a:5:{s:2:\"id\";s:25:\"panasonic_viera_cast_ver1\";s:10:\"user_agent\";s:43:\"Panasonic MIL DLNA CP UPnP/1.0 DLNADOC/1.50\";s:9:\"fall_back\";s:23:\"generic_smarttv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:9:\"Panasonic\";s:10:\"model_name\";s:10:\"Viera Cast\";}}'),('sharp_aquos_net_plus_ver1','DLNADOC/1.50 SHARP-AQUOS-DMP/1.1W','generic_smarttv_browser',1,1,'a:5:{s:2:\"id\";s:25:\"sharp_aquos_net_plus_ver1\";s:10:\"user_agent\";s:33:\"DLNADOC/1.50 SHARP-AQUOS-DMP/1.1W\";s:9:\"fall_back\";s:23:\"generic_smarttv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:5:\"Sharp\";s:10:\"model_name\";s:14:\"Aquos Net Plus\";}}'),('logitech_revue_ver1','Mozilla/5.0 (X11; U; Linux i686; xx-xx) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.127 Large Screen Safari/533.4 GoogleTV/b39389','generic_smarttv_googletv_browser',1,1,'a:5:{s:2:\"id\";s:19:\"logitech_revue_ver1\";s:10:\"user_agent\";s:138:\"Mozilla/5.0 (X11; U; Linux i686; xx-xx) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.127 Large Screen Safari/533.4 GoogleTV/b39389\";s:9:\"fall_back\";s:32:\"generic_smarttv_googletv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:8:\"Logitech\";s:10:\"model_name\";s:5:\"Revue\";}}'),('sony_internet_tv_ver1','Mozilla/5.0 (X11; U; Linux i686; xx-xx) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.127 Large Screen Safari/533.4 GoogleTV/162671','generic_smarttv_googletv_browser',1,1,'a:5:{s:2:\"id\";s:21:\"sony_internet_tv_ver1\";s:10:\"user_agent\";s:138:\"Mozilla/5.0 (X11; U; Linux i686; xx-xx) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.127 Large Screen Safari/533.4 GoogleTV/162671\";s:9:\"fall_back\";s:32:\"generic_smarttv_googletv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:4:\"Sony\";s:10:\"model_name\";s:11:\"Internet TV\";}}'),('logitech_revue_ver1_subua','Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.77 Large Screen Safari/534.24 GoogleTV/b61925','logitech_revue_ver1',NULL,1,'a:4:{s:2:\"id\";s:25:\"logitech_revue_ver1_subua\";s:10:\"user_agent\";s:130:\"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.77 Large Screen Safari/534.24 GoogleTV/b61925\";s:9:\"fall_back\";s:19:\"logitech_revue_ver1\";s:12:\"product_info\";a:1:{s:12:\"release_date\";s:13:\"2011_november\";}}'),('apple_tv_ver1_sub442','AppleTV/4.4.2 Darwin/11.0.0','apple_tv_ver1',NULL,1,'a:3:{s:2:\"id\";s:20:\"apple_tv_ver1_sub442\";s:10:\"user_agent\";s:27:\"AppleTV/4.4.2 Darwin/11.0.0\";s:9:\"fall_back\";s:13:\"apple_tv_ver1\";}'),('vizio_costar_ver1','Mozilla/5.0 (Linux; GoogleTV 3.2; VAP430 Build/MASTER) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.77 Safari/534.24','generic_smarttv_googletv_browser',1,1,'a:5:{s:2:\"id\";s:17:\"vizio_costar_ver1\";s:10:\"user_agent\";s:126:\"Mozilla/5.0 (Linux; GoogleTV 3.2; VAP430 Build/MASTER) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.77 Safari/534.24\";s:9:\"fall_back\";s:32:\"generic_smarttv_googletv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:3:{s:10:\"model_name\";s:7:\"Co-Star\";s:10:\"brand_name\";s:5:\"Vizio\";s:17:\"device_os_version\";d:3.2000000000000002;}}'),('lg_google_tv_ver1','Mozilla/5.0 (Linux; GoogleTV 3.2; LG Google TV Build/MASTER) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.77 Safari/534.24','generic_smarttv_googletv_browser',1,1,'a:5:{s:2:\"id\";s:17:\"lg_google_tv_ver1\";s:10:\"user_agent\";s:132:\"Mozilla/5.0 (Linux; GoogleTV 3.2; LG Google TV Build/MASTER) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.77 Safari/534.24\";s:9:\"fall_back\";s:32:\"generic_smarttv_googletv_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:2:\"LG\";s:17:\"device_os_version\";d:3.2000000000000002;}}'),('sony_bravia_4k_2015_ver1','Mozilla/5.0 (Linux; Android 5.1; BRAVIA 4K 2015 Build/LMY48E.S153) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36','generic_android_ver5_1',1,1,'a:6:{s:2:\"id\";s:24:\"sony_bravia_4k_2015_ver1\";s:10:\"user_agent\";s:146:\"Mozilla/5.0 (Linux; Android 5.1; BRAVIA 4K 2015 Build/LMY48E.S153) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36\";s:9:\"fall_back\";s:22:\"generic_android_ver5_1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:5:{s:10:\"brand_name\";s:4:\"Sony\";s:10:\"model_name\";s:14:\"Bravia 4K 2015\";s:23:\"can_assign_phone_number\";b:0;s:18:\"is_wireless_device\";b:0;s:15:\"pointing_method\";s:8:\"trackpad\";}s:7:\"smarttv\";a:1:{s:10:\"is_smarttv\";b:1;}}');
/*!40000 ALTER TABLE `ztw__SmartTV` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
