-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__HTCMac`
--

DROP TABLE IF EXISTS `ztw__HTCMac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__HTCMac` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__HTCMac`
--

LOCK TABLES `ztw__HTCMac` WRITE;
/*!40000 ALTER TABLE `ztw__HTCMac` DISABLE KEYS */;
INSERT INTO `ztw__HTCMac` VALUES ('htc_desirehd_ver1_suba9191macspoof','HTC~DesireHD~A9191---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_DesireHD_A9191; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_desirehd_ver1',NULL,1,'a:3:{s:2:\"id\";s:34:\"htc_desirehd_ver1_suba9191macspoof\";s:10:\"user_agent\";s:162:\"HTC~DesireHD~A9191---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_DesireHD_A9191; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:17:\"htc_desirehd_ver1\";}'),('htc_desirehd_ver1_submacspoof','HTC~Desire~HD---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Desire_HD; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_desirehd_ver1',NULL,1,'a:3:{s:2:\"id\";s:29:\"htc_desirehd_ver1_submacspoof\";s:10:\"user_agent\";s:152:\"HTC~Desire~HD---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Desire_HD; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:17:\"htc_desirehd_ver1\";}'),('htc_desire_s_ver1_submacspoof','HTC~DesireS~S510e---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_DesireS_S510e; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_desire_s_ver1',NULL,1,'a:3:{s:2:\"id\";s:29:\"htc_desire_s_ver1_submacspoof\";s:10:\"user_agent\";s:160:\"HTC~DesireS~S510e---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_DesireS_S510e; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:17:\"htc_desire_s_ver1\";}'),('htc_evo_3d_ver1_submacspoof','HTC~EVO3D~X515m---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_EVO3D_X515m; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_evo_3d_ver1',NULL,1,'a:3:{s:2:\"id\";s:27:\"htc_evo_3d_ver1_submacspoof\";s:10:\"user_agent\";s:156:\"HTC~EVO3D~X515m---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_EVO3D_X515m; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:15:\"htc_evo_3d_ver1\";}'),('htc_flyer_ver1_sub510emacspoof','HTC~Flyer~P510e---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Flyer_P510e; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_flyer_ver1',NULL,1,'a:3:{s:2:\"id\";s:30:\"htc_flyer_ver1_sub510emacspoof\";s:10:\"user_agent\";s:156:\"HTC~Flyer~P510e---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Flyer_P510e; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:14:\"htc_flyer_ver1\";}'),('htc_flyer_ver1_subp512_submacspoof','HTC~Flyer~P512---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Flyer_P512; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_flyer_ver1_subp512',NULL,1,'a:3:{s:2:\"id\";s:34:\"htc_flyer_ver1_subp512_submacspoof\";s:10:\"user_agent\";s:154:\"HTC~Flyer~P512---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Flyer_P512; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:22:\"htc_flyer_ver1_subp512\";}'),('htc_incredibles_s710e_ver1_submacspoof','HTC~IncredibleS~S710e---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_IncredibleS_S710e; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_incredibles_s710e_ver1',NULL,1,'a:3:{s:2:\"id\";s:38:\"htc_incredibles_s710e_ver1_submacspoof\";s:10:\"user_agent\";s:168:\"HTC~IncredibleS~S710e---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_IncredibleS_S710e; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:26:\"htc_incredibles_s710e_ver1\";}'),('htc_sensation_ver1_submacspoof','HTC~Sensation~1.35.163.1---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC/Sensation/1.35.163.1; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_sensation_ver1',NULL,1,'a:3:{s:2:\"id\";s:30:\"htc_sensation_ver1_submacspoof\";s:10:\"user_agent\";s:174:\"HTC~Sensation~1.35.163.1---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC/Sensation/1.35.163.1; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:18:\"htc_sensation_ver1\";}'),('htc_sensation_ver1_subz710e_submacspoof','HTC~Sensation~Z710e---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Sensation_Z710e; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_sensation_ver1_subz710e',NULL,1,'a:3:{s:2:\"id\";s:39:\"htc_sensation_ver1_subz710e_submacspoof\";s:10:\"user_agent\";s:164:\"HTC~Sensation~Z710e---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Sensation_Z710e; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:27:\"htc_sensation_ver1_subz710e\";}'),('htc_sensation_ver1_subuaz710a_submacspoof','HTC~Sensation~Z710a---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Sensation_Z710a; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','htc_sensation_ver1_subuaz710a',NULL,1,'a:3:{s:2:\"id\";s:41:\"htc_sensation_ver1_subuaz710a_submacspoof\";s:10:\"user_agent\";s:164:\"HTC~Sensation~Z710a---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; HTC_Sensation_Z710a; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:29:\"htc_sensation_ver1_subuaz710a\";}');
/*!40000 ALTER TABLE `ztw__HTCMac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:35
