-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Maemo`
--

DROP TABLE IF EXISTS `ztw__Maemo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Maemo` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Maemo`
--

LOCK TABLES `ztw__Maemo` WRITE;
/*!40000 ALTER TABLE `ztw__Maemo` DISABLE KEYS */;
INSERT INTO `ztw__Maemo` VALUES ('nokia_n800_ver1','Maemo RX-34_2007SE_2.2006.51-6---Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux armv6l; U) Opera 8.5 [en_US] Maemo browser 0.7.6 RX-34_2007SE_2.2006.51-6','nokia_generic_maemo',1,1,'a:12:{s:2:\"id\";s:15:\"nokia_n800_ver1\";s:10:\"user_agent\";s:152:\"Maemo RX-34_2007SE_2.2006.51-6---Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux armv6l; U) Opera 8.5 [en_US] Maemo browser 0.7.6 RX-34_2007SE_2.2006.51-6\";s:9:\"fall_back\";s:19:\"nokia_generic_maemo\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:11:{s:14:\"mobile_browser\";s:5:\"Opera\";s:22:\"mobile_browser_version\";d:8.5;s:25:\"can_skip_aligned_link_row\";b:1;s:10:\"model_name\";s:4:\"N800\";s:25:\"device_claims_web_support\";b:1;s:10:\"brand_name\";s:5:\"Nokia\";s:15:\"pointing_method\";s:11:\"touchscreen\";s:12:\"release_date\";s:12:\"2007_january\";s:9:\"device_os\";s:19:\"Linux Smartphone OS\";s:23:\"can_assign_phone_number\";b:0;s:9:\"is_tablet\";b:1;}s:7:\"display\";a:10:{s:7:\"columns\";d:30;s:4:\"rows\";d:20;s:15:\"max_image_width\";d:320;s:17:\"resolution_height\";d:480;s:16:\"resolution_width\";d:800;s:16:\"max_image_height\";d:533;s:22:\"physical_screen_height\";d:54;s:21:\"physical_screen_width\";d:90;s:16:\"dual_orientation\";b:1;s:13:\"density_class\";d:1.5;}s:6:\"markup\";a:2:{s:19:\"xhtml_support_level\";d:3;s:16:\"preferred_markup\";s:21:\"html_wi_w3_xhtmlbasic\";}s:6:\"bearer\";a:2:{s:4:\"wifi\";b:1;s:13:\"max_data_rate\";d:1800;}s:4:\"ajax\";a:1:{s:23:\"ajax_support_javascript\";b:1;}s:3:\"pdf\";a:1:{s:11:\"pdf_support\";b:1;}s:9:\"streaming\";a:1:{s:13:\"streaming_wmv\";d:7;}s:8:\"xhtml_ui\";a:2:{s:21:\"xhtml_supports_iframe\";s:4:\"full\";s:21:\"xhtml_can_embed_video\";s:5:\"plain\";}}'),('nokia_n900_ver1','Maemo RX-51 N900---Mozilla/5.0 (X11; U; Linux armv7l; xx-xx; rv:1.9.2a1pre) Gecko/20090928 Firefox/3.5 Maemo Browser 1.4.1.15 RX-51 N900','nokia_generic_maemo',1,1,'a:13:{s:2:\"id\";s:15:\"nokia_n900_ver1\";s:10:\"user_agent\";s:136:\"Maemo RX-51 N900---Mozilla/5.0 (X11; U; Linux armv7l; xx-xx; rv:1.9.2a1pre) Gecko/20090928 Firefox/3.5 Maemo Browser 1.4.1.15 RX-51 N900\";s:9:\"fall_back\";s:19:\"nokia_generic_maemo\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:7:{s:14:\"mobile_browser\";s:7:\"Firefox\";s:9:\"device_os\";s:19:\"Linux Smartphone OS\";s:15:\"pointing_method\";s:11:\"touchscreen\";s:10:\"model_name\";s:4:\"N900\";s:10:\"brand_name\";s:5:\"Nokia\";s:12:\"release_date\";s:11:\"2009_august\";s:25:\"device_claims_web_support\";b:1;}s:7:\"display\";a:5:{s:17:\"resolution_height\";d:480;s:16:\"resolution_width\";d:800;s:15:\"max_image_width\";d:320;s:16:\"max_image_height\";d:533;s:13:\"density_class\";d:1.5;}s:6:\"bearer\";a:2:{s:4:\"wifi\";b:1;s:13:\"max_data_rate\";d:1200;}s:3:\"pdf\";a:1:{s:11:\"pdf_support\";b:1;}s:6:\"markup\";a:1:{s:19:\"xhtml_support_level\";d:4;}s:8:\"xhtml_ui\";a:2:{s:21:\"xhtml_supports_iframe\";s:4:\"full\";s:25:\"accept_third_party_cookie\";b:0;}s:4:\"ajax\";a:1:{s:23:\"ajax_support_javascript\";b:1;}s:10:\"flash_lite\";a:1:{s:18:\"full_flash_support\";b:1;}s:9:\"streaming\";a:2:{s:13:\"streaming_wmv\";d:9;s:13:\"streaming_mp4\";b:1;}}'),('nokia_n900_ver1_sub1923','Maemo RX-51 N900---Mozilla/5.0 (X11; U; Linux armv7l; xx-xx; rv:1.9.2.3pre) Gecko/20100723 Firefox/3.5 Maemo Browser 1.7.4.8 RX-51 N900','nokia_n900_ver1',NULL,1,'a:3:{s:2:\"id\";s:23:\"nokia_n900_ver1_sub1923\";s:10:\"user_agent\";s:135:\"Maemo RX-51 N900---Mozilla/5.0 (X11; U; Linux armv7l; xx-xx; rv:1.9.2.3pre) Gecko/20100723 Firefox/3.5 Maemo Browser 1.7.4.8 RX-51 N900\";s:9:\"fall_back\";s:15:\"nokia_n900_ver1\";}'),('nokia_n900_ver1_sub192b6','Maemo RX-51 N900---Mozilla/5.0 (X11; U; Linux armv7l; xx-xx; rv:1.9.2b6pre) Gecko/20100318 Firefox/3.5 Maemo Browser 1.7.4.8 RX-51 N900','nokia_n900_ver1',NULL,1,'a:3:{s:2:\"id\";s:24:\"nokia_n900_ver1_sub192b6\";s:10:\"user_agent\";s:135:\"Maemo RX-51 N900---Mozilla/5.0 (X11; U; Linux armv7l; xx-xx; rv:1.9.2b6pre) Gecko/20100318 Firefox/3.5 Maemo Browser 1.7.4.8 RX-51 N900\";s:9:\"fall_back\";s:15:\"nokia_n900_ver1\";}');
/*!40000 ALTER TABLE `ztw__Maemo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
