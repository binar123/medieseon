-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__JavaMidlet`
--

DROP TABLE IF EXISTS `ztw__JavaMidlet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__JavaMidlet` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__JavaMidlet`
--

LOCK TABLES `ztw__JavaMidlet` WRITE;
/*!40000 ALTER TABLE `ztw__JavaMidlet` DISABLE KEYS */;
INSERT INTO `ztw__JavaMidlet` VALUES ('generic_untrusted_midlet','UNTRUSTED/1.0','generic_mobile',NULL,1,'a:4:{s:2:\"id\";s:24:\"generic_untrusted_midlet\";s:10:\"user_agent\";s:13:\"UNTRUSTED/1.0\";s:9:\"fall_back\";s:14:\"generic_mobile\";s:12:\"product_info\";a:4:{s:10:\"model_name\";s:11:\"J2ME Midlet\";s:10:\"brand_name\";s:7:\"Generic\";s:6:\"unique\";b:0;s:12:\"release_date\";s:12:\"2008_january\";}}'),('huawei_u120_ver1_subua','HUAWEI/1.0/HUAWEI U120/B000 Java/QVM/4.1 Profile/MIDP-2.0 Configuration/CLDC-1.1 UNTRUSTED/1.0','huawei_u120_ver1',NULL,1,'a:3:{s:2:\"id\";s:22:\"huawei_u120_ver1_subua\";s:10:\"user_agent\";s:94:\"HUAWEI/1.0/HUAWEI U120/B000 Java/QVM/4.1 Profile/MIDP-2.0 Configuration/CLDC-1.1 UNTRUSTED/1.0\";s:9:\"fall_back\";s:16:\"huawei_u120_ver1\";}'),('huawei_u120_ver1_subb000','U120, HUAWEI/1.0/HUAWEI U120/B000 Java/QVM/4.1 Profile/MIDP-2.0 Configuration/CLDC-1.1 UNTRUSTED/1.0','huawei_u120_ver1',NULL,1,'a:3:{s:2:\"id\";s:24:\"huawei_u120_ver1_subb000\";s:10:\"user_agent\";s:100:\"U120, HUAWEI/1.0/HUAWEI U120/B000 Java/QVM/4.1 Profile/MIDP-2.0 Configuration/CLDC-1.1 UNTRUSTED/1.0\";s:9:\"fall_back\";s:16:\"huawei_u120_ver1\";}'),('lg_lx_290s_ver1','POLARIS/6.01 (BREW 3.1.5; U; xx-xx; LG; POLARIS/6.01/WAP) Sprint LX290S MMP/2.0 Profile/MIDP-2.1 Configuration/CLDC-1.1 UNTRUSTED/1.0','generic_polaris_6',1,1,'a:7:{s:2:\"id\";s:15:\"lg_lx_290s_ver1\";s:10:\"user_agent\";s:133:\"POLARIS/6.01 (BREW 3.1.5; U; xx-xx; LG; POLARIS/6.01/WAP) Sprint LX290S MMP/2.0 Profile/MIDP-2.1 Configuration/CLDC-1.1 UNTRUSTED/1.0\";s:9:\"fall_back\";s:17:\"generic_polaris_6\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:4:{s:12:\"release_date\";s:13:\"2009_november\";s:10:\"brand_name\";s:2:\"LG\";s:10:\"model_name\";s:6:\"LX290S\";s:6:\"uaprof\";s:44:\"http://device.sprintpcs.com/LG/LX290S/latest\";}s:7:\"display\";a:6:{s:7:\"columns\";d:16;s:16:\"max_image_height\";d:280;s:15:\"max_image_width\";d:228;s:17:\"resolution_height\";d:320;s:16:\"resolution_width\";d:240;s:4:\"rows\";d:11;}s:9:\"streaming\";a:1:{s:13:\"streaming_mp4\";b:0;}}'),('huawei_u3205_ver1','Huawei/1.0/0Huawei U3205/B100 Browser/Obigo-Browser/Q05A MMS/Obigo-MMS/Q05A SyncML/HW-SyncML/1.0 Java/HWJa/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 Player/QTV-Player/5.3 UNTRUSTED/1.0','generic_teleca_obigo_q5',1,1,'a:6:{s:2:\"id\";s:17:\"huawei_u3205_ver1\";s:10:\"user_agent\";s:186:\"Huawei/1.0/0Huawei U3205/B100 Browser/Obigo-Browser/Q05A MMS/Obigo-MMS/Q05A SyncML/HW-SyncML/1.0 Java/HWJa/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 Player/QTV-Player/5.3 UNTRUSTED/1.0\";s:9:\"fall_back\";s:23:\"generic_teleca_obigo_q5\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:5:{s:12:\"release_date\";s:8:\"2012_may\";s:10:\"brand_name\";s:6:\"Huawei\";s:10:\"model_name\";s:5:\"U3205\";s:6:\"uaprof\";s:53:\"http://wap.huawei.com/uaprof/HuaweiU3205v100WCDMA.xml\";s:7:\"uaprof2\";s:52:\"http://wap.huawei.com/uaprof/HuaweiU3205v100GPRS.xml\";}s:7:\"display\";a:6:{s:7:\"columns\";d:11;s:16:\"max_image_height\";d:280;s:15:\"max_image_width\";d:228;s:17:\"resolution_height\";d:320;s:16:\"resolution_width\";d:240;s:4:\"rows\";d:13;}}');
/*!40000 ALTER TABLE `ztw__JavaMidlet` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
