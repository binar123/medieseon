-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Grundig`
--

DROP TABLE IF EXISTS `ztw__Grundig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Grundig` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Grundig`
--

LOCK TABLES `ztw__Grundig` WRITE;
/*!40000 ALTER TABLE `ztw__Grundig` DISABLE KEYS */;
INSERT INTO `ztw__Grundig` VALUES ('grundig_m131_ver1','GRUNDIG M131','generic_mobile',1,1,'a:8:{s:2:\"id\";s:17:\"grundig_m131_ver1\";s:10:\"user_agent\";s:12:\"GRUNDIG M131\";s:9:\"fall_back\";s:14:\"generic_mobile\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:7:\"Grundig\";s:10:\"model_name\";s:4:\"M131\";}s:7:\"display\";a:4:{s:16:\"resolution_width\";d:128;s:17:\"resolution_height\";d:128;s:15:\"max_image_width\";d:128;s:16:\"max_image_height\";d:128;}s:6:\"markup\";a:2:{s:16:\"preferred_markup\";s:7:\"wml_1_1\";s:19:\"xhtml_support_level\";d:-1;}s:8:\"xhtml_ui\";a:1:{s:14:\"cookie_support\";b:1;}}'),('grundig_m131_ver1_sub85108709','GRUNDIG M131/85108709 Browser/1.2.1 Profile/MIDP-1.0 Configuration/CLDC-1.0','grundig_m131_ver1',NULL,1,'a:3:{s:2:\"id\";s:29:\"grundig_m131_ver1_sub85108709\";s:10:\"user_agent\";s:75:\"GRUNDIG M131/85108709 Browser/1.2.1 Profile/MIDP-1.0 Configuration/CLDC-1.0\";s:9:\"fall_back\";s:17:\"grundig_m131_ver1\";}'),('grundig_gr660_ver1','GRUNDIG GR660','htc_wizard_prodigy_ver1',1,1,'a:6:{s:2:\"id\";s:18:\"grundig_gr660_ver1\";s:10:\"user_agent\";s:13:\"GRUNDIG GR660\";s:9:\"fall_back\";s:23:\"htc_wizard_prodigy_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:7:\"Grundig\";s:10:\"model_name\";s:5:\"GR660\";}s:7:\"display\";a:4:{s:16:\"resolution_width\";d:480;s:17:\"resolution_height\";d:640;s:15:\"max_image_width\";d:480;s:16:\"max_image_height\";d:640;}}'),('grundig_gr660_ver1_sub2225102','Grundig GR660/2.22.5.102 Mozilla/4.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)','grundig_gr660_ver1',NULL,1,'a:3:{s:2:\"id\";s:29:\"grundig_gr660_ver1_sub2225102\";s:10:\"user_agent\";s:126:\"Grundig GR660/2.22.5.102 Mozilla/4.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)\";s:9:\"fall_back\";s:18:\"grundig_gr660_ver1\";}'),('grundig_gr980_ver1','GRUNDIG GR980','htc_universal_ver1',1,1,'a:6:{s:2:\"id\";s:18:\"grundig_gr980_ver1\";s:10:\"user_agent\";s:13:\"GRUNDIG GR980\";s:9:\"fall_back\";s:18:\"htc_universal_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:7:\"Grundig\";s:10:\"model_name\";s:5:\"GR980\";}s:7:\"display\";a:4:{s:16:\"resolution_width\";d:480;s:17:\"resolution_height\";d:640;s:15:\"max_image_width\";d:480;s:16:\"max_image_height\";d:640;}}'),('grundig_gr980_sub130152','Grundig GR980/1.30.152 Mozilla/4.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 (compatible; MSIE 4.01; Windows CE; PPC; 640X480)','grundig_gr980_ver1',NULL,1,'a:3:{s:2:\"id\";s:23:\"grundig_gr980_sub130152\";s:10:\"user_agent\";s:124:\"Grundig GR980/1.30.152 Mozilla/4.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 (compatible; MSIE 4.01; Windows CE; PPC; 640X480)\";s:9:\"fall_back\";s:18:\"grundig_gr980_ver1\";}');
/*!40000 ALTER TABLE `ztw__Grundig` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:35
