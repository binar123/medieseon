-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Konqueror`
--

DROP TABLE IF EXISTS `ztw__Konqueror`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Konqueror` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Konqueror`
--

LOCK TABLES `ztw__Konqueror` WRITE;
/*!40000 ALTER TABLE `ztw__Konqueror` DISABLE KEYS */;
INSERT INTO `ztw__Konqueror` VALUES ('konqueror','Konqueror','generic_web_browser',NULL,1,'a:4:{s:2:\"id\";s:9:\"konqueror\";s:10:\"user_agent\";s:9:\"Konqueror\";s:9:\"fall_back\";s:19:\"generic_web_browser\";s:12:\"product_info\";a:1:{s:10:\"brand_name\";s:9:\"konqueror\";}}'),('konqueror_2','Konqueror/2','konqueror',NULL,1,'a:4:{s:2:\"id\";s:11:\"konqueror_2\";s:10:\"user_agent\";s:11:\"Konqueror/2\";s:9:\"fall_back\";s:9:\"konqueror\";s:12:\"product_info\";a:1:{s:10:\"model_name\";d:2;}}'),('konqueror_3','Konqueror/3','konqueror',NULL,1,'a:4:{s:2:\"id\";s:11:\"konqueror_3\";s:10:\"user_agent\";s:11:\"Konqueror/3\";s:9:\"fall_back\";s:9:\"konqueror\";s:12:\"product_info\";a:1:{s:10:\"model_name\";d:3;}}'),('konqueror_4','Konqueror/4','konqueror',NULL,1,'a:4:{s:2:\"id\";s:11:\"konqueror_4\";s:10:\"user_agent\";s:11:\"Konqueror/4\";s:9:\"fall_back\";s:9:\"konqueror\";s:12:\"product_info\";a:1:{s:10:\"model_name\";d:4;}}');
/*!40000 ALTER TABLE `ztw__Konqueror` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
