-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__MSIE`
--

DROP TABLE IF EXISTS `ztw__MSIE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__MSIE` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__MSIE`
--

LOCK TABLES `ztw__MSIE` WRITE;
/*!40000 ALTER TABLE `ztw__MSIE` DISABLE KEYS */;
INSERT INTO `ztw__MSIE` VALUES ('msie','Mozilla/4.0 (compatible; MSIE','generic_web_browser',NULL,1,'a:4:{s:2:\"id\";s:4:\"msie\";s:10:\"user_agent\";s:29:\"Mozilla/4.0 (compatible; MSIE\";s:9:\"fall_back\";s:19:\"generic_web_browser\";s:12:\"product_info\";a:6:{s:10:\"brand_name\";s:9:\"Microsoft\";s:14:\"mobile_browser\";s:4:\"MSIE\";s:10:\"model_name\";s:17:\"Internet Explorer\";s:22:\"mobile_browser_version\";s:3:\"1.0\";s:12:\"release_date\";s:11:\"1995_august\";s:9:\"device_os\";s:7:\"Desktop\";}}'),('msie_4','Mozilla/4.0 (compatible; MSIE 4.0;','msie',NULL,1,'a:4:{s:2:\"id\";s:6:\"msie_4\";s:10:\"user_agent\";s:34:\"Mozilla/4.0 (compatible; MSIE 4.0;\";s:9:\"fall_back\";s:4:\"msie\";s:12:\"product_info\";a:2:{s:22:\"mobile_browser_version\";s:3:\"4.0\";s:12:\"release_date\";s:14:\"1997_september\";}}'),('msie_5','Mozilla/4.0 (compatible; MSIE 5.0;','msie_4',NULL,1,'a:4:{s:2:\"id\";s:6:\"msie_5\";s:10:\"user_agent\";s:34:\"Mozilla/4.0 (compatible; MSIE 5.0;\";s:9:\"fall_back\";s:6:\"msie_4\";s:12:\"product_info\";a:2:{s:12:\"release_date\";s:10:\"1999_march\";s:22:\"mobile_browser_version\";s:3:\"5.0\";}}'),('msie_5_5','Mozilla/4.0 (compatible; MSIE 5.5;','msie_5',NULL,1,'a:4:{s:2:\"id\";s:8:\"msie_5_5\";s:10:\"user_agent\";s:34:\"Mozilla/4.0 (compatible; MSIE 5.5;\";s:9:\"fall_back\";s:6:\"msie_5\";s:12:\"product_info\";a:2:{s:22:\"mobile_browser_version\";d:5.5;s:12:\"release_date\";s:9:\"2000_july\";}}'),('msie_6','Mozilla/4.0 (compatible; MSIE 6.0;','msie_5_5',NULL,1,'a:4:{s:2:\"id\";s:6:\"msie_6\";s:10:\"user_agent\";s:34:\"Mozilla/4.0 (compatible; MSIE 6.0;\";s:9:\"fall_back\";s:8:\"msie_5_5\";s:12:\"product_info\";a:2:{s:22:\"mobile_browser_version\";s:3:\"6.0\";s:12:\"release_date\";s:11:\"2001_august\";}}'),('msie_7','Mozilla/4.0 (compatible; MSIE 7.0;','msie_6',NULL,1,'a:4:{s:2:\"id\";s:6:\"msie_7\";s:10:\"user_agent\";s:34:\"Mozilla/4.0 (compatible; MSIE 7.0;\";s:9:\"fall_back\";s:6:\"msie_6\";s:12:\"product_info\";a:2:{s:22:\"mobile_browser_version\";s:3:\"7.0\";s:12:\"release_date\";s:12:\"2006_october\";}}'),('msie_8','Mozilla/4.0 (compatible; MSIE 8.0;','msie_7',NULL,1,'a:4:{s:2:\"id\";s:6:\"msie_8\";s:10:\"user_agent\";s:34:\"Mozilla/4.0 (compatible; MSIE 8.0;\";s:9:\"fall_back\";s:6:\"msie_7\";s:12:\"product_info\";a:2:{s:22:\"mobile_browser_version\";s:3:\"8.0\";s:12:\"release_date\";s:12:\"2007_january\";}}'),('msie_9','Mozilla/5.0 (compatible; MSIE 9.0;','msie_8',NULL,1,'a:5:{s:2:\"id\";s:6:\"msie_9\";s:10:\"user_agent\";s:34:\"Mozilla/5.0 (compatible; MSIE 9.0;\";s:9:\"fall_back\";s:6:\"msie_8\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";s:3:\"9.0\";}s:7:\"html_ui\";a:1:{s:14:\"canvas_support\";s:4:\"full\";}}'),('msie_10','Mozilla/5.0 (compatible; MSIE 10.0;','msie_9',NULL,1,'a:4:{s:2:\"id\";s:7:\"msie_10\";s:10:\"user_agent\";s:35:\"Mozilla/5.0 (compatible; MSIE 10.0;\";s:9:\"fall_back\";s:6:\"msie_9\";s:12:\"product_info\";a:2:{s:12:\"release_date\";s:8:\"2012_may\";s:22:\"mobile_browser_version\";d:10;}}'),('msie_7_subuanocompat','Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; xx-xx)','msie_7',NULL,1,'a:3:{s:2:\"id\";s:20:\"msie_7_subuanocompat\";s:10:\"user_agent\";s:57:\"Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; xx-xx)\";s:9:\"fall_back\";s:6:\"msie_7\";}');
/*!40000 ALTER TABLE `ztw__MSIE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
