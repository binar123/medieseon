-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Xbox`
--

DROP TABLE IF EXISTS `ztw__Xbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Xbox` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Xbox`
--

LOCK TABLES `ztw__Xbox` WRITE;
/*!40000 ALTER TABLE `ztw__Xbox` DISABLE KEYS */;
INSERT INTO `ztw__Xbox` VALUES ('microsoft_xbox360_ver1','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; Xbox)','generic_tv_console_browser',1,1,'a:5:{s:2:\"id\";s:22:\"microsoft_xbox360_ver1\";s:10:\"user_agent\";s:69:\"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; Xbox)\";s:9:\"fall_back\";s:26:\"generic_tv_console_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:5:{s:10:\"brand_name\";s:9:\"Microsoft\";s:12:\"release_date\";s:12:\"2012_october\";s:10:\"model_name\";s:8:\"Xbox 360\";s:14:\"mobile_browser\";s:4:\"MSIE\";s:22:\"mobile_browser_version\";s:3:\"9.0\";}}'),('microsoft_xbox360_ver1_subie10','Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0; Xbox)','microsoft_xbox360_ver1',NULL,1,'a:4:{s:2:\"id\";s:30:\"microsoft_xbox360_ver1_subie10\";s:10:\"user_agent\";s:70:\"Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0; Xbox)\";s:9:\"fall_back\";s:22:\"microsoft_xbox360_ver1\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";s:4:\"10.0\";}}'),('microsoft_xboxone_ver1','Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0; Xbox; Xbox One)','generic_tv_console_browser',1,1,'a:5:{s:2:\"id\";s:22:\"microsoft_xboxone_ver1\";s:10:\"user_agent\";s:80:\"Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0; Xbox; Xbox One)\";s:9:\"fall_back\";s:26:\"generic_tv_console_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:5:{s:10:\"model_name\";s:8:\"Xbox One\";s:10:\"brand_name\";s:9:\"Microsoft\";s:12:\"release_date\";s:13:\"2013_november\";s:14:\"mobile_browser\";s:4:\"MSIE\";s:22:\"mobile_browser_version\";d:10;}}'),('microsoft_xboxone_ver1_subua10','Mozilla/5.0 (Windows NT 10.0; Win64; x64; Xbox; Xbox One) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10585','microsoft_xboxone_ver1',NULL,1,'a:4:{s:2:\"id\";s:30:\"microsoft_xboxone_ver1_subua10\";s:10:\"user_agent\";s:143:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; Xbox; Xbox One) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10585\";s:9:\"fall_back\";s:22:\"microsoft_xboxone_ver1\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";d:13;}}');
/*!40000 ALTER TABLE `ztw__Xbox` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:35
