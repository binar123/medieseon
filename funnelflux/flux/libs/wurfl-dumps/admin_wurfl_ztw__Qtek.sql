-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Qtek`
--

DROP TABLE IF EXISTS `ztw__Qtek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Qtek` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Qtek`
--

LOCK TABLES `ztw__Qtek` WRITE;
/*!40000 ALTER TABLE `ztw__Qtek` DISABLE KEYS */;
INSERT INTO `ztw__Qtek` VALUES ('qtek_s100_ver1','Qtek S100','htc_magician_ver1',1,1,'a:7:{s:2:\"id\";s:14:\"qtek_s100_ver1\";s:10:\"user_agent\";s:9:\"Qtek S100\";s:9:\"fall_back\";s:17:\"htc_magician_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:4:\"Qtek\";s:10:\"model_name\";s:4:\"S100\";}s:6:\"markup\";a:1:{s:16:\"preferred_markup\";s:23:\"html_wi_oma_xhtmlmp_1_0\";}s:7:\"display\";a:6:{s:16:\"resolution_width\";d:240;s:17:\"resolution_height\";d:320;s:15:\"max_image_width\";d:240;s:16:\"max_image_height\";d:320;s:21:\"physical_screen_width\";d:43;s:22:\"physical_screen_height\";d:57;}}'),('qtek_s200_ver1_subua','Qtek S200','qtek_s200_ver1',NULL,1,'a:8:{s:2:\"id\";s:20:\"qtek_s200_ver1_subua\";s:10:\"user_agent\";s:9:\"Qtek S200\";s:9:\"fall_back\";s:14:\"qtek_s200_ver1\";s:12:\"product_info\";a:3:{s:10:\"brand_name\";s:4:\"Qtek\";s:10:\"model_name\";s:4:\"S200\";s:14:\"mobile_browser\";s:25:\"Microsoft Mobile Explorer\";}s:8:\"xhtml_ui\";a:1:{s:25:\"accept_third_party_cookie\";b:0;}s:4:\"ajax\";a:1:{s:23:\"ajax_support_javascript\";b:1;}s:6:\"markup\";a:1:{s:19:\"xhtml_support_level\";d:3;}s:7:\"display\";a:4:{s:7:\"columns\";d:15;s:4:\"rows\";d:7;s:17:\"resolution_height\";d:320;s:16:\"resolution_width\";d:240;}}'),('qtek_s200_ver1_sub297122','Qtek S200/2.9.7.122 Mozilla/4.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 (Compatible; MSIE 4.01;Windows CE; PPC; 240X320) ','qtek_s200_ver1',NULL,1,'a:3:{s:2:\"id\";s:24:\"qtek_s200_ver1_sub297122\";s:10:\"user_agent\";s:121:\"Qtek S200/2.9.7.122 Mozilla/4.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 (Compatible; MSIE 4.01;Windows CE; PPC; 240X320) \";s:9:\"fall_back\";s:14:\"qtek_s200_ver1\";}'),('qtek_8010_ver1','Qtek8010','htc_typhoon_ver1',1,1,'a:6:{s:2:\"id\";s:14:\"qtek_8010_ver1\";s:10:\"user_agent\";s:8:\"Qtek8010\";s:9:\"fall_back\";s:16:\"htc_typhoon_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:4:\"Qtek\";s:10:\"model_name\";d:8010;}s:7:\"display\";a:2:{s:21:\"physical_screen_width\";d:35;s:22:\"physical_screen_height\";d:44;}}'),('qtek_8010_ver1_sub401','Qtek8010 (Mozilla/4.0 compatible; MSIE 4.01; Windows CE; Smartphone; 176x220)','qtek_8010_ver1',NULL,1,'a:4:{s:2:\"id\";s:21:\"qtek_8010_ver1_sub401\";s:10:\"user_agent\";s:77:\"Qtek8010 (Mozilla/4.0 compatible; MSIE 4.01; Windows CE; Smartphone; 176x220)\";s:9:\"fall_back\";s:14:\"qtek_8010_ver1\";s:6:\"bearer\";a:1:{s:13:\"max_data_rate\";d:40;}}'),('qtek_8080_ver1','Qtek8080','htc_voyager_ver1',1,1,'a:6:{s:2:\"id\";s:14:\"qtek_8080_ver1\";s:10:\"user_agent\";s:8:\"Qtek8080\";s:9:\"fall_back\";s:16:\"htc_voyager_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:4:\"Qtek\";s:10:\"model_name\";d:8080;}s:7:\"display\";a:2:{s:21:\"physical_screen_width\";d:35;s:22:\"physical_screen_height\";d:44;}}'),('qtek_8080_ver1_sub176220','Qtek8080/DBK (;; 4.20.13291.0; Windows Mobile 2003; Smartphone; 176x220)','qtek_8080_ver1',NULL,1,'a:4:{s:2:\"id\";s:24:\"qtek_8080_ver1_sub176220\";s:10:\"user_agent\";s:72:\"Qtek8080/DBK (;; 4.20.13291.0; Windows Mobile 2003; Smartphone; 176x220)\";s:9:\"fall_back\";s:14:\"qtek_8080_ver1\";s:6:\"bearer\";a:1:{s:13:\"max_data_rate\";d:40;}}'),('qtek_9000_ver1','Qtek9000','htc_universal_ver1',1,1,'a:6:{s:2:\"id\";s:14:\"qtek_9000_ver1\";s:10:\"user_agent\";s:8:\"Qtek9000\";s:9:\"fall_back\";s:18:\"htc_universal_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:4:\"Qtek\";s:10:\"model_name\";d:9000;}s:7:\"display\";a:6:{s:16:\"resolution_width\";d:640;s:17:\"resolution_height\";d:480;s:15:\"max_image_width\";d:120;s:16:\"max_image_height\";d:480;s:21:\"physical_screen_width\";d:73;s:22:\"physical_screen_height\";d:55;}}'),('qtek_9090_ver1_subua','Qtek9090','qtek_9090_ver1',NULL,1,'a:5:{s:2:\"id\";s:20:\"qtek_9090_ver1_subua\";s:10:\"user_agent\";s:8:\"Qtek9090\";s:9:\"fall_back\";s:14:\"qtek_9090_ver1\";s:12:\"product_info\";a:4:{s:10:\"brand_name\";s:4:\"Qtek\";s:10:\"model_name\";d:9090;s:19:\"has_qwerty_keyboard\";b:1;s:15:\"pointing_method\";s:6:\"stylus\";}s:7:\"display\";a:3:{s:17:\"resolution_height\";d:320;s:16:\"resolution_width\";d:240;s:16:\"max_image_height\";d:280;}}'),('qtek_9090_ver1','Qtek9090; Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)','htc_blueangel_ver1',1,1,'a:6:{s:2:\"id\";s:14:\"qtek_9090_ver1\";s:10:\"user_agent\";s:71:\"Qtek9090; Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)\";s:9:\"fall_back\";s:18:\"htc_blueangel_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"model_name\";d:9090;s:10:\"brand_name\";s:4:\"Qtek\";}s:7:\"display\";a:2:{s:21:\"physical_screen_width\";d:53;s:22:\"physical_screen_height\";d:71;}}'),('qtek_9100_ver1_subua','Qtek9100','qtek_9100_ver1',NULL,1,'a:5:{s:2:\"id\";s:20:\"qtek_9100_ver1_subua\";s:10:\"user_agent\";s:8:\"Qtek9100\";s:9:\"fall_back\";s:14:\"qtek_9100_ver1\";s:12:\"product_info\";a:2:{s:10:\"brand_name\";s:4:\"Qtek\";s:10:\"model_name\";d:9100;}s:6:\"bearer\";a:1:{s:4:\"wifi\";b:1;}}'),('qtek_8020_ver1_feeler','Qtek8020 (Mozilla\\\\4.0 compatible; MSIE 4.01; Windows CE; Smartphone; 176x220)Mozilla/4.0 (compatible; MSIE 6.0; Windows CE; IEMobile 6.8) PPC; 240x320;SPV M3100; OpVer 14.211.1.613','htc_hermes_ver1',NULL,1,'a:5:{s:2:\"id\";s:21:\"qtek_8020_ver1_feeler\";s:10:\"user_agent\";s:181:\"Qtek8020 (Mozilla\\\\4.0 compatible; MSIE 4.01; Windows CE; Smartphone; 176x220)Mozilla/4.0 (compatible; MSIE 6.0; Windows CE; IEMobile 6.8) PPC; 240x320;SPV M3100; OpVer 14.211.1.613\";s:9:\"fall_back\";s:15:\"htc_hermes_ver1\";s:12:\"product_info\";a:3:{s:10:\"brand_name\";s:3:\"HTC\";s:10:\"model_name\";s:6:\"Hermes\";s:6:\"uaprof\";s:43:\"http://www.htcmms.com.tw/gen/hermes-2.0.xml\";}s:7:\"display\";a:4:{s:7:\"columns\";d:16;s:17:\"resolution_height\";d:320;s:16:\"resolution_width\";d:240;s:4:\"rows\";d:36;}}'),('qtek_9100_ver1_subua1','Qtek_9100/Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)','qtek_9100_ver1',NULL,1,'a:3:{s:2:\"id\";s:21:\"qtek_9100_ver1_subua1\";s:10:\"user_agent\";s:71:\"Qtek_9100/Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)\";s:9:\"fall_back\";s:14:\"qtek_9100_ver1\";}'),('qtek_8020_ver1','Qtek8020 (Mozilla\\4.0 compatible; MSIE 4.01; Windows CE; Smartphone; 176x220)','generic_ms_winmo2003_se',1,1,'a:7:{s:2:\"id\";s:14:\"qtek_8020_ver1\";s:10:\"user_agent\";s:77:\"Qtek8020 (Mozilla\\4.0 compatible; MSIE 4.01; Windows CE; Smartphone; 176x220)\";s:9:\"fall_back\";s:23:\"generic_ms_winmo2003_se\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:4:{s:12:\"release_date\";s:13:\"2008_december\";s:10:\"brand_name\";s:4:\"Qtek\";s:10:\"model_name\";d:8020;s:6:\"uaprof\";s:41:\"http://www.htcmms.com.tw/gen/st20-1.0.xml\";}s:7:\"display\";a:8:{s:7:\"columns\";d:10;s:16:\"max_image_height\";d:180;s:15:\"max_image_width\";d:168;s:17:\"resolution_height\";d:220;s:16:\"resolution_width\";d:176;s:4:\"rows\";d:25;s:21:\"physical_screen_width\";d:35;s:22:\"physical_screen_height\";d:44;}s:6:\"bearer\";a:1:{s:13:\"max_data_rate\";d:40;}}');
/*!40000 ALTER TABLE `ztw__Qtek` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
