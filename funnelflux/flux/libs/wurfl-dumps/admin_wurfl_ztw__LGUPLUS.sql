-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__LGUPLUS`
--

DROP TABLE IF EXISTS `ztw__LGUPLUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__LGUPLUS` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__LGUPLUS`
--

LOCK TABLES `ztw__LGUPLUS` WRITE;
/*!40000 ALTER TABLE `ztw__LGUPLUS` DISABLE KEYS */;
INSERT INTO `ztw__LGUPLUS` VALUES ('generic_lguplus','DO_NOT_MATCH_GENERIC_LGUPLUS','generic_xhtml',NULL,0,'a:9:{s:2:\"id\";s:15:\"generic_lguplus\";s:10:\"user_agent\";s:28:\"DO_NOT_MATCH_GENERIC_LGUPLUS\";s:9:\"fall_back\";s:13:\"generic_xhtml\";s:12:\"product_info\";a:3:{s:10:\"brand_name\";s:7:\"LGUPlus\";s:12:\"release_date\";s:9:\"2010_july\";s:15:\"pointing_method\";s:11:\"touchscreen\";}s:10:\"flash_lite\";a:1:{s:18:\"full_flash_support\";b:1;}s:7:\"display\";a:9:{s:7:\"columns\";d:16;s:22:\"physical_screen_height\";d:40;s:21:\"physical_screen_width\";d:40;s:4:\"rows\";d:10;s:15:\"max_image_width\";d:228;s:17:\"resolution_height\";d:296;s:16:\"resolution_width\";d:240;s:16:\"max_image_height\";d:280;s:16:\"dual_orientation\";b:1;}s:6:\"markup\";a:2:{s:16:\"preferred_markup\";s:12:\"html_web_4_0\";s:19:\"xhtml_support_level\";d:3;}s:8:\"xhtml_ui\";a:1:{s:21:\"xhtml_supports_iframe\";s:4:\"full\";}s:9:\"streaming\";a:3:{s:13:\"streaming_mov\";b:1;s:13:\"streaming_flv\";b:1;s:13:\"streaming_mp4\";b:1;}}'),('generic_lguplus_rexos','DO_NOT_MATCH_GENERIC_LGUPLUS_REXOS','generic_lguplus',NULL,0,'a:4:{s:2:\"id\";s:21:\"generic_lguplus_rexos\";s:10:\"user_agent\";s:34:\"DO_NOT_MATCH_GENERIC_LGUPLUS_REXOS\";s:9:\"fall_back\";s:15:\"generic_lguplus\";s:12:\"product_info\";a:1:{s:9:\"device_os\";s:15:\"Rex Qualcomm OS\";}}'),('generic_lguplus_winmo6_5','DO_NOT_MATCH_GENERIC_LGUPLUS_WINMO6_5','generic_lguplus',NULL,0,'a:5:{s:2:\"id\";s:24:\"generic_lguplus_winmo6_5\";s:10:\"user_agent\";s:37:\"DO_NOT_MATCH_GENERIC_LGUPLUS_WINMO6_5\";s:9:\"fall_back\";s:15:\"generic_lguplus\";s:12:\"product_info\";a:3:{s:10:\"brand_name\";s:7:\"LGUPlus\";s:9:\"device_os\";s:17:\"Windows Mobile OS\";s:17:\"device_os_version\";d:6.5;}s:7:\"display\";a:6:{s:16:\"resolution_width\";d:480;s:17:\"resolution_height\";d:800;s:15:\"max_image_width\";d:480;s:16:\"max_image_height\";d:800;s:7:\"columns\";d:40;s:4:\"rows\";d:40;}}'),('generic_lguplus_android2','DO_NOT_MATCH_GENERIC_LGUPLUS_ANDROID2','generic_lguplus',NULL,0,'a:5:{s:2:\"id\";s:24:\"generic_lguplus_android2\";s:10:\"user_agent\";s:37:\"DO_NOT_MATCH_GENERIC_LGUPLUS_ANDROID2\";s:9:\"fall_back\";s:15:\"generic_lguplus\";s:12:\"product_info\";a:2:{s:9:\"device_os\";s:7:\"Android\";s:17:\"device_os_version\";d:2.2000000000000002;}s:7:\"display\";a:4:{s:16:\"resolution_width\";d:480;s:17:\"resolution_height\";d:800;s:15:\"max_image_width\";d:480;s:16:\"max_image_height\";d:480;}}'),('generic_lguplus_rexos_facebook_browser','DO_NOT_MATCH_GENERIC_LGUPLUS_REXOS_FACEBOOK','generic_lguplus_rexos',NULL,0,'a:5:{s:2:\"id\";s:38:\"generic_lguplus_rexos_facebook_browser\";s:10:\"user_agent\";s:43:\"DO_NOT_MATCH_GENERIC_LGUPLUS_REXOS_FACEBOOK\";s:9:\"fall_back\";s:21:\"generic_lguplus_rexos\";s:12:\"product_info\";a:2:{s:10:\"model_name\";s:20:\"LGUPlus/Rex/FaceBook\";s:14:\"mobile_browser\";s:16:\"LGUPlus FaceBook\";}s:7:\"display\";a:8:{s:7:\"columns\";d:80;s:22:\"physical_screen_height\";d:100;s:21:\"physical_screen_width\";d:50;s:4:\"rows\";d:40;s:15:\"max_image_width\";d:480;s:17:\"resolution_height\";d:800;s:16:\"resolution_width\";d:480;s:16:\"max_image_height\";d:800;}}'),('generic_lguplus_rexos_webviewer_browser','DO_NOT_MATCH_GENERIC_LGUPLUS_REXOS_WEBVIEWER','generic_lguplus_rexos',NULL,0,'a:5:{s:2:\"id\";s:39:\"generic_lguplus_rexos_webviewer_browser\";s:10:\"user_agent\";s:44:\"DO_NOT_MATCH_GENERIC_LGUPLUS_REXOS_WEBVIEWER\";s:9:\"fall_back\";s:21:\"generic_lguplus_rexos\";s:12:\"product_info\";a:2:{s:10:\"model_name\";s:21:\"LGUPlus/Rex/WebViewer\";s:14:\"mobile_browser\";s:17:\"LGUPlus WebViewer\";}s:8:\"xhtml_ui\";a:1:{s:21:\"xhtml_supports_iframe\";s:7:\"partial\";}}'),('generic_lguplus_winmo_facebook_browser','DO_NOT_MATCH_GENERIC_LGUPLUS_WINMO_FACEBOOK','generic_lguplus_winmo6_5',NULL,0,'a:4:{s:2:\"id\";s:38:\"generic_lguplus_winmo_facebook_browser\";s:10:\"user_agent\";s:43:\"DO_NOT_MATCH_GENERIC_LGUPLUS_WINMO_FACEBOOK\";s:9:\"fall_back\";s:24:\"generic_lguplus_winmo6_5\";s:12:\"product_info\";a:2:{s:10:\"model_name\";s:30:\"LGUPlus/WindowsMobile/FaceBook\";s:14:\"mobile_browser\";s:16:\"LGUPlus FaceBook\";}}'),('generic_lguplus_winmo_webviewer_browser','DO_NOT_MATCH_GENERIC_LGUPLUS_WINMO_WEBVIEWER','generic_lguplus_winmo6_5',NULL,0,'a:4:{s:2:\"id\";s:39:\"generic_lguplus_winmo_webviewer_browser\";s:10:\"user_agent\";s:44:\"DO_NOT_MATCH_GENERIC_LGUPLUS_WINMO_WEBVIEWER\";s:9:\"fall_back\";s:24:\"generic_lguplus_winmo6_5\";s:12:\"product_info\";a:2:{s:10:\"model_name\";s:31:\"LGUPlus/WindowsMobile/WebViewer\";s:14:\"mobile_browser\";s:17:\"LGUPlus WebViewer\";}}'),('generic_lguplus_android_webviewer_browser','DO_NOT_MATCH_GENERIC_LGUPLUS_ANDROID_WEBVIEWER','generic_lguplus_winmo6_5',NULL,0,'a:4:{s:2:\"id\";s:41:\"generic_lguplus_android_webviewer_browser\";s:10:\"user_agent\";s:46:\"DO_NOT_MATCH_GENERIC_LGUPLUS_ANDROID_WEBVIEWER\";s:9:\"fall_back\";s:24:\"generic_lguplus_winmo6_5\";s:12:\"product_info\";a:2:{s:10:\"model_name\";s:25:\"LGUPlus/Android/Webviewer\";s:14:\"mobile_browser\";s:17:\"LGUPlus Webviewer\";}}'),('generic_lguplus_android_webkit_browser','DO_NOT_MATCH_GENERIC_LGUPLUS_ANDROID_WEBKIT','generic_lguplus_android2',NULL,0,'a:4:{s:2:\"id\";s:38:\"generic_lguplus_android_webkit_browser\";s:10:\"user_agent\";s:43:\"DO_NOT_MATCH_GENERIC_LGUPLUS_ANDROID_WEBKIT\";s:9:\"fall_back\";s:24:\"generic_lguplus_android2\";s:12:\"product_info\";a:2:{s:10:\"model_name\";s:22:\"LGUPlus/Android/WebKit\";s:14:\"mobile_browser\";s:14:\"LGUPlus WebKit\";}}');
/*!40000 ALTER TABLE `ztw__LGUPLUS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
