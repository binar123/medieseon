-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__OperaMiniOnAndroid`
--

DROP TABLE IF EXISTS `ztw__OperaMiniOnAndroid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__OperaMiniOnAndroid` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__OperaMiniOnAndroid`
--

LOCK TABLES `ztw__OperaMiniOnAndroid` WRITE;
/*!40000 ALTER TABLE `ztw__OperaMiniOnAndroid` DISABLE KEYS */;
INSERT INTO `ztw__OperaMiniOnAndroid` VALUES ('google_nexusone_ver1_suboperamini','Opera/9.80 (J2ME/MIDP; Opera Mini/5.0 (Linux; U; Android 2.1; xx-xx; Nexus One Build/19.916; U; xx-xx) Presto/2.5.25','google_nexusone_ver1',NULL,1,'a:4:{s:2:\"id\";s:33:\"google_nexusone_ver1_suboperamini\";s:10:\"user_agent\";s:116:\"Opera/9.80 (J2ME/MIDP; Opera Mini/5.0 (Linux; U; Android 2.1; xx-xx; Nexus One Build/19.916; U; xx-xx) Presto/2.5.25\";s:9:\"fall_back\";s:20:\"google_nexusone_ver1\";s:12:\"product_info\";a:3:{s:17:\"device_os_version\";d:2.1000000000000001;s:14:\"mobile_browser\";s:10:\"Opera Mini\";s:22:\"mobile_browser_version\";s:3:\"5.0\";}}'),('samsung_gt_s5300_ver1_subuaopera','Mozilla/5.0 (Linux; U; Android 2.3; xx-xx; GT-S5300 Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1 Opera/9.80 (Android; Opera Mini/6.5.27431/28.3392; U; xx-xx) Presto/2.8.119 Version/11.10','samsung_gt_s5300_ver1',NULL,1,'a:4:{s:2:\"id\";s:32:\"samsung_gt_s5300_ver1_subuaopera\";s:10:\"user_agent\";s:230:\"Mozilla/5.0 (Linux; U; Android 2.3; xx-xx; GT-S5300 Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1 Opera/9.80 (Android; Opera Mini/6.5.27431/28.3392; U; xx-xx) Presto/2.8.119 Version/11.10\";s:9:\"fall_back\";s:21:\"samsung_gt_s5300_ver1\";s:12:\"product_info\";a:2:{s:14:\"mobile_browser\";s:10:\"Opera Mini\";s:22:\"mobile_browser_version\";d:6.5;}}'),('uabait_opera_mini_android_v50','Opera/9.80 (Android; Opera Mini/5.0.18302/18.652; U; xx-xx) Presto/2.4.15','generic_opera_mini_android_version5',NULL,1,'a:3:{s:2:\"id\";s:29:\"uabait_opera_mini_android_v50\";s:10:\"user_agent\";s:73:\"Opera/9.80 (Android; Opera Mini/5.0.18302/18.652; U; xx-xx) Presto/2.4.15\";s:9:\"fall_back\";s:35:\"generic_opera_mini_android_version5\";}'),('uabait_opera_mini_android_v51','Opera/9.80 (Android; Opera Mini/5.1.21126/19.916; U; xx-xx) Presto/2.5.25','generic_opera_mini_android_version5',NULL,1,'a:4:{s:2:\"id\";s:29:\"uabait_opera_mini_android_v51\";s:10:\"user_agent\";s:73:\"Opera/9.80 (Android; Opera Mini/5.1.21126/19.916; U; xx-xx) Presto/2.5.25\";s:9:\"fall_back\";s:35:\"generic_opera_mini_android_version5\";s:12:\"product_info\";a:1:{s:10:\"model_name\";s:8:\"Mini 5.1\";}}'),('uabait_opera_mini_android_v70','Opera/9.80 (Android; Opera Mini/7.29473/27.1382; U; xx-xx) Presto/2.8.119 Version/11.10','generic_opera_mini_android_version7',NULL,1,'a:3:{s:2:\"id\";s:29:\"uabait_opera_mini_android_v70\";s:10:\"user_agent\";s:87:\"Opera/9.80 (Android; Opera Mini/7.29473/27.1382; U; xx-xx) Presto/2.8.119 Version/11.10\";s:9:\"fall_back\";s:35:\"generic_opera_mini_android_version7\";}'),('uabait_opera_mini_android_v60','Opera/9.80 (Android; Opera Mini/6.5.27452/27.1251; U; xx-xx) Presto/2.8.119 Version/11.10','generic_opera_mini_android_version6',NULL,1,'a:3:{s:2:\"id\";s:29:\"uabait_opera_mini_android_v60\";s:10:\"user_agent\";s:89:\"Opera/9.80 (Android; Opera Mini/6.5.27452/27.1251; U; xx-xx) Presto/2.8.119 Version/11.10\";s:9:\"fall_back\";s:35:\"generic_opera_mini_android_version6\";}');
/*!40000 ALTER TABLE `ztw__OperaMiniOnAndroid` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
