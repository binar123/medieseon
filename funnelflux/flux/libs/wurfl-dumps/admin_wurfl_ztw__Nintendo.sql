-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Nintendo`
--

DROP TABLE IF EXISTS `ztw__Nintendo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Nintendo` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Nintendo`
--

LOCK TABLES `ztw__Nintendo` WRITE;
/*!40000 ALTER TABLE `ztw__Nintendo` DISABLE KEYS */;
INSERT INTO `ztw__Nintendo` VALUES ('nintendo_ds_ver1','Mozilla/4.0 (compatible; MSIE 6.0; Nitro) Opera 8.50 [en]','generic_opera_symbian',1,1,'a:8:{s:2:\"id\";s:16:\"nintendo_ds_ver1\";s:10:\"user_agent\";s:57:\"Mozilla/4.0 (compatible; MSIE 6.0; Nitro) Opera 8.50 [en]\";s:9:\"fall_back\";s:21:\"generic_opera_symbian\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:7:{s:10:\"brand_name\";s:8:\"Nintendo\";s:10:\"model_name\";s:2:\"DS\";s:18:\"is_wireless_device\";b:1;s:14:\"mobile_browser\";s:5:\"Opera\";s:22:\"mobile_browser_version\";d:8.5;s:23:\"can_assign_phone_number\";b:0;s:12:\"release_date\";s:12:\"2005_january\";}s:7:\"display\";a:4:{s:16:\"resolution_width\";d:256;s:17:\"resolution_height\";d:384;s:15:\"max_image_width\";d:244;s:16:\"max_image_height\";d:288;}s:6:\"markup\";a:2:{s:19:\"xhtml_support_level\";d:2;s:16:\"preferred_markup\";s:23:\"html_wi_oma_xhtmlmp_1_0\";}s:6:\"bearer\";a:2:{s:4:\"wifi\";b:1;s:13:\"max_data_rate\";d:200;}}'),('nintendo_wii_ver1','Opera/9.00 (Nintendo Wii; U; ; 1621; xx-xx)','generic_tv_console_browser',1,1,'a:5:{s:2:\"id\";s:17:\"nintendo_wii_ver1\";s:10:\"user_agent\";s:43:\"Opera/9.00 (Nintendo Wii; U; ; 1621; xx-xx)\";s:9:\"fall_back\";s:26:\"generic_tv_console_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:5:{s:14:\"mobile_browser\";s:5:\"Opera\";s:22:\"mobile_browser_version\";s:3:\"9.0\";s:10:\"model_name\";s:3:\"Wii\";s:10:\"brand_name\";s:8:\"Nintendo\";s:12:\"release_date\";s:12:\"2007_january\";}}'),('nintendo_wii_ver1_subop93','Opera/9.30 (Nintendo Wii; U; ; 2047-7; xx-xx)','nintendo_wii_ver1_subua910',NULL,1,'a:4:{s:2:\"id\";s:25:\"nintendo_wii_ver1_subop93\";s:10:\"user_agent\";s:45:\"Opera/9.30 (Nintendo Wii; U; ; 2047-7; xx-xx)\";s:9:\"fall_back\";s:26:\"nintendo_wii_ver1_subua910\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";s:4:\"9.30\";}}'),('nintendo_wii_ver1_subua910','Opera/9.10 (Nintendo Wii; U; ; 1621; xx-xx)','nintendo_wii_ver1',NULL,1,'a:4:{s:2:\"id\";s:26:\"nintendo_wii_ver1_subua910\";s:10:\"user_agent\";s:43:\"Opera/9.10 (Nintendo Wii; U; ; 1621; xx-xx)\";s:9:\"fall_back\";s:17:\"nintendo_wii_ver1\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";d:9.0999999999999996;}}'),('nintendo_dsi_ver1','Opera/9.50 (Nintendo DSi; Opera/483; U; xx-xx)','nintendo_ds_ver1',1,1,'a:6:{s:2:\"id\";s:17:\"nintendo_dsi_ver1\";s:10:\"user_agent\";s:46:\"Opera/9.50 (Nintendo DSi; Opera/483; U; xx-xx)\";s:9:\"fall_back\";s:16:\"nintendo_ds_ver1\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:2:{s:10:\"model_name\";s:3:\"DSi\";s:22:\"mobile_browser_version\";d:9.5;}s:7:\"display\";a:1:{s:15:\"max_image_width\";d:256;}}'),('nintendo_3ds_ver1','Mozilla/5.0 (Nintendo 3DS; U; ; xx-xx) Version/1.7455.EU','generic_xhtml',1,1,'a:13:{s:2:\"id\";s:17:\"nintendo_3ds_ver1\";s:10:\"user_agent\";s:56:\"Mozilla/5.0 (Nintendo 3DS; U; ; xx-xx) Version/1.7455.EU\";s:9:\"fall_back\";s:13:\"generic_xhtml\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:9:{s:10:\"model_name\";s:3:\"3DS\";s:22:\"mobile_browser_version\";d:1.7;s:12:\"release_date\";s:14:\"2011_september\";s:14:\"mobile_browser\";s:11:\"Netfront NX\";s:9:\"device_os\";s:0:\"\";s:10:\"brand_name\";s:8:\"Nintendo\";s:15:\"pointing_method\";s:6:\"stylus\";s:23:\"can_assign_phone_number\";b:0;s:25:\"can_skip_aligned_link_row\";b:1;}s:7:\"html_ui\";a:1:{s:14:\"canvas_support\";s:4:\"full\";}s:6:\"markup\";a:2:{s:16:\"preferred_markup\";s:12:\"html_web_4_0\";s:19:\"xhtml_support_level\";d:4;}s:7:\"display\";a:8:{s:7:\"columns\";d:40;s:22:\"physical_screen_height\";d:40;s:21:\"physical_screen_width\";d:60;s:4:\"rows\";d:10;s:15:\"max_image_width\";d:400;s:17:\"resolution_height\";d:240;s:16:\"resolution_width\";d:400;s:16:\"max_image_height\";d:200;}s:4:\"ajax\";a:1:{s:23:\"ajax_support_javascript\";b:1;}s:9:\"streaming\";a:1:{s:13:\"streaming_mp4\";b:1;}s:3:\"pdf\";a:1:{s:11:\"pdf_support\";b:0;}s:10:\"flash_lite\";a:1:{s:18:\"full_flash_support\";b:0;}s:6:\"bearer\";a:2:{s:4:\"wifi\";b:1;s:13:\"max_data_rate\";d:7200;}}'),('nintendo_wii_u_ver1','Mozilla/5.0 (Nintendo WiiU) AppleWebKit/534.52 (KHTML, like Gecko) NX/2.1.0.8.23 NintendoBrowser/1.1.0.7579.US','generic_tv_console_browser',1,1,'a:5:{s:2:\"id\";s:19:\"nintendo_wii_u_ver1\";s:10:\"user_agent\";s:110:\"Mozilla/5.0 (Nintendo WiiU) AppleWebKit/534.52 (KHTML, like Gecko) NX/2.1.0.8.23 NintendoBrowser/1.1.0.7579.US\";s:9:\"fall_back\";s:26:\"generic_tv_console_browser\";s:18:\"actual_device_root\";i:1;s:12:\"product_info\";a:5:{s:10:\"model_name\";s:5:\"Wii U\";s:10:\"brand_name\";s:8:\"Nintendo\";s:12:\"release_date\";s:13:\"2012_november\";s:14:\"mobile_browser\";s:11:\"Netfront NX\";s:22:\"mobile_browser_version\";d:2.1000000000000001;}}'),('nintendo_wii_u_ver1_subuabokken','Mozilla/5.0 (WiiU; Bokken) AppleWebkit/525.1+ (KHTML, like Gecko) Bokken/01.01.01 Safari/525.1+ Nintendo WiiU/ (WiiU,,en,us)','nintendo_wii_u_ver1',NULL,1,'a:3:{s:2:\"id\";s:31:\"nintendo_wii_u_ver1_subuabokken\";s:10:\"user_agent\";s:124:\"Mozilla/5.0 (WiiU; Bokken) AppleWebkit/525.1+ (KHTML, like Gecko) Bokken/01.01.01 Safari/525.1+ Nintendo WiiU/ (WiiU,,en,us)\";s:9:\"fall_back\";s:19:\"nintendo_wii_u_ver1\";}');
/*!40000 ALTER TABLE `ztw__Nintendo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:35
