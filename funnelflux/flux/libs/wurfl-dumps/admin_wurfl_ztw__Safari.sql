-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Safari`
--

DROP TABLE IF EXISTS `ztw__Safari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Safari` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Safari`
--

LOCK TABLES `ztw__Safari` WRITE;
/*!40000 ALTER TABLE `ztw__Safari` DISABLE KEYS */;
INSERT INTO `ztw__Safari` VALUES ('safari_525_mac_osx','Mozilla/5.0 (Macintosh; U; Safari/525','safari_525',NULL,1,'a:3:{s:2:\"id\";s:18:\"safari_525_mac_osx\";s:10:\"user_agent\";s:37:\"Mozilla/5.0 (Macintosh; U; Safari/525\";s:9:\"fall_back\";s:10:\"safari_525\";}'),('safari_525_win','Mozilla/5.0 (Windows; U; Safari/525','safari_525',NULL,1,'a:3:{s:2:\"id\";s:14:\"safari_525_win\";s:10:\"user_agent\";s:35:\"Mozilla/5.0 (Windows; U; Safari/525\";s:9:\"fall_back\";s:10:\"safari_525\";}'),('safari_530_mac_osx','Mozilla/5.0 (Macintosh; U; Safari/530','safari_530',NULL,1,'a:3:{s:2:\"id\";s:18:\"safari_530_mac_osx\";s:10:\"user_agent\";s:37:\"Mozilla/5.0 (Macintosh; U; Safari/530\";s:9:\"fall_back\";s:10:\"safari_530\";}'),('safari_530_win','Mozilla/5.0 (Windows; U; Safari/530','safari_530',NULL,1,'a:3:{s:2:\"id\";s:14:\"safari_530_win\";s:10:\"user_agent\";s:35:\"Mozilla/5.0 (Windows; U; Safari/530\";s:9:\"fall_back\";s:10:\"safari_530\";}'),('safari_3_0_windows','Safari 3---Mozilla/5.0 (Windows; U; Windows NT 6.1; xx-xx) AppleWebKit/523.15 (KHTML, like Gecko) Version/3.0 Safari/523.15','safari',NULL,1,'a:4:{s:2:\"id\";s:18:\"safari_3_0_windows\";s:10:\"user_agent\";s:123:\"Safari 3---Mozilla/5.0 (Windows; U; Windows NT 6.1; xx-xx) AppleWebKit/523.15 (KHTML, like Gecko) Version/3.0 Safari/523.15\";s:9:\"fall_back\";s:6:\"safari\";s:12:\"product_info\";a:3:{s:12:\"release_date\";s:9:\"2007_june\";s:22:\"mobile_browser_version\";s:3:\"3.0\";s:9:\"device_os\";s:7:\"Desktop\";}}'),('safari_3_0_mac','Safari 3---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; xx-xx) AppleWebKit/523.15 (KHTML, like Gecko) Version/3.0 Safari/523.15','safari',NULL,1,'a:4:{s:2:\"id\";s:14:\"safari_3_0_mac\";s:10:\"user_agent\";s:132:\"Safari 3---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; xx-xx) AppleWebKit/523.15 (KHTML, like Gecko) Version/3.0 Safari/523.15\";s:9:\"fall_back\";s:6:\"safari\";s:12:\"product_info\";a:2:{s:12:\"release_date\";s:9:\"2007_june\";s:22:\"mobile_browser_version\";s:3:\"3.0\";}}'),('safari_4_0_mac','Safari 4---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_4; xx-xx) AppleWebKit/530.3 (KHTML, like Gecko) Version/4.0 Safari/530.3','safari_3_0_mac',NULL,1,'a:4:{s:2:\"id\";s:14:\"safari_4_0_mac\";s:10:\"user_agent\";s:128:\"Safari 4---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_4; xx-xx) AppleWebKit/530.3 (KHTML, like Gecko) Version/4.0 Safari/530.3\";s:9:\"fall_back\";s:14:\"safari_3_0_mac\";s:12:\"product_info\";a:3:{s:12:\"release_date\";s:13:\"2009_february\";s:22:\"mobile_browser_version\";s:3:\"4.0\";s:17:\"device_os_version\";d:10.4;}}'),('safari_5_0_mac','Safari 5---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; xx-xx) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0 Safari/533.21.1','safari_4_0_mac',NULL,1,'a:4:{s:2:\"id\";s:14:\"safari_5_0_mac\";s:10:\"user_agent\";s:136:\"Safari 5---Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; xx-xx) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0 Safari/533.21.1\";s:9:\"fall_back\";s:14:\"safari_4_0_mac\";s:12:\"product_info\";a:3:{s:12:\"release_date\";s:9:\"2010_june\";s:22:\"mobile_browser_version\";s:3:\"5.0\";s:17:\"device_os_version\";d:10.6;}}'),('safari_6_0_mac','Safari 6---Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8) AppleWebKit/536.20 (KHTML, like Gecko) Version/6.0 Safari/536.20','safari_5_0_mac',NULL,1,'a:4:{s:2:\"id\";s:14:\"safari_6_0_mac\";s:10:\"user_agent\";s:120:\"Safari 6---Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8) AppleWebKit/536.20 (KHTML, like Gecko) Version/6.0 Safari/536.20\";s:9:\"fall_back\";s:14:\"safari_5_0_mac\";s:12:\"product_info\";a:3:{s:12:\"release_date\";s:9:\"2012_july\";s:22:\"mobile_browser_version\";s:3:\"6.0\";s:17:\"device_os_version\";d:10.800000000000001;}}'),('safari_4_0_windows','Safari 4---Mozilla/5.0 (Windows; U; Windows NT 6.1; xx-xx) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Safari/530.17','safari_3_0_windows',NULL,1,'a:4:{s:2:\"id\";s:18:\"safari_4_0_windows\";s:10:\"user_agent\";s:123:\"Safari 4---Mozilla/5.0 (Windows; U; Windows NT 6.1; xx-xx) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Safari/530.17\";s:9:\"fall_back\";s:18:\"safari_3_0_windows\";s:12:\"product_info\";a:2:{s:12:\"release_date\";s:13:\"2009_february\";s:22:\"mobile_browser_version\";s:3:\"4.0\";}}'),('safari_5_0_windows','Safari 5---Mozilla/5.0 (Windows; U; Windows NT 6.1; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16','safari_4_0_windows',NULL,1,'a:4:{s:2:\"id\";s:18:\"safari_5_0_windows\";s:10:\"user_agent\";s:123:\"Safari 5---Mozilla/5.0 (Windows; U; Windows NT 6.1; xx-xx) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16\";s:9:\"fall_back\";s:18:\"safari_4_0_windows\";s:12:\"product_info\";a:2:{s:12:\"release_date\";s:9:\"2010_june\";s:22:\"mobile_browser_version\";s:3:\"5.0\";}}'),('safari_7_0_mac','Safari 7---Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9) AppleWebKit/537.71 (KHTML, like Gecko) Version/7.0 Safari/537.71','safari_6_0_mac',NULL,1,'a:4:{s:2:\"id\";s:14:\"safari_7_0_mac\";s:10:\"user_agent\";s:120:\"Safari 7---Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9) AppleWebKit/537.71 (KHTML, like Gecko) Version/7.0 Safari/537.71\";s:9:\"fall_back\";s:14:\"safari_6_0_mac\";s:12:\"product_info\";a:3:{s:22:\"mobile_browser_version\";s:3:\"7.0\";s:12:\"release_date\";s:9:\"2013_june\";s:17:\"device_os_version\";d:10.9;}}'),('safari_8_0_mac','Safari 8---Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.2.5 (KHTML, like Gecko) Version/8.0.2 Safari/600.2.5','safari_7_0_mac',NULL,1,'a:5:{s:2:\"id\";s:14:\"safari_8_0_mac\";s:10:\"user_agent\";s:127:\"Safari 8---Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.2.5 (KHTML, like Gecko) Version/8.0.2 Safari/600.2.5\";s:9:\"fall_back\";s:14:\"safari_7_0_mac\";s:12:\"product_info\";a:2:{s:22:\"mobile_browser_version\";s:3:\"8.0\";s:17:\"device_os_version\";s:5:\"10.10\";}s:7:\"html_ui\";a:1:{s:14:\"canvas_support\";s:4:\"full\";}}'),('safari_9_0_mac','Safari 9---Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56','safari_8_0_mac',NULL,1,'a:4:{s:2:\"id\";s:14:\"safari_9_0_mac\";s:10:\"user_agent\";s:127:\"Safari 9---Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56\";s:9:\"fall_back\";s:14:\"safari_8_0_mac\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";s:3:\"9.0\";}}');
/*!40000 ALTER TABLE `ztw__Safari` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:34
