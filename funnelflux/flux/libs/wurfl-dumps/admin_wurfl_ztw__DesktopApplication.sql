-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__DesktopApplication`
--

DROP TABLE IF EXISTS `ztw__DesktopApplication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__DesktopApplication` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__DesktopApplication`
--

LOCK TABLES `ztw__DesktopApplication` WRITE;
/*!40000 ALTER TABLE `ztw__DesktopApplication` DISABLE KEYS */;
INSERT INTO `ztw__DesktopApplication` VALUES ('mozilla_thunderbird_subua17','Thunderbird/17.0','mozilla_thunderbird',NULL,1,'a:4:{s:2:\"id\";s:27:\"mozilla_thunderbird_subua17\";s:10:\"user_agent\";s:16:\"Thunderbird/17.0\";s:9:\"fall_back\";s:19:\"mozilla_thunderbird\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";d:17;}}'),('mozilla_thunderbird_subua22','Thunderbird/22.0','mozilla_thunderbird_subua17',NULL,1,'a:4:{s:2:\"id\";s:27:\"mozilla_thunderbird_subua22\";s:10:\"user_agent\";s:16:\"Thunderbird/22.0\";s:9:\"fall_back\";s:27:\"mozilla_thunderbird_subua17\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";d:22;}}'),('mozilla_thunderbird_subua23','Thunderbird/23.0','mozilla_thunderbird_subua22',NULL,1,'a:4:{s:2:\"id\";s:27:\"mozilla_thunderbird_subua23\";s:10:\"user_agent\";s:16:\"Thunderbird/23.0\";s:9:\"fall_back\";s:27:\"mozilla_thunderbird_subua22\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";d:23;}}'),('mozilla_thunderbird_subua24','Thunderbird/24.0','mozilla_thunderbird_subua23',NULL,1,'a:4:{s:2:\"id\";s:27:\"mozilla_thunderbird_subua24\";s:10:\"user_agent\";s:16:\"Thunderbird/24.0\";s:9:\"fall_back\";s:27:\"mozilla_thunderbird_subua23\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";d:24;}}'),('mozilla_thunderbird_subua241','Thunderbird/24.1.0','mozilla_thunderbird_subua24',NULL,1,'a:4:{s:2:\"id\";s:28:\"mozilla_thunderbird_subua241\";s:10:\"user_agent\";s:18:\"Thunderbird/24.1.0\";s:9:\"fall_back\";s:27:\"mozilla_thunderbird_subua24\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";d:24.100000000000001;}}'),('mozilla_thunderbird_subua25','Thunderbird/25.0','mozilla_thunderbird_subua241',NULL,1,'a:4:{s:2:\"id\";s:27:\"mozilla_thunderbird_subua25\";s:10:\"user_agent\";s:16:\"Thunderbird/25.0\";s:9:\"fall_back\";s:28:\"mozilla_thunderbird_subua241\";s:12:\"product_info\";a:1:{s:22:\"mobile_browser_version\";d:25;}}');
/*!40000 ALTER TABLE `ztw__DesktopApplication` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:35
