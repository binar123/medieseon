-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: admin_wurfl
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ztw__Ucweb7OnAndroid`
--

DROP TABLE IF EXISTS `ztw__Ucweb7OnAndroid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ztw__Ucweb7OnAndroid` (
  `deviceID` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `user_agent` varchar(512) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `fall_back` varchar(64) DEFAULT NULL,
  `actual_device_root` tinyint(1) DEFAULT '0',
  `match` tinyint(1) DEFAULT '1',
  `capabilities` mediumtext,
  PRIMARY KEY (`deviceID`),
  KEY `fallback` (`fall_back`),
  KEY `useragent` (`user_agent`),
  KEY `dev_root` (`actual_device_root`),
  KEY `idxmatch` (`match`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ztw__Ucweb7OnAndroid`
--

LOCK TABLES `ztw__Ucweb7OnAndroid` WRITE;
/*!40000 ALTER TABLE `ztw__Ucweb7OnAndroid` DISABLE KEYS */;
INSERT INTO `ztw__Ucweb7OnAndroid` VALUES ('htc_wildfire_ver1_subuaucweb','JUC (Linux; U; Android 2.2; Zh_cn; HTC Wildfire; 240*320;) UCWEB7.8.0.95/140/444','htc_wildfire_ver1',NULL,1,'a:4:{s:2:\"id\";s:28:\"htc_wildfire_ver1_subuaucweb\";s:10:\"user_agent\";s:80:\"JUC (Linux; U; Android 2.2; Zh_cn; HTC Wildfire; 240*320;) UCWEB7.8.0.95/140/444\";s:9:\"fall_back\";s:17:\"htc_wildfire_ver1\";s:12:\"product_info\";a:3:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:22:\"mobile_browser_version\";d:7.7999999999999998;s:17:\"device_os_version\";d:2.2000000000000002;}}'),('samsung_sph_m920_ver1_subuauc','Mozilla/5.0 (Linux; U; Android 2.2; xx-xx; SPH-M920 Build/FROYO; 320*480) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1/UCWEB7.9.3.103/139/999','samsung_sph_m920_ver1',NULL,1,'a:4:{s:2:\"id\";s:29:\"samsung_sph_m920_ver1_subuauc\";s:10:\"user_agent\";s:172:\"Mozilla/5.0 (Linux; U; Android 2.2; xx-xx; SPH-M920 Build/FROYO; 320*480) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1/UCWEB7.9.3.103/139/999\";s:9:\"fall_back\";s:21:\"samsung_sph_m920_ver1\";s:12:\"product_info\";a:3:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:22:\"mobile_browser_version\";d:7.9000000000000004;s:17:\"device_os_version\";d:2.2000000000000002;}}'),('samsung_gt_s5360_ver1_subuaucwebjuc','JUC (Linux; U; Android 2.3; Zh_cn; GT-S5360; 240*320;) UCWEB7.8.0.95/139/444','samsung_gt_s5360_ver1',NULL,1,'a:4:{s:2:\"id\";s:35:\"samsung_gt_s5360_ver1_subuaucwebjuc\";s:10:\"user_agent\";s:76:\"JUC (Linux; U; Android 2.3; Zh_cn; GT-S5360; 240*320;) UCWEB7.8.0.95/139/444\";s:9:\"fall_back\";s:21:\"samsung_gt_s5360_ver1\";s:12:\"product_info\";a:2:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:22:\"mobile_browser_version\";d:7.7999999999999998;}}'),('sonyericsson_st25i_ver1_subuauc','JUC (Linux; U; Android 2.3; xx-xx; ST25i; 480*854) UCWEB7.9.0.94/139/444','sonyericsson_st25i_ver1',NULL,1,'a:4:{s:2:\"id\";s:31:\"sonyericsson_st25i_ver1_subuauc\";s:10:\"user_agent\";s:72:\"JUC (Linux; U; Android 2.3; xx-xx; ST25i; 480*854) UCWEB7.9.0.94/139/444\";s:9:\"fall_back\";s:23:\"sonyericsson_st25i_ver1\";s:12:\"product_info\";a:2:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:22:\"mobile_browser_version\";d:7.9000000000000004;}}'),('htc_chacha_ver1_subuauc7','JUC (Linux; U; Android 2.3; xx-xx; HTC_ChaCha_A810e; 320*480) UCWEB7.9.0.94/140/444','htc_chacha_ver1',NULL,1,'a:4:{s:2:\"id\";s:24:\"htc_chacha_ver1_subuauc7\";s:10:\"user_agent\";s:83:\"JUC (Linux; U; Android 2.3; xx-xx; HTC_ChaCha_A810e; 320*480) UCWEB7.9.0.94/140/444\";s:9:\"fall_back\";s:15:\"htc_chacha_ver1\";s:12:\"product_info\";a:2:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:22:\"mobile_browser_version\";d:7;}}'),('samsung_gt_i9100_ver1_subuajuc','JUC (Linux; U; Android 2.3; xx-xx; GT-I9100; 480*800) UCWEB7.9.3.103/139/800','samsung_gt_i9100_ver1',NULL,1,'a:4:{s:2:\"id\";s:30:\"samsung_gt_i9100_ver1_subuajuc\";s:10:\"user_agent\";s:76:\"JUC (Linux; U; Android 2.3; xx-xx; GT-I9100; 480*800) UCWEB7.9.3.103/139/800\";s:9:\"fall_back\";s:21:\"samsung_gt_i9100_ver1\";s:12:\"product_info\";a:2:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:22:\"mobile_browser_version\";d:7;}}'),('sonyericsson_lt18i_ver1_subuajuc','JUC (Linux; U; Android 2.3; xx-xx; LT18i; 480*854) UCWEB7.9.3.103/139/800','sonyericsson_lt18i_ver1',NULL,1,'a:4:{s:2:\"id\";s:32:\"sonyericsson_lt18i_ver1_subuajuc\";s:10:\"user_agent\";s:73:\"JUC (Linux; U; Android 2.3; xx-xx; LT18i; 480*854) UCWEB7.9.3.103/139/800\";s:9:\"fall_back\";s:23:\"sonyericsson_lt18i_ver1\";s:12:\"product_info\";a:2:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:22:\"mobile_browser_version\";d:7;}}'),('samsung_gt_s7500_ver1_subuajuc','JUC (Linux; U; Android 2.3; xx-xx; GT-S7500; 480*320) UCWEB7.9.0.94/139/800','samsung_gt_s7500_ver1',NULL,1,'a:4:{s:2:\"id\";s:30:\"samsung_gt_s7500_ver1_subuajuc\";s:10:\"user_agent\";s:75:\"JUC (Linux; U; Android 2.3; xx-xx; GT-S7500; 480*320) UCWEB7.9.0.94/139/800\";s:9:\"fall_back\";s:21:\"samsung_gt_s7500_ver1\";s:12:\"product_info\";a:1:{s:14:\"mobile_browser\";s:5:\"UCWeb\";}}'),('samsung_gt_i5500_ver1_suban22juc','JUC (Linux; U; Android 2.2; xx-xx; GT-I5500; 240*320) UCWEB7.9.0.94/139/444','samsung_gt_i5500_ver1',NULL,1,'a:4:{s:2:\"id\";s:32:\"samsung_gt_i5500_ver1_suban22juc\";s:10:\"user_agent\";s:75:\"JUC (Linux; U; Android 2.2; xx-xx; GT-I5500; 240*320) UCWEB7.9.0.94/139/444\";s:9:\"fall_back\";s:21:\"samsung_gt_i5500_ver1\";s:12:\"product_info\";a:2:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:17:\"device_os_version\";d:2.2000000000000002;}}'),('samsung_gt_s5302_ver1_subauc','Mozilla/5.0 (Linux; U; Android 2.3; xx-xx; GT-S5302 Build/GINGERBREAD; 240*320) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1/UCWEB7.9.0.94/139/444','samsung_gt_s5302_ver1',NULL,1,'a:4:{s:2:\"id\";s:28:\"samsung_gt_s5302_ver1_subauc\";s:10:\"user_agent\";s:177:\"Mozilla/5.0 (Linux; U; Android 2.3; xx-xx; GT-S5302 Build/GINGERBREAD; 240*320) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1/UCWEB7.9.0.94/139/444\";s:9:\"fall_back\";s:21:\"samsung_gt_s5302_ver1\";s:12:\"product_info\";a:2:{s:14:\"mobile_browser\";s:5:\"UCWeb\";s:22:\"mobile_browser_version\";d:7.9000000000000004;}}');
/*!40000 ALTER TABLE `ztw__Ucweb7OnAndroid` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-06 15:29:35
