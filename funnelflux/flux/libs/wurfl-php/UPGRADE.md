# Upgrade Guide

## Upgrade to 1.8 from 1.7

> Note: there are breaking changes in API release v1.8.
This upgrade guide will show how upgrade from the 1.7.
If you have a different setup or need additional help feel free to contact us at support@scientiamobile.com

### Requirements

- PHP >= 5.4 (suggested 5.6 or higher)
- extension: xml (XMLReader)
- [Composer](https://getcomposer.org/)

### Updating Dependencies

Extract the v1.8+ PHP API archive as usual and run the following command from the API's root path (i.e. `/u/apps/WURFL-API-PHP`):

`composer install --no-dev --optimize-autoloader`

> NOTE: If you are already using Composer in your project, you can add the WURFL API to your `composer.json` and run `composer update` to install its dependencies.  See the [WURFL OnSite PHP API documentation](https://docs.scientiamobile.com/documentation/onsite/onsite-php-api) for details.

### Bootstrap

Inside your application you must include the new autoloader.

Replace

`require_once "/u/apps/WURFL-API-PHP/WURFL/Application.php";`

with

`require_once "/u/apps/WURFL-API-PHP/vendor/autoload.php";`

> NOTE: Make sure to replace the paths above with your actual paths!

### Configuration

The PHP API uses a dependency container to configure and inject the required dependencies.
To configure WURFL you need to create a PHP configuration file returning a `Container` object.

Create a configuration file (i.e. '/path/to/config.php' )
This file will be used to configure the dependencies for the **WURFLEngine** object
and the **wurfl-updater** tools.

Below is an example configuration with references to values from the deprecated `WURFL_Configuration_InMemoryConfig` object:

Replace

```
$storage_dir = "/u/data/wurfl-api-data";
$wurfl_file = "/u/data/wurfl/wurfl.xml";

$wurfl_config = new WURFL_Configuration_InMemoryConfig();
$wurfl_config->wurflFile($wurfl_file);
$wurfl_config->allowReload(true);
$wurfl_config->matchMode("accuracy");
$wurfl_config->persistence("file", [
    "dir" => "$storage_dir/persistence",
]);
$wurfl_config->cache("file", [
    "dir" => "$storage_dir/cache",
    "expiration" => 36000,
]);
```

with

```
use ScientiaMobile\WURFL\Container\Container;
use ScientiaMobile\WURFL\Storage\StorageFactory;
use ScientiaMobile\WURFL\Cache\CacheFactory;

$storage_dir = '/u/data/wurfl-api-data';
$wurfl_file = '/u/data/wurfl/wurfl.xml';

$container = new Container([
    'wurfl_db' => $wurfl_file,
    'wurfl_storage_path' => $storage_dir,
]);

$storage = StorageFactory::createFileStorage("$storage_dir/persistence");
$container->setStorageAdapter($storage);

$cache = CacheFactory::createFileCache(36000, "$storage_dir/cache");
$container->setCacheAdapter($cache);

return $container;

```

and save the code above in the **configuration** file.


> Note:
- `$wurfl_config->allowReload();` is not needed since the wurfl upload process is handled by the wurfl-updater tool now.
- The `matchMode` is specified using the WURFLEngine object, see below.
- PHP API supports different storage and cache adapters, see the [WURFL OnSite PHP API documentation](https://docs.scientiamobile.com/documentation/onsite/onsite-php-api) for details.


### Run the wurfl-updater tool

API version 1.8.0.0 introduces WURFL updater; a new command line utility which is used to automatically update the WURFL database.

To load the local WURFL DB:

```
php /u/apps/WURFL-API-PHP/vendor/bin/wurfl-updater -c /path/to/config.php
```

### Class instantiation

The `WURFL_WURFLManagerFactory` class must be replaced a `WURFLEngine` object:

Replace

```
$wurfl_factory = new WURFL_WURFLManagerFactory($wurfl_config);
$wurfl = $wurfl_factory->create();
```

with

```
use ScientiaMobile\WURFL\WURFLEngine;

$container = require '/path/to/config.php';

// instantiating the class with a custom dependency container
$wurfl = new WURFLEngine($container);
```

### Configure the performance mode

By default, the WURFL Engine runs in high accuracy mode.
To override the default and enable performance mode:

```
$wurfl->enablePerformanceMode();
```
