# ScientiaMobile WURFL PHP API #

- http://www.scientiamobile.com/

----------

> Please note - there are breaking changes in API release v1.8. For documentation of releases before API v1.8 please refer to the README file included with that version. If you need aditional help for versions prior to 1.8 feel free to contact us at support@scientiamobile.com

# Installation
Once you have registered for a free account on scientiamobile.com and downloaded the latest release from your File Manager, you will need to extract the files to be accessible from your PHP enabled webserver.

> __IMPORTANT__: The WURFL API is closely tied to the WURFL.XML file.  New versions of the WURFL.XML are compatible with old versions of the API by nature, but the reverse is NOT true.  Old versions of the WURFL.XML are NOT guarenteed to be compatible with new versions of the API.  Let's restate that: This API is NOT compatible with old versions of the WURFL.XML.  The oldest copy of WURFL that can be used with this API is included in this distribution.

To start using the API you need to set some configuration options.

## Requirements
- PHP >= 5.4 (suggested 5.6 or higher)
- extension: xml (XMLReader)
- [Composer](https://getcomposer.org/)

## Installing Prerequisites

The WURFL PHP API project uses [Composer](https://getcomposer.org/) for dependency management.

## Install dependencies

```
composer install --optimize-autoloader --no-dev
```

## Adds WURFL PHP API to an existing project using composer.json

If your project uses composer, to simplify maintenance, you can specify the WURFL PHP API as an [artifact](https://getcomposer.org/doc/05-repositories.md#artifact) repository:

- copy the WURFL PHP API ZIP package in a folder will contain the archives
- add the following configuration to your composer.json:
```
"repositories": [
        {
            "type": "artifact",
            "url": "/path/to/directory/with/zips/"
        }
    ],
    "require": {
        "scientiamobile/wurfl-api": "*"
    }
```
- run `composer install` to install the dependencies

Note: in order to update the package simply copy the new ZIP archive in the package folder and run `composer update`

# Configuring WURFL

API 1.8 uses a dependency container to configure and inject the required dependencies.
API 1.8 implements the [Container-Interop](https://github.com/container-interop/container-interop) interface so you can use the built-in Container or any third-party container that implements this interface.

To configure WURFL you need to create a PHP configuration file returning a Container object.
As reference or starting point please refer to the `config/config.php.example` file.

## Required services

The required service are provided to you by the API built-in container, however if you want
use a third-party container it MUST provide the following services:

- **settings**

  Associative array of settings with the keys:
  - wurfl_db: a full path to the WURFL DB file (default to: `resources/wurfl.xml`)
  - wurfl_patches: an array containing the WURFL patches' full path
  - wurfl_capability_filter: an array with the capabilities to load in order to reduce memory usage and increase performance
  - wurfl_snapshot_url: The WURFL Snapshot URL from your customer vault on scientiamobile.com. Ex. https://data.scientiamobile.com/xxxxx/wurfl.zip
  - wurfl_storage_path: the absolute path to the storage dir. Default to "storage"
  - wurfl_debug: a boolean that enable the debug mode (default to: `false`)

- **storage**

  An instance of `\ScientiaMobile\WURFL\Storage\StorageAdapterInterface`

  Default: instance of `\ScientiaMobile\WURFL\Storage\FileStorage`

- **cache**

  An instance of `\ScientiaMobile\WURFL\Cache\CacheAdapter`

  Default: instance of `\ScientiaMobile\WURFL\Cache\FileCache`

- **logger**

  An instance of `\Psr\Log\LoggerInterface`

  Default: instance of `\Psr\Log\NullLogger`

> Note: Since the default configuration uses the file system for both Persistence and Caching,
> you'll need to make sure your webserver can write to the **storage** directories.


## Create a custom dependency container ###

1. Create a new WURFL Dependency Container with optional settings

```
// Create WURFL Dependency Container
$container = new \ScientiaMobile\WURFL\Container\Container([
    'wurfl_db' => '/path/to/wurfl.xml',
    'wurfl_capability_filter' => [
        'device_os',
        'device_os_version',
        ...
    ],
    'wurfl_patches' => [
        '/full/path/to/patch-1.xml',
        '/full/path/to/patch-2.xml',
        ...
    ],
    ...
]);
```

2. Create a Storage adapter using either the StorageFactory factory, or by simply instantiating one of the Storage classes.
  The API supports the following storage mechanisms:
  - File (default)
  - SQLite through the PDO extension
  - Redis through the redis extension
  - MySQL through the PDO or the mysqli extension
  - MongoDB through the mongodb extension
  - Memory

  #### StorageFactory examples

  ```
  use ScientiaMobile\WURFL\Storage\StorageFactory;

  $settings = []; //Array of optional settings

  //File
  $storage = StorageFactory::createFileStorage('/path/to/storage/folder');

  //SQLite storage with the PDO extension
  $storage = StorageFactory::createSqliteStorage($settings);

  //Redis storage
  $storage = StorageFactory::createRedisStorage($settings);

  //MongoDB storage
  $storage = StorageFactory::createMongoStorage($settings);

  //MySQL storage with the PDO extension
  $storage = StorageFactory::createMysqlStorage(['driver' => 'pdo_mysql']);

  //MySQL storage with mysqli extension
  $storage = StorageFactory::createMysqlStorage(['driver' => 'mysqli']);

  //Memory storage
  $storage = StorageFactory::createMemoryStorage();
  ```

3. Configure the Cache provider by specifying the provider and the extra parameters needed to initialize the provider.

  The API supports the following caching mechanisms:
  - Memcache (http://php.net/manual/en/book.memcache.php)
  - APCu (Alternative PHP Cache http://www.php.net/apcu)
  - Redis
  - File
  - SQLite
  - MySQL
  - MongoDB
  - Null (no caching, default)

  #### CacheFactory examples

  ```
  use ScientiaMobile\WURFL\Cache\CacheFactory;
  use ScientiaMobile\WURFL\Cache\CacheAdapterInterface;

  //Time to live for cache item
  $ttl = CacheAdapterInterface::NEVER; //Default
  $settings = []; //Array of optional settings

  //APCu
  $cache = CacheFactory::createApcuCache($ttl, $settings);

  //Redis
  $cache = CacheFactory::createRedisCache($ttl, $settings);

  //Memcache
  $cache = CacheFactory::createMemcacheCache($ttl, $settings);

  //File
  $cache = CacheFactory::createFileCache($ttl, '/path/to/storage/folder');

  //SQLite cache with the PDO extension
  $cache = CacheFactory::createSqliteCache($ttl, $settings);

  //MongoDB cache
  $storage = CacheFactory::createMongoCache($ttl, $settings);

  //MySQL cache with the PDO extension
  $cache = CacheFactory::createMysqlCache($ttl, ['driver' => 'pdo_mysql']);

  //MySQL cache with mysqli extension
  $cache = CacheFactory::createMysqlCache($ttl, ['driver' => 'mysqli']);

  //Memory cache
  $cache = CacheFactory::createMemoryCache();
  ```

4. Add the storage and cache adapters to the container

  ```
  $container->setStorageAdapter($storage);
  $container->setCacheAdapter($cache);
  ```

## The WURFL Engine

The WURFL Engine is the API's entry point.
The WURFL Engine can be created using a factory method or simply instantiating the class.

```
use \ScientiaMobile\WURFL\WURFLEngine;

$container = require '/path/to/config.php';

// instantiating the class with a custom dependency container
$wurfl_engine = new WURFLEngine($container);
```

### Enable the performance mode

The WURFL Engine can be configured to run in performance mode (i.e. simple desktop matching). This feature bypasses the advanced detection methods that are normally used while detecting desktop web browsers; instead, most desktop browsers are detected using simple keywords and expressions.  When enabled, this setting will increase performance but could result in some false positives.

```
// Enable the performance mode. Default to accuracy.
$wurfl_engine->enablePerformanceMode();
```

## The WURFL Updater

API version 1.8.0.0 introduces WURFL updater; a new command line utility which allow a client using WURFL to automatically update.
In order to use the WURFL Updater, you must have your personal WURFL Snapshot url in the following format: https://data.scientiamobile.com/xxxxx/wurfl.zip where xxxxx is replaced with you personal access token.
Also, do note that `wurfl_storage_path` must be writable from the process/task.

### Usage

`php wurfl-updater -c /path/to/config.php`

The `wurfl-updater` is located under:
- `/your/project/root/path/vendor/bin/` for project that uses the API as composer dependency
- `/path/to/wurfl-api/` for project that uses the API as standalone library

### Engine reload in daemon mode

API version 1.8.3.0 introduces the possibility to reload the engine when running as daemon (i.e. long running CLI script).
This will allow to use automatically the updates from the WURFL Updater without restart the daemon.

Example:

```

$engine = new \ScientiaMobile\WURFL\WURFLEngine($container);

while (true) {

    // Check if the repository has been updated and reload the engine
    if($engine->checkReload()) {
        print 'WURFL repository has been updated' . PHP_EOL;
    }

    print $engine->getVersion() . PHP_EOL;
    
    sleep(5);
}


```

# Using the WURFL PHP API #

## Basic usage ##
Please look sample of the configuration files in examples/demo/ directory.

```
$root_dir = __DIR__;

require_once $root_dir . '/vendor/autoload.php';

$container = require '/path/to/config.php';

// Create a WURFL Engine
$wurfl_engine = new \ScientiaMobile\WURFL\WURFLEngine($container);
```

Now you can use some of the `WURFLEngine` class methods;

```
$device = $wurfl_engine->getDeviceForHttpRequest();
$device->getCapability("is_wireless_device");
$device->getVirtualCapability("is_smartphone");
```

### Getting the device ###

You have four methods for retrieving a device:

1. `getDeviceForHttpRequest()`

    Most of the time you will use this method, and the API will create the HttpRequest object for you.

2. `getDeviceForUserAgent(string $user_agent)`

    Used to query the API for a given User Agent string

3. `getDeviceForRequest(ScientiaMobile\WURFL\Request\HttpRequestInterface $request)`

4. `getDevice(string $device_id)`

    Gets the device by its device ID (ex: `apple_iphone_ver1`)

Usage example:

```
$device = $wurfl_engine->getDeviceForHttpRequest();
```

### Getting the device properties and its capabilities ###

The properties Device ID and Fall Back Device ID are properties of the device:

```
$device_id = $device->getID();
```

To get the value of a capability, use `getCapability()`:

```
$value = $device->getCapability("is_tablet");
$all_caps = $device->getAllCapabilities();
```

To get the value of a virtual capability, use `getVirtualCapability()`:

```
$value = $device->getVirtualCapability("is_smartphone");
$all_virtual_caps = $device->getAllVirtualCapabilities();
```

### Useful Methods ###
The root WURFLEngine object has some useful methods:

```
$groups = $wurfl_engine->getGroupsForDeviceAspect();
$is_defined = $wurfl_engine->isCapabilityDefined("foobar");
```

If you have any questions, please take a look at the documentation on http://www.scientiamobile.com,
and/or the ScientiaMobile Support Forum at http://www.scientiamobile.com/forum

# License #
For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
