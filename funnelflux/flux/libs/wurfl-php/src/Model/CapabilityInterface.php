<?php
namespace ScientiaMobile\WURFL\Model; interface CapabilityInterface { public function compare(Capability $sp2fa4bd); public function name(); public function value(); public function asBool(); }