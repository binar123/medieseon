<?php
require_once dirname(__FILE__) . '/postback.php'; require_once dirname(__FILE__) . '/../../db/dbTableApplication.php'; require_once dirname(__FILE__) . '/../../db/dbTableHitConversions.php'; require_once dirname(__FILE__) . '/../../db/dbTableHitRecords.php'; require_once dirname(__FILE__) . '/../../db/dbTableHistory.php'; require_once dirname(__FILE__) . '/../../db/dbTableStatsRoot.php'; postbackDebugLog('Clickbank Notification START'); $sp5fc271 = DBTableApplication::getN()[DBTableApplication::N_CLICKBANK_NOTIFICATION_KEY]; if (Permalinks::isLocalHost()) { $sp3b9dac = '
            {

               "transactionTime": "2014-09-05T13:47:51-06:00",

               "receipt": "CWOGBZLN",


               "transactionType": "RFND",

               "vendor": "testacct",

               "affiliate": "bobkelso",

               "role": "VENDOR",

               "totalAccountAmount": 0.00,


               "paymentMethod": "VISA",

               "totalOrderAmount": 0.00,


               "totalTaxAmount": 0.00,


               "totalShippingAmount": 0.00,

               "currency": "USD",


               "orderLanguage": "EN",


               "trackingCodes": [
                   "122983"
               ],

               "lineItems": [
                   {

                       "itemNo": "1",

                      "productTitle": "Product Title",

                       "shippable": true,

                       "recurring": true,


                       "accountAmount": 0.00,

                       "quantity": 0,

                       "downloadUrl": "<download_url>"
                   }
               ],
               "customer": {


                   "shipping": {
                       "firstName": "TEST",
                       "lastName": "GUY",
                       "fullName": "Test Guy",
                      "phoneNumber": "",
                       "email": "test@example.net",
                       "address": {
                           "address1": "12 Test Lane",
                           "address2": "Suite 100",
                           "city": "LAS VEGAS",
                           "county": "LAS VEGAS",
                           "state": "NV",
                           "postalCode": "89101",
                           "country": "US"
                       }
                   },


                   "billing": {
                       "firstName": "TEST",
                       "lastName": "GUY",
                       "fullName": "Test Guy",
                       "phoneNumber": "",
                       "email": "test@example.net",
                       "address": {

                           "state": "NV",

                           "postalCode": "89101",

                           "country": "US"
                       }
                   }
               },

               "upsell": {

                   "upsellOriginalReceipt": "XXXXXXXX",

                   "upsellFlowId": 55,

                   "upsellSession": "VVVVVVVVVV",

                   "upsellPath": "upsell_path"
               },


               "hopfeed": {
                   "hopfeedClickId": "hopfeed_click",
                   "hopfeedApplicationId": 0000,
                   "hopfeedCreativeId": 0000,
                   "hopfeedApplicationPayout": 0.00,
                   "hopfeedVendorPayout": 0.00
               },

               "version": 6.0,


               "attemptCount": 1,

               "vendorVariables": {
                  "v1": "variable1", 
                  "v2": "variable2" 
               }
           }'; } else { $sp3b9dac = json_decode(file_get_contents('php://input')); } if ($sp3b9dac) { if (is_object($sp3b9dac)) { $spbdefbe = $sp3b9dac->{'notification'}; $sp362a2d = $sp3b9dac->{'iv'}; $spf85d8b = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, substr(sha1($sp5fc271), 0, 32), base64_decode($spbdefbe), MCRYPT_MODE_CBC, base64_decode($sp362a2d)), ' ..'); } else { $spf85d8b = $sp3b9dac; } $sp01c5a5 = json_decode($spf85d8b); if ($sp01c5a5) { postbackDebugLog('Clickbank Notification: Serialized order = ' . serialize($sp01c5a5)); $sp55dfed = $sp01c5a5->{'transactionType'}; $spab787a = $sp01c5a5->{'receipt'}; $sp7d62cb = $sp01c5a5->{'totalAccountAmount'}; $sp8350a5 = $sp01c5a5->{'trackingCodes'}; if (is_array($sp8350a5) && !empty($sp8350a5)) { foreach ($sp8350a5 as $sp1cc944) { if (processCBTransaction($sp55dfed, $sp1cc944, $spab787a, $sp7d62cb)) { break; } } } else { postbackDebugLog('Clickbank Notification: NO tid given!'); switch ($sp55dfed) { case 'RFND': case 'CGBK': case 'INSF': postbackDebugLog('Clickbank Notification: We want to process a refund. Try to find the previous conversion via the transaction id!'); if ($spab787a && !isEmpty(trim($spab787a))) { $sp80dfce = DBTableHitConversions::getAllByTransactionId($spab787a); if (count($sp80dfce) == 1) { $spceed32 = $sp80dfce[0]->getHitId(); postbackDebugLog("Clickbank Notification: Hit ID {$spceed32} corresponds to this transaction id ({$spab787a})"); processCBTransaction($sp55dfed, $spceed32, $spab787a, $sp7d62cb); } else { postbackDebugLog("Clickbank Notification: No conversion found for transactionId = {$spab787a}"); } } else { postbackDebugLog('Clickbank Notification: transaction id is empty... Cannot process this refund...'); } break; } } } } postbackDebugLog('Clickbank Notification END'); die; function processCBTransaction($sp55dfed, $sp1cc944, $spab787a, $sp7d62cb) { $spc1fdcb = false; switch ($sp55dfed) { case 'SALE': case 'BILL': case 'JV_SALE': case 'JV_BILL': case 'TEST_SALE': case 'TEST_BILL': case 'TEST_JV_SALE': case 'TEST_JV_BILL': $spf18aff = DBTableHitRecords::getFluxOfferByHitId($sp1cc944); if ($spf18aff) { postbackDebugLog('Clickbank Notification: Processing conversion'); processConversion($sp1cc944, $spab787a, $sp7d62cb, $spf18aff->getId()); $spc1fdcb = true; } else { DBTableHistory::addEventSystemMessage("processCBTransaction(): NO OFFER FOUND for tid '{$sp1cc944}'", DBTableHistory::EVENT_LEVEL_ERROR); } break; case 'TEST_RFND': case 'RFND': case 'CGBK': case 'INSF': $sped76df = DBTableHitConversions::loadByHitIdAndTxId($sp1cc944, $spab787a); if ($sped76df !== null) { postbackDebugLog('Clickbank Notification: Processing refund'); DBTableStatsRoot::nullifyConversion($sped76df->getIdConversion()); DBTableHitConversions::deleteByHitIdAndTxId($sp1cc944, $spab787a); $spc1fdcb = true; } else { $spab787a = $spab787a === null ? HitConversion::TXID_EMPTY : $spab787a; postbackDebugLog("Clickbank Notification: Refund cannot be processed because no conversion was recorded for this hitId ({$sp1cc944}) and transactionId ({$spab787a})"); } break; } return $spc1fdcb; }